﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Neue OpenDisk-Routine.
:NewOpenDisk		jsr	NewDisk
			txa
			bne	:101

			jsr	GetDirHead
			txa
			bne	:101

			jsr	ChkDkGEOS

			ldx	#r1L
			jsr	GetPtrCurDkNm
			LoadW	r0,curDirHead +$90
			ldx	#r0L
			ldy	#r1L
			lda	#16
			jsr	CopyFString

			ldx	#$00
::101			rts
