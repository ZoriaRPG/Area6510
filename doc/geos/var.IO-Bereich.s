﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Variablen im Bereich $D000-$DFFF.
.mob0xpos		= $d000
.mob0ypos		= $d001
.mob1xpos		= $d002
.mob1ypos		= $d003
.mob2xpos		= $d004
.mob2ypos		= $d005
.mob3xpos		= $d006
.mob3ypos		= $d007
.mob4xpos		= $d008
.mob4ypos		= $d009
.mob5xpos		= $d00a
.mob5ypos		= $d00b
.mob6xpos		= $d00c
.mob6ypos		= $d00d
.mob7xpos		= $d00e
.mob7ypos		= $d00f
.msbxpos		= $d010
.grcntrl1		= $d011
.rasreg			= $d012
.lpxpos			= $d013
.lpypos			= $d014
.mobenble		= $d015
.grcntrl2		= $d016
.moby2			= $d017
.grmemptr		= $d018
.grirq			= $d019
.grirqen		= $d01a
.mobprior		= $d01b
.mobmcm			= $d01c
.mobx2			= $d01d
.mobmobcol		= $d01e
.mobbakcol		= $d01f
.extclr			= $d020
.bakclr0		= $d021
.mcmclr0		= $d025
.mcmclr1		= $d026
.mob0clr		= $d027
.mob1clr		= $d028
.mob2clr		= $d029
.mob3clr		= $d02a
.mob4clr		= $d02b
.mob5clr		= $d02c
.mob6clr		= $d02d
.mob7clr		= $d02e
