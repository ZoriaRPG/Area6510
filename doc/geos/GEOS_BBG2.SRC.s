﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

if .p
			t "GEOS_QuellCo.ext"
endif

			n "GEOS_BBG/2.OBJ"
			f $06
			c "KERNAL_C000 V1.0"
			a "M. Kanet"
			o ReBootGEOS
			p EnterDeskTop
			i
<MISSING_IMAGE_DATA>

;*** ReBoot-Routine für GEOS aus REU laden.
:RL_ReBootGEOS		lda	#%00111111		;Seite #254 in REU aktivieren.
			sta	$dffe			;Bank #0, $FE00.
			lda	#%00000011
			sta	$dfff
			jsr	$de00			;BBG-ReBoot ausführen.
