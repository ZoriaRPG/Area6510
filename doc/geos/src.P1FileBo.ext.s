﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DA_ResetScrn		= $885d
:DB_ArrowGrafx		= $f7a9
:DB_CopyIconInTab	= $f52e
:DB_DefBoxPos		= $f3d8
:DB_DoubleClick		= $f84f
:DB_FileInBuf		= $f837
:DB_FileTabVec		= $8859
:DB_FileWinPos		= $f8c8
:DB_FilesInTab		= $8856
:DB_GetFileIcon		= $f7a1
:DB_GetFileX		= $8857
:DB_GetFileY		= $8858
:DB_GetFiles		= $f688
:DB_Icon_OPEN		= $f5a0
:DB_InvSlctFile		= $f842
:DB_MoveFileList	= $f7cc
:DB_PutFileNames	= $f87a
:DB_SelectedFile	= $885c
:DB_SetFileNam		= $f861
:DB_SetWinEntry		= $f8f0
:DB_SlctNewFile		= $f743
:DB_VecDefTab		= $43
:DB_WinFirstFile	= $885c
:DB_WinLastFile		= $885d
:FileEntryHigh		= $0e
:FileWinIconArea	= $0e
:FileWinXsize		= $7c
:FileWinYsize		= $58
:FilesInWindow		= $05
:SaveSwapFile		= $d839
:SwapFileName		= $d82d
:xRstrFrmDialogue	= $f429
:zpage			= $00
