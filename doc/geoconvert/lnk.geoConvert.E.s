﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

; This is the linker file to combine the source modules to
; an application file.

			n "geoConvert64.EN"
			c "geoConv EN  V4.2"
			a "Markus Kanet"

			h "Convert files from GEOS to CBM, maximum filesize supported is about 16Mb!"

			m
			- "mod.#0"
			- "mod.#1"
			- "mod.#2"
			- "mod.#3"
			- "mod.#4"
			- "mod.#5"
			- "mod.#6"
			- "mod.#7"
			/

