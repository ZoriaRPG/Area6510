# Documentation

For historical and archiving reasons i have converted some original source code files into UTF-8/plaintext files since the original source code is provided in a special multiple-stream file format which can not be converted to a plaint text file easily.
The files located inside the /doc directory are just documentation, this is no computable source code. Also some files will be missing to compile these applications.

### License

The documentation is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit:
http://creativecommons.org/licenses/by-sa/4.0/
