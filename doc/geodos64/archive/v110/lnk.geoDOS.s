﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n	"geoDOS64"
			m
			-	"geoDOS.obj"
			-	"mod.#1.obj"		;Men&Drive
			-	"mod.#10.obj"		;Info
			-	"mod.#20.obj"		;Copy/Options
			-	"mod.#21.obj"		;Copy/DOStoCBM
			-	"mod.#22.obj"		;Copy/CBMtoDOS
			-	"mod.#30.obj"		;DOS/Format+Rename
			-	"mod.#31.obj"		;DOS/Dir
			-	"mod.#40.obj"		;CBM/Format+Rename
			-	"mod.#41.obj"		;CBM/Dir
			-	"mod.#42.obj"		;CBM/Partition
			-	"mod.#50.obj"		;CBM+DOS/Ren+Del
			-	"mod.#100.obj"		;DoCopy/DOStoCBM
			-	"mod.#101.obj"		;DoCopy/CBMtoDOS
			/
