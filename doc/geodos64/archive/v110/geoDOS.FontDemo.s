﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

geoDOS - Icons (c) by M. Kanet

Punktgröße: 6 Punkte
			- =	- _ =			_

Punktgröße: 9 Punkte
			a =	a b =			b	c =	c
			ü =	ü - =			-	C = C

Punktgröße: 16 Punkte
			a =	a b =			b	c =	c
			d =	d e =			e	f =	f
			g =	g h =			h	i =	i
			j =	j k =			k	l =	l
			m =	m n =			n	z =	z

			A =	A B =			B	C =	C
			D =	D E =			E	F =	F
			G =	G H =			H	I =	I
			J =	J K =			K	L =	L
			M =	M N =			N	O =	O
			P =	P Q =			Q	R =	R
			S =	S T =			T	U =	U
			V =	V W =			W	X =	X
			Y =	Y Z =			Z

			0 =	0 1 =			1

Punktgröße: 17 Punkte

			ab	ab cd			cd	ef	ef
			gh	gh ij			ij	kl	kl
			mn	mn op			op	qr	qr
			s	s t			t	u	u
			v	v wx			wx	§	§

Punktgröße: 24 Punkte
			a	a
			b	b c			c	d	d

Punktgröße: 0 Punkte
abcdefghijklmnopqrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=

Punktgröße: 6 Punkte
abcdefghijklmnopqrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=

Punktgröße: 9 Punkte
abcdefghijklmnopqrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=

Punktgröße: 16 Punkte
abcdefghijklmnopqrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=

Punktgröße: 17 Punkte
abcdefgh
ijklmnop
qrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=

Punktgröße: 24 Punkte
abcdefghijklmnopqrstuvwxyzäöüß
ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ
0123456789-_,.;:#'+*?`^!"§$%&/()=
