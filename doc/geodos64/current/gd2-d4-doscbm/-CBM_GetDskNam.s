﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Diskettenname einlesen.
; Datum			: 02.07.97
; Aufruf		: JSR  CBM_GetDskNam
; Übergabe		: -
; Rückgabe		: xReg	Byte $00 = Disk im Laufwerk
; Verändert		: AKKU,xReg,yReg
;			  r0  bis r4
; Variablen		: -
; Routinen		: -NewOpenDisk Neue Diskette öffnen
;			  -DiskError Diskettenfehler anzeigen
;			  -GetPtrCurDkNm									 Zeiger auf Diskettenname
;			  -ConvertChar ASCII nach GEOS-ASCII wandeln
;******************************************************************************

;*** L450: Datenträgername ermitteln.
.CBM_GetDskNam		jsr	NewOpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			beq	:101			;Nein, weiter...
			jmp	DiskError		;Abbruch.

::101			ldx	#r0L			;Zeiger auf Diskettenname einlesen.
			jsr	GetPtrCurDkNm

			ldy	#$0f
::102			lda	(r0L),y			;Diskettenname kopieren.
			jsr	ConvertChar		;ASCII nach GEOS-ASCII wandeln.
			sta	cbmDiskName,y
			dey
			bpl	:102
			rts

;*** Speicher für Disketten-Namen.
.cbmDiskName		s 16 +1
