﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "GD_CBM"

			h   "CBM Disketten-/Datei-"
			h   "funktionen von GeoDOS..."
			h   ""
			h   "C64 (& C128 40-Zeichen)

			m
			-   "mod.#401.obj"		;Diskette formatieren/umbenennen.
			-   "mod.#402.obj"		;Verzeichnis anzeigen.
			-   "mod.#403.obj"		;Partition/Unterverzeichnisse.
			-   "mod.#404.obj"		;Datei-Info anzeigen.
			-   "mod.#405.obj"		;Diskette kopieren.
			-   "mod.#406.obj"		;    "    Kopier-Routine.
			-   "mod.#407.obj"		;Verzeichnis sortieren.
			-   "mod.#408.obj"		;Verzeichnis drucken.
			-   "mod.#409.obj"		;Dateien löschen/umbenennen.
			-   "mod.#410.obj"		;Dateien drucken.
			-   "mod.#411.obj"		;Validate Disk/Undelete Files.
