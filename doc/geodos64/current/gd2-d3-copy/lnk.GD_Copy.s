﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "GD_Copy"

			h   "Kopier-/Konvertierungs-"
			h   "funktionen von GeoDOS..."
			h   ""
			h   "C64 (& C128 40-Zeichen)

			m
			-   "mod.#201.obj"		;Kopier-Optionen definieren.
			-   "mod.#202.obj"		;Dateiauswahl DOS nach CBM.
			-   "mod.#203.obj"		;     "       CBM nach DOS.
			-   "mod.#204.obj"		;     "       CBM nach CBM.
			-   "mod.#205.obj"		;Kopierfehler.
			-   "mod.#210.obj"		;Kopieren: DOS nach CBM.
			-   "mod.#211.obj"		;Kopieren: DOS nach GW.
			-   "mod.#212.obj"		;Kopieren: DOS nach CBM Fast.
			-   "mod.#213.obj"		;Kopieren: CBM nach DOS.
			-   "mod.#214.obj"		;Kopieren: GW  nach DOS.
			-   "mod.#215.obj"		;Kopieren: CBM nach DOS Fast.
			-   "mod.#216.obj"		;Kopieren: CBM nach CBM.
			-   "mod.#217.obj"		;Kopieren: CBM nach GW.
			-   "mod.#218.obj"		;Kopieren: GW  nach CBM.
			-   "mod.#219.obj"		;Kopieren: GW  nach GW.
			-   "mod.#220.obj"		;Kopieren: CBM nach CBM Fast.
