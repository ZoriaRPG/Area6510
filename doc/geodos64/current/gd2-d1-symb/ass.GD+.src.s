﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Systemlabels.
if .p
			t "src.GEOS_MP3.64"
			t "SymbTab_1"
			t "SymbTab_2"
			t "SymbTab64"
			t "TopMac"
			t "ass.Drives"
			t "ass.NativeDir"
			t "ass.Macro"
endif

			n "ass.GD"
			c "ass.SysFile V1.0"
			a "Markus Kanet"
			f $04

			o $4000

;*** AutoAssembler Dateien erstellen.
:MAIN__1		OPEN_SYMBOL

			b $f0,"ass.GeoDOS+.src",$00
			b $f0,"ass.GD_MAIN+.src",$00
			b $f0,"ass.GD_COPY+.src",$00
			b $f0,"ass.GD_DOS+.src",$00
			b $f0,"ass.GD_CBM+.src",$00
			b $f0,"ass.GD_CONV+.src",$00
			b $f0,"ass.GD_TOOL+.src",$00
			b $f0,"ass.GD_HELP+.src",$00
			b $ff
