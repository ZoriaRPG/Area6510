﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:INIT_CBM		OPEN_DOSCBM

			b $f0,"cbm.FormatRename",$00
			b $f0,"cbm.Dir",$00
			b $f0,"cbm.Part_NDir",$00
			b $f0,"cbm.FileInfo",$00
			b $f0,"cbm.DSKtoDSK",$00
			b $f0,"cbm.DoDSKtoDSK",$00
			b $f0,"cbm.SortDir",$00
			b $f0,"cbm.PrintDir",$00
			b $f0,"cbm.RenDelFile",$00
			b $f0,"cbm.PrintFile",$00
			b $f0,"cbm.ValidUndel",$00

			b $f1
			lda	a1H
			jsr	SetDevice
			jsr	OpenDisk
			LoadW	r0,:102
			jsr	DeleteFile
			LoadW	a0,:103
			rts

::102			b "GD_CBM",$00

::103			b $f5
			b $f0,"lnk.GD_CBM",$00
