﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Systemlabels.
if .p
			t "src.GEOS_MP3.64"
			t "SymbTab_1"
			t "SymbTab_2"
			t "SymbTab64"
			t "TopMac"
			t "ass.Drives"
			t "ass.Macro"
endif

			o $4000
			c "ass.SysFile V1.0"
			n "ass.GeoDOS"
			f $04

:MainInit		OPEN_TARGET
			OPEN_SYMBOL

:MainInit1		t "-A3_Main"
			b $f4
:MainInit2		t "-A3_Copy"
			b $f4
:MainInit3		t "-A3_DOS"
			b $f4
:MainInit4		t "-A3_CBM"
			b $f4
:MainInit5		t "-A3_Convert"
			b $f4
:MainInit6		t "-A3_Tools"

:MainInit7		t "-A3_Help"

			b $ff

;--- NativeMode-Unterverzeichnisse einbinden.
			t "ass.NativeDir"
