﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Neuen Zeichensatz aktivieren.
; Datum			: 02.07.97
; Aufruf		: JSR  UseGDFont
; Übergabe		: -
; Rückgabe		: -
; Verändert		: AKKU,xReg,yReg
;			  r0
; Variablen		: -
; Routinen		: -
;******************************************************************************

;*** Neuen Zeichensatz aktivieren.
.UseGDFont		LoadW	r0,Font
			jmp	LoadCharSet

;*** Spezieller Zeichensatz für GeoDOS (7x8)
:Font			v 8,"fnt.GeoDOS #1"
