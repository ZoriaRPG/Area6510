﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:INIT_CONV		OPEN_CONVERT

			b $f0,"src.GD_CONVERT",$00
			b $f0,"dos.ConvTab#02",$00
			b $f0,"dos.ConvTab#03",$00
			b $f0,"dos.ConvTab#04",$00
			b $f0,"dos.ConvTab#05",$00
			b $f0,"dos.ConvTab#06",$00
			b $f0,"dos.ConvTab#07",$00
			b $f0,"dos.ConvTab#08",$00
			b $f0,"dos.ConvTab#09",$00
			b $f0,"dos.ConvTab#10",$00
			b $f0,"cbm.ConvTab#42",$00
			b $f0,"cbm.ConvTab#43",$00
			b $f0,"cbm.ConvTab#44",$00
			b $f0,"cbm.ConvTab#45",$00
			b $f0,"cbm.ConvTab#46",$00
			b $f0,"cbm.ConvTab#47",$00
			b $f0,"cbm.ConvTab#48",$00
			b $f0,"cbm.ConvTab#49",$00
			b $f0,"cbm.ConvTab#50",$00
			b $f0,"txt.ConvTab#82",$00
			b $f0,"txt.ConvTab#83",$00

			b $f1
			lda	a1H
			jsr	SetDevice
			jsr	OpenDisk
			LoadW	r0,:102
			jsr	DeleteFile
			LoadW	a0,:103
			rts

::102			b "GD_CONVERT",$00

::103			b $f5
			b $f0,"lnk.GD_CONVERT",$00
