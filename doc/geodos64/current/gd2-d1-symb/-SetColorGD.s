﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Farbbox zeichnen.
; Datum			: 05.07.97
; Aufruf		: JSR  (Bereichsname)
; Übergabe		: -
; Rückgabe		: -
; Verändert		: AKKU,xReg,yReg
;			  r5  bis r7
; Variablen		: -
; Routinen		: -
;******************************************************************************

;*** Farbbox zeichnen.
.i_C_ColorBack		ldx	#$00
			b $2c
.i_C_ColorClr		ldx	#$01
			b $2c
.i_C_MenuBack		ldx	#$02
			b $2c
.i_C_MenuTBox		ldx	#$03
			b $2c
.i_C_MenuMIcon		ldx	#$04
			b $2c
.i_C_MenuDIcon		ldx	#$05
			b $2c
.i_C_MenuClose		ldx	#$06
			b $2c
.i_C_MenuTitel		ldx	#$07
			b $2c

.i_C_Balken		ldx	#$08
			b $2c
.i_C_Register		ldx	#$09
			b $2c

.i_C_DBoxClose		ldx	#$0c
			b $2c
.i_C_DBoxTitel		ldx	#$0d
			b $2c
.i_C_DBoxBack		ldx	#$0e
			b $2c
.i_C_DBoxDIcon		ldx	#$0f
			b $2c

.i_C_IBoxBack		ldx	#$10
			b $2c

.i_C_FBoxClose		ldx	#$11
			b $2c
.i_C_FBoxTitel		ldx	#$12
			b $2c
.i_C_FBoxBack		ldx	#$13
			b $2c
.i_C_FBoxDIcon		ldx	#$14
			b $2c

.i_C_MainIcon		ldx	#$15
			b $2c

.i_C_GEOS		ldx	#$16
			lda	colSystem,x
			jmp	i_UserColor
