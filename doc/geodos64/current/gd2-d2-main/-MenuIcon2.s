﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Diskettenfunktionen.
:Icon_20
<MISSING_IMAGE_DATA>

:Icon_21
<MISSING_IMAGE_DATA>

:Icon_22
<MISSING_IMAGE_DATA>

:Icon_23
<MISSING_IMAGE_DATA>

:Icon_24
<MISSING_IMAGE_DATA>

:Icon_25
<MISSING_IMAGE_DATA>

:Icon_26
<MISSING_IMAGE_DATA>

:Icon_27
<MISSING_IMAGE_DATA>

:Icon_28
<MISSING_IMAGE_DATA>

:Icon_29
<MISSING_IMAGE_DATA>

;*** Dateifunktionen
:Icon_30
<MISSING_IMAGE_DATA>

:Icon_31
<MISSING_IMAGE_DATA>

:Icon_32
<MISSING_IMAGE_DATA>

:Icon_33
<MISSING_IMAGE_DATA>

:Icon_34
<MISSING_IMAGE_DATA>

:Icon_35
<MISSING_IMAGE_DATA>

;*** Kopieren.
:Icon_40
<MISSING_IMAGE_DATA>

:Icon_41
<MISSING_IMAGE_DATA>

:Icon_42
<MISSING_IMAGE_DATA>

:Icon_50
<MISSING_IMAGE_DATA>

:Icon_51
<MISSING_IMAGE_DATA>

:Icon_52
<MISSING_IMAGE_DATA>

:Icon_60
<MISSING_IMAGE_DATA>

:Icon_61
<MISSING_IMAGE_DATA>

:Icon_62
<MISSING_IMAGE_DATA>

:Icon_63
<MISSING_IMAGE_DATA>

