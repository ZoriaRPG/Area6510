﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n   "GeoDOS64"			;Name VLIR-Datei.

			h   "Eine neue Generation der"
			h   "      DeskTop-Oberflächen..."
			h   ""
			h   "C64 (& C128 40-Zeichen)

			m
			-   "mod.#100.obj"		;Hauptmodul.
			-   "mod.#101.obj"		;Hardware-Erkennung.
			-   "mod.#102.obj"		;Info - Screen.
			-   "mod.#103.obj"		;Laufwerksauswahl.
			-   "mod.#104.obj"		;Menü.
			-   "mod.#105.obj"		;Applikationen/Dokumente.
			-   "mod.#106.obj"		;BASIC-Programme starten.
			-   "mod.#107.obj"		;Farben ändern.
			-   "mod.#108.obj"		;Uhrzeit ändern.
			-   "mod.#109.obj"		;Online-Hilfe.
			-   "mod.#110.obj"		;Laufwerke tauschen.
			-   "mod.#111.obj"		;Diskettenfehler.
			-   "mod.#112.obj"		;Parken/TurnOff.
			-   "mod.#113.obj"		;GeoDOS beenden.
			/
