﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Fenstergrenzen setzen.
.SetWindow_a		ldy	#$00
			b $2c
.SetWindow_b		ldy	#$06
			b $2c
.SetWindow_c		ldy	#$0c
			ldx	#$00
::101			lda	MseMoveAreas,y
			sta	mouseTop,x
			iny
			inx
			cpx	#$06
			bne	:101
			rts

;*** Fenstergrenzen.
:MseMoveAreas		b $00,$c7			;Vollbild.
			w $0000,$013f
			b $28,$8f			;Dialogbox.
			w $0030,$010f
			b $80,$a7			;Laufwerksauswahlbox.
			w $0038,$00c7
