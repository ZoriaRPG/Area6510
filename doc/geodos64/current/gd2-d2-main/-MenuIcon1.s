﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Menü-Icons.
:Icon_01
<MISSING_IMAGE_DATA>

:Icon_02
<MISSING_IMAGE_DATA>

:Icon_03
<MISSING_IMAGE_DATA>

:Icon_04
<MISSING_IMAGE_DATA>

:Icon_05
<MISSING_IMAGE_DATA>

:Icon_14
<MISSING_IMAGE_DATA>

:Icon_15
<MISSING_IMAGE_DATA>

:Icon_16
<MISSING_IMAGE_DATA>

:Icon_17
<MISSING_IMAGE_DATA>

:Icon_18
<MISSING_IMAGE_DATA>

