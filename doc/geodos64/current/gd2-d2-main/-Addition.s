﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Wert 2 zu r15 addieren.
; Datum			: 14.11.18
; Aufruf		: JSR  Add_2_r15
; Übergabe		: r15	Word 16Bit-Zahl
; Rückgabe		: r15	Word 16Bit-Zahl
; Verändert		: AKKU
;			  r15
; Variablen		: -
; Routinen		: -
;******************************************************************************

;******************************************************************************
; Funktion		: Wert 16 zu r15 addieren.
; Datum			: 14.11.18
; Aufruf		: JSR  Add_16_r15
; Übergabe		: r15	Word 16Bit-Zahl
; Rückgabe		: r15	Word 16Bit-Zahl
; Verändert		: AKKU
;			  r15
; Variablen		: -
; Routinen		: -
;******************************************************************************

;*** Wert zu r15 addieren.
.Add_2_r15		lda	#2
			b $2c
.Add_16_r15		lda	#16
			clc
			adc	r15L
			sta	r15L
			bcc	:1
			inc	r15H
::1			rts
