﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Textausgabe
; Datum			: 02.07.97
; Aufruf		: JSR  PutXYText									 Text an Y-Position ausgeben
; Übergabe		: yReg	Byte Y-Koordinate
;			  AKKU,xRegWord Zeigfer auf Textstring
; Rückgabe		: Bildschirmausgabe
; Verändert		: AKKU,xReg,yReg
;			  r0 ,r1L
;			  r2  bis r10
;			  r12 und r13
; Variablen		: -
; Routinen		: -PutString String ausgeben
;******************************************************************************

;*** Textausgabe.
.PutXYText		sty	r1H
.PutText		sta	r0L
			stx	r0H
			jmp	PutString
