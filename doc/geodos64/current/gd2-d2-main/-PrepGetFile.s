﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Vorbereitungen für "GetFile"-Routine.
; Datum			: 04.07.97
; Aufruf		: JSR  PrepGetFile
; Übergabe		: -
; Rückgabe		: -
; Verändert		: AKKU,xReg,yReg
;			  r0  bis r15
; Variablen		: -
; Routinen		: -i_FillRam Speicherbereich füllen
;******************************************************************************

;*** Vorbereitungen für "GetFile"-Routine.
.PrepGetFile		jsr	i_FillRam
			w	417
			w	dlgBoxRamBuf
			b	$00

			lda	#$00
			tax
::101			sta	r0L,x
			inx
			cpx	#(r15H-r0L) +1
			bcc	:101
			rts
