﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Zeichen nach GEOS-ASCII wandeln.
; Datum			: 02.07.97
; Aufruf		: JSR  ConvertChar
; Übergabe		: AKKU	Byte Textzeichen
; Rückgabe		: AKKU	Byte GEOS-Textzeichen
; Verändert		: AKKU
; Variablen		: -
; Routinen		: -
;******************************************************************************

;*** Zeichen nach GEOS-ASCII wandeln.
.ConvertChar		cmp	#$00
			beq	:101
			cmp	#$a0
			bne	:102
::101			lda	#" "
::102			cmp	#$20
			bcc	:101
			cmp	#$7f
			bcc	:103
			sbc	#$20
			jmp	:102
::103			rts
