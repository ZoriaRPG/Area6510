﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: NativeMode-Hauptverzeichnis aktivieren.
; Datum			: 03.07.97
; Aufruf		: JSR  New_CMD_Root
; Übergabe		: -
; Rückgabe		: xReg	 $00 = Kein Fehler.
; Verändert		: AKKU,xReg,yReg
;			  r0  bis r15
; Variablen		: -
; Routinen		: -$9050!!!!!! GateWay: Native-RootDir
;			  -GetDirHead BAM einlesen
;******************************************************************************

;*** Routine zum setzen des Root-Verzeichnisses auf CMD-Geräten.
.New_CMD_Root		lda	$9050			;Prüfen ob der Befehl existiert.
			cmp	#$4c
			bne	:1
			jsr	$9050			;Nur gateWay/MegaPatch!
							;Hauptverzeichnis aktivieren.
::1			jmp	GetDirHead		;BAM einlesen. Der Grund ist der Bug
							;in div. Laufwerkstreibern der beim
							;Aufruf von OpenDisk die BAM zerstört.

;******************************************************************************
; Funktion		: NativeMode-Unterverzeichnis aktivieren.
; Datum			: 03.07.97
; Aufruf		: JSR  New_CMD_SubD
; Übergabe		: -
; Rückgabe		: xReg	 $00 = Kein Fehler.
; Verändert		: AKKU,xReg,yReg
;			  r0  bis r15
; Variablen		: -
; Routinen		: -$9053!!!!!! GateWay: Native-SubDir
;			  -GetDirHead BAM einlesen
;******************************************************************************

;*** Routine zum setzen eines Unterverzeichnisses auf CMD-Geräten.
.New_CMD_SubD		lda	$9053			;Prüfen ob der Befehl existiert.
			cmp	#$4c
			bne	:1
			jsr	$9053			;Nur gateWay/MegaPatch!
							;Unterverzeichnis aktivieren.
::1			jmp	GetDirHead		;BAM einlesen. Der Grund ist der Bug
							;in div. Laufwerkstreibern der beim
							;Aufruf von OpenDisk die BAM zerstört.
