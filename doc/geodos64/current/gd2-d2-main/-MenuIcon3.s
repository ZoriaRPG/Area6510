﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Sonderfunktionen.
:Icon_70
<MISSING_IMAGE_DATA>

:Icon_71
<MISSING_IMAGE_DATA>

:Icon_72
<MISSING_IMAGE_DATA>

:Icon_73
<MISSING_IMAGE_DATA>

:Icon_74
<MISSING_IMAGE_DATA>

:Icon_75
<MISSING_IMAGE_DATA>

:Icon_76
<MISSING_IMAGE_DATA>

:Icon_77
<MISSING_IMAGE_DATA>

:Icon_78
<MISSING_IMAGE_DATA>

:Icon_79
<MISSING_IMAGE_DATA>

;*** Systemsteuerung.
:Icon_80
<MISSING_IMAGE_DATA>

:Icon_81
<MISSING_IMAGE_DATA>

:Icon_82
<MISSING_IMAGE_DATA>

:Icon_83
<MISSING_IMAGE_DATA>

:Icon_84
<MISSING_IMAGE_DATA>

:Icon_85
<MISSING_IMAGE_DATA>

:Icon_86
<MISSING_IMAGE_DATA>

:Icon_87
<MISSING_IMAGE_DATA>

:Icon_88
<MISSING_IMAGE_DATA>

:Icon_89
<MISSING_IMAGE_DATA>

:Icon_90
<MISSING_IMAGE_DATA>

:Icon_98
<MISSING_IMAGE_DATA>

:Icon_99
<MISSING_IMAGE_DATA>

:Icon_10
<MISSING_IMAGE_DATA>

:Icon_12
<MISSING_IMAGE_DATA>

:Icon_13
<MISSING_IMAGE_DATA>

;--- Ergänzung: 22.12.18/M.Kanet
;Unterstützung für Option D'n'D On/Off ergänzt.
;Kann im Menü TOOLS ein-/ausgeschaltet werden.
:Icon_91
<MISSING_IMAGE_DATA>

:Icon_92
<MISSING_IMAGE_DATA>
