# Area6510

### GEODOS64
This is a documentation of the source code for GeoDOS 64.

This version is nearly the same as the previously released version 2.95 except for some small fixes when converting files from geoWrite to DOS/CBM.

The source code is under review and will be released later together with a new binary release.

### What is this?
GeoDOS is a tool for the GEOS Graphical Desktop Environment.

GeoDOS can handle DOS formatted 3,5" diskettes. It is possible to read files from a DOS-disk or write sequantial data files to a DOS-disk.

GeoDOS also can convert files from/to geoWrite or DOS/Linux using converting tables. It can also re-format geoWrite documents using a default setup of font, printer or ruler settings.

GeoDOS can also be used as a desktop replacement since it can delete/copy/rename files or whole disk.

GeoDOS includes a new tool named GeoHelpView which can be used to display some kind of 'Online help' documentation.
