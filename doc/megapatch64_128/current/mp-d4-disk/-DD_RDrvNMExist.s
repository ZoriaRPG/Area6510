﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Laufwerk kann nur einmal installiert werden.
;    Übergabe: AKKU: RealDriveType
:CheckRDrvExist		ldx	#03
::1			cmp	RealDrvType,x		;Laufwerk bereits installiert?
			bne	:2			; => Nein, weiter...

			LoadW	r0,Dlg_ActivSRAM	;Hinweis: Laufwerk kann nur
			jsr	DoDlgBox		;einmal verwendet werden!
			ldx	#DEV_NOT_FOUND
			rts

::2			dex				;Alle Laufwerke durchsucht?
			bpl	:1			; => Nein, weiter...
			ldx	#NO_ERROR
			rts

;*** Dialogbox.
:Dlg_ActivSRAM		b %11100001
			b DBGRPHSTR
			w Dlg_DrawBoxTitel
			b DBTXTSTR ,$10,$0b
			w DlgBoxTitle
			b DBTXTSTR ,$10,$20
			w :51
			b DBTXTSTR ,$10,$2a
			w :52
			b OK       ,$10,$48
			b NULL

if Sprache = Deutsch
::51			b PLAINTEXT,BOLDON
			b "Dieser Laufwerkstyp kann",NULL
::52			b "nur einmal verwendet werden!",NULL
endif

if Sprache = Englisch
::51			b PLAINTEXT,BOLDON
			b "This drive type can only",NULL
::52			b "be used once!",NULL
endif
