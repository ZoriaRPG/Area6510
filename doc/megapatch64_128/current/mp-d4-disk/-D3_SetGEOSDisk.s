﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = RL_NM!RD_NM!RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
::tmp0b = FD_NM!HD_NM!HD_NM_PP!IEC_NM!S2I_NM
::tmp0  = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** GEOS-Diskette erstellen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r3,r4
:xSetGEOSDisk		lda	DirHead_Tr		;Haupt-/Unterverzeichnis ?
			ldx	DirHead_Se
			cmp	#$01
			bne	:51
			cpx	#$01
			bne	:51
			jsr	xGetDirHead
			txa
			bne	:56

			jmp	:SetGEOSCode

::51			pha
			txa
			pha
			jsr	xOpenRootDir
			txa
			beq	:53
::52			pla
			pla
			rts

::53			jsr	:SetGEOSCode
			txa
			bne	:52

			pla
			sta	r1H
			pla
			sta	r1L
			jmp	xOpenSubDir
							;in Unterverzeichnis eintragen.
::SetGEOSCode		jsr	Set_curDirHead		;Zeiger auf BAM berechnen.
			jsr	xCalcBlksFree		;Anzahl freier Blocks berechnen.

			ldx	#$03
			lda	r4L
			ora	r4H			;Sektor auf Diskette frei ?
			beq	:56			; => Nein, Abbruch...

			LoadB	r3L,$01
			LoadB	r3H,$40
			jsr	xSetNextFree		;Freien Sektor auf Diskette suchen.
			txa				;Diskettenfehler ?
			bne	:56			; => ja, Abbruch...

			MoveB	r3L,r1L
			MoveB	r3H,r1H
			jsr	Clr_diskBlkBuf		;Inhalt des BorderBlocks löschen.
			txa				;Diskettenfehler ?
			bne	:56			;Ja, Abbruch...

			lda	r1L			;Zeiger auf BorderBlock in
			ldy	r1H			;BAM zwischenspeichern.
::54			sta	curDirHead +171
			sty	curDirHead +172

			ldx	#$0f
::55			lda	FormatText     ,x	;GEOS-Formatkennung setzen.
			sta	curDirHead +173,x
			dex
			bpl	:55

			jsr	xPutDirHead		;BAM auf Diskette speichern.
::56			rts
endif

;******************************************************************************
::tmp1a = RL_81!RL_71!RL_41!RD_81!RD_71!RD_41
::tmp1b = C_41!C_71!C_81!FD_41!FD_71!FD_81!HD_41!HD_71!HD_81
::tmp1c = HD_41_PP!HD_71_PP!HD_81_PP
::tmp1  = :tmp1a!:tmp1b!:tmp1c
if :tmp1 = TRUE
;******************************************************************************
;*** GEOS-Diskette erstellen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r3,r4
:xSetGEOSDisk		jsr	xGetDirHead		;Aktuelle BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	Set_curDirHead		;Zeiger auf BAM berechnen.
			jsr	xCalcBlksFree		;Anzahl freier Blocks berechnen.

			jsr	IsDirSekFree		;Freien Verzeichnis-Sektor suchen.
			txa				;Ist Sektor frei ?
			bne	:52			; => Abbruch wenn kein Sektor frei.

			lda	#Tr_BorderBlock		;Zeiger auf BorderBlock.
			sta	r3L
			lda	#Se_BorderBlock
			sta	r3H
			jsr	SetNextFree		;BorderBlock belegen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			lda	r3H
			sta	r1H
			lda	r3L
			sta	r1L
			jsr	Clr_diskBlkBuf		;Inhalt des BorderBlocks löschen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			lda	r1L			;Zeiger auf BorderBlock in
			sta	curDirHead +171		;BAM zwischenspeichern.
			lda	r1H
			sta	curDirHead +172

			ldx	#$0f
::55			lda	FormatText     ,x	;GEOS-Formatkennung setzen.
			sta	curDirHead +173,x
			dex
			bpl	:55

			jsr	xPutDirHead		;BAM auf Diskette speichern.
::52			rts
endif
