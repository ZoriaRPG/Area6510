﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
:E_DRIVER_DATA
;******************************************************************************

;*** Include-Dateien.
;--- Ergänzung: 11.08.18/M.Kanet
;Die Routine "TestRL" ist zu Testzwecken deaktiviert, da der Test auf die
;RAMLink bereits bei der Installation erfolgt. Daher besteht kein Grund
;bei jedem RAMLink-Zugriff auf die RAMLink zu testen.
;D01			t "-D3_TestRL"
:D02			t "-D3_FindRAMLink"
:D03			t "-D3_SvLd_dir3Hd"
:D04			t "-D3_SwapDkNmDat"
:D05			t "-D3_TestTrSeAdr"
:D06			t "-D3_ClrDkBlkBuf"
:D07			t "-D3_SvLd_RegDat"
:D08			t "-D3_DefSkAdrREU"
:D09			t "-D3_Dsk_DoRAMOp"
:D10			t "-D3_Dsk_DoSekOp"
:D11			t "-D3_1541_Cache"
:D12			t "-D3_SdiskBlkBuf"
:D13			t "-D3_ScurDirHead"
:D14			t "-D3_S1stDirSek"
:D15			t "-D3_SDirHead"
:D16			t "-D3_InitForIO"
:D17			t "-D3_DoneWithIO"
:D18			t "-D3_PurgeTurbo"
:D19			t "-D3_ExitTurbo"
:D20			t "-D3_EnterTurbo"
:D21			t "-D3_OpenDir"
:D22			t "-D3_OpenDisk"
:D23			t "-D3_OpenPart"
:D25			t "-D3_LogNewPart"
:D26			t "-D3_LogNewDisk"
:D24			t "-D3_NewDisk"
:D27			t "-D3_CalcBlkFree"
:D28			t "-D3_GetDiskSize"
:D29			t "-D3_ChkDkGEOS"
:D30			t "-D3_SetGEOSDisk"
:D31			t "-D3_ChangeDDev"
:D32			t "-D3_GetBlock"
:D33			t "-D3_PutBlock"
:D34			t "-D3_VerWrBlock"
:D38			t "-D3_GetFreeDirB"
:D39			t "-D3_CreateNewDB"
:D40			t "-D3_GetDirEntry"
:D41			t "-D3_GetBorderB"
:D42			t "-D3_GetPDEntry"
:D43			t "-D3_GetPTypes"
;D44			t "-D3_RdPartHead"
:D45			t "-D3_BlkAlloc"
:D46			t "-D3_AllocFreBlk"
:D48			t "-D3_SwapBlkMode"
:D49			t "-D3_FindBAMBit"
:D50			t "-D3_SetNextFree"
:D51			t "-D3_GetDirHead"
:D52			t "-D3_PutDirHead"
:D53			t "-D3_BAMBlockJob"
:D54			t "-D3_SetBAM_TrSe"
:D55			t "-D3_IsDirSkFree"
:D56			t "-D3_GetMaxSekTr"
:D57			t "-D3_InitTD"
:D58			t "-D3_TurnOffTD"
:D59			t "-D3_GetBAMOffst"
:D60			t "-D3_TurboRout"
:D61			t "-D3_TurboPutByt"
:D62			t "-D3_TurboGetByt"
:D63			t "-D3_CopyTDByte"
:D64			t "-D3_TPutGetBlk"
:D65			t "-D3_TD_DATACLK"
:D66			t "-D3_GetDskError"
:D67			t "-D3_SendFCom"
;D68			t "-D3_FComInitDsk"
:D69			t "-D3_StashDrvDat"
