﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_71
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor-Daten über TurboDOS an Floppy senden.
:Turbo_PutBlock		lda	#< TD_WrSekData
			ldx	#> TD_WrSekData
			bne	Turbo_SetBlock

;*** Sektor-Daten über TurboDOS aus Floppy einlesen.
:Turbo_GetBlock		lda	#< TD_RdSekData
			ldx	#> TD_RdSekData
:Turbo_SetBlock		jsr	xTurboRoutSet_r1
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET/SEND-Routine übergeben.
			ldy	#$00
			rts
endif

;******************************************************************************
::tmp1 = S2I_NM
if :tmp1 = TRUE
;******************************************************************************
;*** Sektor-Daten über TurboDOS an Floppy senden.
:Turbo_PutBlock		lda	#< TD_WrSekData
			ldx	#> TD_WrSekData
			bne	Turbo_SetBlock

;*** Sektor-Daten über TurboDOS aus Floppy einlesen.
;--- Ergänzung: 17.10.18/M.Kanet
;Das SD2IEC arbeitet auch mit dem TurboDOS der 1581 und kann damit die Spuren
;1-127 lesen und schreiben. Um nur die beiden Link-Bytes einzulesen setzt das
;TurboDOS der 1581 aber Bit#7 in der Spur-Adresse. Damit kann das TurboDOS der
;1581 keine Spuren oberhalb von 128 lesen.
;Der SD2IEC-Treiber nutzt dazu jetzt den ReadSektor-Einsprung für die 1571 da
;es diese Option hier nicht gibt.
:Turbo_GetBlock		lda	#< TD_RdSekData71
			ldx	#> TD_RdSekData71
:Turbo_SetBlock		jsr	xTurboRoutSet_r1
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET/SEND-Routine übergeben.
			ldy	#$00
			rts
endif
