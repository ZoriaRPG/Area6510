﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "GEOS64.TaskMse"
			c "TaskManSet V1.0"
			a "M.Kanet/W.Grimm"
			t "G3_SymMacExt"

			f $06
			z $80

			i
<MISSING_IMAGE_DATA>

if Sprache = Deutsch
			h "TaskManager über linke+rechte Maustaste starten"
endif

if Sprache = Englisch
			h "Press left and right mouse-button to start TaskManager"
endif

;*** TaskManager über Maustasten starten.
:PatchTaskMan		lda	#%01111111
			sta	TaskManKey1 +1
			lda	#%11101110
			sta	TaskManKey2 +1
			jmp	EnterDeskTop
