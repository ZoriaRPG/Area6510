﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Auf 1581 oder SD2IEC testen.
;Dazu den Befehl "M-R",$00,$03,$03 senden.
;Die Rückmeldung "00,(OK,00,00)" deutet auf ein SD2IEC hin.
:TestSD2IEC		ldx	#8
::100			stx	curDrvTest		;Aktuelles Laufwerk merken.
			lda	driveType -8,x		;Laufwerk installiert?
			beq	:103			; => Nein, weiter...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	TestDevSD2IEC		;SD2IEC-Laufwerk?
			b $2c
::103			ldx	#$00
			txa
			ldx	curDrvTest		;SD2IEC-Flag speichern.
			sta	DrvTypeSD -8,x
			inx				;Zeiger auf nächstes Laufwerk.
			cpx	#12			;Alle Laufwerke geprüft?
			bcc	:100			; => Nein, weiter...
			rts

;*** Aktuelles Laufwerk auf SD2IEC testen.
:TestDevSD2IEC		ldx	curDrive
			lda	RealDrvType -8,x
			beq	:103
			and	#%10111111
			cmp	#$10			;CMD/RAM-Laufwerk?
			bcs	:103			; => Ja, kein SD2IEC.

			jsr	PurgeTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O aktivieren.

			lda	#$0f
			tay
			ldx	curDrive
			jsr	SETLFS			;Befehlskanal öffnen.

			jsr	OPENCHN

			lda	#<FComTest		;"M-R"-Befehl senden.
			ldx	#>FComTest
			ldy	#$06
			jsr	SendFCom

			lda	#<FComReply		;Antwort empfangen.
			ldx	#>FComReply
			ldy	#$03
			jsr	GetFData

			lda	#$0f			;Befehlskanal schließen.
			jsr	CLOSE

			ldx	#$ff			;Vorgabe: SD2IEC.
			lda	FComReply +0		;Rückmeldung auswerten.
			cmp	#"0"			;"00," ?
			bne	:101			; => Nein, Ende...
			lda	FComReply +1
			cmp	#"0"
			bne	:101
			lda	FComReply +2
			cmp	#","
			beq	:102
::101			ldx	#$00			;Kein SD2IEC.
::102			jmp	DoneWithIO

::103			ldx	#$00
			rts

:FComTest		b "M-R",$00,$03,$03
:FComReply		s $03
:curDrvTest		b $00

;*** Floppy-Befehl senden.
:SendFCom		sta	r0L
			stx	r0H
			sty	r1L

			ldx	#$0f
			jsr	CKOUT

			lda	#$00
			sta	:101 +1
::101			ldy	#$ff
			cpy	r1L
			beq	:102
			lda	(r0L),y
			jsr	BSOUT
			inc	:101 +1
			jmp	:101

::102			jmp	CLRCHN

;*** Rückmeldung von Floppy empfangen.
:GetFData		sta	r0L
			stx	r0H
			sty	r1L

			ClrB	STATUS

			ldx	#$0f
			jsr	CHKIN

			lda	#$00
			sta	:101 +4
::101			jsr	GETIN
			ldy	#$ff
			cpy	r1L
			beq	:102
			sta	(r0L),y
			inc	:101 +4
			jmp	:101

::102			jmp	CLRCHN
