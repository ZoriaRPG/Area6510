﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Da im GEOS128.Editor nicht genügend freier Speicher zur Verfügung steht,
;wird die Routine zum wechseln vom SD2IEC-Images in Bank#0 ausgelagert.
;******************************************************************************

			n "mod.GE_#102"
			t "G3_SymMacExtEdit"
			t "s.MP3.Edit.2.ext"

			c "GEOS.Edit128V3.1"
			a "Markus Kanet"
			o Base1SDTools

;*** Einsprungtabelle.
:xTestSD2IEC		jmp	TestSD2IEC
:xSlctDiskImg		jmp	SlctDiskImg

			t "-G3_TestSD2IEC"
			t "-G3_SlctDskImg"

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g BASE_EDITOR_DATA -1
;******************************************************************************
