﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "obj.Patch_SRAM"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Data"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Data"
endif

			o $d300

			h "MegaPatch-Kernal"
			h "SuperCPU-Funktionen..."

;******************************************************************************
;RAM_Type = RAM_SCPU
;******************************************************************************
.StashRAM_SCPU		jmp	SCPU_STASH_RAM
.FetchRAM_SCPU		jmp	SCPU_FETCH_RAM
.SwapRAM_SCPU		jmp	SCPU_SWAP_RAM
.VerifyRAM_SCPU		jmp	SCPU_VERIFY_RAM

;*** Speicherbank berechnen.
:DefBankAdr		lda	RamBankFirst +1		;Speicherbank berechnen.
			clc
			adc	r3L
			rts

			t "-R3_SRAM16Bit"
