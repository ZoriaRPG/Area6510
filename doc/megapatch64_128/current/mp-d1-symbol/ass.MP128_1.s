﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Systemlabels.
if .p
			t "src.GEOS_MP3.128"
			t "SymbTab_1"
			t "SymbTab_2"
			t "SymbTab128"
			t "MacTab"
			t "ass.Drives"
			t "ass.Macro"
endif

			n "ass.MP128_1"
			c "ass.SysFile V1.0"
			a "Markus Kanet"
			h "* AutoAssembler Systemdatei."
			h "Erstellt komprimierten MegaPatch-Kernal."
			f $04

			o $4000

:COMP_SYS		= TRUE_C128

			t "-A3_Kernal"
			b $ff

;--- NativeMode-Unterverzeichnisse einbinden.
			t "ass.NativeDir"
