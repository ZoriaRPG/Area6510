﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Einsprungtabelle RAM-Tools/GeoRAM.
:VerifyRAM_GRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:StashRAM_GRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:SwapRAM_GRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:FetchRAM_GRAM		ldy	#%10010001		;RAM-Bereich laden.

:JobRAM_GRAM		php				;IRQ sperren.
			sei

if Flag64_128 = TRUE_C64
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU
			pha
			lda	#$7e
			sta	MMU
endif

			lda	GRAM_BANK_SIZE		;Bank-Größe einlesen und
			jsr	DoRAMOp_GRAM		;Job ausführen.
			tay				;AKKU-Register speichern.

if Flag64_128 = TRUE_C64
			pla
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			pla
			sta	MMU
endif

			plp				;IRQ-Status zurücksetzen.

			tya				;Job-Ergebnis in AKKU übergeben.
			ldx	#NO_ERROR		;Flag für "Kein Fehler".
			rts
