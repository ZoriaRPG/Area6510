﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Einsprungtabelle RAM-Tools/CREU.
:VerifyRAM_CREU		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:StashRAM_CREU		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:SwapRAM_CREU		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:FetchRAM_CREU		ldy	#%10010001		;RAM-Bereich laden.

:JobRAM_CREU		php
			sei

if Flag64_128 = TRUE_C64
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU
			pha
			lda	#$7e
			sta	MMU
			lda	RAM_Conf_Reg
			pha
			lda	#$40			;keine CommonArea VIC =
			sta	RAM_Conf_Reg		;Bank1 für REU Transfer
			lda	CLKRATE			;aktuellen Takt
			pha				;zwischenspeichern.
			lda	#$00			;auf 1 Mhz schalten!
			sta	CLKRATE			;Sonst geht nichts!
endif

			jsr	DoRAMOp_CREU		;Job ausführen.
			tay				;Ergebniss zwischenspeichern.

if Flag64_128 = TRUE_C64
			pla
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			pla				;aktuellen Takt zurücksetzen
			sta	CLKRATE
			pla
			sta	RAM_Conf_Reg
			pla
			sta	MMU
endif

			plp

			tya				;Job-Ergebnis in AKKU übergeben.
			ldx	#NO_ERROR
			rts
