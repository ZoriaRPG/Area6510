﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DirNavEntry		= $4c8f
:DrvNmA			= $59de
:DrvNmB			= $59ef
:DrvNmC			= $5a00
:DrvNmD			= $5a11
:DrvNmVec		= $59d6
:DrvTypeSD		= $4c8b
:PartNmA		= $5a2a
:PartNmB		= $5a3b
:PartNmC		= $5a4c
:PartNmD		= $5a5d
:PartNmVec		= $5a22
