﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3e76
:AutoInitTaskMan	= $3de2
:BankCodeTab1		= $46a8
:BankCodeTab2		= $46ac
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $46b0
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $46bc
:BankType_Disk		= $46b8
:BankType_GEOS		= $46b4
:BankUsed		= $4668
:BankUsed_2GEOS		= $3bbd
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGCalcFix		= $292b
:BootGrfxFile		= $2909
:BootInptName		= $292c
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $291a
:BootQWERTZ		= $293d
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $293e
:BootVarEnd		= $293f
:BootVarStart		= $2880
:CheckDrvConfig		= $2d23
:CheckForSpeed		= $3598
:Class_GeoPaint		= $4606
:Class_ScrSaver		= $45f5
:ClearDiskName		= $35bc
:ClearDriveData		= $35d3
:ClrBank_Blocked	= $3c2b
:ClrBank_Spooler	= $3c28
:ClrBank_TaskMan	= $3c25
:CopyStrg_Device	= $3fec
:CountDrives		= $35a9
:CurDriveMode		= $4617
:DiskDataRAM_A		= $2180

:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $45b8
:DiskDriver_DISK	= $45e1
:DiskDriver_FName	= $45e4
:DiskDriver_INIT	= $45e0
:DiskDriver_SIZE	= $45e2
:DiskDriver_TYPE	= $45df
:DiskFileDrive		= $45de
:Dlg_DrawTitel		= $355f
:Dlg_IllegalCfg		= $4823
:Dlg_LdDskDrv		= $47b3
:Dlg_LdMenuErr		= $4893
:Dlg_NoDskFile		= $4731
:Dlg_SetNewDev		= $492a
:Dlg_Titel1		= $499e
:Dlg_Titel2		= $49ae
:DoInstallDskDev	= $36a2
:DriveInUseTab		= $462b
:DriveRAMLink		= $4616
:Err_IllegalConf	= $34d3
:ExitToDeskTop		= $2ce6
:FetchRAM_DkDrv		= $3079
:FindCurRLPart		= $4c81
:FindDiskDrvFile	= $30e2
:FindDkDvAllDrv		= $310b
:FindDriveType		= $39c0
:FindRTC_64Net		= $40d6
:FindRTC_SM		= $40c2
:FindRTCdrive		= $4085
:FindSystemFile		= $34fe
:Flag_ME1stBoot		= $4619
:GetDrvModVec		= $3360
:GetInpDrvFile		= $3fd2
:GetMaxFree		= $3c5c
:GetMaxSpool		= $3c62
:GetMaxTask		= $3c5f
:GetPrntDrvFile		= $3f4d
:InitDkDrv_Disk		= $31a8
:InitDkDrv_RAM		= $31a2
:InitGCalcFix		= $3f64
:InitQWERTZ		= $3f73
:InitScrSaver		= $3eb2
:InstallRL_Part		= $36ce
:IsDrvAdrFree		= $37d2
:IsDrvOnline		= $3acf
:LastSpeedMode		= $46db
:LdBootScrn		= $2e0b
:LdMenuError		= $354a
:LdScrnFrmDisk		= $2e37
:LoadDiskDrivers	= $316e
:LoadDskDrvData		= $3406
:LoadInptDevice		= $3fc4
:LoadPrntDevice		= $3f3f
:LoadSysRecord		= $352a
:LookForDkDvFile	= $3149
:NewDrive		= $4613
:NewDriveMode		= $4614
:NoInptName		= $46ec
:NoPrntName		= $46de
:PrepareExitDT		= $2cf7
:RL_Aktiv		= $46dc

:SCPU_Aktiv		= $46dd
:SetClockGEOS		= $4058
:SetSystemDevice	= $305e
:StashRAM_DkDrv		= $307c
:StdClrGrfx		= $2e29
:StdClrScrn		= $2e19
:StdDiskName		= $46f8
:SwapScrSaver		= $3f0f
:SysDrive		= $45ca
:SysDrvType		= $45cb
:SysFileName		= $45cd
:SysRealDrvType		= $45cc
:SysStackPointer	= $45c9
:SystemClass		= $45a7
:SystemDlgBox		= $3558
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $4615
:UpdateDiskDriver	= $31e7
:VLIR_BASE		= $4a30
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $22d0
:VLIR_Types		= $2380
:firstBootCopy		= $4618
