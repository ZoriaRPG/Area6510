﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $4019
:AutoInitTaskMan	= $3f18
:BankCodeTab1		= $483c
:BankCodeTab2		= $4840
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4844
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $4850
:BankType_Disk		= $484c
:BankType_GEOS		= $4848
:BankUsed		= $47fc
:BankUsed_2GEOS		= $3cb0
:Base0SDTools		= $3b00
:Base1SDTools		= $1c80
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootQWERTZ		= $294e
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294f
:BootVarEnd		= $2950
:BootVarStart		= $2880
:CheckDrvConfig		= $2d7d
:CheckForSpeed		= $3667
:Class_GeoPaint		= $479a
:Class_ScrSaver		= $4789
:ClearDiskName		= $368e
:ClearDriveData		= $36a5
:ClrBank_Blocked	= $3d1e
:ClrBank_Spooler	= $3d1b
:ClrBank_TaskMan	= $3d18
:CopyStrg_Device	= $4180
:CountDrives		= $367b
:CurDriveMode		= $47ab

:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $474c
:DiskDriver_DISK	= $4775
:DiskDriver_FName	= $4778
:DiskDriver_INIT	= $4774
:DiskDriver_SIZE	= $4776
:DiskDriver_TYPE	= $4773
:DiskFileDrive		= $4772
:Dlg_DrawTitel		= $362e
:Dlg_IllegalCfg		= $49bb
:Dlg_LdDskDrv		= $494b
:Dlg_LdMenuErr		= $4a2b
:Dlg_NoDskFile		= $48c8
:Dlg_SetNewDev		= $4ac3
:Dlg_Titel1		= $4b37
:Dlg_Titel2		= $4b47
:DoInstallDskDev	= $3774
:DriveInUseTab		= $47bf
:DriveRAMLink		= $47aa
:Err_IllegalConf	= $35a2
:ExitToDeskTop		= $2d40
:FetchRAM_DkDrv		= $3148
:FindCurRLPart		= $4e5e
:FindDiskDrvFile	= $31b1
:FindDkDvAllDrv		= $31da
:FindDriveType		= $3a92
:FindRTC_64Net		= $426a
:FindRTC_SM		= $4256
:FindRTCdrive		= $4219
:FindSystemFile		= $35cd
:Flag_ME1stBoot		= $47ad
:GetDrvModVec		= $342f
:GetInpDrvFile		= $4166
:GetMaxFree		= $3d4f
:GetMaxSpool		= $3d55
:GetMaxTask		= $3d52
:GetPrntDrvFile		= $40f0
:InitDkDrv_Disk		= $3277
:InitDkDrv_RAM		= $3271
:InitQWERTZ		= $4107
:InitScrSaver		= $4055
:InstallRL_Part		= $37a0
:IsDrvAdrFree		= $38a4
:IsDrvOnline		= $3ba1
:LastSpeedMode		= $4872
:LdBootScrn		= $2ed8
:LdMenuError		= $3619
:LdScrnFrmDisk		= $2f0b
:LoadDiskDrivers	= $323d
:LoadDskDrvData		= $34d5
:LoadInptDevice		= $4158
:LoadPrntDevice		= $40e2
:LoadSysRecord		= $35f9
:LookForDkDvFile	= $3218
:NewDrive		= $47a7
:NewDriveMode		= $47a8
:NoInptName		= $4883
:NoPrntName		= $4875
:PrepareExitDT		= $2d51
:RL_Aktiv		= $4873

:SCPU_Aktiv		= $4874
:SetClockGEOS		= $41ec
:SetSystemDevice	= $312d
:SizeSDTools		= $0500
:StashRAM_DkDrv		= $314b
:StdClrGrfx		= $2efd
:StdClrScrn		= $2ee6
:StdDiskName		= $488f
:SwapScrSaver		= $40b2
:SysDrive		= $475e
:SysDrvType		= $475f
:SysFileName		= $4761
:SysRealDrvType		= $4760
:SysStackPointer	= $475d
:SystemClass		= $473b
:SystemDlgBox		= $3627
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $47a9
:UpdateDiskDriver	= $32b6
:VLIR_BASE		= $4bc9
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2137
:VLIR_Types		= $2380
:firstBootCopy		= $47ac
