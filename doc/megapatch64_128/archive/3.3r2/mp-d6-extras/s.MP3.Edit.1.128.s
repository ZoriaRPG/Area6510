﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $4012
:AutoInitTaskMan	= $3f11
:BankCodeTab1		= $4809
:BankCodeTab2		= $480d
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4811
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $481d
:BankType_Disk		= $4819
:BankType_GEOS		= $4815
:BankUsed		= $47c9
:BankUsed_2GEOS		= $3ca9
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294e
:BootVarEnd		= $294f
:BootVarStart		= $2880
:CheckDrvConfig		= $2d76
:CheckForSpeed		= $3660
:Class_GeoPaint		= $4767
:Class_ScrSaver		= $4756
:ClearDiskName		= $3687
:ClearDriveData		= $369e
:ClrBank_Blocked	= $3d17
:ClrBank_Spooler	= $3d14
:ClrBank_TaskMan	= $3d11
:CopyStrg_Device	= $414d
:CountDrives		= $3674
:CurDriveMode		= $4778
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $4719

:DiskDriver_DISK	= $4742
:DiskDriver_FName	= $4745
:DiskDriver_INIT	= $4741
:DiskDriver_SIZE	= $4743
:DiskDriver_TYPE	= $4740
:DiskFileDrive		= $473f
:Dlg_DrawTitel		= $3627
:Dlg_IllegalCfg		= $4988
:Dlg_LdDskDrv		= $4918
:Dlg_LdMenuErr		= $49f8
:Dlg_NoDskFile		= $4895
:Dlg_SetNewDev		= $4a90
:Dlg_Titel1		= $4b04
:Dlg_Titel2		= $4b14
:DoInstallDskDev	= $376d
:DriveInUseTab		= $478c
:DriveRAMLink		= $4777
:Err_IllegalConf	= $359b
:ExitToDeskTop		= $2d39
:FetchRAM_DkDrv		= $3141
:FindCurRLPart		= $4de7
:FindDiskDrvFile	= $31aa
:FindDkDvAllDrv		= $31d3
:FindDriveType		= $3a8b
:FindRTC_64Net		= $4237
:FindRTC_SM		= $4223
:FindRTCdrive		= $41e6
:FindSystemFile		= $35c6
:Flag_ME1stBoot		= $477a
:GetDrvModVec		= $3428
:GetInpDrvFile		= $4133
:GetMaxFree		= $3d48
:GetMaxSpool		= $3d4e
:GetMaxTask		= $3d4b
:GetPrntDrvFile		= $40e9
:InitDkDrv_Disk		= $3270
:InitDkDrv_RAM		= $326a
:InitScrSaver		= $404e
:InstallRL_Part		= $3799
:IsDrvAdrFree		= $389d
:IsDrvOnline		= $3b9a
:LastSpeedMode		= $483f
:LdMenuError		= $3612
:LdScrnFrmDisk		= $2f04
:LoadBootScrn		= $2ed1
:LoadDiskDrivers	= $3236
:LoadDskDrvData		= $34ce
:LoadInptDevice		= $4125
:LoadPrntDevice		= $40db
:LoadSysRecord		= $35f2
:LookForDkDvFile	= $3211
:NewDrive		= $4774
:NewDriveMode		= $4775
:NoInptName		= $4850
:NoPrntName		= $4842
:PrepareExitDT		= $2d4a
:RL_Aktiv		= $4840
:SCPU_Aktiv		= $4841
:SetClockGEOS		= $41b9
:SetSystemDevice	= $3126
:StashRAM_DkDrv		= $3144

:StdClrGrfx		= $2ef6
:StdClrScreen		= $2edf
:StdDiskName		= $485c
:SwapScrSaver		= $40ab
:SysDrive		= $472b
:SysDrvType		= $472c
:SysFileName		= $472e
:SysRealDrvType		= $472d
:SysStackPointer	= $472a
:SystemClass		= $4708
:SystemDlgBox		= $3620
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $4776
:UpdateDiskDriver	= $32af
:VLIR_BASE		= $4b96
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $216a
:VLIR_Types		= $2380
:firstBootCopy		= $4779
