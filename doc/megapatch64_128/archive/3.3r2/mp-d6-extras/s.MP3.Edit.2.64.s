﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DirNavEntry		= $4e7c
:DrvNmA			= $5b67
:DrvNmB			= $5b78
:DrvNmC			= $5b89
:DrvNmD			= $5b9a
:DrvNmVec		= $5b5f
:DrvTypeSD		= $4e78
:PartNmA		= $5bb3
:PartNmB		= $5bc4
:PartNmC		= $5bd5
:PartNmD		= $5be6
:PartNmVec		= $5bab
