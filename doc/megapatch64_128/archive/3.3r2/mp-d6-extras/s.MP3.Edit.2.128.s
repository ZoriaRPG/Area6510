﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DirNavEntry		= $4c6f
:DrvNmA			= $599c
:DrvNmB			= $59ad
:DrvNmC			= $59be
:DrvNmD			= $59cf
:DrvNmVec		= $5994
:DrvTypeSD		= $4c6b
:PartNmA		= $59e8
:PartNmB		= $59f9
:PartNmC		= $5a0a
:PartNmD		= $5a1b
:PartNmVec		= $59e0
:SlctPartBase		= $1c80
:SlctPartSize		= $0500
