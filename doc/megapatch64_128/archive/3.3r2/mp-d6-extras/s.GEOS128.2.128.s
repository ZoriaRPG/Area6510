﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $4c02
:ReBoot_REU		= $484d
:ReBoot_RL		= $449b
:ReBoot_SCPU		= $4000
:x_ClrDlgScreen		= $6c84
:x_DoAlarm		= $52f6
:x_EnterDeskTop		= $50fc
:x_GetFiles		= $536c
:x_GetFilesData		= $678a
:x_GetFilesIcon		= $6909
:x_GetNextDay		= $52a8
:x_PanicBox		= $51fc
:x_ToBASIC		= $5169
