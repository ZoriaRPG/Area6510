﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3e6e
:AutoInitTaskMan	= $3dda
:BankCodeTab1		= $4665
:BankCodeTab2		= $4669
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $466d
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $4679
:BankType_Disk		= $4675
:BankType_GEOS		= $4671
:BankUsed		= $4625
:BankUsed_2GEOS		= $3bb5
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $2909
:BootInptName		= $292b
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $291a
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $293c
:BootVarEnd		= $293d
:BootVarStart		= $2880
:CheckDrvConfig		= $2d1b
:CheckForSpeed		= $3590
:Class_GeoPaint		= $45c3
:Class_ScrSaver		= $45b2
:ClearDiskName		= $35b4
:ClearDriveData		= $35cb
:ClrBank_Blocked	= $3c23
:ClrBank_Spooler	= $3c20
:ClrBank_TaskMan	= $3c1d
:CopyStrg_Device	= $3fa9
:CountDrives		= $35a1
:CurDriveMode		= $45d4
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $4575

:DiskDriver_DISK	= $459e
:DiskDriver_FName	= $45a1
:DiskDriver_INIT	= $459d
:DiskDriver_SIZE	= $459f
:DiskDriver_TYPE	= $459c
:DiskFileDrive		= $459b
:Dlg_DrawTitel		= $3557
:Dlg_IllegalCfg		= $47e0
:Dlg_LdDskDrv		= $4770
:Dlg_LdMenuErr		= $4850
:Dlg_NoDskFile		= $46ee
:Dlg_SetNewDev		= $48e7
:Dlg_Titel1		= $495b
:Dlg_Titel2		= $496b
:DoInstallDskDev	= $369a
:DriveInUseTab		= $45e8
:DriveRAMLink		= $45d3
:Err_IllegalConf	= $34cb
:ExitToDeskTop		= $2cde
:FetchRAM_DkDrv		= $3071
:FindCurRLPart		= $4c3e
:FindDiskDrvFile	= $30da
:FindDkDvAllDrv		= $3103
:FindDriveType		= $39b8
:FindRTC_64Net		= $4093
:FindRTC_SM		= $407f
:FindRTCdrive		= $4042
:FindSystemFile		= $34f6
:Flag_ME1stBoot		= $45d6
:GetDrvModVec		= $3358
:GetInpDrvFile		= $3f8f
:GetMaxFree		= $3c54
:GetMaxSpool		= $3c5a
:GetMaxTask		= $3c57
:GetPrntDrvFile		= $3f45
:InitDkDrv_Disk		= $31a0
:InitDkDrv_RAM		= $319a
:InitScrSaver		= $3eaa
:InstallRL_Part		= $36c6
:IsDrvAdrFree		= $37ca
:IsDrvOnline		= $3ac7
:LastSpeedMode		= $4698
:LdMenuError		= $3542
:LdScrnFrmDisk		= $2e2f
:LoadBootScrn		= $2e03
:LoadDiskDrivers	= $3166
:LoadDskDrvData		= $33fe
:LoadInptDevice		= $3f81
:LoadPrntDevice		= $3f37
:LoadSysRecord		= $3522
:LookForDkDvFile	= $3141
:NewDrive		= $45d0
:NewDriveMode		= $45d1
:NoInptName		= $46a9
:NoPrntName		= $469b
:PrepareExitDT		= $2cef
:RL_Aktiv		= $4699
:SCPU_Aktiv		= $469a
:SetClockGEOS		= $4015
:SetSystemDevice	= $3056
:StashRAM_DkDrv		= $3074

:StdClrGrfx		= $2e21
:StdClrScreen		= $2e11
:StdDiskName		= $46b5
:SwapScrSaver		= $3f07
:SysDrive		= $4587
:SysDrvType		= $4588
:SysFileName		= $458a
:SysRealDrvType		= $4589
:SysStackPointer	= $4586
:SystemClass		= $4564
:SystemDlgBox		= $3550
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $45d2
:UpdateDiskDriver	= $31df
:VLIR_BASE		= $49ed
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2313
:VLIR_Types		= $2380
:firstBootCopy		= $45d5
