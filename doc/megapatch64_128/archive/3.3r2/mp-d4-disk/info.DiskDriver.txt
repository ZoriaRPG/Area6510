﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Auswahl des IECBus-NM oder SD2IEC-NM-Treibers:
;--- Ergänzung: 17.10.18/M.Kanet
;IECBNM -> Kompatibel mit CMD-FD für Test unter VICE.
;SD2IEC -> Erfordert SD2IEC da Firmware-spezifische Aufrufe genutzt werden.
;
;Es kann aber nur ein Treiber in GEOS.Disk eingebunden werden da beide
;Treiber die gleiche ID verwenden. Standard ist der SD2IEC-Treiber.
;Um den aktiven Treiber zu wechseln ist die Datei `lnk.G3.Disk` und
;`s.Info.DTypes` anzupassen.

