﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Name für RAM-Laufwerk definieren.
;    Übergabe: A/X/Y enthalten die drei Buchstaben für den Standard-
;              Laufwerksnamen (RAM, GEOS, REU, SRC).
;              Zusammen mit dem Laufwerks-Buchstaben erhält damit jedes
;              RAM-Laufwerk einen individuellen Namen. Notwendig für
;              ältere DeskTop-varianten die Laufwerke über den Disknamen
;              erkennen (z.B. DESKTOP v2.0).
:SetRDrvName		sta	BAM_NM +4		;Laufwerkskennung speichern.
			stx	BAM_NM +5
			sty	BAM_NM +6
			sta	RDrvNMDskName +0
			stx	RDrvNMDskName +1
			sty	RDrvNMDskName +2

			lda	DriveAdr		;Laufwerksbuchstabe speichern.
			clc
			adc	#$39
			sta	BAM_NM +7
			sta	RDrvNMDskName +3
			rts

;*** Ist BAM und Partitionsgröße gültig ?
:TestCurBAM		jsr	CheckDiskBAM
			txa				;Gültige BAM verfügbar?
			bne	:51			; => Nein, Ende...

			jsr	GetSekPartSize		;Größe des Laufwerks einlesen.
			txa				;Diskettenfehler?
			bne	:51			; => Ja, Abbruch...

			cpy	SetSizeRRAM		;Partitionsgröße unverändert ?
			bne	:51			; => Nein, Ende...

			ldx	#NO_ERROR
			rts
::51			ldx	#BAD_BAM
			rts

;*** Ist ein gültige BAM vorhanden ?
:CheckDiskBAM		jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:52			; => Ja, Laufwerk initialisieren.

;--- Ergänzung: 16.12.18/M.Kanet
;Da standardmäßig keine GEOS-Disketten mehr erzeugt werden kann der
;GEOS-Format-String nicht als Referenz genutzt werden.
;Byte#2=$48 und Byte#3=$00 verwenden.
			lda	curDirHead +2
			cmp	#$48
			bne	:52
			ldy	curDirHead +3
			bne	:52
::50			lda	curDirHead+64,y
			bne	:52
			iny
			cpy	#$10
			bcc	:50

if FALSE
			LoadW	r0,curDirHead +$ad
			LoadW	r1,DrvFormatCode
			ldx	#r0L
			ldy	#r1L			;Auf GEOS-Kennung
			lda	#12			;"GEOS-format" testen.
			jsr	CmpFString		;Kennung vorhanden ?
			bne	:52			; => Nein, Directory löschen.
endif

::51			ldx	#NO_ERROR
			rts
::52			ldx	#BAD_BAM
			rts

;*** BAM erstellen.
:InitRDrvNM		jsr	CreateBAM		;BAM erstellen.
			txa				;Installationsfehler ?
			bne	:2			; => Ja, Abbruch...

			lda	DriveAdr		;Laufwerk aktivieren.
			jsr	SetDevice
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskfehler?
			bne	:2			; => Ja, Abbruch...

			ldx	#r0L			;Zeiger auf aktuellen Disknamen.
			jsr	GetPtrCurDkNm

			ldy	#$00
			lda	(r0L),y			;Diskettenname gültig ?
			bne	:3			; => Ja, weiter...

			ldy	#$00			;BAM Teil #1 definieren.
::1			lda	BAM_NM    ,y
			sta	curDirHead,y
			iny
			cpy	#$c0
			bcc	:1

			jsr	PutDirHead		;Aktuellen BAM-Sektor speichern.
			txa				;Vorgang erfolgreich?
			beq	:3			;Ja, weiter...

;--- Fehler beim erstellen der BAM, Laufwerk nicht installiert.
::2			lda	MinFreeRRAM		;Laufwerksdaten löschen.
			jsr	DeInstallDrvData
			ldx	#BAD_BAM
			rts

;--- Laufwerk installiert, Ende...
::3			ldx	#NO_ERROR
			rts

;*** Partitionsgröße einstellen.
:GetPartSize		lda	MaxSizeRRAM		;Speicher verfügbar?
			bne	:52			; => Ja, weiter...
::51			ldx	#DEV_NOT_FOUND		; => Nein, Abbruch.
			rts

::52			ldx	SetSizeRRAM		;Größe bereits festgelegt ?
			bne	:54			; => Ja, weiter.
::53			sta	SetSizeRRAM		;Max. Größe vorbelegen.

::54			cmp	SetSizeRRAM		;Max. Größe überschritten ?
			bcs	:54a			; => Nein, weiter...
			sta	SetSizeRRAM		; => Ja, Größe zurücksetzen...

::54a			bit	firstBoot		;GEOS-BootUp ?
			bmi	:55			; => Nein, weiter...

			lda	SetSizeRRAM		;Startadresse RAM-Speicher in
			cmp	MaxSizeRRAM		;Größe bereits festgelegt ?
			bcc	:57			; => Ja, weiter...
			beq	:57

;--- Partitionsgröße einstellen.
::55			jsr	DoDlg_RDrvNMSize	;Partitionsgröße wählen.
			txa				;Auswahl abgebrochen ?
			bne	:51			; => Ja, Ende...

::56			jsr	UpdateDskDrvData	;INIT-Routine im Laufwerkstreiber
							;aktualisieren.
::57			ldx	#NO_ERROR		;Kein Fehler, Ende.
			rts

;*** Laufwerksinformationen speichern.
:UpdateDskDrvData	ldx	DriveAdr		;Für RAMNative/GEOS-DACC die neue
			lda	SetSizeRRAM		;Größe als Vorgabe speichern.
			sta	DskSizeA -8,x

			lda	DriveMode		;INIT-Routine im Laufwerkstreiber
			ldx	#< DSK_INIT_SIZE	;aktualisieren. Damit werden die
			stx	r2L			;Vorgabe-Werte in die Systemdatei
			ldx	#> DSK_INIT_SIZE	;geschrieben und können beim
			stx	r2H			;beim Systemstart abgerufen werden.
			jsr	SaveDskDrvData

			lda	DriveAdr		;Laufwerk zurücksetzen.
			jmp	SetDevice

;*** Größe der aktiven Partition einlesen.
:GetCurPartSize		bit	firstBoot		;GEOS-BootUp ?
			bmi	:51			; => Nein, weiter...

			lda	SetSizeRRAM		;Partitionsgröße definiert ?
			bne	:51			; => Ja, Ende...

			jsr	CheckDiskBAM		;BAM überprüfen.
			txa				;Ist BAM gespeichert?
			bne	:51			; => Nein, Ende...

			jsr	GetSekPartSize		;Größe des installierten
			txa				;Laufwerks ermitteln.
			bne	:51

			sty	SetSizeRRAM 		;Partitionsgröße speichern.
::51			rts

;*** Sektor mit Laufwerksgröße einlesen.
:GetSekPartSize		ldx	#$01			;Sektor 01/02 mit BAM einlesen.
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf
			txa				;Diskettenfehler?
			bne	:51			;Ja, Abbruch...

			ldy	diskBlkBuf +8		;Laufwerksgröße einlesen.
::51			rts

;*** Neue BAM erstellen.
:ClearCurBAM		lda	SetSizeRRAM		;Partitionsgröße festlegen.
			sta	BAM_NMa   +8

			ldy	#$00			;BAM Teil #1 definieren.
::51			lda	BAM_NM    ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$c0
			bcc	:51

			lda	#$00
::52			sta	diskBlkBuf,y
			iny
			bne	:52

			lda	#$01			;BAM Teil #1 speichern.
			sta	r1L
			sta	r1H
			jsr	WriteSektor
			txa				;Diskettenfehler?
			bne	:58			;Ja, Abbruch...

			ldy	#$00			;BAM Teil #1 definieren.
::53			lda	BAM_NMa   ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$40
			bcc	:53

			lda	#$ff
::54			sta	diskBlkBuf,y
			iny
			bne	:54

			inc	r1H			;BAM Teil #2 speichern.
			jsr	WriteSektor
			txa				;Diskettenfehler?
			bne	:58			;Ja, Abbruch...

			ldy	#$00			;BAM Teil #3 definieren.
			lda	#$ff
::55			sta	diskBlkBuf,y
			iny
			bne	:55

::56			inc	r1H			;Zeiger auf nächstenn Sektor.
			CmpBI	r1H,$22			;BAM erstellt?
			bcs	:57			;Ja, -> Ende.

			jsr	WriteSektor
			txa				;Diskettenfehler?
			bne	:58			;Ja, Abbruch...
			beq	:56

;--- Ersten Verzeichnissektor löschen.
::57			jsr	OpenDisk		;BAM einlesen und Größe der Native-
			txa				;Partition innerhalb des Treibers
			bne	:58			;festlegen.

			jsr	ClrDiskSekBuf		;Sektorspeicher löschen.

			lda	#$ff			;Ersten Verzeichnissektor löschen.
			sta	diskBlkBuf +$01
			lda	#$01
			sta	r1L
			lda	#$22
			sta	r1H
			jsr	WriteSektor
			txa				;Diskettenfehler?
			bne	:58			;Ja, Abbruch...

			lda	#$ff			;Sektor $01/$ff löschen.
			sta	r1H			;Ist Borderblock für DeskTop 2.0!
			jsr	WriteSektor
::58			rts

;*** Sektorspeicher löschen.
:ClrDiskSekBuf		ldy	#$00
			tya
::1			sta	diskBlkBuf,y
			dey
			bne	:1
			rts

;*** BAM für RAMNative-Laufwerk.
:BAM_NM			b $01,$22,$48,$00,"R","A","M","A"
			b "N","a","t","i","v","e",$a0,$a0
			b $a0,$a0,$a0,$a0,$a0,$a0,"R","D"
			b $a0,"1","H",$a0,$a0,$00,$00,$00
			b $01,$01,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
:RDrvNMDskName		b "R","A","M","A","N","a","t","i"
			b "v","e",$a0,$a0,$a0,$a0,$a0,$a0
			b $a0,$a0,"R","D",$a0,"1","H",$a0
			b $a0,$a0,$a0

;--- Ergänzung: 16.12.18/M.Kanet
;Standardmäßig keine GEOS-Diskette erzeugen.
:RDrvNMBorderTS		b $00,$00
			b $00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00
			b $00,$00,$00
if FALSE
:RDrvNMBorderTS		b $01,$ff
			b "G","E","O","S"," "
			b "f","o","r","m","a","t"," "
			b "V","1",".","0"
			b $00,$00,$00
endif

:BAM_NMa		b $00,$00,$48,$b7,$52,$44,$c0,$00
			b $02,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$1f,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff
;--- Ergänzung: 16.12.18/M.Kanet
;Standardmäßig keine GEOS-Diskette erzeugen.
;Bit#0 steht für Track $01/Sektor $ff
:RDrvNMBorderBAM	b %11111111
if FALSE
:RDrvNMBorderBAM	b %11111110
endif

;*** Kennung für eine gültige GEOS-Diskette.
:DrvFormatCode		b "GEOS format "
