﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#124"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

			t "-DD_JumpTab"

;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		ldy	#$0d			;13x64K für RAM1581.
			jmp	GetFreeBankTab

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			lda	DriveAdr
			clc
			adc	#$39
			sta	BAM_81 +3

			jsr	TestDriveMode		;Freien RAM-Speicher testen.
			cpx	#$00			;Ist genügend Speicher frei ?
			beq	:52			; => Ja, weiter...
			bne	:51a

;--- Laufwerk kann nicht installliert werden.
::51			ldx	#DEV_NOT_FOUND
::51a			rts

;--- RAM reservieren.
::52			pha				;RAM-Speicher in REU belegen.
			ldy	#$0d
			jsr	AllocateBankTab
			pla
			cpx	#$00			;Speicher reserviert ?
			bne	:51a			; => Nein, Installationsfehler.

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			sta	ramBase   -8,x		;REU zwischenspeichern.

;--- Laufwerk installieren.
			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			sta	driveType   -8,x
			sta	curType
			sta	RealDrvType -8,x
			lda	#SET_MODE_FASTDISK
			sta	RealDrvMode-8,x

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- BAM erstellen.
			jsr	CreateBAM		;BAM erstellen.
			stx	:54 +1			;Fehlercode speichern.

			lda	DriveAdr		;Laufwerk aktivieren.
			jsr	SetDevice
			jsr	OpenDisk
			ldx	#r0L
			jsr	GetPtrCurDkNm

			ldy	#$00
			lda	(r0L),y			;Diskettenname gültig ?
			bne	:54			; => Ja, weiter...

			lda	curDirHead +$ab		;Zeiger auf Borderblock
			beq	:53			;retten.
			sta	BAM_81     +$1b
			lda	curDirHead +$ac
			sta	BAM_81     +$1c

::53			jsr	DefDskNmData		;Diskettenname festlegen.

			jsr	PutDirHead

::54			ldx	#$ff
			rts

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	lda	driveType   -8,x	;RAM-Laufwerk installiert ?
			bpl	:51			; => Nein, weiter...

			txa				;RAM-Speicher in der REU wieder
			pha				;freigeben.
			lda	ramBase     -8,x
			ldy	#$0d
			jsr	FreeBankTab
			pla
			tax

::51			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** Sektorspeicher löschen.
:ClrDiskSekBuf		ldy	#$00
			tya
::51			sta	diskBlkBuf,y
			dey
			bne	:51
			rts

;*** RAM-Laufwerk bereits installiert ?
:TestCurBAM		jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Laufwerk initialisieren.

			LoadW	r0,curDirHead +$ad
			LoadW	r1,BAM_81     +$1d
			ldx	#r0L
			ldy	#r1L			;Auf GEOS-Kennung
			lda	#12			;"GEOS-format" testen.
			jsr	CmpFString		;Kennung vorhanden ?
			bne	:51			; => Ja, Directory nicht löschen.

			ldx	#$00
			rts

::51			ldx	#BAD_BAM
			rts

;*** Neue BAM erstellen.
:ClearCurBAM		ldy	#$00			;BAM Teil #1 definieren.
			tya
::51			sta	curDirHead,y
			iny
			bne	:51

			lda	#$28			;Zeiger auf ersten Verzeichnis-
			sta	curDirHead +$00		;Sektor richten.
			ldx	#$03
			stx	curDirHead +$01
			ldx	#$44
			stx	curDirHead +$02

			sta	r1L
			sty	r1H

			jsr	DefDskNmData

			LoadW	r4,curDirHead
			jsr	PutBlock
			txa
			bne	:55

::54			lda	BAM_81a  ,x
			sta	dir2Head ,x
			inx
			bne	:54

			inc	r1H
			LoadW	r4,dir2Head
			jsr	PutBlock
			txa
			bne	:55

			lda	#$ff			;BAM Teil #3 definieren.
			sta	dir2Head +$01
			stx	dir2Head +$00
			ldy	#$28
			sty	dir2Head +$fa
			sta	dir2Head +$fb
			sta	dir2Head +$fd
			inc	r1H
			jsr	PutBlock
			txa
			bne	:55

			jsr	GetDirHead		;BAM einlesen.
			txa
			bne	:55

			jsr	ClrDiskSekBuf		;Sektorspeicher löschen.

			lda	#$ff			;Ersten Verzeichnissektor löschen.
			sta	diskBlkBuf +$01
			LoadB	r1L,$28
			LoadB	r1H,$03
			LoadW	r4,diskBlkBuf
			jsr	PutBlock
			txa
			bne	:55

			lda	#$27			;Sektor $28/$27 löschen.
			sta	r1H			;Ist Borderblock für DeskTop 2.0!
			jsr	PutBlock
::55			rts

;*** Diskettenname definieren.
:DefDskNmData		ldy	#$18
::51			lda	BAM_81,y
			sta	curDirHead +$04,y
			dey
			bpl	:51

			ldy	#$2d
::52			lda	BAM_81,y
			sta	curDirHead +$90,y
			dey
			bpl	:52
			rts

;*** BAM für RAM81-Laufwerk.
:BAM_81			b $52,$41,$4d,$20,$31,$35,$38,$31
			b $a0,$a0,$a0,$a0,$a0,$a0,$a0,$a0
			b $a0,$a0,$52,$44,$a0,$33,$44,$a0
			b $a0,$00,$00,$28,$27,$47,$45,$4f;$28/$27 Borderblock.
			b $53,$20,$66,$6f,$72,$6d,$61,$74
			b $20,$56,$31,$2e,$30,$00

:BAM_81a		b $28,$02,$44,$bb,$45,$41,$c0,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$28,$ff,$ff,$ff,$ff,$ff
			b $28,$ff,$ff,$ff,$ff,$ff,$28,$ff
			b $ff,$ff,$ff,$ff,$28,$ff,$ff,$ff
			b $ff,$ff,$23,$f0,$ff,$ff,$ff,$7f

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000

;*** Dialogbox-Texte.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation RAM1581",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation RAM1581",NULL
endif

;******************************************************************************
			t "-DD_AskClrBAM"
;******************************************************************************

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
