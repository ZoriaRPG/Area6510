﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Auf SD2IEC testen.
;Dazu den Befehl "M-R",$00,$03,$03 senden.
;Die Rückmeldung "00,(OK,00,00)" deutet auf ein SD2IEC hin.
:TestSD2IEC		tya				;1541: Y-Reg sichern.
			pha

			jsr	PurgeTurbo		;TurboDOS entfernen.
			jsr	InitForIO		;I/O-Bereich aktivieren.

			ldx	DriveAdr		;Laufwerksadresse einlesen und
			lda	#15			;testen ob Laufwerk aktiv.
			tay
			jsr	SETLFS
			jsr	OPENCHN			;Befehlskanal öffnen.
			lda	#$0f
			jsr	CLOSE			;Befehlskanal schließen.
			lda	STATUS			;STATUS = OK ?
			bne	:101			; => Nein, Abbruch...

			ldx	DriveAdr
			lda	#15
			tay
			jsr	SETLFS
			jsr	OPENCHN			;Befehlskanal öffnen.

			lda	#<V151e3		;"M-R"-Befehl senden.
			ldx	#>V151e3
			ldy	#$06
			jsr	SendFCom

			lda	#<V151e4		;Ergebnis einlesen.
			ldx	#>V151e4
			ldy	#$03
			jsr	GetFData

			lda	#15			;Befehlskanal schließen.
			jsr	CLOSE

			ldx	#$ff			;Vorgabe: SD2IEC.
			lda	V151e4 +0		;Rückmeldung auswerten.
			cmp	#"0"			;"00," ?
			bne	:101			; => Nein, Ende...
			lda	V151e4 +1
			cmp	#"0"
			bne	:101
			lda	V151e4 +2
			cmp	#","
			beq	:102
::101			ldx	#$00			;Kein SD2IEC.
::102			jsr	DoneWithIO		;I/O-Bereich deaktivieren.
			pla
			tay
			rts

:V151e3			b "M-R",$00,$03,$03
:V151e4			s $03

;*** Floppy-Befehl senden.
:SendFCom		sta	r0L
			stx	r0H
			sty	r1L

			ldx	#$0f
			jsr	CKOUT

			lda	#$00
			sta	:101 +1
::101			ldy	#$ff
			cpy	r1L
			beq	:102
			lda	(r0L),y
			jsr	BSOUT
			inc	:101 +1
			jmp	:101

::102			jmp	CLRCHN

;*** Rückmeldung von Floppy empfangen.
:GetFData		sta	r0L
			stx	r0H
			sty	r1L

			ClrB	STATUS

			ldx	#$0f
			jsr	CHKIN

			lda	#$00
			sta	:101 +4
::101			jsr	GETIN
			ldy	#$ff
			cpy	r1L
			beq	:102
			sta	(r0L),y
			inc	:101 +4
			jmp	:101

::102			jmp	CLRCHN

;*** Laufwerks-ROM für SD2IEC laden.
:LoadDriveROM		sta	FComLoadROM +8		;DOS-Kennung speichern.
			stx	FComLoadROM +9

			jsr	PurgeTurbo		;TurboDOS entfernen.
			jsr	InitForIO		;I/O-Bereich aktivieren.

			ldx	DriveAdr
			lda	#15
			tay
			jsr	SETLFS
			jsr	OPENCHN			;Befehlskanal öffnen.

			lda	#<FComLoadROM		;"XR:"-Befehl senden.
			ldx	#>FComLoadROM
			ldy	#$0e
			jsr	SendFCom

			lda	#15
			jsr	CLOSE			;Befehlskanal schließen.
			jmp	DoneWithIO		;I/O-Bereich abschalten.

:FComLoadROM		b "XR:DOS15??.BIN"
