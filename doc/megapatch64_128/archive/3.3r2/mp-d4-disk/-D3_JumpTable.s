﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	jmp	xTurboRoutine_r1
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vReadPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vOpenPartition		ldx	#ILLEGAL_DEVICE
			rts
:vSwapPartition		ldx	#ILLEGAL_DEVICE
			rts
:vGetPTypeData		ldx	#ILLEGAL_DEVICE
			rts
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-Performance-DiskDriver V3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp1 = C_71!C_81
if :tmp1 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	jmp	xTurboRoutine_r1
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vReadPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vOpenPartition		ldx	#ILLEGAL_DEVICE
			rts
:vSwapPartition		ldx	#ILLEGAL_DEVICE
			rts
:vGetPTypeData		ldx	#ILLEGAL_DEVICE
			rts
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp2 = RD_41!RD_71!RD_81
if :tmp2 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#$00			;1541: TurboRoutine ausführen.
			rts
:vGetDiskError		ldx	#$00			;1541: TurboDOS-fehler einlesen.
			rts
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vReadPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vOpenPartition		ldx	#ILLEGAL_DEVICE
			rts
:vSwapPartition		ldx	#ILLEGAL_DEVICE
			rts
:vGetPTypeData		ldx	#ILLEGAL_DEVICE
			rts
:vSendCommand		ldx	#ILLEGAL_DEVICE
			rts

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp3 = FD_41!FD_71!FD_81!HD_41!HD_71!HD_81
if :tmp3 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	jmp	xTurboRoutine_r1
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp4 = FD_NM!HD_NM
if :tmp4 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	jmp	xTurboRoutine_r1
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		jmp	xOpenRootDir
:vOpenSubDir		jmp	xOpenSubDir
:vGetBAMBlock		jmp	xGetBAMBlock
:vPutBAMBlock		jmp	xPutBAMBlock

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp5 = RL_NM
if :tmp5 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#$00			;1541: TurboRoutine ausführen.
			rts
:vGetDiskError		ldx	#$00			;1541: TurboDOS-fehler einlesen.
			rts
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		jmp	xOpenRootDir
:vOpenSubDir		jmp	xOpenSubDir
:vGetBAMBlock		jmp	xGetBAMBlock
:vPutBAMBlock		jmp	xPutBAMBlock

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp6 = RD_NM!RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
if :tmp6 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#$00			;1541: TurboRoutine ausführen.
			rts
:vGetDiskError		ldx	#$00			;1541: TurboDOS-fehler einlesen.
			rts
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		jmp	xOpenRootDir
:vOpenSubDir		jmp	xOpenSubDir
:vGetBAMBlock		jmp	xGetBAMBlock
:vPutBAMBlock		jmp	xPutBAMBlock

;*** Erweiterte Funktionen.
:vGetPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vReadPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vOpenPartition		ldx	#ILLEGAL_DEVICE
			rts
:vSwapPartition		ldx	#ILLEGAL_DEVICE
			rts
:vGetPTypeData		ldx	#ILLEGAL_DEVICE
			rts
:vSendCommand		ldx	#ILLEGAL_DEVICE
			rts

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp7 = RL_41!RL_71!RL_81
if :tmp7 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#$00			;1541: TurboRoutine ausführen.
			rts
:vGetDiskError		ldx	#$00			;1541: TurboDOS-fehler einlesen.
			rts
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp8 = HD_41_PP!HD_71_PP!HD_81_PP
if :tmp8 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#NO_ERROR
			rts
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		ldx	#ILLEGAL_DEVICE
			rts
:vOpenSubDir		ldx	#ILLEGAL_DEVICE
			rts
:vGetBAMBlock		ldx	#ILLEGAL_DEVICE
			rts
:vPutBAMBlock		ldx	#ILLEGAL_DEVICE
			rts

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp9 = HD_NM_PP
if :tmp9 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	ldx	#NO_ERROR
			rts
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		jmp	xOpenRootDir
:vOpenSubDir		jmp	xOpenSubDir
:vGetBAMBlock		jmp	xGetBAMBlock
:vPutBAMBlock		jmp	xPutBAMBlock

;*** Erweiterte Funktionen.
:vGetPDirEntry		jmp	xGetPDirEntry
:vReadPDirEntry		jmp	xReadPDirEntry
:vOpenPartition		jmp	xOpenPartition
:vSwapPartition		jmp	xSwapPartition
:vGetPTypeData		jmp	xGetPTypeData
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif

;******************************************************************************
::tmp10 = IEC_NM!S2I_NM
if :tmp10 = TRUE
;******************************************************************************
;*** Sprungtabelle.
:vInitForIO		w xInitForIO
:vDoneWithIO		w xDoneWithIO
:vExitTurbo		w xExitTurbo
:vPurgeTurbo		w xPurgeTurbo
:vEnterTurbo		w xEnterTurbo
:vChangeDiskDev		w xChangeDiskDev
:vNewDisk		w xNewDisk
:vReadBlock		w xReadBlock
:vWriteBlock		w xWriteBlock
:vVerWriteBlock		w xVerWriteBlock
:vOpenDisk		w xOpenDisk
:vGetBlock		w xGetBlock
:vPutBlock		w xPutBlock
:vGetDirHead		w xGetDirHead
:vPutDirHead		w xPutDirHead
:vGetFreeDirBlk		w xGetFreeDirBlk
:vCalcBlksFree		w xCalcBlksFree
:vFreeBlock		w xFreeBlock
:vSetNextFree		w xSetNextFree
:vFindBAMBit		w xFindBAMBit
:vNxtBlkAlloc		w xNxtBlkAlloc
:vBlkAlloc		w xBlkAlloc
:vChkDkGEOS		w xChkDkGEOS
:vSetGEOSDisk		w xSetGEOSDisk

:vGet1stDirEntry	jmp	xGet1stDirEntry
:vGetNxtDirEntry	jmp	xGetNxtDirEntry
:vGetBorderBlock	jmp	xGetBorderBlock
:vCreateNewDirBlk	jmp	xCreateNewDirBlk
:vGetBlock_dskBuf	jmp	xGetBlock_dskBuf
:vPutBlock_dskBuf	jmp	xPutBlock_dskBuf
:vTurboRoutine_r1	jmp	xTurboRoutine_r1
:vGetDiskError		jmp	xGetDiskError
:vAllocateBlock		jmp	xAllocateBlock
:vReadLink		jmp	xReadLink

;*** Kennbyte für Laufwerkstreiber.
:xDiskDrvType		b DiskDrvMode
:xDiskDrvVersion	b DriverVersion

;*** Einsprungtabelle für NativeMode-Funktionen.
:vOpenRootDir		jmp	xOpenRootDir
:vOpenSubDir		jmp	xOpenSubDir
:vGetBAMBlock		jmp	xGetBAMBlock
:vPutBAMBlock		jmp	xPutBAMBlock

;*** Erweiterte Funktionen.
:vGetPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vReadPDirEntry		ldx	#ILLEGAL_DEVICE
			rts
:vOpenPartition		ldx	#ILLEGAL_DEVICE
			rts
:vSwapPartition		ldx	#ILLEGAL_DEVICE
			rts
:vGetPTypeData		ldx	#ILLEGAL_DEVICE
			rts
:vSendCommand		jmp	xSendCommand

;*** Kennung für erweiterte Laufwerkstreiber.
:vDiskDrvTypeCode	b "MPDD3",NULL			;High-End-DiskDriver Version 3.

;******************************************************************************
:S_DRIVER_DATA
;******************************************************************************
endif
