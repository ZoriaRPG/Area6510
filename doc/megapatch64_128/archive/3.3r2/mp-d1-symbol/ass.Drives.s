﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Laufwerke.
;Laufwerk für Symboltabellen, Macros
;und AutoAssembler Dateien.
;RAMLink
;:DvPart_Symbol = 6
;:DvAdr_Symbol  = 11
;3x1581
;:DvPart_Symbol = 0
;:DvAdr_Symbol  = 9
;2xRAMNative
:DvPart_Symbol = 0
:DvAdr_Symbol  = 9

;Laufwerk für Quelltexte
;RAMLink
;:DvPart_Kernal = 7
;:DvAdr_Kernal  = 10
;:DvPart_System = 8
;:DvAdr_System  = 10
;:DvPart_Disk   = 9
;:DvAdr_Disk    = 10
;:DvPart_Prog   = 10
;:DvAdr_Prog    = 10
;3x1581
;:DvPart_Kernal = 0
;:DvAdr_Kernal  = 0
;:DvPart_System = 0
;:DvAdr_System  = 0
;:DvPart_Disk   = 0
;:DvAdr_Disk    = 0
;:DvPart_Prog   = 0
;:DvAdr_Prog    = 0
;2xRAMNative
:DvPart_Kernal = 0
:DvAdr_Kernal  = 11
:DvPart_System = 0
:DvAdr_System  = 11
:DvPart_Disk   = 0
:DvAdr_Disk    = 11
:DvPart_Prog   = 0
:DvAdr_Prog    = 11

;Laufwerk für Bootpartition und
;Ausgabe des Programmcodes
;RAMLink
;:DvPart_Target = 2
;:DvAdr_Target  = 8
;3x1581
;:DvPart_Target = 0
;:DvAdr_Target  = 8
;2xRAMNative
:DvPart_Target = 0
:DvAdr_Target  = 8
