﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DrvNmA			= $560d
:DrvNmB			= $561e
:DrvNmC			= $562f
:DrvNmD			= $5640
:DrvNmVec		= $5605
:PartNmA		= $5659
:PartNmB		= $566a
:PartNmC		= $567b
:PartNmD		= $568c
:PartNmVec		= $5651
