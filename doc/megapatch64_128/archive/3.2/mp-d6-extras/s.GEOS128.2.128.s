﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $4bed
:ReBoot_REU		= $4835
:ReBoot_RL		= $4487
:ReBoot_SCPU		= $4000
:x_ClrDlgScreen		= $6c6f
:x_DoAlarm		= $52e1
:x_EnterDeskTop		= $50e7
:x_GetFiles		= $5357
:x_GetFilesData		= $6775
:x_GetFilesIcon		= $68f4
:x_GetNextDay		= $5293
:x_PanicBox		= $51e7
:x_ToBASIC		= $5154
