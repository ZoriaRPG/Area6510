﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:CurDiskCRC		= $0725
:FATSEK			= $0727
:TD_GetSektor		= $06a7
:TD_NewDisk		= $06a2
:TD_RdSekData		= $051f
:TD_SendStatus		= $052f
:TD_Start		= $0608
:TD_Stop		= $0650
:TD_WrSekData		= $0675
:WasDiskChanged		= $06d6
