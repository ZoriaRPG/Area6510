﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

if .p
			t "src.GEOS_MP3.64"
			t "SymbTab_1"
			t "SymbTab_2"
			t "MacTab"
endif

			n "StampDateAuthor"
			f APPLICATION
			a "Markus Kanet"
			o APP_RAM

h "Setzt aktuelles Datum, Autor und Infotext für alle Dateien im Verzeichnis."

;*** Datum festlegen.
:MainInit		jsr	OpenDisk
			txa
			beq	:51
			rts

::51			lda	year     ,x
			sta	Date_Time,x
			inx
			cpx	#$05
			bcc	:51

			jsr	Get1stDirEntry

			lda	#$00
::52			sta	r2L
			asl
			asl
			asl
			asl
			asl
			tax
			clc
			adc	#25
			tay
			lda	diskBlkBuf+2,x
			beq	:54

			jsr	EditInfoBlock

			ldx	#$00
::53			lda	Date_Time ,x
			sta	diskBlkBuf,y
			iny
			inx
			cpx	#$05
			bcc	:53

::54			lda	r2L
			clc
			adc	#$01
			cmp	#$08
			bcc	:52

			jsr	PutBlock
			txa
			bne	:55

			lda	diskBlkBuf +0
			beq	:55
			sta	r1L
			lda	diskBlkBuf +1
			sta	r1H
			jsr	GetBlock
			txa
			beq	:52
::55			jmp	EnterDeskTop

:EditInfoBlock		lda	diskBlkBuf+2+19,x
			bne	:10
			rts

::10			stx	VecDirEntry
			txa
			pha
			tya
			pha
			PushB	r2L
			PushB	r1L
			PushB	r1H
			PushW	r4

			lda	diskBlkBuf+2+19,x
			sta	r1L
			lda	diskBlkBuf+2+20,x
			sta	r1H

			LoadW	a0,author1
			LoadW	a1,infotext1

			ldy	#$00
			ldx	VecDirEntry
::21			lda	diskBlkBuf+2+3,x
			cmp	#$a0
			beq	:23
			cmp	#"1"
			bne	:22
			lda	diskBlkBuf+2+4,x
			cmp	#"2"
			bne	:22
			lda	diskBlkBuf+2+5,x
			cmp	#"8"
			bne	:22
			LoadW	a0,author2
			LoadW	a1,infotext2
			jmp	:23

::22			inx
			iny
			cpy	#16
			bne	:21

::23			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"+"
			bne	:23a
			LoadW	a0,author2
			LoadW	a1,infotext2

::23a			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"a"
			bne	:24
			lda	diskBlkBuf+2+4,x
			cmp	#"s"
			bne	:24
			lda	diskBlkBuf+2+5,x
			cmp	#"s"
			bne	:24
			lda	diskBlkBuf+2+6,x
			cmp	#"."
			bne	:24
			LoadW	a0,author1
			LoadW	a1,infotext3

::24			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"-"
			bne	:25
			lda	diskBlkBuf+2+4,x
			cmp	#"A"
			bne	:25
			lda	diskBlkBuf+2+5,x
			cmp	#"3"
			bne	:25
			lda	diskBlkBuf+2+6,x
			cmp	#"_"
			bne	:25
			LoadW	a0,author1
			LoadW	a1,infotext4

::25			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"G"
			bne	:26
			lda	diskBlkBuf+2+4,x
			cmp	#"3"
			bne	:26
			lda	diskBlkBuf+2+5,x
			cmp	#"_"
			bne	:26
			lda	diskBlkBuf+2+6,x
			cmp	#"V"
			bne	:26
			LoadW	a0,author1
			LoadW	a1,infotext5

::26			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"S"
			bne	:27
			lda	diskBlkBuf+2+4,x
			cmp	#"y"
			bne	:27
			lda	diskBlkBuf+2+5,x
			cmp	#"m"
			bne	:27
			lda	diskBlkBuf+2+6,x
			cmp	#"b"
			bne	:27
			lda	diskBlkBuf+2+7,x
			cmp	#"T"
			bne	:27
			LoadW	a1,infotext6

::27			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"M"
			bne	:28
			lda	diskBlkBuf+2+4,x
			cmp	#"a"
			bne	:28
			lda	diskBlkBuf+2+5,x
			cmp	#"c"
			bne	:28
			lda	diskBlkBuf+2+6,x
			cmp	#"T"
			bne	:28
			LoadW	a1,infotext7

::28			ldx	VecDirEntry
			lda	diskBlkBuf+2+3,x
			cmp	#"G"
			bne	:29
			lda	diskBlkBuf+2+4,x
			cmp	#"3"
			bne	:29
			lda	diskBlkBuf+2+5,x
			cmp	#"_"
			bne	:29
			lda	diskBlkBuf+2+6,x
			cmp	#"S"
			bne	:29
			LoadW	a0,author1
			LoadW	a1,infotext8

::29

::30			LoadW	r4,fileHeader
			jsr	GetBlock
			txa
			beq	:12
::11			jmp	:90

::12			lda	fileHeader+2
			cmp	#3
			bne	:11
			lda	fileHeader+3
			cmp	#21
			bne	:11
			lda	fileHeader+4
			cmp	#$bf
			bne	:11

			ldy	#$00
::1			lda	(a0L),y
			sta	fileHeader+97,y
			beq	:2
			iny
			bne	:1

::2			lda	fileHeader+160
			cmp	#"*"
			beq	:80

			ldy	#$00
::3			lda	(a1L),y
			sta	fileHeader+160,y
			beq	:4
			iny
			cpy	#96
			bcc	:3

::4			lda	#$00
::5			sta	fileHeader+160,y
			iny
			cpy	#96
			bcc	:5

::80			jsr	PutBlock

::90			PopW	r4
			PopB	r1H
			PopB	r1L
			PopB	r2L
			pla
			tay
			pla
			tax
::99			rts

:Date_Time		s $05
:VecDirEntry		b $00

:author1		b "Markus Kanet",NULL
:author2		b "M.Kanet/W.Grimm",NULL
:infotext1		b "SourceCode MegaPatch",CR
			b "by Markus Kanet",NULL
:infotext2		b "SourceCode MegaPatch",CR
			b "by M.Kanet/W.Grimm",NULL
:infotext3		b "AutoAssembler Dateien für MegaAssembler",NULL
:infotext4		b "Zusätzliche AutoAssembler Dateien"
			b NULL
:infotext5		b "GEOS-Klasse und Autor für MegaPatch Quelltexte festlegen"
			b NULL
:infotext6		b "Symboldateien für MegaPatch und MegaAssembler"
			b NULL
:infotext7		b "Macro-Definitionen für MegaPatch und MegaAssembler"
			b NULL
:infotext8		b "Symboldateien für MegaPatch Quelltexte festlegen"
			b NULL
