﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $36b3
:ReBoot_REU		= $3414
:ReBoot_RL		= $317e
:ReBoot_SCPU		= $2e00
:x_ClrDlgScreen		= $557a
:x_DoAlarm		= $3db6
:x_EnterDeskTop		= $3aa4
:x_GetFiles		= $3e10
:x_GetFilesData		= $508c
:x_GetFilesIcon		= $520b
:x_GetNextDay		= $3d68
:x_PanicBox		= $3cbc
:x_ToBASIC		= $3b11
