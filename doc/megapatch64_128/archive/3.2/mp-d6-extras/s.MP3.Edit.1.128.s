﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3f11
:AutoInitTaskMan	= $3e10
:BankCodeTab1		= $4711
:BankCodeTab2		= $4715
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4719
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $4725
:BankType_Disk		= $4721
:BankType_GEOS		= $471d
:BankUsed		= $46d1
:BankUsed_2GEOS		= $3ba8
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294e
:BootVarEnd		= $294f
:BootVarStart		= $2880
:CheckDrvConfig		= $2c7a
:CheckForSpeed		= $354e
:Class_GeoPaint		= $466f
:Class_ScrSaver		= $465e
:ClearDiskName		= $3575
:ClearDriveData		= $358c
:ClrBank_Blocked	= $3c16
:ClrBank_Spooler	= $3c13
:ClrBank_TaskMan	= $3c10
:CopyStrg_Device	= $404c
:CountDrives		= $3562
:CurDriveMode		= $4680
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $4621

:DiskDriver_DISK	= $464a
:DiskDriver_FName	= $464d
:DiskDriver_INIT	= $4649
:DiskDriver_SIZE	= $464b
:DiskDriver_TYPE	= $4648
:DiskFileDrive		= $4647
:Dlg_DrawTitel		= $3515
:Dlg_IllegalCfg		= $4890
:Dlg_LdDskDrv		= $4820
:Dlg_LdMenuErr		= $4900
:Dlg_NoDskFile		= $479d
:Dlg_SetNewDev		= $4998
:Dlg_Titel1		= $4a0c
:Dlg_Titel2		= $4a1c
:DoInstallDskDev	= $365b
:DriveInUseTab		= $4694
:DriveRAMLink		= $467f
:Err_IllegalConf	= $349f
:ExitToDeskTop		= $2c3d
:FetchRAM_DkDrv		= $3045
:FindCurRLPart		= $4cef
:FindDiskDrvFile	= $30ae
:FindDkDvAllDrv		= $30d7
:FindDriveType		= $398a
:FindRTC_64Net		= $413e
:FindRTC_SM		= $412a
:FindRTCdrive		= $40ed
:Flag_ME1stBoot		= $4682
:GetDrvModVec		= $332c
:GetInpDrvFile		= $4032
:GetMaxFree		= $3c47
:GetMaxSpool		= $3c4d
:GetMaxTask		= $3c4a
:GetPrntDrvFile		= $3fe8
:InitDkDrv_Disk		= $3174
:InitDkDrv_RAM		= $316e
:InitScrSaver		= $3f4d
:InstallRL_Part		= $3687
:IsDrvAdrFree		= $38cb
:IsDrvOnline		= $3a99
:LastSpeedMode		= $4747
:LdScrnFrmDisk		= $2e08
:LoadBootScrn		= $2dd5
:LoadDiskDrivers	= $313a
:LoadDskDrvData		= $33d2
:LoadInptDevice		= $4024
:LoadPrntDevice		= $3fda
:LookForDkDvFile	= $3115
:NewDrive		= $467c
:NewDriveMode		= $467d
:NoInptName		= $4758
:NoPrntName		= $474a
:PrepareExitDT		= $2c4e
:RL_Aktiv		= $4748
:SCPU_Aktiv		= $4749
:SetClockGEOS		= $40b8
:SetSystemDevice	= $302a
:StashRAM_DkDrv		= $3048
:StdClrGrfx		= $2dfa
:StdClrScreen		= $2de3
:StdDiskName		= $4764

:SwapScrSaver		= $3faa
:SysDrive		= $4633
:SysDrvType		= $4634
:SysFileName		= $4636
:SysRealDrvType		= $4635
:SysStackPointer	= $4632
:SystemClass		= $4610
:SystemDlgBox		= $350e
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $467e
:UpdateDiskDriver	= $31b3
:VLIR_BASE		= $4a9e
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2262
:VLIR_Types		= $2380
:firstBootCopy		= $4681
