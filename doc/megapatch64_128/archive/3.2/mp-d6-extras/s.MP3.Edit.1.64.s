﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3d9f
:AutoInitTaskMan	= $3d0b
:BankCodeTab1		= $459f
:BankCodeTab2		= $45a3
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $45a7
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $45b3
:BankType_Disk		= $45af
:BankType_GEOS		= $45ab
:BankUsed		= $455f
:BankUsed_2GEOS		= $3ae6
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $2909
:BootInptName		= $292b
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $291a
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $293c
:BootVarEnd		= $293d
:BootVarStart		= $2880
:CheckDrvConfig		= $2c51
:CheckForSpeed		= $34b0
:Class_GeoPaint		= $44fd
:Class_ScrSaver		= $44ec
:ClearDiskName		= $34d4
:ClearDriveData		= $34eb
:ClrBank_Blocked	= $3b54
:ClrBank_Spooler	= $3b51
:ClrBank_TaskMan	= $3b4e
:CopyStrg_Device	= $3eda
:CountDrives		= $34c1
:CurDriveMode		= $450e
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $44af

:DiskDriver_DISK	= $44d8
:DiskDriver_FName	= $44db
:DiskDriver_INIT	= $44d7
:DiskDriver_SIZE	= $44d9
:DiskDriver_TYPE	= $44d6
:DiskFileDrive		= $44d5
:Dlg_DrawTitel		= $3477
:Dlg_IllegalCfg		= $471a
:Dlg_LdDskDrv		= $46aa
:Dlg_LdMenuErr		= $478a
:Dlg_NoDskFile		= $4628
:Dlg_SetNewDev		= $4821
:Dlg_Titel1		= $4895
:Dlg_Titel2		= $48a5
:DoInstallDskDev	= $35ba
:DriveInUseTab		= $4522
:DriveRAMLink		= $450d
:Err_IllegalConf	= $3401
:ExitToDeskTop		= $2c14
:FetchRAM_DkDrv		= $2fa7
:FindCurRLPart		= $4b78
:FindDiskDrvFile	= $3010
:FindDkDvAllDrv		= $3039
:FindDriveType		= $38e9
:FindRTC_64Net		= $3fcc
:FindRTC_SM		= $3fb8
:FindRTCdrive		= $3f7b
:Flag_ME1stBoot		= $4510
:GetDrvModVec		= $328e
:GetInpDrvFile		= $3ec0
:GetMaxFree		= $3b85
:GetMaxSpool		= $3b8b
:GetMaxTask		= $3b88
:GetPrntDrvFile		= $3e76
:InitDkDrv_Disk		= $30d6
:InitDkDrv_RAM		= $30d0
:InitScrSaver		= $3ddb
:InstallRL_Part		= $35e6
:IsDrvAdrFree		= $382a
:IsDrvOnline		= $39f8
:LastSpeedMode		= $45d2
:LdScrnFrmDisk		= $2d65
:LoadBootScrn		= $2d39
:LoadDiskDrivers	= $309c
:LoadDskDrvData		= $3334
:LoadInptDevice		= $3eb2
:LoadPrntDevice		= $3e68
:LookForDkDvFile	= $3077
:NewDrive		= $450a
:NewDriveMode		= $450b
:NoInptName		= $45e3
:NoPrntName		= $45d5
:PrepareExitDT		= $2c25
:RL_Aktiv		= $45d3
:SCPU_Aktiv		= $45d4
:SetClockGEOS		= $3f46
:SetSystemDevice	= $2f8c
:StashRAM_DkDrv		= $2faa
:StdClrGrfx		= $2d57
:StdClrScreen		= $2d47
:StdDiskName		= $45ef

:SwapScrSaver		= $3e38
:SysDrive		= $44c1
:SysDrvType		= $44c2
:SysFileName		= $44c4
:SysRealDrvType		= $44c3
:SysStackPointer	= $44c0
:SystemClass		= $449e
:SystemDlgBox		= $3470
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $450c
:UpdateDiskDriver	= $3115
:VLIR_BASE		= $4927
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $23d9
:VLIR_Types		= $2380
:firstBootCopy		= $450f
