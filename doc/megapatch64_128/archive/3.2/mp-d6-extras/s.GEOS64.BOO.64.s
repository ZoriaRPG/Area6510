﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $1a2c
:AutoBoot_b		= $1bbb
:Code10L		= $49
:Code10a		= $1f55
:Code10b		= $1f9e
:Code1L			= $53
:Code1a			= $1bbb
:Code1b			= $1c0e
:Code2L			= $48
:Code2a			= $1c0e
:Code2b			= $1c56
:Code3L			= $49
:Code3a			= $1c56
:Code3b			= $1c9f
:Code4L			= $fd
:Code4a			= $1c9f
:Code4b			= $1d9c
:Code6L			= $b4
:Code6a			= $1d9c
:Code6b			= $1e50
:Code8L			= $09
:Code8a			= $1e50
:Code8b			= $1e59
:Code9L			= $fc
:Code9a			= $1e59
:Code9b			= $1f55
:E_KernelData		= $1f9e
:L_KernelData		= $0ffe
:MP3_BANK_1		= $19d2
:MP3_BANK_2		= $1a08
:S_KernelData		= $19ca
:Vec_ReBoot		= $19ca
