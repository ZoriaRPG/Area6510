﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;*** Haupt-Initialisierungsroutine für den GEOS.Editor.
;*** Beim ersten Start wird die Konfiguration im laufenden Betrieb übernommen
;*** bzw. wenn während des Boot-Vorgangs ausgeführt werden alle physikalischen
;*** Laufwerk 8-11 erkannt und installiert.
;*** Zu Testzwecken kann DEBUG_EDITOR auf TRUE gesetzt werden.
;*** In diesem Fall gibt der Editor detailierte Startmeldungen aus.
;******************************************************************************

if .p
;DEBUG_EDITOR = TRUE
:DEBUG_EDITOR = FALSE
endif

;*** Startvorgang initialisieren.
:MainInitBoot		lda	#ST_WR_FORE		;Bildschirm löschen.
			sta	dispBufferOn

			bit	Flag_ME1stBoot		;GEOS.Editor im SETUP-Modus?
			bmi	:53a			; => Nein, weiter...
			bit	firstBoot		;GEOS-BootUp ?
			bmi	:53a			; => Nein, weiter...
			jsr	PrntCfgMessage

;--- Laufwerkstreiber in REU kopieren.
::53a
if DEBUG_EDITOR = TRUE
			jsr	ClearScreen
			jsr	PrintArea001p
endif
			jsr	LoadDiskDrivers
			txa
			beq	:54
			jmp	Err_LdDskFile

;--- Aktiven TaskManager einlesen und in Zwischenspeicher kopieren.
::54
if DEBUG_EDITOR = TRUE
			jsr	ClearScreen
			jsr	PrintArea010p
endif

			bit	firstBoot		;GEOS-BootUp ?
			bpl	:57			; => Ja, weiter...
			lda	Flag_TaskAktiv		;Ist TaskManager installiert ?
			bmi	:56			; => Nein, weiter...

if DEBUG_EDITOR = TRUE
			jsr	PrintArea020p
endif

			LoadW	r0,R2_ADDR_TASKMAN_E
			LoadW	r1,R2_ADDR_TASKMAN
			LoadW	r2,R2_SIZE_TASKMAN
			lda	Flag_TaskBank
			sta	r3L
			jsr	FetchRAM
			jsr	SetTaskBank		;Zeiger auf TaskManager und
			jsr	StashRAM		;aktuellen Manager speichern.

			ldy	#$08			;Variablen einlesen.
::55			lda	R2_ADDR_TASKMAN_E +3,y
			sta	TASK_BANK_ADDR      ,y

if Flag64_128 = TRUE_C128
			lda	R2_ADDR_TASKMAN_E +22,y
			sta	TASK_VDC_ADDR      ,y
			lda	R2_ADDR_TASKMAN_E +22+9,y
			sta	TASK_BANK0_ADDR      ,y
endif
			dey
			bpl	:55

			lda	R2_ADDR_TASKMAN_E +21
			sta	TASK_COUNT
			lda	#$00			;Taskmanager war aktiviert,
::56			sta	BootTaskMan		;"Install"-Flag setzen.
::57			lda	#$ff			;TaskManager abschalten.
			sta	Flag_TaskAktiv

;--- Aktiven Druckerspooler deaktivieren.
			bit	firstBoot		;GEOS-BootUp ?
			bpl	:59			; => Ja, weiter...
			lda	Flag_Spooler		;Ist Spooler installiert ?
			bpl	:58			; => Nein, weiter...

if DEBUG_EDITOR = TRUE
			jsr	PrintArea025p
endif

			lda	Flag_SpoolMinB		;Ist RAM für Druckerspooler
			ora	Flag_SpoolMaxB		;reserviert ?
			beq	:58			; => Nein, weiter...
			lda	Flag_SpoolMaxB
			sec
			sbc	Flag_SpoolMinB
			clc
			adc	#$01
			sta	BootSpoolSize
			ldx	Flag_SpoolCount		;Verzögerungszeit für
			stx	BootSpoolCount		;Druckerspooler setzen.

			lda	#$80			;Spooler war installiert,
::58			sta	BootSpooler		;"Install"-Flag setzen.
::59			lda	#$00			;Spooler deaktivieren.
			sta	Flag_Spooler

;*** Speicherbelegungstabelle erstellen.
:InitRamTab
if DEBUG_EDITOR = TRUE
			jsr	PrintArea030p
endif

			jsr	Make64KRamTab		;Bank-Belegungstabelle definieren.
							;TaskMan/Spooler nicht beachten.
			bit	firstBoot		;GEOS-BootUp ?
			bmi	Find_CMD_SCPU		; => Nein, weiter...
			jsr	AllocBankUser		;Anwenderspeicher reservieren.

;*** SuperCPU erkennen.
:Find_CMD_SCPU
if DEBUG_EDITOR = TRUE
			jsr	PrintArea040p
endif

			lda	#$00			;Takt für SCPU auf 1Mhz setzen.
			sta	LastSpeedMode		;(Falls keine SCPU vorhanden)
			sta	SCPU_Aktiv		;Flag: "Keine SCPU".

			php
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			ldx	MMU
			lda	#$7e
			sta	MMU
endif
			lda	$d0bc

if Flag64_128 = TRUE_C64
			stx	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			stx	MMU
endif

			plp
			and	#%10000000		;Bit 7=1, SCPU nicht aktiv.
			bne	Find_CMD_RL
			dec	SCPU_Aktiv		;Flag setzen: "SCPU verfügbar".

			jsr	CheckForSpeed		;SCPU-Takt ermitteln und
			sta	LastSpeedMode		;zwischenspeichern.

;*** RAMLink erkennen.
:Find_CMD_RL
if DEBUG_EDITOR = TRUE
			jsr	PrintArea045p
endif

			lda	#$00			;Flag: "Keine RAMLink".
			sta	RL_Aktiv

			php
			sei

if Flag64_128 = TRUE_C64
			ldy	CPU_DATA
			lda	#$36
			sta	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			ldy	MMU
			lda	#$4e
			sta	MMU
endif

			ldx	$e0a9			;Byte aus C64-Kernal einlesen.

if Flag64_128 = TRUE_C64
			sty	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			sty	MMU
endif

			plp
			cpx	#$78			;"SEI"-Befehl ?
			bne	:51			;Nein, weiter...
			dec	RL_Aktiv		;RAMLink verfügbar.

;--- Keine RAMLink, RLxy-Laufwerke nach RAMxy konvertieren.
::51			lda	RL_Aktiv		;RAMLink verfügbar ?
			bne	Install			; => Ja, weiter...

			ldx	#$00			;RAMLink-Laufwerke in RAM-Laufwerke
::52			lda	BootConfig,x		;umwandeln, da keine RAMLink
			and	#%11110000		;verfügbar ist. Damit wird versucht
			cmp	#DrvRAMLink		;die Konfiguration beizubehalten!
			bne	:53
			lda	BootConfig,x
			and	#%00001111		;Emulationsmodus isolieren und
			ora	#%10000000		;"RAM-Laufwerk"-Flag setzen.
			sta	BootConfig,x
::53			inx
			cpx	#$04
			bcc	:52

;*** MegaPatch konfigurieren/Menü-Oberfläche starten.
:Install
if DEBUG_EDITOR = TRUE
			jsr	PrintArea050p
endif

			jsr	GetAllSerDrive		;<*> Alle Laufwerke erkennen.

			bit	firstBoot		;GEOS-BootUp ?
			bpl	:51			; => Ja, automatisch installieren.
			ldx	#3
::50			lda	driveType     ,x
			beq	:49
			bmi	:49
			lda	DriveInfoTab  ,x
			beq	:49
			lda	#$ff
			sta	DriveInUseTab ,x
::49			dex
			bpl	:50
			jmp	LoadMainMenu		;Hauptmenü starten.

::51			jsr	PurgeTurbo		;GEOS-TurboDOS abschalten.
if DEBUG_EDITOR = TRUE
			jsr	PrintArea055p
endif
			jsr	InstallDkDev		;Laufwerke installieren.
			jmp	AutoInstall		;Editor/Standard konfigurieren.

;*** MegaPatch während des bootens automatisch konfigurieren.
:AutoInstall
if DEBUG_EDITOR = TRUE
			jsr	PrintArea060p
endif
			jsr	SetClockGEOS		;Uhrzeit einlesen.

:Install_SCPU		bit	SCPU_Aktiv		;Ist SuperCPU aktiviert ?
			bpl	Install_Cursor		; => Nein, weiter...

if DEBUG_EDITOR = TRUE
			jsr	PrintArea070p
endif

			php				;SuperCPU-Taktfrequenz festlegen.
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA
endif

			ldy	#$00
			bit	BootSpeed
			bvs	:51
			iny
::51			sta	$d07a,y			;Takt über Register $D07A/$D07B

if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;einstellen.
endif

			plp

:Install_SCPU_Opt	lda	BootOptimize		;Optimierung für SuperCPU
			jsr	SCPU_SetOpt		;festlegen.

:Install_Cursor
if DEBUG_EDITOR = TRUE
			jsr	PrintArea080p
endif
			lda	BootCRSR_Repeat		;Wiederholungsgeschwindigkeit für
			sta	Flag_CrsrRepeat		;CURSOR festlegen.

:Install_Printer
if DEBUG_EDITOR = TRUE
			jsr	PrintArea082p
endif
			lda	BootPrntMode		;Modus für Druckertreiber
			sta	Flag_LoadPrnt		;aus RAM/DISK festlegen.
			jsr	InitPrntDevice

:Install_Input
if DEBUG_EDITOR = TRUE
			jsr	PrintArea084p
endif
			jsr	InitInptDevice

:Install_Menu
if DEBUG_EDITOR = TRUE
			jsr	PrintArea086p
endif
			lda	BootColsMode		;Modus für Systemfarben festlegen.
			sta	Flag_SetColor
			lda	BootMenuStatus		;Menü-Parameter festlegen.
			sta	Flag_MenuStatus
			lda	BootMLineMode
			sta	Flag_SetMLine

:Install_ScrSaver
if DEBUG_EDITOR = TRUE
			jsr	PrintArea088p
endif
			lda	BootScrSaver		;Modus für Bildschirmschoner
			sta	Flag_ScrSaver		;installieren.
			lda	BootScrSvCnt		;Startverzögerung für
			sta	Flag_ScrSvCnt		;Bildschirmschoner festlegen.
			lda	BootSaverName		;Bildschirmschoner nachladen ?
			beq	:51			;Nein, weiter...
			LoadW	r6,BootSaverName	;Neuen Bildschirmschoner starten.
			jsr	InitScrSaver

::51			bit	Flag_ME1stBoot		;Konfiguration gespeichert, dann
			bpl	Install_Task_Spl	;ist dieses Flag für immer $FF.

if DEBUG_EDITOR = TRUE
			jsr	PrintArea100p
endif
			jmp	ExitToDeskTop

;*** MegaPatch während des bootens automatisch konfigurieren.
;--- Wenn MP3 zum ersten mal installiert wird, dann wird automatisch der
;    TaskManager und der Druckerspooler konfiguriert.
:Install_Task_Spl
if DEBUG_EDITOR = TRUE
			jsr	PrintArea090p
endif
			lda	#$00
			sta	TASK_COUNT		;Vorgabewert: Alle Tasks löschen.
			sta	BootSpoolSize		;Vorgabewert: Spooler deaktivieren.
			jsr	GetMaxFree		;Max. freien Speicher ermitteln.
			cpy	#$03			;Genügend Speicher frei ?
			bcc	:57			; => Nein, Ende...

;--- Max. RAM für TaskManager aktivieren.
			bit	BootTaskMan		;TaskManager installieren ?
			bmi	:52			; => Nein, weiter...
			lda	#MAX_TASK_ACTIV		;Vorgabewert: Alle Tasks aktivieren.
			sta	TASK_COUNT

;--- Max. RAM für Spooler aktivieren.
::52			bit	BootSpooler		;Spooler installieren ?
			bpl	:55			; => Nein, weiter...
			lda	ramExpSize		;Vorgabewert: Max. Spoolergröße.
			cmp	#MAX_SPOOL_SIZE		;Max. Größe des Spoolers
			bcc	:53			;überschritten (Nur Demo-Version) ?
			lda	#MAX_SPOOL_SIZE		; => Ja, Größe auf Maximum setzen.
::53			cmp	#$04			;Mehr als 256K reserviert ?
			bcc	:54			; => Nein, weiter...
			lda	#$04			;Nicht mehr als 256K für Spooler.
::54			sta	BootSpoolSize		;Spoolergröße festlegen.

;--- Taskmanager und Spooler konfigurieren.
::55
if DEBUG_EDITOR = TRUE
			jsr	PrintArea095p
endif
			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.
			jsr	BlockFreeBank		;Zwei Bänke reservieren: 1x für
			jsr	BlockFreeBank		;GEOS-Anwendungen, 1x für Spooler.
			jsr	InitTaskManager		;TaskManager installieren.

			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.
			jsr	BlockFreeBank		;64K für Anwendungen reservieren.
			jsr	InitPrntSpooler		;Spooler installieren.

			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.

;--- Installierte Größe von TaskManager retten.
			jsr	GetMaxTask		;Vorgabewerte für Taskmanager und
							;Druckerspooler bestimmen.
if Flag64_128 = TRUE_C64
			sty	TASK_COUNT
endif
if Flag64_128 = TRUE_C128
			ldx	#0
::2			cpy	#0
			beq	:1
			inx
			dey
			dey
			dey
			jmp	:2
::1			stx	TASK_COUNT
endif

;--- Installierte Größe von Spooler retten.
			jsr	GetMaxSpool
			cpy	#MAX_SPOOL_SIZE		;Max. Größe des Spoolers
			bcc	:56			;überschritten (Nur Demo-Version) ?
			ldy	#MAX_SPOOL_SIZE		; => Ja, Größe auf Maximum setzen.
::56			sty	BootSpoolSize
::57
if DEBUG_EDITOR = TRUE
			jsr	PrintArea100p
endif
			jmp	ExitToDeskTop

;*** Dialogbox zeichnen.
:DrawCfgDlgBox		lda	#$00			;Schatten zeichnen.
			jsr	SetPattern
			jsr	i_Rectangle
			b	$28,$87
			w	$0048 ! DOUBLE_W
			w	$0107 ! DOUBLE_W ! ADD1_W
			lda	C_WinShadow
			jsr	DirectColor

			jsr	i_Rectangle		;Titel zeichnen.
			b	$20,$2f
			w	$0040 ! DOUBLE_W
			w	$00ff ! DOUBLE_W ! ADD1_W
			lda	C_DBoxTitel
			jsr	DirectColor

			jsr	i_Rectangle		;Dialogbox zeichnen.
			b	$30,$7f
			w	$0040 ! DOUBLE_W
			w	$00ff ! DOUBLE_W ! ADD1_W
			lda	#%11111111
			jsr	FrameRectangle
			lda	C_DBoxBack
			jmp	DirectColor

;*** Dialogbox: GEOS-Editor wird konfiguriert.
:PrntCfgMessage		jsr	DrawCfgDlgBox
			jsr	i_PutString		;Textmeldung ausgeben.
			w	$0050 ! DOUBLE_W
			b	$2b
if Sprache = Deutsch
			b	PLAINTEXT,BOLDON
			b	"System vorbereiten"
endif
if Sprache = Englisch
			b	PLAINTEXT,BOLDON
			b	 "Preparing System"
endif
			b	GOTOXY
			w	$0050 ! DOUBLE_W
			b	$40
if Sprache = Deutsch
			b	"Der GEOS.Editor wird"
endif
if Sprache = Englisch
			b	"GEOS.Editor will be configured,"
endif
			b	GOTOXY
			w	$0050 ! DOUBLE_W
			b	$4c
if Sprache = Deutsch
			b	"konfiguriert. Bitte warten..."
endif
if Sprache = Englisch
			b	"please wait..."
endif
			b	NULL
			rts

;*** Bildschirm löschen.
if DEBUG_EDITOR = TRUE
:ClearScreen		lda	#$00
			jsr	SetPattern
			jsr	i_Rectangle
			b	$b8,$c7
			w	$0000 ! DOUBLE_W
			w	$013f ! DOUBLE_W
			lda	#%11111111
			jsr	FrameRectangle
			lda	C_WinBack
			jsr	DirectColor

			lda	#$05
			jsr	SetPattern
			jsr	i_Rectangle
			b	$ba,$c5
			w	$0076 ! DOUBLE_W
			w	$013d ! DOUBLE_W
			lda	#%11111111
			jmp	FrameRectangle
endif

;*** Statusmeldung ausgeben.
if DEBUG_EDITOR = TRUE
:PrintArea001p		lda	#0
			b $2c
:PrintArea010p		lda	#1
			b $2c
:PrintArea020p		lda	#2
			b $2c
:PrintArea025p		lda	#3
			b $2c
:PrintArea030p		lda	#4
			b $2c
:PrintArea040p		lda	#5
			b $2c
:PrintArea045p		lda	#6
			b $2c
:PrintArea050p		lda	#7
			b $2c
:PrintArea055p		lda	#8
			b $2c
:PrintArea060p		lda	#9
			b $2c
:PrintArea070p		lda	#10
			b $2c
:PrintArea080p		lda	#11
			b $2c
:PrintArea082p		lda	#12
			b $2c
:PrintArea084p		lda	#13
			b $2c
:PrintArea086p		lda	#14
			b $2c
:PrintArea088p		lda	#15
			b $2c
:PrintArea090p		lda	#16
			b $2c
:PrintArea095p		lda	#17
			b $2c
:PrintArea100p		lda	#18
:PrintStatus		pha

			jsr	i_GraphicsString
			b	NEWPATTERN,$00
			b	MOVEPENTO
			w	$0001 ! DOUBLE_W
			b	$b9
			b	RECTANGLETO
			w	$0075 ! DOUBLE_W
			b	$c6
			b	ESC_PUTSTRING
			w	$0008 ! DOUBLE_W
			b	$c2
			b	PLAINTEXT,BOLDON
			b	NULL

			pla
			pha
			asl
			tax
			lda	StatusBarText +0,x
			sta	r0L
			lda	StatusBarText +1,x
			sta	r0H
			jsr	PutString

			LoadB	r2L,$ba
			LoadB	r2H,$c5
			LoadW	r3,118 ! DOUBLE_W
			pla
			pha
			asl
			tax
			lda	StatusBarWidth +0,x
			sta	r4L
			lda	StatusBarWidth +1,x
			sta	r4H
			lda	#$01
			jsr	SetPattern
			jsr	Rectangle
			pla
			tax
			lda	StatusBarPercent,x
			sta	r0L
			lda	#$00
			sta	r0H
			LoadW	r11,$0052 ! DOUBLE_W
			LoadB	r1H,$c2
			lda	#"("
			jsr	PutChar
			lda	#%11000000
			jsr	PutDecimal
			lda	#"%"
			jsr	PutChar
			lda	#")"
			jmp	PutChar
endif

if DEBUG_EDITOR = TRUE
:StatusBarWidth		w (118 +2* 000 -1) ! DOUBLE_W
			w (118 +2* 010 -1) ! DOUBLE_W
			w (118 +2* 020 -1) ! DOUBLE_W
			w (118 +2* 025 -1) ! DOUBLE_W
			w (118 +2* 030 -1) ! DOUBLE_W
			w (118 +2* 040 -1) ! DOUBLE_W
			w (118 +2* 045 -1) ! DOUBLE_W
			w (118 +2* 050 -1) ! DOUBLE_W
			w (118 +2* 055 -1) ! DOUBLE_W
			w (118 +2* 060 -1) ! DOUBLE_W
			w (118 +2* 070 -1) ! DOUBLE_W
			w (118 +2* 080 -1) ! DOUBLE_W
			w (118 +2* 082 -1) ! DOUBLE_W
			w (118 +2* 084 -1) ! DOUBLE_W
			w (118 +2* 086 -1) ! DOUBLE_W
			w (118 +2* 088 -1) ! DOUBLE_W
			w (118 +2* 090 -1) ! DOUBLE_W
			w (118 +2* 095 -1) ! DOUBLE_W
			w (118 +2* 100 -1) ! DOUBLE_W

:StatusBarPercent	b 1,10,20,25,30,40,45,50,55
			b 60,70,80,82,84,86,88,90,95,100

:StatusBarText		w PrntStat001p
			w PrntStat010p
			w PrntStat020p
			w PrntStat025p
			w PrntStat030p
			w PrntStat040p
			w PrntStat045p
			w PrntStat050p
			w PrntStat055p
			w PrntStat060p
			w PrntStat070p
			w PrntStat080p
			w PrntStat082p
			w PrntStat084p
			w PrntStat086p
			w PrntStat088p
			w PrntStat090p
			w PrntStat095p
			w PrntStat100p

:PrntStat001p		b "Lfwk.-Treiber",NULL
:PrntStat010p		b "Init",NULL
:PrntStat020p		b "TaskMan #1",NULL
:PrntStat025p		b "Spooler #1",NULL
:PrntStat030p		b "InitRAM",NULL
:PrntStat040p		b "SuperCPU #1",NULL
:PrntStat045p		b "RAMLink",NULL
:PrntStat050p		b "Laufwerk #1",NULL
:PrntStat055p		b "Laufwerk #2",NULL
:PrntStat060p		b "Datum setzen",NULL
:PrntStat070p		b "SuperCPU #2",NULL
:PrntStat080p		b "Cursor",NULL
:PrntStat082p		b "Drucker",NULL
:PrntStat084p		b "Maus/Joystick",NULL
:PrntStat086p		b "Anzeige",NULL
:PrntStat088p		b "Bildschirmschoner",NULL
:PrntStat090p		b "TaskMan/Spooler",NULL
:PrntStat095p		b "Systemspeicher",NULL
:PrntStat100p		b "OK!",NULL
endif
