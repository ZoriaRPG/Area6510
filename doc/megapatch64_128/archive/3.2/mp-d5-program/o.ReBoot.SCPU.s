﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "obj.ReBoot.SCPU"
			t "G3_SymMacExt"
			t "G3_V.Cl.64.Data"

			o BASE_REBOOT
			p GEOS_ReBootSys

:RBOOT_TYPE		= RAM_SCPU

if Flag64_128 = TRUE_C64
			t "-G3_ReBootCode"
endif
if Flag64_128 = TRUE_C128
			t "+G3_ReBootCode"
endif

;*** DoRAMOp-Routine für RAMCard.
			t "-R3_DoRAMOpSRAM"

;*** FetchRAM-Routine für ReBoot.
:SysFetchRAM		sei
			php

if Flag64_128 = TRUE_C64
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU			;I/O-Bereich aktivieren.
			pha
			lda	#$7e
			sta	MMU
endif

			lda	r3L
			pha
			clc
			adc	RamBankFirst +1		;Ersatz für RamBankFirst.
			sta	r3L

			ldy	#%10010001		;JobCode "FetchRAM".
			jsr	DoRAMOp_SRAM
			tay

			pla
			sta	r3L

if Flag64_128 = TRUE_C64
			pla
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			pla
			sta	MMU
endif

			tya
			plp				;I/O deaktivieren.
			rts

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g BASE_REBOOT+R1_SIZE_REBOOT
;******************************************************************************
