﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Einsprungtabelle RAM-Funktionen.
;    Übergabe:		r0	= Startadresse C128-RAM.
;			r1	= Startadresse REU.
;			r2	= Anzahl Bytes.
;			r3L	= Speicherbank.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:DoRAMOp_SRAM		php
			sei

			cpy	#%10010000		;StashRAM ?
			bne	:51			; => Nein, weiter...
			jsr	SCPU_STASH_RAM		;StashRAM für SCPU ausführen.
			jmp	:54

::51			cpy	#%10010001		;FetchRAM ?
			bne	:52			; => Nein, weiter...
			jsr	SCPU_FETCH_RAM		;FetchRAM für SCPU ausführen.
			jmp	:54

::52			cpy	#%10010010		;SwapRAM ?
			bne	:53			; => Nein, weiter...
			jsr	SCPU_SWAP_RAM		;SwapRAM für SCPU ausführen.
			jmp	:54

::53			cpy	#%10010011		;SwapRAM ?
			bne	:54			; => Nein, weiter...
			jsr	SCPU_VERIFY_RAM		;SwapRAM für SCPU ausführen.
			txa				;Verify-Error ?
			beq	:54			; => Nein, weiter...

			ldx	#%00100000
			b $2c
::54			ldx	#%01000000		;Kein Fehler...
			txa
			ldx	#NO_ERROR
			plp
			rts

;*** StashRAM-Routine (16-Bit NativeCode).
:SCPU_STASH_RAM		lda	r3L			;Speicherbank berechnen.
			sta	:51 +1

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$02			;ldx $0002
			b $a4,$04			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			rts

;*** FetchRAM-Routine (16-Bit NativeCode).
:SCPU_FETCH_RAM		lda	r3L			;Speicherbank berechnen.
			sta	:51 +2

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$04			;ldx $0002
			b $a4,$02			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			rts

;*** SwapRAM-Routine (16-Bit NativeCode).
:SCPU_SWAP_RAM		PushW	r0			;Register r0/r1 speichern.
			PushW	r1

			lda	r3L			;Speicherbank berechnen.
			sta	:52 +3
			sta	:53 +3

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $48				;pha
			b $a6,$04			;ldx r1
::52			b $bf,$00,$00,$00		;lda $??:0000,x
			b $a6,$02			;ldx r0
			b $9f,$00,$00,$00		;sta $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $68				;pla
			b $a6,$04			;ldx r1
::53			b $9f,$00,$00,$00		;sta $??:0000,x
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$db			;bne :51

			b $38				;sec
			b $fb				;xce

			PopW	r1			;Register r0/r1 zurücksetzen.
			PopW	r0
			rts

;*** VerifyRAM-Routine (16-Bit NativeCode).
:SCPU_VERIFY_RAM	PushW	r0			;Register r0/r1/r3L speichern.
			PushW	r1
			PushB	r3L

			lda	r3L			;Speicherbank berechnen.
			sta	:52 +3

			lda	#$ff
			sta	r3L

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $a6,$04			;ldx r1
::52			b $df,$00,$00,$00		;cmp $??:0000,x
			b $d0,$0a			;bne :53
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$e7			;bne :51
			b $e6,$08			;inc r3L

::53			b $38				;sec
			b $fb				;xce

			ldx	r3L

			PopB	r3L			;Register r0/r1/r3L zurücksetzen.
			PopW	r1
			PopW	r0
			rts
