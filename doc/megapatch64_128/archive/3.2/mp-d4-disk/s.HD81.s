﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "DiskDev_HD81"
			t "G3_SymMacExtDisk"

			a "M. Kanet"
			o DISK_BASE

if .p
			t "s.1581_Turbo.ext"

:C_41			= FALSE
:C_71			= FALSE
:C_81			= FALSE
:RD_41			= FALSE
:RD_71			= FALSE
:RD_81			= FALSE
:RD_NM			= FALSE
:RD_NM_SCPU		= FALSE
:RD_NM_CREU		= FALSE
:RD_NM_GRAM		= FALSE
:RL_41			= FALSE
:RL_71			= FALSE
:RL_81			= FALSE
:RL_NM			= FALSE
:HD_41			= FALSE
:HD_71			= FALSE
:HD_81			= TRUE
:HD_NM			= FALSE
:HD_41_PP		= FALSE
:HD_71_PP		= FALSE
:HD_81_PP		= FALSE
:HD_NM_PP		= FALSE
:FD_41			= FALSE
:FD_71			= FALSE
:FD_81			= FALSE
:FD_NM			= FALSE
:PC_DOS			= FALSE

:DriveModeFlags		= $00 ! SET_MODE_PARTITION

:PART_TYPE		= $03
:PART_MAX		= 254

:dir3Head		= $9c80
:DiskDrvMode		= DrvHD81
:Tr_BorderBlock		= 40
:Se_BorderBlock		= 39
:Tr_1stDirSek		= 40
:Se_1stDirSek		= 3
:Tr_1stDataSek		= 1
:Se_1stDataSek		= 0
:Tr_DskNameSek		= 40
:Se_DskNameSek		= 0
:MaxDirPages		= 37
endif

;*** Sprungtabelle.
:JumpTab		t "-D3_JumpTable"
:Flag_SetPart		b $00

;*** Tabellen für TurboDOS-Übertragung.
:NibbleByteH		b $00,$80,$20,$a0,$40,$c0,$60,$e0
			b $10,$90,$30,$b0,$50,$d0,$70,$f0
:NibbleByteL		b $00,$20,$00,$20,$10,$30,$10,$30
			b $00,$20,$00,$20,$10,$30,$10,$30

;*** Include-Dateien.
			t "-D3_IncludeFile"

;******************************************************************************
			g dir3Head
;******************************************************************************
