﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#170"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

;******************************************************************************
			t "-DD_JumpTab"
;******************************************************************************
			t "-R3_DetectSRAM"
			t "-R3_GetSizeSRAM"
			t "-R3_DoRAM_SRAM"
			t "-R3_DoRAMOpSRAM"
			t "-DD_RDrvNMSize"
			t "-DD_RDrvNMExist"
			t "-DD_RDrvNMPart"
			t "-DD_AskClrBAM"
;******************************************************************************

;*** Prüfen ob Laufwerk installiert werden kann.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		= DetectSCPU

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:1			; => Ja, Laufwerk initialisieren.

			inx
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf

::1			ldx	curDrive
			lda	ramBase     -8,x	;RAM-Speicher in der REU wieder

:DeInstallDrvData	jsr	FREE_SRAM		;RAMCard Speicher freigeben.

			ldx	curDrive
			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode -8,x
			tax
			rts

;*** Speicher in RAMCard reservieren.
:FREE_SRAM		php				;IRQ sperren.
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA		;I/O-Bereich aktivieren.
			ldy	#$35
			sty	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldx	MMU			;I/O-Bereich aktivieren.
			ldy	#$7e
			sty	MMU
endif

			sta	$d07e			;SuperCPU-Register aktivieren.
			sta	$d27d			;Freien Speicher zurücksetzen.
			sta	$d07f			;SuperCPU-Register abschalten.

if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O-Bereich ausblenden.
endif
if Flag64_128 = TRUE_C128
			stx	MMU			;I/O-Bereich ausblenden.
endif

			plp				;IRQ-Status zurücksetzen.
			rts

;*** Speicher in RAMCard freigeben.
:LOCK_SRAM		php				;IRQ sperren.
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA		;I/O-Bereich aktivieren.
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldx	MMU			;I/O-Bereich aktivieren.
			lda	#$7e
			sta	MMU
endif

			sta	$d07e			;SuperCPU-Register aktivieren.

			ldy	DriveAdr		;Größe des freien Speichers in
			lda	ramBase   -8,y		;der SuperCPU korrigieren.
			clc
			adc	SetSizeRRAM
			sta	$d27d
			lda	#$00
			sta	$d27e

			sta	$d07f			;SuperCPU-Register abschalten.

if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O-Bereich ausblenden.
endif
if Flag64_128 = TRUE_C128
			stx	MMU			;I/O-Bereich ausblenden.
endif

			plp				;IRQ-Status zurücksetzen.
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			lda	#"S"			;Kennung für RAM-Laufwerk/GEOS-DACC.
			ldx	#"R"
			ldy	#"C"
			jsr	SetRDrvName

;--- RAMCard suchen.
			jsr	xTestDriveMode		;RAMCard suchen.
			txa				;Installiert?
			beq	:2			; => Nein, Ende...
::1			rts

;--- RAMCard bereits installiert?
::2			lda	DriveMode
			jsr	CheckRDrvExist		;Laufwerk bereits installiert?
			txa
			bne	:1			; => Nein, Ende...

;--- Verfügbares RAM ermitteln.
			php				;IRQ sperren.
			sei

if Flag64_128 = TRUE_C64
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU
			pha
			lda	#$7e
			sta	MMU
endif

			jsr	SRAM_GET_SIZE
			txa
			bne	:11

			lda	SRAM_BANK_COUNT
			b $2c
::11			lda	#$00

;--- Ergänzung: 16.08.18/M.Kanet
;Im Vergleich zu anderen Speichererweiterungen besitzt die RAMCard ein
;internes Speichermanagement. Der von GEOS reservierte Speicher ist hier
;bereits als "belegt" markiert. Die jetzt ermittelte Anzahl der freien
;Speicherbänke steht somit komplett für das SCPU-Laufwerk zur Verfügung.
;			cmp	#$00			;Speicher verfügbar?
;			beq	:11d			;Nein, Abbruch...
;			ldy	ramExpSize		;Zeiger auf erste Bank ermitteln.
;			ldx	GEOS_RAM_TYP		;GEOS-DACC-Typ einlesen.
;			cpx	#RAM_SCPU		;RAMCard = GEOS-DACC?
;			beq	:11c			;Ja, Speicher beginnt hinter DACC.
;			ldy	SRAM_FREE_START		;Erste freie Speicherbank.
;			lda	SRAM_FREE_END		;Letzte freie Speicherbank.
;::11c			sty	MinFreeRRAM		;Freien Speicher berechnen.
;			cmp	MinFreeRRAM
;			bcc	:11
;			sta	MaxFreeRRAM
;			sec
;			sbc	MinFreeRRAM
::11d			sta	MaxSizeRRAM

if Flag64_128 = TRUE_C64
			pla
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			pla
			sta	MMU
endif
			plp

			lda	MaxSizeRRAM
			cmp	#$02			;Mind 2x64K verfügbar?
			bcs	:21			; => Ja, weiter...
			ldx	#NO_FREE_RAM
			rts

;--- Treiber installieren.
::21			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			sta	RealDrvType -8,x
			and	#%10001111		;Bit #6/#5/#4=Ext.RAM-Type löschen.
			sta	driveType   -8,x
			sta	curType
			lda	#SET_MODE_SUBDIR!SET_MODE_FASTDISK!SET_MODE_SRAM
			sta	RealDrvMode-8,x

			jsr	InitForDskDvJob		;Laufwerkstreiber in GEOS-Speicher
			jsr	StashRAM		;kopieren => Aktueller Treiber
			jsr	DoneWithDskDvJob	;immer im erweiterten Speicher!

;--- Größe des Laufwerks bestimmen.
			jsr	GetCurPartSize		;Falls Laufwerk schon einmal
							;installiert, Größe übernehmen.
			jsr	GetPartSize		;Größe festlegen.
			txa				;Abbruch der Installation?
			beq	:31			; => Nein, weiter...
			ldx	#DEV_NOT_FOUND
			rts

;--- Installation fortsetzen.
::31			ldx	DriveAdr		;Startadresse Laufwerk in
			lda	SRAM_FREE_START		;RAMCard zwischenspeichern.
			sta	ramBase     -8,x

			jsr	LOCK_SRAM		;RAMCard Speicher reservieren.

;--- Laufwerk initialisieren.
::41			jsr	InitRDrvNM
			txa
			beq	:42

			lda	MinFreeRRAM		;Fehler beim erstellen der BAM,
			jsr	DeInstallDrvData	;Laufwerk nicht installiert.

			ldx	#DEV_NOT_FOUND
			b $2c
::42			ldx	#NO_ERROR
			rts

;*** Routine zum schreiben von Sektoren.
;    StashRAM kann nicht verwendet werden, da evtl. GEOS-DACC nicht im
;    RAM gespeichert ist => Falsche RAM-Treiber!
:WriteSektor		PushW	r1

			dec	r1L
			lda	r1H
			clc
			adc	#$00
			sta	r1H
			lda	r1L
			ldx	curDrive
			adc	ramBase -8,x
			sta	r3L
			lda	#$00
			sta	r1L

			LoadW	r0,diskBlkBuf
			LoadW	r2,$0100

			jsr	StashRAM_SRAM

			PopW	r1

			lda	#%01000000		;Kein Fehler...
			ldx	#NO_ERROR
			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:MinFreeRRAM		b $00
:MaxFreeRRAM		b $00

;*** Titelzeile für Dialogbox.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation SuperRAM Native",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation SuperRAM Native",NULL
endif

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
