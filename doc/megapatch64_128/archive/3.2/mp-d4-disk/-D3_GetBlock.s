﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadLink
:xReadBlock		jsr	TestTrSe_ADDR		;Sektor-Adresse testen.
			bcc	:53			;Fehler, Abbruch...

			bit	curType			;Shadow1541 aktiv ?
			bvc	:51			;Nein, weiter...
			jsr	IsSekInShadowRAM	;Sektor in RAM gespeichert ?
			bne	:53			;Ja, weiter...

::51			ldx	#> TD_VerSekData
			lda	#< TD_VerSekData
			jsr	xTurboRoutSet_r1
			ldx	#> TD_RdSekData
			lda	#< TD_RdSekData
			jsr	xTurboRoutine
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET-Routine übergeben.
			ldy	#$00
			jsr	Turbo_GetBytes
			jsr	GetReadError
			txa
			beq	:52

			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:52
			bcs	:51

::52			txa
			bne	:53
			bit	curType
			bvc	:53
			jsr	SaveSekInRAM
::53			ldy	#$00
			rts
endif

;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadLink
:xReadBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:52			;Nein, Abbruch...

::51			jsr	Turbo_GetBlock
			jsr	Turbo_GetBytes
			jsr	GetReadError		;TurboDOS-Status einlesen.
			txa				;Fehler aufgetreten ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:52			;Ja, Abbruch...
			bcs	:51			;Sektor nochmal lesen.

::52			ldy	#$00
			rts
endif

;******************************************************************************
::tmp2 = FD_41!FD_71!HD_41!HD_71
if :tmp2 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadBlock		jsr	TestTrSe_ADDR
			bcc	ExitRead

;*** Einsprung aus ":ReadLink".
:GetLinkBytes		ldx	#> TD_GetSektor
			lda	#< TD_GetSektor
			jsr	xTurboRoutSet_r1
			ldx	#> TD_RdSekData
			lda	#< TD_RdSekData
			jsr	xTurboRoutine

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET-Routine übergeben.

			ldy	#$00
			lda	r1L
			bpl	:51
			ldy	#$02
::51			jsr	Turbo_GetBytes
			jsr	GetReadError
			beq	:52

			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:52
			bcs	GetLinkBytes

::52			ldy	#$00
:ExitRead		rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		jsr	TestTrSe_ADDR		;Sektoradresse testen.
			bcc	:51			;Fehler, Abbruch...

			lda	r1L			;Flag für LinkBytes setzen.
			ora	#$80
			sta	r1L
			jsr	GetLinkBytes		;LinkBytes einlesen
			lda	r1L
			and	#$7f			;Flag für LinkBytes löschen.
			sta	r1L
::51			rts
endif

;******************************************************************************
::tmp3 = C_81!FD_81!FD_NM!HD_81
if :tmp3 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadBlock		jsr	TestTrSe_ADDR
			bcc	ExitRead

;*** Einsprung aus ":ReadLink".
:GetLinkBytes		ldx	#> TD_GetSektor
			lda	#< TD_GetSektor
			jsr	xTurboRoutSet_r1
			ldx	#> TD_RdSekData
			lda	#< TD_RdSekData
			jsr	xTurboRoutine

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET-Routine übergeben.

			ldy	#$00
			lda	r1L
			bpl	:51
			ldy	#$02
::51			jsr	Turbo_GetBytes
			jsr	GetReadError
			txa
			beq	:52

			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:52
			bcs	GetLinkBytes

::52			jsr	SwapDskNamData
			ldy	#$00
:ExitRead		rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		jsr	TestTrSe_ADDR		;Sektoradresse testen.
			bcc	:51			;Fehler, Abbruch...

			lda	r1L			;Flag für LinkBytes setzen.
			ora	#$80
			sta	r1L
			jsr	GetLinkBytes		;LinkBytes einlesen
			lda	r1L
			and	#$7f			;Flag für LinkBytes löschen.
			sta	r1L
::51			rts
endif

;******************************************************************************
::tmp4 = HD_NM
if :tmp4 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadBlock
:xReadLink		jsr	TestTrSe_ADDR
			bcc	ExitRead

;*** Einsprung aus ":ReadLink".
:GetLinkBytes		ldx	#> TD_GetSektor
			lda	#< TD_GetSektor
			jsr	xTurboRoutSet_r1
			ldx	#> TD_RdSekData
			lda	#< TD_RdSekData
			jsr	xTurboRoutine

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET-Routine übergeben.

			ldy	#$00
			jsr	Turbo_GetBytes
			jsr	GetReadError
			txa
			beq	:52

			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:52
			bcs	GetLinkBytes

::52			jsr	SwapDskNamData
			ldy	#$00
:ExitRead		rts
endif

;******************************************************************************
::tmp5 = RL_41!RL_71
if :tmp5 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock
:xReadBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	RL_DataCheck		;RAMLink-Daten überprüfen.
			txa				;Partitionsfehler ?
			bne	:51			; => Ja, Abbruch...

			jsr	TestTrSe_ADDR		;Sektor-Transfer initialisieren.
			bcc	:51			;Fehler, Abbrch...

			PushB	r3H			;Register ":r3H" zwischenspeichern.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekRead		;Sektor von Diskette lesen.
			PopB	r3H			;Register ":r3H" zurücksetzen.

::51			ldy	#$00
			plp				;IRQ-Status zurücksetzen.
			txa
			rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	RL_DataCheck		;RAMLink-Daten überprüfen.
			txa				;Partitionsfehler ?
			bne	:51			; => Ja, Abbruch...

			jsr	TestTrSe_ADDR		;Track/Sektor-Adresse testen.
			bcc	:51			;Fehler, Abbrch...

			jsr	Save_RegData		;Register ":r0" bis ":r4" speichern.

			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			LoadW	r2,$0002		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.
			jsr	xDsk_FetchRAM		;Daten aus RAMLink einlesen.

			jsr	Load_RegData		;Register ":r0" bis ":r4" einlesen.

			ldx	#$00			;Flag: "Kein Fehler"...
::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp6 = RL_81!RL_NM
if :tmp6 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock
:xReadBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	RL_DataCheck		;RAMLink-Daten überprüfen.
			txa				;Partitionsfehler ?
			bne	:51			; => Ja, Abbruch...

			jsr	TestTrSe_ADDR		;Block-Transfer initialisieren.
			bcc	:51			; => Sektor-Adresse fehlerhaft.

			PushB	r3H			;Register ":r3" retten.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekRead		;Sektor von Diskette lesen.
			jsr	SwapDskNamData		;Diskettenname nach GEOS.
			PopB	r3H			;Register ":r3" zurücksetzen.

::51			ldy	#$00
			plp				;IRQ-Status zurücksetzen.
			txa
			rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	RL_DataCheck		;RAMLink-Daten überprüfen.
			txa				;Partitionsfehler ?
			bne	:51			; => Ja, Abbruch...

			jsr	TestTrSe_ADDR		;Track/Sektor-Adresse testen.
			bcc	:51			;Fehler, Abbrch...

			jsr	Save_RegData		;Register ":r0" bis ":r4" speichern.

			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			LoadW	r2,$0002		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.
			jsr	xDsk_FetchRAM		;Daten aus RAMLink einlesen.

			jsr	Load_RegData		;Register ":r0" bis ":r4" einlesen.

			ldx	#$00			;Flag: "Kein Fehler"...
::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp7 = RD_41!RD_71!RD_81!RD_NM
if :tmp7 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock
:xReadBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Block-Transfer initialisieren.
			bcc	:51			; => Sektor-Adresse fehlerhaft.

			jsr	xDsk_SekRead		;Sektor von Diskette lesen.

::51			ldy	#$00
			plp				;IRQ-Status zurücksetzen.
			txa
			rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Track/Sektor-Adresse testen.
			bcc	:51			;Fehler, Abbrch...

			jsr	Save_RegData		;Register ":r0" bis ":r4" speichern.

			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			LoadW	r2,$0002		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.
			jsr	FetchRAM		;Daten aus RAMLink einlesen.

			jsr	Load_RegData		;Register ":r0" bis ":r4" einlesen.

			ldx	#$00			;Flag: "Kein Fehler"...
::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp8 = RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
if :tmp8 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock
:xReadBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Block-Transfer initialisieren.
			bcc	:51			; => Sektor-Adresse fehlerhaft.

			jsr	xDsk_SekRead		;Sektor von Diskette lesen.

::51			ldy	#$00
			plp				;IRQ-Status zurücksetzen.
			txa
			rts

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xReadLink		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Track/Sektor-Adresse testen.
			bcc	:51			;Fehler, Abbrch...

			jsr	Save_RegData		;Register ":r0" bis ":r4" speichern.

			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			LoadW	r2,$0002		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.
			jsr	DoFetch			;FetchRAM für SCPU ausführen.

			jsr	Load_RegData		;Register ":r0" bis ":r4" einlesen.

			ldx	#$00			;Flag: "Kein Fehler"...
::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp9 = HD_41_PP!HD_71_PP
if :tmp9 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadBlock		lda	#$00
			b $2c

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadLink		lda	#$02
			sta	Flag_RdDataMode

			jsr	TestTrSe_ADDR
			bcc	:3

			ldx	#$02
			lda	Flag_RdDataMode
			beq	:1
			inx
::1			jsr	TurboRoutine1

::2			ldx	ErrorCode
::3			rts

::4			ldx	#$02
			rts

:Flag_RdDataMode	b $00
endif

;******************************************************************************
::tmp10 = HD_81_PP!HD_NM_PP
if :tmp10 = TRUE
;******************************************************************************
;*** Sektor nach ":diskBlkBuf" einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xGetBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xGetBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xReadBlock		;Sektor von Diskette lesen.
			jmp	DoneWithIO		;I/O abschalten
::51			rts				;Ende...

;*** Sektor von Diskette einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadBlock		lda	#$00
			b $2c

;*** Link-Bytes eines Sektors einlesen.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xReadLink		lda	#$02
			sta	Flag_RdDataMode

			jsr	TestTrSe_ADDR
			bcc	:3

			ldx	#$02
			lda	Flag_RdDataMode
			beq	:1
			inx
::1			jsr	TurboRoutine1
			jsr	SwapDskNamData

::2			ldx	ErrorCode
::3			rts

::4			ldx	#$02
			rts

:Flag_RdDataMode	b $00
endif
