﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren und aus Laufwerk entfernen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPurgeTurbo		jsr	InitShadowRAM
			jsr	xExitTurbo

;*** TurboDOS in aktuellem Laufwerk "Nicht verfügbar".
:TurboOff_curDrv	ldy	curDrive
			lda	#$00
			sta	turboFlags -8,y
			rts
endif

;******************************************************************************
::tmp1a = C_71!C_81!FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp1b = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp1c = RL_41!RL_71!RL_81!RL_NM!RD_41!RD_71!RD_81!RD_NM
::tmp1d = RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
::tmp1  = :tmp1a!:tmp1b!:tmp1c!:tmp1d
if :tmp1 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren und aus Laufwerk entfernen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPurgeTurbo		jsr	xExitTurbo

;*** TurboFlags auf aktuellem Laufwerk löschen.
:TurboOff_curDrv	ldy	curDrive
			lda	#$00
			sta	turboFlags -8,y
			rts
endif
