﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = FD_NM!HD_NM!HD_NM_PP
::tmp0b = RL_41!RL_71!RL_NM!RD_NM
::tmp0c = RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
::tmp0  = :tmp0a!:tmp0b!:tmp0c
if :tmp0 = TRUE
;******************************************************************************
:StashDriverData	jsr	InitForDskDvJob
			jsr	Save_RegData		;ZeroPage-Register speichern.

			LoadW	r0 , S_DRIVER_DATA
			AddVW	    (S_DRIVER_DATA-DISK_BASE    ),r1
			LoadW	r2 ,(E_DRIVER_DATA-S_DRIVER_DATA)
			jsr	StashRAM

			jsr	Load_RegData
			jmp	DoneWithDskDvJob
endif

;******************************************************************************
::tmp1 = C_81!FD_81!HD_81!RL_81!HD_81_PP
if :tmp1 = TRUE
;******************************************************************************
:StashDriverData	jsr	InitForDskDvJob
			jsr	Save_RegData		;ZeroPage-Register speichern.

			LoadW	r0 , S_DRIVER_DATA
			AddVW	    (S_DRIVER_DATA-DISK_BASE    ),r1
			LoadW	r2 ,(E_DRIVER_DATA-S_DRIVER_DATA)
			jsr	StashRAM

			jsr	Load_RegData
			jsr	DoneWithDskDvJob

			jsr	Save_RegData		;ZeroPage-Register speichern.
			jsr	Save_dir3Head
			jmp	Load_RegData
endif

;******************************************************************************
::tmp2 = RD_81
if :tmp2 = TRUE
;******************************************************************************
:StashDriverData	jsr	Save_RegData		;ZeroPage-Register speichern.
			jsr	Save_dir3Head
			jmp	Load_RegData
endif
