﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81!FD_41!FD_71!FD_81!PC_DOS!HD_41!HD_71!HD_81
::tmp0b = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp0  = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** 32 Byte-Daten aus TurboDOS-Routine in FloppyRAM kopieren.
:CopyTurboDOSByt	ldx	#> Floppy_MW
			lda	#< Floppy_MW
			jsr	SendFloppyCom		;"M-W"-Befehl an Floppy senden.
			txa				;Laufwerksfehler ?
			bne	:53			;Ja, Abbruch...

			lda	#$20			;Anzahl Bytes an Floppy senden.
			jsr	$ffa8			;(Max. 32 Bytes wegen Puffergröße!)

			ldy	#$00
::51			lda	($8b),y			;Byte einlesen und an Floppy senden.
			jsr	$ffa8
			iny
			cpy	#$20			;Alle Bytes gesendet ?
			bcc	:51			;Nein, weiter...

			jsr	$ffae			;Laufwerk abschalten.
::52			ldx	#$00			;Flag: "Kein Fehler..."
::53			rts

;*** Befehl für "M-W".
:Floppy_MW		b "M-W"
:Floppy_ADDR
:Floppy_ADDR_L		b $00
:Floppy_ADDR_H		b $00
endif
