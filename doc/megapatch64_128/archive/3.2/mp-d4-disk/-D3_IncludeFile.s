﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
:E_DRIVER_DATA
;******************************************************************************

;*** Include-Dateien.
;--- Ergänyung: 11.08.18/M.Kanet
;Die Routine "TestRL" ist zu Testzwecken deaktiviert, da der Test auf die
;RAMLink bereits bei der Installation erfolgt. Daher besteht kein Grund
;bei jedem RAMLink-Zugriff auf die RAMLink zu testen.
:D3_001			;t "-D3_TestRL"
:D3_002			t "-D3_FindRAMLink"
:D3_003			t "-D3_SvLd_dir3Hd"
:D3_004			t "-D3_SwapDkNmDat"
:D3_005			t "-D3_TestTrSeAdr"
:D3_006			t "-D3_ClrDkBlkBuf"
:D3_007			t "-D3_SvLd_RegDat"
:D3_008			t "-D3_DefSkAdrREU"
:D3_009			t "-D3_Dsk_DoRAMOp"
:D3_010			t "-D3_Dsk_DoSekOp"
:D3_011			t "-D3_1541_Cache"
:D3_012			t "-D3_SdiskBlkBuf"
:D3_013			t "-D3_ScurDirHead"
:D3_014			t "-D3_S1stDirSek"
:D3_015			t "-D3_SDirHead"
:D3_016			t "-D3_InitForIO"
:D3_017			t "-D3_DoneWithIO"
:D3_018			t "-D3_PurgeTurbo"
:D3_019			t "-D3_ExitTurbo"
:D3_020			t "-D3_EnterTurbo"
:D3_021			t "-D3_OpenDir"
:D3_022			t "-D3_OpenDisk"
:D3_023			t "-D3_OpenPart"
:D3_025			t "-D3_LogNewPart"
:D3_026			t "-D3_LogNewDisk"
:D3_024			t "-D3_NewDisk"
:D3_027			t "-D3_CalcBlkFree"
:D3_028			t "-D3_GetDiskSize"
:D3_029			t "-D3_ChkDkGEOS"
:D3_030			t "-D3_SetGEOSDisk"
:D3_031			t "-D3_ChangeDDev"
:D3_032			t "-D3_GetBlock"
:D3_033			t "-D3_PutBlock"
:D3_034			t "-D3_VerWrBlock"
:D3_038			t "-D3_GetFreeDirB"
:D3_039			t "-D3_CreateNewDB"
:D3_040			t "-D3_GetDirEntry"
:D3_041			t "-D3_GetBorderB"
:D3_042			t "-D3_GetPDEntry"
:D3_043			t "-D3_GetPTypes"
;D3_044			t "-D3_RdPartHead"
:D3_045			t "-D3_BlkAlloc"
:D3_046			t "-D3_AllocFreBlk"
:D3_048			t "-D3_SwapBlkMode"
:D3_049			t "-D3_FindBAMBit"
:D3_050			t "-D3_SetNextFree"
:D3_051			t "-D3_GetDirHead"
:D3_052			t "-D3_PutDirHead"
:D3_053			t "-D3_BAMBlockJob"
:D3_054			t "-D3_SetBAM_TrSe"
:D3_055			t "-D3_IsDirSkFree"
:D3_056			t "-D3_GetMaxSekTr"
:D3_057			t "-D3_InitTD"
:D3_058			t "-D3_TurnOffTD"
:D3_059			t "-D3_GetBAMOffst"
:D3_060			t "-D3_TurboRout"
:D3_061			t "-D3_TurboPutByt"
:D3_062			t "-D3_TurboGetByt"
:D3_063			t "-D3_CopyTDByte"
:D3_064			t "-D3_TPutGetBlk"
:D3_065			t "-D3_TD_DATACLK"
:D3_066			t "-D3_GetDskError"
:D3_067			t "-D3_SendFCom"
;D3_068			t "-D3_FComInitDsk"
:D3_069			t "-D3_StashDrvDat"
