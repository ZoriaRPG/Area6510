﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#160"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

			t "-DD_JumpTab"

;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		jmp	GetFreeBank

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			jsr	TestDriveMode		;Freien RAM-Speicher testen.
			cpx	#NO_ERROR		;Ist genügend Speicher frei ?
			bne	:53			; => Nein, Installationsfehler.

			sta	DrvRamBase		;Startadresse RAM-Speicher merken.

;--- Angeschlossenes Laufwerk testen.
::51			lda	DriveAdr		;Aktuelles Laufwerk feststellen.
			jsr	TestDriveType
			cpx	#NO_ERROR		;Installationsfehler ?
			bne	:53			; => Ja, Abbruch...

			lda	DriveMode
			cmp	#Drv81DOS
			bne	:54

;--- 1581/DOS installieren.
			cpy	#Drv1581		;1581 erkannt ?
			beq	:57			; => Ja, weiter...

;--- Kompatibles Laufwerk suchen.
::Find81DOS		lda	#Drv1581
			ldy	DriveAdr
			jsr	FindDrive		;1581-Laufwerk suchen.
			txa				;Laufwerk gefunden ?
			beq	:57			; => Ja, weiter...

			lda	DriveAdr
			jsr	TurnOnNewDrive		;Dialogbox ausgeben.
			txa				;Lauafwerk eingeschaltet ?
			beq	:Find81DOS		; => Ja, Laufwerk suchen...

;--- Kein passendes Laufwerk gefunden.
::52			ldx	#DEV_NOT_FOUND
::53			rts

;--- CMDFD/DOS installieren.
::54			cpy	#DrvFD			;CMDFD erkannt ?
			beq	:57			; => Ja, weiter...

;--- Kompatibles Laufwerk suchen.
::FindFDDOS		lda	#DrvFD
			ldy	DriveAdr
			jsr	FindDrive		;CMDFD-Laufwerk suchen.
			txa				;Laufwerk gefunden ?
			beq	:57			; => Ja, weiter...

			lda	DriveAdr
			jsr	TurnOnNewDrive		;Dialogbox ausgeben.
			txa				;Lauafwerk eingeschaltet ?
			beq	:FindFDDOS		; => Ja, Laufwerk suchen...
			bne	:52

;--- Laufwerk installieren.
::57			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			and	#%00001111
			sta	driveType   -8,x
			sta	curType

			lda	DrvRamBase		;Cache-Speicher in REU belegen.
			ldy	#$01
			jsr	AllocateBankTab
			cpx	#NO_ERROR		;Speicher reserviert ?
			bne	:53			; => Nein, Installationsfehler.

			ldx	DriveAdr		;Startadresse Cache-Speicher in
			lda	DrvRamBase
			sta	ramBase     -8,x	;REU zwischenspeichern.

			lda	DriveMode
;			and	#%00001111
			sta	RealDrvType -8,x
			sta	BASE_DDRV_DATA + (DiskDrvType - DISK_BASE)
			lda	#SET_MODE_SUBDIR
			sta	RealDrvMode -8,x

;--- PCDOS Zusatz-Treiber installieren.
			LoadW	r0,DataExtDOS		;Erweiterte DOS-Routinen
			LoadW	r1,$f000		;in Speicher verschieben.
			LoadW	r2,$1000
			lda	DrvRamBase
			sta	r3L
			jsr	StashRAM

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- Ende, kein Fehler...
			ldx	#$00
			rts

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa				;RAM-Speicher in der REU wieder
			pha				;freigeben.
			lda	ramBase     -8,x
			jsr	FreeBank
			pla
			tax

::51			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000
:DrvOnline		b $00
:DrvRamBase		b $00
:DataExtDOS		d "obj.PCDOS",NULL

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
