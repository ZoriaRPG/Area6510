﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "obj.DvRAM_RL"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Data"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Data"
endif

			o BASE_RAM_DRV

			h "MegaPatch-Kernal"
			h "RAMLink-RAM-Funktionen..."

;******************************************************************************
;RAM_Type = RAM_RL
;******************************************************************************
;*** Einsprungtabelle RAM-Tools.
:xVerifyRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:xStashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xSwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xFetchRAM		ldy	#%10010001		;RAM-Bereich laden.

:xDoRAMOp		php
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA
			lda	#$36
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU
			pha
			lda	#$4e
			sta	MMU
			lda	RAM_Conf_Reg
			pha
			and	#$f0
			ora	#$04
			sta	RAM_Conf_Reg
endif

			jsr	$e0a9

			sty	EXP_BASE2    + 1

			ldy	#$03
::51			lda	zpage        + 1,y
			sta	EXP_BASE2    + 1,y
			dey
			bne	:51

			sty	EXP_BASE2    +10

			lda	r1H
			clc
			adc	RamBankFirst + 0
			sta	EXP_BASE2    + 5
			lda	r3L
			adc	RamBankFirst + 1
			sta	EXP_BASE2    + 6

			lda	r2L
			sta	EXP_BASE2    + 7
			lda	r2H
			sta	EXP_BASE2    + 8

			jsr	$fe06			;Job ausführen und

			lda	$de00			;Fehlerflag auslesen und
			pha
			jsr	$fe0f			;RL-Hardware abschalten.
			pla

if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O ausblenden.
endif
if Flag64_128 = TRUE_C128
			tax
			pla
			sta	RAM_Conf_Reg
			pla
			sta	MMU
			txa				;Fehlerflag in Akku.
endif

			plp
			ldx	#NO_ERROR		;Kein Fehler.
			rts

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g BASE_RAM_DRV_END
;******************************************************************************
