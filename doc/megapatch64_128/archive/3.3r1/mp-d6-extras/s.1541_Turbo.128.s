﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:TD_NewDisk		= $04dc
:TD_NewDrvAdr		= $0439
:TD_RdSekData		= $0320
:TD_SendStatus		= $0325
:TD_Start		= $03e2
:TD_Stop		= $0420
:TD_VerSekData		= $058e
:TD_WrSekData		= $057c
