﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:TD_ClearCache		= $04b9
:TD_GetSektor		= $04cc
:TD_NewDisk		= $049b
:TD_RdSekData		= $031f
:TD_SendStatus		= $032b
:TD_Start		= $040f
:TD_Stop		= $0457
:TD_WrSekData		= $047c
