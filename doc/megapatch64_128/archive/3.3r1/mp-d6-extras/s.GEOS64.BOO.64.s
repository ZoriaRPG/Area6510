﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $1afd
:AutoBoot_b		= $1c8c
:Code10L		= $49
:Code10a		= $202a
:Code10b		= $2073
:Code1L			= $57
:Code1a			= $1c8c
:Code1b			= $1ce3
:Code2L			= $48
:Code2a			= $1ce3
:Code2b			= $1d2b
:Code3L			= $49
:Code3a			= $1d2b
:Code3b			= $1d74
:Code4L			= $fd
:Code4a			= $1d74
:Code4b			= $1e71
:Code6L			= $b4
:Code6a			= $1e71
:Code6b			= $1f25
:Code8L			= $09
:Code8a			= $1f25
:Code8b			= $1f2e
:Code9L			= $fc
:Code9a			= $1f2e
:Code9b			= $202a
:E_KernelData		= $2073
:L_KernelData		= $0ffe
:MP3_BANK_1		= $1aa3
:MP3_BANK_2		= $1ad9
:S_KernelData		= $1a9b
:Vec_ReBoot		= $1a9b
