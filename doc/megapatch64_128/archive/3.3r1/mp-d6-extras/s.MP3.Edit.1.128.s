﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3f43
:AutoInitTaskMan	= $3e42
:BankCodeTab1		= $473a
:BankCodeTab2		= $473e
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4742
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $474e
:BankType_Disk		= $474a
:BankType_GEOS		= $4746
:BankUsed		= $46fa
:BankUsed_2GEOS		= $3bda
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294e
:BootVarEnd		= $294f
:BootVarStart		= $2880
:CheckDrvConfig		= $2cac
:CheckForSpeed		= $3580
:Class_GeoPaint		= $4698
:Class_ScrSaver		= $4687
:ClearDiskName		= $35a7
:ClearDriveData		= $35be
:ClrBank_Blocked	= $3c48
:ClrBank_Spooler	= $3c45
:ClrBank_TaskMan	= $3c42
:CopyStrg_Device	= $407e
:CountDrives		= $3594
:CurDriveMode		= $46a9
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $464a

:DiskDriver_DISK	= $4673
:DiskDriver_FName	= $4676
:DiskDriver_INIT	= $4672
:DiskDriver_SIZE	= $4674
:DiskDriver_TYPE	= $4671
:DiskFileDrive		= $4670
:Dlg_DrawTitel		= $3547
:Dlg_IllegalCfg		= $48b9
:Dlg_LdDskDrv		= $4849
:Dlg_LdMenuErr		= $4929
:Dlg_NoDskFile		= $47c6
:Dlg_SetNewDev		= $49c1
:Dlg_Titel1		= $4a35
:Dlg_Titel2		= $4a45
:DoInstallDskDev	= $368d
:DriveInUseTab		= $46bd
:DriveRAMLink		= $46a8
:Err_IllegalConf	= $34d1
:ExitToDeskTop		= $2c6f
:FetchRAM_DkDrv		= $3077
:FindCurRLPart		= $4d18
:FindDiskDrvFile	= $30e0
:FindDkDvAllDrv		= $3109
:FindDriveType		= $39bc
:FindRTC_64Net		= $4168
:FindRTC_SM		= $4154
:FindRTCdrive		= $4117
:Flag_ME1stBoot		= $46ab
:GetDrvModVec		= $335e
:GetInpDrvFile		= $4064
:GetMaxFree		= $3c79
:GetMaxSpool		= $3c7f
:GetMaxTask		= $3c7c
:GetPrntDrvFile		= $401a
:InitDkDrv_Disk		= $31a6
:InitDkDrv_RAM		= $31a0
:InitScrSaver		= $3f7f
:InstallRL_Part		= $36b9
:IsDrvAdrFree		= $38fd
:IsDrvOnline		= $3acb
:LastSpeedMode		= $4770
:LdScrnFrmDisk		= $2e3a
:LoadBootScrn		= $2e07
:LoadDiskDrivers	= $316c
:LoadDskDrvData		= $3404
:LoadInptDevice		= $4056
:LoadPrntDevice		= $400c
:LookForDkDvFile	= $3147
:NewDrive		= $46a5
:NewDriveMode		= $46a6
:NoInptName		= $4781
:NoPrntName		= $4773
:PrepareExitDT		= $2c80
:RL_Aktiv		= $4771
:SCPU_Aktiv		= $4772
:SetClockGEOS		= $40ea
:SetSystemDevice	= $305c
:StashRAM_DkDrv		= $307a
:StdClrGrfx		= $2e2c
:StdClrScreen		= $2e15
:StdDiskName		= $478d

:SwapScrSaver		= $3fdc
:SysDrive		= $465c
:SysDrvType		= $465d
:SysFileName		= $465f
:SysRealDrvType		= $465e
:SysStackPointer	= $465b
:SystemClass		= $4639
:SystemDlgBox		= $3540
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $46a7
:UpdateDiskDriver	= $31e5
:VLIR_BASE		= $4ac7
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2239
:VLIR_Types		= $2380
:firstBootCopy		= $46aa
