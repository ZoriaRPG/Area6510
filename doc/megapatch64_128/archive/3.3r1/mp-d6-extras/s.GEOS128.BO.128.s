﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $2984
:AutoBoot_b		= $2b7b
:Code10L		= $4c
:Code10a		= $2f60
:Code10b		= $2fac
:Code1L			= $6d
:Code1a			= $2b7b
:Code1b			= $2be8
:Code2L			= $65
:Code2a			= $2be8
:Code2b			= $2c4d
:Code3L			= $4c
:Code3a			= $2c4d
:Code3b			= $2c99
:Code4L			= $fd
:Code4a			= $2c99
:Code4b			= $2d96
:Code6L			= $c5
:Code6a			= $2d96
:Code6b			= $2e5b
:Code8L			= $09
:Code8a			= $2e5b
:Code8b			= $2e64
:Code9L			= $fc
:Code9a			= $2e64
:Code9b			= $2f60
:E_KernelData		= $2fac
:L_KernelData		= $1cfe
:MP3_BANK_1		= $292a
:MP3_BANK_2		= $2960
:S_KernelData		= $2922
:Vec_ReBoot		= $2922
