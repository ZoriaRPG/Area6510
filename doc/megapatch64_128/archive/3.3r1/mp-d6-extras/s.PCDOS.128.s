﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ConvClu2Sek		= $97bb
:ConvDOS2CBMsek		= $9860
:CurrentDiskName	= $90fc
:DOS_DataArea		= $90a6
:Data_1stRDirSek	= $90c6
:Data_1stSDirClu	= $90cd
:Data_AliasSektor	= $90d7
:Data_AnzSLK		= $90c2
:Data_Anz_Fat		= $90b8
:Data_Anz_Files		= $90b9
:Data_Anz_Sektor	= $90bb
:Data_AreSek		= $90b6
:Data_Boot		= $90a8
:Data_BpSek		= $90b3
:Data_Disk_Typ		= $90ab
:Data_FstSek		= $90c4
:Data_Media		= $90bd
:Data_ParentSDir	= $90cf
:Data_SekFat		= $90be
:Data_SekSpr		= $90c0
:Data_SpClu		= $90b5
:Def1stDataSek		= $96fb
:Def1stRDirSek		= $971b
:DefAdrRootDir		= $975e
:DiskDrvMode		= $05
:DriveModeFlags		= $40
:DummyDiskName		= $90ea
:Flag_DirType		= $90cc
:Flag_UpdateDir		= $90e7
:Flag_UpdateDkDv	= $90e8
:GetClusterLink		= $96e6
:Inc_Sek		= $9799
:LoadAliasData		= $95a0
:Load_Reg_r0_r4		= $913b
:MaxDirPages		= $ff
:PART_MAX		= $00
:PART_TYPE		= $05
:RAM_AREA_ALIAS		= $4000
:RAM_AREA_BOOT		= $0200
:RAM_AREA_BUFFER	= $3e00
:RAM_AREA_DIR		= $2000
:RAM_AREA_FAT		= $0400
:RAM_AREA_SEKTOR	= $00
:ReadBlock_DOS		= $9097
:Save_Reg_r0_r4		= $914c
:Se_1stDataSek		= $00
:Se_1stDirSek		= $01
:Se_BootSektor		= $01
:Se_BorderBlock		= $01
:Se_DskNameSek		= $01
:SearchEntryFAT		= $96b0
:Seite			= $90a3
:Sektor			= $90a5
:SendFCom_CRC1		= $909d
:SendFCom_CRC2		= $90a0
:SetBOOT_Area		= $91a2
:SetDOS_Area		= $91b4
:Spur			= $90a4
:SwapBOOT_Buffer	= $9188
:SwapDOS_Buffer		= $917b
:SwapFAT_Buffer		= $9167

:SwapTMP_Buffer		= $9195
:Swap_ExtData		= $910e
:TMP_AREA_ALIAS		= $4000
:TMP_AREA_BOOT		= $8000
:TMP_AREA_BUFFER	= $8200
:TMP_AREA_DIR		= $4000
:TMP_AREA_FAT		= $4000
:TMP_AREA_SEKTOR	= $8000
:Tr_1stDataSek		= $20
:Tr_1stDirSek		= $01
:Tr_BootSektor		= $00
:Tr_BorderBlock		= $4f
:Tr_DskNameSek		= $00
:UpdateDriver		= $91bd
:WriteBlock_DOS		= $909a
:dir3Head		= $9b80
