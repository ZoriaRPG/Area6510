﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $36c7
:ReBoot_REU		= $3428
:ReBoot_RL		= $3192
:ReBoot_SCPU		= $2e00
:x_ClrDlgScreen		= $558e
:x_DoAlarm		= $3dca
:x_EnterDeskTop		= $3ab8
:x_GetFiles		= $3e24
:x_GetFilesData		= $50a0
:x_GetFilesIcon		= $521f
:x_GetNextDay		= $3d7c
:x_PanicBox		= $3cd0
:x_ToBASIC		= $3b25
