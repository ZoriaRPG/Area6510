﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!FD_41!FD_71!HD_41!HD_41_PP!HD_71!HD_71_PP
::tmp0b = RL_41!RL_71!RD_41!RD_71
::tmp0  = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** Ist Verzeichnis-Sektor frei ?
:IsDirSekFree		ldx	#$04			;Vorbereiten: "Verzeichnis voll".
			lda	curDirHead +$48		;Freie Verzeichnis-Sektoren testen.
			beq	:51
			ldx	#$00
::51			rts
endif

;******************************************************************************
::tmp1 = C_81!FD_81!HD_81!HD_81_PP!RL_81!RD_81
if :tmp1 = TRUE
;******************************************************************************
;*** Ist Verzeichnis-Sektor frei ?
:IsDirSekFree		ldx	#$04			;Vorbereiten: "Verzeichnis voll".
			lda	dir2Head +$fa		;Freie Verzeichnis-Sektoren testen.
			beq	:51
			ldx	#$00
::51			rts
endif
