﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:53			;Fehler? => Ja, Abbruch...

			jsr	IsSekInRAM_OK
			bcc	:53

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutInitByt
			jsr	xGetDiskError
			txa
			beq	:52

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:53			; => Ja, Abbruch...
			bcs	:51			;Sektor nochmal schreiben.

::52			bit	curType			;Shadow-Laufwerk?
			bvc	:53			; => Nein, weiter...
			jsr	SaveSekInRAM		;Shadow-RAM aktualisieren.

::53			rts
endif

;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:52			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...
			jsr	xVerWriteBlock		;Sektor vergleichen.
::51			jmp	DoneWithIO		;I/O abschalten.
::52			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:52			;Fehler? => Ja, Abbruch...

::51			jsr	Turbo_PutBlock
			jsr	Turbo_PutBytes
			jsr	GetReadError		;TurboDOS-Status einlesen.
			txa				;Fehler aufgetreten ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:52			; => Ja, Abbruch...
			bcs	:51			;Sektor nochmal schreiben.

::52			rts
endif

;******************************************************************************
::tmp2 = FD_41!FD_71!HD_41!HD_71
if :tmp2 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:52			;Fehler? => Ja, Abbruch...

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutBytes		;256 Byte an Floppy senden.

			jsr	GetReadError		;Diskettenfehler ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:52			; => Ja, Abbruch...
			bcs	:51			;Sektor nochmal schreiben.

::52			rts
endif

;******************************************************************************
::tmp3 = C_81!FD_81!FD_NM!HD_81!HD_NM!IEC_NM!S2I_NM
if :tmp3 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:53			;Fehler? => Ja, Abbruch...

			jsr	SwapDskNamData		;BAM nach 1581 Konvertieren.

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutBytes		;256 Byte an Floppy senden.

			jsr	GetReadError		;Diskettenfehler ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:52			; => Ja, Abbruch...
			bcs	:51			;Sektor nochmal schreiben.

::52			jsr	SwapDskNamData		;BAM zurückkonvertieren.

::53			rts
endif

;******************************************************************************
::tmp4 = RD_41!RD_71
if :tmp4 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.

			plp				;IRQ-Status zurücksetzen.
			txa
::51			rts
endif

;******************************************************************************
::tmp5 = RD_81!RD_NM!RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
if :tmp5 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	SwapDskNamData		;Diskettenname nach CBM/Standard.

			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.

			jsr	SwapDskNamData		;Diskettenname nach GEOS.

			plp				;IRQ-Status zurücksetzen.
			txa
::51			rts
endif

;******************************************************************************
::tmp6 = RL_41!RL_71
if :tmp6 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo
			jsr	InitForIO
			jsr	xWriteBlock
			jmp	DoneWithIO

:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			PushB	r3H			;Register ":r3H" zwischenspeichern.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.
			PopB	r3H			;Register ":r3H" zurücksetzen.

			plp				;IRQ-Status zurücksetzen.
			txa
::51			rts
endif

;******************************************************************************
::tmp7 = RL_81!RL_NM
if :tmp7 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo
			jsr	InitForIO
			jsr	xWriteBlock
			jmp	DoneWithIO

:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.

			jsr	SwapDskNamData		;Diskettenname nach CBM/Standard.

			PushB	r3H			;Register ":r3" retten.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.
			PopB	r3H			;Register ":r3" zurücksetzen.

			jsr	SwapDskNamData		;Diskettenname nach GEOS.

			plp				;IRQ-Status zurücksetzen.
			txa
::51			rts
endif

;******************************************************************************
::tmp8 = HD_81_PP!HD_NM_PP
if :tmp8 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			jsr	SwapDskNamData		;Diskname ab $A0 in BAM patchen.

			ldx	#$01			;TurboDOS-Befehl: Block schreiben.
			jsr	TurboRoutine1

			jsr	SwapDskNamData		;Diskname ab $A0 in BAM patchen.

			ldx	ErrorCode
::51			rts
endif

;******************************************************************************
::tmp9 = HD_41_PP!HD_71_PP
if :tmp9 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:51			;Fehler? => Ja, Abbruch...

			ldx	#$01			;TurboDOS-Befehl: Block schreiben.
			jsr	TurboRoutine1

			ldx	ErrorCode
::51			rts
endif
