﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = FD_41!FD_71!FD_81!FD_NM
if :tmp0 = TRUE
;******************************************************************************
;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xGetPTypeData		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xReadPTypeData
			jmp	DoneWithIO

;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xReadPTypeData		ldx	#> com_PartTabFD
			lda	#< com_PartTabFD
			ldy	#$06
			jsr	Job_Command
			txa
			bne	:53

			jsr	$ffae

			lda	#$00
			sta	STATUS
			lda	curDrive
			jsr	$ffb4
			lda	#$ff
			jsr	$ff96

			ldy	#$00
::51			jsr	$ffa5
			jsr	DefPTypeGEOS
			sta	(r4L),y
			iny
			cpy	#$20
			bne	:51

			jsr	$ffab

			ldy	#$20
			lda	#$00
::52			sta	(r4L),y
			iny
			bne	:52

			tax				;Flag: "Kein Fehler".
::53			rts

:com_PartTabFD		b "M-R",$00,$2a,$20

;*** Umwandeln der CMD-Partitionskennung nach GEOS-Format.
:DefPTypeGEOS		cmp	#$00
			beq	:51
			cmp	#$05
			bcs	:51
			sec
			sbc	#$01
			bne	:51
			lda	#$04
::51			rts
endif

;******************************************************************************
::tmp1 = HD_41!HD_71!HD_81!HD_NM!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp1 = TRUE
;******************************************************************************
;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xGetPTypeData		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xReadPTypeData
			jmp	DoneWithIO

;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xReadPTypeData		ldx	#> com_PartTabHD
			lda	#< com_PartTabHD
			ldy	#$06
			jsr	Job_Command
			txa
			bne	:52

			jsr	$ffae

			lda	#$00
			sta	STATUS
			lda	curDrive
			jsr	$ffb4
			lda	#$ff
			jsr	$ff96

			ldy	#$00
::51			jsr	$ffa5
			jsr	DefPTypeGEOS
			sta	(r4L),y
			iny
			cpy	#$ff
			bne	:51

			lda	#$00			;Letztes Byte in Puffer löschen.
			sta	(r4L),y			;(für Partitions-Nr. 255).

			jsr	$ffab

			ldx	#$00
::52			rts

:com_PartTabHD		b "M-R",$00,$82,$ff

;*** Umwandeln der CMD-Partitionskennung nach GEOS-Format.
:DefPTypeGEOS		cmp	#$00
			beq	:51
			cmp	#$05
			bcs	:51
			sec
			sbc	#$01
			bne	:51
			lda	#$04
::51			rts
endif

;******************************************************************************
::tmp2 = RL_41!RL_71!RL_81!RL_NM
if :tmp2 = TRUE
;******************************************************************************
;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xGetPTypeData		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xReadPTypeData
			jmp	DoneWithIO

;*** Partitionstypen einlesen.
;    Übergabe:		r4 = Zeiger auf Speicher für PartTyp.
;    Rückgabe:		   = 256 Bytes mit Partitionstypen für Partition 0-255.
:xReadPTypeData		PushB	r1L
			PushB	r1H
			PushB	r3L
			PushB	r3H
			PushW	r5
			lda	r4H
			sta	r5H
			pha
			lda	r4L
			sta	r5L
			pha

			LoadB	r1L,$01
			LoadW	r4 ,dir3Head

			LoadB	r3H,$ff
			LoadB	r3L,$00

::51			lsr
			lsr
			lsr
			sta	r1H
			jsr	xDsk_SekRead		;Verzeichnis-Sektor einlesen.

			ldx	#$00
::52			txa
			asl
			asl
			asl
			asl
			asl
			tay
			lda	dir3Head +2,y
			jsr	DefPTypeGEOS
			ldy	r3L
			sta	(r5L),y
			inc	r3L
			inx
			cpx	#$08
			bcc	:52

			lda	r3L
			cmp	#32
			bcc	:51

			tay
			lda	#$00
::53			sta	(r5L),y
			iny
			bne	:53

			PopW	r4
			PopW	r5
			PopB	r3H
			PopB	r3L
			PopB	r1H
			PopB	r1L

			ldx	#$00
::54			rts

;*** Umwandeln der CMD-Partitionskennung nach GEOS-Format.
:DefPTypeGEOS		cmp	#$00
			beq	:51
			cmp	#$05
			bcs	:51
			sec
			sbc	#$01
			bne	:51
			lda	#$04
::51			rts
endif
