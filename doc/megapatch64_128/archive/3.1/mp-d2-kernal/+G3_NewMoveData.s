﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Inline: Speicher verschieben.
:xi_MoveData		pla
			sta	returnAddress +0
			pla
			sta	returnAddress +1
			ldy	#$06
::51			lda	(returnAddress)   ,y
			sta	r0              -1,y
			dey
			bne	:51
			jsr	xMoveData
			jmp	Exit7ByteInline

;*** Speicherbereich veschieben.
:xMoveData		lda	r2L
			ora	r2H			;Anzahl Bytes = $0000 ?
			beq	:107			;Ja, -> Keine Funktion.

			ldx	#$06
::100			lda	r0L -1,x
			pha
			dex
			bne	:100

			lda	sysRAMFlg		;MoveData über REU ?
			bpl	:101			;Nein, weiter...

			lda	r2H			;Mehr als $38ff Bytes verschieben?
			cmp	#$38			;Abfrage da beim 128er im Bereich von
			beq	:1			;$3900 bis $78ff in der REU       >nein
			bcs	:101			;Bank 0 gespeichert wird          >ja
::1			lda	r0H			;Startbereich unter $0200 ?
			cmp	#$02
			bcc	:101			;>ja
			lda	r1H			;Zielbereich unter $0200 ?
			cmp	#$02
			bcc	:101			;>ja

			lda	r1H
			pha
			stx	r1H
			stx	r3L			;Speicherbereich aus RAM
			jsr	StashRAM		;in REU übertragen.
			pla
			sta	r0H
			lda	r1L
			sta	r0L			;Speicherbereich aus REU
			jsr	FetchRAM		;in RAM zurückschreiben.
			jmp	:106			;Ende ":MoveData".

::101			jsr	c128_MoveData		;MoveData in Bank0 C128!
							;dann weiter mit Register wiederherst.

;*** Ende ":MoveData", Register wiederherstellen.
::106			ldx	#$00
::106a			pla
			sta	r0L,x
			inx
			cpx	#$06
			bcc	:106a
::107			rts
