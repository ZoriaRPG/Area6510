﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "obj.BuildID"

			c "BuildID     V1.0"
			a "Markus Kanet"
			h "Datum und Revision für das aktuelle MegaPatch-Build"
			f $04

			o $0400

;*** Versions-Nummer angeben.
			b "V3.1"			;Nur Großbuchstaben verwenden!
			b "-"

;*** Automatische Datumsangabe für Build-Info.
			k				;Aktuelles Datum.

;*** Feste Datumsangabe für Build-Info.
;			b "150618"			;Festes Datum vorgeben.
;			b "."
;			x				;Uhrzeit

