﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = RL_NM!RL_81
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR
			bcc	:51

			jsr	SwapDskNamData		;Diskettenname nach CBM/Standard.
			PushB	r3H			;Register ":r3" retten.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekVerify		;Sektor auf Diskette vergleichen.
			PopB	r3H			;Register ":r3" zurücksetzen.
			jsr	SwapDskNamData		;Diskettenname nach GEOS.

::51			plp				;IRQ-Status zurücksetzen.
			rts
endif

;******************************************************************************
::tmp1 = RD_NM!RD_NM_SCPU!RD_81
if :tmp1 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR
			bcc	:51

			jsr	xDsk_SekVerify		;Sektor auf Diskette vergleichen.

::51			plp				;IRQ-Status zurücksetzen.
			rts
endif

;******************************************************************************
::tmp2 = RL_71!RL_41
if :tmp2 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Sektor-Transfer initialisieren.
			bcc	:51			;Fehler, Abburch...

			PushB	r3H			;Register ":r3" retten.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekVerify		;Sektor auf Diskette vergleichen.
			PopB	r3H			;Register ":r3" zurücksetzen.

::51			plp				;IRQ-Status zurücksetzen.
			rts
endif

;******************************************************************************
::tmp3 = RD_71!RD_41
if :tmp3 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Sektor-Transfer initialisieren.
			bcc	:51			;Fehler, Abburch...

			jsr	xDsk_SekVerify		;Sektor auf Diskette vergleichen.

::51			plp				;IRQ-Status zurücksetzen.
			rts
endif

;******************************************************************************
::tmp4 = C_41
if :tmp4 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		jsr	IsSekInRAM_OK		;Sektor in ShadowRAM gespeichert ?
			bcc	:54

::51			lda	#$03
			sta	RepeatVerify
::52			ldx	#> TD_VerSekData
			lda	#< TD_VerSekData
			jsr	xTurboRoutSet_r1
			jsr	xGetDiskError
			txa
			beq	:53

			dec	RepeatVerify
			bne	:52

			ldx	#$25
			inc	RepeatFunction
			lda	RepeatFunction
			cmp	#$05
			beq	:53
			pha
			jsr	WriteBlock
			pla
			sta	RepeatFunction
			txa
			beq	:51

::53			txa
			bne	:54
			bit	curType
			bvc	:54
			jmp	SaveSekInRAM
::54			rts

:RepeatVerify		b $00
endif

;******************************************************************************
::tmp5 = C_71
if :tmp5 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:54			;Nein, Abbruch...

			ldx	#$00
::51			lda	#$03
			sta	RepeatVerify

::52			jsr	Turbo_GetBlock
			sty	$8d

			lda	#$51			;Assembler-Befehl definieren:
			sta	Def_AssCode1		; EOR ($8D),Y
			lda	#$85			;Assembler-Befehl definieren:
			sta	Def_AssCode2		; STA $8D

			jsr	Turbo_GetBytes

			lda	#$91			;Assembler-Befehl definieren:
			sta	Def_AssCode1		; STA ($8D),Y
			lda	#$05			;Assembler-Befehl definieren:
			sta	Def_AssCode2		; ORA $8D

			lda	$8d
			pha
			jsr	GetReadError
			pla
			cpx	#$00
			bne	:53
			tax
			beq	:54
			ldx	#$25
::53			dec	RepeatVerify
			bne	:52
			inc	RepeatFunction		;Wiederholungszähler setzen.
			lda	RepeatFunction
			cmp	#$05			;Alle Versuche fehlgeschlagen ?
			beq	:54			;Ja, Abbruch...
			pha
			jsr	xWriteBlock		;Sektor nochmals schreiben.
			pla				;Wiederholungszähler zurücksetzen.
			sta	RepeatFunction
			txa				;Wurde Sektor korrekt gespeichert ?
			beq	:51			;Ja, Sektor nochmals vergleichen.
::54			rts

:RepeatVerify		b $00
endif

;******************************************************************************
::tmp6a = C_81!FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
::tmp6b = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp6  = :tmp6a!:tmp6b
if :tmp6 = TRUE
;******************************************************************************
;*** Sektor auf Diskette vergleichen.
;    Aus Platzgründen bei 1581/CMD nicht implementiert.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xVerWriteBlock		ldx	#$00
			rts
endif
