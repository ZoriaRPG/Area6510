﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS senden.
;    Übergabe:		$8b,$8c = Zeiger auf Datenspeicher.
;			yReg    = Anzahl Bytes (0=256 Bytes).
:Turbo_PutInitByt	jsr	DataClkATN_HIGH		;DATA_OUT, CLOCK_OUT, ATN-Signal
							;löschen, warten auf DATA_IN = LOW.

			tya				;Anzal Bytes in AKKU übertragen und
			pha				;zwischenspeichern.
			ldy	#$00			;Anzahl folgender Bytes an TurboDOS
			jsr	Turbo_SendByte		;in Floppy-RAM senden.
			pla
			tay				;Anzahl Bytes zurücksetzen.

;*** Bytes an Floppy senden.
;    Übergabe:		$8b,$8c = Zeiger auf Daten.
;			yReg    = Anzahl Bytes.
:Turbo_PutBytes		jsr	DataClkATN_HIGH
:Turbo_PutNxByte	dey				;Zeiger auf Daten korrigieren.
			lda	($8b),y			;Byte einlesen.

			ldx	$8e			;TurboDOS-Übertragung starten.
			stx	$dd00

;*** Byte an Floppy senden.
:Turbo_SendByte		tax				;LOW-Nibble für Übertragung
			and	#$0f			;berechnen und speichern.
			sta	$8d

::51			sec				;Warteschleife.
			lda	$d012			;Daten dürfen erst gesendet werden,
			sbc	#$31			;wenn der VDC seine Daten auf dem
			bcc	:52			;Bildschirm aufgebaut hat!
			and	#$06
			beq	:51

::52			txa

			ldx	$8f			;Startzeichen an TurboRoutine in
			stx	$dd00			;FloppyRAM übergeben.

			and	#$f0			;HIGH-Nibble für Übertragung
			ora	$8e			;berechnen.
			sta	$dd00			;Bit #5 und #4 senden.
			ror
			ror
			and	#$f0
			ora	$8e
			sta	$dd00			;Bit #7 und Bit #6 senden.

			ldx	$8d			;LOW-Nibble senden.
			lda	NibbleByteH,x
			ora	$8e
			sta	$dd00
			ror
			ror
			and	#$f0
			ora	$8e
			cpy	#$00
			sta	$dd00
			bne	Turbo_PutNxByte
			nop
			nop
			beq	CLOCK_OUT_LOW
endif

;******************************************************************************
::tmp1a = C_71!C_81!FD_41!FD_71!FD_81!FD_NM!PC_DOS
::tmp1b = HD_41!HD_71!HD_81!HD_NM
::tmp1  = :tmp1a!:tmp1b
if :tmp1 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS senden.
;    Übergabe:		$8b,$8c = Zeiger auf Datenspeicher.
;			yReg    = Anzahl Bytes (0=256 Bytes).
:Turbo_PutInitByt	jsr	DataClkATN_HIGH		;DATA_OUT, CLOCK_OUT, ATN-Signal
							;löschen, warten auf DATA_IN = LOW.

			tya				;Anzal Bytes in AKKU übertragen und
			pha				;zwischenspeichern.
			ldy	#$00			;Anzahl folgender Bytes an TurboDOS
			jsr	Turbo_SendByte		;in Floppy-RAM senden.
			pla
			tay				;Anzahl Bytes zurücksetzen.

;*** Bytes an Floppy senden.
;    Übergabe:		$8b,$8c = Zeiger auf Daten.
;			yReg    = Anzahl Bytes.
:Turbo_PutBytes		jsr	DataClkATN_HIGH
:Turbo_PutNxByte	dey				;Zeiger auf Daten korrigieren.
			lda	($8b),y			;Byte einlesen.

			ldx	$8e			;TurboDOS-Übertragung starten.
			stx	$dd00

;*** Byte an Floppy senden.
:Turbo_SendByte		tax				;LOW-Nibble für Übertragung
			and	#$0f			;berechnen und speichern.
			sta	$8d

::51			sec				;Warteschleife.
			lda	$d012			;Daten dürfen erst gesendet werden,
			sbc	#$31			;wenn der VDC seine Daten auf dem
			bcc	:52			;Bildschirm aufgebaut hat!
			and	#$06
			beq	:51

::52			txa

			ldx	$8f			;Startzeichen an TurboRoutine in
			stx	$dd00			;FloppyRAM übergeben.

			and	#$f0			;HIGH-Nibble für Übertragung
			ora	$8e			;berechnen.
			sta	$dd00			;Bit #5 und #4 senden.
			ror
			ror
			and	#$f0
			ora	$8e
			sta	$dd00			;Bit #7 und Bit #6 senden.

			ldx	$8d			;LOW-Nibble senden.
			lda	NibbleByteH,x
			sta	$dd00
			lda	NibbleByteL,x
			cpy	#$00
			sta	$dd00
			bne	Turbo_PutNxByte
			beq	CLOCK_OUT_LOW
endif

;******************************************************************************
::tmp2 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp2 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS senden.
;    Übergabe:		$8b,$8c = Zeiger auf Datenspeicher.
;			yReg    = Anzahl Bytes (0=256 Bytes).
;*** Daten über HD-Kabel senden.
:Turbo_PutBytes		jsr	HD_MODE_RECEIVE
			jsr	DataClkATN_HIGH

::0			dey				;Datenbyte einlesen und über
			lda	($8b),y			;HD-Kabel senden.
			sta	$df40

			jsr	WAIT_DATA

;			ldx	$8f			;Warten bis Byte von HD
;			stx	$dd00			;empfangen wurde.
;::1			bit	$dd00
;			bmi	:1

			tya				;Alle Bytes gesendet?
			beq	END_OF_DATA		; => Ja, Ende...

			dey				;Datenbyte einlesen und über
			lda	($8b),y			;HD-Kabel senden.
			sta	$df40

			ldx	$8e			;Warten bis Byte von HD
			stx	$dd00			;empfangen wurde.
::2			bit	$dd00
			bpl	:2

			tya				;Alle Bytes gesendet?
			bne	:0			; => Nein, weiter...

:WAIT_DATA		ldx	$8f			;Warten bis Übertragung beendet.
			stx	$dd00
::3			bit	$dd00
			bmi	:3
:END_OF_DATA		rts

endif
