﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#128"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

			t "-DD_JumpTab"

;--- Ergänzung: 08.07.18/M.Kanet
;Adressen für Prüfung der SuperCPU-Version.
if Flag64_128 = TRUE_C64
:SCPU_VCODE1 = $e487
:SCPU_VCODE2 = $e488
:SCPU_VCODE3 = $e489
endif
if Flag64_128 = TRUE_C128
:SCPU_VCODE1 = $f6dd
:SCPU_VCODE2 = $f6de
:SCPU_VCODE3 = $f6df
endif

;*** Prüfen ob Laufwerk installiert werden kann.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		php				;IRQ sperren.
			sei

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			ldy	CPU_DATA		;I/O-Bereich und ROM aktivieren.
			lda	#$37
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldy	MMU			;I/O-Bereich und ROM aktivieren.
			lda	#$4e
			sta	MMU
endif

			lda	$d0bc
			cmp	#$ff			;SuperCPU verügbar ?
			beq	:53			; => Nein, Ende...

			lda	SCPU_VCODE2		;CPU-ROM testen.
			cmp	#"."			;RAMCard erst ab ROM V1.40
			bne	:53			;verfügbar. Ältere Versionen der
			lda	SCPU_VCODE1		;SCPU unterstützen keine RAMCard!
			cmp	#"1"
			bcc	:53
			beq	:51
			bcs	:52
::51			lda	SCPU_VCODE3
			cmp	#"4"
			bcc	:53

::52			ldx	#NO_ERROR
			b $2c
::53			ldx	#DEV_NOT_FOUND

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			sty	CPU_DATA		;I/O-Bereich und ROM ausblenden.
endif
if Flag64_128 = TRUE_C128
			sty	MMU			;I/O-Bereich und ROM ausblenden.
endif

			plp
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			lda	DriveAdr
			clc
			adc	#$39
			sta	BAM_NM +7
			sta	DRIVE_NAME +3

;--- SuperCPU/RAMCard suchen.
			jsr	xTestDriveMode		;SuperCPU/RAMCard suchen.
			txa				;Beides installiert?
			bne	:2			; => Nein, Ende...

;--- Installierten SRAM-Native Treivber suchen.
			ldx	#03
::1			lda	RealDrvType,x
			cmp	DriveMode		;Laufwerk bereits installiert?
			bne	:3			; => Nein, weiter...

			LoadW	r0,Dlg_ActivSRAM	;Hinweis: Nur ein SRAM-Laufwerk
			jsr	DoDlgBox		;kann verwendet werden!
			ldx	#DEV_NOT_FOUND
::2			rts

::3			dex				;Alle Laufwerke durchsucht?
			bpl	:1			; => Nein, weiter...

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- Verfügbares SuperRAM ermitteln.
			php				;IRQ sperren.
			sei

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			ldx	CPU_DATA		;I/O-Bereich aktivieren.
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldx	MMU			;I/O-Bereich aktivieren.
			lda	#$7e
			sta	MMU
endif

			ldy	$d27d
			lda	$d27e
			beq	:11
			iny
::11			sty	MinFreeSRAM
			lda	$d27f
			sta	MaxFreeSRAM
			sec
			sbc	MinFreeSRAM
			sta	MaxSizeSRAM

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O-Bereich ausblenden.
endif
if Flag64_128 = TRUE_C128
			stx	MMU			;I/O-Bereich ausblenden.
endif

			plp				;IRQ-Status zurücksetzen.

			cmp	#$02			;SuperRAM verfügbar?
			bcs	:21			; => Ja, weiter...
			ldx	#NO_FREE_RAM
			rts

;--- Größe des Laufwerks bestimmen.
::21			jsr	GetCurPartSize		;Falls Laufwerk schon einmal
							;installiert, Größe übernehmen.
			jsr	GetPartSize		;Größe festlegen.
			txa				;Abbruch der Installation?
			beq	:31			; => Nein, weiter...
			ldx	#DEV_NOT_FOUND
			rts

;--- Installation fortsetzen.
::31			ldx	DriveAdr		;Startadresse RAM-Speicher in
			lda	MinFreeSRAM		;SuperCPU zwischenspeichern.
			sta	ramBase     -8,x
			lda	DriveMode
			sta	RealDrvType -8,x
			and	#%10111111
			sta	driveType   -8,x
			sta	curType
			lda	#SET_MODE_SUBDIR!SET_MODE_FASTDISK
			sta	RealDrvMode-8,x

;--- SuperRAM als belegt markieren.
			php				;IRQ sperren.
			sei

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			ldx	CPU_DATA		;I/O-Bereich aktivieren.
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldx	MMU			;I/O-Bereich aktivieren.
			lda	#$7e
			sta	MMU
endif

			sta	$d07e			;SuperCPU-Register aktivieren.

			ldy	DriveAdr		;Größe des freien Speichers in
			lda	ramBase   -8,y		;der SuperCPU korrigieren.
			clc
			adc	SetSizeSRAM
			sta	$d27d
			lda	#$00
			sta	$d27e

			sta	$d07f			;SuperCPU-Register abschalten.

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O-Bereich ausblenden.
endif
if Flag64_128 = TRUE_C128
			stx	MMU			;I/O-Bereich ausblenden.
endif

			plp				;IRQ-Status zurücksetzen.

;--- BAM erstellen.
::41			jsr	CreateBAM		;BAM erstellen.
			txa				;Installationsfehler ?
			bne	:43			; => Ja, Abbruch...

			lda	DriveAdr		;Laufwerk aktivieren.
			jsr	SetDevice
			jsr	OpenDisk
			txa
			bne	:43

			ldx	#r0L
			jsr	GetPtrCurDkNm

			ldy	#$00
			lda	(r0L),y			;Diskettenname gültig ?
			bne	:44			; => Ja, weiter...

			ldy	#$00			;BAM Teil #1 definieren.
::42			lda	BAM_NM    ,y
			sta	curDirHead,y
			iny
			cpy	#$c0
			bcc	:42

			jsr	PutDirHead
			txa
			beq	:44

;--- Fehler beim erstellen der BAM, Laufwerk nicht installiert.
::43			lda	MinFreeSRAM
			jsr	DeInstallDrvData
			ldx	#DEV_NOT_FOUND
			rts

;--- Laufwerk installiert, Ende...
::44			ldx	#NO_ERROR
			rts

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	DeInstallDrvData	; => Ja, Laufwerk initialisieren.

			inx
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf

			ldx	curDrive
			lda	ramBase     -8,x	;RAM-Speicher in der REU wieder

:DeInstallDrvData	php				;IRQ sperren.
			sei

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			ldx	CPU_DATA		;I/O-Bereich aktivieren.
			ldy	#$35
			sty	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			ldx	MMU			;I/O-Bereich aktivieren.
			ldy	#$7e
			sty	MMU
endif

			sta	$d07e			;SuperCPU-Register aktivieren.
			sta	$d27d			;Freien Speicher zurücksetzen.
			sta	$d07f			;SuperCPU-Register abschalten.

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;I/O-Bereich ausblenden.
endif
if Flag64_128 = TRUE_C128
			stx	MMU			;I/O-Bereich ausblenden.
endif

			plp				;IRQ-Status zurücksetzen.

			ldx	curDrive
			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode -8,x
			tax
			rts

;*** Partitionsgröße einstellen.
:GetPartSize		lda	MaxSizeSRAM		;Speicher verfügbar?
			bne	:52			; => Ja, weiter...
::51			ldx	#DEV_NOT_FOUND		; => Nein, Ende...
			rts

::52			ldx	SetSizeSRAM		;Größe bereits festgelegt ?
			bne	:54			; => Ja, weiter.
::53			sta	SetSizeSRAM		;Max. Größe vorbelegen.

::54			cmp	SetSizeSRAM		;Max. Größe überschritten ?
			bcs	:54a			; => Nein, weiter...
			sta	SetSizeSRAM		; => Ja, Größe zurücksetzen...

::54a			bit	firstBoot		;GEOS-BootUp ?
			bmi	:55			; => Nein, weiter...

			lda	SetSizeSRAM		;Startadresse RAM-Speicher in
			cmp	MaxSizeSRAM		;Größe bereits festgelegt ?
			bcc	:57			; => Ja, weiter...
			beq	:57

;--- Partitionsgröße einstellen.
::55			LoadW	r0,Dlg_GetSize		;Partitionsgröße wählen.
			jsr	DoDlgBox

			lda	sysDBData
			cmp	#CANCEL			;Auswahl abgebrochen ?
			beq	:51			; => Ja, Ende...

::56			lda	DriveMode
			ldx	#< DSK_INIT_SIZE
			stx	r2L
			ldx	#> DSK_INIT_SIZE
			stx	r2H
			jsr	SaveDskDrvData
			lda	DriveAdr		;Laufwerk zurücksetzen.
			jsr	SetDevice
::57			ldx	#NO_ERROR
			rts

;*** Größe der aktiven Partition einlesen.
:GetCurPartSize		bit	firstBoot
			bmi	:51

			lda	SetSizeSRAM		;Partitionsgröße definiert ?
			bne	:51			; => Ja, Ende...

			jsr	CheckDiskBAM		;BAM überprüfen.
			txa				;Ist BAM gespeichert?
			bne	:51			; => Nein, Ende...

			jsr	GetSekPartSize		;Größe des installierten
			txa				;Laufwerks ermitteln.
			bne	:51

			tya
							;Partitionsgröße speichern.
			sta	SetSizeSRAM
::51			rts

;*** Mehr RAM.
:Add64K			php				;Mausabfrage.
			sei
			lda	#$50
			jsr	SetIconArea
			jsr	IsMseInRegion
			plp
			tax
			beq	Sub64K

			ldy	SetSizeSRAM
			cpy	MaxSizeSRAM		;Weiterer Speicher verfügbar?
			bcc	:51			; => Ja, weiter...
			ldx	#NO_FREE_RAM
			rts

::51			iny				;Neue Größe festlegen.
			sty	SetSizeSRAM

			lda	#$50
			jsr	PrntNewSize

			bit	mouseData
			bpl	Add64K
			ClrB	pressFlag
			rts

;*** Weniger RAM.
:Sub64K			php				;Mausabfrage.
			sei
			lda	#$58
			jsr	SetIconArea
			jsr	IsMseInRegion
			plp
			tax
			beq	:54

			ldy	SetSizeSRAM
			dey				;Speicher weiter reduzierbar?
			bne	:51			; => Ja, weiter...
			ldx	#NO_FREE_RAM
			rts

::51			sty	SetSizeSRAM		;Neue Größe festlegen.

			lda	#$58
			jsr	PrntNewSize

			bit	mouseData
			bpl	Sub64K
			ClrB	pressFlag
::54			rts

;*** Grenzen für Mausabfrage festlegen.
:SetIconArea		sta	r2L
			clc
			adc	#$07
			sta	r2H
			LoadW	r3 ,($12*8   ) ! DOUBLE_W
			LoadW	r4 ,($12*8+15) ! DOUBLE_W ! ADD1_W
			rts

;*** Button invertieren und Laufwerksgröße ausgeben.
:PrntNewSize		pha
			jsr	SetIconArea
			jsr	InvertRectangle
			jsr	PrntCurSize
			jsr	SCPU_Pause
			pla
			jsr	SetIconArea
			jmp	InvertRectangle

;*** Laufwerksgröße ausgeben.
:PrntCurSize		LoadW	r0,Dlg_Graphics2
			jsr	GraphicsString

			lda	SetSizeSRAM		;Anzahl Tracks in freien
			sta	r0L			;Speicher umrechnen.
			lda	#$40
			sta	r1L
			ldx	#r0L
			ldy	#r1L
			jsr	BBMult

			SubVW	16,r0			;16K für Verzeichnis abziehen.

			LoadB	r1H,$5b
			LoadW	r11,$0063 ! DOUBLE_W
			lda	#%11000000
			jmp	PutDecimal

;*** Ist BAM und Partitionsgröße gültig ?
:TestCurBAM		jsr	CheckDiskBAM
			txa				;Gültige BAM verfügbar?
			bne	:51			; => Nein, Ende...

			jsr	GetSekPartSize		;Größe des Laufwerks einlesen.
			txa				;Diskettenfehler?
			bne	:51			; => Ja, Abbruch...

			cpy	SetSizeSRAM		;Partitionsgröße unverändert ?
			bne	:51			; => Nein, Ende...

			ldx	#NO_ERROR
			rts
::51			ldx	#BAD_BAM
			rts

;*** Ist BAM gültige BAM vorhanden ?
:CheckDiskBAM		jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:52			; => Ja, Laufwerk initialisieren.

			LoadW	r0,curDirHead +$ad
			LoadW	r1,DrvFormatCode
			ldx	#r0L
			ldy	#r1L			;Auf GEOS-Kennung
			lda	#12			;"GEOS-format" testen.
			jsr	CmpFString		;Kennung vorhanden ?
			bne	:52			; => Nein, Directory löschen.

::51			ldx	#NO_ERROR
			rts
::52			ldx	#BAD_BAM
			rts

;*** Sektor mit Laufwerksgröße einlesen.
:GetSekPartSize		ldx	#$01
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf
			txa
			bne	:51

			ldy	diskBlkBuf +8
::51			rts

;*** Sektorspeicher löschen.
:ClrDiskSekBuf		ldy	#$00
			tya
::51			sta	diskBlkBuf,y
			dey
			bne	:51
			rts

;*** Routine zum schreiben von Sektoren.
;    StashRAM kann nicht verwendet werden, da evtl. GEOS-DACC nicht im
;    SuperRAM gespeichert ist => Falsche RAM-Treiber!
:WriteSektor		php
			sei

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			lda	MMU			;I/O-Bereich aktivieren.
			pha
			lda	#$7e
			sta	MMU
endif

			PushW	r1

			dec	r1L
			lda	r1H
			clc
			adc	#$00
			sta	r1H
			lda	r1L
			ldx	curDrive
			adc	ramBase -8,x
			sta	r3L
			lda	#$00
			sta	r1L

			LoadW	r0,diskBlkBuf
			LoadW	r2,$0100

			lda	r3L			;Speicherbank berechnen.
			sta	:51 +1

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$02			;ldx $0002
			b $a4,$04			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			ldx	#%01000000		;Kein Fehler...

			PopW	r1

			pla				;I/O-Bereiche deaktivieren.

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Beim C128 das Register MMU verwenden.
if Flag64_128 = TRUE_C64
			sta	CPU_DATA
endif
if Flag64_128 = TRUE_C128
			sta	MMU
endif

			plp

			txa				;Zurück zum Programm.
			ldx	#NO_ERROR
			rts

;*** Neue BAM erstellen.
:ClearCurBAM		lda	SetSizeSRAM		;Partitionsgröße festlegen.
			sta	BAM_NMa   +8

			ldy	#$00			;BAM Teil #1 definieren.
::51			lda	BAM_NM    ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$c0
			bcc	:51

			lda	#$00
::52			sta	diskBlkBuf,y
			iny
			bne	:52

			lda	#$01			;BAM Teil #1 speichern.
			sta	r1L
			sta	r1H
			jsr	WriteSektor
			txa
			bne	:58

			ldy	#$00			;BAM Teil #1 definieren.
::53			lda	BAM_NMa   ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$40
			bcc	:53

			lda	#$ff
::54			sta	diskBlkBuf,y
			iny
			bne	:54

			inc	r1H			;BAM Teil #2 speichern.
			jsr	WriteSektor
			txa
			bne	:58

			ldy	#$00			;BAM Teil #3 definieren.
			lda	#$ff
::55			sta	diskBlkBuf,y
			iny
			bne	:55

::56			inc	r1H
			CmpBI	r1H,$22
			bcs	:57

			jsr	WriteSektor
			txa
			bne	:58
			beq	:56

;--- Ersten Verzeichnissektor löschen.
::57			jsr	OpenDisk		;BAM einlesen und Größe der Native-
			txa				;Partition innerhalb des Treibers
			bne	:58			;festlegen.

			jsr	ClrDiskSekBuf		;Sektorspeicher löschen.

			lda	#$ff			;Ersten Verzeichnissektor löschen.
			sta	diskBlkBuf +$01
			lda	#$01
			sta	r1L
			lda	#$22
			sta	r1H
			jsr	WriteSektor
			txa
			bne	:58

			lda	#$ff			;Sektor $28/$27 löschen.
			sta	r1H			;Ist Borderblock für DeskTop 2.0!
			jsr	WriteSektor
::58			rts

;*** BAM für RAMNative-Laufwerk.
:BAM_NM			b $01,$22,$48,$00,$52,$41,$4d,$20
			b $4e,$61,$74,$69,$76,$65,$a0,$a0
			b $a0,$a0,$a0,$a0,$a0,$a0,$52,$4c
			b $a0,$31,$48,$a0,$a0,$00,$00,$00
			b $01,$01,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
:DRIVE_NAME		b $52,$41,$4d,$20,$4e,$61,$74,$69
			b $76,$65,$a0,$a0,$a0,$a0,$a0,$a0
			b $a0,$a0,$52,$4c,$a0,$31,$48,$a0
			b $a0,$a0,$a0,$01,$ff,$47,$45,$4f;$01/$ff Borderblock!
			b $53,$20,$66,$6f,$72,$6d,$61,$74
			b $20,$56,$31,$2e,$30,$00,$00,$00

:BAM_NMa		b $00,$00,$48,$b7,$52,$44,$c0,$00
			b $02,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$1f,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$fe

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000
:DrvFormatCode		b "GEOS format "
:MinFreeSRAM		b $00
:MaxFreeSRAM		b $00
:MaxSizeSRAM		b $00
:SetSizeSRAM		b $00

;*** Titelzeile für Dialogbox.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation SuperRAM Native",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation SuperRAM Native",NULL
endif

;*** Dialogbox.
:Dlg_GetSize		b %10000001
			b DBGRPHSTR
			w Dlg_Graphics1
			b DBTXTSTR   ,$10,$0b
			w DlgBoxTitle
			b DBTXTSTR   ,$10,$20
			w :53
			b DBTXTSTR   ,$68,$3b
			w :54
			b DB_USR_ROUT
			w PrntCurSize
			b DBOPVEC
			w Add64K
			b DB_USR_ROUT
			w Dlg_DrawIcons
			b OK         ,$02,$48
			b CANCEL     ,$10,$48
			b NULL

if Sprache = Deutsch
::53			b PLAINTEXT,BOLDON
			b "Partitionsgröße wählen:",NULL
::54			b "KBytes",NULL
endif

if Sprache = Englisch
::53			b PLAINTEXT,BOLDON
			b "Select partition-size:",NULL
::54			b "KBytes",NULL
endif

:Dlg_DrawIcons		jsr	i_BitmapUp
			w	Icon01
			b	$12 ! DOUBLE_B
			b	$50
			b	Icon01x ! DOUBLE_B
			b	Icon01y

			lda	C_DBoxDIcon
			jsr	i_UserColor
			b	$12 ! DOUBLE_B
			b	$50/8
			b	Icon01x ! DOUBLE_B
			b	Icon01y/8
			rts

:Dlg_Graphics1		b NEWPATTERN,$01
			b MOVEPENTO
			w $0040 ! DOUBLE_W
			b $20
			b RECTANGLETO
			w $00ff ! DOUBLE_W
			b $2f
			b MOVEPENTO
			w $0060 ! DOUBLE_W
			b $50
			b FRAME_RECTO
			w $008f ! DOUBLE_W
			b $5f

:Dlg_Graphics2		b NEWPATTERN,$00
			b MOVEPENTO
			w $0061 ! DOUBLE_W
			b $51
			b RECTANGLETO
			w $008e ! DOUBLE_W
			b $5e
			b NULL

:Dlg_ActivSRAM		b %11100001
			b DBGRPHSTR
			w Dlg_DrawBoxTitel
			b DBTXTSTR ,$10,$0b
			w DlgBoxTitle
			b DBTXTSTR ,$10,$20
			w :51
			b DBTXTSTR ,$10,$2a
			w :52
			b OK       ,$10,$48
			b NULL

if Sprache = Deutsch
::51			b PLAINTEXT,BOLDON
			b "Ein SuperRAM-Laufwerk",NULL
::52			b "ist bereits installiert!",NULL
endif

if Sprache = Englisch
::51			b PLAINTEXT,BOLDON
			b "A SuperRAM-drive is",NULL
::52			b "allready installed!",NULL
endif

;*** Icons
:Icon01
<MISSING_IMAGE_DATA>

:Icon01x		= .x
:Icon01y		= .y

;******************************************************************************
			t "-DD_AskClrBAM"
;******************************************************************************

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
