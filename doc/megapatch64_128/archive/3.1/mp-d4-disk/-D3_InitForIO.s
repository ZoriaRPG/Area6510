﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = Flag64_128!C_41
if :tmp0 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei
			lda	CPU_DATA
			sta	CPU_RegBuf		;CPU-Status speichern.
			lda	#$36			;I/O + Kernal einblenden.
			sta	CPU_DATA

			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			ldy	#$00
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			lda	mobenble		;Aktive Sprites zwischenspeichern.
			sta	mobenble_Buf
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::51			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:51
			sta	$8f
			dey
			bne	:51

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#$10
			sta	CLOCK_OUT_LOW +1
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
endif

;******************************************************************************
::tmp1 = Flag64_128!C_41
if :tmp1 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE			;Takt-Frequenz speichern.
			sta	CLKRATE_Buf
			ldy	#$00
			sty	CLKRATE
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::51			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:51
			sta	$8f
			dey
			bne	:51

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#$10
			sta	CLOCK_OUT_LOW +1
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
endif

;******************************************************************************
::tmp2 = Flag64_128!C_71!C_81!FD_41!FD_71!FD_81!PC_DOS!HD_41!HD_71!HD_81
if :tmp2 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei
			lda	CPU_DATA
			sta	CPU_RegBuf		;CPU-Status speichern.
			lda	#$36			;I/O + Kernal einblenden.
			sta	CPU_DATA

			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			ldy	#$00
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			lda	mobenble		;Aktive Sprites zwischenspeichern.
			sta	mobenble_Buf
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::51			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:51
			sta	$8f
			dey
			bne	:51

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#%00010000
			sta	CLOCK_OUT_LOW +1

			ldy	#$1f
::52			lda	NibbleByteH,y
			and	#%11110000
			ora	$8e
			sta	NibbleByteH,y
			dey
			bpl	:52
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
endif

;******************************************************************************
::tmp3 = Flag64_128!C_71!C_81!FD_41!FD_71!FD_81!PC_DOS!HD_41!HD_71!HD_81
if :tmp3 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE
			sta	CLKRATE_Buf
			ldy	#$00
			sty	CLKRATE
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::51			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:51
			sta	$8f
			dey
			bne	:51

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#%00010000
			sta	CLOCK_OUT_LOW +1

			ldy	#$1f
::52			lda	NibbleByteH,y
			and	#%11110000
			ora	$8e
			sta	NibbleByteH,y
			dey
			bpl	:52
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
endif

;******************************************************************************
::tmp4 = Flag64_128!FD_NM!HD_NM
if :tmp4 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		bit	IO_Activ		;I/O-Modus bereits aktiv ?
			bpl	:51			; => Nein, weiter...
			rts				;Ende...

::51			dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei
			lda	CPU_DATA
			sta	CPU_RegBuf		;CPU-Status speichern.
			lda	#$36			;I/O + Kernal einblenden.
			sta	CPU_DATA

			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			ldy	#$00
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			lda	mobenble		;Aktive Sprites zwischenspeichern.
			sta	mobenble_Buf
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::52			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:52
			sta	$8f
			dey
			bne	:52

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#%00010000
			sta	CLOCK_OUT_LOW +1

			ldy	#$1f
::53			lda	NibbleByteH,y
			and	#%11110000
			ora	$8e
			sta	NibbleByteH,y
			dey
			bpl	:53
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:IO_Activ		b $00
endif

;******************************************************************************
::tmp5 = Flag64_128!FD_NM!HD_NM
if :tmp5 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		bit	IO_Activ		;I/O-Modus bereits aktiv ?
			bpl	:51			; => Nein, weiter...
			rts				;Ende...

::51			dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE
			sta	CLKRATE_Buf
			ldy	#$00
			sty	CLKRATE
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e

			ldy	#$2c			;Warteschleife bis Ser. Bus
::52			lda	rasreg			;initialisiert (Turbo-Routinen!)
			cmp	$8f
			beq	:52
			sta	$8f
			dey
			bne	:52

			lda	$dd00
			and	#%00000111		;Byte zum aktivieren des Turbo-
			sta	$8e			;Modus ermitteln.
			ora	#%00110000		;Byte zum abschalten des Turbo-
			sta	$8f			;Modus ermitteln.
			lda	$8e
			ora	#%00010000
			sta	CLOCK_OUT_LOW +1

			ldy	#$1f
::53			lda	NibbleByteH,y
			and	#%11110000
			ora	$8e
			sta	NibbleByteH,y
			dey
			bpl	:53
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
:IO_Activ		b $00
endif

;******************************************************************************
::tmp6 = Flag64_128!RL_41!RL_71!RL_81!RD_41!RD_71!RD_81
if :tmp6 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.

			sei
			lda	CPU_DATA
			sta	CPU_RegBuf		;CPU-Status speichern.

			lda	#$36			;I/O + Kernal einblenden.
			sta	CPU_DATA

			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf

			ldy	#$00
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0

			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)

			lda	mobenble		;Aktive Sprites zwischenspeichern.
			sta	mobenble_Buf
			sty	mobenble		;Sprites abschalten.

			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
endif

;******************************************************************************
::tmp7 = Flag64_128!RL_41!RL_71!RL_81!RD_41!RD_71!RD_81
if :tmp7 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE			;Takt-Frequenz speichern.
			sta	CLKRATE_Buf
			ldy	#$00
;--- Ergänzung: 21.07.18/M.Kanet
;InitForIO/RAMLink/RAMDisk:
;In der Version von 2003 wird nicht auf den C128/1MHz-Modus umgeschaltet.
;Die Register müssen aber trotzdem gesichert werden für den Fall das andere
;Routinen zwischen :InitForIO und :DonewithIO das Register verändert haben.
;			sty	CLKRATE
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0

			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
endif

;******************************************************************************
::tmp8 = Flag64_128!RL_NM!RD_NM!RD_NM_SCPU
if :tmp8 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		bit	IO_Activ		;I/O-Modus bereits aktiv ?
			bmi	:51			; => Ja, Ende...
			dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.

			sei
			lda	CPU_DATA
			sta	CPU_RegBuf		;CPU-Status speichern.

			lda	#$36			;I/O + Kernal einblenden.
			sta	CPU_DATA

			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf

			ldy	#$00
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0

			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)

			lda	mobenble		;Aktive Sprites zwischenspeichern.
			sta	mobenble_Buf
			sty	mobenble		;Sprites abschalten.

			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e
::51			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:IO_Activ		b $00
endif

;******************************************************************************
::tmp9 = Flag64_128!RL_NM!RD_NM!RD_NM_SCPU
if :tmp9 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		bit	IO_Activ		;I/O-Modus bereits aktiv ?
			bmi	:51			; => Ja, Ende...
			dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf		;IRQ-Status speichern.
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE			;Takt-Frequenz speichern.
			sta	CLKRATE_Buf
			ldy	#$00
;--- Ergänzung: 21.07.18/M.Kanet
;InitForIO/RAMLink/RAMDisk/NativeMode:
;In der Version von 2003 wird nicht auf den C128/1MHz-Modus umgeschaltet.
;Die Register müssen aber trotzdem gesichert werden für den Fall das andere
;Routinen zwischen :InitForIO und :DonewithIO das Register verändert haben.
;			sty	CLKRATE
			sty	grirqen

			lda	#%01111111		;VIC-Interrupt sperren.
			sta	grirq
			sta	$dc0d			;IRQs sperren.
			sta	$dd0d			;NMIs sperren.

			lda	#> NewIRQ		;IRQ-Routine abschalten.
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0

			lda	#> NewNMI		;NMI-Routine abschalten.
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f			;Datenrichtungsregister A setzen.
			sta	$dd02			;(Serieller Bus)
			sty	mobenble		;Sprites abschalten.
			sty	$dd05			;Timer A löschen.
			iny
			sty	$dd04

			lda	#$81			;NMI-Register initialisieren.
			sta	$dd0d
			lda	#$09			;Timer A starten.
			sta	$dd0e
::51			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
:IO_Activ		b $00
endif

;******************************************************************************
::tmp10 = Flag64_128!HD_41_PP!HD_71_PP!HD_81_PP
if :tmp10 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf

			sei
			lda	CPU_DATA
			sta	CPU_RegBuf
			lda	#$36
			sta	CPU_DATA

			lda	grirqen
			sta	grirqen_Buf
			lda	mobenble
			sta	mobenble_Buf

			ldy	#$00
			sty	grirqen

			lda	#$7f
			sta	grirq
			sta	$dc0d
			sta	$dd0d
			bit	$dc0d
			bit	$dd0d

			lda	#> NewIRQ
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f
			sta	$dd02
			sty	mobenble
			sty	$dd05
			iny
			sty	$dd04

			lda	#$81
			sta	$dd0d
			lda	$dd0e
			and	#$80
			ora	#$09
			sta	$dd0e

			ldy	#$2c
::1			lda	rasreg
			cmp	$8f
			beq	:1
			sta	$8f
			dey
			bne	:1

			lda	$dd00
			and	#$07
			sta	$8e
			ora	#$10
			sta	$8f
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
endif

;******************************************************************************
::tmp11 = Flag64_128!HD_41_PP!HD_71_PP!HD_81_PP
if :tmp11 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		php
			pla
			sta	IRQ_RegBuf
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE			;Takt-Frequenz speichern.
			sta	CLKRATE_Buf
			ldy	#$00
;--- Ergänzung: 21.07.18/M.Kanet
;InitForIO/HD-ParallelPort:
;In der Version von 2003 wird nicht auf den C128/1MHz-Modus umgeschaltet.
;Die Register müssen aber trotzdem gesichert werden für den Fall das andere
;Routinen zwischen :InitForIO und :DonewithIO das Register verändert haben.
;			sty	CLKRATE
			sty	grirqen

			lda	#$7f
			sta	grirq
			sta	$dc0d
			sta	$dd0d
			bit	$dc0d
			bit	$dd0d

			lda	#> NewIRQ
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f
			sta	$dd02
			sty	mobenble
			sty	$dd05
			iny
			sty	$dd04

			lda	#$81
			sta	$dd0d
			lda	$dd0e
			and	#$80
			ora	#$09
			sta	$dd0e

			ldy	#$2c
::1			lda	rasreg
			cmp	$8f
			beq	:1
			sta	$8f
			dey
			bne	:1

			lda	$dd00
			and	#$07
			sta	$8e
			ora	#$10
			sta	$8f
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
endif

;******************************************************************************
::tmp12 = Flag64_128!HD_NM_PP
if :tmp12 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf

			sei
			lda	CPU_DATA
			sta	CPU_RegBuf
			lda	#$36
			sta	CPU_DATA

			lda	grirqen
			sta	grirqen_Buf
			lda	mobenble
			sta	mobenble_Buf

			ldy	#$00
			sty	grirqen

			lda	#$7f
			sta	grirq
			sta	$dc0d
			sta	$dd0d
			bit	$dc0d
			bit	$dd0d

			lda	#> NewIRQ
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f
			sta	$dd02
			sty	mobenble
			sty	$dd05
			iny
			sty	$dd04

			lda	#$81
			sta	$dd0d
			lda	$dd0e
			and	#$80
			ora	#$09
			sta	$dd0e

			ldy	#$2c
::1			lda	rasreg
			cmp	$8f
			beq	:1
			sta	$8f
			dey
			bne	:1

			lda	$dd00
			and	#$07
			sta	$8e
			ora	#$10
			sta	$8f
			rts

:IRQ_RegBuf		b $00
:CPU_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:IO_Activ		b $00
endif

;******************************************************************************
::tmp13 = Flag64_128!HD_NM_PP
if :tmp13 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O - Bereich aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xInitForIO		dec	IO_Activ

			php
			pla
			sta	IRQ_RegBuf
			sei

			lda	mobenble
			sta	mobenble_Buf
			lda	grirqen			;IRQ-Maskenregister speichern.
			sta	grirqen_Buf
			lda	CLKRATE			;Takt-Frequenz speichern.
			sta	CLKRATE_Buf
			ldy	#$00
;--- Ergänzung: 21.07.18/M.Kanet
;InitForIO/HD-ParallelPort/NativeMode:
;In der Version von 2003 wird nicht auf den C128/1MHz-Modus umgeschaltet.
;Die Register müssen aber trotzdem gesichert werden für den Fall das andere
;Routinen zwischen :InitForIO und :DonewithIO das Register verändert haben.
;			sty	CLKRATE
			sty	grirqen

			lda	#$7f
			sta	grirq
			sta	$dc0d
			sta	$dd0d
			bit	$dc0d
			bit	$dd0d

			lda	#> NewIRQ
			sta	irqvec+1
			lda	#< NewIRQ
			sta	irqvec+0
			lda	#> NewNMI
			sta	nmivec+1
			lda	#< NewNMI
			sta	nmivec+0

			lda	#$3f
			sta	$dd02
			sty	mobenble
			sty	$dd05
			iny
			sty	$dd04

			lda	#$81
			sta	$dd0d
			lda	$dd0e
			and	#$80
			ora	#$09
			sta	$dd0e

			ldy	#$2c
::1			lda	rasreg
			cmp	$8f
			beq	:1
			sta	$8f
			dey
			bne	:1

			lda	$dd00
			and	#$07
			sta	$8e
			ora	#$10
			sta	$8f
			rts

:IRQ_RegBuf		b $00
:mobenble_Buf		b $00
:grirqen_Buf		b $00
:CLKRATE_Buf		b $00
:IO_Activ		b $00
endif
