﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41!C_71!C_81!FD_41!FD_71!FD_81!FD_NM!PC_DOS
if :tmp0 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xLogNewDisk		jsr	xEnterTurbo
			txa
			bne	:53
			stx	RepeatFunction
			inx
			stx	r1L
			stx	r1H

			jsr	InitForIO

::51			ldx	#> TD_NewDisk		;NewDisk ausführen.
			lda	#< TD_NewDisk
			jsr	xTurboRoutSet_r1
			jsr	xGetDiskError
			txa
			beq	:52
			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:52
			bcs	:51

::52			jmp	DoneWithIO
::53			rts
endif
