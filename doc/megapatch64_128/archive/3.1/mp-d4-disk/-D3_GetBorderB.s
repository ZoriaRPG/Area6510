﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81!FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
::tmp0b = RL_41!RL_71!RL_81!RL_NM!RD_41!RD_71!RD_81!RD_NM!RD_NM_SCPU
::tmp0c = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp0  = :tmp0a!:tmp0b!:tmp0c
if :tmp0 = TRUE
;******************************************************************************
;*** BorderBlock einlesen.
;    Übergabe:		-
;    Rückgabe:		r1	= Track/Sektor für Borderblock.
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xGetBorderBlock	jsr	xGetDirHead		;Aktuelle BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	Set_curDirHead		;Zeiger auf aktuelle BAM.
			jsr	xChkDkGEOS		;Auf GEOS-Diskette testen.
;--- Ergänzung: 20.07.18/M.Kanet
;Die ursprüngliche Routine von ChkDkGEOS konnte unter bestimmten
;Umständen ein fehlerhaftes Z-Flag zurückmelden weswegen hier eine
;zusätzliche Abfrage auf $00=Keine GEOS-Disk erfolgt.
			cmp	#$00			;$00 ?
			beq	:51			; => Keine GEOS-Diskette.

			lda	curDirHead +171		;Zeiger auf BorderBlock einlesen.
			sta	r1L
			lda	curDirHead +172
			sta	r1H

			ldy	#$00
			b $2c
::51			ldy	#$ff
			ldx	#$00
::52			rts
endif
