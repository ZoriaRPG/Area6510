﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3bc5
:AllocBankT_Block	= $3c40
:AllocBankT_Disk	= $3c37
:AllocBankT_GEOS	= $3c34
:AllocBankT_Spool	= $3c3d
:AllocBankT_Task	= $3c3a
:AllocBankUser		= $3be8
:AllocBank_Block	= $3c6b
:AllocBank_Disk		= $3c62
:AllocBank_GEOS		= $3c5f
:AllocBank_Spool	= $3c68
:AllocBank_Task		= $3c65
:AllocateBank		= $3c6d
:AllocateBankTab	= $3c42
:AutoInitSpooler	= $3d94
:AutoInitTaskMan	= $3d00
:BankCodeTab1		= $4594
:BankCodeTab2		= $4598
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $459c
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $45a8
:BankType_Disk		= $45a4
:BankType_GEOS		= $45a0
:BankUsed		= $4554
:BankUsed_2GEOS		= $3adb
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $2909
:BootInptName		= $292b
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $291a
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $293c
:BootVarEnd		= $293d
:BootVarStart		= $2880
:CheckDrvConfig		= $2c51

:CheckForSpeed		= $34ad
:Class_GeoPaint		= $44f2
:Class_ScrSaver		= $44e1
:ClearDiskName		= $34d1
:ClearDriveData		= $34e8
:ClrBank_Blocked	= $3b49
:ClrBank_Spooler	= $3b46
:ClrBank_TaskMan	= $3b43
:CopyStrg_Device	= $3ecf
:CountDrives		= $34be
:CurDriveMode		= $4503
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $44a4
:DiskDriver_DISK	= $44cd
:DiskDriver_FName	= $44d0
:DiskDriver_INIT	= $44cc
:DiskDriver_SIZE	= $44ce
:DiskDriver_TYPE	= $44cb
:DiskFileDrive		= $44ca
:Dlg_DrawTitel		= $3474
:Dlg_IllegalCfg		= $470f
:Dlg_LdDskDrv		= $469f
:Dlg_LdMenuErr		= $477f
:Dlg_NoDskFile		= $461d
:Dlg_SetNewDev		= $4816
:Dlg_Titel1		= $488a
:Dlg_Titel2		= $489a
:DoInstallDskDev	= $35af
:DriveInUseTab		= $4517
:DriveRAMLink		= $4502
:Err_IllegalConf	= $33fe
:ExitToDeskTop		= $2c14
:FetchRAM_DkDrv		= $2fa4
:FindCurRLPart		= $4b6d
:FindDiskDrvFile	= $300d
:FindDkDvAllDrv		= $3036
:FindDriveType		= $38de
:FindRTC_64Net		= $3fc1
:FindRTC_SM		= $3fad
:FindRTCdrive		= $3f70
:Flag_ME1stBoot		= $4505
:FreeBank		= $3c96
:FreeBankTab		= $3c81
:GetDrvModVec		= $328b
:GetFreeBank		= $3bfa
:GetFreeBankTab		= $3bfc
:GetInpDrvFile		= $3eb5
:GetMaxFree		= $3b7a
:GetMaxSpool		= $3b80
:GetMaxTask		= $3b7d
:GetPrntDrvFile		= $3e6b
:InitDkDrv_Disk		= $30d3
:InitDkDrv_RAM		= $30cd
:InitScrSaver		= $3dd0
:InstallRL_Part		= $35db
:IsDrvAdrFree		= $381f
:IsDrvOnline		= $39ed
:LastSpeedMode		= $45c7
:LdScrnFrmDisk		= $2d62
:LoadBootScrn		= $2d36

:LoadDiskDrivers	= $3099
:LoadDskDrvData		= $3331
:LoadInptDevice		= $3ea7
:LoadPrntDevice		= $3e5d
:LookForDkDvFile	= $3074
:NewDrive		= $44ff
:NewDriveMode		= $4500
:NoInptName		= $45d8
:NoPrntName		= $45ca
:PrepareExitDT		= $2c25
:RL_Aktiv		= $45c8
:SCPU_Aktiv		= $45c9
:SaveDskDrvData		= $32b7
:SetClockGEOS		= $3f3b
:SetSystemDevice	= $2f89
:StashRAM_DkDrv		= $2fa7
:StdClrGrfx		= $2d54
:StdClrScreen		= $2d44
:StdDiskName		= $45e4
:SwapScrSaver		= $3e2d
:SysDrive		= $44b6
:SysDrvType		= $44b7
:SysFileName		= $44b9
:SysRealDrvType		= $44b8
:SysStackPointer	= $44b5
:SystemClass		= $4493
:SystemDlgBox		= $346d
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $4501
:UpdateDiskDriver	= $3112
:VLIR_BASE		= $491c
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $23e4
:VLIR_Types		= $2380
:firstBootCopy		= $4504
