﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3c93
:AllocBankT_Block	= $3d0e
:AllocBankT_Disk	= $3d05
:AllocBankT_GEOS	= $3d02
:AllocBankT_Spool	= $3d0b
:AllocBankT_Task	= $3d08
:AllocBankUser		= $3cb6
:AllocBank_Block	= $3d39
:AllocBank_Disk		= $3d30
:AllocBank_GEOS		= $3d2d
:AllocBank_Spool	= $3d36
:AllocBank_Task		= $3d33
:AllocateBank		= $3d3b
:AllocateBankTab	= $3d10
:AutoInitSpooler	= $3efa
:AutoInitTaskMan	= $3df9
:BankCodeTab1		= $46fa
:BankCodeTab2		= $46fe
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4702
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $470e
:BankType_Disk		= $470a
:BankType_GEOS		= $4706
:BankUsed		= $46ba
:BankUsed_2GEOS		= $3b91
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294e
:BootVarEnd		= $294f
:BootVarStart		= $2880
:CheckDrvConfig		= $2c7a

:CheckForSpeed		= $353f
:Class_GeoPaint		= $4658
:Class_ScrSaver		= $4647
:ClearDiskName		= $3566
:ClearDriveData		= $357d
:ClrBank_Blocked	= $3bff
:ClrBank_Spooler	= $3bfc
:ClrBank_TaskMan	= $3bf9
:CopyStrg_Device	= $4035
:CountDrives		= $3553
:CurDriveMode		= $4669
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $460a
:DiskDriver_DISK	= $4633
:DiskDriver_FName	= $4636
:DiskDriver_INIT	= $4632
:DiskDriver_SIZE	= $4634
:DiskDriver_TYPE	= $4631
:DiskFileDrive		= $4630
:Dlg_DrawTitel		= $3506
:Dlg_IllegalCfg		= $4879
:Dlg_LdDskDrv		= $4809
:Dlg_LdMenuErr		= $48e9
:Dlg_NoDskFile		= $4786
:Dlg_SetNewDev		= $4981
:Dlg_Titel1		= $49f5
:Dlg_Titel2		= $4a05
:DoInstallDskDev	= $3644
:DriveInUseTab		= $467d
:DriveRAMLink		= $4668
:Err_IllegalConf	= $3490
:ExitToDeskTop		= $2c3d
:FetchRAM_DkDrv		= $3036
:FindCurRLPart		= $4cd8
:FindDiskDrvFile	= $309f
:FindDkDvAllDrv		= $30c8
:FindDriveType		= $3973
:FindRTC_64Net		= $4127
:FindRTC_SM		= $4113
:FindRTCdrive		= $40d6
:Flag_ME1stBoot		= $466b
:FreeBank		= $3d64
:FreeBankTab		= $3d4f
:GetDrvModVec		= $331d
:GetFreeBank		= $3cc8
:GetFreeBankTab		= $3cca
:GetInpDrvFile		= $401b
:GetMaxFree		= $3c30
:GetMaxSpool		= $3c36
:GetMaxTask		= $3c33
:GetPrntDrvFile		= $3fd1
:InitDkDrv_Disk		= $3165
:InitDkDrv_RAM		= $315f
:InitScrSaver		= $3f36
:InstallRL_Part		= $3670
:IsDrvAdrFree		= $38b4
:IsDrvOnline		= $3a82
:LastSpeedMode		= $4730
:LdScrnFrmDisk		= $2df9
:LoadBootScrn		= $2dc6

:LoadDiskDrivers	= $312b
:LoadDskDrvData		= $33c3
:LoadInptDevice		= $400d
:LoadPrntDevice		= $3fc3
:LookForDkDvFile	= $3106
:NewDrive		= $4665
:NewDriveMode		= $4666
:NoInptName		= $4741
:NoPrntName		= $4733
:PrepareExitDT		= $2c4e
:RL_Aktiv		= $4731
:SCPU_Aktiv		= $4732
:SaveDskDrvData		= $3349
:SetClockGEOS		= $40a1
:SetSystemDevice	= $301b
:StashRAM_DkDrv		= $3039
:StdClrGrfx		= $2deb
:StdClrScreen		= $2dd4
:StdDiskName		= $474d
:SwapScrSaver		= $3f93
:SysDrive		= $461c
:SysDrvType		= $461d
:SysFileName		= $461f
:SysRealDrvType		= $461e
:SysStackPointer	= $461b
:SystemClass		= $45f9
:SystemDlgBox		= $34ff
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $4667
:UpdateDiskDriver	= $31a4
:VLIR_BASE		= $4a87
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2279
:VLIR_Types		= $2380
:firstBootCopy		= $466a
