﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

English users please scroll down!

Willkommen zum MegaPatch für GEOS - Version 3.1
https://gitlab.com/mkslack/Area6510

Um MP3 zu installieren startet man die Datei "StartMP" von der aktuellen DeskTop-Oberfläche. Bei der Installation folgt man am besten den Bildschirm-Anzeigen und kopiert alle Dateien auf die Ziel-Diskette. Falls möglich sollte man eine leere Diskette zur Installation verwenden. Nachdem MP3 im System installiert wurde wird der DeskTop gestartet.

Nachdem einem Neustart kann man MP3 aus einem laufenden GEOS-V2-System heraus starten, indem man die Datei "GEOS64.MP3" doppelklickt.

Welcome to MegaPatch for GEOS - Version 3.1
https://gitlab.com/mkslack/Area6510

To install MP3 on your C64 you have to run the StartMP-File from your current deskTop.  Please follow the menus and copy all files to your MP3-workdisk. If possible, please use an empty disk/partition when installing MP3. After the files has been copied, MP3 will be loaded. After MP3 has been installed successfully, your deskTop appears on the screen.

After you have installed MP3 you can reload MP3 anytime from a running GEOSV2-system using the file "GEOS64.MP3".
