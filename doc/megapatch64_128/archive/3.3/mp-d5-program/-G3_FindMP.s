﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Auf GEOS-MegaPatch testen.
;    GEOS-Boot mit MP3: Rückkehr zum Hauptprogramm.
;    GEOS-Boot mit V2x: Sofortiges Programm-Ende.
;    Programmstart V2x: Fehler ausgeben, zurück zum DeskTop.
:FindMegaPatch		lda	MP3_CODE +0		;Kennbyte für MegaPatch.
			cmp	#"M"			;MegaPatch installiert ?
			bne	:51			;Nein, weiter...
			lda	MP3_CODE +1
			cmp	#"P"
			beq	:54

::51			bit	firstBoot		;GEOS-BootUp ?
			bpl	:53			;Keine Meldung ausgeben.

			lda	screencolors		;Bildschirm löschen.
			sta	:52
			jsr	i_FillRam
			w	1000
			w	COLOR_MATRIX
::52			b	$00

			lda	#$0b			;Bildschirm löschen.
			jsr	SetPattern
			jsr	i_Rectangle
			b	$00,$c7
			w	$0000 ! DOUBLE_W,$013f ! DOUBLE_W ! ADD1_W

			lda	#<Dlg_WrongGEOS		;Fehlermeldung ausgeben.
			ldx	#>Dlg_WrongGEOS
			sta	r0L
			stx	r0H
			jsr	DoDlgBox
::53			jmp	EnterDeskTop
::54			rts

;*** Dialogbox: Falsche GEOS-Version.
:Dlg_WrongGEOS		b $81
			b DBTXTSTR,$0c,$10
			w :101
			b DBTXTSTR,$0c,$1a
			w :102
			b DBTXTSTR,$0c,$2a
			w :103
			b DBTXTSTR,$0c,$34
			w :104
			b DBTXTSTR,$0c,$3e
			w :105
			b OK      ,$10,$48
			b NULL

if Sprache = Deutsch
::101			b PLAINTEXT,BOLDON
			b "Das 'GEOS-MegaPatch' ist nicht",NULL
::102			b "in Ihrem System installiert!",NULL
::103			b "Booten Sie 'GEOS' erneut von",NULL
::104			b "einer MegaPatch-Systemdiskette",NULL
::105			b "um das Programm zu starten.",NULL
endif

if Sprache = Englisch
::101			b PLAINTEXT,BOLDON
			b "The 'GEOS-MegaPatch' is not",NULL
::102			b "installed in your System!",NULL
::103			b "Boot 'GEOS' once more from",NULL
::104			b "an MegaPatch-Systemdisk",NULL
::105			b "bevor you run this program.",NULL
endif
