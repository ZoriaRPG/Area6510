﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			ldx	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,x
			lda	#DriveModeFlags
			sta	RealDrvMode -8,x
			lda	turboFlags  -8,x	;TurboRoutinen in FloppyRAM ?
			bmi	:51			;Ja, weiter...

			jsr	InitTurboDOS		;TuroDOS installieren.
			txa				;Laufwerksfehler ?
			bne	:56			;Ja, Abbruch...

			ldx	curDrive
			lda	#$80			;Flag für "TurboDOS in FloppyRAM"
			sta	turboFlags -8,x		;setzen.

::51			and	#$40			;TurboDOS bereits aktiv ?
			bne	:54			;Ja, weiter...

			jsr	InitForIO		;I/O aktivieren.

			ldx	#>ExecTurboDOS
			lda	#<ExecTurboDOS
			jsr	SendFloppyCom		;"M-E" ausführen.
			txa				;Laufwerksfehler ?
			bne	:55			;Ja, Abbruch...

			jsr	$ffae			;Laufwerk abschalten.

			sei				;IRQ sperren.

			ldy	#$21			;Warteschleife.
::52			dey
			bne	:52

			jsr	CLOCK_OUT_LOW

::53			bit	$dd00			;Warten bis Laufwerk aktiv.
			bmi	:53

			jsr	DoneWithIO		;I/O abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			ora	#$40			;Flag für "TurboDOS in FloppyRAM
			sta	turboFlags -8,x		;ist aktiv" setzen.

::54			ldx	#$00			;Flag "Kein Fehler"...
			rts

::55			jsr	DoneWithIO		;I/O abschalten.
			txa
::56			rts				;Ende...

;*** Befehl zum aktivieren des TurboDOS.
:ExecTurboDOS		b "M-E"
			w TD_Start
endif

;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			lda	#$06			;Sektor-Interleave für 1571
			sta	interleave		;festlegen.

			ldx	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,x
			lda	#DriveModeFlags
			sta	RealDrvMode -8,x
			lda	turboFlags  -8,x	;TurboRoutinen in FloppyRAM ?
			bmi	:51			;Ja, weiter...

			jsr	InitTurboDOS		;TuroDOS installieren.
			txa				;Laufwerksfehler ?
			bne	:56			;Ja, Abbruch...

			ldx	curDrive
			lda	#$80			;Flag für "TurboDOS in FloppyRAM"
			sta	turboFlags -8,x		;setzen.
::51			and	#$40			;TurboDOS bereits aktiv ?
			bne	:54			;Ja, weiter...

			jsr	InitForIO		;I/O aktivieren.

			ldx	#>ExecTurboDOS
			lda	#<ExecTurboDOS
			jsr	SendFloppyCom		;"M-E" ausführen.
			txa				;Laufwerksfehler ?
			bne	:55			;Ja, Abbruch...

			jsr	$ffae			;Laufwerk abschalten.

			sei				;IRQ sperren.
			ldy	#$21			;Warteschleife.
::52			dey
			bne	:52

			jsr	CLOCK_OUT_LOW		;ATN-Signal an Floppy senden.
::53			bit	$dd00			;Warten bis DATA_IN von Floppy
			bmi	:53			;auf LOW gesetzt wird.

			jsr	DoneWithIO

			ldx	curDrive
			lda	turboFlags -8,x
			ora	#$40			;Flag für "TurboDOS in FloppyRAM
			sta	turboFlags -8,x		;ist aktiv" setzen.

::54			ldx	#$00			;Flag "Kein Fehler"...
			rts

::55			jsr	DoneWithIO		;I/O abschalten.
			txa
::56			rts				;Ende...

;*** Befehl zum aktivieren des TurboDOS.
:ExecTurboDOS		b "M-E"
			w TD_Start
endif

;******************************************************************************
::tmp2 = C_81!PC_DOS!IEC_NM!S2I_NM
if :tmp2 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			ldx	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,x
			lda	#DriveModeFlags
			sta	RealDrvMode -8,x
			lda	turboFlags  -8,x	;TurboRoutinen in FloppyRAM ?
			bmi	:51			;Ja, weiter...

			jsr	InitTurboDOS		;TuroDOS installieren.
			txa				;Laufwerksfehler ?
			bne	:56			;Ja, Abbruch...

			ldx	curDrive
			lda	#$80			;Flag für "TurboDOS in FloppyRAM"
			sta	turboFlags -8,x		;setzen.
::51			and	#$40			;TurboDOS bereits aktiv ?
			bne	:54			;Ja, weiter...

			jsr	InitForIO		;I/O aktivieren.

			ldx	#>ExecTurboDOS
			lda	#<ExecTurboDOS
			jsr	SendFloppyCom		;"M-E" ausführen.
			txa				;Laufwerksfehler ?
			bne	:55			;Ja, Abbruch...

			jsr	$ffae			;Laufwerk abschalten.

			sei				;IRQ sperren.
			ldy	#$21			;Warteschleife.
::52			dey
			bne	:52

			jsr	CLOCK_OUT_LOW		;ATN-Signal an Floppy senden.
::53			bit	$dd00			;Warten bis DATA_IN von Floppy
			bmi	:53			;auf LOW gesetzt wird.

			jsr	DoneWithIO		;I/O abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			ora	#$40			;Flag für "TurboDOS in FloppyRAM
			sta	turboFlags -8,x		;ist aktiv" setzen.
::54			ldx	#$00			;Flag "Kein Fehler"...
::56			rts				;Ende...

::55			jmp	DoneWithIO

;*** Befehl zum aktivieren des TurboDOS.
:ExecTurboDOS		b "M-E"
			w TD_Start
endif

;******************************************************************************
::tmp3 = FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
if :tmp3 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			lda	#$01			;Sektor-Interleave für FD/HD
			sta	interleave		;festlegen.

			ldx	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,x
			lda	#DriveModeFlags
			sta	RealDrvMode -8,x
			lda	turboFlags  -8,x	;TurboRoutinen in FloppyRAM ?
			bmi	:51			;Ja, weiter...

			jsr	InitTurboDOS		;TuroDOS installieren.
			txa				;Laufwerksfehler ?
			bne	:56			;Ja, Abbruch...

			ldx	curDrive
			lda	#$80			;Flag für "TurboDOS in FloppyRAM"
			sta	turboFlags -8,x		;setzen.
::51			and	#$40			;TurboDOS bereits aktiv ?
			bne	:54			;Ja, weiter...

			jsr	InitForIO		;I/O aktivieren.

			ldx	#>ExecTurboDOS
			lda	#<ExecTurboDOS
			jsr	SendFloppyCom		;"M-E" ausführen.
			txa				;Laufwerksfehler ?
			bne	:55			;Ja, Abbruch...

			jsr	$ffae			;Laufwerk abschalten.

			sei				;IRQ sperren.
			ldy	#$21			;Warteschleife.
::52			dey
			bne	:52

			jsr	CLOCK_OUT_LOW		;ATN-Signal an Floppy senden.
::53			bit	$dd00			;Warten bis DATA_IN von Floppy
			bmi	:53			;auf LOW gesetzt wird.

			jsr	DoneWithIO		;I/O abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			ora	#$40			;Flag für "TurboDOS in FloppyRAM
			sta	turboFlags -8,x		;ist aktiv" setzen.

::54			ldx	#$00			;Flag "Kein Fehler"...
			rts

::55			jsr	DoneWithIO		;I/O abschalten.
			txa
::56			rts				;Ende...

;*** Befehl zum aktivieren des TurboDOS.
:ExecTurboDOS		b "M-E"
			w TD_Start
endif

;******************************************************************************
::tmp4 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp4 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			lda	#$01			;Sektor-Interleave für FD/HD
			sta	interleave		;festlegen.

			ldx	curDrive
			lda	turboFlags -8,x
			bmi	:1

			jsr	InitTurboDOS
			txa
			bne	:4

			ldx	curDrive
			lda	#$80
			sta	turboFlags -8,x
::1			ldx	#$00
			and	#$40
			bne	:4

			jsr	InitForIO

			ldx	#> ExecTurboDOS
			lda	#< ExecTurboDOS
			ldy	#$05
			jsr	Job_Command
			jsr	$ffae

;			ldx	#$21
;::2			dex
;			bne	:2

			lda	$8f
			sta	$dd00
::3			bit	$dd00
			bmi	:3

			ldy	curDrive
			lda	#$c0
			sta	turboFlags -8,y
			jsr	DoneWithIO
::4			txa
			rts

;*** Befehl zum aktivieren des TurboDOS.
:ExecTurboDOS		b $4d,$2d,$45
			w TD_MLoop_Start
endif

;******************************************************************************
::tmp5 = RL_NM!RL_81!RL_71!RL_41
if :tmp5 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			ldy	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,y
			lda	#DriveModeFlags
			sta	RealDrvMode -8,y
			lda	#$c0
			sta	turboFlags  -8,y

;*** RAMLink-Daten testen.
:RL_DataCheck		bit	RL_DATA_READY		;Wurden Part.-Informationen bereits
			bmi	:51			;eingelesen ? => Ja, weiter...
			jsr	GetPartData

::51			ldx	RL_PartNr		;Aktive Partition ermittelt ?
			beq	:52			; => Nein, weiter...
			ldy	curDrive

;--- Ergänzung: 18.10.18/M.Kanet
;Der Test ramBase=0 -> UpdatePartInfo ist immer TRUE wenn nur eine Partition
;auf der RAMLink existiert, z.B. nach einer Neu-Initialisierung. Dann beginnt
;die Partition bei $00:$0000.
;Daher prüfen ob drivePartData=0, wenn ja dann erste Partition wählen.
			lda	drivePartData-8,y
			beq	:52
			lda	ramBase     - 8,y
;			beq	:52			; => Erste Partition installieren.
			cmp	RL_PartADDR_H  ,x	;Wurde Partition gewechselt ?
			beq	:53			; => Nein, weiter...
::52			jsr	UpdatePartInfo
			b $2c
::53			ldx	#NO_ERROR

			ldy	curDrive
			lda	#$00
			cpx	#NO_ERROR
			bne	:54
			lda	RL_PartNr		;Partitions-Nr. zwischenspeichern.
::54			sta	drivePartData -8,y
			rts

;*** Partitions-Informationen einlesen.
:GetPartData		jsr	Save_RegData		;":rX"-Register sichern.
			jsr	xExitTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O aktivieren.

			LoadW	r4,GP_DATA		;Zeiger auf Zwischenspeicher.
			ldx	#$01			;Zeiger auf erste Partition.
::51			stx	r3H 			;Partitionsdaten aus RAMLink-
			jsr	xReadPDirEntry		;Systemverzeichnis einlesen.

			ldx	r3H
			lda	GP_DATA     + 0		;Partitionstyp einlesen und
			sta	RL_PartTYPE    ,x	;zwischenspeichern.
			lda	GP_DATA     +20		;Partitionsadresse einlesen und
			sta	RL_PartADDR_H  ,x	;zwischenspeichern.
			lda	GP_DATA     +21
			sta	RL_PartADDR_L  ,x
			inx				;Zeiger auf nächste Partition.
			cpx	#PART_MAX   + 1		;Alle Partitionen durchsucht ?
			bcc	:51			;Nein, weiter...

			jsr	DoneWithIO		;I/O abschalten.
			jsr	Load_RegData		;":rX"-Register zurücksetzen.

			lda	#$ff			;Flag setzen: "Alle Partitions-
			sta	RL_DATA_READY		;daten eingelesen"
			rts

;*** Aktuelle Partitions-Nr. bestimmen.
:UpdatePartInfo		ldx	#$00
			stx	Find1stPart +0
			inx
			ldy	curDrive

::51			lda	RL_PartTYPE    ,x
			eor	curType
			and	#%00001111		;Partitionsformat gültig ?
			bne	:52			; => Nein, weiter...
			bit	Find1stPart + 0		;Erste Partition gefunden ?
			bmi	:51a			; => Ja, weiter...
			dec	Find1stPart + 0		;Erste partition als Vorgabe
			stx	Find1stPart + 1		;definieren.

::51a			lda	ramBase     - 8,y	;Partitionsadresse einlesen.
			cmp	RL_PartADDR_H  ,x	;Partition gefunden ?
			beq	:53			; => Ja, weiter...

::52			inx				;Zeiger auf nächste Partition.
			cpx	#PART_MAX   + 1		;Alle Partitionen durchsucht ?
			bcc	:51			;Nein, weiter...

			ldx	Find1stPart + 1
			bit	Find1stPart + 0		;Erste Partition gefunden ?
			bmi	:53			; => Ja, aktivieren...
			ldx	#NO_PARTITION		;Keine Partition gefunden.
			rts

::53			stx	RL_PartNr		;Partitions-Nr. zwischenspeichern.

;--- Ergänzung: 18.10.18/M.Kanet
;XReg wird an dieser Stelle nicht mehr abgefragt. Befehl deaktiviert.
;			txa
;			ldx	curDrive
;			sta	RL_PartNr		;Partitions-Nr. zwischenspeichern.

;--- Ergänzung: 18.10.18/M.Kanet
;Startadresse Partition in Laufwerkstreiber übertragen.
			lda	RL_PartADDR_L  ,x
			sta	RL_PartADDR+0
			lda	RL_PartADDR_H  ,x
			sta	RL_PartADDR+1

			jsr	StashDriverData		;Laufwerksinformationen speichern.

			jsr	Save_RegData		;":rX"-Register sichern.
			jsr	xExitTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O aktivieren.
			lda	RL_PartNr
			sta	r3H
			jsr	xSwapPartition		;neue Partition aktivieren.
			jsr	DoneWithIO		;I/O abschalten.
			jmp	Load_RegData		;":rX"-Register zurücksetzen.

:Find1stPart		b $00,$00
endif

;******************************************************************************
::tmp6 = RD_NM!RD_81!RD_71!RD_41!RD_NM_SCPU!RD_NM_CREU
if :tmp6 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			ldy	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,y
			lda	#DriveModeFlags
			sta	RealDrvMode -8,y
			lda	#$c0
			sta	turboFlags  -8,y
			ldx	#$00
			rts
endif

;******************************************************************************
::tmp7 = RD_NM_GRAM
if :tmp7 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xEnterTurbo		lda	curDrive
			jsr	SetDevice

			ldy	curDrive
			lda	DiskDrvType
			sta	RealDrvType -8,y
			lda	#DriveModeFlags
			ora	GeoRAMBSize
			sta	RealDrvMode -8,y
			lda	#$c0
			sta	turboFlags  -8,y
			ldx	#$00
			rts
endif
