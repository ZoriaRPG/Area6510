﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS einlesen.
;    Übergabe:		$8B/$8C  , Zeiger auf Bytespeicher.
;			yReg     , Zeiger auf erstes Byte.
:Turbo_GetBytes		jsr	DataClkATN_HIGH
			pha
			pla
			pha
			pla
			sty	$8d

:Turbo_GetNxByte	sec				;Warteschleife.
::51			lda	$d012
			sbc	#$31
			bcc	:52
			and	#$06
			beq	:51

::52			lda	$8f			;TurboDOS-Übertragung abschalten.
			sta	$dd00

			lda	$8b
			lda	$8e			;TurboDOS-Übertragung starten.
			sta	$dd00
			dec	$8d
			nop
			nop
			nop

			lda	$dd00			;Byte über TurboDOS einlesen und
			lsr				;Low/High-Nibble berechnen.
			lsr
			nop
			ora	$dd00
			lsr
			lsr
			lsr
			lsr
			ldy	$dd00
			tax
			tya
			lsr
			lsr
			ora	$dd00
			and	#$f0
			ora	NibbleByteL,x

			ldy	$8d			;Zeiger auf Byte-Speicher lesen und
			sta	($8b),y			;Byte in Floppy-Speicher schreiben.
			bne	Turbo_GetNxByte		;Alle Bytes gelsen ? Nein, weiter...

;*** TurboModus abschalten.
:CLOCK_OUT_LOW		ldx	#$ff
			stx	$dd00
			rts
endif

;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS einlesen.
;    Übergabe:		$8B/$8C  , Zeiger auf Bytespeicher.
;			yReg     , Zeiger auf erstes Byte.
:Turbo_GetBytes		lda	r0L			;Register ":r0L" zwischenspeichern.
			pha
			jsr	DataClkATN_HIGH		;TurboDOS-Leitungen auf HIGH.
			sty	r0L			;Anzahl Bytes zwischenspeichern.

:Turbo_GetNxByte	sec				;Warteschleife bis TurboDOS
::51			lda	$d012			;aktiviert ist.
			sbc	#$31
			bcc	:52
			and	#$06
			beq	:51

::52			lda	$8f			;CLOCK_OUT auf LOW setzen.
			sta	$dd00
			lda	$8e			;CLOCK_OUT zurück auf HIGH.
			sta	$dd00
			dec	r0L

			lda	$dd00			;High/Low-Nibble einlesen und
			lsr				;Byte-Wert berechnen.
			lsr
			nop
			ora	$dd00
			lsr
			lsr
			lsr
			lsr
			ldy	$dd00
			tax
			tya
			lsr
			lsr
			ora	$dd00
			and	#$f0
			ora	DefNibbleData,x
			ldy	r0L
:Def_AssCode1		sta	($8b),y			;Byte speichern. ACHTUNG! Diese
			ora	$8d			;Befehle werden modifiziert!
:Def_AssCode2		ora	$8d
			tya				;Alle Bytes eingelesen ?
			bne	Turbo_GetNxByte		;Nein, weiter...
			jsr	CLOCK_OUT_LOW
			pla				;Register ":r0L" zurücksetzen.
			sta	r0L
			lda	($8b),y			;Erstes Byte aus Speicher in AKKU.
			rts				;Ende.

;*** CLOCK_OUT-Leitung auf LOW setzen.
:CLOCK_OUT_LOW		ldx	#$ff
			stx	$dd00
			rts
endif

;******************************************************************************
::tmp2a = C_81!FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp2b = IEC_NM!S2I_NM
::tmp2 = :tmp2a!:tmp2b
if :tmp2 = TRUE
;******************************************************************************
;*** Bytes über ser. Bus / TurboDOS einlesen.
;    Übergabe:		$8B/$8C  , Zeiger auf Bytespeicher.
;			yReg     , Zeiger auf erstes Byte.
:Turbo_GetBytes		jsr	DataClkATN_HIGH

:Turbo_GetNxByte	sec				;Warteschleife bis TurboDOS
::51			lda	$d012			;aktiviert ist.
			sbc	#$32
			and	#$07
			beq	:51

			lda	$8f			;CLOCK_OUT auf LOW setzen.
			sta	$dd00
			and	#$0f			;CLOCK_OUT zurück auf HIGH.
			sta	$dd00

			lda	$dd00			;High/Low-Nibble einlesen und
			lsr				;Byte-Wert berechnen.
			lsr
			ora	$dd00
			lsr
			lsr
			eor	$8e
			eor	$dd00
			lsr
			lsr
			eor	$8e
			eor	$dd00
			dey
			sta	($8b),y			;Nyte speichern und weiter mit
			bne	Turbo_GetNxByte		;nächstem Byte.

;*** ATN-Signal an Floppy senden.
:CLOCK_OUT_LOW		ldx	#$ff			;Bit #4 = CLOCK_OUT auf LOW.
			stx	$dd00			;Signal an Floppy senden.
			rts
endif

;******************************************************************************
::tmp3 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp3 = TRUE
;******************************************************************************
;*** Daten über HD-Kabel empfangen.
;    Übergabe:		$8B/$8C  , Zeiger auf Bytespeicher.
;			yReg     , Zeiger auf erstes Byte.
:Turbo_GetBytes		jsr	HD_MODE_SEND
			jsr	DataClkATN_HIGH

::0			jsr	WAIT_DATA

;			ldx	$8f			;Warten bis Byte von HD über Kabel
;			stx	$dd00			;bereitgestellt wird.
;::1			bit	$dd00
;			bmi	:1

			lda	$df40			;Byte über HD-Kabel einlesen und
			dey				;zwischenspeichern.
			sta	($8b),y

			ldx	$8e			;Warten bis Byte von HD über Kabel
			stx	$dd00			;bereitgestellt wird.
::2			bit	$dd00
			bpl	:2

			tya				;Alle Bytes eingelesen ?
			beq	:3			; => Ja, Ende...

			lda	$df40			;Byte über HD-Kabel einlesen und
			dey				;zwischenspeichern.
			sta	($8b),y
			bne	:0
::3			jmp	WAIT_DATA

;::3			ldx	$8f			;Warten bis Übertragung beendet.
;			stx	$dd00
;::4			bit	$dd00
;			bmi	:4
;			rts

endif
