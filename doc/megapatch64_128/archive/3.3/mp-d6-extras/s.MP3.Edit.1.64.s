﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $3d9f
:AutoInitTaskMan	= $3d0b
:BankCodeTab1		= $4596
:BankCodeTab2		= $459a
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $459e
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $45aa
:BankType_Disk		= $45a6
:BankType_GEOS		= $45a2
:BankUsed		= $4556
:BankUsed_2GEOS		= $3ae6
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $2909
:BootInptName		= $292b
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $291a
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $293c
:BootVarEnd		= $293d
:BootVarStart		= $2880
:CheckDrvConfig		= $2c51
:CheckForSpeed		= $34b0
:Class_GeoPaint		= $44f4
:Class_ScrSaver		= $44e3
:ClearDiskName		= $34d4
:ClearDriveData		= $34eb
:ClrBank_Blocked	= $3b54
:ClrBank_Spooler	= $3b51
:ClrBank_TaskMan	= $3b4e
:CopyStrg_Device	= $3eda
:CountDrives		= $34c1
:CurDriveMode		= $4505
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $44a6

:DiskDriver_DISK	= $44cf
:DiskDriver_FName	= $44d2
:DiskDriver_INIT	= $44ce
:DiskDriver_SIZE	= $44d0
:DiskDriver_TYPE	= $44cd
:DiskFileDrive		= $44cc
:Dlg_DrawTitel		= $3477
:Dlg_IllegalCfg		= $4711
:Dlg_LdDskDrv		= $46a1
:Dlg_LdMenuErr		= $4781
:Dlg_NoDskFile		= $461f
:Dlg_SetNewDev		= $4818
:Dlg_Titel1		= $488c
:Dlg_Titel2		= $489c
:DoInstallDskDev	= $35ba
:DriveInUseTab		= $4519
:DriveRAMLink		= $4504
:Err_IllegalConf	= $3401
:ExitToDeskTop		= $2c14
:FetchRAM_DkDrv		= $2fa7
:FindCurRLPart		= $4b6f
:FindDiskDrvFile	= $3010
:FindDkDvAllDrv		= $3039
:FindDriveType		= $38e9
:FindRTC_64Net		= $3fc4
:FindRTC_SM		= $3fb0
:FindRTCdrive		= $3f73
:Flag_ME1stBoot		= $4507
:GetDrvModVec		= $328e
:GetInpDrvFile		= $3ec0
:GetMaxFree		= $3b85
:GetMaxSpool		= $3b8b
:GetMaxTask		= $3b88
:GetPrntDrvFile		= $3e76
:InitDkDrv_Disk		= $30d6
:InitDkDrv_RAM		= $30d0
:InitScrSaver		= $3ddb
:InstallRL_Part		= $35e6
:IsDrvAdrFree		= $382a
:IsDrvOnline		= $39f8
:LastSpeedMode		= $45c9
:LdScrnFrmDisk		= $2d65
:LoadBootScrn		= $2d39
:LoadDiskDrivers	= $309c
:LoadDskDrvData		= $3334
:LoadInptDevice		= $3eb2
:LoadPrntDevice		= $3e68
:LookForDkDvFile	= $3077
:NewDrive		= $4501
:NewDriveMode		= $4502
:NoInptName		= $45da
:NoPrntName		= $45cc
:PrepareExitDT		= $2c25
:RL_Aktiv		= $45ca
:SCPU_Aktiv		= $45cb
:SetClockGEOS		= $3f46
:SetSystemDevice	= $2f8c
:StashRAM_DkDrv		= $2faa
:StdClrGrfx		= $2d57
:StdClrScreen		= $2d47
:StdDiskName		= $45e6

:SwapScrSaver		= $3e38
:SysDrive		= $44b8
:SysDrvType		= $44b9
:SysFileName		= $44bb
:SysRealDrvType		= $44ba
:SysStackPointer	= $44b7
:SystemClass		= $4495
:SystemDlgBox		= $3470
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $4503
:UpdateDiskDriver	= $3115
:VLIR_BASE		= $491e
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $23e2
:VLIR_Types		= $2380
:firstBootCopy		= $4506
