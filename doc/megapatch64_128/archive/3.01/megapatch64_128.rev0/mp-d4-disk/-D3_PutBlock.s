﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	IsSekInRAM_OK
			bcc	:53

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutInitByt
			jsr	xGetDiskError
			txa
			beq	:52

			inc	RepeatFunction
			cpy	RepeatFunction
			beq	:53
			bcs	:51
::52			bit	curType
			bvc	:53
			jsr	SaveSekInRAM
::53			rts
endif

;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:52			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...
			jsr	xVerWriteBlock		;Sektor vergleichen.
::51			jmp	DoneWithIO		;I/O abschalten.
::52			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Ist Track/Sektor-Adresse gültig ?
			bcc	:52			;Nein, Abbruch...

::51			jsr	Turbo_PutBlock
			jsr	Turbo_PutBytes
			jsr	GetReadError		;TurboDOS-Status einlesen.
			txa				;Fehler aufgetreten ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Wiederholungszähler setzen.
			cpy	RepeatFunction		;Alle Versuche fehlgeschlagen ?
			beq	:52			;Ja, Abbruch...
			bcs	:51			;Sektor nochmal lesen.
::52			rts
endif

;******************************************************************************
::tmp2 = FD_41!FD_71!HD_41!HD_71
if :tmp2 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Sektor-Adresse testen.
			bcc	:52			; => Fehler, Abbruch...

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutBytes		;256 Byte an Floppy senden.

			jsr	GetReadError		;Diskettenfehler ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Fehlerzähler korrigieren.
			cpy	RepeatFunction		;Zähler abgelaufen ?
			beq	:52			;Ja, Ende...
			bcs	:51			;Nein, nochmal schreiben...

::52			rts
endif

;******************************************************************************
::tmp3 = C_81!FD_81!FD_NM!HD_81!HD_NM
if :tmp3 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Sektor-Adresse testen.
			bcc	:53			; => Fehler, Abbruch...

			jsr	SwapDskNamData		;BAM nach 1581 Konvertieren.

::51			ldx	#> TD_WrSekData
			lda	#< TD_WrSekData
			jsr	xTurboRoutSet_r1

			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;SEND-Routine übergeben.
			ldy	#$00
			jsr	Turbo_PutBytes		;256 Byte an Floppy senden.

			jsr	GetReadError		;Diskettenfehler ?
			beq	:52			;Nein, weiter...

			inc	RepeatFunction		;Fehlerzähler korrigieren.
			cpy	RepeatFunction		;Zähler abgelaufen ?
			beq	:52			;Ja, Ende...
			bcs	:51			;Nein, nochmal schreiben...

::52			jsr	SwapDskNamData		;BAM zurückkonvertieren.
::53			rts
endif

;******************************************************************************
::tmp4 = RD_41!RD_71!RD_81!RD_NM!RD_NM_SCPU
if :tmp4 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock
:xWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Sektor-Transfer initialisieren.
			bcc	:51			;Fehler, Abbrch...

			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.

::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp6 = RL_41!RL_71
if :tmp6 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo
			jsr	InitForIO
			jsr	xWriteBlock
			jmp	DoneWithIO

:xWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR		;Sektor-Transfer initialisieren.
			bcc	:51			;Fehler, Abbrch...

			PushB	r3H			;Register ":r3H" zwischenspeichern.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.
			PopB	r3H			;Register ":r3H" zurücksetzen.

::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp7 = RL_81!RL_NM
if :tmp7 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo
			jsr	InitForIO
			jsr	xWriteBlock
			jmp	DoneWithIO

:xWriteBlock		php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			jsr	TestTrSe_ADDR
			bcc	:51

			jsr	SwapDskNamData		;Diskettenname nach CBM/Standard.
			PushB	r3H			;Register ":r3" retten.
			lda	RL_PartNr		;Partitionsadresse setzen.
			sta	r3H
			jsr	xDsk_SekWrite		;Sektor auf Diskette schreiben.
			PopB	r3H			;Register ":r3" zurücksetzen.
			jsr	SwapDskNamData		;Diskettenname nach GEOS.

::51			plp				;IRQ-Status zurücksetzen.
			txa
			rts
endif

;******************************************************************************
::tmp8 = HD_81_PP!HD_NM_PP
if :tmp8 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Sektor-Adresse testen.
			bcc	:1			; => Fehler, Abbruch...

			jsr	SwapDskNamData

			ldx	#$01
			jsr	TurboRoutine1

			jsr	SwapDskNamData

			ldx	ErrorCode
::1			rts
::2			ldx	#$02
			rts
endif

;******************************************************************************
::tmp9 = HD_41_PP!HD_71_PP
if :tmp9 = TRUE
;******************************************************************************
;*** Sektor in ":diskBlkBuf" auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r4
:xPutBlock_dskBuf	jsr	Set_diskBlkBuf

;*** Sektor von Diskette einlesen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xPutBlock		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
			jmp	DoneWithIO		;I/O abschalten.
::51			rts				;Ende...

;*** Sektor auf Diskette schreiben.
;    Übergabe:		r1 = Track/Sektor.
;			r4 = Zeiger auf Sektorspeicher.
:xWriteBlock		jsr	TestTrSe_ADDR		;Sektor-Adresse testen.
			bcc	:1			; => Fehler, Abbruch...

			ldx	#$01
			jsr	TurboRoutine1

			ldx	ErrorCode
::1			rts
::2			ldx	#$02
			rts
endif
