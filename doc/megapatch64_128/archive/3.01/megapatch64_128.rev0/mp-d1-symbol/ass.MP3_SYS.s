﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			t "G3_SymMacExt"
			t "ass.Drives"
			t "ass.Macro"
			t "ass.System"

if ASS_COMP!ASS_DEMO = TRUE_C64!FALSE
			n "ass.MP3_64_SYS"
endif
if ASS_COMP!ASS_DEMO = TRUE_C64!TRUE
			n "ass.MP3_64D_SYS"
endif
if ASS_COMP!ASS_DEMO = TRUE_C128!FALSE
			n "ass.MP3_128_SYS"
endif
if ASS_COMP!ASS_DEMO = TRUE_C128!TRUE
			n "ass.MP3_128D_SYS"
endif

			c "ass.SysFile V1.0"
			a "M. Kanet"
			f $04

			o $4000

			t "-A3_Prog"
			b $ff
