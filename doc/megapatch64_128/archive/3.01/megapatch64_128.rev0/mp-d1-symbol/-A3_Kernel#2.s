﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

if ASS_COMP = TRUE_C64
;--- Kernel
			b $f0,"src.BuildID.Rev",$00	;Build-ID
			b $f0,"src.GEOS_MP3.64",$00	;Kernel
			b $f0,"src.MakeKernal",$00	;Kernel-Packer
endif

if ASS_COMP = TRUE_C128
;--- Kernel
			b $f0,"src.BuildID.Rev",$00	;Build-ID
			b $f0,"src.GEOS_MP3.128",$00	;Kernel Bank 1
			b $f0,"src.G3_RBasic128",$00	;Externe ToBasic-Routine für Bank 0
			b $f0,"src.G3_B0_128",$00	;Kernel Bank 0
			b $f0,"src.MakeKernal",$00	;Kernel-Packer
endif
