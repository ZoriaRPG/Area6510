﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "GEOS128.3"
			t "G3_SymMacExt"
			t "G3_Boot.V.Class"

			o BASE_GEOS_SYS -2
			p BASE_GEOS_SYS

			i
<MISSING_IMAGE_DATA>

if Sprache = Deutsch
			h "MegaPatch-Kernal"
			h "Zusatzfunktionen..."
endif

if Sprache = Englisch
			h "MegaPatch-kernal"
			h "extended functions..."
endif

;--- Ladeadresse.
:MainInit		w BASE_GEOS_SYS			;DUMMY-Bytes, da Kernal über
							;BASIC-Load eingelesen wird.

;--- Erweiterte MP3-Funktionen.
.x_TaskSwitch		d "obj.TaskSwitch"
.x_ScreenSaver		d "obj.ScreenSaver"
.x_GetBackScrn		d "obj.GetBackScrn"
.x_SpoolPrint		d "obj.SpoolPrinter"
.x_SpoolMenu		d "obj.SpoolMenu"
.x_Register		d "obj.Register"
