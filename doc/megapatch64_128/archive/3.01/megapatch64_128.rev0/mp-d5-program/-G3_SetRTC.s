﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** GEOS-Uhrzeit setzen.
.SetClockGEOS		lda	BootRTCdrive		;Uhrzeit setzen ?
			beq	:53			; => Nein, weiter...
			cmp	#$ff			;AutoMatik ?
			beq	:52			; => Ja, RTC-Gerät suchen.

::51			jsr	FindRTCdrive		;Laufwerk suchen und Zeit setzen.
			txa				;RTC-Fehler ?
			beq	:53			; => Nein, weiter...

;--- Vorgegebenes Laufwerk nicht gefunden.
;    Andere Laufwerke mit RTC-Uhr suchen.
::52			lda	#DrvRAMLink		;RAMLink mit RTC-Uhr suchen.
			jsr	FindRTCdrive
			txa				;RTC-Fehler ?
			beq	:53			; => Nein, weiter...

			lda	#DrvFD			;CMD_FD mit RTC-Uhr suchen.
			jsr	FindRTCdrive
			txa				;RTC-Fehler ?
			beq	:53			; => Nein, weiter...

			lda	#DrvHD			;CMD_HD mit RTC-Uhr suchen.
			jsr	FindRTCdrive
			txa				;RTC-Fehler ?
			beq	:53			; => Nein, weiter...

			lda	#$fe			;SmartMouse mit RTC-Uhr suchen.
			jmp	FindRTCdrive
::53			rts

;*** CMD-Laufwerk mit Echtzeituhr suchen.
.FindRTCdrive		sta	RTC_Type		;Laufwerkstyp speichern.
			cmp	#$fe			;SmartMouse ?
			beq	FindRTC_SM		; => Ja, weiter...

			jsr	PurgeTurbo		;TurboDOS deaktivieren.

			ldx	#$08
::51			stx	RTC_Drive		;Laufwerksadresse einlesen und

			ldy	DriveInfoTab-8,x	;Laufwerk erkennen.
			cpy	RTC_Type		;Laufwerk gefunden ?
			bne	:53			; => Nein, weiter...

			jsr	InitForIO		;I/O-Bereich einblenden.

			ldx	RTC_Drive
			jsr	GetRTCTime		;Uhrzeit einlesen.
			txa				;RTC-Fehler ?
			bne	:52			; => Ja, weiter...

			jsr	SetCPUtime		;Uhrzeit setzen.

			ldx	#NO_ERROR
::52			jsr	DoneWithIO
			txa				;Uhrzeit gesetzt ?
			beq	:54			; => Ja, Ende...

			ldx	RTC_Drive		;Nächstes Laufwerk testen.
::53			inx
			cpx	#29 +1
			bcc	:51
			ldx	#DEV_NOT_FOUND
::54			rts

;*** SmartMouse mit RTC suchen.
.FindRTC_SM		jsr	PurgeTurbo		;TurboDOS abschalten und
			jsr	InitForIO		;I/O-Bereich einblenden.
			jsr	GetRTCmodeSM		;SmartMouse-RTC-abfragen.
			txa				;RTC-Fehler ?
			bne	:51			; => Ja, Ende...
			jsr	SetCPUtime		;Uhrzeit setzen.
			ldx	#NO_ERROR
::51			jmp	DoneWithIO

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** Uhrzeit einlesen.
:GetRTCTime		PushB	curDevice
			stx	curDevice
			jsr	GetRTCData
			PopB	curDevice
			rts

:GetRTCData		ClrB	STATUS			;Befehlskanal zum Gerät öffnen.
			jsr	UNLSN
			lda	curDevice
			jsr	LISTEN
			lda	#$ff
			jsr	SECOND

			lda	STATUS
			beq	:51
			jsr	UNLSN
			ldx	#DEV_NOT_FOUND
			rts

::51			ldy	#$00			;Befehl zum lesen der Uhrzeit an
::52			lda	RTC_GetTime,y		;Laufwerk senden.
			jsr	CIOUT
			iny
			cpy	#$04
			bcc	:52

			jsr	UNLSN

			ClrB	STATUS			;Laufwerk auf "senden" umschalten.
			jsr	UNTALK
			lda	curDevice
			jsr	TALK
			lda	#$ff
			jsr	TKSA

			lda	STATUS
			beq	:53
			jsr	UNTALK
			ldx	#DEV_NOT_FOUND
			rts

::53			ldy	#$00
::54			jsr	ACPTR
			sta	RTC_DATA,y
			iny
			cpy	#$09
			bcc	:54

			pha

::55			lda	STATUS
			bne	:56
			jsr	ACPTR
			jmp	:55

::56			jsr	UNTALK
			pla

			ldx	#DEV_NOT_FOUND
			cmp	#CR
			bne	:57

			ldx	#NO_ERROR
::57			rts

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** SmartMouse auf RTC prüfen und Uhrzeit einlesen.
:GetRTCmodeSM		jsr	SM_RdClk		;Uhrzeit einlesen.

			ldx	RTC_SM_DATA  +0
			cpx	#$ff			;SmartMouse verfügbar ?
			bne	:51			;Nein, übergehen.
			ldx	#DEV_NOT_FOUND
			rts

::51			lda	RTC_SM_DATA  +5		;Wochentag.
			jsr	BCDtoDEZ
			sec
			sbc	#$01
			bcs	:52
			lda	#$00
::52			cmp	#$07
			bcc	:53
			lda	#$06
::53			sta	RTC_DATA +0

			lda	RTC_SM_DATA  +6		;Jahr.
			jsr	BCDtoDEZ
			sta	RTC_DATA +1

			lda	RTC_SM_DATA  +4		;Monat.
			jsr	BCDtoDEZ
			sta	RTC_DATA +2

			lda	RTC_SM_DATA  +3		;Tag.
			jsr	BCDtoDEZ
			sta	RTC_DATA +3

			lda	RTC_SM_DATA  +2		;Stunde.
			jsr	SM_ConvHour1
			sta	RTC_DATA +4
			stx	RTC_DATA +7

			lda	RTC_SM_DATA  +1		;Minute.
			jsr	BCDtoDEZ
			sta	RTC_DATA +5

			lda	RTC_SM_DATA  +0		;Sekunde.
			jsr	BCDtoDEZ
			sta	RTC_DATA +6

			LoadB	RTC_DATA +8,$0d

			ldx	#NO_ERROR
			rts

;*** SmartMouse-Zeitsystem korrigieren.
:SM_ConvHour1		cmp	#%10000000
			bcc	:101
			pha
			and	#%00100000
			tax
			pla
			and	#%00011111
			jmp	BCDtoDEZ

::101			and	#%00111111
			jsr	BCDtoDEZ
			ldx	#$00
			rts

			ldx	#$00
			cmp	#12
			bcc	:102
			dex
			rts

::102			cmp	#$00
			bne	:103
			lda	#12
::103			rts

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** Abfragemodus starten.
:SM_Setup		php
			sei
			sta	RTC_SM_BUF +2
			pla
			sta	RTC_SM_BUF +3
			lda	mport
			sta	RTC_SM_BUF +0
			lda	mpddr
			sta	RTC_SM_BUF +1
			lda	#%11111111
			sta	mport
			lda	#%00001010
			sta	mpddr
			lda	RTC_SM_BUF +2
			rts

;*** Abfragemodus beenden.
:SM_Exit		sta	RTC_SM_BUF +2
			lda	RTC_SM_BUF +0
			sta	mport
			lda	RTC_SM_BUF +1
			sta	mpddr
			lda	RTC_SM_BUF +3
			pha
			lda	RTC_SM_BUF +2
			plp
			rts

;*** Uhrzeit einlesen.
:SM_RdClk		jsr	SM_Setup
			lda	#$bf			;burst rd clk cmd
			jsr	SM_SendCom		;send it
			ldy	#00
::101			jsr	SM_GetByte
			sta	RTC_SM_DATA,y
			iny
			cpy	#$08
			bcc	:101
			jsr	SM_End1
			jmp	SM_Exit

;*** Befehl an SmartMouse senden.
:SM_SendCom		pha
			jsr	SM_Init1		;SM_Output
			pla
:SM_Com1		jsr	SM_SendByte
			jmp	SM_Input

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** Byte von SmartMouse einlesen.
:SM_GetByte		ldx	#08
::101			jsr	SM_Init3
			lda	mport
			lsr
			lsr
			lsr
			ror	RTC_SM_BUF +2
			jsr	SM_End2
			dex
			bne	:101
			lda	RTC_SM_BUF +2
			rts

;*** Byte an SmartMouse senden.
:SM_SendByte		sta	RTC_SM_BUF +2
			ldx	#08
::101			jsr	SM_Init3
			lda	#00
			ror	RTC_SM_BUF +2
			rol
			asl
			asl
			ora	#%11110001		;set io bit
			sta	mport
			jsr	SM_End2
			dex
			bne	:101
			rts

;*** Warten bis SMartMouse bereit.
:SM_Init1		jsr	SM_Output
			jsr	SM_Init3
:SM_Init2		lda	#%11110111
			b $2c
:SM_Init3		lda	#%11111101
			and	mport
			sta	mport
			rts

;*** SmartMouse deaktivieren.
:SM_End1		lda	#%00001000
			b $2c
:SM_End2		lda	#%00000010
			ora	mport
			sta	mport
			rts

;*** Datenrichtung bestimmen.
:SM_Output		lda	#%00001110
			b $2c
:SM_Input		lda	#%00001010
			sta	mpddr
			rts

;******************************************************************************
;*** GEOS-Uhrzeit setzen.
;******************************************************************************
;*** Neue Uhrzeit setzen.
:SetCPUtime		ldx	#$02
::51			lda	RTC_DATA +1,x		;Datum festlegen.
			sta	year       ,x
			dex
			bpl	:51

			ldx	#19			;<*> Jahr2000-Byte definieren.
			cmp	#99
			bcs	:51a
			inx
::51a			stx	millenium

			lda	RTC_DATA +4		;Stunde nach BCD wandeln.
			ldx	RTC_DATA +7
			bne	:52
			cmp	#12
			bne	:53
			lda	#0
			beq	:53
::52			cmp	#12
			beq	:53
			clc
			adc	#12
::53			sta	RTC_DATA +4
			jsr	DEZtoBCD
			sed				;AM/PM-Flag berechnen.
			cmp	#$13
			bcc	:54
			sbc	#$12
			ora	#%10000000
::54			tax
			and	#%10000000
			sta	r0L
			lda	$dc0b
			ldy	$dc08
			txa
			sta	$dc0b			;Stunde setzen.
			cld

			lda	RTC_DATA +5
			jsr	DEZtoBCD		;Minute nach BCD wandeln.
			sta	$dc0a			;Minute setzen.

			lda	RTC_DATA +6
			jsr	DEZtoBCD		;Sekunde nach BCD wandeln.
			sta	$dc09			;Sekunde setzen.

			ClrB	$dc08
			rts

;*** Dezimal nach BCD.
:DEZtoBCD		ldx	#0
::101			cmp	#10
			bcc	:102
			inx
			sbc	#10
			bcs	:101
::102			sta	r0L
			txa
			asl
			asl
			asl
			asl
			ora	r0L
			rts

;*** BCD nach Dezimal wandeln.
:BCDtoDEZ		pha
			and	#%11110000
			lsr
			lsr
			lsr
			lsr
			tay
			lda	#$00
			cpy	#$00
			beq	:102
::101			clc
			adc	#10
			dey
			bne	:101
::102			sta	r0L
			pla
			and	#%00001111
			clc
			adc	r0L
			rts
