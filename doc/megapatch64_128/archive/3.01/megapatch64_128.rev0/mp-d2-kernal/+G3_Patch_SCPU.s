﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;*** SuperCPU-I/O-Routinen.
;******************************************************************************
:s_FillRam		php
			sei
			txa				;X-Register zwischenspeichern.
			pha				;(Original-Routine verwendet
							; nur A/Y-Register!)
			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a2,$00,$00			;ldx #$0000
			b $a4,$04			;ldy r1
			b $a5,$06			;lda r2L
			b $99,$00,$00			;sta $0000,y  --> ::1
			b $c8				;iny
			b $e8				;inx
			b $e4,$02			;cpx r0
			b $d0,$f7			;bne :1

			b $38				;sec
			b $fb				;xce

			pla
			tax
			plp
			rts

;*** Inline: Speicher verschieben.
:s_i_MoveData		pla
			sta	returnAddress +0
			pla
			sta	returnAddress +1
			jsr	Get2Word1Byte
			iny
			lda	(returnAddress),y
			sta	r2H
			jsr	s_MoveData
			jmp	Exit7ByteInline

;*** Speicherbereich veschieben.
:s_MoveData		lda	r2L
			ora	r2H			;Anzahl Bytes = $0000 ?
			beq	:1			;Ja, -> Keine Funktion.

			txa				;X-Register sichern.
			pha
			jsr	DoMove			;MoveData ausführen.
			pla
			tax				;X-Register zurücksetzen.

::1			rts

:DoMove			php				;IRQ sperren.
			sei

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$02			;lda $0002
			b $c5,$04			;cmp $0004
			b $90,$0e			;bcc CopyDown

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$02			;ldx $0002
			b $a4,$04			;ldy $0004

			b $54,$00,$00			;mvn $00.$00

			b $38				;sec
			b $fb				;xce
			plp				;(Pause/Umschalten auf 8Bit!!)
			rts

:CopyDown		b $a5,$06			;lda $0006
			b $3a				;dea
			b $48				;pha
			b $18				;clc
			b $65,$02			;adc $0002
			b $aa				;tax
			b $68				;pla
			b $48				;pha
			b $18				;clc
			b $65,$04			;adc $0004
			b $a8				;tay
			b $68				;pla

			b $44,$00,$00			;mvp $00.$00

			b $38				;sec
			b $fb				;xce
			plp				;(Pause/Umschalten auf 8Bit!!)
			rts

;******************************************************************************
;*** Neue SuperCPU-I/O-Routinen.
;******************************************************************************
:s_InitForIO		jsr	:4			;InitForIO aufrufen.

			lda	$d0b8			;Aktuellen Takt einlesen und
			sta	s_DoneWithIO +1		;zwischenspeichern.

			ldy	curDrive
			lda	RealDrvMode -8,y
			and	#SET_MODE_FASTDISK	;Laufwerkstyp RAM ?
			bne	:3			;Ja, weiter...

			ldy	#4			;wenn 64Net Treiber aktiv
::1			lda	:Net,y			;dann ebenfalls auf 20Mhz
			cmp	$904f,y			;bleiben
			bne	:2			;Im Treiber ab $904f steht
			dey				;'64NET'
			bpl	:1
			rts
::Net			b	"64NET"

::2			sta	$d07a			;Umschalten auf 1Mhz.
::3			rts

::4			jmp	($9000)			;InitForIO aufrufen.

:s_DoneWithIO		lda	#$ff			;Speed-Flag testen.
			bmi	:1			;War 20Mhz-Modus aktiv ?
			sta	$d07b			; => Ja, auf 20Mhz umschalten.
::1			jmp	($9002)			;DoneWithIO aufrufen.

;******************************************************************************
;*** GEOS optimieren.
;******************************************************************************
:s_SCPU_OptOn		ldy	#$00			;Flag: GEOS-Optimieren.
			b	$2c
:s_SCPU_OptOff		ldy	#$03			;Flag: GEOS nicht optimieren.
			sty	Flag_Optimize		;Modus merken.

:s_SCPU_SetOpt		php
			sei				;IRQ abschalten.

			ldx	MMU
			lda	#$7e
			sta	MMU

			ldy	Flag_Optimize		;Modus merken.
			sta	$d07e			;Hardware-Register einschalten.
			bne	:1			;>keine Optimierung
			lda	graphMode
			bmi	:2			;>80 Zeichenmodus
			lda	#%00000010		;40/80 Zeichen Optimierung
			b	$2c
::2			lda	#%10000100		;nur 80 Zeichen Optimierung
							;-> schneller
			b	$2c			;aber 40Zeichen nicht
							;funktionstüchtig!!
::1			lda	#%11000001
			sta	$d0b3
			sta	$d07f			;Hardware-Register ausschalten.

			stx	MMU

			plp				;IRQ zurücksetzen.
			rts
