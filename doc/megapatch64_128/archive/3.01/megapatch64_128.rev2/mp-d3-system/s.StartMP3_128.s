﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "StartMP3_128"
			t "G3_SymMacExt"

			c "StartMP_128 V2.1"
			a "M.Kanet/W.Grimm"
			f APPLICATION
			z $40

			o $0400
			p MainMenu

			i
<MISSING_IMAGE_DATA>

;*** Infoblock definieren.
if Sprache = Deutsch
			h "Installiert MegaPatch128..."
endif
if Sprache = Englisch
			h "Install MegaPatch128..."
endif

;******************************************************************************
;*** Programmteile einbinden.
;******************************************************************************
;*** Dateiliste einbinden.
			t "-G3_FilesMP3"

;*** Zeichensatz einbinden.
			t "-G3_UseFontG3"

;*** Prüfsummen-Routine.
;    Hier wird eine eigene Routine eingebunden, da nicht auszuschließen
;    ist das andere GEOS-Versionen andere CRC-Ergebnisse liefern.
			t "-G3_PatchCRC"

;*** Hauptprogramm.
			t "-S3_Shared"

;*** Dialogboxen.
			t "-S3_DlgBox"

;*** Systemtexte.
			t "-S3_Text"

;*** Text für Copyright-Hinweis.
:LOGO_TEXT		b PLAINTEXT,BOLDON
			b GOTOXY
			w (LOGO_2_x *8 +8) ! DOUBLE_W
			b $08
			b "(c)1999 Markus Kanet"
			b GOTOXY
			w (LOGO_2_x *8 +8) ! DOUBLE_W
			b $12
			b "(c)2003 Megacom Software"
			b GOTOXY
			w (LOGO_2_x *8 +8) ! DOUBLE_W
			b $1c
			b "        www.C64.MCSoft.de"
:Build_ID		b GOTOXY
			w (LOGO_2_x *8 +8) ! DOUBLE_W
			b $26
			b "BUILD:"

			b "FULL-"
			d "obj.BuildID"
			b NULL

;*** System-Icons.
			t "-S3_Icons"

;*** Archiv-Informationen.
:PatchDataTS		b $00,$00
			b $00,$00
			b $00,$00
			b $00,$00
			b $00,$00
:PatchInfoTS		b $00,$00
			b $00,$00
			b $00,$00
			b $00,$00
			b $00,$00

:PatchSizeKB		b $00
			b $00
			b $00
			b $00
			b $00

;*** Startadressen der Dateien innerhalb des Archivs.
:PackFileSAdr		s (MP3_Files   * 32) + 4
:PackFileSAdr_1		= PackFileSAdr
:PackFileSAdr_2		= PackFileSAdr_1 + (FileCount_1 *  4)
:PackFileSAdr_3		= PackFileSAdr_2 + (FileCount_2 *  4)
:PackFileSAdr_4		= PackFileSAdr_3 + (FileCount_3 *  4)
:PackFileSAdr_5		= PackFileSAdr_4 + (FileCount_4 *  4)

:PackFileVecAdr		w PackFileSAdr_1
			w PackFileSAdr_2
			w PackFileSAdr_3
			w PackFileSAdr_4
			w PackFileSAdr_5

:CRC_CODE		w $0000				;Prüfsumme.
:FNameTab1
:CopyBuffer		= FNameTab1      + (MP3_Files   * 32) + 1 +3
:DskDvVLIR		= CopyBuffer     + 256
:DskDvVLIR_org		= DskDvVLIR      + 256
:DskInfTab		= DskDvVLIR_org  + 256
:DskInf_VLIR		= DskInfTab      + 2*254
:DskInf_Modes		= DskInfTab      + 3*254
:DskInf_VlirSet		= DskInfTab      + 3*254 +64
:DskInf_Names		= DskInfTab      + 3*254 +64 +64*2
:FreeSekTab		= DskInfTab      + 3*254 +64 +64*2 +64*17
