﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Setup starten.
:Icon_Tab0		b $01
			w $0000
			b $00

			w Icon_07
			b $1c ! DOUBLE_B,$90,Icon_07x ! DOUBLE_B,Icon_07y
			w SlctTarget

;*** Ziel-Laufwerk wählen.
:Icon_Tab1		b $05
			w $0000
			b $00

			w Icon_08
			b $04 ! DOUBLE_B,$70,Icon_08x ! DOUBLE_B,Icon_08y
			w SlctDrvA

			w Icon_08
			b $04 ! DOUBLE_B,$98,Icon_08x ! DOUBLE_B,Icon_08y
			w SlctDrvB

			w Icon_08
			b $14 ! DOUBLE_B,$70,Icon_08x ! DOUBLE_B,Icon_08y
			w SlctDrvC

			w Icon_08
			b $14 ! DOUBLE_B,$98,Icon_08x ! DOUBLE_B,Icon_08y
			w SlctDrvD

			w Icon_14
			b $20 ! DOUBLE_B,$50,Icon_14x ! DOUBLE_B,Icon_14y
			w SlctTarget

;*** Installationsmodus wählen.
:Icon_Tab2		b $02
			w $0000
			b $00

			w Icon_00
			b $03 ! DOUBLE_B,$70,Icon_00x ! DOUBLE_B,Icon_00y
			w CopyAllFiles

			w Icon_01
			b $03 ! DOUBLE_B,$90,Icon_01x ! DOUBLE_B,Icon_01y
			w CopySlctFiles

;*** Nicht genügend freier Speicher.
:Icon_Tab3		b $02
			w $0000
			b $00

			w Icon_07
			b $03 ! DOUBLE_B,$70,Icon_07x ! DOUBLE_B,Icon_07y
			w CopyMenu

			w Icon_09
			b $03 ! DOUBLE_B,$90,Icon_09x ! DOUBLE_B,Icon_09y
			w SlctTarget

;*** Benutzerdefinierte Installation.
:Icon_Tab4		b $06
			w $0000
			b $00

			w Icon_02
			b $06 ! DOUBLE_B,$68,Icon_02x ! DOUBLE_B,Icon_02y
			w CopySystem

			w Icon_03
			b $11 ! DOUBLE_B,$68,Icon_03x ! DOUBLE_B,Icon_03y
			w CopyRBoot

			w Icon_06
			b $1c ! DOUBLE_B,$68,Icon_06x ! DOUBLE_B,Icon_06y
			w CopyDskDvMenu

			w Icon_04
			b $06 ! DOUBLE_B,$90,Icon_04x ! DOUBLE_B,Icon_04y
			w CopyBackScrn

			w Icon_05
			b $11 ! DOUBLE_B,$90,Icon_05x ! DOUBLE_B,Icon_05y
			w CopyScrSaver

			w Icon_07
			b $1c ! DOUBLE_B,$90,Icon_07x ! DOUBLE_B,Icon_07y
			w RunMP3Menu

;*** Alle Laufwerkstreiber kopieren ?
:Icon_Tab5		b $02
			w $0000
			b $00

			w Icon_00
			b $03 ! DOUBLE_B,$70,Icon_00x ! DOUBLE_B,Icon_00y
			w CopyDskDev

			w Icon_01
			b $03 ! DOUBLE_B,$90,Icon_01x ! DOUBLE_B,Icon_01y
			w CopySlctDkDv

;*** Laufwerkstreiber auswählen.
:Icon_Tab6		b $04
			w $0000
			b $00

			w Icon_10
			b $04 ! DOUBLE_B,$90,Icon_10x ! DOUBLE_B,Icon_10y
			w NextDkDrv

			w Icon_11
			b $0d ! DOUBLE_B,$90,Icon_11x ! DOUBLE_B,Icon_11y
			w ReSlctDkDrv

			w Icon_07
			b $16 ! DOUBLE_B,$90,Icon_07x ! DOUBLE_B,Icon_07y
			w ModifyDriver

			w Icon_12
			b $1f ! DOUBLE_B,$90,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** Startdiskette untersuchen.
:Icon_Tab7		b $02
			w $0000
			b $00

			w Icon_02
			b $06 ! DOUBLE_B,$78,Icon_02x ! DOUBLE_B,Icon_02y
			w CheckFiles

			w Icon_12
			b $1c ! DOUBLE_B,$78,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** MegaPatch starten.
:Icon_Tab8		b $02
			w $0000
			b $00

			w Icon_07
			b $06 ! DOUBLE_B,$78,Icon_07x ! DOUBLE_B,Icon_07y
			w InstallMP

			w Icon_12
			b $1c ! DOUBLE_B,$78,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** Dateien fehlen, MegaPatch starten.
:Icon_Tab9		b $03
			w $0000
			b $00

			w Icon_02
			b $06 ! DOUBLE_B,$78,Icon_02x ! DOUBLE_B,Icon_02y
			w CopyMenu

			w Icon_07
			b $11 ! DOUBLE_B,$78,Icon_07x ! DOUBLE_B,Icon_07y
			w InstallMP

			w Icon_12
			b $1c ! DOUBLE_B,$78,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** Fehlende Dateien kopieren.
:Icon_Tab10		b $02
			w $0000
			b $00

			w Icon_02
			b $06 ! DOUBLE_B,$78,Icon_02x ! DOUBLE_B,Icon_02y
			w CopyMenu

			w Icon_12
			b $1c ! DOUBLE_B,$78,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** Vorhandene Installation löschen.
:Icon_Tab11		b $03
			w $0000
			b $00

			w Icon_13
			b $06 ! DOUBLE_B,$78,Icon_13x ! DOUBLE_B,Icon_13y
			w DeleteSysFiles

			w Icon_07
			b $11 ! DOUBLE_B,$78,Icon_07x ! DOUBLE_B,Icon_07y
			w CopyFiles

			w Icon_12
			b $1c ! DOUBLE_B,$78,Icon_12x ! DOUBLE_B,Icon_12y
			w ExitToDeskTop

;*** Systemtexte.
if Sprache = Deutsch
:NoDrvText		b PLAINTEXT,"Laufwerk ?",NULL
:NoDskText		b PLAINTEXT,"Diskette ?",NULL
:KFreeText		b PLAINTEXT,"Kb frei",NULL
endif
if Sprache = Englisch
:NoDrvText		b PLAINTEXT,"Drive ?",NULL
:NoDskText		b PLAINTEXT,"Disk ?",NULL
:KFreeText		b PLAINTEXT,"Kb free",NULL
endif

:InfoText2		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $3a
if Sprache = Deutsch
			b "Freier Speicher auf Ziel-Diskette: "
endif
if Sprache = Englisch
			b "Free space on target-disk: "
endif
			b NULL

:ExtractFName		b PLAINTEXT
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "Entpacke Datei: "
endif
if Sprache = Englisch
			b "Extract file: "
endif
			b NULL

;*** Texte für Diskettenfehler.
:DskErrInfText		b PLAINTEXT,BOLDON
			b GOTOXY
			w $0050 ! DOUBLE_W
			b $74
if Sprache = Deutsch
			b "Fehler-Code:"
endif
if Sprache = Englisch
			b "Error-code:"
endif
			b NULL

:DskErrCode		b $00

:DskErrTitel		b PLAINTEXT,BOLDON
if Sprache = Deutsch
			b "Installation fehlgeschlagen:"
endif
if Sprache = Englisch
			b "Installation has failed:"
endif
			b NULL

:DlgInfoTitel		b PLAINTEXT,BOLDON
			b "Information:"
			b NULL

if Sprache = Deutsch
:DlgT_01_01		b "Unbekannter Fehler!",NULL
:DlgT_02_01		b "Die GEOS-ID konnte nicht",NULL
:DlgT_02_02		b "gespeichert werden!",NULL
:DlgT_03_01		b "Die Datei konnte nicht",NULL
:DlgT_03_02		b "entpackt werden!",NULL
endif
if Flag64_128 ! Sprache = TRUE_C64 ! Deutsch
:DlgT_04_01		b "Die Datei 'StartMP3_64'",NULL
endif
if Flag64_128 ! Sprache = TRUE_C128 ! Deutsch
:DlgT_04_01		b "Die Datei 'StartMP3_128'",NULL
endif
if Sprache = Deutsch
:DlgT_04_02		b "ist fehlerhaft!",NULL
:DlgT_05_01		b "Prüfsummenfehler in",NULL
endif
if Flag64_128 ! Sprache = TRUE_C64 ! Deutsch
:DlgT_05_02		b "Datei 'StartMP3_64'!",NULL
endif
if Flag64_128 ! Sprache = TRUE_C128 ! Deutsch
:DlgT_05_02		b "Datei 'StartMP3_128'!",NULL
endif
if Sprache = Deutsch
:DlgT_06_01		b "Datei nicht gefunden:",NULL
:DlgT_06_02		b "Bitte Diskette wenden!",NULL
endif

if Sprache = Englisch
:DlgT_01_01		b "Unknown Diskerror!",NULL
:DlgT_02_01		b "Not able to write GEOS-ID",NULL
:DlgT_02_02		b "to Systemdisk!",NULL
:DlgT_03_01		b "Not able to extract",NULL
:DlgT_03_02		b "this file!",NULL
endif
if Flag64_128 ! Sprache = TRUE_C64 ! Englisch
:DlgT_04_01		b "The file 'StartMP3_64'",NULL
endif
if Flag64_128 ! Sprache = TRUE_C128 ! Englisch
:DlgT_04_01		b "The file 'StartMP3_128'",NULL
endif
if Sprache = Englisch
:DlgT_04_02		b "is partly destroyed!",NULL
:DlgT_05_01		b "Checksum-error in",NULL
endif
if Flag64_128 ! Sprache = TRUE_C64 ! Englisch
:DlgT_05_02		b "file 'StartMP3_64'!",NULL
endif
if Flag64_128 ! Sprache = TRUE_C128 ! Englisch
:DlgT_05_02		b "file 'StartMP3_128'!",NULL
endif
if Sprache = Englisch
:DlgT_06_01		b "File not found:",NULL
:DlgT_06_02		b "Please flip disk!",NULL
endif

:InfoText0		b PLAINTEXT
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Bitte haben Sie einen kleinen Augenblick Geduld,"
endif
if Sprache = Englisch
			b "Please have a small moment patience while SetupMP"
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "während -SetupMP- das Archiv mit den gepackten"
endif
if Sprache = Englisch
			b "examines the archive with the packed systemfiles."
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $70
if Sprache = Deutsch
			b "MegaPatch-Dateien untersucht."
endif
if Sprache = Englisch
			b ""
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $80
if Sprache = Deutsch
			b "Dieser Vorgang kann einige Minuten dauern..."
endif
if Sprache = Englisch
			b "This process can last some minutes."
endif

:InfoText0a		b GOTOXY
			w $0010 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "* Archiv auf Fehler untersuchen..."
endif
if Sprache = Englisch
			b "* Testing archive...",NULL
endif
			b NULL

:InfoText0b		b GOTOXY
			w $0010 ! DOUBLE_W
			b $a8
if Sprache = Deutsch
			b "* Datei-Informationen einlesen..."
endif
if Sprache = Englisch
			b "* Get systemfile-informations...",NULL
endif
			b NULL

:InfoText0c		b GOTOXY
			w $0010 ! DOUBLE_W
			b $94
if Sprache = Deutsch
			b "Installationsdatei: "
endif
if Sprache = Englisch
			b "Installationfile: ",NULL
endif
			b NULL

;*** Setup starten.
:Icon_Text0		b PLAINTEXT
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Installationsprogramm für GEOS-MegaPatch128"
endif
if Sprache = Englisch
			b "Installationprogram for GEOS - MegaPatch128"
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $70
if Sprache = Deutsch
			b "Das Programm  wird Sie während der Installation"
endif
if Sprache = Englisch
			b "This programm will help you  to install the GEOS-"
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $78
if Sprache = Deutsch
			b "des GEOS-MegaPatch unterstützen."
endif
if Sprache = Englisch
			b "MegaPatch on your computer."
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $88
if Sprache = Deutsch
			b "Mit der Taste '!' kann der Installationsvorgang"
endif
if Sprache = Englisch
			b "If you want to cancel the MegaPatch-installation"
endif
			b GOTOXY
			w $0010 ! DOUBLE_W
			b $90
if Sprache = Deutsch
			b "jederzeit beendet werden."
endif
if Sprache = Englisch
			b "please press the '!'-key."
endif

			b GOTOXY
			w $00d2 ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $00d7 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif
			b NULL

;*** Ziel-Laufwerk wählen.
:Icon_Text1		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Wählen Sie das Laufwerk auf das die"
endif
if Sprache = Englisch
			b "Please choose the drive, on which the"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Systemdateien kopiert werden sollen:"
endif
if Sprache = Englisch
			b "system-files should be copied:"
endif

			b GOTOXY
			w $0023 ! DOUBLE_W
			b $84
			b "A:"
			b GOTOXY
			w $0023 ! DOUBLE_W
			b $ac
			b "B:"
			b GOTOXY
			w $00a3 ! DOUBLE_W
			b $84
			b "C:"
			b GOTOXY
			w $00a3 ! DOUBLE_W
			b $ac
			b "D:"
			b NULL

;*** Installationsmodus wählen.
:Icon_Text2		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Installationsprogramm für GEOS - MegaPatch128"
endif
if Sprache = Englisch
			b "Installationprogram for GEOS - MegaPatch128"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Bitte wählen Sie die Art der Installation:"
endif
if Sprache = Englisch
			b "Please choose the type of installation:"
endif

			b GOTOXY
			w $0048 ! DOUBLE_W
			b $76
if Sprache = Deutsch
			b "Komplette Installation mit allen Dateien"
endif
if Sprache = Englisch
			b "Complete installation with all files"
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $7e
if Sprache = Deutsch
			b "auf Diskette oder CMD-Partition."
endif
if Sprache = Englisch
			b "on disk or CMD-partition."
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Benutzerdefinierte Installation."
endif
if Sprache = Englisch
			b "User defined installation / update an"
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "Aktualisieren einer vorhandenen"
endif
if Sprache = Englisch
			b "existing system-disk."
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $a6
if Sprache = Deutsch
			b "Startdiskette."
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $ae
			b "(Empfohlen für 1541-Startdisketten)"
endif
if Sprache = Englisch
			b "(Recommended for 1541-installation)"
endif
			b NULL

;*** Vorhandene Installation löschen.
:Icon_Text11		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Das Ziel-Laufwerk enthält bereits Dateien"
endif
if Sprache = Englisch
			b "The target-drive already contains files of"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "des GEOS-MegaPatch. Sollen die vorhandenen"
endif
if Sprache = Englisch
			b "the GEOS-MegaPatch. Should the existing files"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "Dateien gelöscht werden ?"
endif
if Sprache = Englisch
			b "be deleted ?"
endif

			b GOTOXY
			w $001f ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Systemdateien"
endif
if Sprache = Englisch
			b "Delete"
endif
			b GOTOXY
			w $0030 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "löschen"
endif
if Sprache = Englisch
			b "Systemfiles"
endif

			b GOTOXY
			w $007a ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $7f ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif

			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

;*** Nicht genügend freier Speicher.
:Icon_Text3		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Nicht genügend freier Speicher verfügbar"
endif
if Sprache = Englisch
			b "Not enough space available on the"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "um alle Dateien zu entpacken!"
endif
if Sprache = Englisch
			b "selected target-drive!"
endif

			b GOTOXY
			w $0048 ! DOUBLE_W
			b $76
if Sprache = Deutsch
			b "Installation forsetzen und nicht"
endif
if Sprache = Englisch
			b "Continue with installation and copy"
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $7e
if Sprache = Deutsch
			b "alle Dateien kopieren."
endif
if Sprache = Englisch
			b "only selected files."
endif

			b GOTOXY
			w $0048 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Ein anderes Laufwerk für die"
endif
if Sprache = Englisch
			b "Choose another target-drive and try"
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "Installation wählen."
endif
if Sprache = Englisch
			b "installation again."
endif
			b NULL

:Icon_Text4		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Kopieren Sie jetzt die MegaPatch-Dateien. Ein"
endif
if Sprache = Englisch
			b "Copy the MegaPatch-systemfiles now. A '*'"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "'*' symbolisiert benötigte Systemdateien."
endif
if Sprache = Englisch
			b "symbolizes required start-files."
endif

			b GOTOXY
			w $0028 ! DOUBLE_W
			b $70
			b "*"
			b GOTOXY
			w $0020 ! DOUBLE_W
			b $86
if Sprache = Deutsch
			b "Startdateien"
endif
if Sprache = Englisch
			b "Start-files"
endif

			b GOTOXY
			w $0078 ! DOUBLE_W
			b $86
if Sprache = Deutsch
			b "ReBoot-System"
endif
if Sprache = Englisch
			b "ReBoot-system"
endif

			b GOTOXY
			w $00d8 ! DOUBLE_W
			b $70
			b "*"
			b GOTOXY
			w $00da ! DOUBLE_W
			b $86
if Sprache = Deutsch
			b "Laufwerks-"
endif
if Sprache = Englisch
			b "DiskDriver"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $8e
if Sprache = Deutsch
			b "treiber"
endif
if Sprache = Englisch
			b ""
endif

			b GOTOXY
			w $0021 ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Hintergrund-"
endif
if Sprache = Englisch
			b "Background-"
endif
			b GOTOXY
			w $0033 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "Bilder"
endif
if Sprache = Englisch
			b "Pictures"
endif

			b GOTOXY
			w $007f ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Bildschirm-"
endif
if Sprache = Englisch
			b "ScreenSaver"
endif
			b GOTOXY
			w $0087 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "schoner"
endif
if Sprache = Englisch
			b ""
endif

			b GOTOXY
			w $00d2 ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $00d7 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif
			b NULL

;*** Alle Laufwerkstreiber kopieren ?
:Icon_Text5		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Bitte wählen Sie den Modus zum Kopieren der"
endif
if Sprache = Englisch
			b "Please choose the copy-mode for the MegaPatch"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "einzelnen Treiber für Diskettenlaufwerke:"
endif
if Sprache = Englisch
			b "disk-driver-installation:"
endif

			b GOTOXY
			w $0048 ! DOUBLE_W
			b $76
if Sprache = Deutsch
			b "Alle Laufwerkstreiber kopieren"
endif
if Sprache = Englisch
			b "Copy all disk-drivers"
endif

			b GOTOXY
			w $0048 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Nur bestimmte Laufwerkstreiber"
endif
if Sprache = Englisch
			b "Copy selected disk-drivers only"
endif
			b GOTOXY
			w $0048 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "für die Installation wählen."
endif
if Sprache = Englisch
			b ""
endif
			b NULL

;*** Laufwerkstreiber auswählen.
:Icon_Text6		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Soll der folgende Laufwerkstreiber auf der"
endif
if Sprache = Englisch
			b "Should the following disk-driver be installed"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Startdiskette installiert werden ?"
endif
if Sprache = Englisch
			b "on the startdisk ?"
endif
			b GOTOXY
			w $0020 ! DOUBLE_W
			b $70
if Sprache = Deutsch
			b "Laufwerkstreiber für"
endif
if Sprache = Englisch
			b "Disk-driver for"
endif

			b GOTOXY
			w $001d ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Kopieren"
endif
if Sprache = Englisch
			b "Copy"
endif

			b GOTOXY
			w $006f ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Nicht"
endif
if Sprache = Englisch
			b "Do not"
endif
			b GOTOXY
			w $0066 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "Kopieren"
endif
if Sprache = Englisch
			b "Copy"
endif

			b GOTOXY
			w $00a2 ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $00a7 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif

			b GOTOXY
			w $00f8 ! DOUBLE_W
			b $ae
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00f9 ! DOUBLE_W
			b $b6
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

:Icon_Text6a		b PLAINTEXT
if Sprache = Deutsch
			b " - Laufwerk ?"
endif
if Sprache = Englisch
			b " - drive ?"
endif
			b NULL

;*** Startdiskette untersuchen.
:Icon_Text7		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Das kopieren der Systemdateien ist beendet."
endif
if Sprache = Englisch
			b "Systemfiles were copied. The startdisk"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Die Startdiskette wird jetzt auf fehlende"
endif
if Sprache = Englisch
			b "is now examined for lacking files."
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "Dateien untersucht."
endif
if Sprache = Englisch
			b ""
endif

			b GOTOXY
			w $0020 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Startdiskette"
endif
if Sprache = Englisch
			b "Check"
endif
			b GOTOXY
			w $0025 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "überprüfen"
endif
if Sprache = Englisch
			b "systemfiles"
endif

			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

;*** MegaPatch starten.
:Icon_Text8		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Die Diskette wurde überprüft und alle Dateien"
endif
if Sprache = Englisch
			b "The disk was checked and all files of the GEOS-"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "des MegaPatch sind vorhanden. Sie können die"
endif
if Sprache = Englisch
			b "MegaPatch  were found.  You can  now continue"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "Installation jetzt fortsetzen."
endif
if Sprache = Englisch
			b "with the installation."
endif

			b GOTOXY
			w $0021 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $0026 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

;*** Dateien fehlen, DeskTop starten.
:Icon_Text9		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Es wurden nicht alle Systemdateien auf der"
endif
if Sprache = Englisch
			b "Not all system-files were found, however you"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Startdiskette  gefunden.  Die  Installation"
endif
if Sprache = Englisch
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "kann jedoch fortgesetzt werden."
endif
if Sprache = Englisch
			b "can continue with the installation."
endif

			b GOTOXY
			w $001e ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Systemdateien"
endif
if Sprache = Englisch
			b "Copy"
endif
			b GOTOXY
			w $002c ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "kopieren"
endif
if Sprache = Englisch
			b "systemfiles"
endif
			b GOTOXY
			w $007a ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Installation"
endif
if Sprache = Englisch
			b "Continue with"
endif
			b GOTOXY
			w $007e ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "fortsetzen"
endif
if Sprache = Englisch
			b "installation"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

;*** Fehlende Dateien kopieren.
:Icon_Text10		b PLAINTEXT
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $58
if Sprache = Deutsch
			b "Es wurden  nicht alle  Systemdateien  auf der"
endif
if Sprache = Englisch
			b "Not all system-files were found. MegaPatch"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $60
if Sprache = Deutsch
			b "Startdiskette gefunden. Die Diskette ist nicht"
endif
if Sprache = Englisch
			b "cannot be started by this disk."
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $68
if Sprache = Deutsch
			b "startfähig. Bitte Systemdateien ergänzen."
endif
if Sprache = Englisch
			b ""
endif

			b GOTOXY
			w $0020 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "Systemdateien"
endif
if Sprache = Englisch
			b "Copy"
endif
			b GOTOXY
			w $002c ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "kopieren"
endif
if Sprache = Englisch
			b "systemfiles"
endif

			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $96
if Sprache = Deutsch
			b "DeskTop"
endif
if Sprache = Englisch
			b "Open"
endif
			b GOTOXY
			w $00e1 ! DOUBLE_W
			b $9e
if Sprache = Deutsch
			b "starten"
endif
if Sprache = Englisch
			b "DeskTop"
endif
			b NULL

;*** Information über Kopierstatus.
:Inf_Wait		b GOTOXY
			w $0018 ! DOUBLE_W
			b $5a
			b PLAINTEXT,OUTLINEON
if Sprache = Deutsch
			b "Bitte warten!"
endif
if Sprache = Englisch
			b "Please wait!"
endif
			b GOTOXY
			w $0018 ! DOUBLE_W
			b $70
			b PLAINTEXT
			b NULL

if Sprache = Deutsch
:Inf_DelSysFiles	b "Systemdateien werden gelöscht...",NULL
:Inf_CopySystem		b "Systemdateien werden kopiert...",NULL
:Inf_CopyRBoot		b "ReBoot-Routine wird kopiert...",NULL
:Inf_CopyBkScr		b "Hintergrundbild wird kopiert...",NULL
:Inf_CopyScrSv		b "Bildschirmschoner werden kopiert...",NULL
:Inf_CopyDskDrv		b "Laufwerkstreiber werden kopiert...",NULL
:Inf_InstallMP		b "Systemdiskette wird untersucht...",NULL
:Inf_ChkDkSpace		b "Zieldiskette wird überprüft...",NULL
endif
if Sprache = Englisch
:Inf_DelSysFiles	b "Delete Systemfiles...",NULL
:Inf_CopySystem		b "Copy System-files...",NULL
:Inf_CopyRBoot		b "Copy ReBoot-files...",NULL
:Inf_CopyBkScr		b "Copy Background-picture...",NULL
:Inf_CopyScrSv		b "Copy ScreenSaver...",NULL
:Inf_CopyDskDrv		b "Copy DiskDrivers...",NULL
:Inf_InstallMP		b "System-disk is examined...",NULL
:Inf_ChkDkSpace		b "Target-disk is checked...",NULL
endif

:InfoText1		b PLAINTEXT
			b GOTOXY
			w $0020 ! DOUBLE_W
			b $76
if Sprache = Deutsch
			b "Bitte haben Sie einen kleinen Augenblick"
endif
if Sprache = Englisch
			b "Please have a small moment patience, while"
endif
			b GOTOXY
			w $0020 ! DOUBLE_W
			b $80
if Sprache = Deutsch
			b "Geduld, während Setup die Startdiskette"
endif
if Sprache = Englisch
			b "Setup configures your MegaPatch-bootdisk!"
endif
			b GOTOXY
			w $0020 ! DOUBLE_W
			b $8a
if Sprache = Deutsch
			b "für MegaPatch konfiguriert..."
endif
if Sprache = Englisch
			b ""
endif
			b NULL
