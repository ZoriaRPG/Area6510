﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81
::tmp0b = FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp0 = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** Floppy-Routine ohne Parameter aufrufen.
;    Übergabe:		AKKU/xReg, Low/High-Byte der Turbo-Routine.
:xTurboRoutine		stx	$8c			;Zeiger auf Routine nach $8b/$8c
			sta	$8b			;kopieren.
			ldy	#$02			;2-Byte-Befehl.
			bne	InitTurboData		;Befehl ausführen.

;*** Floppy-Programm mit zwei Byte-Parameter starten.
;    Übergabe:		AKKU/xReg, Low/High-Byte der Turbo-Routine.
;			r1L/r1H  , Parameter-Bytes.
:xTurboRoutSet_r1	stx	$8c			;Zeiger auf Routine nach $8b/$8c
			sta	$8b			;kopieren.

;*** Floppy-Programm mit zwei Byte-Parameter starten.
;    Übergabe:		$8B/$8C  , Low/High-Byte der Turbo-Routine.
;			r1L/r1H  , Parameter-Bytes.
:xTurboRoutine_r1	ldy	#$04			;4-Byte-Befehl.

			lda	r1H			;Parameter-Bytes in Init-Befehl
			sta	TurboParameter2 		;kopieren.
			lda	r1L
			sta	TurboParameter1

;*** Turbodaten initialisieren.
;    Übergabe:		$8B/$8C = Zeiger auf TurboRoutine.
;			yReg	 = Anzahl Bytes (Routine+Parameter)
:InitTurboData		lda	$8c			;Auszuführende Routine in
			sta	TurboRoutineH		;Init-Befehl kopieren.
			lda	$8b
			sta	TurboRoutineL

			lda	#> TurboRoutineL
			sta	$8c
			lda	#< TurboRoutineL
			sta	$8b
			jmp	Turbo_PutInitByt

:TurboRoutineL		b $00
:TurboRoutineH		b $00
:TurboParameter1	b $00
:TurboParameter2	b $00
endif

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Getrennte Routinen für C64 und C128 da unterschiedliche
;Register zum RAM- und I/O-Umschalten verwendet werden.
;******************************************************************************
::tmp2 = Flag64_128!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp2 = TRUE_C64!TRUE
;******************************************************************************
;*** Floppy-Routine aufrufen.
;xReg = $00		= TurboDOS deaktivieren
;     = $01		= Sektor schreiben
;     = $02		= Sektor lesen
;     = $03		= Linkbytes lesen
;     = $04		= Fehlerstatus abfragen.
:TurboRoutine1		lda	r4H
			sta	$8c
			lda	r4L
			sta	$8b
			b $2c
:xGetDiskError		ldx	#$04
:TurboRoutine2		stx	TurboMode

			lda	#$00			;Fehlercode löschen.
			sta	ErrorCode

			lda	PP_DOS_ROUT_L,x		;Zeiger auf auszuführende Routine
			sta	TurboRoutineL		;im TurboDOS einlesen und speichern.
			lda	PP_DOS_ROUT_H,x
			sta	TurboRoutineH

			lda	r1L			;Track und Sektor an TurboDOS
			sta	TurboParameter1		;übergeben.
			lda	r1H
			sta	TurboParameter2

			jsr	EN_SET_REC		;RL-Hardware aktivieren.

			lda	$8c			;Zeiger auf Sektorspeicher
			pha				;auf Stack retten.
			lda	$8b
			pha

			lda	#> TurboRoutineL	;TurboDOS-Routine
			sta	$8c			;ausführen.
			lda	#< TurboRoutineL
			sta	$8b
			ldy	#$04
			jsr	Turbo_PutBytes
			pla
			sta	$8b
			pla
			sta	$8c

			ldy	TurboMode		;TurboPP starten?
			beq	:4			; => Ja, Ende...

			dey				;Sektor schreiben?
			bne	:1			; => Nein, weiter...
			jsr	Turbo_PutBytes		;Sektor auf Diskette schreiben und
			jmp	:3			;Fehlerstatus abfragen.

::1			dey				;Sektor lesen?
			beq	:2			; => Ja, weiter...
			dey				;Linkbytes lesen?
			bne	:3			; => Nein, weiter...
			ldy	#$02
::2			jsr	Turbo_GetBytes		;Daten von Diskette lesen.

::3			lda	#> ErrorCode
			sta	$8c
			lda	#< ErrorCode
			sta	$8b
			ldy	#$01
			jsr	Turbo_GetBytes		;Fehlerstatus abfragen.

::4			jsr	RL_HW_DIS2		;RL-Hardware deaktivieren.

			lda	ErrorCode
			cmp	#$02
			bcc	:5
			adc	#$1d
			b $2c
::5			lda	#$00
			tax
			stx	ErrorCode
			rts
endif

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: Getrennte Routinen für C64 und C128 da unterschiedliche
;Register zum RAM- und I/O-Umschalten verwendet werden.
;******************************************************************************
::tmp3 = Flag64_128!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp3 = TRUE_C128!TRUE
;******************************************************************************
;*** Floppy-Routine aufrufen.
;xReg = $00		= TurboDOS deaktivieren
;     = $01		= Sektor schreiben
;     = $02		= Sektor lesen
;     = $03		= Linkbytes lesen
;     = $04		= Fehlerstatus abfragen.
:TurboRoutine1		lda	r4H
			sta	$8c
			lda	r4L
			sta	$8b
			b $2c
:xGetDiskError		ldx	#$04
:TurboRoutine2		stx	TurboMode

			lda	#$00			;Fehlercode löschen.
			sta	ErrorCode

			lda	PP_DOS_ROUT_L,x		;Zeiger auf auszuführende Routine
			sta	TurboRoutineL		;im TurboDOS einlesen und speichern.
			lda	PP_DOS_ROUT_H,x
			sta	TurboRoutineH

			lda	r1L			;Track und Sektor an TurboDOS
			sta	TurboParameter1		;übergeben.
			lda	r1H
			sta	TurboParameter2

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: MMU und RAM_Conf_Reg sichern und RAM-Bank umschalten.
			jsr	L98da

			jsr	EN_SET_REC		;RL-Hardware aktivieren.

			lda	$8c			;Zeiger auf Sektorspeicher
			pha				;auf Stack retten.
			lda	$8b
			pha

			lda	#> TurboRoutineL	;TurboDOS-Routine
			sta	$8c			;ausführen.
			lda	#< TurboRoutineL
			sta	$8b
			ldy	#$04
			jsr	Turbo_PutBytes
			pla
			sta	$8b
			pla
			sta	$8c

			ldy	TurboMode		;TurboPP starten?
			beq	:4			; => Ja, Ende...

			dey				;Sektor schreiben?
			bne	:1			; => Nein, weiter...
			jsr	Turbo_PutBytes		;Sektor auf Diskette schreiben und
			jmp	:3			;Fehlerstatus abfragen.

::1			dey				;Sektor lesen?
			beq	:2			; => Ja, weiter...
			dey				;Linkbytes lesen?
			bne	:3			; => Nein, weiter...
			ldy	#$02
::2			jsr	Turbo_GetBytes		;Daten von Diskette lesen.

::3			lda	#> ErrorCode
			sta	$8c
			lda	#< ErrorCode
			sta	$8b
			ldy	#$01
			jsr	Turbo_GetBytes		;Fehlerstatus abfragen.

;--- Ergänzung: 08.07.18/M.Kanet
;Code-Rekonstruktion: RAMLink deaktivieren und MMU/RAM_Conf_Reg zurücksetzen.
::4			jsr	L98cc

			lda	ErrorCode
			cmp	#$02
			bcc	:5
			adc	#$1d
			b $2c
::5			lda	#$00
			tax
			stx	ErrorCode
			rts
endif

;******************************************************************************
::tmp4 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp4 = TRUE
;******************************************************************************
;*** HD-Kabel-Modus wechseln.
:HD_MODE_SEND		ldx	#$98
			b $2c
:HD_MODE_RECEIVE	ldx	#$88
			lda	$df41
			pha
			lda	$df42
			stx	$df43
			sta	$df42
			pla
			sta	$df41
			rts
endif

;******************************************************************************
::tmp5 = Flag64_128!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp5 = TRUE_C128!TRUE
;******************************************************************************
;--- Ergänzung: 04.07.18/M.Kanet
;Code-Rekonstruktion: In der Version von 2003 wurden folgende Routinen
;ergänzt, vermutlich RAM-Bank-Konfiguration.
:L98cc			jsr	RL_HW_DIS2		;RL-Hardware deaktivieren.
:L98cf			lda	#$00
			sta	RAM_Conf_Reg
:L98d4			lda	#$00
			sta	MMU
			rts

:L98da			lda	MMU
			sta	L98d4+1
			lda	#$4e
			sta	MMU
			lda	RAM_Conf_Reg
			sta	L98cf+1
			and	#%11110000
			ora	#%00000100
			sta	RAM_Conf_Reg
			rts
endif

;******************************************************************************
::tmp6 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp6 = TRUE
;******************************************************************************
;*** Variablen.
:PP_DOS_ROUT_L		b < TD_MLoop_Stop
			b < TD_WriteBlock
			b < TD_ReadBlock
			b < TD_ReadLink
			b < TD_GetError

:PP_DOS_ROUT_H		b > TD_MLoop_Stop
			b > TD_WriteBlock
			b > TD_ReadBlock
			b > TD_ReadLink
			b > TD_GetError

:TurboMode		b $00
:TurboRoutineL		b $00
:TurboRoutineH		b $00
:TurboParameter1	b $00
:TurboParameter2	b $00

:ErrorCode		b $00
endif
