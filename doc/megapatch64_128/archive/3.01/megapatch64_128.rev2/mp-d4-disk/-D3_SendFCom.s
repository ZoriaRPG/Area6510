﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81
::tmp0b = FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp0c = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp0 = :tmp0a!:tmp0b!:tmp0c
if :tmp0 = TRUE
;******************************************************************************
;*** Floppy-Befehl an Laufwerk senden.
;    Übergabe:		r0	= Zeiger auf Floppy-Befehl.
;			r2L	= Anzahl Zeichen in Befehl.
:xSendCommand		lda	r0L
			ldx	r0H
			ldy	r2L
			b $2c

;*** Floppy-Befehl an Laufwerk senden.
;    Sendet genau 5 Bytes!
;    Übergabe:		AKKU/xReg, Zeiger auf Floppy-Befehl.
:SendFloppyCom		ldy	#$05

;*** Floppy-Befehl an Laufwerk senden.
;    Übergabe:		AKKU	= Low -Byte, Zeiger auf Floppy-Befehl.
;			xReg	= High-Byte, Zeiger auf Floppy-Befehl.
;			yReg	= Länge (Zeichen) Floppy-befehl.
:Job_Command		sta	:51 +1			;Zeiger auf Floppy-Befehl sichern.
			stx	:51 +2
			sty	:52 +1

			lda	#$00			;Status-Byte löschen.
			sta	STATUS

			jsr	$ffab
			lda	curDrive
			jsr	$ffb1			;Laufwerk aktivieren.
			bit	STATUS			;Fehler aufgetreten ?
			bmi	:53			;Ja, Abbruch...

			lda	#$ff
			jsr	$ff93			;Laufwerk auf Empfang schalten.
			bit	STATUS			;Fehler aufgetreten ?
			bmi	:53			;Ja, Abbruch...

			ldy	#$00
::51			lda	$ffff,y			;Bytes an Floppy-Laufwerk senden.
			jsr	$ffa8
			iny
::52			cpy	#$05
			bcc	:51
			ldx	#$00
			rts

::53			jsr	$ffae			;Laufwerk abschalten.
			ldx	#$0d			;Fehler: "Kein Laufwerk"...
			rts
endif

;******************************************************************************
::tmp1 = RL_41!RL_71!RL_81!RL_NM
if :tmp1 = TRUE
;******************************************************************************
;*** Floppy-Befehl an Laufwerk senden.
;    Übergabe:		r0	= Zeiger auf Floppy-Befehl.
;			r2L	= Anzahl Zeichen in Befehl.
:xSendCommand		lda	r0L
			ldx	r0H
			ldy	r2L
			b $2c

;*** Floppy-Befehl an Laufwerk senden.
;    Sendet genau 5 Bytes!
;    Übergabe:		AKKU/xReg, Zeiger auf Floppy-Befehl.
:SendFloppyCom		ldy	#$05

;*** Floppy-Befehl an Laufwerk senden.
:Job_Command		sta	:51 +1			;Zeiger auf Floppy-Befehl sichern.
			stx	:51 +2
			sty	:52 +1

			lda	#$00
			sta	STATUS
			lda	RL_DEV_ADDR
			jsr	$ffb1			;Laufwerk aktivieren.
			bit	STATUS			;Laufwerksfehler ?
			bmi	:53			;Ja, Abbruch...
			lda	#$ff
			jsr	$ff93			;Sekundäradresse senden.
			bit	STATUS			;Laufwerksfehler ?
			bmi	:53			;Ja, Abbruch...

			ldy	#$00
::51			lda	$ffff,y			;Bytes an Floppy-Laufwerk senden.
			jsr	$ffa8
			iny
::52			cpy	#$05
			bcc	:51
			ldx	#$00
			rts

::53			jsr	$ffae			;Laufwerk abschalten.
			ldx	#$0d			;Fehler: "Kein Laufwerk"...
			rts
endif
