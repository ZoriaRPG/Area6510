﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren und neue Diskette öffnen.
:xNewDisk		jsr	InitShadowRAM		;ShadowRAM löschen.
			jmp	xLogNewDisk
endif

;******************************************************************************
::tmp1 = C_71!C_81!FD_41!FD_71!FD_81!FD_NM!PC_DOS
if :tmp1 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren und neue Diskette öffnen.
:xNewDisk = xLogNewDisk
endif

;******************************************************************************
::tmp2a = HD_41!HD_71!HD_81!HD_NM!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp2b = RL_41!RL_71!RL_81!RL_NM!RD_41!RD_71!RD_81!RD_NM!RD_NM_SCPU
::tmp2  = :tmp2a!:tmp2b
if :tmp2 = TRUE
;******************************************************************************
;*** TurboDOS aktivieren und neue Diskette öffnen.
:xNewDisk = xEnterTurbo
endif
