﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41!C_71!PC_DOS
if :tmp0 = TRUE
;******************************************************************************
;*** TurboDOS-Routine in FloppyRAM abschalten.
:TurnOffTurboDOS	jsr	InitForIO

			ldx	#> TD_Stop
			lda	#< TD_Stop
			jsr	xTurboRoutine
			jsr	DataClkATN_HIGH

;*** Aktuelles Laufwerk deaktivieren.
:TurnOffCurDrive	lda	curDrive
			jsr	$ffb1
			lda	#$ef
			jsr	$ff93
			jsr	$ffae

			ldx	#$00
			jmp	DoneWithIO
endif

;******************************************************************************
::tmp1 = C_81!FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
if :tmp1 = TRUE
;******************************************************************************
;*** TurboDOS-Routine in FloppyRAM abschalten.
:TurnOffTurboDOS	jsr	InitForIO

			ldx	#> TD_ClearCache
			lda	#< TD_ClearCache
			jsr	xTurboRoutine
			ldx	#> TD_Stop
			lda	#< TD_Stop
			jsr	xTurboRoutine
			jsr	DataClkATN_HIGH

;*** Aktuelles Laufwerk deaktivieren.
:TurnOffCurDrive	lda	curDrive
			jsr	$ffb1
			lda	#$ef
			jsr	$ff93
			jsr	$ffae

			ldx	#$00
			jmp	DoneWithIO
endif

;******************************************************************************
::tmp2 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp2 = TRUE
;******************************************************************************
;*** TurboDOS-Routine in FloppyRAM abschalten.
:TurnOffTurboDOS	jsr	InitForIO
			ldx	#$00
			jsr	TurboRoutine2
			jsr	DataClkATN_HIGH
			jmp	DoneWithIO
endif
