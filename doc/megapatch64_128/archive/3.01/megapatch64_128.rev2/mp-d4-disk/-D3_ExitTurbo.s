﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41!PC_DOS
if :tmp0 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		txa				;xReg zwischenspeichern.
			pha

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$40			;Aktuelle Diskette geöffnet ?
			beq	:51			;Nein, weiter...

			jsr	TurnOffTurboDOS		;TurboDOS abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

::51			pla				;xReg wieder zurücksetzen.
			tax
			rts
endif

;******************************************************************************
::tmp1 = C_71!FD_41!FD_71!HD_41!HD_71!HD_41_PP!HD_71_PP
if :tmp1 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		lda	#$08			;Sektor-Interleave auf
			sta	interleave		;Vorgabewert zurücksetzen.

			txa				;xReg zwischenspeichern.
			pha

			ldx	curDrive
			lda	turboFlags -8,x		;TurboDOS-Status einlesen.
			and	#$40			;TurboDOS aktiviert ?
			beq	:51			;Nein, Ende...

			jsr	TurnOffTurboDOS		;TurboDOS abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

::51			pla				;xReg wieder zurücksetzen.
			tax
			rts
endif

;******************************************************************************
::tmp2 = C_81
if :tmp2 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		txa
			pha

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$40			;Aktuelle Diskette geöffnet ?
			beq	:51			;Nein, weiter...

			jsr	TurnOffTurboDOS		;TurboDOS abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

			jsr	StashDriverData		;Variablen sichern.

::51			pla
			tax
			rts
endif

;******************************************************************************
::tmp3 = FD_81!HD_81!HD_81_PP
if :tmp3 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		lda	#$08			;Sektor-Interleave auf
			sta	interleave		;Vorgabewert zurücksetzen.

			txa
			pha

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$40			;Aktuelle Diskette geöffnet ?
			beq	:51			;Nein, weiter...

			jsr	TurnOffTurboDOS		;TurboDOS abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

			jsr	StashDriverData		;Variablen sichern.

::51			pla
			tax
			rts
endif

;******************************************************************************
::tmp4 = FD_NM!HD_NM!HD_NM_PP
if :tmp4 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    ACHTUNG! Hier muß unbedingt auch der aktuelle Treiber zurück in die REU
;    kopiert werden, da einige Variablen/Speicherbereiche geändert wurden.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xExitTurbo		lda	#$08			;Sektor-Interleave auf
			sta	interleave		;Vorgabewert zurücksetzen.

			txa
			pha

			ldx	curDrive
			lda	turboFlags -8,x		;Diskette geöffnet ?
			and	#%01000000
			beq	:51			; => Nein, weiter...

			jsr	xPutBAMBlock		;BAM auf Diskette aktualisieren.
							;(KEINE FEHLERABFRAGE!!!)

			lda	#$00			;BAM-Sektor im Speicher löschen.
			sta	CurSek_BAM

			jsr	TurnOffTurboDOS		;TurboDOS abschalten.

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

			jsr	StashDriverData		;Variablen sichern.

::51			pla
			tax
			rts
endif

;******************************************************************************
::tmp5 = RL_41!RL_71!RL_81!RD_81
if :tmp5 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		txa
			pha

			ldx	curDrive
			lda	turboFlags -8,x		;Diskette geöffnet ?
			and	#%01000000
			beq	:51			; => Nein, weiter...

			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

			jsr	StashDriverData		;Variablen sichern.

::51			pla
			tax
			rts
endif

;******************************************************************************
::tmp6 = RD_41!RD_71
if :tmp6 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:xExitTurbo		ldy	curDrive
			lda	turboFlags -8,y
			and	#$bf
			sta	turboFlags -8,y
			rts
endif

;******************************************************************************
::tmp7 = RL_NM!RD_NM!RD_NM_SCPU
if :tmp7 = TRUE
;******************************************************************************
;*** TurboDOS deaktivieren.
;    ACHTUNG! Hier muß unbedingt auch der aktuelle Treiber zurück in die REU
;    kopiert werden, da einige Variablen/Speicherbereiche geändert wurden.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xExitTurbo		txa
			pha

			ldx	curDrive
			lda	turboFlags -8,x		;Diskette geöffnet ?
			and	#%01000000
			beq	:51			; => Nein, weiter...

			jsr	xPutBAMBlock		;BAM auf Diskette aktualisieren.
							;(KEINE FEHLERABFRAGE!!!)

			lda	#$00			;BAM-Sektor im Speicher löschen.
			sta	CurSek_BAM

			ldx	curDrive
			lda	turboFlags -8,x
			and	#$bf
			sta	turboFlags -8,x

			jsr	StashDriverData		;Variablen sichern.

::51			pla
			tax
			rts
endif
