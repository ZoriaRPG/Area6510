﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;RAM_Type = RAM_BBG
;******************************************************************************
;*** Einsprungtabelle RAM-Tools.
:xVerifyRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:xStashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xSwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xFetchRAM		ldy	#%10010001		;RAM-Bereich laden.

:xDoRAMOp		php				;IRQ sperren.
			sei

			lda	MMU
			pha
			lda	#$7e
			sta	MMU

			jsr	SwapBBG_Data		;DoRAMOp-Routine einlesen.
			jsr	$e100			;und ausführen.
			tay				;AKKU-Register speichern.
			jsr	SwapBBG_Data		;DoRAMOp-Routine zurücksetzen.

			pla
			sta	MMU

			plp				;IRQ-Status zurücksetzen.

			tya
			ldx	#$00			;Flag für "Kein Fehler".
			rts

;*** DoRAMOp-Routine aus BBGRAM laden.
:SwapBBG_Data		tya				;Y-Register zwischenspeichern.
			pha

			lda	#%00111110		;Seite #254 in REU aktivieren.
			sta	$dffe			;Bank #0, $FE00.
			lda	#%00000011
			sta	$dfff

			ldy	#$00			;256 Byte in RAM mit REU
::51			ldx	$e100,y			;tauschen.
			lda	$de00,y			;Dabei wird die erweiterte
			sta	$e100,y			;DoRAMOp-Routine nach $E100
			txa				;eingelesen bzw. der Original-
			sta	$de00,y			;Inhalt wieder zurückgesetzt.
			iny
			bne	:51

			pla
			tay
			rts

			g $9f54
