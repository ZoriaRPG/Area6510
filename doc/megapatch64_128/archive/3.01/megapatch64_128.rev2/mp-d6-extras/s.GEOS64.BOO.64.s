﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $1a1a
:AutoBoot_b		= $1ba9
:Code10L		= $49
:Code10a		= $1f25
:Code10b		= $1f6e
:Code1L			= $53
:Code1a			= $1ba9
:Code1b			= $1bfc
:Code2L			= $48
:Code2a			= $1bfc
:Code2b			= $1c44
:Code3L			= $47
:Code3a			= $1c44
:Code3b			= $1c8b
:Code4L			= $e1
:Code4a			= $1c8b
:Code4b			= $1d6c
:Code6L			= $b4
:Code6a			= $1d6c
:Code6b			= $1e20
:Code8L			= $09
:Code8a			= $1e20
:Code8b			= $1e29
:Code9L			= $fc
:Code9a			= $1e29
:Code9b			= $1f25
:E_KernelData		= $1f6e
:L_KernelData		= $0ffe
:MP3_BANK_1		= $19c0
:MP3_BANK_2		= $19f6
:S_KernelData		= $19b8
:Vec_ReBoot		= $19b8
