﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3bbe
:AllocBankT_Block	= $3c39
:AllocBankT_Disk	= $3c30
:AllocBankT_GEOS	= $3c2d
:AllocBankT_Spool	= $3c36
:AllocBankT_Task	= $3c33
:AllocBankUser		= $3be1
:AllocBank_Block	= $3c64
:AllocBank_Disk		= $3c5b
:AllocBank_GEOS		= $3c58
:AllocBank_Spool	= $3c61
:AllocBank_Task		= $3c5e
:AllocateBank		= $3c66
:AllocateBankTab	= $3c3b
:AutoInitSpooler	= $3e25
:AutoInitTaskMan	= $3d24
:BankCodeTab1		= $4606
:BankCodeTab2		= $460a
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $460e
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $461a
:BankType_Disk		= $4616
:BankType_GEOS		= $4612
:BankUsed		= $45c6
:BootBankBlocked	= $28b0
:BootCRSR_Repeat	= $28a5
:BootColsMode		= $28a9
:BootConfig		= $2880
:BootGrfxFile		= $2917
:BootInptName		= $2939
:BootInstalled		= $2890
:BootLoadDkDv		= $28f1
:BootMLineMode		= $28ab
:BootMenuStatus		= $28aa
:BootOptimize		= $28a7
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28a8
:BootPrntName		= $2928
:BootRAM_Flag		= $2891
:BootRTCdrive		= $28f0
:BootSaverName		= $2894
:BootScrSaver		= $2892
:BootScrSvCnt		= $2893
:BootSpeed		= $28a6
:BootSpoolCount		= $28ae
:BootSpoolSize		= $28af
:BootSpooler		= $28ad
:BootTaskMan		= $28ac
:BootUseFastPP		= $294a
:BootVarEnd		= $294b
:BootVarStart		= $2880
:CheckDrvConfig		= $2bd0
:CheckForSpeed		= $347b
:Class_GeoPaint		= $4564

:Class_ScrSaver		= $4553
:ClearDiskName		= $34a2
:ClearDriveData		= $34b9
:ClrBank_Blocked	= $3b2a
:ClrBank_Spooler	= $3b27
:ClrBank_TaskMan	= $3b24
:CopyStrg_Device	= $3f41
:CountDrives		= $348f
:CurDriveMode		= $4575
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $4516
:DiskDriver_DISK	= $453f
:DiskDriver_FName	= $4542
:DiskDriver_INIT	= $453e
:DiskDriver_SIZE	= $4540
:DiskDriver_TYPE	= $453d
:DiskFileDrive		= $453c
:Dlg_DrawTitel		= $3442
:Dlg_IllegalCfg		= $4785
:Dlg_LdDskDrv		= $4715
:Dlg_LdMenuErr		= $47f5
:Dlg_NoDskFile		= $4692
:Dlg_SetNewDev		= $488d
:Dlg_Titel1		= $4901
:Dlg_Titel2		= $4911
:DoInstallDskDev	= $356f
:DriveInUseTab		= $4589
:DriveRAMLink		= $4574
:Err_IllegalConf	= $33cc
:ExitToDeskTop		= $2b93
:FetchRAM_DkDrv		= $2f72
:FindCurRLPart		= $4b70
:FindDiskDrvFile	= $2fdb
:FindDkDvAllDrv		= $3004
:FindDriveType		= $389e
:FindRTC_64Net		= $4033
:FindRTC_SM		= $401f
:FindRTCdrive		= $3fe2
:Flag_ME1stBoot		= $4577
:FreeBank		= $3c8f
:FreeBankTab		= $3c7a
:GetDrvModVec		= $3259
:GetFreeBank		= $3bf3
:GetFreeBankTab		= $3bf5
:GetInpDrvFile		= $3f27
:GetMaxFree		= $3b5b
:GetMaxSpool		= $3b61
:GetMaxTask		= $3b5e
:GetPrntDrvFile		= $3edd
:InitDkDrv_Disk		= $30a1
:InitDkDrv_RAM		= $309b
:InitScrSaver		= $3e61
:InstallRL_Part		= $359b
:IsDrvAdrFree		= $37df
:IsDrvOnline		= $39ad
:LastSpeedMode		= $463c
:LdScrnFrmDisk		= $2d4f
:LoadBootScrn		= $2d1c
:LoadDiskDrivers	= $3067
:LoadDskDrvData		= $32ff

:LoadInptDevice		= $3f19
:LoadPrntDevice		= $3ecf
:LookForDkDvFile	= $3042
:NewDrive		= $4571
:NewDriveMode		= $4572
:NoInptName		= $464d
:NoPrntName		= $463f
:PrepareExitDT		= $2ba4
:RL_Aktiv		= $463d
:SCPU_Aktiv		= $463e
:SaveDskDrvData		= $3285
:SetClockGEOS		= $3fad
:SetSystemDevice	= $2f57
:StashRAM_DkDrv		= $2f75
:StdClrGrfx		= $2d41
:StdClrScreen		= $2d2a
:StdDiskName		= $4659
:SwapScrSaver		= $3e9f
:SysDrive		= $4528
:SysDrvType		= $4529
:SysFileName		= $452b
:SysRealDrvType		= $452a
:SysStackPointer	= $4527
:SystemClass		= $4505
:SystemDlgBox		= $343b
:TASK_BANK0_ADDR	= $290e
:TASK_BANK_ADDR		= $28f2
:TASK_BANK_USED		= $28fb
:TASK_COUNT		= $2904
:TASK_VDC_ADDR		= $2905
:TurnOnDriveAdr		= $4573
:UpdateDiskDriver	= $30e0
:VLIR_BASE		= $491f
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $23e1
:VLIR_Types		= $2380
:firstBootCopy		= $4576
