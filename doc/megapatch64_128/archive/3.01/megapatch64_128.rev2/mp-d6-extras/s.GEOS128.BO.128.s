﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $2914
:AutoBoot_b		= $2b0b
:Code10L		= $4c
:Code10a		= $2ed5
:Code10b		= $2f21
:Code1L			= $6d
:Code1a			= $2b0b
:Code1b			= $2b78
:Code2L			= $68
:Code2a			= $2b78
:Code2b			= $2be0
:Code3L			= $4a
:Code3a			= $2be0
:Code3b			= $2c2a
:Code4L			= $e1
:Code4a			= $2c2a
:Code4b			= $2d0b
:Code6L			= $c5
:Code6a			= $2d0b
:Code6b			= $2dd0
:Code8L			= $09
:Code8a			= $2dd0
:Code8b			= $2dd9
:Code9L			= $fc
:Code9a			= $2dd9
:Code9b			= $2ed5
:E_KernelData		= $2f21
:L_KernelData		= $1cfe
:MP3_BANK_1		= $28ba
:MP3_BANK_2		= $28f0
:S_KernelData		= $28b2
:Vec_ReBoot		= $28b2
