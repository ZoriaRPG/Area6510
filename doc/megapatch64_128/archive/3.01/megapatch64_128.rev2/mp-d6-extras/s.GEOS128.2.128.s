﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $4a86
:ReBoot_REU		= $46fe
:ReBoot_RL		= $436e
:ReBoot_SCPU		= $4000
:x_ClrDlgScreen		= $69f1
:x_DoAlarm		= $5063
:x_EnterDeskTop		= $4e69
:x_GetFiles		= $50d9
:x_GetFilesData		= $64f7
:x_GetFilesIcon		= $6676
:x_GetNextDay		= $5015
:x_PanicBox		= $4f69
:x_ToBASIC		= $4ed6
