﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3af0
:AllocBankT_Block	= $3b6b
:AllocBankT_Disk	= $3b62
:AllocBankT_GEOS	= $3b5f
:AllocBankT_Spool	= $3b68
:AllocBankT_Task	= $3b65
:AllocBankUser		= $3b13
:AllocBank_Block	= $3b96
:AllocBank_Disk		= $3b8d
:AllocBank_GEOS		= $3b8a
:AllocBank_Spool	= $3b93
:AllocBank_Task		= $3b90
:AllocateBank		= $3b98
:AllocateBankTab	= $3b6d
:AutoInitSpooler	= $3cbf
:AutoInitTaskMan	= $3c2b
:BankCodeTab1		= $44a0
:BankCodeTab2		= $44a4
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $44a8
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $44b4
:BankType_Disk		= $44b0
:BankType_GEOS		= $44ac
:BankUsed		= $4460
:BootBankBlocked	= $28b0
:BootCRSR_Repeat	= $28a5
:BootColsMode		= $28a9
:BootConfig		= $2880
:BootGrfxFile		= $2905
:BootInptName		= $2927
:BootInstalled		= $2890
:BootLoadDkDv		= $28f1
:BootMLineMode		= $28ab
:BootMenuStatus		= $28aa
:BootOptimize		= $28a7
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28a8
:BootPrntName		= $2916
:BootRAM_Flag		= $2891
:BootRTCdrive		= $28f0
:BootSaverName		= $2894
:BootScrSaver		= $2892
:BootScrSvCnt		= $2893
:BootSpeed		= $28a6
:BootSpoolCount		= $28ae
:BootSpoolSize		= $28af
:BootSpooler		= $28ad
:BootTaskMan		= $28ac
:BootUseFastPP		= $2938
:BootVarEnd		= $2939
:BootVarStart		= $2880
:CheckDrvConfig		= $2ba7
:CheckForSpeed		= $33e9
:Class_GeoPaint		= $43fe

:Class_ScrSaver		= $43ed
:ClearDiskName		= $340d
:ClearDriveData		= $3424
:ClrBank_Blocked	= $3a74
:ClrBank_Spooler	= $3a71
:ClrBank_TaskMan	= $3a6e
:CopyStrg_Device	= $3ddb
:CountDrives		= $33fa
:CurDriveMode		= $440f
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $43b0
:DiskDriver_DISK	= $43d9
:DiskDriver_FName	= $43dc
:DiskDriver_INIT	= $43d8
:DiskDriver_SIZE	= $43da
:DiskDriver_TYPE	= $43d7
:DiskFileDrive		= $43d6
:Dlg_DrawTitel		= $33b0
:Dlg_IllegalCfg		= $461b
:Dlg_LdDskDrv		= $45ab
:Dlg_LdMenuErr		= $468b
:Dlg_NoDskFile		= $4529
:Dlg_SetNewDev		= $4722
:Dlg_Titel1		= $4796
:Dlg_Titel2		= $47a6
:DoInstallDskDev	= $34da
:DriveInUseTab		= $4423
:DriveRAMLink		= $440e
:Err_IllegalConf	= $333a
:ExitToDeskTop		= $2b6a
:FetchRAM_DkDrv		= $2ee0
:FindCurRLPart		= $4a05
:FindDiskDrvFile	= $2f49
:FindDkDvAllDrv		= $2f72
:FindDriveType		= $3809
:FindRTC_64Net		= $3ecd
:FindRTC_SM		= $3eb9
:FindRTCdrive		= $3e7c
:Flag_ME1stBoot		= $4411
:FreeBank		= $3bc1
:FreeBankTab		= $3bac
:GetDrvModVec		= $31c7
:GetFreeBank		= $3b25
:GetFreeBankTab		= $3b27
:GetInpDrvFile		= $3dc1
:GetMaxFree		= $3aa5
:GetMaxSpool		= $3aab
:GetMaxTask		= $3aa8
:GetPrntDrvFile		= $3d77
:InitDkDrv_Disk		= $300f
:InitDkDrv_RAM		= $3009
:InitScrSaver		= $3cfb
:InstallRL_Part		= $3506
:IsDrvAdrFree		= $374a
:IsDrvOnline		= $3918
:LastSpeedMode		= $44d3
:LdScrnFrmDisk		= $2cb8
:LoadBootScrn		= $2c8c
:LoadDiskDrivers	= $2fd5
:LoadDskDrvData		= $326d

:LoadInptDevice		= $3db3
:LoadPrntDevice		= $3d69
:LookForDkDvFile	= $2fb0
:NewDrive		= $440b
:NewDriveMode		= $440c
:NoInptName		= $44e4
:NoPrntName		= $44d6
:PrepareExitDT		= $2b7b
:RL_Aktiv		= $44d4
:SCPU_Aktiv		= $44d5
:SaveDskDrvData		= $31f3
:SetClockGEOS		= $3e47
:SetSystemDevice	= $2ec5
:StashRAM_DkDrv		= $2ee3
:StdClrGrfx		= $2caa
:StdClrScreen		= $2c9a
:StdDiskName		= $44f0
:SwapScrSaver		= $3d39
:SysDrive		= $43c2
:SysDrvType		= $43c3
:SysFileName		= $43c5
:SysRealDrvType		= $43c4
:SysStackPointer	= $43c1
:SystemClass		= $439f
:SystemDlgBox		= $33a9
:TASK_BANK_ADDR		= $28f2
:TASK_BANK_USED		= $28fb
:TASK_COUNT		= $2904
:TurnOnDriveAdr		= $440d
:UpdateDiskDriver	= $304e
:VLIR_BASE		= $47b4
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $254c
:VLIR_Types		= $2380
:firstBootCopy		= $4410
