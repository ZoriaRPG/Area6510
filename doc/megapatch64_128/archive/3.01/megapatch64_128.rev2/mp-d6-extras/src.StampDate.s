﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

if .p
			t "src.GEOS_MP3.64"
			t "SymbTab_1"
			t "SymbTab_2"
			t "MacTab"
endif

			n "StampDate"
			f APPLICATION
			a "Markus Kanet"
			o APP_RAM

h "Setzt aktuelles Datum für alle Dateien im Verzeichnis."

;*** Datum festlegen.
:MainInit		jsr	OpenDisk
			txa
			beq	:51
			rts

::51			lda	year     ,x
			sta	Date_Time,x
			inx
			cpx	#$05
			bcc	:51

			jsr	Get1stDirEntry

			lda	#$00
::52			sta	r2L
			asl
			asl
			asl
			asl
			asl
			tax
			clc
			adc	#25
			tay
			lda	diskBlkBuf+2,x
			beq	:54

			ldx	#$00
::53			lda	Date_Time ,x
			sta	diskBlkBuf,y
			iny
			inx
			cpx	#$05
			bcc	:53

::54			lda	r2L
			clc
			adc	#$01
			cmp	#$08
			bcc	:52

			jsr	PutBlock
			txa
			bne	:55

			lda	diskBlkBuf +0
			beq	:55
			sta	r1L
			lda	diskBlkBuf +1
			sta	r1H
			jsr	GetBlock
			txa
			beq	:52
::55			jmp	EnterDeskTop

:Date_Time		s $05
