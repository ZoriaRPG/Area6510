﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;*** Haupt-Initialisierungsroutine für den GEOS.Editor.
;*** Beim ersten Start wird die Konfiguration im laufenden Betrieb übernommen
;*** bzw. wenn während des Boot-Vorgangs ausgeführt werden alle physikalischen
;*** Laufwerk 8-11 erkannt und installiert.
;******************************************************************************

;--- Laufwerkstreiber in REU kopieren.
:MainInitBoot		jsr	LoadDiskDrivers
			txa
			beq	:54
			jmp	Err_LdDskFile

;--- Aktiven TaskManager einlesen und in Zwischenspeicher kopieren.
::54			bit	firstBoot		;GEOS-BootUp ?
			bpl	:57			; => Ja, weiter...
			lda	Flag_TaskAktiv		;Ist TaskManager installiert ?
			bmi	:56			; => Nein, weiter...

			LoadW	r0,R2_ADDR_TASKMAN_E
			LoadW	r1,R2_ADDR_TASKMAN
			LoadW	r2,R2_SIZE_TASKMAN
			lda	Flag_TaskBank
			sta	r3L
			jsr	FetchRAM
			jsr	SetTaskBank		;Zeiger auf TaskManager und
			jsr	StashRAM		;aktuellen Manager speichern.

			ldy	#$08			;Variablen einlesen.
::55			lda	R2_ADDR_TASKMAN_E +3,y
			sta	TASK_BANK_ADDR      ,y

if Flag64_128 = TRUE_C128
			lda	R2_ADDR_TASKMAN_E +22,y
			sta	TASK_VDC_ADDR      ,y
			lda	R2_ADDR_TASKMAN_E +22+9,y
			sta	TASK_BANK0_ADDR      ,y
endif
			dey
			bpl	:55

			lda	R2_ADDR_TASKMAN_E +21
			sta	TASK_COUNT
			lda	#$00			;Taskmanager war aktiviert,
::56			sta	BootTaskMan		;"Install"-Flag setzen.
::57			lda	#$ff			;TaskManager abschalten.
			sta	Flag_TaskAktiv

;--- Aktiven Druckerspooler deaktivieren.
			bit	firstBoot		;GEOS-BootUp ?
			bpl	:59			; => Ja, weiter...
			lda	Flag_Spooler		;Ist Spooler installiert ?
			bpl	:58			; => Nein, weiter...

			lda	Flag_SpoolMinB		;Ist RAM für Druckerspooler
			ora	Flag_SpoolMaxB		;reserviert ?
			beq	:58			; => Nein, weiter...
			lda	Flag_SpoolMaxB
			sec
			sbc	Flag_SpoolMinB
			clc
			adc	#$01
			sta	BootSpoolSize
			ldx	Flag_SpoolCount		;Verzögerungszeit für
			stx	BootSpoolCount		;Druckerspooler setzen.

			lda	#$80			;Spooler war installiert,
::58			sta	BootSpooler		;"Install"-Flag setzen.
::59			lda	#$00			;Spooler deaktivieren.
			sta	Flag_Spooler

;*** Speicherbelegungstabelle erstellen.
:InitRamTab		jsr	Make64KRamTab		;Bank-Belegungstabelle definieren.
							;TaskMan/Spooler nicht beachten.
			bit	firstBoot		;GEOS-BootUp ?
			bmi	Find_CMD_SCPU		; => Nein, weiter...
			jsr	AllocBankUser		;Anwenderspeicher reservieren.

;*** SuperCPU erkennen.
:Find_CMD_SCPU		lda	#$00			;Takt für SCPU auf 1Mhz setzen.
			sta	LastSpeedMode		;(Falls keine SCPU vorhanden)
			sta	SCPU_Aktiv		;Flag: "Keine SCPU".

			php
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			ldx	MMU
			lda	#$7e
			sta	MMU
endif
			lda	$d0bc

if Flag64_128 = TRUE_C64
			stx	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			stx	MMU
endif

			plp
			and	#%10000000		;Bit 7=1, SCPU nicht aktiv.
			bne	Find_CMD_RL
			dec	SCPU_Aktiv		;Flag setzen: "SCPU verfügbar".

			jsr	CheckForSpeed		;SCPU-Takt ermitteln und
			sta	LastSpeedMode		;zwischenspeichern.

;*** RAMLink erkennen.
:Find_CMD_RL		lda	#$00			;Flag: "Keine RAMLink".
			sta	RL_Aktiv

			php
			sei

if Flag64_128 = TRUE_C64
			ldy	CPU_DATA
			lda	#$36
			sta	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			ldy	MMU
			lda	#$4e
			sta	MMU
endif

			ldx	$e0a9			;Byte aus C64-Kernal einlesen.

if Flag64_128 = TRUE_C64
			sty	CPU_DATA
endif

if Flag64_128 = TRUE_C128
			sty	MMU
endif

			plp
			cpx	#$78			;"SEI"-Befehl ?
			bne	:51			;Nein, weiter...
			dec	RL_Aktiv		;RAMLink verfügbar.

;--- Keine RAMLink, RLxy-Laufwerke nach RAMxy konvertieren.
::51			lda	RL_Aktiv		;RAMLink verfügbar ?
			bne	Install			; => Ja, weiter...

			ldx	#$00			;RAMLink-Laufwerke in RAM-Laufwerke
::52			lda	BootConfig,x		;umwandeln, da keine RAMLink
			and	#%11110000		;verfügbar ist. Damit wird versucht
			cmp	#DrvRAMLink		;die Konfiguration beizubehalten!
			bne	:53
			lda	BootConfig,x
			and	#%00001111		;Emulationsmodus isolieren und
			ora	#%10000000		;"RAM-Laufwerk"-Flag setzen.
			sta	BootConfig,x
::53			inx
			cpx	#$04
			bcc	:52

;*** MegaPatch konfigurieren/Menü-Oberfläche starten.
:Install		jsr	GetAllSerDrive		;<*> Alle Laufwerke erkennen.

			bit	firstBoot		;GEOS-BootUp ?
			bpl	:51			; => Ja, automatisch installieren.

			ldx	#3
::50			lda	driveType     ,x
			beq	:49
			bmi	:49
			lda	DriveInfoTab  ,x
			beq	:49
			lda	#$ff
			sta	DriveInUseTab ,x
::49			dex
			bpl	:50

			jmp	LoadMainMenu		;Hauptmenü starten.

::51			jsr	PurgeTurbo		;GEOS-TurboDOS abschalten.
			jsr	InstallDkDev		;Alle Laufwerke erkennen.
			jmp	AutoInstall

;*** MegaPatch während des bootens automatisch konfigurieren.
:AutoInstall		jsr	SetClockGEOS		;Uhrzeit einlesen.

:Install_SCPU		bit	SCPU_Aktiv		;Ist SuperCPU aktiviert ?
			bpl	Install_Cursor		; => Nein, weiter...

			php				;SuperCPU-Taktfrequenz festlegen.
			sei

if Flag64_128 = TRUE_C64
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA
endif

			ldy	#$00
			bit	BootSpeed
			bvs	:51
			iny
::51			sta	$d07a,y			;Takt über Register $D07A/$D07B

if Flag64_128 = TRUE_C64
			stx	CPU_DATA		;einstellen.
endif

			plp

:Install_SCPU_Opt	lda	BootOptimize		;Optimierung für SuperCPU
			jsr	SCPU_SetOpt		;festlegen.

:Install_Cursor		lda	BootCRSR_Repeat		;Wiederholungsgeschwindigkeit für
			sta	Flag_CrsrRepeat		;CURSOR festlegen.

:Install_Printer	lda	BootPrntMode		;Modus für Druckertreiber
			sta	Flag_LoadPrnt		;aus RAM/DISK festlegen.
			jsr	InitPrntDevice

:Install_Input		jsr	InitInptDevice

:Install_Menu		lda	BootColsMode		;Modus für Systemfarben festlegen.
			sta	Flag_SetColor
			lda	BootMenuStatus		;Menü-Parameter festlegen.
			sta	Flag_MenuStatus
			lda	BootMLineMode
			sta	Flag_SetMLine

:Install_ScrSaver	lda	BootScrSaver		;Modus für Bildschirmschoner
			sta	Flag_ScrSaver		;installieren.
			lda	BootScrSvCnt		;Startverzögerung für
			sta	Flag_ScrSvCnt		;Bildschirmschoner festlegen.
			lda	BootSaverName		;Bildschirmschoner nachladen ?
			beq	:51			;Nein, weiter...
			LoadW	r6,BootSaverName	;Neuen Bildschirmschoner starten.
			jsr	InitScrSaver

::51			bit	Flag_ME1stBoot		;Konfiguration gespeichert, dann
			bmi	ExitToDeskTop		;ist dieses Flag für immer $FF.

;*** MegaPatch während des bootens automatisch konfigurieren.
;--- Wenn MP3 zum ersten mal installiert wird, dann wird automatisch der
;    TaskManager und der Druckerspooler konfiguriert.
:Install_Task_Spl	lda	#$00
			sta	TASK_COUNT		;Vorgabewert: Alle Tasks löschen.
			sta	BootSpoolSize		;Vorgabewert: Spooler deaktivieren.
			jsr	GetMaxFree		;Max. freien Speicher ermitteln.
			cpy	#$03			;Genügend Speicher frei ?
			bcc	ExitToDeskTop		; => Nein, Ende...

;--- Max. RAM für TaskManager aktivieren.
			bit	BootTaskMan		;TaskManager installieren ?
			bmi	:52			; => Nein, weiter...
			lda	#MAX_TASK_ACTIV		;Vorgabewert: Alle Tasks aktivieren.
			sta	TASK_COUNT

;--- Max. RAM für Spooler aktivieren.
::52			bit	BootSpooler		;Spooler installieren ?
			bpl	:55			; => Nein, weiter...
			lda	ramExpSize		;Vorgabewert: Max. Spoolergröße.
			cmp	#MAX_SPOOL_SIZE		;Max. Größe des Spoolers
			bcc	:53			;überschritten (Nur Demo-Version) ?
			lda	#MAX_SPOOL_SIZE		; => Ja, Größe auf Maximum setzen.
::53			cmp	#$04			;Mehr als 256K reserviert ?
			bcc	:54			; => Nein, weiter...
			lda	#$04			;Nicht mehr als 256K für Spooler.
::54			sta	BootSpoolSize		;Spoolergröße festlegen.

;--- Taskmanager und Spooler konfigurieren.
::55			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.
			jsr	BlockFreeBank		;Zwei Bänke reservieren: 1x für
			jsr	BlockFreeBank		;GEOS-Anwendungen, 1x für Spooler.
			jsr	InitTaskManager		;TaskManager installieren.

			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.
			jsr	BlockFreeBank		;64K für Anwendungen reservieren.
			jsr	InitPrntSpooler		;Spooler installieren.

			jsr	ClrBank_Blocked		;Reserviertes RAM freigeben.
			jsr	AllocBankUser		;Anwenderspeicher reservieren.

;--- Installierte Größe von TaskManager und Spooler retten.
			jsr	GetMaxTask		;Vorgabewerte für Taskmanager und
if Flag64_128 = TRUE_C128
			ldx	#0
::2			cpy	#0
			beq	:1
			inx
			dey
			dey
			dey
			jmp	:2
::1			stx	TASK_COUNT		;Druckerspooler bestimmen.
else
			sty	TASK_COUNT		;Druckerspooler bestimmen.
endif
			jsr	GetMaxSpool
			cpy	#MAX_SPOOL_SIZE		;Max. Größe des Spoolers
			bcc	:56			;überschritten (Nur Demo-Version) ?
			ldy	#MAX_SPOOL_SIZE		; => Ja, Größe auf Maximum setzen.
::56			sty	BootSpoolSize

;--- Ergänzung: 19.07.18/M.Kanet
;Ab hier Fortsetzung in "s.MP3.Edit.1" mit Label ".ExitToDeskTop"!
