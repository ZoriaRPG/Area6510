# Area6510

### GEOS MEGAPATCH 64/128 V3.01
This is the recovered code from the MegaPatch128 V3.01 release from 2003. Not all possible builds have been tested, just the german/c128 code has been recovered from the binary release.
There is also a diff file which list all changes from my code base from 1999 against the binary release from 2003.
