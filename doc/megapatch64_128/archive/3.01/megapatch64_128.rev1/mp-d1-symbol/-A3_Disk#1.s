﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

if ASS_DEMO		= FALSE
;--- Laufwerkstreiber.
			b $f0,"s.1541_Turbo",$00
			b $f0,"s.1571_Turbo",$00
			b $f0,"s.1581_Turbo",$00
			b $f0,"s.DOS_Turbo",$00
			b $f0,"s.PP_Turbo",$00

			b $f0,"s.1541",$00
			b $f0,"s.1571",$00
			b $f0,"s.1581",$00
			b $f0,"s.RAM41",$00
			b $f0,"s.RAM71",$00
			b $f0,"s.RAM81",$00
			b $f0,"s.RAMNM",$00
			b $f0,"s.RAMNM_SCPU",$00
			b $f0,"s.FD41",$00
			b $f0,"s.FD71",$00
			b $f0,"s.FD81",$00
			b $f0,"s.FDNM",$00
			b $f0,"s.PCDOS",$00
			b $f0,"s.PCDOS_EXT",$00
			b $f0,"s.HD41",$00
			b $f0,"s.HD71",$00
			b $f0,"s.HD81",$00
			b $f0,"s.HDNM",$00
			b $f0,"s.HD41_PP",$00
			b $f0,"s.HD71_PP",$00
			b $f0,"s.HD81_PP",$00
			b $f0,"s.HDNM_PP",$00
			b $f0,"s.RL41",$00
			b $f0,"s.RL71",$00
			b $f0,"s.RL81",$00
			b $f0,"s.RLNM",$00

			b $f0,"s.Info.DTypes",$00
			b $f0,"s.INIT 1541",$00
			b $f0,"s.INIT 1571",$00
			b $f0,"s.INIT 81FD",$00
			b $f0,"s.INIT PCDOS",$00
			b $f0,"s.INIT HD41",$00
			b $f0,"s.INIT HD71",$00
			b $f0,"s.INIT HD81",$00
			b $f0,"s.INIT HDNM",$00
			b $f0,"s.INIT RAM41",$00
			b $f0,"s.INIT RAM71",$00
			b $f0,"s.INIT RAM81",$00
			b $f0,"s.INIT RAMNM",$00
			b $f0,"s.INIT SRAMNM",$00
			b $f0,"s.INIT RL",$00
endif

if ASS_DEMO = TRUE
;--- Laufwerkstreiber.
			b $f0,"s.1541_Turbo",$00
			b $f0,"s.1571_Turbo",$00
			b $f0,"s.1581_Turbo",$00
			b $f0,"s.DOS_Turbo",$00
			b $f0,"s.PP_Turbo",$00

			b $f0,"s.1541",$00
			b $f0,"s.1571",$00
			b $f0,"s.1581",$00
			b $f0,"s.RAM41",$00
			b $f0,"s.RAM71",$00
			b $f0,"s.RAM81",$00
			b $f0,"s.RAMNM",$00
			b $f0,"s.RAMNM_SCPU",$00
			b $f0,"s.FD81",$00
			b $f0,"s.PCDOS",$00
			b $f0,"s.PCDOS_EXT",$00
			b $f0,"s.HD81",$00
			b $f0,"s.HD81_PP",$00
			b $f0,"s.RL81",$00

			b $f0,"s.Info.DTypes",$00
			b $f0,"s.INIT 1541",$00
			b $f0,"s.INIT 1571",$00
			b $f0,"s.INIT 81FD",$00
			b $f0,"s.INIT HD81",$00
			b $f0,"s.INIT PCDOS",$00
			b $f0,"s.INIT RAM41",$00
			b $f0,"s.INIT RAM71",$00
			b $f0,"s.INIT RAM81",$00
			b $f0,"s.INIT RAMNM",$00
			b $f0,"s.INIT SRAMNM",$00
			b $f0,"s.INIT RL",$00
endif
