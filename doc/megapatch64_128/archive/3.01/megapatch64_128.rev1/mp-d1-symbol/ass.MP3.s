﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			t "G3_SymMacExt"
			t "ass.Drives"
			t "ass.Macro"
			t "ass.System"

			n "ass.MP3"

			c "ass.SysFile V1.0"
			a "M. Kanet"
			f $04

			o $4000

;*** MegaPatch-Kernal assemblieren.
:MainInit		OPEN_SYMBOL

;--- ass.Dateien.
			b $f0,"ass.MP3_1.s",$00
			b $f0,"ass.MP3_2.s",$00
			b $f0,"ass.MP3_SYS.s",$00
			b $f0,"ass.MP3_DSK.s",$00
			b $ff
