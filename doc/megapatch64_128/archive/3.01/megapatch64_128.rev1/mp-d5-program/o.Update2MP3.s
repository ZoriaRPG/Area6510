﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Dieses Programm wird zum Abschluß des Installation/Update-Vorgangs gestartet.
;Das Programm startet den GEOS.Editor und konfiguriert das GEOS-System.
;Danach wird im Falle einer Installation die Konfiguration gespeichert und
;die Diskette bootfähig gemacht.
;******************************************************************************

			n "obj.Update2MP3"
			t "G3_SymMacExtEdit"
			t "G3_Appl.V.Class"

			o BASE_AUTO_BOOT		;BASIC-Start beachten!

;*** Einsprungtabelle.
:MainInit		jmp	ContInstall

;*** Laufwerksdaten, werden vom Installationsprogramm ergänzt.
:UserConfig		s $04
:UserPConfig		s $04
:UserTools		s $02

;*** GEOS.Editor nachladen und neu initialisieren.
:ContInstall		lda	curDrive
			sta	BOOT_DEVICE

			lda	EnterDeskTop +1		;Zeiger auf ":EnterDeskTop"-Routine
			sta	vecEnterDT   +0		;zwischenspeichern.
			lda	EnterDeskTop +2
			sta	vecEnterDT   +1

			lda	#<EndInstall		;Neue ":EnterDeskTop"-Routine
			sta	EnterDeskTop +1		;installieren.
			lda	#>EndInstall
			sta	EnterDeskTop +2

			LoadW	r0,InfoText01		;Installationsmeldung ausgeben.
			jsr	InfoString

			LoadB	r0L,%00000001
			LoadW	r6 ,FNameME3
			LoadW	r7 ,BASE_EDITOR_MAIN
			jsr	GetFile			;GEOS.Editor laden.
			txa				;Diskettenfehler ?
			beq	:52			; => Nein, weiter...
::51			jmp	EndUpdate

::52			LoadW	r9,dirEntryBuf
			jsr	GetFHdrInfo		;InfoBlock einlesen.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			jsr	PurgeTurbo		;TurboDOS abschalten.

			lda	UserTools +0
			cmp	#"-"
			beq	:56

			ldy	#8
::53			lda	UserConfig  -8,y	;Aktuellen Konfiguration einlesen.
			sta	BootConfig  -8,y	;LW an GEOS.Editor übergeben.
			and	#%11110000		;CMD RAMLink ?
			cmp	#DrvRAMLink
			bne	:54			;Nein, weiter...
			lda	UserPConfig -8,y
			b $2c
::54			lda	#$00
			sta	BootPartRL  -8,y	;Partition an GEOS.Editor übergeben.
			lda	#$00
			sta	BootPartRL_I-8,y
			lda	UserConfig  -8,y
			and	#%00001111
			sta	BootPartType-8,y	;PartTyp an GEOS.Editor übergeben.

			cpy	curDrive		;Aktuelles Laufwerk ?
			beq	:55			;Ja, weiter...

			lda	#$00			;Laufwerk löschen. Die Register
			sta	driveType   -8,y	;":ramBase"/":driveData" dürfen hier
;			sta	ramBase     -8,y	;nicht gelöscht werden, da diese
;			sta	driveData   -8,y	;von den alten GEOSV2-Treiber noch
			sta	turboFlags  -8,y	;mitverwendet werden (RAMLink)!

::55			iny
			cpy	#12
			bne	:53

::56			ldx	curDrive
			lda	BootConfig  -8,x	;Der reale Laufwerkstyp wird vom
			sta	RealDrvType -8,x	;GEOS.Editor in die aktuelle Boot-
							;konfiguration übernommen.

			lda	#$00			;GEOS.Editor starten.
			sta	r0L
			sta	firstBoot
			sta	BootInstalled
			lda	fileHeader +$4b
			sta	r7L
			lda	fileHeader +$4c
			sta	r7H
			jmp	StartAppl

;*** AutoBoot beenden.
:EndInstall		lda	vecEnterDT   +0		;Vektor für ":EnterDeskTop"
			sta	EnterDeskTop +1		;zurücksetzen.
			lda	vecEnterDT   +1
			sta	EnterDeskTop +2

;--- Konfiguration des GEOS-Editors auslesen und auf Diskette speichern.
			lda	UserTools +0
			cmp	#"-"
			beq	:51

			LoadW	r0,InfoText02		;Installationsmeldung ausgeben.
			jsr	InfoString
			jsr	PatchMP_Files

::51			lda	UserTools +1
			cmp	#"-"
			bne	:53
::52			jmp	EndUpdate		;Zurück zum DeskTop.

;******************************************************************************
;*** Voll-Version.
;******************************************************************************
if MP_DEMO = FALSE
;--- Bei Update-Vorgang "GEOS64.MakeBoot" starten.
::53			LoadW	r0,InfoText03		;Installationsmeldung ausgeben.
			jsr	InfoString

			LoadW	r6,FNameMkBoot
			jsr	FindFile		;Bootprogramm suchen.
			txa				;Diskettenfehler ?
			bne	:52			; => Ja, Abbruch...

			lda	#>EnterDeskTop -1	;Rücksprungadresse setzen.
			pha
			lda	#<EnterDeskTop -1
			pha
			LoadB	r0L,%00000000
			LoadW	r6 ,FNameMkBoot
			jmp	GetFile			;MakeBoot starten.
endif
;******************************************************************************

;******************************************************************************
;*** Demo-Version.
;******************************************************************************
if MP_DEMO = TRUE
::53
endif
;******************************************************************************

;*** Bildschirm löschen und zurück zum DeskTop.
:EndUpdate		lda	screencolors		;Fehler, Bildschirm löschen und
			sta	:51			;zurück zum DeskTop.
			jsr	i_FillRam
			w	1000
			w	COLOR_MATRIX
::51			b	$00

			lda	#$02
			jsr	SetPattern
			jsr	i_Rectangle
			b	$00,$c7
			w	$0000 ! DOUBLE_W,$013f ! DOUBLE_W ! ADD1_W

if Flag64_128 = TRUE_C128
			bit	graphMode		;80-Zeichen-Modus ?
			bpl	:52			; => Nein, weiter...
			lda	scr80colors		;80-Zeichen-Bildschirm löschen.
			jsr	ColorRectangle
endif
::52			jmp	EnterDeskTop

;*** Warteschleife.
:InfoString		PushW	r0
			LoadW	r0,InfoText00		;Bildschirm löschen.
			jsr	GraphicsString
			PopW	r0
			jsr	PutString

			php
			sei

if Flag64_128 = TRUE_C64
			lda	CPU_DATA
			pha
			lda	#$35
			sta	CPU_DATA
endif

			ldx	#$04
::51			lda	$dc08			;Sekunden/10 - Register.
::52			cmp	$dc08
			beq	:52
			dex
			bne	:51

if Flag64_128 = TRUE_C64
			pla
			sta	CPU_DATA
endif
			plp
			rts

;*** RAM-Konfiguration speichern.
:PatchMP_Files		lda	BOOT_DEVICE		;Startlaufwerk aktivieren.
			jsr	SetDevice
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			beq	:52			; => Nein, weiter...
::51			rts

::52			LoadW	r6,FNameME3
			jsr	FindFile		;GEOS-Editor suchen.
			txa				;Editor gefunden ?
			bne	:51			; => Nein, Abbruch...

			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock		;VLIR-Header einlesen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...

			lda	diskBlkBuf  +2
			sta	r1L
			lda	diskBlkBuf  +3
			sta	r1H
			jsr	GetBlock		;Ersten Programmsektor einlesen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...

::53			lda	BASE_EDITOR_MAIN,x	;Konfiguration übertragen.
			sta	diskBlkBuf  +2,x
			inx
			cpx	#(BootVarEnd - BootVarStart)
			bne	:53

			jmp	PutBlock		;Konfiguration speichern.

;*** Systemtexte.
:InfoText00		b NEWPATTERN,$00
			b MOVEPENTO
			w $0000 ! DOUBLE_W
			b $00
			b RECTANGLETO
			w $013f ! DOUBLE_W ! ADD1_W
			b $c7
			b ESC_PUTSTRING
			w $0000 ! DOUBLE_W
			b $10
			b PLAINTEXT,BOLDON

if Flag64_128 = TRUE_C64
			b "GEOS-MEGAPATCH64 (C)1999: KANET/MEGACOM",CR
endif

if Flag64_128 = TRUE_C128
			b "GEOS-MEGAPATCH128 (C)1999: KANET/MEGACOM",CR
endif

			b "KERNEL-VERSION 3.0  BUILD:"
			d "obj.BuildID"
			b CR,CR,NULL

if Sprache = Deutsch
:InfoText01		b "MegaPatch ist installiert.",CR,CR
			b "Für die Laufwerksinstallation wird nun",CR
			b "der GEOS.Editor gestartet. Bitte warten...",NULL
:InfoText02		b "Konfiguration speichern...",NULL
:InfoText03		b "Laufwerksinstallation beendet.",CR,CR
			b "Um die Diskette bootfähig zu machen wird nun",CR
			b "GEOS.MakeBoot gestartet. Bitte warten...",NULL
endif

if Sprache = Englisch
:InfoText01		b "MegaPatch is installed.",CR,CR
			b "To configure the diskdrives GEOS.Editor",CR
			b "is started now. Please wait...",NULL
:InfoText02		b "Save configuration...",NULL
:InfoText03		b "Drive-installation finished.",CR,CR
			b "Please wait while loading GEOS.MakeBoot to",CR
			b "make the current disk bootable...",NULL
endif

;*** Variablen.
:vecEnterDT		w $0000

:BOOT_DEVICE		b $00

if Flag64_128 = TRUE_C64
:FNameME3		b "GEOS64.Editor",NULL
:FNameMkBoot		b "GEOS64.MakeBoot",NULL
endif

if Flag64_128 = TRUE_C128
:FNameME3		b "GEOS128.Editor",NULL
:FNameMkBoot		b "GEOS128.MakeBoot",NULL
endif
