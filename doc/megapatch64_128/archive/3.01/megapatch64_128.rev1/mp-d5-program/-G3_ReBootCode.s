﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS-ReBoot-Routine.
:GEOS_ReBootSys		sei
			cld
			ldx	#$ff
			txs
			lda	#$30
			sta	CPU_DATA

			PushW	$0314			;IRQ-Vektor speichern.

			LoadW	r0,DISK_BASE
			LoadW	r1,$8300
			LoadW	r2,$0d80
			jsr	SysFetchRAM		;Laufwerkstreiber einlesen.

			LoadW	r0,$9d80
			LoadW	r1,$b900
			LoadW	r2,$0280
			jsr	SysFetchRAM		;Kernal Teil #1 einlesen.

			LoadW	r0,$bf40
			LoadW	r1,$bb80
			LoadW	r2,$00c0
			jsr	SysFetchRAM		;Kernal Teil #2 einlesen.

			LoadW	r0,$c100
			LoadW	r1,$bd40
			LoadW	r2,$0f00
			jsr	SysFetchRAM		;Kernal Teil #3 einlesen.

			LoadW	r0,$1000
			LoadW	r1,$cc40
			LoadW	r2,$3000
			jsr	SysFetchRAM		;Kernal Teil #4 einlesen.

			LoadW	r0 ,$1000
			LoadW	r1 ,$d000

			ldx	#$30
			ldy	#$00
::51			lda	(r0L),y
			sta	(r1L),y
			iny
			bne	:51
			inc	r0H
			inc	r1H
			dex
			bne	:51

;*** Variablenspeicher löschen.
			jsr	i_FillRam
			w	$0500
			w	$8400
			b	$00

;*** GEOS initiailisieren.
			jsr	FirstInit		;GEOS initialisieren.
			jsr	SCPU_OptOn		;SCPU optimieren.

			lda	#$ff			;GEOS-Boot-Vorgang.
			sta	firstBoot

			jsr	InitMouse		;Mausabfrage initialisieren.

			LoadB	dispBufferOn,ST_WR_FORE

			lda	#$02			;Bildschirm löschen.
			jsr	SetPattern

			jsr	i_Rectangle
			b	$00,$c7
			w	$0000,$013f

;*** GEOS-Variablen aus REU einlesen.
			LoadW	r0,ramExpSize		;GEOS-Variablen aus REU in
			LoadW	r1,$7dc3		;C64-RAM kopieren.
			LoadW	r2,$0002
			jsr	SysFetchRAM

			lda	sysFlgCopy		;System-Flag speichern.
			sta	sysRAMFlg

			LoadW	r0,year			;Datum aus REU einlesen.
			LoadW	r1,$7a16
			LoadW	r2,$0003
			lda	#$00
			sta	r3L
			jsr	FetchRAM

			LoadW	r0,driveType		;Laufwerkstypen aus REU einlesen.
			LoadW	r1,$798e
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,ramBase		;RAM-Adressen aus REU einlesen.
			LoadW	r1,$7dc7
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,driveData		;Laufwersdaten aus REU einlesen.
			LoadW	r1,$7dbf
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,PrntFileName		;Name des Druckertreibes aus
			LoadW	r1,$7965		;REU einlesen.
			LoadW	r2,$0011
			jsr	FetchRAM

			LoadW	r0,inputDevName		;Name des Eingabetreibes aus
			LoadW	r1,$7dcb		;REU einlesen.
			LoadW	r2,$0011
			jsr	FetchRAM

			LoadW	r0,curDrive		;Aktuelles Laufwerk aus
			LoadW	r1,$7989		;REU einlesen.
			LoadW	r2,$0001
			jsr	FetchRAM

			LoadW	r0,$8a00		;Mauszeiger aus.
			LoadW	r1,$fc40		;REU einlesen.
			LoadW	r2,$003f
			jsr	FetchRAM

			LoadW	r0,mousePicData		;Mauszeiger aus.
			LoadW	r1,$fc40		;REU einlesen.
			LoadW	r2,$003f
			jsr	FetchRAM

;*** Warteschleife.
;    Dabei wird zuerst der I/O-Bereich eingeblendet und anschließend die
;    Original-IRQ-Routine aktiviert. Diese Routine ist beim C64 zwingend
;    notwendig. Feht diese Routine ist ohne ein Laufwerk wie z.B. C=1541
;    ein Start über RBOOT nicht möglich (Fehlerhaftes IRQ-verhalten!)
;    Ist kein Gerät am ser. Bus aktiviert, kann GEOS ohne diese Routine
;    nicht gestartet werden!!!
:Wait			jsr	InitForIO		;I/O-Bereich einblenden.

			lda	$dc08			;Uhrzeit starten.
			sta	$dc08

			PopW	$0314			;Zeiger auf IRQ-Routine setzen.
			cli				;IRQ aktivieren und warten bis
			lda	$dc08			;IRQ ausgeführt wurde...
::51			cmp	$dc08
			beq	:51

			jsr	DoneWithIO		;I/O-Bereich ausblenden.

;*** Laufwerke aktivieren.
:InstallDrives		lda	curDrive		;Aktuelles Laufwerk merken.
			pha

			lda	#$00			;Anzahl Laufwerke löschen.
			sta	numDrives

			ldy	#8			;Zeiger auf Laufwerk #8.
::51			sty	:52 +1
			lda	driveType -8,y		;Laufwerk verfügbar ?
			beq	:53			;Nein, weiter...

			inc	numDrives		;Anzahl Laufwerke +1.
			lda	:52 +1
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	NewDisk			;Diskette öffnen.

::52			ldy	#$ff
::53			iny				;Zeiger auf nächstes Laufwerk.
			cpy	#12			;Alle Laufwerke getestet ?
			bcc	:51			;Nein, weiter...

			pla
			jsr	SetDevice		;Laufwerk zurücksetzen.
			jmp	EnterDeskTop		;Zurück zum DeskTop.

;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_SCPU
:SysFetchRAM		php
			sei
			lda	CPU_DATA
			pha
			lda	#$35
			sta	CPU_DATA

			lda	r3L			;Bank in SCPU-Bank umrechnen.
			clc
			adc	RamBankFirst +1		;Ersatz für RamBankFirst.
			sta	:51 +2

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a4,$02			;ldy $0002
			b $a6,$04			;ldx $0004
			b $8b				;phb
::51			b $54,$00,$02			;mvn $00.$02
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			pla
			sta	CPU_DATA
			plp
			rts
endif

;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_RL
:SysFetchRAM		php
			sei
			lda	CPU_DATA
			pha
			lda	#$36
			sta	CPU_DATA

			jsr	$e0a9

			ldx	#$04
::51			lda	zpage +1,x
			sta	$de01   ,x
			dex
			bne	:51

			ldx	#$00			;Bank für Transfer.
			stx	$de06

			lda	r2L			;Anzahl Bytes.
			sta	$de07
			lda	r2H
			sta	$de08
			stx	$de0a			;Bank für C128-Transfer.

			ldy	#$91			;JobCode setzen.
			sty	$de01

			jsr	$fe06			;Job ausführen und
			jsr	$fe0f			;RL-Hardware abschalten.

			pla
			sta	CPU_DATA
			plp
			rts
endif

;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_REU
:SysFetchRAM		php
			sei
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA

			lda	r0L			;Startadresse C64-RAM.
			sta	$df02
			lda	r0H
			sta	$df03

			lda	r1L			;Startadresse REU.
			sta	$df04
			lda	r1H
			sta	$df05
			lda	#$00
			sta	$df06

			lda	r2L			;Anzahl Bytes.
			sta	$df07
			lda	r2H
			sta	$df08

			lda	#$00
			sta	$df09
			sta	$df0a
			lda	#$91			;Job-Code.
			sta	$df01

::51			lda	$df00
			and	#$60
			beq	:51
			stx	CPU_DATA
			plp
			rts

endif

;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_BBG
:SysFetchRAM		sei
			php
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA

:ExecFetchRAM		ldx	#$0f
::51			lda	r0L,x			;Register ":r0" bis ":r7"
			pha				;zwischenspeichern.
			dex
			bpl	:51

			lda	#> $de00		;High-Byte für REU-Adresse immer
			sta	r5H			;auf $DExx setzen.
			lda	r1L			;":r5" zeigt jetzt auf das erste
			sta	r5L			;benötigte Byte auf der REU-Seite.
			jsr	DefPageBBG		;Speicher-Seite berechnen.

::52			lda	#$01			;$0100 Bytes als Startwert für
			sta	r7H			;RAM-Routinen.
			lda	#$00
			tay
			sta	r7L
			sec				;Anzahl der zu kopierenden Bytes
			sbc	r5L			;berechnen.
			sta	r7L
			lda	r7H
			sbc	#$00
			sta	r7H

			lda	r7H
			cmp	r2H
			bne	:53
			lda	r7L
			cmp	r2L			;Weniger als 256 Byte kopieren ?
::53			bcc	:54			;Nein, weiter...

			lda	r2H
			sta	r7H
			lda	r2L
			sta	r7L

::54			ldx	r7L			;Bytes aus REU einlesen und inc
::55			lda	(r5L),y			;C64-RAM kopieren.
			sta	(r0L),y
			iny
			dex
			bne	:55

			lda	r0L			;Zeiger auf C64-Adresse korrigieren.
			clc
			adc	r7L
			sta	r0L
			lda	r0H
			adc	r7H
			sta	r0H

			lda	r2L			;Anzahl bereits bearbeiteter Bytes
			sec				;korrigieren.
			sbc	r7L
			sta	r2L
			lda	r2H
			sbc	r7H
			sta	r2H

			lda	#$00			;LOW-Byte Speicherseite auf #0
			sta	r5L			;setzen (Speicherseite ab $DE00).
			inc	r1H			;Zeiger auf nächste Speicherseite.
			jsr	DefPageBBG

			lda	r2L
			ora	r2H			;Alle Bytes kopiert ?
			bne	:52			;Nein, weiter.

			ldx	#$00
			ldy	#$00
::56			pla				;Register ":r0" bis ":r7"
			sta	r0,y			;wieder zurücksetzen.
			iny
			cpy	#$10
			bne	:56

			pla
			sta	CPU_DATA
			plp				;I/O deaktivieren.
			rts

;*** REU-Speicherseite berechnen.
:DefPageBBG		lda	r1H
			pha
			sta	$dffe
			lda	#$00
			asl	r1H
			rol
			asl	r1H
			rol
			sta	$dfff
			pla
			sta	r1H
			rts
