﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Prozessortyp ausgeben.
:PrintBootInfo
if Flag64_128 = TRUE_C64
			jsr	GetPAL_NTSC		;PAL/NTSC-Flag aktualisieren.
endif
			lda	#$15
			sta	$d018
			lda	#$00			;Bildschirm löschen und Titel
			sta	$d020			;ausgeben.
			sta	$d021
			lda	#$0f
			sta	COLOR
			jsr	CLEAR
			jsr	Strg_Titel		;Installationsmeldung ausgeben.

;--- Ergänzung: 02.07.2018/M.Kanet
;Code-Rekonstruktion: Diese Routine wurde in die Version von 2003
;eingebaut um einen aktiven Emulator zu erkennen. Noch nicht verifiziert!
			lda	$dfff
			cmp	#%10101010
			beq	:901
			cmp	#%01010101
			bne	:903
			lda	$dfff
			cmp	#%10101010
			bne	:903
			beq	:902
::901			lda	$dfff
			cmp	#%01010101
			bne	:903

::902			lda	#< EmulatorText
			ldy	#> EmulatorText
			jmp	:904
;---

::903			lda	#< CompText
			ldy	#> CompText
::904			jsr	Strg_CurText

			jsr	GetComputer

			lda	ComputerType
			asl
			tax
			lda	CompVecTab +0,x
			ldy	CompVecTab +1,x
			jsr	Strg_CurText

			lda	#< SystemText
			ldy	#> SystemText
			jsr	Strg_CurText

			lda	SystemType
			asl
			tax
			lda	SysVecTab +0,x
			ldy	SysVecTab +1,x
			jsr	Strg_CurText

			lda	#< ProcText
			ldy	#> ProcText
			jsr	Strg_CurText

			jsr	GetProcessor

			lda	ProcessorType
			asl
			tax
			lda	ProcVecTab +0,x
			ldy	ProcVecTab +1,x
			jmp	Strg_CurText

;*** Aktuellen Prozessor erkennen.
:GetProcessor		sed
			lda	#$99
			clc
			adc	#$01
			cld
			bpl	:51
			LoadB	ProcessorType,0		;6502,8500,8502
			rts

::51			lda	#$01
			ldx	#$ff
			b $42
			inx
			cpx	#$00
			beq	:52
			LoadB	ProcessorType,1		;65816
			rts

::52			cmp	#$01
			beq	:53
			LoadB	ProcessorType,2		;65CE02
			rts

::53			LoadB	ProcessorType,3		;65C02
			rts

;*** Aktiven Computer ermitteln.
:GetComputer		php				;Register sichern.
			sei
if Flag64_128 = TRUE_C64
			PushB	CPU_DATA
			LoadB	CPU_DATA,$35
			PushB	$d030

			ldy	#$00

			LoadB	$d030,$00		;Auf 1Mhz umschalten.
			lda	$d030			;Mhz-Register einlesen. 1Mhz aktiv ?
			and	#%00000001		; => Nein, C64...
			bne	:51
			iny
endif
if Flag64_128 = TRUE_C128
			ldy	#1
endif
::51			sty	ComputerType		;Computertyp speichern.

if Flag64_128 = TRUE_C64
			lda	PAL_NTSC		;PAL/NTSC-Register einlesen.
endif
if Flag64_128 = TRUE_C128
			ldy	PAL_NTSC		;PAL ($FF) oder NTSC ($00) ?
			bpl	:1
			ldy	#1
::1			tya
endif
			and	#%00000001		;Flag isolieren und speichern.
			sta	SystemType

if Flag64_128 = TRUE_C64
			PopB	$d030			;Register zurücksetzen.
			PopB	CPU_DATA
endif
			plp
			rts

;******************************************************************************
;*** PAL/NTSC-Erkennung
;******************************************************************************
if Flag64_128 = TRUE_C64
			t "-G3_GetPAL_NTSC"
endif
;******************************************************************************

;*** Kernel-/Prozessor-Information.
if Flag64_128 = TRUE_C64
:BootText00		b "GEOS-MEGAPATCH64 (C)1999: KANET/MEGACOM",CR
endif
if Flag64_128 = TRUE_C128
:BootText00		b "GEOS-MEGAPATCH128 (C)1999: MARKUS KANET",CR
			b "                  (C)2003: MEGACOM SOFTWARE",CR
endif
			b "KERNEL-VERSION 3.01  BUILD:"
			d "obj.BuildID"
			b CR,CR,NULL

;*** Variablen für Computertyp.
:ComputerType		b $00
:CompVecTab		w C64
			w C128

:CompText		b "COMPUTER  : ",NULL
:EmulatorText		b "EMULATOR  : ",NULL

:C64			b "C64",CR,NULL
:C128			b "C128",CR,NULL

;*** Variablen für Computertyp.
:SystemType		b $00
:SysVecTab		w NTSC
			w PAL

:SystemText		b "SYSTEM    : ",NULL

:NTSC			b "NTSC",CR,NULL
:PAL			b "PAL",CR,NULL

;*** Variablen für Prozessortyp.
:ProcessorType		b $00
:ProcVecTab		w P6502
			w P65816
			w P65CE02
			w P65C02

if Sprache = Deutsch
:ProcText		b "AKTIVE CPU: ",NULL
endif

if Sprache = Englisch
:ProcText		b "CURRENT CPU: ",NULL
endif

:P6502			b "6502/8500/8502",CR,CR,NULL
:P65816			b "65816",CR,CR,NULL
:P65CE02		b "65CE02",CR,CR,NULL
:P65C02			b "65C02",CR,CR,NULL
