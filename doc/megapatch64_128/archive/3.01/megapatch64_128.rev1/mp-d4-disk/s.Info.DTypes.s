﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#100"
			t "G3_SymMacExt"
			f SYSTEM
			o $6000

if Flag64_128 = TRUE_C64
			c "MP_64_Disk  V3.0"
			a "M. Kanet"
endif

if Flag64_128 = TRUE_C128
			c "MP_128_Disk V3.0"
			a "M. Kanet/W. Grimm"
endif

			i
<MISSING_IMAGE_DATA>

if MP_DEMO = FALSE
;*** Kennbytes für installiert Laufwerkstreiber.
:DskDrvTypes		b $00
			b Drv1541
			b DrvShadow1541
			b Drv1571
			b Drv1581
			b Drv81DOS
			b DrvRAM1541
			b DrvRAM1571
			b DrvRAM1581
			b DrvRAMNM
			b DrvRAMNM_SCPU
			b DrvRL41
			b DrvRL71
			b DrvRL81
			b DrvRLNM
			b DrvFD41
			b DrvFD71
			b DrvFD81
			b DrvFDNM
			b DrvFDDOS
			b DrvHD41
			b DrvHD71
			b DrvHD81
			b DrvHDNM

			e DskDrvTypes +64

;*** Datensätze für installiert Laufwerkstreiber.
:DskDrvVLIR		b $00,$00			;Kein Laufwerk.
			b $01,$02			;C=1541
			b $01,$02			;C=1541 Cache
			b $03,$04			;C=1571
			b $05,$06			;C=1581
			b $29,$2a			;C=1581_DOS
			b $07,$08			;RAM 1541
			b $09,$0a			;RAM 1571
			b $0b,$0c			;RAM 1581
			b $0d,$0e			;RAM Native
			b $0f,$10			;SCPU RAM Native
			b $21,$22			;CMD RL 1541
			b $21,$24			;CMD RL 1571
			b $21,$26			;CMD RL 1581
			b $21,$28			;CMD RL NativeMode
			b $05,$12			;CMD FD 1541
			b $05,$14			;CMD FD 1571
			b $05,$16			;CMD FD 1581
			b $05,$18			;CMD FD Native
			b $29,$2a			;CMD FD DOS
			b $19,$1a			;CMD HD 1541
			b $1b,$1c			;CMD HD 1571
			b $1d,$1e			;CMD HD 1581
			b $1f,$20			;CMD HD Native

			e DskDrvVLIR +64*2
endif

if MP_DEMO = TRUE
;*** Kennbytes für installiert Laufwerkstreiber.
:DskDrvTypes		b $00
			b Drv1541
			b DrvShadow1541
			b Drv1571
			b Drv1581
			b Drv81DOS
			b DrvRAM1541
			b DrvRAM1571
			b DrvRAM1581
			b DrvRAMNM
			b DrvRAMNM_SCPU
			b DrvRL81
			b DrvFD81
			b DrvFDDOS
			b DrvHD81

			e DskDrvTypes +64

;*** Datensätze für installiert Laufwerkstreiber.
:DskDrvVLIR		b $00,$00			;Kein Laufwerk.
			b $01,$02			;C=1541
			b $01,$02			;C=1541 Cache
			b $03,$04			;C=1571
			b $05,$06			;C=1581
			b $29,$2a			;C=1581_DOS
			b $07,$08			;RAM 1541
			b $09,$0a			;RAM 1571
			b $0b,$0c			;RAM 1581
			b $0d,$0e			;RAM Native
			b $0f,$10			;SCPU RAM Native
			b $21,$26			;CMD RL 1581
			b $05,$16			;CMD FD 1581
			b $29,$2a			;CMD FD DOS
			b $1d,$1e			;CMD HD 1581

			e DskDrvVLIR +64*2
endif

if Sprache = Deutsch
;*** Installierte Laufwerkstreiber.
:DiskDrivers		b "Kein Laufwerk"
			e DiskDrivers + 1*17
endif

if Sprache = Englisch
;*** Installierte Laufwerkstreiber.
:DiskDrivers		b "No drive"
			e DiskDrivers + 1*17
endif

if MP_DEMO = FALSE
			b "C=1541"
			e DiskDrivers + 2*17
			b "C=1541 (Cache)"
			e DiskDrivers + 3*17
			b "C=1571"
			e DiskDrivers + 4*17
			b "C=1581"
			e DiskDrivers + 5*17
			b "C=1581/DOS"
			e DiskDrivers + 6*17
			b "RAM 1541"
			e DiskDrivers + 7*17
			b "RAM 1571"
			e DiskDrivers + 8*17
			b "RAM 1581"
			e DiskDrivers + 9*17
			b "RAM Native"
			e DiskDrivers +10*17
			b "SuperRAM Native"
			e DiskDrivers +11*17
			b "CMD RL 1541"
			e DiskDrivers +12*17
			b "CMD RL 1571"
			e DiskDrivers +13*17
			b "CMD RL 1581"
			e DiskDrivers +14*17
			b "CMD RL Native"
			e DiskDrivers +15*17
			b "CMD FD 1541"
			e DiskDrivers +16*17
			b "CMD FD 1571"
			e DiskDrivers +17*17
			b "CMD FD 1581"
			e DiskDrivers +18*17
			b "CMD FD Native"
			e DiskDrivers +19*17
			b "CMD FD PCDOS"
			e DiskDrivers +20*17
			b "CMD HD 1541"
			e DiskDrivers +21*17
			b "CMD HD 1571"
			e DiskDrivers +22*17
			b "CMD HD 1581"
			e DiskDrivers +23*17
			b "CMD HD Native"
			e DiskDrivers +24*17
			b NULL

			e DiskDrivers +64*17
endif

if MP_DEMO = TRUE
			b "C=1541"
			e DiskDrivers + 2*17
			b "C=1541 (Cache)"
			e DiskDrivers + 3*17
			b "C=1571"
			e DiskDrivers + 4*17
			b "C=1581"
			e DiskDrivers + 5*17
			b "C=1581/DOS"
			e DiskDrivers + 6*17
			b "RAM 1541"
			e DiskDrivers + 7*17
			b "RAM 1571"
			e DiskDrivers + 8*17
			b "RAM 1581"
			e DiskDrivers + 9*17
			b "RAM Native"
			e DiskDrivers +10*17
			b "SuperRAM Native"
			e DiskDrivers +11*17
			b "CMD RL 1581"
			e DiskDrivers +12*17
			b "CMD FD 1581"
			e DiskDrivers +13*17
			b "CMD FD PCDOS"
			e DiskDrivers +14*17
			b "CMD HD 1581"
			e DiskDrivers +15*17
			b NULL

			e DiskDrivers +64*17
endif
