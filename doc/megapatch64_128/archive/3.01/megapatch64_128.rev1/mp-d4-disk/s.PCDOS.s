﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "DiskDev_PCDOS"
			t "G3_SymMacExtDisk"

			a "M. Kanet"
			o DISK_BASE

if .p
			t "s.DOS_Turbo.ext"

:C_41			= FALSE
:C_71			= FALSE
:C_81			= FALSE
:RD_41			= FALSE
:RD_71			= FALSE
:RD_81			= FALSE
:RD_NM			= FALSE
:RD_NM_SCPU		= FALSE
:RL_41			= FALSE
:RL_71			= FALSE
:RL_81			= FALSE
:RL_NM			= FALSE
:HD_41			= FALSE
:HD_71			= FALSE
:HD_81			= FALSE
:HD_NM			= FALSE
:HD_41_PP		= FALSE
:HD_71_PP		= FALSE
:HD_81_PP		= FALSE
:HD_NM_PP		= FALSE
:FD_41			= FALSE
:FD_71			= FALSE
:FD_81			= FALSE
:FD_NM			= FALSE
:PC_DOS			= TRUE

.DriveModeFlags		= SET_MODE_SUBDIR

.PART_TYPE		= $05
.PART_MAX		= 0

.dir3Head		= $9b80
.DiskDrvMode		= DrvPCDOS
.Tr_BorderBlock		= 79
.Se_BorderBlock		= 01
.Tr_1stDirSek		= 01
.Se_1stDirSek		= 01
.Tr_1stDataSek		= 32
.Se_1stDataSek		= 00
.Tr_DskNameSek		= 00
.Se_DskNameSek		= 01
.Tr_BootSektor		= 00
.Se_BootSektor		= 01
.MaxDirPages		= 255

;*** Einsprungadressen für Bootsektor-Informationen.
.TMP_AREA_SEKTOR	= diskBlkBuf
.RAM_AREA_SEKTOR	= $0000
.TMP_AREA_BUFFER	= curDirHead
.RAM_AREA_BUFFER	= $3e00
.TMP_AREA_BOOT		= diskBlkBuf
.RAM_AREA_BOOT		= $0200
.TMP_AREA_FAT		= $4000
.RAM_AREA_FAT		= $0400
.TMP_AREA_DIR		= $4000
.RAM_AREA_DIR		= $2000
.TMP_AREA_ALIAS		= $4000
.RAM_AREA_ALIAS		= $4000
endif

;*** Sprungtabelle.
:JumpTab		t "-D3_PCDOS"

;*** Include-Dateien.
			t "-D3_IncludeFile"

;******************************************************************************
			g DISK_BASE + DISK_DRIVER_SIZE -1
;******************************************************************************
