﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "DiskDev_1571"
			t "G3_SymMacExtDisk"

			a "M. Kanet"
			o DISK_BASE

if .p
			t "s.1571_Turbo.ext"

:C_41			= FALSE
:C_71			= TRUE
:C_81			= FALSE
:RD_41			= FALSE
:RD_71			= FALSE
:RD_81			= FALSE
:RD_NM			= FALSE
:RD_NM_SCPU		= FALSE
:RL_41			= FALSE
:RL_71			= FALSE
:RL_81			= FALSE
:RL_NM			= FALSE
:HD_41			= FALSE
:HD_71			= FALSE
:HD_81			= FALSE
:HD_NM			= FALSE
:HD_41_PP		= FALSE
:HD_71_PP		= FALSE
:HD_81_PP		= FALSE
:HD_NM_PP		= FALSE
:FD_41			= FALSE
:FD_71			= FALSE
:FD_81			= FALSE
:FD_NM			= FALSE
:PC_DOS			= FALSE

:DriveModeFlags		= $00

:PART_TYPE		= $02
:PART_MAX		= 0

:dir3Head		= $9c80
:DiskDrvMode		= Drv1571
:Tr_BorderBlock		= 18
:Se_BorderBlock		= 2
:Tr_1stDirSek		= 18
:Se_1stDirSek		= 1
:Tr_1stDataSek		= 1
:Se_1stDataSek		= 0
:Tr_DskNameSek		= 18
:Se_DskNameSek		= 0
:MaxDirPages		= 18
endif

;*** Sprungtabelle.
:JumpTab		t "-D3_JumpTable"

;*** Tabellen für TurboDOS-Übertragung.
:DefNibbleData		b $0f,$07,$0d,$05,$0b,$03,$09,$01
			b $0e,$06,$0c,$04,$0a,$02,$08,$00
:NibbleByteH		b $00,$80,$20,$a0,$40,$c0,$60,$e0
			b $10,$90,$30,$b0,$50,$d0,$70,$f0
:NibbleByteL		b $00,$20,$00,$20,$10,$30,$10,$30
			b $00,$20,$00,$20,$10,$30,$10,$30

;*** Include-Dateien.
			t "-D3_IncludeFile"

;*** TurboDOS-Routine für 1571.
:TurboDOS_1571		d "obj.Turbo71"

;******************************************************************************
			g DISK_BASE + DISK_DRIVER_SIZE -1
;******************************************************************************
