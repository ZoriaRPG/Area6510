﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** BAM für RAM-Laufwerk erzeugen.
:CreateBAM		lda	curDrive
			pha
			lda	DriveAdr
			jsr	SetDevice		;Laufwerk aktivieren.

;--- RAM-Laufwerk bereits installiert ?
::51			jsr	TestCurBAM		;Bam testen.
			txa				;Ist BAM gültig ?
			bne	:52			; => Nein, weiter...

			jsr	AskClearCurBAM		;Diskette formatieren ?
			txa
			beq	:53			; => Nicht löschen, weiter...

::52			jsr	ClearCurBAM		;Aktuelle Diskette löschen.
			txa				;Diskettenfehler ?
			bne	:54			; => Ja, Ende...

::53			jsr	AskClearBAMboot		;Abfrage "BAM immer löschen ?"

			ldx	#$00			;Kein Fehler...
			b $2c
::54			ldx	#DEV_NOT_FOUND		;Nicht installiert.
			stx	:55 +1
			pla
			jsr	SetDevice		;Laufwerk zurücksetzen.
::55			ldx	#$ff
			rts

;*** Dialogbox: "Aktuelle BAM löschen ?".
:AskClearCurBAM		bit	firstBoot		;GEOS-BootUp ?
			bmi	:51			; => Nein, weiter...
			ldx	AutoClearBAM		;Flag für "BAM löschen" einlesen.
			rts

::51			lda	DriveAdr
			clc
			adc	#$39
			sta	DrvName +9
			LoadW	r0,Dlg_ClearBAM
			jsr	DoDlgBox		;Abfrage: "BAM löschen ?"

			ldx	#$00
			lda	sysDBData
			cmp	#NO			;BAM löschen ?
			beq	:52			; => Nein, weiter...
			dex
::52			rts

;*** Dialogbox: "BAM löschen ?".
:AskClearBAMboot	bit	firstBoot		;GEOS-BootUp ?
			bmi	:51			; => Nein, weiter...
			rts

::51			LoadW	r0,Dlg_AutoClearBAM
			jsr	DoDlgBox		;Abfrage: "BAM automatisch löschen?"

			ldx	#$00
			lda	sysDBData
			cmp	#NO			;BAM während GEOS-BootUp löschen ?
			beq	:52			; => Nein, weiter...
			dex
::52			cpx	AutoClearBAM
			beq	:53
			stx	AutoClearBAM

			lda	DriveMode
			ldx	#< DSK_INIT_SIZE
			stx	r2L
			ldx	#> DSK_INIT_SIZE
			stx	r2H
			jsr	SaveDskDrvData

::53			lda	DriveAdr
			jsr	SetDevice		;Laufwerk aktivieren.
			jmp	OpenDisk		;Diskette öffnen.

;*** Variablen.
:AutoClearBAM		b $00

;*** Dialogbox.
:Dlg_ClearBAM		b %11100001
			b DBGRPHSTR
			w Dlg_DrawBoxTitel
			b DBTXTSTR ,$10,$0b
			w DlgBoxTitle
			b DBTXTSTR ,$10,$20
			w :51
			b DBTXTSTR ,$10,$2a
			w DrvName
			b YES      ,$02,$48
			b NO       ,$10,$48
			b NULL

if Sprache = Deutsch
::51			b PLAINTEXT,BOLDON
			b "Inhaltsverzeichnis auf",NULL
:DrvName		b "Laufwerk X: löschen ?",NULL
endif

if Sprache = Englisch
::51			b PLAINTEXT,BOLDON
			b "Clear directory",NULL
:DrvName		b "on drive X: ?",NULL
endif

;*** Dialogbox.
:Dlg_AutoClearBAM	b %11100001
			b DBGRPHSTR
			w Dlg_DrawBoxTitel
			b DBTXTSTR ,$10,$0b
			w DlgBoxTitle
			b DBTXTSTR ,$10,$20
			w :51
			b DBTXTSTR ,$10,$2a
			w :52
			b YES      ,$02,$48
			b NO       ,$10,$48
			b NULL

if Sprache = Deutsch
::51			b PLAINTEXT,BOLDON
			b "Inhaltsverzeichnis bei",NULL
::52			b "jedem Neustart löschen ?",NULL
endif

if Sprache = Englisch
::51			b PLAINTEXT,BOLDON
			b "Clear directory at",NULL
::52			b "GEOS-startup ?",NULL
endif

;*** Titel für Dialogbox zeichnen.
:Dlg_DrawBoxTitel	b NEWPATTERN,$01
			b MOVEPENTO
			w $0040 ! DOUBLE_W
			b $20
			b RECTANGLETO
			w $00ff ! DOUBLE_W
			b $2f
			b NULL
