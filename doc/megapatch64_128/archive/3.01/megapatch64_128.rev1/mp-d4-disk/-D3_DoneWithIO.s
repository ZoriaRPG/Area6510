﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81!FD_41!FD_71!FD_81!PC_DOS!HD_41!HD_71!HD_81
::tmp0b = HD_41_PP!HD_71_PP!HD_81_PP
::tmp0  = Flag64_128!:tmp0a!:tmp0b
if :tmp0 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	CPU_RegBuf		;CPU-Register zurücksetzen.
			sta	CPU_DATA

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp1a = C_41!C_71!C_81!FD_41!FD_71!FD_81!PC_DOS!HD_41!HD_71!HD_81
::tmp1b = HD_41_PP!HD_71_PP!HD_81_PP
::tmp1  = Flag64_128!:tmp1a!:tmp1b
if :tmp1 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	RegD030_Buf
			sta	$d030
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücks.
			sta	$d01a

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			sta	MMU
			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp2 = Flag64_128!FD_NM!HD_NM!HD_NM_PP
if :tmp2 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	#$00			;I/O-Modus löschen.
			sta	IO_Activ

			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	CPU_RegBuf		;CPU-Register zurücksetzen.
			sta	CPU_DATA

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp3 = Flag64_128!FD_NM!HD_NM!HD_NM_PP
if :tmp3 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	#$00			;I/O-Modus löschen.
			sta	IO_Activ

			lda	RegD030_Buf
			sta	$d030
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			sta	MMU
			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp4 = Flag64_128!RL_41!RL_71!RL_81!RD_41!RD_71!RD_81
if :tmp4 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	CPU_RegBuf		;CPU-Register zurücksetzen.
			sta	CPU_DATA

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp5 = Flag64_128!RL_41!RL_71!RL_81!RD_41!RD_71!RD_81
if :tmp5 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	RegD030_Buf
			sta	$d030
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			sta	MMU
			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp6 = Flag64_128!RL_NM!RD_NM!RD_NM_SCPU
if :tmp6 = TRUE_C64!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	#$00			;I/O-Modus löschen.
			sta	IO_Activ

			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	CPU_RegBuf		;CPU-Register zurücksetzen.
			sta	CPU_DATA

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif

;******************************************************************************
::tmp7 = Flag64_128!RL_NM!RD_NM!RD_NM_SCPU
if :tmp7 = TRUE_C128!TRUE
;******************************************************************************
;*** I/O abschalten.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU
:xDoneWithIO		sei
			lda	#$00			;I/O-Modus löschen.
			sta	IO_Activ

			lda	RegD030_Buf
			sta	$d030
			lda	RegD015_Buf		;Sprites wieder aktivieren.
			sta	$d015

			lda	#$7f			;NMIs sperren.
			sta	$dd0d
			lda	$dd0d

			lda	RegD01A_Buf		;IRQ-Maskenregister zurücksetzen.
			sta	$d01a

			lda	IRQ_RegBuf		;IRQ-Status zurücksetzen.
			pha
			plp
			rts

;*** Neue IRQ/NMI-Routine.
:NewIRQ			pla
			sta	MMU
			pla
			tay
			pla
			tax
			pla
:NewNMI			rti
endif
