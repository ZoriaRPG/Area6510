﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp0 = TRUE_C64!TRUE
;******************************************************************************
;*** Sektor über Partitions-Register einlesen.
;    Übergabe:		r1   = Track/Sektor.
;			r3H  = Partitions-Nr.
;			r4   = Sektorspeicher.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_SekRead		ldy	#$80
			b $2c
:xDsk_SekWrite		ldy	#$90
			b $2c
:xDsk_SekVerify		ldy	#$a0
			b $2c
:xDsk_SekSwap		ldy	#$b0
:xDsk_DoSekJob		jsr	xTestRL_RAM		;Auf RAMLink testen.
			cpx	#$00			;Ist RAMLink verfügbar ?
			bne	:51			;Nein, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			lda	CPU_DATA		;CPU Register einlesen und
			pha				;zwischenspeichern.
			lda	#$36			;I/O-Bereich und Kernal für
			sta	CPU_DATA		;RAMLink-Transfer aktivieren.

			tya
			pha
			jsr	$e0a9			;RL-Hardware aktivieren.
			pla				;Sektor-Daten setzen.
			sta	$de20
			lda	r1L
			sta	$de21
			lda	r1H
			sta	$de22
			lda	r4L
			sta	$de23
			lda	r4H
			sta	$de24
			lda	r3H
			sta	$de25
			lda	#$01
			sta	$de26

			jsr	$fe09			;Sektor-Jobcode ausführen.

			lda	$de20			;Fehlerstatus einlesen und
			pha				;zwischenspeichern.
			jsr	$fe0f			;RL-Hardware abschalten.
			pla
			tax
			pla
			sta	CPU_DATA		;CPU-Register zurücksetzen.
			plp				;IRQ-Status zurücksetzen.
::51			rts
endif

;******************************************************************************
::tmp1 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp1 = TRUE_C128!TRUE
;******************************************************************************
;*** Sektor über Partitions-Register einlesen.
;    Übergabe:		r1   = Track/Sektor.
;			r3H  = Partitions-Nr.
;			r4   = Sektorspeicher.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_SekRead		ldy	#$80
			b $2c
:xDsk_SekWrite		ldy	#$90
			b $2c
:xDsk_SekVerify		ldy	#$a0
			b $2c
:xDsk_SekSwap		ldy	#$b0
:xDsk_DoSekJob		jsr	xTestRL_RAM		;Auf RAMLink testen.
			cpx	#$00			;Ist RAMLink verfügbar ?
			bne	:51			;Nein, Abbruch...

			php				;IRQ-Status zwischenspeichern
			jsr	InitRLKonfig		;RL-Konfiguration einschalten

			pha
			jsr	$e0a9			;RL-Hardware aktivieren.

			pla				;Sektor-Daten setzen.
			sta	$de20
			lda	r1L
			sta	$de21
			lda	r1H
			sta	$de22
			lda	r4L
			sta	$de23
			lda	r4H
			sta	$de24
			lda	r3H
			sta	$de25
			LoadB	$de26,1			;C128 Bank 1

			jsr	$fe09			;Sektor-Jobcode ausführen.

			lda	$de20			;Fehlerstatus einlesen und
			pha				;zwischenspeichern.
			jsr	ExitRLKonfig
			pla
			tax
			plp				;IRQ-Status zurücksetzen.
::51			rts

:ExitRLKonfig		jsr	$fe0f			;RL-Hardware abschalten.
:LastRAM_Conf_Reg	lda	#$00			;wird gesetzt
			sta	RAM_Conf_Reg		;Konfiguration rücksetzen
:LastMMU		lda	#$00			;wird gesetzt
			sta	MMU			;Konfiguration rücksetzen
			rts

:InitRLKonfig		sei				;und IRQs sperren.
			lda	MMU			;Konfiguration sichern
			sta	LastMMU+1
			LoadB	MMU,%01001110		;Ram1 bis $bfff + IO + Kernal
							;I/O-Bereich und Kernal für
							;RAMLink-Transfer aktivieren.
			lda	RAM_Conf_Reg		;Konfiguration sichern
			sta	LastRAM_Conf_Reg+1
			and	#%11110000
			ora	#%00000100		;Common Area $0000 bis $0400
			sta	RAM_Conf_Reg
			tya
			rts
endif

;******************************************************************************
::tmp2 = RD_NM!RD_81!RD_71!RD_41
if :tmp2 = TRUE
;******************************************************************************
;*** Sektor über Partitions-Register einlesen.
;    Übergabe:		r1   = Track/Sektor.
;			r3H  = Partitions-Nr.
;			r4   = Sektorspeicher.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_SekRead		ldy	#%10010001
			b $2c
:xDsk_SekWrite		ldy	#%10010000
			b $2c
:xDsk_SekVerify		ldy	#%10010011
			b $2c
:xDsk_SekSwap		ldy	#%10010010
:xDsk_DoSekJob		jsr	Save_RegData

			tya
			pha
			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			pla
			tay

			LoadW	r2,$0100		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.
			jsr	DoRAMOp			;Daten aus RAMLink einlesen.

			jsr	Load_RegData
			ldx	#$00			;Flag für "Kein Fehler..."
			rts
endif

;******************************************************************************
::tmp3 = RD_NM_SCPU
if :tmp3 = TRUE
;******************************************************************************
;*** Sektor über Partitions-Register einlesen.
;    Übergabe:		r1   = Track/Sektor.
;			r3H  = Partitions-Nr.
;			r4   = Sektorspeicher.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_SekRead		ldy	#%10010001
			b $2c
:xDsk_SekWrite		ldy	#%10010000
			b $2c
:xDsk_SekVerify		ldy	#%10010011
			b $2c
:xDsk_SekSwap		ldy	#%10010010
:xDsk_DoSekJob		jsr	Save_RegData

			tya
			pha
			jsr	DefSekAdrREU		;Sektor-Adresse berechnen.
			pla
			tay

			LoadW	r2,$0100		;Anzahl Bytes.
			MoveW	r4,r0			;Zeiger auf C64-Speicher.

			php
			sei

			cpy	#%10010000		;StashRAM ?
			bne	:51			; => Nein, weiter...
			jsr	DoStash			;StashRAM für SCPU ausführen.
			jmp	:54

::51			cpy	#%10010001		;FetchRAM ?
			bne	:52			; => Nein, weiter...
			jsr	DoFetch			;FetchRAM für SCPU ausführen.
			jmp	:54

::52			cpy	#%10010010		;SwapRAM ?
			bne	:53			; => Nein, weiter...
			jsr	DoSwap			;SwapRAM für SCPU ausführen.
			jmp	:54

::53			cpy	#%10010011		;SwapRAM ?
			bne	:54			; => Nein, weiter...
			jsr	DoVerify		;SwapRAM für SCPU ausführen.
			txa				;Verify-Error ?
			beq	:54			; => Nein, weiter...
			ldx	#%00100000
			b $2c

::54			ldx	#%01000000		;Kein Fehler...
			plp

			txa				;Zurück zum Programm.
			pha
			jsr	Load_RegData
			pla
			ldx	#NO_ERROR
			rts

;*** StashRAM-Routine (16-Bit NativeCode).
:DoStash		lda	r3L			;Speicherbank berechnen.
			sta	:51 +1

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$02			;ldx $0002
			b $a4,$04			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			rts

;*** FetchRAM-Routine (16-Bit NativeCode).
:DoFetch		lda	r3L			;Speicherbank berechnen.
			sta	:51 +2

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$04			;ldx $0002
			b $a4,$02			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			rts

;*** SwapRAM-Routine (16-Bit NativeCode).
:DoSwap			PushW	r0			;Register r0/r1 speichern.
			PushW	r1

			lda	r3L			;Speicherbank berechnen.
			sta	:52 +3
			sta	:53 +3

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $48				;pha
			b $a6,$04			;ldx r1
::52			b $bf,$00,$00,$00		;lda $??:0000,x
			b $a6,$02			;ldx r0
			b $9f,$00,$00,$00		;sta $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $68				;pla
			b $a6,$04			;ldx r1
::53			b $9f,$00,$00,$00		;sta $??:0000,x
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$db			;bne :51

			b $38				;sec
			b $fb				;xce

			PopW	r1			;Register r0/r1 speichern.
			PopW	r0
			rts

;*** VerifyRAM-Routine (16-Bit NativeCode).
:DoVerify		PushW	r0			;Register r0/r1 speichern.
			PushW	r1
			PushB	r3L

			lda	r3L			;Speicherbank berechnen.
			sta	:52 +3

			lda	#$ff
			sta	r3L

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $a6,$04			;ldx r1
::52			b $df,$00,$00,$00		;cmp $??:0000,x
			b $d0,$0a			;bne :53
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$e7			;bne :51
			b $e6,$08			;inc r3L

::53			b $38				;sec
			b $fb				;xce

			ldx	r3L

			PopB	r3L
			PopW	r1			;Register r0/r1 speichern.
			PopW	r0
			rts
endif
