﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#114"
			t "G3_SymMacExt"
			t "G3_Disk.V.Class"
			t "-DD_JumpTab"

;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		lda	#$00
			tax
			tay
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

;--- Angeschlossenes Laufwerk testen.
::51			lda	DriveAdr		;Aktuelles Laufwerk feststellen.
			jsr	TestDriveType
			cpx	#$00			;Installationsfehler ?
			bne	:53			; => Ja, Abbruch...
			sta	r0L

			ldx	DriveMode
			cpx	#Drv1581		;1581-Laufwerk installieren ?
			bne	:52			; => Nein, weiter...

;--- 1581-Laufwerk erkennen.
			cpx	r0L			;1581-Laufwerk angeschlossen ?
			beq	:56			; => Ja, weiter...

;--- 1581-Treiber darf nur 1541 einrichten, sonst kommt es beim booten zu
;    Installationsproblemen wenn eine 1541 und eine 1541 installiert wird!
;<*>			lda	r0L
;<*>			and	#%11110000
;<*>			cmp	#DrvFD			;CMD-FD angeschlossen ?
;<*>			beq	:56			; => Ja, weiter...

;--- Keine 1581, Einschaltmeldung ausgeben.
			bne	:53			; => Nein, 1581 suchen...

;--- FD-Laufwerk erkennen.
::52			eor	DriveMode		;Laufwerkstyp erkannt ?
			beq	:56			; => Ja, weiter...
			and	#%11110000		;CMD-Laufwerk angeschlossen ?
			beq	:56			; => Ja, Laufwerk installieren.
							;    Hier stimmt nur das Partitions-
							;    format nicht. Dieses wird von
							;    ":OpenDisk" aktiviert.
;--- Kompatibles Laufwerk suchen.
::53			lda	DriveMode
			and	#%11110000		;CMD-FD/HD-Laufwerk ?
			bne	:54			; => Ja, weiter...
			lda	DriveMode		; => 15x1-Laufwerk.
			and	#%00001111
::54			ldy	DriveAdr
			jsr	FindDrive		;15x1-Laufwerk suchen.
			cpx	#$00			;Laufwerk gefunden ?
			beq	:56			; => Ja, weiter...

			lda	DriveAdr
			jsr	TurnOnNewDrive		;Dialogbox ausgeben.
			txa				;Lauafwerk eingeschaltet ?
			beq	:53			; => Ja, Laufwerk suchen...

;--- Kein passendes Laufwerk gefunden.
::55			ldx	#DEV_NOT_FOUND
			rts

;--- Laufwerk installieren.
::56			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			and	#%00001111
			sta	driveType   -8,x
			sta	curType
			txa				;Aktuelles Laufwerk feststellen.
			jsr	TestDriveType
			cpx	#$00			;Installationsfehler ?
			bne	:55			; => Ja, Abbruch...
			tya
			ldx	DriveAdr		;Laufwerksdaten setzen.
			and	#%11110000
			ora	curType
			sta	RealDrvType -8,x
			sta	BASE_DDRV_DATA + (DiskDrvType - DISK_BASE)

			ldy	#$00
			and	#%11110000
			beq	:57
			ldy	#SET_MODE_PARTITION
::57			lda	RealDrvType-8,x
			and	#%00001111
			cmp	#DrvNative
			bne	:58
			tya
			ora	#SET_MODE_SUBDIR
			tay
::58			tya
			sta	RealDrvMode-8,x

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- Ende, kein Fehler...
			ldx	#NO_ERROR
			rts

;*** Laufwerk deinstallieren.
:xDeInstallDrive	lda	#$00
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
