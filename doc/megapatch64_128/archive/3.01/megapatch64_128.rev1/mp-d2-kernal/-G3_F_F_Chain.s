﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Sektorkette auf Disk freigeben.
:FreeSeqChain		lda	r1H
			ldx	r1L
			beq	:3

			ldy	#$00
			sty	r2L			;Blocks löschen.
			sty	r2H

::1			sta	r1H
			stx	r1L

			sta	r6H
			stx	r6L
			jsr	FreeBlock		;Sektor freigeben.
			txa				;Diskettenfehler ?
			bne	:3			;Ja, Abbruch...

			inc	r2L			;Anzahl gelöschte Blocks
			bne	:2			;um 1 erhöhen.
			inc	r2H

::2			jsr	GetBlock_dskBuf		;Sektor einlesen.
			txa				;Diskettenfehler ?
			bne	:3			;Ja, Abbruch...

			lda	diskBlkBuf +1		;Noch ein Sektor ?
			ldx	diskBlkBuf +0
			bne	:1			;Nächsten Sektor freigeben.

::3			rts

;*** Sektorkette verfolgen und
;    Track/Sektor-Tabelle anlegen.
:xFollowChain		lda	r3H
			pha

			lda	r1H
			ldx	r1L
			beq	:4

			ldy	#$00
::1			iny
			sta	(r3L),y			;eintragen.
			dey
			txa
			sta	(r3L),y
			iny
			iny
			bne	:2
			inc	r3H

::2			txa				;Sektor verfügbar ?
			beq	:4			;Nein, Ende...
			tya
			pha

			jsr	GetBlock_dskBuf		;Sektor einlesen.
			pla
			tay
			txa				;Diskettenfehler ?
			bne	:4			;Ja, Abbruch...

;--- Ergänzung: 08.07.18/M.Kanet
;In der Version von 2003 wurde das Problem mit FollowChain teilweise
;behoben. Die Routine arbeitet aber trotzdem nicht korrekt wenn
;der erste Track/Sektor = $00/$FF ist. In diesem Fall wird keine
;gültige Sektortabelle übergebem.
			lda	diskBlkBuf +1		;Zeiger auf nächsten Sektor.
			sta	r1H
			ldx	diskBlkBuf +0
			stx	r1L
			jmp	:1

::4			pla
			sta	r3H
			rts
