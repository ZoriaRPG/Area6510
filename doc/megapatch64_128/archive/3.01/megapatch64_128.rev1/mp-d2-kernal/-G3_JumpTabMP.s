﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;*** Speicher bis zur MP-Sprungtabelle mit $00-Bytes auffüllen.
;*** ACHTUNG! Sprungtabelle darf nur nach "hinten" mit neuen
;*** Sprungbefehlen aufgefüllt werden.
;******************************************************************************
.Mem_C0FF_Temp		e $c0dc
.Mem_C0FF
;******************************************************************************
;*** Einsprungtabelle für neue Kernal-Routinen.
.i_UserColor		jmp	xi_UserColor
.i_ColorBox		jmp	xi_ColorBox
.DirectColor		jmp	xDirectColor
.RecColorBox		jmp	xRecColorBox
.GetBackScreen		jmp	xGetBackScreen
.ResetScreen		jmp	xResetScreen
.GEOS_InitSystem	jmp	GEOS_Init1
.PutKeyInBuffer		jmp	NewKeyInBuf

;*** Einsprungtabelle für SCPU-Routinen.
.SCPU_Pause		jmp	xSCPU_Pause

;*** Hier wird direkt variabler Code eingetragen.
;    Beim booten mit SCPU stehen hier Vektoren auf die
;    Optimierungsroutinen.
;    Wird beim booten von GEOS modifiziert wenn SCPU vorhanden.

.Patch_BASE_4
if SCPU_Mode = FALSE
.SCPU_OptOn		rts
			b $00,$00
.SCPU_OptOff		rts
			b $00,$00
.SCPU_SetOpt		rts
			b $00,$00
else
.SCPU_OptOn		jmp	xSCPU_OptOn
.SCPU_OptOff		jmp	xSCPU_OptOff
.SCPU_SetOpt		jmp	xSCPU_SetOpt
endif
