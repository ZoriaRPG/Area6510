﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** IRQ-Routine von GEOS.
:xInterruptMain

;--- C64: Maus/Bildschirmschoner/Druckerspooler aktualisieren.
if Flag64_128 = TRUE_C64
			jsr	InitMouseData		;Mausabfrage.
			jsr	IntScrnSave		;Bildschirmschoner testen.
			jsr	IntPrnSpool		;Druckerspooler testen.
endif

if Flag64_128 = TRUE_C128
;--- C128: Bildschirmschoner/Druckerspooler aktualisieren.
;Beide Routinen befinden sich unter IO-Bereich!
;jsr InitMouseData liegt in der Haupt-IRQ-Routine!
			jsr	dIntScrnSave		;Bildschirmschoner testen.
			jsr	dIntPrnSpool		;Druckerspooler testen.
endif

			jsr	PrepProcData		;Prozessabfrage.
			jsr	DecSleepTime		;"SLEEP"-Abfrage.
			jsr	SetCursorMode		;Cursormodus festlegen.

			jmp	GetRandom		;Zufallszahlen berechnen.
							;(Bei C128 im $d000-Bereich!!!)
