# Area6510

### GEOS MegaPatch64_128 Rev.0
This is a documentation of the source code for GEOS MegaPatch64 and MegaPatch128. This might be the code that was shared between the two main developers but might not include most of my recent changes for the MegaPatch64 version..
There is also another code base name "MegaPatch64 Rev.0" which includes some additional changes/fixes, see the /diff directory for some details.

There is currently no final source code in geoWrite Format available so do not even ask for it. There is still a lot of work to do.
If everything is done you can download the sources as D81-diskimages from the /src directory.
