﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "GEOS128.1"
			t "G3_SymMacExt"
			t "G3_Boot.V.Class"

;			o BASE_GEOS_SYS -2
;			p BASE_GEOS_SYS

			o $2000 -2
			p $2000

			i

if Sprache = Deutsch
			h "MegaPatch-Kernal Bank 1"
			h "Grundfunktionen..."
endif

if Sprache = Englisch
			h "MegaPatch-kernal bank 1"
			h "mainprogramm..."
endif

;--- Ladeadresse.
:MainInit		w BASE_GEOS_SYS			;DUMMY-Bytes, da Kernal über
							;BASIC-Load eingelesen wird.
;--- Diskettentreiber für Bootvorgang.
.DiskDriver		;d "DiskDev_1581"		;Standard-Bootlaufwerk.
			d "DiskDev_RL81"		;Testlaufwerk
;			e BASE_GEOS_SYS + DISK_DRIVER_SIZE
			e $2000 + DISK_DRIVER_SIZE

;--- GEOS128-Kernal.
.GEOS_Kernal1		d "obj.G3_K128_B1"
