﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Systemtexte.
:NoDrvText		b PLAINTEXT,"Laufwerk ?",NULL
:NoDskText		b PLAINTEXT,"Diskette ?",NULL
:KFreeText		b PLAINTEXT,"Kb frei",NULL
:InfoText2		b PLAINTEXT
			b GOTOXY
			w $0018
			b $3a
			b "Freier Speicher auf Ziel-Diskette: "
			b NULL

:ExtractFName		b PLAINTEXT
			b GOTOXY
			w $0010
			b $b6
			b "Entpacke Datei: ",NULL

:DskErrInfText		b PLAINTEXT,BOLDON
			b GOTOXY
			w $0050
			b $74
			b "Fehler-Code:",NULL
:DskErrCode		b $00
:DskErrTitel		b PLAINTEXT,BOLDON
			b "Installation fehlgeschlagen:",NULL

:DlgT_01_01		b "Unbekannter Fehler!",NULL
:DlgT_02_01		b "Die GEOS-ID konnte nicht",NULL
:DlgT_02_02		b "gespeichert werden!",NULL
:DlgT_03_01		b "Die Datei konnte nicht",NULL
:DlgT_03_02		b "entpackt werden!",NULL
:DlgT_04_01		b "Die Datei 'StartMP3_64'",NULL
:DlgT_04_02		b "ist fehlerhaft!",NULL
:DlgT_05_01		b "Prüfsummenfehler in",NULL
:DlgT_05_02		b "Datei 'StartMP3_64'!",NULL

:InfoText0		b PLAINTEXT
			b GOTOXY
			w $0010
			b $60
			b "Bitte haben Sie einen kleinen Augenblick Geduld,"
			b GOTOXY
			w $0010
			b $68
			b "während -SetupMP- das Archiv mit den gepackten"
			b GOTOXY
			w $0010
			b $70
			b "MegaPatch-Dateien untersucht."
			b GOTOXY
			w $0010
			b $80
			b "Dieser Vorgang kann einige Minuten dauern..."

:InfoText0a		b GOTOXY
			w $0010
			b $9e
			b "* Archiv auf Fehler untersuchen...",NULL

:InfoText0b		b GOTOXY
			w $0010
			b $a8
			b "* Datei-Informationen einlesen...",NULL
:Icon_Text0		b PLAINTEXT
			b GOTOXY
			w $0010
			b $60
			b "Installationsprogramm für GEOS-MegaPatch64"
			b GOTOXY
			w $0010
			b $70
			b "Das Programm  wird Sie während der Installation"
			b GOTOXY
			w $0010
			b $78
			b "des GEOS-MegaPatch unterstützen."
			b GOTOXY
			w $0010
			b $88
			b "Mit der Taste '!' kann der Installationsvorgang"
			b GOTOXY
			w $0010
			b $90
			b "jederzeit beendet werden."

			b GOTOXY
			w $00d2
			b $ae
			b "Installation"
			b GOTOXY
			w $00d7
			b $b6
			b "fortsetzen"
			b NULL
:Icon_Text1		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Wählen Sie das Laufwerk auf das die"
			b GOTOXY
			w $0018
			b $60
			b "Systemdateien kopiert werden sollen:"

			b GOTOXY
			w $0023
			b $84
			b "A:"
			b GOTOXY
			w $0023
			b $ac
			b "B:"
			b GOTOXY
			w $00a3
			b $84
			b "C:"
			b GOTOXY
			w $00a3
			b $ac
			b "D:"
			b NULL

:Icon_Text2		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Installationsprogramm für GEOS - MegaPatch64"
			b GOTOXY
			w $0018
			b $60
			b "Bitte wählen Sie die Art der Installation:"

			b GOTOXY
			w $0048
			b $76
			b "Komplette Installation mit allen Dateien"
			b GOTOXY
			w $0048
			b $7e
			b "auf Diskette oder CMD-Partition."
			b GOTOXY
			w $0048
			b $96
			b "Teilweise Installation / aktualisieren"
			b GOTOXY
			w $0048
			b $9e
			b "einer vorhandenen Startdiskette."
			b GOTOXY
			w $0048
			b $a6
			b "(Empfohlen für 1541-Startdisketten)"
			b NULL

:Icon_Text11		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Das Ziel-Laufwerk enthält bereits Dateien"
			b GOTOXY
			w $0018
			b $60
			b "des GEOS-MegaPatch. Sollen die vorhandenen"
			b GOTOXY
			w $0018
			b $68
			b "Dateien gelöscht werden ?"

			b GOTOXY
			w $001f
			b $96
			b "Systemdateien"
			b GOTOXY
			w $0030
			b $9e
			b "löschen"

			b GOTOXY
			w $007a
			b $96
			b "Installation"
			b GOTOXY
			w $7f
			b $9e
			b "fortsetzen"

			b GOTOXY
			w $00e1
			b $96
			b "DeskTop"
			b GOTOXY
			w $00e1
			b $9e
			b "starten"
			b NULL
:Icon_Text3		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Nicht genügend freier Speicher verfügbar"
			b GOTOXY
			w $0018
			b $60
			b "um alle Dateien zu entpacken!"

			b GOTOXY
			w $0048
			b $76
			b "Installation forsetzen und nicht"
			b GOTOXY
			w $0048
			b $7e
			b "alle Dateien kopieren."

			b GOTOXY
			w $0048
			b $96
			b "Ein anderes Laufwerk für die"
			b GOTOXY
			w $0048
			b $9e
			b "Installation wählen."
			b NULL

:Icon_Text4		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Kopieren Sie jetzt die MegaPatch-Dateien. Ein"
			b GOTOXY
			w $0018
			b $60
			b "'*' symbolisiert benötigte Systemdateien."

			b GOTOXY
			w $0028
			b $70
			b "*"
			b GOTOXY
			w $0020
			b $86
			b "Startdateien"

			b GOTOXY
			w $0078
			b $86
			b "ReBoot-System"

			b GOTOXY
			w $00d8
			b $70
			b "*"
			b GOTOXY
			w $00da
			b $86
			b "Laufwerks-"
			b GOTOXY
			w $00e1
			b $8e
			b "treiber"

			b GOTOXY
			w $0021
			b $ae
			b "Hintergrund-"
			b GOTOXY
			w $0033
			b $b6
			b "Bilder"

			b GOTOXY
			w $007f
			b $ae
			b "Bildschirm-"
			b GOTOXY
			w $0087
			b $b6
			b "schoner"

			b GOTOXY
			w $00d2
			b $ae
			b "Installation"
			b GOTOXY
			w $00d7
			b $b6
			b "fortsetzen"
			b NULL
:Icon_Text5		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Bitte wählen Sie den Modus zum Kopieren der"
			b GOTOXY
			w $0018
			b $60
			b "einzelnen Treiber für Diskettenlaufwerke:"

			b GOTOXY
			w $0048
			b $76
			b "Alle Laufwerkstreiber kopieren"

			b GOTOXY
			w $0048
			b $96
			b "Nur bestimmte Laufwerkstreiber"
			b GOTOXY
			w $0048
			b $9e
			b "für die Installation wählen."
			b NULL

:Icon_Text6		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Soll der folgende Laufwerkstreiber auf der"
			b GOTOXY
			w $0018
			b $60
			b "Startdiskette installiert werden ?"
			b GOTOXY
			w $0020
			b $7c
			b "Laufwerk:"

			b GOTOXY
			w $001d
			b $ae
			b "Kopieren"

			b GOTOXY
			w $006f
			b $ae
			b "Nicht"
			b GOTOXY
			w $0066
			b $b6
			b "Kopieren"

			b GOTOXY
			w $00a2
			b $ae
			b "Installation"
			b GOTOXY
			w $00a7
			b $b6
			b "fortsetzen"

			b GOTOXY
			w $00f8
			b $ae
			b "DeskTop"
			b GOTOXY
			w $00f9
			b $b6
			b "starten"
			b NULL
:Icon_Text7		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Das kopieren der Systemdateien ist beendet."
			b GOTOXY
			w $0018
			b $60
			b "Die Startdiskette wird jetzt auf fehlende"
			b GOTOXY
			w $0018
			b $68
			b "Dateien untersucht."
			b GOTOXY
			w $0020
			b $96
			b "Startdiskette"
			b GOTOXY
			w $0025
			b $9e
			b "überprüfen"

			b GOTOXY
			w $00e1
			b $96
			b "DeskTop"
			b GOTOXY
			w $00e1
			b $9e
			b "starten"
			b NULL

:Icon_Text8		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Die Diskette wurde überprüft und alle Dateien"
			b GOTOXY
			w $0018
			b $60
			b "des MegaPatch sind vorhanden. Sie können die"
			b GOTOXY
			w $0018
			b $68
			b "Installation jetzt fortsetzen."

			b GOTOXY
			w $0021
			b $96
			b "Installation"
			b GOTOXY
			w $26
			b $9e
			b "fortsetzen"
			b GOTOXY
			w $00e1
			b $96
			b "DeskTop"
			b GOTOXY
			w $00e1
			b $9e
			b "starten"
			b NULL

:Icon_Text9		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Es wurden nicht alle Systemdateien auf der"
			b GOTOXY
			w $0018
			b $60
			b "Startdiskette  gefunden.  Die  Installation"
			b GOTOXY
			w $0018
			b $68
			b "kann jedoch fortgesetzt werden."

			b GOTOXY
			w $001e
			b $96
			b "Systemdateien"
			b GOTOXY
			w $002c
			b $9e
			b "kopieren"
			b GOTOXY
			w $007a
			b $96
			b "Installation"
			b GOTOXY
			w $007e
			b $9e
			b "fortsetzen"
			b GOTOXY
			w $00e1
			b $96
			b "DeskTop"
			b GOTOXY
			w $00e1
			b $9e
			b "starten"
			b NULL
:Icon_Text10		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Es wurden  nicht alle  Systemdateien  auf der"
			b GOTOXY
			w $0018
			b $60
			b "Startdiskette gefunden. Die Diskette ist nicht"
			b GOTOXY
			w $0018
			b $68
			b "startfähig. Bitte Systemdateien ergänzen."

			b GOTOXY
			w $0020
			b $96
			b "Systemdateien"
			b GOTOXY
			w $002c
			b $9e
			b "kopieren"

			b GOTOXY
			w $00e1
			b $96
			b "DeskTop"
			b GOTOXY
			w $00e1
			b $9e
			b "starten"
			b NULL

;*** Information über Kopierstatus.
:Inf_Wait		b GOTOXY
			w $0018
			b $5a
			b PLAINTEXT,OUTLINEON
			b "Bitte warten!"
			b GOTOXY
			w $0018
			b $70
			b PLAINTEXT
			b NULL

:Inf_DelSysFiles	b "Systemdateien werden gelöscht...",NULL
:Inf_CopySystem		b "Systemdateien werden kopiert...",NULL
:Inf_CopyRBoot		b "ReBoot-Routine wird kopiert...",NULL
:Inf_CopyBkScr		b "Hintergrundbild wird kopiert...",NULL
:Inf_CopyScrSv		b "Bildschirmschoner werden kopiert...",NULL
:Inf_CopyDskDrv		b "Laufwerkstreiber werden kopiert...",NULL
:Inf_InstallMP		b "Systemdiskette wird untersucht...",NULL
:Inf_ChkDkSpace		b "Zieldiskette wird überprüft...",NULL

:InfoText1		b PLAINTEXT
			b GOTOXY
			w $0020
			b $76
			b "Bitte haben Sie einen kleinen Augenblick"
			b GOTOXY
			w $0020
			b $80
			b "Geduld, während Setup die Startdiskette"
			b GOTOXY
			w $0020
			b $8a
			b "für MegaPatch konfiguriert..."
			b NULL
