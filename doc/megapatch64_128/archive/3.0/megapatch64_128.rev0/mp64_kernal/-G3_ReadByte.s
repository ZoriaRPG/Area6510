﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Byte aus Datensatz einlesen.
:xReadByte		ldy	r5H
			cpy	r5L
			beq	:52
			lda	(r4L),y
			inc	r5H
			ldx	#$00
::51			rts

::52			ldx	#$0b
			lda	r1L
			beq	:51

			jsr	GetBlock
			txa
			bne	:51

			ldy	#$02
			sty	r5H
			dey
			lda	(r4L),y
			sta	r1H
			tax
			dey
			lda	(r4L),y
			sta	r1L
			beq	:53
			ldx	#$ff
::53			inx
			stx	r5L
			jmp	xReadByte
