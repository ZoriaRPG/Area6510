﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
;RAM_Type = RAM_SCPU
;******************************************************************************
;*** Einsprungtabelle RAM-Tools.
:xVerifyRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:xStashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xSwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xFetchRAM		ldy	#%10010001		;RAM-Bereich laden.

:xDoRAMOp		php
			sei
			lda	CPU_DATA		;I/O-Bereich aktivieren.
			pha
			lda	#$35
			sta	CPU_DATA

			cpy	#%10010000		;StashRAM ?
			bne	:51			; => Nein, weiter...
			jsr	StashRAM_SCPU		;StashRAM für SCPU ausführen.
			jmp	:54

::51			cpy	#%10010001		;FetchRAM ?
			bne	:52			; => Nein, weiter...
			jsr	FetchRAM_SCPU		;FetchRAM für SCPU ausführen.
			jmp	:54

::52			cpy	#%10010010		;SwapRAM ?
			bne	:53			; => Nein, weiter...
			jsr	SwapRAM_SCPU		;SwapRAM für SCPU ausführen.
			jmp	:54

::53			cpy	#%10010011		;SwapRAM ?
			bne	:54			; => Nein, weiter...
			jsr	VerifyRAM_SCPU		;SwapRAM für SCPU ausführen.
			txa				;Verify-Error ?
			beq	:54			; => Nein, weiter...
			ldx	#%00100000
			b $2c

::54			ldx	#%01000000		;Kein Fehler...
			pla				;I/O-Bereiche deaktivieren.
			sta	CPU_DATA
			plp

			txa				;Zurück zum Programm.
			ldx	#$00
			rts


;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g MP_JUMPTAB
