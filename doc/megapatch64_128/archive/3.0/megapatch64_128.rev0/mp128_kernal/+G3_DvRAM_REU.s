﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
;RAM_Type = RAM_REU
;******************************************************************************
;*** Einsprungtabelle RAM-Tools.
:xVerifyRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:xStashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xSwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xFetchRAM		ldy	#%10010001		;RAM-Bereich laden.

:xDoRAMOp		php
			sei

			lda	MHZ			;aktuellen Takt zwischenspeichern
			sta	:mhz+1
			LoadB	MHZ,0			;auf 1 Mhz schalten! Sonst geht nichts!
			lda	MMU
			pha
			lda	#$7e
			sta	MMU
			lda	RAM_Conf_Reg
			pha
			lda	#$40			;keine Common Area VIC = Bank 1 für
			sta	RAM_Conf_Reg		;REU Transfer

			ldx	#$03
::1			lda	r0L         ,x
			sta	EXP_BASE1 +2,x
			dex
			bpl	:1

			lda	r3L			;Bank in der REU.
			sta	EXP_BASE1 + 6
			lda	r2L
			sta	EXP_BASE1 + 7
			lda	r2H			;Anzahl Bytes.
			sta	EXP_BASE1 + 8
			lda	#$00
			sta	EXP_BASE1 + 9
			sta	EXP_BASE1 +10
			sty	EXP_BASE1 + 1

::2			lda	EXP_BASE1 + 0		;Job ausführen.
			and	#%01100000
			beq	:2
			tax				;Job-Ergebnis retten.

			pla
			sta	RAM_Conf_Reg
			pla
			sta	MMU

::mhz			lda	#0			;aktuellen Takt zurücksetzen
			sta	MHZ
			txa				;Job-Ergebnis zurücksetzen.
			plp
			ldx	#$00
			rts

			g $9f54
