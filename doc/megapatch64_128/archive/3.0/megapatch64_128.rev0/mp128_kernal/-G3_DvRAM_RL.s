﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
;RAM_Type = RAM_RL
;******************************************************************************
;*** Einsprungtabelle RAM-Tools.
:xVerifyRAM		ldy	#%10010011		;RAM-Bereich vergleichen.
			b $2c
:xStashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xSwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xFetchRAM		ldy	#%10010001		;RAM-Bereich laden.

:xDoRAMOp		php
			sei

			ldx	CPU_DATA
			lda	#$36
			sta	CPU_DATA

			jsr	$e0a9

			sty	EXP_BASE2    + 1

			ldy	#$03
::51			lda	zpage        + 1,y
			sta	EXP_BASE2    + 1,y
			dey
			bne	:51

			sty	EXP_BASE2    +10

			lda	r1H
			clc
			adc	RamBankFirst + 0
			sta	EXP_BASE2    + 5
			lda	r3L
			adc	RamBankFirst + 1
			sta	EXP_BASE2    + 6

			lda	r2L
			sta	EXP_BASE2    + 7
			lda	r2H
			sta	EXP_BASE2    + 8

			jsr	$fe06			;Job ausführen und

			lda	$de00			;Fehlerflag auslesen und
			pha
			jsr	$fe0f			;RL-Hardware abschalten.
			pla
			stx	CPU_DATA		;I/O ausblenden.
			plp
			ldx	#$00			;Kein Fehler.
			rts

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g MP_JUMPTAB
