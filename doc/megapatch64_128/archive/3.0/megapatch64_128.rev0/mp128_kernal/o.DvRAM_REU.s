﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "obj.DvRAM_REU"
			t "G3_SymMacExt"
			t "G3_Data.V.Class"

			o BASE_RAM_DRV

			h "MegaPatch-Kernal"
			h "REU-Funktionen..."

if Flag64_128 = TRUE_C64
			t "-G3_DvRAM_REU"
else
			t "+G3_DvRAM_REU"
