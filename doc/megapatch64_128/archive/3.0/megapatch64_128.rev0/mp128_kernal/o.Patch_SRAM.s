﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "obj.Patch_SRAM"
			t "G3_SymMacExt"
			t "G3_Data.V.Class"

			o $d300

			h "MegaPatch-Kernal"
			h "SuperCPU-Funktionen..."
;******************************************************************************
;RAM_Type = RAM_SCPU
;******************************************************************************
:xStashRAM_SCPU		jmp	DoStash
:xFetchRAM_SCPU		jmp	DoFetch
:xSwapRAM_SCPU		jmp	DoSwap
:xVerifyRAM_SCPU	jmp	DoVerify

;*** StashRAM-Routine (16-Bit NativeCode).
:DoStash		sta	$d07e

			jsr	DefBankAdr		;Speicherbank berechnen.
			sta	:51 +1

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$02			;ldx $0002
			b $a4,$04			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			sta	$d07f
			rts

;*** FetchRAM-Routine (16-Bit NativeCode).
:DoFetch		sta	$d07e

			jsr	DefBankAdr		;Speicherbank berechnen.
			sta	:51 +2

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$04			;ldx $0002
			b $a4,$02			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			sta	$d07f
			rts

;*** SwapRAM-Routine (16-Bit NativeCode).
:DoSwap			PushW	r0			;Register r0/r1 speichern.
			PushW	r1

			sta	$d07e

			jsr	DefBankAdr		;Speicherbank berechnen.
			sta	:52 +3
			sta	:53 +3

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $48				;pha
			b $a6,$04			;ldx r1
::52			b $bf,$00,$00,$00		;lda $??:0000,x
			b $a6,$02			;ldx r0
			b $9f,$00,$00,$00		;sta $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $68				;pla
			b $a6,$04			;ldx r1
::53			b $9f,$00,$00,$00		;sta $??:0000,x
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$db			;bne :51

			b $38				;sec
			b $fb				;xce

			sta	$d07f

			PopW	r1			;Register r0/r1 speichern.
			PopW	r0
			rts
;*** VerifyRAM-Routine (16-Bit NativeCode).
:DoVerify		PushW	r0			;Register r0/r1 speichern.
			PushW	r1
			PushB	r3L

			sta	$d07e

			jsr	DefBankAdr		;Speicherbank berechnen.
			sta	:52 +3

			lda	#$ff
			sta	r3L

			clc
			b $fb				;xce
			b $c2,$10			;rep #$00010000

			b $a0,$00,$00			;ldy #$0000
::51			b $a6,$02			;ldx r0
			b $bf,$00,$00,$00		;lda $00:0000,x
			b $e8				;inx
			b $86,$02			;stx r0
			b $a6,$04			;ldx r1
::52			b $df,$00,$00,$00		;cmp $??:0000,x
			b $d0,$0a			;bne :53
			b $e8				;inx
			b $86,$04			;stx r1

			b $c8				;iny
			b $c4,$06			;cpy r2
			b $d0,$e7			;bne :51
			b $e6,$08			;inc r3L

::53			b $38				;sec
			b $fb				;xce

			sta	$d07f

			ldx	r3L

			PopB	r3L
			PopW	r1			;Register r0/r1 speichern.
			PopW	r0
			rts

;*** Erste Speicherbank berechnen.
:DefBankAdr		lda	r3L			;Bank in SCPU-Bank umrechnen.
			clc
			adc	RamBankFirst +1
			rts

;******************************************************************************
			g $d3ff
;******************************************************************************
