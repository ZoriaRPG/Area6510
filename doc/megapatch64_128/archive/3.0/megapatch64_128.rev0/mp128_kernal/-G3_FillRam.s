﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Speicherbereich mit Byte füllen.
:xFillRam		lda	r0H			;Mehr als 256 Byte füllen ?
			beq	:2			;Nein, weiter...

			lda	r2L
			ldy	#$00
::1			sta	(r1L),y			;256 Byte füllen.
			dey
			bne	:1
			inc	r1H
			dec	r0H
			bne	:1

::2			lda	r2L
			ldy	r0L
			beq	:4
			dey
::3			sta	(r1L),y			;Restbereich füllen.
			dey
			cpy	#$ff
			bne	:3
::4			rts

