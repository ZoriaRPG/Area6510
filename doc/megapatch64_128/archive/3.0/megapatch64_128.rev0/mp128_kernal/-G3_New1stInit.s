﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** GEOS-Variablen löschen.
:xFirstInit		sei
			cld
if Flag64_128 = TRUE_C128
			LoadB	scr80polar,%01000000
endif
			jsr	GEOS_Init1

			lda	#>xEnterDeskTop
			sta	EnterDeskTop   +2
			lda	#<xEnterDeskTop
			sta	EnterDeskTop   +1

			lda	#$7f
			sta	maxMouseSpeed
			sta	mouseAccel
			lda	#$1e
			sta	minMouseSpeed

			jsr	xResetScreen

			ldy	#$3e
			lda	#$00
::2			sta	mousePicData,y
			dey
			bpl	:2

			ldx	#$15
::3			lda	OrgMouseData,x		;Daten für Mauszeiger ab
			sta	mousePicData,x		;":OrgMouseData" kopieren.
			dex
			bpl	:3

;*** Sprite-Zeiger-Kopie setzen.
:DefSprPoi		lda	#$bf
			sta	$8ff0
			ldx	#$07
			lda	#$bb
::1			sta	$8fe8,x
			dex
			bpl	:1

if Flag64_128 = TRUE_C64
			rts
else
			lda	#0
			sta	r0L
			sta	r0H
			jmp	xSetMsePic		;80-Zeichen Mausdaten setzen
endif
