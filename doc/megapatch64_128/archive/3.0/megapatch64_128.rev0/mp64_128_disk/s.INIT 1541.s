﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "mod.MDD_#110"
			t "G3_SymMacExt"
			t "G3_Disk.V.Class"
			t "-DD_JumpTab"
;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		and	#%01000000
			beq	:51
			ldy	#$03
			jmp	GetFreeBankTab

::51			tax
			tay
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			and	#%01000000		;1541-Cache-Laufwerk ?
			beq	:51			; => Nein, weiter...

			jsr	TestDriveMode		;Freien RAM-Speicher testen.
			cpx	#$00			;Ist genügend Speicher frei ?
			bne	:53			; => Nein, Installationsfehler.

			pha				;Cache-Speicher in REU belegen.
			ldy	#$03
			jsr	AllocateBankTab
			pla
			cpx	#$00			;Speicher reserviert ?
			bne	:53			; => Nein, Installationsfehler.

			ldx	DriveAdr		;Startadresse Cache-Speicher in
			sta	ramBase   -8,x		;REU zwischenspeichern.

;--- Angeschlossenes Laufwerk testen.
::51			lda	DriveAdr		;Aktuelles Laufwerk feststellen.
			jsr	TestDriveType
			cpx	#$00			;Laufwerk erkannt ?
			bne	:52			; => Nein, weiter...
			cpy	#Drv1541		;1541-Laufwerk erkannt ?
			beq	:54			; => Ja, weiter...

;--- 1541-Treiber darf nur 1541 einrichten, sonst kommt es beim booten zu
;    Installationsproblemen wenn eine 1541 und eine 1541 installiert wird!
			cpy	#Drv1571		;1571-Laufwerk erkannt ?
			beq	:54			; => Ja, weiter...

;--- Kompatibles Laufwerk suchen.
::52			lda	#Drv1541
			ldy	DriveAdr
			jsr	FindDrive		;1541-Laufwerk suchen.
			cpx	#$00			;Laufwerk gefunden ?
			beq	:54			; => Ja, weiter...

			lda	#Drv1571
			jsr	FindDrive		;1571-Laufwerk/41-Modus suchen.
			cpx	#$00			;Laufwerk gefunden ?
			beq	:54			; => Ja, weiter...

			lda	DriveAdr
			jsr	TurnOnNewDrive		;Dialogbox ausgeben.
			txa				;Lauafwerk eingeschaltet ?
			beq	:51			; =: Ja, Laufwerk suchen...

;--- Kein passendes Laufwerk gefunden.
::53			ldx	#DEV_NOT_FOUND
			rts

;--- Laufwerk installieren.
::54			ldx	DriveAdr		;Laufwerksdaten setzen.
			stx	curDrive
			lda	DriveMode
			sta	driveType   -8,x
			sta	curType
			sta	RealDrvType -8,x
			sta	BASE_DDRV_DATA + (DiskDrvType - DISK_BASE)
			lda	#$00
			sta	RealDrvMode -8,x

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob


;--- Cache-Speicher löschen.
			jsr	InitShadowRAM

;--- Ende, kein Fehler...
			ldx	#$00
			rts
;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	lda	driveType   -8,x
			and	#%01000000		;Cache-Laufwerk installiert ?
			beq	:51			; => Nein, weiter...

			txa				;Cache-Speicher in der REU wieder
			pha				;freigeben.
			lda	ramBase     -8,x
			ldx	#$03
			jsr	FreeBankTab
			pla
			tax

::51			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** ShadowRAM initialisieren.
:InitShadowRAM		bit	DriveMode		;Shaow1541 ?
			bvc	:53			;Ja, weiter...

::51			lda	#>InitWordData		;Zeiger auf Initialisierungswert
			sta	r0H			;für Sektortabelle (2x NULL-Byte!)
			lda	#<InitWordData
			sta	r0L

			ldy	#$00			;Offset in 64K-Bank.
			sty	r1L
			sty	r1H
			sty	r2H			;Anzahl Bytes.
			iny
			iny
			sty	r2L

			iny				;Bank-Zähler initialisieren.
			sty	r3H

			ldy	DriveAdr		;Zeiger auf erste Bank für
			lda	ramBase -8,y		;Shadow1541-Laufwerk richten.
			sta	r3L

::52			jsr	StashRAM		;Sektor "Nicht gespeichert" setzen.
			inc	r1H			;Zeiger auf nächsten Sektor in Bank.
			bne	:52			;Schleife.

			inc	r3L			;Zeiger auf nächste Bank.
			dec	r3H			;Alle Bänke initialisiert ?
			bne	:52			;Nein, weiter...
::53			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000
:InitWordData		w $0000

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
