﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = RL_NM!FD_NM!HD_NM!HD_NM_PP
if :tmp0 = TRUE
;******************************************************************************
;*** Diskettennamen definieren.
;    Übergabe:		r4	= Zeiger auf dir2Head.
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:SwapDskNamData		php
			sei

			lda	r1L
			cmp	DirHead_Tr
			bne	:52
			lda	r1H
			cmp	DirHead_Se
			bne	:52

			txa
			pha
			PushW	r2

			LoadB	r2L,$04			;Diskettename korrigieren.
			LoadB	r2H,$90

::51			ldy	r2L
			lda	(r4L),y
			pha

			ldy	r2H
			lda	(r4L),y

			ldy	r2L
			sta	(r4L),y

			pla
			ldy	r2H
			sta	(r4L),y

			inc	r2L
			inc	r2H
			iny
			cpy	#$ab
			bne	:51

			ldy	#$00

			PopW	r2
			pla
			tax

::52			plp
			rts
endif
;******************************************************************************
::tmp1 = RL_81!C_81!FD_81!HD_81!HD_81_PP
if :tmp1 = TRUE
;******************************************************************************
;*** Diskettennamen definieren.
;    Übergabe:		r4	= Zeiger auf dir2Head.
;    Rückgabe:		-
;    Geändert:		AKKU,yReg
:SwapDskNamData		php
			sei

			lda	r1L
			cmp	#Tr_DskNameSek
			bne	:52
			lda	r1H
			cmp	#Se_DskNameSek
			bne	:52

			txa
			pha
			PushW	r2

			LoadB	r2L,$04			;Diskettename korrigieren.
			LoadB	r2H,$90

::51			ldy	r2L
			lda	(r4L),y
			pha

			ldy	r2H
			lda	(r4L),y

			ldy	r2L
			sta	(r4L),y

			pla
			ldy	r2H
			sta	(r4L),y

			inc	r2L
			inc	r2H
			iny
			cpy	#$ab
			bne	:51

			ldy	#$00

			PopW	r2
			pla
			tax

::52			plp
			rts
endif
