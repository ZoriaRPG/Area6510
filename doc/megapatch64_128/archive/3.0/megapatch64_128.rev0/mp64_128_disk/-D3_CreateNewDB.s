﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = FD_NM!HD_NM!HD_NM_PP!RL_NM!RD_NM!RD_NM_SCPU
if :tmp0 = TRUE
;******************************************************************************
;*** Neuen Verzeichnis-Sektor erstellen.
;    Übergabe:		r1 = Aktueller Verzeichnis-Track/Sektor.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r3,r4,r7,r8H
:xCreateNewDirBlk	PushW	r6			;Register ":r6" zwischenspeichern.

			lda	r1H			;Aktuellen Verzeichnis-Sektor als
			sta	r3H			;Startwert für Suche nach freien
			lda	r1L			;Verzeichnis-Sektor setzen.
			sta	r3L
			jsr	SetNextFreeAll		;Freien Sektor suchen.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			lda	r3H			;Freien Sektor als LinkBytes in
			sta	diskBlkBuf +$01		;aktuellem Verzeichnis-Sektor
			lda	r3L			;eintragen.
			sta	diskBlkBuf +$00
			jsr	xPutBlock_dskBuf	;Sektor auf Diskette speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			MoveB	r3L,r1L			;Zeiger auf aktuellen Sektor.
			MoveB	r3H,r1H
			jsr	Clr_diskBlkBuf		;Sektor-Speicher löschen.

::51			PopW	r6			;Register ":r6" zurücksetzen.
::52			rts
endif
;******************************************************************************
::tmp1b = C_41!C_71!C_81!FD_41!FD_71!FD_81!HD_41!HD_71!HD_81
::tmp1a = RL_41!RL_71!RL_81!RD_41!RD_71!RD_81
::tmp1c = HD_41_PP!HD_71_PP!HD_81_PP
::tmp1  = :tmp1a!:tmp1b!:tmp1c
if :tmp1 = TRUE
;******************************************************************************
;*** Neuen Verzeichnis-Sektor erstellen.
;    Übergabe:		r1 = Aktueller Verzeichnis-Track/Sektor.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r3,r4,r7,r8H
:xCreateNewDirBlk	jsr	IsDirSekFree		;Freien Verzeichnis-Sektor suchen.
			txa				;Ist Sektor frei ?
			bne	:52			; => Abbruch wenn kein Sektor frei.

			PushW	r6			;Register ":r6" zwischenspeichern.

			lda	r1H			;Aktuellen Verzeichnis-Sektor als
			sta	r3H			;Startwert für Suche nach freien
			lda	r1L			;Verzeichnis-Sektor setzen.
			sta	r3L
			jsr	xSetNextFree		;Freien Sektor suchen.

			lda	r3H			;Freien Sektor als LinkBytes in
			sta	diskBlkBuf +$01		;aktuellem Verzeichnis-Sektor
			lda	r3L			;eintragen.
			sta	diskBlkBuf +$00
			jsr	xPutBlock_dskBuf	;Sektor auf Diskette speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			MoveB	r3L,r1L			;Zeiger auf aktuellen Sektor.
			MoveB	r3H,r1H
			jsr	Clr_diskBlkBuf		;Sektor-Speicher löschen.

::51			PopW	r6			;Register ":r6" zurücksetzen.
::52			rts
endif
