﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "mod.MDD_#126"
			t "G3_SymMacExt"
			t "G3_Disk.V.Class"
			t "-DD_JumpTab"
;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		ldy	#$01			;13x64K für RAM1581.
			jmp	GetFreeBankTab

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			lda	DriveAdr
			clc
			adc	#$39
			sta	BAM_NM +7
			sta	DRIVE_NAME +3

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- Größe der aktiven Partition einlesen.
			jsr	GetCurPartSize

;--- RAM reservieren.
			jsr	GetPartSize		;Partitionsgröße ermitteln.
			cpx	#$00			;Abbruch ?
			bne	:51			; => Ja, Ende...

			ldx	DriveAdr
			ldy	DskSizeA  -8,x
			beq	:51
			jsr	GetFreeBankTab		;Freien RAM-Speicher testen.
			cpx	#$00			;Ist genügend Speicher frei ?
			beq	:52			; => Ja, weiter...
			bne	:51a

;--- Laufwerk kann nicht installliert werden.
::51			ldx	#DEV_NOT_FOUND
::51a			rts

;--- Installation fortsetzen.
::52			pha				;RAM-Speicher in REU belegen.
			ldx	DriveAdr
			ldy	DskSizeA  -8,x
			jsr	AllocateBankTab
			pla
			cpx	#$00			;Speicher reserviert ?
			bne	:51			; => Nein, Installationsfehler.

;--- Variablen definieren.
			ldx	DriveAdr		;Startadresse RAM-Speicher in
			sta	ramBase   -8,x		;REU zwischenspeichern.
			lda	DriveMode
			sta	driveType   -8,x
			sta	curType
			sta	RealDrvType -8,x
			lda	#SET_MODE_SUBDIR!SET_MODE_FASTDISK
			sta	RealDrvMode-8,x

;--- BAM erstellen.
			jsr	CreateBAM		;BAM erstellen.
			txa				;Installationsfehler ?
			bne	:54			; => Ja, Abbruch...

			lda	DriveAdr		;Laufwerk aktivieren.
			jsr	SetDevice
			jsr	OpenDisk
			txa
			bne	:54

			ldx	#r0L
			jsr	GetPtrCurDkNm

			ldy	#$00
			lda	(r0L),y			;Diskettenname gültig ?
			bne	:55			; => Ja, weiter...

			ldy	#$00			;BAM Teil #1 definieren.
::53			lda	BAM_NM    ,y
			sta	curDirHead,y
			iny
			cpy	#$c0
			bcc	:53

			jsr	PutDirHead
			txa
			beq	:55

;--- Fehler beim erstellen der BAM, Laufwerk nicht installiert.
::54			ldx	DriveAdr
			ldy	DskSizeA  -8,x
			jsr	FreeBankTab
			jsr	DeInstallDrvData
			ldx	#DEV_NOT_FOUND
			rts

;--- Laufwerk installiert, Ende...
::55			ldx	#NO_ERROR
			rts

:Low			b $00
:High			b $00
;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	DeInstallDrvData	; => Ja, Laufwerk initialisieren.

			inx
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf

			ldx	curDrive
			lda	ramBase     -8,x	;RAM-Speicher in der REU wieder
			ldy	diskBlkBuf  +8		;freigeben.
			jsr	FreeBankTab

:DeInstallDrvData	ldx	curDrive
			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts
;*** Partitionsgröße einstellen.
:GetPartSize		jsr	GetMaxSize
			tya
			bne	:52
::51			ldx	#DEV_NOT_FOUND
			rts

::52			ldy	DriveAdr		;Startadresse RAM-Speicher in
			ldx	DskSizeA  -8,y		;Größe bereits festgelegt ?
			bne	:54			; => Ja, weiter.
::53			sta	DskSizeA  -8,y		;Max. Größe vorbelegen.

::54			cmp	DskSizeA  -8,y		;Max. Größe überschritten ?
			bcs	:54a			; => Nein, weiter...
			sta	DskSizeA  -8,y		; => Ja, Größe zurücksetzen...

::54a			bit	firstBoot		;GEOS-BootUp ?
			bmi	:55			; => Nein, weiter...

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			ldy	DskSizeA  -8,x		;Größe bereits festgelegt ?
			jsr	GetFreeBankTab		;Laufwerksgröße testen.
			cpx	#$00			;Ist Laufwerk möglich ?
			beq	:57			; => Ja, weiter...

;--- Partitionsgröße einstellen.
::55			LoadW	r0,Dlg_GetSize		;Partitionsgröße wählen.
			jsr	DoDlgBox

			lda	sysDBData
			cmp	#CANCEL			;Auswahl abgebrochen ?
			beq	:51			; => Ja, Ende...

::56			lda	DriveMode
			ldx	#< DSK_INIT_SIZE
			stx	r2L
			ldx	#> DSK_INIT_SIZE
			stx	r2H
			jsr	SaveDskDrvData
			lda	DriveAdr		;Laufwerk zurücksetzen.
			jsr	SetDevice
::57			ldx	#NO_ERROR
			rts

;*** Größe der aktiven Partition einlesen.
:GetCurPartSize		bit	firstBoot
			bmi	:51

			ldy	DriveAdr		;Startadresse RAM-Speicher in
			lda	DskSizeA   -8,y		;Partitionsgröße unverändert ?
			bne	:51

			jsr	CheckDiskBAM
			txa
			bne	:51

			jsr	GetSekPartSize
			txa
			bne	:51

			tya
			ldy	DriveAdr		;Partitionsgröße speichern.
			sta	DskSizeA   -8,y
::51			rts
;*** Max. freien Speicher ermitteln.
:GetMaxSize		ldy	ramExpSize		;Max. Größe für Laufwerk
			sty	r2L			;ermitteln.

::52			ldy	r2L
			beq	:53
			jsr	GetFreeBankTab
			cpx	#$00
			beq	:54
			dec	r2L
			jmp	:52

;--- Kein Speicher frei.
::53			ldy	#$00
			rts

;--- Freien Speicher gefunden.
::54			ldy	r2L
			rts

;*** Mehr RAM.
:Add64K			php
			sei
			lda	#$50
			jsr	SetIconArea
			jsr	IsMseInRegion
			plp
			tax
			beq	Sub64K

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			ldy	DskSizeA  -8,x		;Größe bereits festgelegt ?
			iny
			sty	r2L
			jsr	GetFreeBankTab
			cpx	#$00
			beq	:51
			jsr	GetMaxSize
			tya
			bne	:52
			ldx	#NO_FREE_RAM
			rts

::51			lda	r2L
::52			ldx	DriveAdr
			sta	DskSizeA  -8,x

			lda	#$50
			jsr	PrntNewSize

			bit	mouseData
			bpl	Add64K
			ClrB	pressFlag
			rts

;*** Weniger RAM.
:Sub64K			php
			sei
			lda	#$58
			jsr	SetIconArea
			jsr	IsMseInRegion
			plp
			tax
			beq	:54

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			ldy	DskSizeA  -8,x		;Größe bereits festgelegt ?
			dey
			beq	:51
			sty	r2L
			jsr	GetFreeBankTab
			cpx	#$00
			beq	:52
			jsr	GetMaxSize
			tya
			bne	:53
::51			ldx	#NO_FREE_RAM
			rts

::52			lda	r2L
::53			ldx	DriveAdr
			sta	DskSizeA  -8,x

			lda	#$58
			jsr	PrntNewSize

			bit	mouseData
			bpl	Sub64K
			ClrB	pressFlag
::54			rts

:SetIconArea		sta	r2L
			clc
			adc	#$07
			sta	r2H
			LoadW	r3 ,($12*8   ) ! DOUBLE_W
			LoadW	r4 ,($12*8+15) ! DOUBLE_W ! ADD1_W
			rts

;*** Neue Partitionsgröße ausgeben.
:PrntNewSize		pha
			jsr	SetIconArea
			jsr	InvertRectangle
			jsr	PrntCurSize
			jsr	SCPU_Pause
			jsr	SCPU_Pause
			pla
			jsr	SetIconArea
			jmp	InvertRectangle

;*** Partitionsgröße ausgeben.
:PrntCurSize		LoadW	r0,Dlg_Graphics2
			jsr	GraphicsString

			ldx	DriveAdr
			lda	DskSizeA  -8,x		;Anzahl Tracks in freien
			sta	r0L			;Speicher umrechnen.
			lda	#$40
			sta	r1L
			ldx	#r0L
			ldy	#r1L
			jsr	BBMult

			SubVW	16,r0			;16K für Verzeichnis abziehen.

			LoadB	r1H,$5b
			LoadW	r11,$0063 ! DOUBLE_W
			lda	#%11000000
			jmp	PutDecimal
;*** Ist BAM und Partitionsgröße gültig ?
:TestCurBAM		jsr	CheckDiskBAM
			txa
			bne	:51
			jsr	GetSekPartSize
			txa
			bne	:51

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			tya
			cmp	DskSizeA   -8,x		;Partitionsgröße unverändert ?
			bne	:51

			ldx	#NO_ERROR
			rts
::51			ldx	#BAD_BAM
			rts

;*** Ist BAM gültige BAM vorhanden ?
:CheckDiskBAM		jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:52			; => Ja, Laufwerk initialisieren.

			LoadW	r0,curDirHead +$ad
			LoadW	r1,DrvFormatCode
			ldx	#r0L
			ldy	#r1L			;Auf GEOS-Kennung
			lda	#12			;"GEOS-format" testen.
			jsr	CmpFString		;Kennung vorhanden ?
			bne	:52			; => Nein, Directory löschen.

::51			ldx	#NO_ERROR
			rts
::52			ldx	#BAD_BAM
			rts

;*** Sektor mit Partitionsgröße einlesen.
:GetSekPartSize		ldx	#$01
			stx	r1L
			inx
			stx	r1H
			jsr	GetBlock_dskBuf
			txa
			bne	:51

			ldy	diskBlkBuf +8
::51			rts

;*** Sektorspeicher löschen.
:ClrDiskSekBuf		ldy	#$00
			tya
::51			sta	diskBlkBuf,y
			dey
			bne	:51
			rts

;*** Routine zum schreiben von Sektoren.
:WriteSektor		PushW	r1

			dec	r1L
			lda	r1H
			clc
			adc	#$00
			sta	r1H
			lda	r1L
			ldx	curDrive
			adc	ramBase -8,x
			sta	r3L
			lda	#$00
			sta	r1L

			LoadW	r0,diskBlkBuf
			LoadW	r2,$0100
			jsr	StashRAM

			PopW	r1
			ldx	#NO_ERROR
			rts
;*** Neue BAM erstellen.
:ClearCurBAM		ldx	DriveAdr
			lda	DskSizeA  -8,x		;Partitionsgröße festlegen.
			sta	BAM_NMa   +8

			ldy	#$00			;BAM Teil #1 definieren.
::51			lda	BAM_NM    ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$c0
			bcc	:51

			lda	#$00
::52			sta	diskBlkBuf,y
			iny
			bne	:52

			lda	#$01			;BAM Teil #1 speichern.
			sta	r1L
			sta	r1H
			jsr	WriteSektor
			txa
			bne	:58

			ldy	#$00			;BAM Teil #1 definieren.
::53			lda	BAM_NMa   ,y
			sta	diskBlkBuf,y
			iny
			cpy	#$40
			bcc	:53

			lda	#$ff
::54			sta	diskBlkBuf,y
			iny
			bne	:54

			inc	r1H			;BAM Teil #2 speichern.
			jsr	WriteSektor
			txa
			bne	:58

			ldy	#$00			;BAM Teil #3 definieren.
			lda	#$ff
::55			sta	diskBlkBuf,y
			iny
			bne	:55

::56			inc	r1H
			CmpBI	r1H,$22
			bcs	:57

			jsr	WriteSektor
			txa
			bne	:58
			beq	:56

;--- Ersten Verzeichnissektor löschen.
::57			jsr	OpenDisk		;BAM einlesen und Größe der Native-
			txa				;Partition innerhalb des Treibers
			bne	:58			;festlegen.

			jsr	ClrDiskSekBuf		;Sektorspeicher löschen.

			lda	#$ff			;Ersten Verzeichnissektor löschen.
			sta	diskBlkBuf +$01
			lda	#$01
			sta	r1L
			lda	#$22
			sta	r1H
			jsr	WriteSektor
			txa
			bne	:58

			lda	#$ff			;Sektor $28/$27 löschen.
			sta	r1H			;Ist Borderblock für DeskTop 2.0!
			jsr	WriteSektor
::58			rts
;*** BAM für RAMNative-Laufwerk.
:BAM_NM			b $01,$22,$48,$00,$52,$41,$4d,$20
			b $4e,$61,$74,$69,$76,$65,$a0,$a0
			b $a0,$a0,$a0,$a0,$a0,$a0,$52,$4c
			b $a0,$31,$48,$a0,$a0,$00,$00,$00
			b $01,$01,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
:DRIVE_NAME		b $52,$41,$4d,$20,$4e,$61,$74,$69
			b $76,$65,$a0,$a0,$a0,$a0,$a0,$a0
			b $a0,$a0,$52,$4c,$a0,$31,$48,$a0
			b $a0,$a0,$a0,$01,$ff,$47,$45,$4f;$01/$ff Borderblock!
			b $53,$20,$66,$6f,$72,$6d,$61,$74
			b $20,$56,$31,$2e,$30,$00,$00,$00

:BAM_NMa		b $00,$00,$48,$b7,$52,$44,$c0,$00
			b $02,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$00,$00,$00,$00
			b $00,$00,$00,$00,$1f,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$fe

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000
:DrvFormatCode		b "GEOS format "
:DskSizeA		b $00
:DskSizeB		b $00
:DskSizeC		b $00
:DskSizeD		b $00

;*** Titelzeile für Dialogbox.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAMNative",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAMNative",NULL
endif

;*** Dialogbox.
:Dlg_GetSize		b %10000001
			b DBGRPHSTR
			w Dlg_Graphics1
			b DBTXTSTR   ,$10,$0b
			w :52
			b DBTXTSTR   ,$10,$20
			w :53
			b DBTXTSTR   ,$68,$3b
			w :54
			b DB_USR_ROUT
			w PrntCurSize
			b DBOPVEC
			w Add64K
			b DB_USR_ROUT
			w Dlg_DrawIcons
			b OK         ,$02,$48
			b CANCEL     ,$10,$48
			b NULL

if Sprache = Deutsch
::52			b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAMNative",NULL
::53			b PLAINTEXT,BOLDON
			b "Partitionsgröße wählen:",NULL
::54			b "KBytes",NULL
endif

if Sprache = Englisch
::52			b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAMNative",NULL
::53			b PLAINTEXT,BOLDON
			b "Select partition-size:",NULL
::54			b "KBytes",NULL
endif

:Dlg_DrawIcons		jsr	i_BitmapUp
			w	Icon01
			b	$12 ! DOUBLE_B
			b	$50
			b	Icon01x ! DOUBLE_B
			b	Icon01y

			lda	C_DBoxDIcon
			jsr	i_UserColor
			b	$12 ! DOUBLE_B
			b	$50/8
			b	Icon01x ! DOUBLE_B
			b	Icon01y/8
			rts

:Dlg_Graphics1		b NEWPATTERN,$01
			b MOVEPENTO
			w $0040 ! DOUBLE_W
			b $20
			b RECTANGLETO
			w $00ff ! DOUBLE_W
			b $2f
			b MOVEPENTO
			w $0060 ! DOUBLE_W
			b $50
			b FRAME_RECTO
			w $008f ! DOUBLE_W
			b $5f

:Dlg_Graphics2		b NEWPATTERN,$00
			b MOVEPENTO
			w $0061 ! DOUBLE_W
			b $51
			b RECTANGLETO
			w $008e ! DOUBLE_W
			b $5e
			b NULL

;******************************************************************************
			t "-DD_AskClrBAM"
;******************************************************************************
;*** Icons.
:Icon01
:Icon01x		= .x
:Icon01y		= .y

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
