﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Zufallszahl berechnen.
:xGetRandom		inc	random+0
			bne	:1
			inc	random+1
::1			asl	random+0
			rol	random+1
			bcc	:3
			lda	#$0e
			adc	random+0
			sta	random+0
			bcc	:2
			inc	random+1
::2			rts

::3			lda	random+1
			cmp	#$ff
			bcc	:4
			lda	random+0
			sbc	#$f1
			bcc	:4
			sta	random+0
			lda	#$00
			sta	random+1
::4			rts
