﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Serien-Nummer des GEOS-Systems.
;    Wird nur dann benötigt wenn eine bootfähige MP-Version erstellt werden
;    soll. Wird MP3 über das Update installiert muß die ID nicht geändert
;    werden. GEOS-ID befindet sich in "src.GEOS3_64" bzw. "src.GEOS3_128"

if Flag64_128 = TRUE_C64
; 64er GEOS-ID's
			w $0064
else
; 128er GEOS-ID's
			w $0128
