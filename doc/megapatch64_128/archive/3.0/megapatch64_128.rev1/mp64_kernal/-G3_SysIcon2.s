﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Daten für "NO"-Icon.
if Sprache = Deutsch
.Icon_NO
endif

if Sprache = Englisch
.Icon_NO
endif

;*** Daten für "YES"-Icon.
if Sprache = Deutsch
.Icon_YES
endif

if Sprache = Englisch
.Icon_YES
endif

;*** Daten für "OPEN"-Icon.
if Sprache = Deutsch
.Icon_OPEN
endif

if Sprache = Englisch
.Icon_OPEN
endif

;*** Daten für "DISK"-Icon.
.Icon_DISK

;*** Daten für Laufwerk-Icons.
.Icon_DRIVE_A

.Icon_DRIVE_B

.Icon_DRIVE_C

.Icon_DRIVE_D
