﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** GEOS-ReBoot-Routine.
:GEOS_ReBootSys		sei
			cld
			ldx	#$ff
			txs

			PushW	$0314			;Zeiger auf IRQ-Routine retten

			LoadB	CPU_DATA,$30
			LoadB	RAM_Conf_Reg,$40	;keine Common Area    VIC = Bank 1
			LoadB	MMU,$7e			;nur RAM Bank 1 + IO

			LoadW	r0,DISK_BASE
			LoadW	r1,$8300
			LoadW	r2,$0d80
			jsr	SysFetchRAM		;Laufwerkstreiber einlesen.

			LoadW	r0,$9d80
			LoadW	r1,$b900
			LoadW	r2,$0280
			jsr	SysFetchRAM		;Kernal Teil #1 einlesen.

			LoadW	r0,$bf40
			LoadW	r1,$bb80
			LoadW	r2,$00c0
			jsr	SysFetchRAM		;Kernal Teil #2 einlesen.

			LoadW	r0,$c100
			LoadW	r1,$bd40
			LoadW	r2,$0f00
			jsr	SysFetchRAM		;Kernal Teil #3 einlesen.

			LoadW	r0,$1000
			LoadW	r1,$cc40
			LoadW	r2,$3000
			jsr	SysFetchRAM		;Kernal Teil #4 einlesen.

			LoadW	r0 ,$1000
			LoadW	r1 ,$d000

			LoadB	MMU,$7f			;nur RAM Bank 1

			ldy	#$00
::4			lda	(r0L),y			;Daten nach
			sta	(r1L),y			;$d000 (Bank 1)
			iny	 			;bis $feff verschieben
			bne	:4
			inc	r0H
			inc	r1H
			lda	r1H
			cmp	#$ff
			bne	:4

			ldy	#5			;Bereich $ff05 bis $ffff
::3			lda	(r0L),y			;setzen in Bank 1
			sta	(r1L),y
			iny
			bne	:3

			LoadB	MMU,$7e			;nur RAM Bank 1 + IO

			LoadW	r0,$1000
			LoadW	r1,$3900
			LoadW	r2,$3000
			jsr	SysFetchRAM		;Kernal Teil #5 (Bank 0) einlesen.

			LoadW	r0 ,$1000
			LoadW	r1 ,$c000		;$c000 - $efff in Bank 0 setzen

			LoadB	RAM_Conf_Reg,$4b	;16kByte Common Area oben = Bank 0
			LoadB	MMU,$7f			;nur RAM Bank 1

			ldy	#$00
::4a			lda	(r0L),y			;Daten nach
			sta	(r1L),y			;$c000 (Bank 0)
			iny	 			;bis $efff verschieben
			bne	:4a
			inc	r0H
			inc	r1H
			lda	r1H
			cmp	#$f0			;Ende $f000 erreicht?
			bne	:4a			;>nein

			LoadB	RAM_Conf_Reg,$40	;keine Common Area    VIC = Bank 1
			LoadB	MMU,$7e			;nur RAM Bank 1 + IO

			LoadW	r0,$1000
			LoadW	r1,$6900
			LoadW	r2,$1000
			jsr	SysFetchRAM		;Kernal Teil #6 (Bank 0) einlesen.

			LoadW	r0 ,$1000
			LoadW	r1 ,$f000		;$f000 - $ffff in Bank 0 setzen

			LoadB	RAM_Conf_Reg,$4b	;16kByte Common Area oben = Bank 0
			LoadB	MMU,$7f			;nur RAM Bank 1

			ldy	#$00
::4b			lda	(r0L),y			;Daten nach
			sta	(r1L),y			;$f000 (Bank 0)
			iny	 			;bis $feff verschieben
			bne	:4b
			inc	r0H
			inc	r1H
			lda	r1H
			cmp	#$ff
			bne	:4b

			ldy	#5			;Bereich $ff05 bis $ffff
::3a			lda	(r0L),y			;setzen in Bank 0
			sta	(r1L),y
			iny
			bne	:3a

			LoadB	MMU,$7e			;nur RAM Bank 1 + IO
			LoadB	RAM_Conf_Reg,$40	;keine Common Area    VIC = Bank 1
;*** Variablenspeicher löschen.
			jsr	i_FillRam
			w	$0500
			w	$8400
			b	$00

;*** GEOS initiailisieren.
			LoadB	dispBufferOn,ST_WR_FORE

			lda	Mode_Conf_Reg		;40(1)/80(2) Zeichen Modus
			and	#$80			;Flag maskieren und umdrehen
			eor	#$80			;umdrehen und in graphMode
			sta	graphMode		;speichern 40($00)/80($80)
			bmi	:80

			LoadW	r0,$a000
			ldx	#$7d
::1			ldy	#$3f
::2			lda	#$55
			sta	(r0L),y
			dey
			lda	#$aa
			sta	(r0L),y
			dey
			bpl	:2
			lda	r0L
			clc
			adc	#$40
			sta	r0L
			bcc	:7
			inc	r0H
::7			dex
			bne	:1
			beq	:weiter

::80			lda	#$02
			jsr	SetPattern
			jsr	i_Rectangle
			b	$00,$c7
			w	$0000,$027f

::weiter		jsr	FirstInit
			jsr	SCPU_OptOn		;SCPU optimieren.
			lda	#$ff
			sta	firstBoot

			jsr	InitMouse		;Mausabfrage initialisieren.

;*** GEOS-Variablen aus REU einlesen.
			LoadW	r0,ramExpSize		;GEOS-Variablen aus REU in
			LoadW	r1,$7dc3		;C64-RAM kopieren.
			LoadW	r2,$0002
			jsr	SysFetchRAM

			lda	sysFlgCopy		;System-Flag speichern.
			sta	sysRAMFlg

			LoadW	r0,year			;Datum aus REU einlesen.
			LoadW	r1,$7a16
			LoadW	r2,$0003
			lda	#$00
			sta	r3L
			jsr	FetchRAM

			lda	$dc08
			sta	$dc08

			LoadW	r0,driveType		;Laufwerkstypen aus REU einlesen.
			LoadW	r1,$798e
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,ramBase		;RAM-Adressen aus REU einlesen.
			LoadW	r1,$7dc7
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,driveData		;Laufwersdaten aus REU einlesen.
			LoadW	r1,$7dbf
			LoadW	r2,$0004
			jsr	FetchRAM

			LoadW	r0,PrntFileName		;Name des Druckertreibes aus
			LoadW	r1,$7965		;REU einlesen.
			LoadW	r2,$0011
			jsr	FetchRAM

			LoadW	r0,inputDevName		;Name des Eingabetreibes aus
			LoadW	r1,$7dcb		;REU einlesen.
			LoadW	r2,$0011
			jsr	FetchRAM

			LoadW	r0,curDrive		;Aktuelles Laufwerk aus
			LoadW	r1,$7989		;REU einlesen.
			LoadW	r2,$0001
			jsr	FetchRAM

			LoadW	r0,$8a00		;Mauszeiger aus.
			LoadW	r1,$fc40		;REU einlesen.
			LoadW	r2,$003f
			jsr	FetchRAM

			LoadW	r0,mousePicData		;Mauszeiger aus.
			LoadW	r1,$fc40		;REU einlesen.
			LoadW	r2,$003f
			jsr	FetchRAM
;*** Warteschleife.
;    Dabei wird zuerst der I/O-Bereich eingeblendet und anschließend die
;    Original-IRQ-Routine aktiviert. Diese Routine ist beim C64 zwingend
;    notwendig. Fehlt diese Routine ist ohne ein Laufwerk wie z.B. C=1541
;    ein Start über RBOOT nicht möglich (Fehlerhaftes IRQ-verhalten!)
;    Ist kein Gerät am ser. Bus aktiviert, kann GEOS ohne diese Routine
;    nicht gestartet werden!!!
:Wait			jsr	InitForIO		;I/O-Bereich einblenden.

			PopW	$0314			;Zeiger auf IRQ-Routine setzen.
			cli				;IRQ aktivieren und warten bis
			lda	$dc08			;IRQ ausgeführt wurde...
::51			cmp	$dc08
			beq	:51

			jsr	DoneWithIO		;I/O-Bereich ausblenden.

;*** Laufwerke aktivieren.
:InstallDrives		lda	curDrive		;Aktuelles Laufwerk merken.
			pha

			lda	#$00			;Anzahl Laufwerke löschen.
			sta	numDrives

			ldy	#8			;Zeiger auf Laufwerk #8.
::51			sty	:52 +1
			lda	driveType -8,y		;Laufwerk verfügbar ?
			beq	:53			;Nein, weiter...

			inc	numDrives		;Anzahl Laufwerke +1.
			lda	:52 +1
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	NewDisk			;Diskette öffnen.

::52			ldy	#$ff
::53			iny				;Zeiger auf nächstes Laufwerk.
			cpy	#12			;Alle Laufwerke getestet ?
			bcc	:51			;Nein, weiter...

			pla
			jsr	SetDevice		;Laufwerk zurücksetzen.
			jmp	EnterDeskTop		;Zurück zum DeskTop.
;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_SCPU
:SysFetchRAM		php
			sei
			PushB	MMU
			LoadB	MMU,$7e

			lda	r3L			;Bank in SCPU-Bank umrechnen.
			clc
			adc	RamBankFirst +1		;Ersatz für RamBankFirst.
			sta	:51 +2

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a4,$02			;ldy $0002
			b $a6,$04			;ldx $0004
			b $8b				;phb
::51			b $54,$00,$02			;mvn $00.$02
			b $ab				;plb
			b $38				;sec
			b $fb				;xce

			PopB	MMU
			plp
			rts
endif
;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_RL
:SysFetchRAM		php
			sei
			PushB	MMU
			LoadB	MMU,$4e
			PushB	RAM_Conf_Reg
			and	#$f0
			ora	#$04
			sta	RAM_Conf_Reg

			jsr	$e0a9

			ldx	#$04
::51			lda	zpage +1,x
			sta	$de01   ,x
			dex
			bne	:51

			ldx	#$00			;Bank für Transfer.
			stx	$de06

			lda	r2L			;Anzahl Bytes.
			sta	$de07
			lda	r2H
			sta	$de08
			stx	$de0a
			inx
			stx	$de10			;Bank 1 für C128-Transfer.

			ldy	#$91			;JobCode setzen.
			sty	$de01

			jsr	$fe06			;Job ausführen und
			jsr	$fe0f			;RL-Hardware abschalten.

			PopB	RAM_Conf_Reg
			PopB	MMU
			plp
			rts
endif
;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_REU
:SysFetchRAM		php
			sei
			ldx	MMU
			LoadB	MMU,$7e

			lda	r0L			;Startadresse C64-RAM.
			sta	$df02
			lda	r0H
			sta	$df03

			lda	r1L			;Startadresse REU.
			sta	$df04
			lda	r1H
			sta	$df05
			lda	#$00
			sta	$df06

			lda	r2L			;Anzahl Bytes.
			sta	$df07
			lda	r2H
			sta	$df08

			lda	#$00
			sta	$df09
			sta	$df0a
			lda	#$91			;Job-Code.
			sta	$df01

::51			lda	$df00
			and	#$60
			beq	:51
			stx	MMU
			plp
			rts
endif
;*** FetchRAM-Routine für ReBoot.
if RBOOT_TYPE = RAM_BBG
:SysFetchRAM		sei
			php
			PushB	MMU			;I/O-Bereich aktivieren.
			LoadB	MMU,$7e

:ExecFetchRAM		ldx	#$0f
::51			lda	r0L,x			;Register ":r0" bis ":r7"
			pha				;zwischenspeichern.
			dex
			bpl	:51

			lda	#> $de00		;High-Byte für REU-Adresse immer
			sta	r5H			;auf $DExx setzen.
			lda	r1L			;":r5" zeigt jetzt auf das erste
			sta	r5L			;benötigte Byte auf der REU-Seite.
			jsr	DefPageBBG		;Speicher-Seite berechnen.

::52			lda	#$01			;$0100 Bytes als Startwert für
			sta	r7H			;RAM-Routinen.
			lda	#$00
			tay
			sta	r7L
			sec				;Anzahl der zu kopierenden Bytes
			sbc	r5L			;berechnen.
			sta	r7L
			lda	r7H
			sbc	#$00
			sta	r7H

			lda	r7H
			cmp	r2H
			bne	:53
			lda	r7L
			cmp	r2L			;Weniger als 256 Byte kopieren ?
::53			bcc	:54			;Nein, weiter...

			lda	r2H
			sta	r7H
			lda	r2L
			sta	r7L

::54			ldx	r7L			;Bytes aus REU einlesen und inc
::55			lda	(r5L),y			;C64-RAM kopieren.
			sta	(r0L),y
			iny
			dex
			bne	:55

			lda	r0L			;Zeiger auf C64-Adresse korrigieren.
			clc
			adc	r7L
			sta	r0L
			lda	r0H
			adc	r7H
			sta	r0H

			lda	r2L			;Anzahl bereits bearbeiteter Bytes
			sec				;korrigieren.
			sbc	r7L
			sta	r2L
			lda	r2H
			sbc	r7H
			sta	r2H

			lda	#$00			;LOW-Byte Speicherseite auf #0
			sta	r5L			;setzen (Speicherseite ab $DE00).
			inc	r1H			;Zeiger auf nächste Speicherseite.
			jsr	DefPageBBG

			lda	r2L
			ora	r2H			;Alle Bytes kopiert ?
			bne	:52			;Nein, weiter.

			ldx	#$00
			ldy	#$00
::56			pla				;Register ":r0" bis ":r7"
			sta	r0,y			;wieder zurücksetzen.
			iny
			cpy	#$10
			bne	:56

			PopB	MMU
			plp				;I/O deaktivieren.
			rts

;*** REU-Speicherseite berechnen.
:DefPageBBG		lda	r1H
			pha
			sta	$dffe
			lda	#$00
			asl	r1H
			rol
			asl	r1H
			rol
			sta	$dfff
			pla
			sta	r1H
			rts
