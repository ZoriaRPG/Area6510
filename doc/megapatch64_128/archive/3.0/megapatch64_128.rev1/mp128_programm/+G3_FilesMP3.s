﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


if MP_DEMO = FALSE
;******************************************************************************
;*** MegaPatch-Dateien.
;******************************************************************************
:MP3_Files		= 24-3

;--- Gruppe #1
:File_G64_GEOS		b "GEOS128",NULL
:File_G64_GEOSr		b "GEOS128.RESET",NULL
:File_G64_GEOSb		b "GEOS128.BOOT",NULL
:File_G64_0		b "GEOS128.0",NULL
:File_G64_1		b "GEOS128.1",NULL
:File_G64_2		b "GEOS128.2",NULL
:File_G64_3		b "GEOS128.3",NULL
:File_G64_MP3		b "GEOS128.MP3",NULL
:File_G64_MBoot		b "GEOS128.MakeBoot",NULL
:File_G64_TkMse		b "GEOS128.TaskMse",NULL
:File_G64_Editor	b "GEOS128.Editor",NULL
:File_G64_Arrow		b "NewMouse128",NULL
:File_G64_Mouse		b "SuperMouse128",NULL
:File_G64_Stick1	b "SuperStick128.1",NULL
:File_G64_Stick2	b "SuperStick128.2",NULL

;--- Gruppe #2
:File_G64_RBOOT		b "RBOOT128",NULL
:File_G64_RBOOTb	b "RBOOT128.BOOT",NULL

;--- Gruppe #3
:File_G64_Disk		b "GEOS128.Disk",NULL

;--- Gruppe #4
:File_G64_Pic		b "MegaScreen40.pic",NULL

;--- Gruppe #5
;:File_G64_ScrSv1	b "PacMan",NULL
:File_G64_ScrSv2	b "PuzzleIt!",NULL
:File_G64_ScrSv3	b "Starfield",NULL
;:File_G64_ScrSv4	b "Rasterbars",NULL
;:File_G64_ScrSv5	b "64erMove",NULL


;*** Datei-Informationen.
;    Word #1 		= Zeiger auf Dateiname.
;    Word #2 		= Datei-Information.
;			Low -Byte = Dateigruppe #1 bis #5.
;			High-Byte = $00 = Bootfile für MP3-Startdiskette.
;			            $FF = Zum MP3-Start nicht notwendig.

:FileDataTab
;--- Gruppe #1
:FileCount_1		= 15
:FileGroup_1		w File_G64_GEOS,$0001
			w File_G64_GEOSr									,$ff01
			w File_G64_GEOSb									,$0001
			w File_G64_0,$0001
			w File_G64_1,$0001
			w File_G64_2,$0001
			w File_G64_3,$0001
			w File_G64_MP3,$0001
			w File_G64_MBoot									,$0001
			w File_G64_TkMse									,$0001
			w File_G64_Editor									,$0001
			w File_G64_Arrow									,$ff01
			w File_G64_Mouse									,$ff01
			w File_G64_Stick1									,$ff01
			w File_G64_Stick2									,$ff01

;--- Gruppe #2
:FileCount_2		= 2
:FileGroup_2		w File_G64_RBOOT									,$ff02
			w File_G64_RBOOTb									,$ff02

;--- Gruppe #3
:FileCount_3		= 1
:FileGroup_3		w File_G64_Disk,$0003

;--- Gruppe #4
:FileCount_4		= 1
:FileGroup_4		w File_G64_Pic,$ff04

;--- Gruppe #5
:FileCount_5		= 2
:FileGroup_5		;w File_G64_ScrSv1									,$ff05
			w File_G64_ScrSv2									,$ff05
			w File_G64_ScrSv3									,$ff05
			;w File_G64_ScrSv4									,$ff05
			;w File_G64_ScrSv5									,$ff05

			w $0000,$0000

;*** Zeiger auf Datei-Gruppen-Informationen.
:VecFileGroupL		b < FileDataTab
			b < FileGroup_1
			b < FileGroup_2
			b < FileGroup_3
			b < FileGroup_4
			b < FileGroup_5
:VecFileGroupH		b > FileDataTab
			b > FileGroup_1
			b > FileGroup_2
			b > FileGroup_3
			b > FileGroup_4
			b > FileGroup_5

;*** Anzahl Dateien in Dateigruppe.
:FilesInGroup		b MP3_Files
			b FileCount_1
			b FileCount_2
			b FileCount_3
			b FileCount_4
			b FileCount_5

endif
if MP_DEMO = TRUE
;******************************************************************************
;*** MegaPatch-Dateien.
;******************************************************************************
:MP3_Files		= 18-3

;--- Gruppe #1
:File_G64_GEOSb		b "GEOS128.BOOT",NULL
:File_G64_0		b "GEOS128.0",NULL
:File_G64_1		b "GEOS128.1",NULL
:File_G64_2		b "GEOS128.2",NULL
:File_G64_3		b "GEOS128.3",NULL
:File_G64_MP3		b "GEOS128.MP3",NULL
:File_G64_Editor	b "GEOS128.Editor",NULL
:File_G64_Arrow		b "NewMouse128",NULL
:File_G64_Mouse		b "SuperMouse128",NULL
:File_G64_Stick1	b "SuperStick128.1",NULL
:File_G64_Stick2	b "SuperStick128.2",NULL
;:File_G64_DT		b "128 DESKTOP",NULL

;--- Gruppe #2

;--- Gruppe #3
:File_G64_Disk		b "GEOS128.Disk",NULL

;--- Gruppe #4
:File_G64_Pic		b "MegaScreen40.pic",NULL

;--- Gruppe #5
;:File_G64_ScrSv1	b "PacMan",NULL
:File_G64_ScrSv2	b "PuzzleIt!",NULL
:File_G64_ScrSv3	b "Starfield",NULL
;:File_G64_ScrSv4	b "Rasterbars",NULL
;:File_G64_ScrSv5	b "64erMove",NULL


;*** Datei-Informationen.
;    Word #1 		= Zeiger auf Dateiname.
;    Word #2 		= Datei-Information.
;			Low -Byte = Dateigruppe #1 bis #5.
;			High-Byte = $00 = Bootfile für MP3-Startdiskette.
;			            $FF = Zum MP3-Start nicht notwendig.
:FileDataTab
;--- Gruppe #1
:FileCount_1		= 11
:FileGroup_1		w File_G64_GEOSb									,$0001
			w File_G64_0,$0001
			w File_G64_1,$0001
			w File_G64_2,$0001
			w File_G64_3,$0001
			w File_G64_MP3,$0001
			w File_G64_Editor									,$0001
			w File_G64_Arrow									,$ff01
			w File_G64_Mouse									,$ff01
			w File_G64_Stick1									,$ff01
			w File_G64_Stick2									,$ff01

;--- Gruppe #2
:FileCount_2		= 0
:FileGroup_2

;--- Gruppe #3
:FileCount_3		= 1
:FileGroup_3		w File_G64_Disk,$0003

;--- Gruppe #4
:FileCount_4		= 1
:FileGroup_4		w File_G64_Pic,$ff04

;--- Gruppe #5
:FileCount_5		= 2
:FileGroup_5		;w File_G64_ScrSv1									,$ff05
			w File_G64_ScrSv2									,$ff05
			w File_G64_ScrSv3									,$ff05
			;w File_G64_ScrSv4									,$ff05
			;w File_G64_ScrSv5									,$ff05

			w $0000,$0000

;*** Zeiger auf Datei-Gruppen-Informationen.
:VecFileGroupL		b < FileDataTab
			b < FileGroup_1
			b < FileGroup_2
			b < FileGroup_3
			b < FileGroup_4
			b < FileGroup_5
:VecFileGroupH		b > FileDataTab
			b > FileGroup_1
			b > FileGroup_2
			b > FileGroup_3
			b > FileGroup_4
			b > FileGroup_5

;*** Anzahl Dateien in Dateigruppe.
:FilesInGroup		b MP3_Files
			b FileCount_1
			b FileCount_2
			b FileCount_3
			b FileCount_4
			b FileCount_5

endif
