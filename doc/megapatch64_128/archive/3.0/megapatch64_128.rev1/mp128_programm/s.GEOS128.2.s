﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "GEOS128.2"
			t "G3_SymMacExt"
			t "G3_Boot.V.Class"

			o BASE_GEOS_SYS -2
			p BASE_GEOS_SYS

			i

if Sprache = Deutsch
			h "MegaPatch-Kernal"
			h "Zusatzfunktionen..."
endif

if Sprache = Englisch
			h "MegaPatch-kernal"
			h "extended functions..."
endif

;--- Ladeadresse.
:MainInit		w BASE_GEOS_SYS			;DUMMY-Bytes, da Kernal über
							;BASIC-Load eingelesen wird.

;--- ReBoot-Funktionen.
.ReBoot_SCPU		d "obj.ReBoot.SCPU"
.ReBoot_RL		d "obj.ReBoot.RL"
.ReBoot_REU		d "obj.ReBoot.REU"
.ReBoot_BBG		d "obj.ReBoot.BBG"

;--- Erweiterte GEOS-Funktionen.
.x_EnterDeskTop		d "obj.EnterDeskTop"
.x_ToBASIC		d "obj.NewToBasic"
.x_PanicBox		d "obj.NewPanicBox"
.x_GetNextDay		d "obj.GetNextDay"
.x_DoAlarm		d "obj.DoAlarm"
.x_GetFiles		d "obj.GetFiles"
.x_GetFilesData		d "obj.GetFilesData"
.x_GetFilesIcon		d "obj.GetFilesMenu"
.x_ClrDlgScreen		d "obj.ClrDlgScreen"

