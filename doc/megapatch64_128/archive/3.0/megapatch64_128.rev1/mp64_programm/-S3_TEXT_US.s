﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Systemtexte.
:NoDrvText		b PLAINTEXT,"Drive ?",NULL
:NoDskText		b PLAINTEXT,"Disk ?",NULL
:KFreeText		b PLAINTEXT,"Kb free",NULL
:InfoText2		b PLAINTEXT
			b GOTOXY
			w $0018
			b $3e
			b "Free space on target-disk: "
			b NULL

:ExtractFName		b PLAINTEXT
			b GOTOXY
			w $0010
			b $b6
			b "Extract file: ",NULL

:DskErrInfText		b PLAINTEXT,BOLDON
			b GOTOXY
			w $0050
			b $74
			b "Error-code:",NULL
:DskErrCode		b $00
:DskErrTitel		b PLAINTEXT,BOLDON
			b "Installation has failed:",NULL
:DlgInfoTitel		b PLAINTEXT,BOLDON
			b "Information:",NULL

:DlgT_01_01		b "Unknown Diskerror!",NULL
:DlgT_02_01		b "Not able to write GEOS-ID",NULL
:DlgT_02_02		b "to Systemdisk!",NULL
:DlgT_03_01		b "Not able to extract",NULL
:DlgT_03_02		b "this file!",NULL
:DlgT_04_01		b "The file 'StartMP3_64'",NULL
:DlgT_04_02		b "is partly destroyed!",NULL
:DlgT_05_01		b "Checksum-error in",NULL
:DlgT_05_02		b "file 'StartMP3_64'!",NULL
:DlgT_06_01		b "File not found:",NULL
:DlgT_06_02		b "Please flip disk!",NULL

:InfoText0		b PLAINTEXT
			b GOTOXY
			w $0010
			b $60
			b "Please have a small moment patience while SetupMP"
			b GOTOXY
			w $0010
			b $68
			b "examines the archive with the packed systemfiles."
			b GOTOXY
			w $0010
			b $80
			b "This process can last some minutes."

:InfoText0a		b GOTOXY
			w $0010
			b $9e
			b "* Testing archive...",NULL

:InfoText0b		b GOTOXY
			w $0010
			b $a8
			b "* Get systemfile-informations...",NULL

:InfoText0c		b GOTOXY
			w $0010
			b $94
			b "Installationfile: ",NULL

:Icon_Text0		b PLAINTEXT
			b GOTOXY
			w $0010
			b $60
			b "Installationprogram for GEOS - MegaPatch64"
			b GOTOXY
			w $0010
			b $70
			b "This programm will help you  to install the GEOS-"
			b GOTOXY
			w $0010
			b $78
			b "MegaPatch on your computer."
			b GOTOXY
			w $0010
			b $88
			b "If you want to cancel the MegaPatch-installation"
			b GOTOXY
			w $0010
			b $90
			b "please press the '!'-key."

			b GOTOXY
			w $00cf
			b $ae
			b "Continue with"
			b GOTOXY
			w $00d4
			b $b6
			b "installation"
			b NULL
:Icon_Text1		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Please choose the drive, on which the"
			b GOTOXY
			w $0018
			b $60
			b "system-files should be copied:"

			b GOTOXY
			w $0023
			b $84
			b "A:"
			b GOTOXY
			w $0023
			b $ac
			b "B:"
			b GOTOXY
			w $00a3
			b $84
			b "C:"
			b GOTOXY
			w $00a3
			b $ac
			b "D:"
			b NULL

:Icon_Text2		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Installationprogram for GEOS - MegaPatch64"
			b GOTOXY
			w $0018
			b $60
			b "Please choose the type of installation:"

			b GOTOXY
			w $0048
			b $76
			b "Complete installation with all files"
			b GOTOXY
			w $0048
			b $7e
			b "on disk or CMD-partition."
			b GOTOXY
			w $0048
			b $96
			b "User defined installation / update an"
			b GOTOXY
			w $0048
			b $9e
			b "existing system-disk."
			b GOTOXY
			w $0048
			b $a6
			b "(Recommended for 1541-installation)"
			b NULL

:Icon_Text11		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "The target-drive already contains files of"
			b GOTOXY
			w $0018
			b $60
			b "the GEOS-MegaPatch. Should the existing files"
			b GOTOXY
			w $0018
			b $68
			b "be deleted ?"

			b GOTOXY
			w $0033
			b $96
			b "Delete"
			b GOTOXY
			w $0024
			b $9e
			b "Systemfiles"

			b GOTOXY
			w $0075
			b $96
			b "Continue with"
			b GOTOXY
			w $007a
			b $9e
			b "installation"

			b GOTOXY
			w $00e8
			b $96
			b "Open"
			b GOTOXY
			w $00e0
			b $9e
			b "DeskTop"
			b NULL
:Icon_Text3		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Not enough space available on the"
			b GOTOXY
			w $0018
			b $60
			b "selected target-drive!"

			b GOTOXY
			w $0048
			b $76
			b "Continue with installation and copy"
			b GOTOXY
			w $0048
			b $7e
			b "only selected files."

			b GOTOXY
			w $0048
			b $96
			b "Choose another target-drive and try"
			b GOTOXY
			w $0048
			b $9e
			b "installation again."
			b NULL

:Icon_Text4		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Copy the MegaPatch-systemfiles now. A '*'"
			b GOTOXY
			w $0018
			b $60
			b "symbolizes required start-files."

			b GOTOXY
			w $0028
			b $70
			b "*"
			b GOTOXY
			w $0025
			b $86
			b "Start-files"

			b GOTOXY
			w $0078
			b $86
			b "ReBoot-system"

			b GOTOXY
			w $00d8
			b $70
			b "*"
			b GOTOXY
			w $00d8
			b $86
			b "DiskDriver"

			b GOTOXY
			w $0024
			b $ae
			b "Background-"
			b GOTOXY
			w $002e
			b $b6
			b "Pictures"

			b GOTOXY
			w $007b
			b $ae
			b "ScreenSaver"

			b GOTOXY
			w $00cf
			b $ae
			b "Continue with"
			b GOTOXY
			w $00d4
			b $b6
			b "installation"
			b NULL
:Icon_Text5		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Please choose the copy-mode for the MegaPatch"
			b GOTOXY
			w $0018
			b $60
			b "disk-driver-installation:"

			b GOTOXY
			w $0048
			b $76
			b "Copy all disk-drivers"

			b GOTOXY
			w $0048
			b $96
			b "Copy selected disk-drivers only"
			b NULL

:Icon_Text6		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Should the following disk-driver be installed"
			b GOTOXY
			w $0018
			b $60
			b "on the startdisk ?"
			b GOTOXY
			w $0020
			b $70
			b "Disk-driver for"

			b GOTOXY
			w $0028
			b $ae
			b "Copy"

			b GOTOXY
			w $006b
			b $ae
			b "Do not"
			b GOTOXY
			w $006f
			b $b6
			b "Copy"

			b GOTOXY
			w $009d
			b $ae
			b "Continue with"
			b GOTOXY
			w $00a2
			b $b6
			b "installation"

			b GOTOXY
			w $0100
			b $ae
			b "Open"
			b GOTOXY
			w $00f8
			b $b6
			b "DeskTop"
			b NULL

:Icon_Text6a		b PLAINTEXT
			b " - drive ?",0
:Icon_Text7		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Systemfiles were copied. The startdisk"
			b GOTOXY
			w $0018
			b $60
			b "is now examined for lacking files."

			b GOTOXY
			w $0035
			b $96
			b "Check"
			b GOTOXY
			w $0024
			b $9e
			b "systemfiles"
			b GOTOXY
			w $00e8
			b $96
			b "Open"
			b GOTOXY
			w $00e0
			b $9e
			b "DeskTop"
			b NULL

:Icon_Text8		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "The disk was checked and all files of the GEOS-"
			b GOTOXY
			w $0018
			b $60
			b "MegaPatch  were found.  You can  now continue"
			b GOTOXY
			w $0018
			b $68
			b "with the installation."

			b GOTOXY
			w $001e
			b $96
			b "Continue with"
			b GOTOXY
			w $0023
			b $9e
			b "installation"
			b GOTOXY
			w $00e8
			b $96
			b "Open"
			b GOTOXY
			w $00e0
			b $9e
			b "DeskTop"
			b NULL

:Icon_Text9		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Not all system-files were found, however you"
			b GOTOXY
			w $0018
			b $60
			b "can continue with the installation."

			b GOTOXY
			w $0037
			b $96
			b "Copy"
			b GOTOXY
			w $0024
			b $9e
			b "systemfiles"
			b GOTOXY
			w $0077
			b $96
			b "Continue with"
			b GOTOXY
			w $007c
			b $9e
			b "installation"
			b GOTOXY
			w $00e8
			b $96
			b "Open"
			b GOTOXY
			w $00e0
			b $9e
			b "DeskTop"
			b NULL
:Icon_Text10		b PLAINTEXT
			b GOTOXY
			w $0018
			b $58
			b "Not all system-files were found. MegaPatch"
			b GOTOXY
			w $0018
			b $60
			b "cannot be started by this disk."

			b GOTOXY
			w $0037
			b $96
			b "Copy"
			b GOTOXY
			w $0024
			b $9e
			b "systemfiles"

			b GOTOXY
			w $00e8
			b $96
			b "Open"
			b GOTOXY
			w $00e0
			b $9e
			b "DeskTop"
			b NULL

;*** Information über Kopierstatus.
:Inf_Wait		b GOTOXY
			w $0018
			b $5a
			b PLAINTEXT,OUTLINEON
			b "Please wait!"
			b GOTOXY
			w $0018
			b $70
			b PLAINTEXT
			b NULL

:Inf_DelSysFiles	b "Delete Systemfiles...",NULL
:Inf_CopySystem		b "Copy System-files...",NULL
:Inf_CopyRBoot		b "Copy ReBoot-files...",NULL
:Inf_CopyBkScr		b "Copy Background-picture...",NULL
:Inf_CopyScrSv		b "Copy ScreenSaver...",NULL
:Inf_CopyDskDrv		b "Copy DiskDrivers...",NULL
:Inf_InstallMP		b "System-disk is examined...",NULL
:Inf_ChkDkSpace		b "Target-disk is checked...",NULL


:InfoText1		b PLAINTEXT
			b GOTOXY
			w $0020
			b $76
			b "Please have a small moment patience, while"
			b GOTOXY
			w $0020
			b $80
			b "Setup configures your MegaPatch-bootdisk!"
			b NULL
