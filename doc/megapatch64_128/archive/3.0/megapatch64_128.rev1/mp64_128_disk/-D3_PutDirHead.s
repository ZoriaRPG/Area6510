﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = RL_NM!RD_NM!RD_NM_SCPU!FD_NM!HD_NM!HD_NM_PP
if :tmp0 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	Set_DirHead		;Zeiger auf BAM setzen.
			jsr	xPutBlock		;Verzeichnis-Header einlesen.
			txa
			bne	:51			;Diskettenfehler ? => Ja, Abbruch...

			jmp	xPutBAMBlock		;Aktuelle BAM auf Disk schreiben.
::51			rts
endif
;******************************************************************************
::tmp1 = RL_41!RD_41
if :tmp1 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	SetBAM_TrSe		;Zeiger auf BAM-Sektor/-Speicher.
			jmp	xWriteBlock		;BAM-Sektor konvertieren/speichern.
endif
;******************************************************************************
::tmp2 = RL_71!RD_71
if :tmp2 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	SetBAM_TrSe1		;Zeiger auf BAM-Sektor/-Speicher.
			jsr	xWriteBlock		;BAM-Sektor konvertieren/speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			jsr	SetBAM_TrSe2		;Zeiger auf BAM-Sektor/-Speicher.
			jmp	xWriteBlock		;BAM-Sektor konvertieren/speichern.
::51			rts
endif
;******************************************************************************
::tmp3 = RL_81!RD_81
if :tmp3 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	SetBAM_TrSe1		;Zeiger auf BAM-Sektor/-Speicher.
			jsr	xWriteBlock		;BAM-Sektor konvertieren/speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	SetBAM_TrSe2		;Zeiger auf BAM-Sektor/-Speicher.
			jsr	xWriteBlock		;BAM-Sektor konvertieren/speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	SetBAM_TrSe3		;Zeiger auf BAM-Sektor/-Speicher.
			jmp	xWriteBlock		;BAM-Sektor konvertieren/speichern.
::51			rts
endif
;******************************************************************************
::tmp4 = C_41!FD_41!HD_41!HD_41_PP
if :tmp4 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	xEnterTurbo		;Turbo aktivieren.
			txa				;Laufwerksfehler ?
			bne	:52			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.
			jsr	SetBAM_TrSe		;Zeiger auf Track/Sektor/Speicher.
			jsr	xWriteBlock		;Sektor auf Diskette schreiben.
::51			jmp	DoneWithIO		;I/O abschalten.
::52			rts				;Ende...
endif
;******************************************************************************
::tmp5 = C_71
if :tmp5 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	SetBAM_TrSe1		;Zeiger auf BAM-Sektor #1.
			jsr	xPutBlock		;BAM-Sektor speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			ldy	curDrive
			lda	curDirHead +3		;Diskettenmodus einlesen.
			sta	doubleSideFlg-8,y	;1541-Diskette ?
			bpl	:51			;Ja, Ende...

			jsr	SetBAM_TrSe2		;Zeiger auf BAM-Sektor #2.
			jsr	xPutBlock		;BAM-Sektor speichern.
::51			rts
endif
;******************************************************************************
::tmp6 = FD_71!HD_71!HD_71_PP
if :tmp6 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	xEnterTurbo		;TurboDOS aktivieren.
			txa				;Laufwerksfehler ?
			bne	:52			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.

			jsr	SetBAM_TrSe1		;Zeiger auf BAM-Sektor #1.
			jsr	xWriteBlock		;BAM-Sektor speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			jsr	SetBAM_TrSe2		;Zeiger auf BAM-Sektor #2.
			jsr	xWriteBlock		;BAM-Sektor speichern.
::51			jmp	DoneWithIO
::52			rts
endif
;******************************************************************************
::tmp7 = C_81!FD_81!HD_81!HD_81_PP
if :tmp7 = TRUE
;******************************************************************************
;*** BAM auf Diskette schreiben.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4
:xPutDirHead		jsr	xEnterTurbo		;TurboDOS aktivieren.
			txa				;Laufwerksfehler ?
			bne	:52			;Ja, Abbruch...
			jsr	InitForIO		;I/O aktivieren.

			jsr	SetBAM_TrSe1		;Zeiger auf BAM-Sektor #1.
			jsr	xWriteBlock		;BAM-Sektor speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			jsr	SetBAM_TrSe2		;Zeiger auf BAM-Sektor #2.
			jsr	xWriteBlock		;BAM-Sektor speichern.
			txa				;Diskettenfehler ?
			bne	:51			;Ja, Abbruch...

			jsr	SetBAM_TrSe3		;Zeiger auf BAM-Sektor #2.
			jsr	xWriteBlock		;BAM-Sektor speichern.
::51			jmp	DoneWithIO
::52			rts
endif
