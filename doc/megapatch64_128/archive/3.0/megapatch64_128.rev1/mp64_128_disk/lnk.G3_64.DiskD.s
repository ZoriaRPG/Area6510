﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;



			n "GEOS64.Disk"

			h "Laufwerkstreiber"
			h "Demo-Version..."

			m
			- "mod.MDD_#100"		;Info

			- "mod.MDD_#110"		;INIT 1541
			- "DiskDev_1541"
			- "mod.MDD_#112"		;INIT 1571
			- "DiskDev_1571"
			- "mod.MDD_#114"		;INIT 1581
			- "DiskDev_1581"

			- "mod.MDD_#120"		;INIT RAM41
			- "DiskDev_RAM41"
			- "mod.MDD_#122"		;INIT RAM71
			- "DiskDev_RAM71"
			- "mod.MDD_#124"		;INIT RAM81
			- "DiskDev_RAM81"
			- "mod.MDD_#126"		;INIT RAMNM
			- "DiskDev_RAMNM"
			- "mod.MDD_#128"		;INIT RAMNM SCPU
			- "DiskDev_RAMNMS"

			- 				;INIT FD41  => INIT 1581
			-
			- 				;INIT FD71  => INIT 1581
			-
			- 				;INIT FD81  => INIT 1581
			- "DiskDev_FD81"
			- 				;INIT FDNM  => INIT 1581
			-

			- 				;INIT HD41
			-
			- 				;INIT HD71
			-
			- "mod.MDD_#144"		;INIT HD81
			- "DiskDev_HD81"
			- 				;INIT HDNM
			-

			- "mod.MDD_#150"		;INIT RL41
			-
			- 				;INIT RL71  => INIT RL41
			-
			- 				;INIT RL81  => INIT RL41
			- "DiskDev_RL81"
			- 				;INIT RLNM  => INIT RL41
			-

			- "mod.MDD_#160"		;INIT PCDOS
			- "DiskDev_PCDOS"

			/
