﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldy	curDrive
			lda	driveType -8,y		;Laufwerkstyp einlesen und
			sta	driveTypeCopy		;zwischenspeichern.
			and	#%10111111		;Shadow-Bit löschen und
			sta	driveType -8,y 		;zurückschreiben.
			sta	curType

			ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00
			tay
			sta	(r5L),y

			jsr	xNewDisk		;Neue Diskette initialisieren.
			txa				;Diskettenfehler ?
			bne	:53			;Ja, Abbruch...

			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:53			;Ja, Abbruch...

			bit	driveTypeCopy		;Shadow1541 ?
			bvc	:51			;Nein, weiter...

			jsr	VerifySekInRAM		;BAM in ShadowRAM gespeichert ?
			beq	:51			;Ja, weiter...

			jsr	NewShadowDisk		;ShadowRAM löschen.
			jsr	SetBAM_TrSe		;Zeiger auf BAM-Sektor setzen.
			jsr	SaveSekInRAM		;BAM in ShadowRAM speichern.

::51			ldy	#$11
::52			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:52

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	#NO_ERROR
::53			lda	driveTypeCopy
			ldy	curDrive
			sta	driveType -8,y		;Laufwerkstyp zurücksetzen.
			sta	curType
			rts

:driveTypeCopy		b $00
endif
;******************************************************************************
::tmp1 = C_71!C_81!RD_41!RD_71!RD_81!PC_DOS
if :tmp1 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00
			tay
			sta	(r5L),y

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			ldy	#$11
::51			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:51

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	#NO_ERROR
::52			rts
endif
;******************************************************************************
::tmp2 = FD_41!HD_41!HD_41_PP
if :tmp2 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00			;Vorhandenen Diskettennamen
			tay				;löschen, falls keine Diskette im
			sta	(r5L),y			;Lauafwerk.

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:5			; => Ja, Abbruch...

::1			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:2			; => Ja, Abbruch...

			lda	curDirHead +2		;Partitionsformat testen.
			cmp	#$41
			bne	:2
			lda	curDirHead +3
			beq	:3

;--- Gültige Partition suchen.
::2			jsr	xLogNewPart		;Gültige Partition suchen.
			txa				;Partition gefunden ?
			beq	:1			; => Neue Partition aktivieren.
			bne	:5			; => Nein, Abbruch...

;--- Disketten-Informationen einlesen.
::3			ldy	#$11			;Diskettenname kopieren,.
::4			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:4

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			ldx	#NO_ERROR
::5			rts
endif
;******************************************************************************
::tmp3 = FD_71!HD_71!HD_71_PP
if :tmp3 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00			;Vorhandenen Diskettennamen
			tay				;löschen, falls keine Diskette im
			sta	(r5L),y			;Lauafwerk.

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:5			; => Ja, Abbruch...

::1			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:2			; => Ja, Abbruch...

			lda	curDirHead +2		;Partitionsformat testen.
			cmp	#$41
			bne	:2
			lda	curDirHead +3
			cmp	#$80
			beq	:3

;--- Gültige Partition suchen.
::2			jsr	xLogNewPart		;Gültige Partition suchen.
			txa				;Partition gefunden ?
			beq	:1			; => Neue Partition aktivieren.
			bne	:5			; => Nein, Abbruch...

;--- Disketten-Informationen einlesen.
::3			ldy	#$11			;Diskettenname kopieren,.
::4			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:4

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			ldx	#NO_ERROR
::5			rts
endif
;******************************************************************************
::tmp4 = FD_81!HD_81!HD_81_PP
if :tmp4 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00			;Vorhandenen Diskettennamen
			tay				;löschen, falls keine Diskette im
			sta	(r5L),y			;Lauafwerk.

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:5			; => Ja, Abbruch...

::1			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:2			; => Ja, Abbruch...

			lda	dir3Head +2		;Partitionsformat testen.
			cmp	#$44
			bne	:2
			lda	dir3Head +3
			cmp	#$bb
			beq	:3

;--- Gültige Partition suchen.
::2			jsr	xLogNewPart		;Gültige Partition suchen.
			txa				;Partition gefunden ?
			beq	:1			; => Neue Partition aktivieren.
			bne	:5			; => Nein, Abbruch...

;--- Disketten-Informationen einlesen.
::3			ldy	#$11			;Diskettenname kopieren,.
::4			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:4

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			ldx	#NO_ERROR
::5			rts
endif
;******************************************************************************
::tmp5 = FD_NM!HD_NM!HD_NM_PP
if :tmp5 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00			;Vorhandenen Diskettennamen
			tay				;löschen, falls keine Diskette im
			sta	(r5L),y			;Lauafwerk.

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:5			; => Ja, Abbruch...

::1			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:2			; => Ja, Abbruch...

			lda	curDirHead +2		;Partitionsformat testen.
			cmp	#$48
			bne	:2
			lda	curDirHead +3
			beq	:3

;--- Gültige Partition suchen.
::2			jsr	xLogNewPart		;Gültige Partition suchen.
			txa				;Partition gefunden ?
			beq	:1			; => Neue Partition aktivieren.
			bne	:5			; => Nein, Abbruch...

;--- Disketten-Informationen einlesen.
::3			ldy	#$11			;Diskettenname kopieren,.
::4			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:4

			jsr	xChkDkGEOS 		;Auf GEOS-Diskette testen.

			jsr	xGetDiskSize

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			ldx	#NO_ERROR
::5			rts
endif
;******************************************************************************
::tmp6 = RL_41!RL_71!RL_81
if :tmp6 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00
			tay
			sta	(r5L),y

			jsr	FindRAMLink
			txa
			bne	:52

			jsr	xNewDisk		;Diskette/Partition testen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	xGetDirHead		;Aktuelle BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			ldy	#$11
::51			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:51

			jsr	xChkDkGEOS 		;Auf GEOS-Diskette testen.

			MoveW	RL_PartADDR,r3		;Startadr. Partition nach ":r3".

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			lda	RL_DEV_ADDR		;Geräteadresse RL in AKKU.
			ldx	#NO_ERROR		;Flag: "Kein Fehler"...
::52			rts				;Kein Befehl mehr nach LDX #xx. Es
							;gibt Programme die ohne TXA nur
							;mittels BEQ xz auf Fehler testen!
endif
;******************************************************************************
::tmp7 = RL_NM
if :tmp7 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00
			tay
			sta	(r5L),y

			jsr	FindRAMLink
			txa
			bne	:52

			jsr	xNewDisk		;Diskette/Partition testen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	xGetDirHead		;Aktuelle BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			ldy	#$11
::51			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:51

			jsr	xChkDkGEOS 		;Auf GEOS-Diskette testen.

			jsr	xGetDiskSize		;Diskettengröße ermitteln.

			MoveW	RL_PartADDR,r3		;Startadr. Partition nach ":r3".

			ldx	curDrive
			ldy	drivePartData-8,x	;Aktive Partition einlesen.
			lda	RL_DEV_ADDR		;Geräteadresse RL in AKKU.
			ldx	#NO_ERROR		;Flag: "Kein Fehler"...
::52			rts				;Kein Befehl mehr nach LDX #xx. Es
							;gibt Programme die ohne TXA nur
							;mittels BEQ xz auf Fehler testen!
endif
;******************************************************************************
::tmp8 = RD_NM!RD_NM_SCPU
if :tmp8 = TRUE
;******************************************************************************
;*** Diskette öffnen.
;    Übergabe:		-
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1,r4,r5
:xOpenDisk		ldx	#r5L
			jsr	GetPtrCurDkNm		;Zeiger auf Diskettenname.

			lda	#$00
			tay
			sta	(r5L),y

			jsr	xNewDisk		;Neue Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			jsr	xGetDiskSize		;Diskettengröße ermitteln.

			jsr	xGetDirHead		;BAM einlesen.
			txa				;Diskettenfehler ?
			bne	:52			;Ja, Abbruch...

			ldy	#$11
::51			lda	curDirHead +$90,y
			sta	(r5L),y
			dey
			bpl	:51

			jsr	xChkDkGEOS 		;auf GEOS-Diskette testen.

			ldx	#NO_ERROR
::52			rts
endif
