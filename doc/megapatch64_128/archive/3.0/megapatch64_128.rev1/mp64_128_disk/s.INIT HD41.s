﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "mod.MDD_#140"
			t "G3_SymMacExt"
			t "G3_Disk.V.Class"
			t "-DD_JumpTab"

			t "-DD_InitHD"

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000
:FastPPmode		b $00

;*** TurboPP-Treiber.
:HD_PP			d "DiskDev_HD41_PP"

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
;******************************************************************************
