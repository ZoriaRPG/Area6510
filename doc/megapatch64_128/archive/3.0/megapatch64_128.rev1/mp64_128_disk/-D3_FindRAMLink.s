﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = RL_41!RL_71!RL_81!RL_NM
if :tmp0 = TRUE
;******************************************************************************
;*** RL_Laufwerk suchen.
:FindRAMLink		jsr	xExitTurbo
			jsr	InitForIO

			lda	RL_DEV_ADDR		;Ist RL-Adresse bereits definiert ?
			beq	:51			; => Nein, weiter...
			jsr	TestRL_Device		;RAMLink-Adresse testen.
			txa				;RL gefunden ?
			bne	:51			; => Ja, Ende...
			jmp	DoneWithIO

;--- Diese Routine wird beim ersten Aufruf des Treibers aktiviert und wenn
;    von der RL mit einer anderen Adresse gebootet wurde.
::51			lda	#$08
			sta	RL_DEV_ADDR
::52			jsr	TestRL_Device
			txa
			beq	:53
			inc	RL_DEV_ADDR
			lda	RL_DEV_ADDR
			cmp	#30
			bcc	:52
			lda	#$00
			sta	RL_DEV_ADDR

			ldx	#$0d
::53			jmp	DoneWithIO

;*** RAMLink-Laufwerk suchen.
:TestRL_Device		ldx	#> com_MR_CMD
			lda	#< com_MR_CMD
			ldy	#$06
			jsr	Job_Command
			txa
			bne	:53

			jsr	$ffae
			lda	#$00
			sta	STATUS
			lda	RL_DEV_ADDR
			jsr	$ffb4
			lda	#$ff
			jsr	$ff96

			ldy	#$00
::51			jsr	$ffa5
			cmp	RL_DEV_CODE,y
			bne	:52
			iny
			cpy	#$06
			bne	:51
			jsr	$ffab
			ldx	#$00
			rts

::52			jsr	$ffa5
			iny
			cpy	#$06
			bne	:52
::53			jsr	$ffab
			ldx	#$0d
			rts

:RL_DEV_CODE		b "CMD RL"
:com_MR_CMD		b "M-R",$a0,$fe,$06
endif
