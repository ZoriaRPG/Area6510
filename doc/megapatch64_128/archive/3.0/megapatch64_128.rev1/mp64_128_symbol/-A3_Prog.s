﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;--- Programme
:ASS_PROG		OPEN_BOOT
			OPEN_SYMBOL

;--- Laufwerkstreiber für Bootdiskette.
			OPEN_DISK
			t "-A3_Disk#2"

;--- Externes Kernel.
			OPEN_KERNEL
			t "-A3_Kernel#4"

;--- GEOS.Editor.
			OPEN_PROG
			t "-A3_Prog#1"

			b $f1
			lda	a1H
			jsr	SetDevice
			jsr	OpenDisk

			LoadW	r0,:114
			jsr	DeleteFile
			LoadW	a0,:115
			rts

if ASS_COMP = TRUE_C64
::114			b "GEOS64.Editor",$00
::115			b $f5
			b $f0,"lnk.GEOS64.Edit",$00
			b $f4
endif

if ASS_COMP = TRUE_C128
::114			b "GEOS128.Editor",$00
::115			b $f5
			b $f0,"lnk.GEOS128.Edit",$00
			b $f4
endif

;--- Zusatzprogramme.
			OPEN_PROG
			t "-A3_Prog#2"
			b $f4
