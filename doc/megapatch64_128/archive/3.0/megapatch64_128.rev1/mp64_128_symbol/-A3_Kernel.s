﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;--- Kernel #1.
:ASS_KERNEL		OPEN_BOOT
			OPEN_SYMBOL

;--- Maustreiber.
			OPEN_PROG
			t "-A3_Kernel#1"

;--- Kernel #2.
			OPEN_KERNEL
			t "-A3_Kernel#2"

;--- Joysticktreiber.
			OPEN_PROG
			t "-A3_Kernel#3"

;--- Kernel packen.
			b $f1
			LoadW	r0,40*25		;Bildschirm löschen.
			LoadW	r1,COLOR_MATRIX
			lda	screencolors
			sta	r2L
			jsr	FillRam

			lda	#DvAdr_Target		;Auf Laufwerk #9 umschalten und
			jsr	SetDevice		;Diskette öffnen.
			jsr	OpenDisk

			LoadB	r0L,%00000000
			LoadW	r6 ,:101
			jsr	GetFile			;Kernal packen.
			jmp	EnterDeskTop		;Fehler: Zum DeskTop zurück.

::101			b "MP_MakeKernal",$00
