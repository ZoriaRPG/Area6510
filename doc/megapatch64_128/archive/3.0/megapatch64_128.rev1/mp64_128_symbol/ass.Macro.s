﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


:OPEN_BOOT		m
			b $f1
			lda	#DvAdr_Target
			jsr	SetDevice
			jsr	OpenDisk
			lda	#DvPart_Target
			sta	r3H
			jsr	OpenPartition
			lda	a1L
			jsr	SetDevice
			LoadW	a0,:END_BOOT
			rts
::END_BOOT
			/

:OPEN_SYMBOL		m
			b $f1
			lda	#DvAdr_Symbol
			jsr	SetDevice
			jsr	OpenDisk
			lda	#DvPart_Symbol
			sta	r3H
			jsr	OpenPartition

			lda	a1L
			jsr	SetDevice
			LoadW	a0,:END_SYMBOL
			rts
::END_SYMBOL
			/

:OPEN_PROG		m
			b $f1
			lda	#DvAdr_Source
			jsr	SetDevice
			jsr	OpenDisk
			lda	#DvPart_Prog
			sta	r3H
			jsr	OpenPartition

			lda	a1L
			jsr	SetDevice
			LoadW	a0,:END_PROG
			rts
::END_PROG
			/

:OPEN_KERNEL		m
			b $f1
			lda	#DvAdr_Source
			jsr	SetDevice
			jsr	OpenDisk
			lda	#DvPart_Kernel
			sta	r3H
			jsr	OpenPartition

			lda	a1L
			jsr	SetDevice
			LoadW	a0,:END_KERNEL
			rts
::END_KERNEL
			/

:OPEN_DISK		m
			b $f1
			lda	#DvAdr_Source
			jsr	SetDevice
			jsr	OpenDisk
			lda	#DvPart_Disk
			sta	r3H
			jsr	OpenPartition

			lda	a1L
			jsr	SetDevice
			LoadW	a0,:END_DISK
			rts
::END_DISK
			/
