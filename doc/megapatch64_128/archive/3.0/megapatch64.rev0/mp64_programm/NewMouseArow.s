﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			t "G3_SymMacExt"

			n "NewMouse128"
			f AUTO_EXEC
			a "MegaCom Soft"
			c "NewMouse    V1.0"
			z $40

:D_OrgMouseData		= $c985
:E_OrgMouseData		= $c991
:D_OrgMouseData80	= $c856
:E_OrgMouseData80	= $c844


:Start			jsr	TestGEOSVersion
			jsr	TestC128
			bmi	C128
			jmp	EnterDeskTop

:C128			LoadW	r0,Mouse80Pic
			LoadW	r1,D_OrgMouseData80
			lda	nationality
			bne	:deutsch
			LoadW	r1,E_OrgMouseData80
::deutsch		LoadW	r2,$0020
			lda	#$01
			sta	r3L
			lda	#$00
			sta	r3H
			jsr	MoveBData
			lda	#$00
			sta	r0H
			sta	r0L
			jsr	SetMsePic
			LoadW	r0,Mouse40Pic
			LoadW	r1,D_OrgMouseData
			lda	nationality
			bne	:deutsch2
			LoadW	r1,E_OrgMouseData
::deutsch2		LoadW	r2,$0018
			jsr	MoveData
			LoadW	r1,$84c1
			jsr	MoveData
;			lda	#$0b
;			sta	C_GEOS_OUSE
;			sta	$d027
;			sta	$d028
			jmp	EnterDeskTop

:Mouse80Pic		b $3f,$ff,$5f,$ff
			b $6f,$ff,$77,$ff
			b $7b,$ff,$7d,$ff
			b $5b,$ff,$a7,$ff
			b $00,$00,$40,$00
			b $60,$00,$70,$00
			b $78,$00,$7c,$00
			b $58,$00,$00,$00

:Mouse40Pic		b $40,$00,$00,$60
			b $00,$00,$70,$00
			b $00,$78,$00,$00
			b $7c,$00,$00,$58
			b $00,$00,$00,$00
			b $00,$00,$00,$00


:TestGEOSVersion	lda	version
			cmp	#$20
			bne	:1
			rts

::1			jmp	EnterDeskTop


:TestC128		lda	#$12
			cmp	version
			bpl	:1
			lda	c128Flag
::1			rts
