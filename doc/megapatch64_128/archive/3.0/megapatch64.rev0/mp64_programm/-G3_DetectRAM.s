﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Auf RAMCard testen.
if Flag64_128 = TRUE_C64
:SCPU_ROM_NR		=	$e487
else
:SCPU_ROM_NR		=	$f6dd
endif

:DetectRAMCard		lda	Device_SCPU		;SuperCPU verfügbar ?
			beq	:53			; => Nein, weiter...

			lda	SCPU_ROM_NR+1		;CPU-ROM testen.
			cmp	#"."			;RAMCard erst ab ROM V1.40
			bne	:53			;verfügbar. Ältere Versionen der
			lda	SCPU_ROM_NR		;SCPU unterstützen keine RAMCard!
			cmp	#"1"
			bcc	:53
			beq	:51
			bcs	:52
::51			lda	SCPU_ROM_NR+2
			cmp	#"4"
			bcc	:53
::52			ldx	#NO_ERROR
			rts
::53			ldx	#DEV_NOT_FOUND
			rts

;*** Auf RAMLink testen.
:DetectRAMLink		lda	Device_RL		;RL angeschlossen ?
			beq	:51			; => Nein, weitersuchen.
			ldx	#NO_ERROR
			rts
::51			ldx	#DEV_NOT_FOUND
			rts

;*** Auf C=REU/CMD-REU testen.
:DetectREU		ldx	#$02			;Sektor-Adressen in Steuerregister
::51			txa				;speichern.
			sta	$df00,x
			inx
			cpx	#$06
			bcc	:51

			ldx	#$02			;Steuerregister auslesen und Werte
::52			txa				;überprüfen.
			cmp	$df00,x
			bne	:53			;Steuerregister fehlerhaft, Ende...
			inx
			cpx	#$06
			bcc	:52

			ldx	#NO_ERROR
			rts
::53			ldx	#DEV_NOT_FOUND
			rts

;*** Auf GeoRAM/BBGRAM testen.
:DetectBBG		ldy	#$00
::51			lda	$de00,y
			eor	#$ff
			sta	$de00,y
			cmp	$de00,y
			php
			eor	#$ff
			sta	$de00,y
			plp
			bne	:52
			iny
			bne	:51

			ldx	#NO_ERROR
			rts
::52			ldx	#DEV_NOT_FOUND
			rts
