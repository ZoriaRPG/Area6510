﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = FD_NM
if :tmp0 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xGetDiskSize		jsr	Save_RegData

			jsr	InitForIO

			lda	#$7f
			sta	LastTrOnDsk

			ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head
			jsr	xReadBlock
			txa
			bne	:51

			ldx	#$20
			lda	dir3Head +2
			cmp	#$48
			bne	:51

			lda	dir3Head +8
			sta	LastTrOnDsk		;zwischenspeichern.
			sta	DiskSize_Hb		;Gesamtspeicher in KBytes berechnen.
			lda	#$00
			sta	DiskSize_Lb

			lsr	DiskSize_Hb		;Gesamtspeicher in KBytes umrechnen.
			ror	DiskSize_Lb
			lsr	DiskSize_Hb
			ror	DiskSize_Lb

			ldx	#$00
::51			jsr	DoneWithIO
			jmp	Load_RegData
endif
;******************************************************************************
::tmp1 = HD_NM
if :tmp1 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xGetDiskSize		jsr	Save_RegData

			jsr	InitForIO

			lda	#$7f
			sta	LastTrOnDsk

			ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head
			jsr	xReadBlock
			txa
			bne	:51

			ldx	#$20
			lda	dir3Head +2
			cmp	#$48
			bne	:51

			lda	dir3Head +8
			sta	LastTrOnDsk		;zwischenspeichern.
			sta	DiskSize_Hb		;Gesamtspeicher in KBytes berechnen.
			lda	#$00
			sta	DiskSize_Lb

			lsr	DiskSize_Hb		;Gesamtspeicher in KBytes umrechnen.
			ror	DiskSize_Lb
			lsr	DiskSize_Hb
			ror	DiskSize_Lb

			ldx	#$00
::51			jsr	DoneWithIO
			jmp	Load_RegData
endif
;******************************************************************************
::tmp2 = HD_NM_PP
if :tmp2 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xGetDiskSize		jsr	Save_RegData

			jsr	InitForIO

			lda	#$7f
			sta	LastTrOnDsk

			ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head
			jsr	xReadBlock
			txa
			bne	:51

			ldx	#$20
			lda	dir3Head +2
			cmp	#$48
			bne	:51

			lda	dir3Head +8
			sta	LastTrOnDsk		;zwischenspeichern.
			sta	DiskSize_Hb		;Gesamtspeicher in KBytes berechnen.
			lda	#$00
			sta	DiskSize_Lb

			lsr	DiskSize_Hb		;Gesamtspeicher in KBytes umrechnen.
			ror	DiskSize_Lb
			lsr	DiskSize_Hb
			ror	DiskSize_Lb

			ldx	#$00
::51			jsr	DoneWithIO
			jsr	Load_dir3Head
			jmp	Load_RegData
endif
;******************************************************************************
::tmp3 = RL_NM
if :tmp3 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xGetDiskSize		jsr	Save_RegData

			ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head		;Sonst Probleme beim erkennen der
			lda	RL_PartNr		;Partitionsgröße!
			sta	r3H
			jsr	xDsk_SekRead
			txa
			bne	:51

			ldx	dir3Head +8		;Letzten verfügbaren Track
			stx	LastTrOnDsk		;zwischenspeichern.

			stx	DiskSize_Hb		;Gesamtspeicher in KBytes berechnen.
			ldx	#$00
			stx	DiskSize_Lb

			lsr	DiskSize_Hb		;Gesamtspeicher in KBytes umrechnen.
			ror	DiskSize_Lb
			lsr	DiskSize_Hb
			ror	DiskSize_Lb

;			ldx	#$00			;Flag: "Kein Fehler..."
::51			jmp	Load_RegData
endif
;******************************************************************************
::tmp4 = RD_NM!RD_NM_SCPU
if :tmp4 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xGetDiskSize		jsr	Save_RegData

			ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head		;Sonst Probleme beim erkennen der
			jsr	xDsk_SekRead		;Partitionsgröße!
			txa
			bne	:51

			ldx	dir3Head +8		;Letzten verfügbaren Track
			stx	LastTrOnDsk		;zwischenspeichern.

			stx	DiskSize_Hb		;Gesamtspeicher in KBytes berechnen.
			ldx	#$00
			stx	DiskSize_Lb

			lsr	DiskSize_Hb		;Gesamtspeicher in KBytes umrechnen.
			ror	DiskSize_Lb
			lsr	DiskSize_Hb
			ror	DiskSize_Lb

;			ldx	#$00			;Flag: "Kein Fehler..."
::51			jmp	Load_RegData
endif
