﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0a = C_41!C_71!C_81!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
::tmp0b = FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp0  = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** Turbo-Modus aktivieren.
:DataClkATN_HIGH	sei
			lda	$8e
			sta	$dd00

;*** Warten, bis TurboModus bereit.
:Wait_DATA_IN_LOW	bit	$dd00
			bpl	Wait_DATA_IN_LOW
			rts
endif
