﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0a = C_41!C_71!C_81
::tmp0b = FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp0 = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** Floppy-Routine ohne Parameter aufrufen.
;    Übergabe:		AKKU/xReg, Low/High-Byte der Turbo-Routine.
:xTurboRoutine		stx	$8c			;Zeiger auf Routine nach $8b/$8c
			sta	$8b			;kopieren.
			ldy	#$02			;2-Byte-Befehl.
			bne	InitTurboData		;Befehl ausführen.

;*** Floppy-Programm mit zwei Byte-Parameter starten.
;    Übergabe:		AKKU/xReg, Low/High-Byte der Turbo-Routine.
;			r1L/r1H  , Parameter-Bytes.
:xTurboRoutSet_r1	stx	$8c			;Zeiger auf Routine nach $8b/$8c
			sta	$8b			;kopieren.

;*** Floppy-Programm mit zwei Byte-Parameter starten.
;    Übergabe:		$8B/$8C  , Low/High-Byte der Turbo-Routine.
;			r1L/r1H  , Parameter-Bytes.
:xTurboRoutine_r1	ldy	#$04			;4-Byte-Befehl.

			lda	r1H			;Parameter-Bytes in Init-Befehl
			sta	TurboParameter2 		;kopieren.
			lda	r1L
			sta	TurboParameter1

;*** Turbodaten initialisieren.
;    Übergabe:		$8B/$8C = Zeiger auf TurboRoutine.
;			yReg	 = Anzahl Bytes (Routine+Parameter)
:InitTurboData		lda	$8c			;Auszuführende Routine in
			sta	TurboRoutineH		;Init-Befehl kopieren.
			lda	$8b
			sta	TurboRoutineL

			lda	#> TurboRoutineL
			sta	$8c
			lda	#< TurboRoutineL
			sta	$8b
			jmp	Turbo_PutInitByt

:TurboRoutineL		b $00
:TurboRoutineH		b $00
:TurboParameter1	b $00
:TurboParameter2	b $00
endif
;******************************************************************************
;Original-Routine von Maurice Randall
;Wurde nur um unnötigen Ballast entfernt. Da diese Routine trotzdem noch zu
;groß war wurde die Routine weiter gepackt (siehe nächste Seite).
::tmp1 = FALSE ;HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp1 = TRUE
;******************************************************************************
;*** Floppy-Routine aufrufen.
;xReg = $00		= TurboDOS deaktivieren
;     = $01		= Sektor schreiben
;     = $02		= Sektor lesen
;     = $03		= Linkbytes lesen
;     = $04		= Fehlerstatus abfragen.
:TurboRoutine1		lda	r4H
			sta	$8c
			lda	r4L
			sta	$8b
			b $2c
:xGetDiskError		ldx	#$04
:TurboRoutine2		lda	#$00
			sta	ErrorCode
			lda	PP_DOS_MODE_1,x
			sta	TurboMode1
			lda	PP_DOS_MODE_2,x
			sta	TurboMode2
			lda	PP_DOS_ROUT_L,x
			sta	TurboRoutineL
			lda	PP_DOS_ROUT_H,x
			sta	TurboRoutineH

			jsr	$e0a9

			lda	r1H
			sta	TurboParameter2
			lda	r1L
			sta	TurboParameter1

			lda	$8c
			pha
			lda	$8b
			pha

			lda	#> TurboRoutineL
			sta	$8c
			lda	#< TurboRoutineL
			sta	$8b
			ldy	#$04
			jsr	Turbo_PutBytes
			pla
			sta	$8b
			pla
			sta	$8c

			ldy	TurboMode1
			beq	:3
			bpl	:1

			ldy	#$00
::1			bit	TurboMode2
			bmi	:2
			jsr	Turbo_GetBytes
			clv
			bvc	:3

;--- Sektor an HD senden.
::2			jsr	Turbo_PutBytes

::3			lda	TurboMode2
			and	#$01
			beq	:4
			tay
			lda	#> ErrorCode
			sta	$8c
			lda	#< ErrorCode
			sta	$8b
			jsr	Turbo_GetBytes

::4			jsr	$fe0f

			lda	ErrorCode
			cmp	#$02
			bcc	:5
			clc
			adc	#$1e
			b $2c
::5			lda	#$00
			tax
			stx	ErrorCode
			rts

;*** RAMLINK-IO für HD-Kabel aktivieren.
;:INIT_RAMLINK_IO	sei
;			jmp	$e0a9

;*** RAMLINK-IO für HD-Kabel deaktivieren.
;:EXIT_RAMLINK_IO	jsr	HD_MODE_SEND
;			jmp	$fe0f

;*** HD-Kabel-Modus wechseln.
:HD_MODE_SEND		ldx	#$98
			b $2c
:HD_MODE_RECEIVE	ldx	#$88

;			sei
;			sec
;::1			lda	$d012
;			sbc	#$31
;			bcc	:2
;			and	#$06
;			beq	:1

::2			lda	$df41
			pha
			lda	$df42
			stx	$df43
			sta	$df42
			pla
			sta	$df41
			rts

;*** Variablen.
; $00 = Fehlercode von HD abfragen.
; $80 = Sektordaten von HD empfangen.
; $02 = Linkbytes von HD empfangen.
:PP_DOS_MODE_1		b $00,$80,$80,$02,$00

; Bit#7 =1, Daten von HD empfangen.
; Bit#0 =1, Fehlercode von HD empfangen.
:PP_DOS_MODE_2		b $80,$81,$01,$01,$01

:PP_DOS_ROUT_L		b < TD_MLoop_Stop
			b < TD_WriteBlock
			b < TD_ReadBlock
			b < TD_ReadLink
			b < TD_GetError

:PP_DOS_ROUT_H		b > TD_MLoop_Stop
			b > TD_WriteBlock
			b > TD_ReadBlock
			b > TD_ReadLink
			b > TD_GetError

:TurboMode1		b $00
:TurboMode2		b $00
:TurboRoutineL		b $00
:TurboRoutineH		b $00
:TurboParameter1	b $00
:TurboParameter2	b $00

:ErrorCode		b $00
endif
;******************************************************************************
::tmp2 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp2 = TRUE
;******************************************************************************
;*** Floppy-Routine aufrufen.
;xReg = $00		= TurboDOS deaktivieren
;     = $01		= Sektor schreiben
;     = $02		= Sektor lesen
;     = $03		= Linkbytes lesen
;     = $04		= Fehlerstatus abfragen.
:TurboRoutine1		lda	r4H
			sta	$8c
			lda	r4L
			sta	$8b
			b $2c
:xGetDiskError		ldx	#$04
:TurboRoutine2		stx	TurboMode

			lda	#$00			;Fehlercode löschen.
			sta	ErrorCode

			lda	PP_DOS_ROUT_L,x		;Zeiger auf auszuführende Routine
			sta	TurboRoutineL		;im TurboDOS einlesen und speichern.
			lda	PP_DOS_ROUT_H,x
			sta	TurboRoutineH

			lda	r1L			;Track und Sektor an TurboDOS
			sta	TurboParameter1		;übergeben.
			lda	r1H
			sta	TurboParameter2

			jsr	EN_SET_REC		;RL-Hardware aktivieren.

			lda	$8c			;Zeiger auf Sektorspeicher
			pha				;auf Stack retten.
			lda	$8b
			pha

			lda	#> TurboRoutineL	;TurboDOS-Routine
			sta	$8c			;ausführen.
			lda	#< TurboRoutineL
			sta	$8b
			ldy	#$04
			jsr	Turbo_PutBytes
			pla
			sta	$8b
			pla
			sta	$8c

			ldy	TurboMode		;TurboPP starten?
			beq	:4			; => Ja, Ende...

			dey				;Sektor schreiben?
			bne	:1			; => Nein, weiter...
			jsr	Turbo_PutBytes		;Sektor auf Diskette schreiben und
			jmp	:3			;Fehlerstatus abfragen.

::1			dey				;Sektor lesen?
			beq	:2			; => Ja, weiter...
			dey				;Linkbytes lesen?
			bne	:3			; => Nein, weiter...
			ldy	#$02
::2			jsr	Turbo_GetBytes		;Daten von Diskette lesen.

::3			lda	#> ErrorCode
			sta	$8c
			lda	#< ErrorCode
			sta	$8b
			ldy	#$01
			jsr	Turbo_GetBytes		;Fehlerstatus abfragen.

::4			jsr	RL_HW_DIS2		;RL-Hardware deaktivieren.

			lda	ErrorCode
			cmp	#$02
			bcc	:5
			adc	#$1d
			b $2c
::5			lda	#$00
			tax
			stx	ErrorCode
			rts

;*** HD-Kabel-Modus wechseln.
:HD_MODE_SEND		ldx	#$98
			b $2c
:HD_MODE_RECEIVE	ldx	#$88
			lda	$df41
			pha
			lda	$df42
			stx	$df43
			sta	$df42
			pla
			sta	$df41
			rts

;*** Variablen.
:PP_DOS_ROUT_L		b < TD_MLoop_Stop
			b < TD_WriteBlock
			b < TD_ReadBlock
			b < TD_ReadLink
			b < TD_GetError

:PP_DOS_ROUT_H		b > TD_MLoop_Stop
			b > TD_WriteBlock
			b > TD_ReadBlock
			b > TD_ReadLink
			b > TD_GetError

:TurboMode		b $00
:TurboRoutineL		b $00
:TurboRoutineH		b $00
:TurboParameter1	b $00
:TurboParameter2	b $00

:ErrorCode		b $00
endif
