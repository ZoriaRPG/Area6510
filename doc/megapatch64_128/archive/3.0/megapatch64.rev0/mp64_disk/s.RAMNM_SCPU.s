﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "DiskDev_RAMNMS"
			t "G3_SymMacExtDisk"

			a "M. Kanet"
			o DISK_BASE

if .p
:C_41			= FALSE
:C_71			= FALSE
:C_81			= FALSE
:RD_41			= FALSE
:RD_71			= FALSE
:RD_81			= FALSE
:RD_NM			= FALSE
:RD_NM_SCPU		= TRUE
:RL_41			= FALSE
:RL_71			= FALSE
:RL_81			= FALSE
:RL_NM			= FALSE
:HD_41			= FALSE
:HD_71			= FALSE
:HD_81			= FALSE
:HD_NM			= FALSE
:HD_41_PP		= FALSE
:HD_71_PP		= FALSE
:HD_81_PP		= FALSE
:HD_NM_PP		= FALSE
:FD_41			= FALSE
:FD_71			= FALSE
:FD_81			= FALSE
:FD_NM			= FALSE
:PC_DOS			= FALSE

:DriveModeFlags		= SET_MODE_SUBDIR ! SET_MODE_FASTDISK

:PART_TYPE		= $04
:PART_MAX		= 0

:dir3Head		= $9c80
:DiskDrvMode		= DrvRAMNM_SCPU
:Tr_BorderBlock		= 1
:Se_BorderBlock		= 255
:Tr_1stDirSek		= 1
:Se_1stDirSek		= 1
:Tr_1stDataSek		= 1
:Se_1stDataSek		= 64
:MaxDirPages		= 255
endif


;*** Sprungtabelle.
:JumpTab		t "-D3_JumpTable"

;*** Eintrag für aktuelle Partition.
;    Werden auch in der REU abgespeichert!
:RL_DataStart
:DiskSize_Lb		b $00				;(Angabe in Kb!)
:DiskSize_Hb		b $00
:LastTrOnDsk		b $7f

:DirHead_Tr		b $01
:DirHead_Se		b $01

:LastSearchTr		b $00

:CurSek_BAM		b $00
:BAM_Modified		b $00
:RL_DataEnd

;*** Include-Dateien.
			t "-D3_IncludeFile"

;******************************************************************************
			g dir3Head
;******************************************************************************
