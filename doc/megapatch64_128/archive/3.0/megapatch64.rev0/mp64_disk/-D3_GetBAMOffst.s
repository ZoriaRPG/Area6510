﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = C_81!FD_81!HD_81!HD_81_PP
if :tmp0 = TRUE
;******************************************************************************
;*** Offest auf BAM berechnen.
;    Übergabe:		r6L  = Track
;    Rückgabe:		xReg = Offset auf Byte.
;			AKKU = $00, dir2Head
;			       $FF, dir3Head
:GetBAM_Offset		lda	#$00
			sta	:53 +1

			lda	r6L
			cmp	#41
			bcc	:51
			sbc	#40
			dec	:53 +1
::51			sec
			sbc	#$01
			asl
			sta	:52 +1
			asl
			clc
::52			adc	#$ff
			tax
::53			lda	#$ff
			rts
endif
