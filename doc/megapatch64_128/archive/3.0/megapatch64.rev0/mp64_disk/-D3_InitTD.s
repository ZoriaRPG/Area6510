﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = C_41
if :tmp0 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O aktivieren.

			lda	#< TurboDOS_1541	;Zeiger auf TurboDOS-Routine in
			sta	$8b			;C64-Speicher.
			lda	#> TurboDOS_1541
			sta	$8c

			lda	#> $0300		;Zeiger auf TurboDOS-Routine in
			sta	Floppy_ADDR_H		;Floppy-Speicher.
			lda	#< $0300
			sta	Floppy_ADDR_L

			lda	#$1a			;26 * 32 Bytes kopieren.
			sta	$8d
::51			jsr	CopyTurboDOSByt		;TurboDOS-Daten an Floppy senden.
			txa				;Laufwerkfehler ?
			bne	:54			;Ja, Abbruch...

			clc				;Zeiger auf C64-Speicher
			lda	#$20			;korrigieren.
			adc	$8b
			sta	$8b
			bcc	:52
			inc	$8c
::52			clc				;Zeiger auf Floppy-Speicher
			lda	#$20			;korrigieren.
			adc	Floppy_ADDR_L
			sta	Floppy_ADDR_L
			bcc	:53
			inc	Floppy_ADDR_H
::53			dec	$8d			;Alle Bytes gesendet ?
			bpl	:51			;Nein, weiter...
::54			jmp	DoneWithIO		;I/O abschalten.
endif
;******************************************************************************
::tmp1 = C_71
if :tmp1 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O aktivieren.

			lda	#< TurboDOS_1571	;Zeiger auf TurboDOS-Routine in
			sta	$8b			;C64-Speicher.
			lda	#> TurboDOS_1571
			sta	$8c

			lda	#> $0300		;Zeiger auf TurboDOS-Routine in
			sta	Floppy_ADDR_H		;Floppy-Speicher.
			lda	#< $0300
			sta	Floppy_ADDR_L

			lda	#$1f			;31 * 32 Bytes kopieren.
			sta	$8d
::51			jsr	CopyTurboDOSByt		;TurboDOS-Daten an Floppy senden.
			txa				;Laufwerkfehler ?
			bne	:54			;Ja, Abbruch...

			clc				;Zeiger auf C64-Speicher
			lda	#$20			;korrigieren.
			adc	$8b
			sta	$8b
			bcc	:52
			inc	$8c
::52			clc				;Zeiger auf Floppy-Speicher
			lda	#$20			;korrigieren.
			adc	Floppy_ADDR_L
			sta	Floppy_ADDR_L
			bcc	:53
			inc	Floppy_ADDR_H
::53			dec	$8d			;Alle Bytes gesendet ?
			bpl	:51			;Nein, weiter...

::54			jmp	DoneWithIO		;I/O abschalten.
endif
;******************************************************************************
::tmp2 = C_81
if :tmp2 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O aktivieren.

			lda	#< TurboDOS_1581	;Zeiger auf TurboDOS-Routine in
			sta	$8b			;C64-Speicher.
			lda	#> TurboDOS_1581
			sta	$8c

			lda	#> $0300		;Zeiger auf TurboDOS-Routine in
			sta	Floppy_ADDR_H		;Floppy-Speicher.
			lda	#< $0300
			sta	Floppy_ADDR_L

			lda	#$0f			;15 * 32 Bytes kopieren.
			sta	$8d
::51			jsr	CopyTurboDOSByt		;TurboDOS-Daten an Floppy senden.
			txa				;Laufwerkfehler ?
			bne	:54			;Ja, Abbruch...

			clc				;Zeiger auf C64-Speicher
			lda	#$20			;korrigieren.
			adc	$8b
			sta	$8b
			bcc	:52
			inc	$8c
::52			clc				;Zeiger auf Floppy-Speicher
			lda	#$20			;korrigieren.
			adc	Floppy_ADDR_L
			sta	Floppy_ADDR_L
			bcc	:53
			inc	Floppy_ADDR_H
::53			dec	$8d			;Alle Bytes gesendet ?
			bpl	:51			;Nein, weiter...

::54			jmp	DoneWithIO		;I/O abschalten.
endif
;******************************************************************************
::tmp3 = FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81
if :tmp3 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O-Bereich einbleden.
			ldx	#> CMD_LdTurboDOS
			lda	#< CMD_LdTurboDOS
			jsr	SendFloppyCom		;GEOS-Modus aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	$ffae
			ldx	#$00			;Flag: "Kein Fehler..."
::51			jmp	DoneWithIO		;I/O-Bereich ausblenden.

;*** Befehl zum installieren des TurboDOS in CMD-Laufwerken.
:CMD_LdTurboDOS		b "GEOS",NULL

endif
;******************************************************************************
::tmp4 = HD_NM
if :tmp4 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O-Bereich einbleden.
			ldx	#> CMD_LdTurboDOS
			lda	#< CMD_LdTurboDOS
			jsr	SendFloppyCom		;GEOS-Modus aktivieren.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...
			jsr	$ffae

			lda	#< $0327		;Routinen in CMD-HD
			ldx	#> $0327		;patchen um den Native-8Mb-Bug
			ldy	#$00			;zu beheben. ReadLink wird dabei
			jsr	PatchCMD_Dos		;durch ReadBlock ersetzt!!!

			lda	#< $04d7
			ldx	#> $04d7
			ldy	#$ff
			jsr	PatchCMD_Dos

			lda	#< $04ed
			ldx	#> $04ed
			ldy	#$ff
			jsr	PatchCMD_Dos

			ldx	#$00			;Flag: "Kein Fehler..."
::51			jmp	DoneWithIO		;I/O-Bereich ausblenden.

:PatchCMD_Dos		sta	CMD_Patch +3
			stx	CMD_Patch +4
			sty	CMD_Patch +6
			lda	#<CMD_Patch
			ldx	#>CMD_Patch
			ldy	#$07
			jsr	Job_Command		;GEOS-Modus aktivieren.
			jmp	$ffae

;*** Befehl zum installieren des TurboDOS in CMD-Laufwerken.
:CMD_LdTurboDOS		b "GEOS",NULL
:CMD_Patch		b "M-W",$27,$05,$01,$00
endif
;******************************************************************************
::tmp5 = PC_DOS
if :tmp5 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O aktivieren.

			lda	#< TurboDOS_DOS		;Zeiger auf TurboDOS-Routine in
			sta	$8b			;C64-Speicher.
			lda	#> TurboDOS_DOS
			sta	$8c

			lda	#> $0500		;Zeiger auf TurboDOS-Routine in
			sta	Floppy_ADDR_H		;Floppy-Speicher.
			lda	#< $0500
			sta	Floppy_ADDR_L

			lda	#$13			;16 * 32 Bytes kopieren.
			sta	$8d
::51			jsr	CopyTurboDOSByt		;TurboDOS-Daten an Floppy senden.
			txa				;Laufwerkfehler ?
			bne	:54			;Ja, Abbruch...

			clc				;Zeiger auf C64-Speicher
			lda	#$20			;korrigieren.
			adc	$8b
			sta	$8b
			bcc	:52
			inc	$8c
::52			clc				;Zeiger auf Floppy-Speicher
			lda	#$20			;korrigieren.
			adc	Floppy_ADDR_L
			sta	Floppy_ADDR_L
			bcc	:53
			inc	Floppy_ADDR_H
::53			dec	$8d			;Alle Bytes gesendet ?
			bpl	:51			;Nein, weiter...

::54			jmp	DoneWithIO		;I/O abschalten.
endif
;******************************************************************************
::tmp6 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp6 = TRUE
;******************************************************************************
;*** Turbo-Routine in FloppyRAM kopieren.
:InitTurboDOS		jsr	InitForIO		;I/O aktivieren.

			lda	#< TurboPP		;Zeiger auf TurboDOS-Routine in
			sta	$8b			;C64-Speicher.
			lda	#> TurboPP
			sta	$8c

			lda	#> $0300		;Zeiger auf TurboDOS-Routine in
			sta	Floppy_ADDR_H		;Floppy-Speicher.
			lda	#< $0300
			sta	Floppy_ADDR_L

			lda	#$0b			;11 * 32 Bytes kopieren.
			sta	$8d
::51			jsr	CopyTurboDOSByt		;TurboDOS-Daten an Floppy senden.
			txa				;Laufwerkfehler ?
			bne	:54			;Ja, Abbruch...

			clc				;Zeiger auf C64-Speicher
			lda	#$20			;korrigieren.
			adc	$8b
			sta	$8b
			bcc	:52
			inc	$8c
::52			clc				;Zeiger auf Floppy-Speicher
			lda	#$20			;korrigieren.
			adc	Floppy_ADDR_L
			sta	Floppy_ADDR_L
			bcc	:53
			inc	Floppy_ADDR_H
::53			dec	$8d			;Alle Bytes gesendet ?
			bpl	:51			;Nein, weiter...

::54			jmp	DoneWithIO		;I/O abschalten.
endif
