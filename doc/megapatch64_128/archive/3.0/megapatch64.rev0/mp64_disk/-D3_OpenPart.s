﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;******************************************************************************
::tmp0 = FD_41!FD_71!FD_81!FD_NM
if :tmp0 = TRUE
;******************************************************************************
;*** Partition wechseln.
:xOpenPartition		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xSwapPartition
			jsr	DoneWithIO
			txa
			bne	:51
			jmp	xOpenDisk
::51			rts

;*** Partition wechseln.
;    Übergabe:		r3H	= Partitions-Nr.
:xSwapPartition		ldx	#ILLEGAL_PARTITION
			lda	r3H
			cmp	#PART_MAX +1
			bcs	:51

			LoadW	r4,GP_DATA
			jsr	xReadPDirEntry
			txa
			bne	:51

			ldx	#NO_PART_FD_ERR
			bit	GP_DATA_TYPE +1
			bpl	:51

			ldx	#PART_FORMAT_ERR
			lda	GP_DATA_TYPE
			cmp	#PART_TYPE
			bne	:51

			lda	r3H
			sta	com_CP +2

			ldx	#> com_CP
			lda	#< com_CP
			ldy	#$04
			jsr	Job_Command
			jsr	$ffae
			txa
			bne	:51

			ldx	#> com_InitDisk
			lda	#< com_InitDisk
			jsr	SendFloppyCom
			jsr	$ffae

			ldx	curDrive
			lda	r3H			;Aktive Partition speichern.
			sta	drivePartData -8,x

			ldx	#NO_ERROR
::51			rts

:com_CP			b $43,$d0,$00,CR
:com_InitDisk		b "I0:",CR,NULL
endif
;******************************************************************************
::tmp1 = HD_41!HD_71!HD_81!HD_NM!HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp1 = TRUE
;******************************************************************************
;*** Partition wechseln.
:xOpenPartition		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xSwapPartition
			jsr	DoneWithIO
			txa
			bne	:51
			jmp	xOpenDisk
::51			rts

;*** Partition wechseln.
;    Übergabe:		r3H	= Partitions-Nr.
:xSwapPartition		ldx	#ILLEGAL_PARTITION
			lda	r3H
			cmp	#PART_MAX +1
			bcs	:51

			LoadW	r4,GP_DATA
			jsr	xReadPDirEntry
			txa
			bne	:51

			stx	STATUS

			ldx	#PART_FORMAT_ERR
			lda	GP_DATA_TYPE
			cmp	#PART_TYPE
			bne	:51

			lda	r3H
			sta	com_CP +2

			ldx	#> com_CP
			lda	#< com_CP
			ldy	#$04
			jsr	Job_Command
			jsr	$ffae
			txa				;Fehler aufgetreten ?
			bne	:51			;Ja, Abbruch...

			ldx	#> com_InitDisk
			lda	#< com_InitDisk
			jsr	SendFloppyCom
			jsr	$ffae

			ldx	curDrive
			lda	r3H			;Aktive Partition speichern.
			sta	drivePartData -8,x

			ldx	#NO_ERROR
::51			rts

:com_CP			b $43,$d0,$00,CR
:com_InitDisk		b "I0:",CR,NULL
endif
;******************************************************************************
::tmp2 = RL_41!RL_71!RL_81!RL_NM
if :tmp2 = TRUE
;******************************************************************************
;*** Partition wechseln.
:xOpenPartition		jsr	xExitTurbo
			jsr	InitForIO
			jsr	xSwapPartition
			jsr	DoneWithIO
			txa
			bne	:51
			jmp	xOpenDisk
::51			rts

;*** Partition wechseln.
;    Übergabe:		r3H	= Partitions-Nr.
:xSwapPartition		ldx	#ILLEGAL_PARTITION
			lda	r3H
			cmp	#PART_MAX +1
			bcs	:51

			LoadW	r4,GP_DATA
			jsr	xReadPDirEntry
			txa
			bne	:51

			ldx	#PART_FORMAT_ERR
			lda	GP_DATA_TYPE
			cmp	#PART_TYPE
			bne	:51

			lda	r3H
			sta	com_CP +2

			ldx	#> com_CP
			lda	#< com_CP
			ldy	#$05
			jsr	Job_Command

			jsr	$ffae

			ldx	curDrive
			lda	GP_DATA   +20
			sta	ramBase   - 8,x
			lda	GP_DATA   +21		;Nur um kompatibel zu alten Prg.
			sta	driveData + 3		;zu bleiben. ":driveData" wird von
							;die RL-Treibern nicht benötigt.
							;Die 1571-Treiber ändern dieses
							;Bytes ebenfalls nicht mehr!
			lda	r3H			;Aktive Partition speichern.
			sta	drivePartData -8,x

			ldx	#NO_ERROR
::51			rts

:com_CP			b $43,$d0,$00,CR,NULL
:com_InitDisk		b "I0:",CR,NULL
endif
