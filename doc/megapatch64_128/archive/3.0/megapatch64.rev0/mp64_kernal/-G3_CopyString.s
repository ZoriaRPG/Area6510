﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** String kopieren. (Akku =$00 bis zum $00-Byte, <>$00 = Anzahl Zeichen).
;    Akku =  $00, Ende durch $00-Byte.
;    Akku <> $00, Anzahl Zeichen.
:xCopyString		lda	#$00
:xCopyFString		stx	:1 +1
			sty	:2 +1
			tax
			ldy	#$00
::1			lda	(r4L),y
::2			sta	(r5L),y
			bne	:3
			txa
			beq	:4
::3			iny
			beq	:4
			txa
			beq	:1
			dex
			bne	:1
::4			rts
