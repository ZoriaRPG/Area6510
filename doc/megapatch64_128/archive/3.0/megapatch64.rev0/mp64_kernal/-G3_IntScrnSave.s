﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;Beim 128er unter IO-Bereich ($d000) Bank 1!

;*** Bildschirmschoner aktivieren ?
:IntScrnSave		bit	Flag_ScrSaver		;ScreenSaver-Modus testen.
			bmi	:5			; => Nicht aktiv.
			bvs	:6			; => Neu initialisieren.
			beq	:6			; => ScreenSaver aufrufen.

			lda	inputData
			eor	#%11111111		;Mausbewegung ?
			bne	:6			;Ja, Zähler neu setzen.

			lda	pressFlag		;Taste gedrückt ?
			and	#%11100000		;Nein, Zähler korrigieren.
			beq	:1

::6			ldx	Flag_ScrSvCnt		;Zähler neu initialisieren.
			stx	:1 +1
			stx	:3 +1
			ldx	#%00100000		;Flag für "Zähler läuft".
			bne	:4

::1			ldx	#$06
			beq	:2
			dec	:1 +1
			rts

::2			dec	:1 +1
::3			ldx	#$06
			beq	:4
			dec	:3 +1
			rts

::4			stx	Flag_ScrSaver		;$00 = ScreenSaver starten.
::5			rts

