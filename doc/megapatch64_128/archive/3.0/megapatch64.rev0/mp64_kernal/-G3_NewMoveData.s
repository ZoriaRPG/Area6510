﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


;*** Inline: Speicher verschieben.
:xi_MoveData		pla
			sta	returnAddress +0
			pla
			sta	returnAddress +1
			ldy	#$06
::51			lda	(returnAddress)   ,y
			sta	r0              -1,y
			dey
			bne	:51
			jsr	xMoveData
			jmp	Exit7ByteInline

;*** Speicherbereich veschieben.
:xMoveData		lda	r2L
			ora	r2H			;Anzahl Bytes = $0000 ?
			beq	:107			;Ja, -> Keine Funktion.

			ldx	#$06
::100			lda	r0L -1,x
			pha
			dex
			bne	:100

			lda	sysRAMFlg		;MoveData über REU ?
			bpl	:101			;Nein, weiter...

			lda	r1H
			pha
			stx	r1H
			stx	r3L			;Speicherbereich aus RAM
			jsr	StashRAM		;in REU übertragen.
			pla
			sta	r0H
			lda	r1L
			sta	r0L			;Speicherbereich aus REU
			jsr	FetchRAM		;in RAM zurückschreiben.
			jmp	:106			;Ende ":MoveData".

::101			lda	r0H
			cmp	r1H
			bne	:102
			lda	r0L
			cmp	r1L
::102			bcc	:108			; -> Daten Abwärts kopieren.

;*** ":MoveData" (Daten Aufwärts kopieren).
::103			ldy	#$00
			lda	r2H
			beq	:105
::104			lda	(r0L),y
			sta	(r1L),y
			iny
			bne	:104
			inc	r0H
			inc	r1H
			dec	r2H
			bne	:104
::105			cpy	r2L
			beq	:106
			lda	(r0L),y
			sta	(r1L),y
			iny
			jmp	:105

;*** Ende ":MoveData", Register wiederherstellen.
::106			ldx	#$00
::106a			pla
			sta	r0L,x
			inx
			cpx	#$06
			bcc	:106a
::107			rts

;*** ":MoveData" (Daten Abwärts kopieren).
::108			clc
			lda	r2H
			adc	r0H
			sta	r0H
			clc
			lda	r2H
			adc	r1H
			sta	r1H

			ldy	r2L
			beq	:110
			jsr	MoveBytes

::110			dec	r0H
			dec	r1H
			lda	r2H
			beq	:106
			jsr	MoveBytes
			dec	r2H
			jmp	:110

;*** Einzelne Bytes kopieren.
:MoveBytes		dey
			lda	(r0L),y
			sta	(r1L),y
			tya
			bne	MoveBytes
