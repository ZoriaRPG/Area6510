﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


			n "64erMove"
			t "G3_SymMacExt"

			a "M. Kanet"
			f SYSTEM
			o LD_ADDR_SCRSAVER

			i

if Flag64_128 = TRUE_C64
			c "ScrSaver64  V1.0"
			z $80				;nur GEOS64 bei MP3-64
endif

if Flag64_128 = TRUE_C128
			c "ScrSaver128 V1.0"
			z $40				;40 und 80 Zeichen-Modus bei MP3-128
endif

;*** ScreenSaver aufrufen.
:MainInit		jmp	InitScreenSaver

;*** ScreenSaver installieren.
;    Laufwerk von dem ScreenSaver geladen wurde muß noch aktiv sein!
;    Rückgabe eines Fehlers im xReg ($00=Kein Fehler).
;    ACHTUNG! Nur JMP-Befehl oder "LDX #$00:RTS", da direkt im Anschluß
;    der Name des ScreenSavers erwartet wird! (Addr: G3_ScrSave +6)
:InstallSaver		ldx	#$00
			rts

;*** Name des ScreenSavers.
;    Direkt nach dem JMP-Befehl, da über MEGA-Editor
;    der name angezeigt wird.
:SaverName		b "64er Mover",NULL

;*** ScreenSaver aufrufen.
:InitScreenSaver	jsr	FindFreeBank		;Freie Speicherbank suchen.
			tya				;64K-Bank gefunden ?
			bne	:50			; => Ja, weiter...
			lda	#%10000000		;Bildschirmschoner abschalten,
			sta	Flag_ScrSaver		;da kein RAM frei ist...
			rts

::50			sty	RAM_BANK		;Bank für Zwischenspeicher setzen.

			php				;IRQ sperren.
			sei				;Screener läuft in der MainLoop!

			ldx	#$1f			;Register ":r0" bis ":r3"
::51			lda	r0L,x			;zwischenspeichern.
			pha
			dex
			bpl	:51

			jsr	SaveRamData

			jsr	i_MoveData
			w	ScrSaverCode
			w	$0a00
			w	EndSaverCode - ScrSaverCode

			lda	RAM_BANK
			jsr	$0a00			;Bildschirmschoner aktivieren.

			jsr	LoadRamData

			lda	#%01000000		;Bildschirmschoner neu starten.
			sta	Flag_ScrSaver

			ldx	#$00			;Register ":r0" bis ":r3"
::52			pla				;zurückschreiben.
			sta	r0L,x
			inx
			cpx	#$20
			bne	:52

			sei				;IRQ abschalten.
			ldx	CPU_DATA		;CPU-Register zwischenspeichern und
			lda	#$35			;I/O-Bereich einblenden.
			sta	CPU_DATA

::53			lda	#$00
			sta	$dc00			;Tastenregister aktivieren.
			lda	$dc01			;Tastenstatus einlesen.
			eor	#$ff			;Taste noch gedrückt ?
			bne	:53			;Ja, Warteschleife...

			lda	$d011
			ora	#%00010000
			sta	$d011

			stx	CPU_DATA		;CPU-Register zurücksetzen.
			plp				;IRQ zurücksetzen und
			rts
;*** GEOS-Kernel zwischenspeichern.
:SaveRamData		jsr	SetRamVec1
			jsr	StashRAM

			jsr	i_MoveData
			w	$c000
			w	$2000
			w	$4000

			jsr	SetRamVec2
			jmp	StashRAM

;*** GEOS-Kernel wieder einlesen.
:LoadRamData		jsr	SetRamVec2
			jsr	FetchRAM

			ldy	#$00
::51			lda	(r0L),y
			sta	(r1L),y
			iny
			bne	:51
			inc	r0H
			inc	r1H
			dec	r2H
			bne	:51

			jsr	SetRamVec1
			jmp	FetchRAM

;*** Zeiger auf Kernelspeicher setzen.
:SetRamVec1		LoadW	r0 ,$0400
			LoadW	r1 ,$0400
			LoadW	r2 ,$bc00
			lda	RAM_BANK
			sta	r3L
			rts

:SetRamVec2		LoadW	r0 ,$2000
			LoadW	r1 ,$c000
			LoadW	r2 ,$4000
			lda	RAM_BANK
			sta	r3L
			rts

;*** Grafikeffekt.
:ScrSaverCode		d "obj.SS_64erMove"
:EndSaverCode

;*** Freie 64K-Speicherbank suchen.
:FindFreeBank		ldy	#$00
::51			jsr	GetBankByte
			beq	:52
			iny
			cpy	ramExpSize
			bne	:51
			ldy	#$00
::52			rts

;*** Tabellenwert für Speicherbank finden.
:GetBankByte		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
::51			cpx	#$00
			beq	:52
			asl
			asl
			dex
			bne	:51
::52			and	#%11000000
			rts

;*** Variablen für 64K-Speicher.
:RAM_BANK		b $00

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
			g LD_ADDR_SCRSAVER + R2_SIZE_SCRSAVER -1
;******************************************************************************
