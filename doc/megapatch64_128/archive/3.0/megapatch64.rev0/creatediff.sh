#!/bin/sh
rm -rf ./diff
mkdir diff

MP3_SOURCE1=$(cd ../megapatch64.rev0; pwd)
MP3_SOURCE2=$(cd ../megapatch64_128.rev0; pwd)

original_source=$(find ./ -type f -name "*.s" -mindepth 2 -maxdepth 2 -print | sort | sed "s, ,@@,g")

for sourcefile in ${original_source}; do
	fnames=${sourcefile//@@/ }
	fname=$(basename ${sourcefile//@@/ })
	target_source=$(find ${MP3_SOURCE2}/ -type f -mindepth 2 -maxdepth 2 -name "${fname}" -print | sort | sed "s, ,@@,g")
	for targetfile in ${target_source}; do
		fnamet=${targetfile//@@/ }
		check=$(diff -U5 -d -r -N "${fnames}" "${fnamet}" | sed "s,^--- ./,--- $MP3_SOURCE1/,g" | sed "s, .*gitlab/doc/, ./,g")
		diffname=${fnamet//\//\~}
		diffname=${diffname//*gitlab\~doc\~/}.diff
		if [ x"${check}" != x"" ]; then
			echo "${fname} -> ${fnamet}"
			echo "${check}" > diff/"${diffname}"
		fi
	done
done
