﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;"Strg_name"-Routinen geben eine Meldung auf dem Bildschirm aus.
;******************************************************************************

			n "GEOS64.BOOT"
			t "G3_SymMacExt"
			t "G3_V.Cl.64.Boot"

			o BASE_GEOSBOOT -2		;BASIC-Start beachten!
			p InitBootProc

			z $80
			i
<MISSING_IMAGE_DATA>

if Sprache = Deutsch
			h "Installiert MegaPatch 64"
			h "in Ihrem GEOS-System..."
endif

if Sprache = Englisch
			h "Installs MegaPatch 64"
			h "in your GEOS-kernal..."
endif

if .p
			t "s.GEOS64.1.ext"
			t "s.GEOS64.2.ext"
			t "s.GEOS64.3.ext"
			t "o.Patch_SCPU.ext"
endif

;*** Boot-Informationen einbinden.
.L_KernelData		t "-G3_BootData"

;*** Angaben zur Speichererweiterung.
:ExtRAM_Type		b $00				;$00 = keine RAM-Erweiterung.
							;$80 = RAMLink / RAMDrive.
							;$40 = Commodore REU.
							;$20 = BBG/GEORAM.
							;$10 = SuperCPU/RAMCard.
:ExtRAM_Size		b $00				;Anzahl 64K-Bänke.
:ExtRAM_Bank		w $0000
:ExtRAM_Part		b $00
:ExtRAM_Name		w $0000

;*** Kennbytes der Speichererweiterung.
:RamTypeCodes		b NULL
			b RAM_SCPU
			b RAM_RL
			b RAM_REU
			b RAM_BBG
			b NULL

;*** Dateinamen für Systemdateien.
:FileName0		b "GEOS64.BOOT",NULL
:FileName1		b "GEOS64.1",NULL
:FileName2		b "GEOS64.2",NULL
:FileName3		b "GEOS64.3",NULL
:FileName4		b "RBOOT64.BOOT",NULL

;*** Partitions-Daten.
:GP_Befehl		b $47,$2d,$50,$ff,$0d
:GP_Data		s $20

;*** Laufwerksadresse RAMLink.
:RL_BootAddr		b $00

;*** Verzögerung für Textausgabe.
:TEXT_OUT_DELAY		b $00

;*** Hardware erkennen.
:MainInit		sei				;IRQ sperren.
			cld				;Dezimal-Flag löschen.
			ldx	#$ff			;Stack-Pointer löschen.
			txs

			lda	#$37			;Standard-RAM-Bereiche einblenden.
			sta	CPU_DATA

			ldx	#$ff			;SuperCPU verügbar ?
			lda	$d0bc
			bpl	:51
			inx
::51			stx	Device_SCPU

			ldx	#$ff			;RAMLink verügbar ?
			lda	EN_SET_REC
			cmp	#$78
			beq	:52
			inx
::52			stx	Device_RL

			lda	Boot_Type
			and	#%11110000
			cmp	#DrvRAMLink		;Startlaufwerk = RAMLink ?
			bne	:53			; => Nein, weiter...

			lda	curDevice		;RL-Geräteadresse speichern.
			sta	RL_BootAddr
			cmp	#12			;Adresse #8 bis #11 ?
			bcs	:54			; => Nein, weiter...

::53			lda	curDevice		;Boot-Laufwerk speichern.
			sta	Boot_Drive

::54			jsr	PrintBootInfo

;*** Speichererweiterung wählen.
:DetectRAM		jsr	FindRamExp		;Speichererweiterung suchen.
			jsr	FindRL_Part		;RAMLink-Startpartition ermitteln.

;*** GEOS-Kernal einlesen und installieren.
:InitKernelMP3		jsr	LoadKernal_1		;Kernal-Teil #1 laden und
			jsr	InitKernal_1		;installieren.

			ldx	ramExpSize		;Speicherbereich für Megapatch-
			dex				;Kernal in REU festlegen.
			stx	MP3_64K_SYSTEM
			dex
			stx	MP3_64K_DATA

			ldx	#$00			;Laufwerkstreiber von
			stx	MP3_64K_DISK		;Diskette installieren.

			sei
			lda	#$30			;64K-RAM-Bereich einblenden.
			sta	CPU_DATA

			jsr	InitDeviceRAM		;RAM-Patches installieren.

			jsr	LoadKernal_2		;Kernal-Teil #2 laden.
			jsr	InitKernal_2

			jsr	LoadKernal_3		;Kernal-Teil #3 laden.
			jsr	InitKernal_3

			sei
			lda	#$37			;Standard-RAM-Bereiche einblenden.
			sta	CPU_DATA

			jsr	InitDeviceSCPU		;SuperCPU patchen.
;			jsr	InitDeviceHD		;"P0"-Befehl ausführen. Dieser
							;Befehl deaktiviert ein vorhandenes
							;Kabel von der RL zur HD.

;*** GEOS-Variablen initialisieren.
:InitGEOS		jsr	Strg_InitGEOS		;Installationsmeldung ausgeben.

			sei
			lda	#$35			;I/O-Bereiche einblenden.
			sta	CPU_DATA

			lda	cia1base +15		;I/O-Register initialisieren.
			and	#$7f
			sta	cia1base +15
			lda	#$81
			sta	cia1base +11
			lda	#$00
			sta	cia1base +10
			sta	cia1base + 9
			sta	cia1base + 8

			lda	#$30			;GEOS-RAM aktivieren.
			sta	CPU_DATA

			ldx	#$07			;Sprite-Pointer setzen.
			lda	#$bb
::51			sta	$8fe8,x
			dex
			bpl	:51

			lda	#$bf
			sta	$8ff0

			lda	#%01110000		;Kein MoveData, DiskDriver in REU,
			sta	sysRAMFlg		;ReBoot-Kernal in REU.
			lda	#$ff			;TaskSwitcher deaktivieren (da noch
			sta	Flag_TaskAktiv		;nicht installiert... Erst über
							;MegaEditor!!!)
			jsr	FirstInit		;GEOS initialisieren.

			jsr	SCPU_OptOn		;SCPU aktivieren (auch wenn keine
							;SCPU verfügbar ist!)
			jsr	InitMouse		;Mausabfrage starten (nur temporär
							;notwendig, da gewünschter Treiber
							;erst später geladen wird!)

			lda	#$08			;Sektor-Interleave #8.
			sta	interleave

			LoadB	day  ,03		;Startdatum setzen.
			LoadB	month,10
			LoadB	year ,94

			lda	#$01			;Anzahl Laufwerke löschen.
			sta	numDrives

;*** Laufwerksvariablen initialisieren.
:InitDriveData		ldy	Boot_Drive		;Startlaufwerk aktivieren.
			sty	curDrive

			lda	Boot_Type		;Typ "RAMLink" nach "RAMxy"
			and	#%11110000		;wandeln.
			cmp	#DrvRAMLink		;Startlaufwerk = RAMLink ?
			bne	:51			; => Nein, weiter...

			lda	Boot_Type		;Emulationstyp isolieren und
			and	#%00001111		;RAM-Flag setzen.
			ora	#%10000000
			bne	:52

::51			lda	Boot_Type
::52			sta	curType			;Emulationstyp speichern.
			sta	driveType   -8,y

			lda	Boot_Mode		;Laufwerksmodi speichern.
			sta	RealDrvMode -8,y
			lda	Boot_Type		;Laufwerkstyp speichern.
			sta	RealDrvType -8,y
			and	#%11110000
			cmp	#DrvRAMLink		;Startlaufwerk = RAMLink ?
			bne	:53			; => Nein, weiter...

			lda	Boot_Part   +1		;Bootpartition aktivieren.
			sta	ramBase     -8,y

::53			lda	Boot_Drive		;Startlaufwerk aktivieren. Dabei
			jsr	SetDevice		;werden bei der RAMLink auch die
			jsr	OpenDisk		;Laufwerkstreiber-Variablen gesetzt.

;*** Standard-Gerätetreiber laden.
			jsr	LoadPrnDevice		;Druckertreiber laden.
			jsr	LoadMseDevice		;Eingabetreiber laden.

;*** Konfiguration speichern.
:SaveConfig		LoadW	r6,FileName0		;"GEOS64.BOOT" modifizieren.
			jsr	FindFile
			txa
			bne	:51

			jsr	SaveRamType

::51			LoadW	r6,FileName4		;"RBOOT64.BOOT" modifizieren.
			jsr	FindFile
			txa
			bne	AUTO_INSTALL

			jsr	SaveRamType

;*** AutoBoot-Programme ausführen.
:AUTO_INSTALL		jsr	i_MoveData		;AutoBoot-Routine kopieren.
			w	AutoBoot_a
			w	BASE_AUTO_BOOT
			w	(AutoBoot_b - AutoBoot_a)

			jmp	BASE_AUTO_BOOT		;AutoBoot starten.

;*** Ersten Druckertreiber auf Diskette suchen/laden.
:LoadPrnDevice		lda	#$ff			;Druckername in RAM löschen.
			sta	PrntFileNameRAM

			LoadW	r6 ,PrntFileName
			LoadB	r7L,PRINTER
			LoadB	r7H,$01
			LoadW	r10,$0000
			jsr	FindFTypes		;Druckertreiber suchen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...
			lda	r7H			;Treiber gefunden ?
			bne	:51			; => Nein, Abbruch...

			lda	Flag_LoadPrnt		;Druckertreiber in REU laden ?
			bne	:51			;Nein, weiter...

			LoadB	r0L,%00000001
			LoadW	r6 ,PrntFileName
			LoadW	r7 ,PRINTBASE
			jsr	GetFile			;Druckertreiber laden.
::51			rts

;*** Ersten Maustreiber auf Diskette suchen/laden.
:LoadMseDevice		LoadW	r6 ,inputDevName
			LoadB	r7L,INPUT_DEVICE
			LoadB	r7H,$01
			LoadW	r10,$0000
			jsr	FindFTypes		;Eingabetreiber suchen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Abbruch...
			lda	r7H			;Treiber gefunden ?
			bne	:51			; => Nein, Abbruch...

			LoadB	r0L,%00000001
			LoadW	r6 ,inputDevName
			LoadW	r7 ,MOUSE_BASE
			jsr	GetFile			;Eingabetreiber laden und
			jsr	InitMouse		;initialisieren.
::51			rts

;*** Konfiguration speichern.
:SaveRamType		lda	dirEntryBuf +1		;Ersten Programmsektor einlesen.
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock
			txa				;Diskettenfehler ?
			bne	:52			; => Ja, Abbruch...

::51			lda	ExtRAM_Type   ,x
			sta	diskBlkBuf +14,x
			inx
			cpx	#$05
			bcc	:51

			jsr	PutBlock		;Sektor wieder auf Disk speichern.
::52			rts

;*** Datei "GEOS.1.SYS" nachladen.
:LoadKernal_1		jsr	Strg_LdGEOS_1		;Installationsmeldung ausgeben.

			ldy	#>FileName1
			ldx	#<FileName1
			jmp	SystemFile

;*** Datei "GEOS.2.SYS" nachladen.
:LoadKernal_2		jsr	Strg_LdGEOS_2		;Installationsmeldung ausgeben.

			ldy	#>FileName2
			ldx	#<FileName2
			jmp	SystemFile

;*** Datei "GEOS.3.SYS" nachladen.
:LoadKernal_3		jsr	Strg_LdGEOS_3		;Installationsmeldung ausgeben.

			ldy	#>FileName3
			ldx	#<FileName3

;*** Systemdatei nachladen.
:SystemFile		lda	#$37			;GEOS-Bereich ausblenden.
			sta	CPU_DATA
			lda	#8			;Länge Dateiname = 10 Zeichen.
			jsr	SETNAM			;Dateiname festlegen.

			lda	#$01
			ldx	$ba
			ldy	#$00
			jsr	SETLFS			;Dateiparameter festlegen.

			lda	#$00
			ldx	#<BASE_GEOS_SYS
			ldy	#>BASE_GEOS_SYS
			jsr	LOAD			;Datei laden.
			bcs	ERROR			;Fehler ? Nein, weiter...

			jsr	Strg_OK			;Installationsmeldung ausgeben.

			sei				;IRQ sperren.
			lda	#$30			;GEOS-Bereich einblenden.
			sta	CPU_DATA
			rts

;*** Fehler, zurück zum BASIC.
:ERROR			lda	#$37
			sta	CPU_DATA
			cli

			jsr	Strg_LoadError		;Fehlermeldung ausgeben und Ende.
			jmp	ROM_BASIC_READY

;*** Kernal-Teil #1 installieren.
;    Programmcode liegt ab ":BASE_GEOS_SYS" im Speicher und wird
;    nach $9000-$9C7F und $BF40-$FFFF kopiert.
:InitKernal_1		jsr	Strg_Install_1		;Installationsmeldung ausgeben.

			sei
			lda	#$30			;GEOS-Bereich einblenden.
			sta	CPU_DATA

			LoadW	r0,$8000		;GEOS-Variablen löschen.

			ldx	#$10
			ldy	#$00
			tya
::51			sta	(r0L),y
			iny
			bne	:51
			inc	r0H
			dex
			bne	:51

			LoadW	r0,BASE_GEOS_SYS	;Laufwerkstreiber aus Startdatei
			LoadW	r1,DISK_BASE		;nach $9000 kopieren.

			ldx	#$10
			ldy	#$00
::52			lda	(r0L),y
			sta	(r1L),y
			iny
			bne	:52
			inc	r0H
			inc	r1H
			dex
			bne	:52

			LoadW	r1,$bf40		;GEOS-Kernal aus Startdatei
							;nach $BF40 kopieren.
			ldy	#$00
::53			lda	(r0L),y
			sta	(r1L),y
			iny
			bne	:53
			inc	r0H
			inc	r1H
			lda	r1H
			cmp	#$ff
			bne	:53

			ldy	#$00
::54			lda	(r0L),y
			sta	(r1L),y
			iny
			cpy	#$c0
			bne	:54

			lda	ExtRAM_Type		;RAM-Typ an GEOS übergeben.
			sta	GEOS_RAM_TYP

			lda	ExtRAM_Size		;Größe des ermittelten Speichers
			sta	ramExpSize		;an GEOS übergeben.

			lda	ExtRAM_Bank  +0
			sta	RamBankFirst +0
			lda	ExtRAM_Bank  +1
			sta	RamBankFirst +1

			jmp	Strg_OK			;Installationsmeldung ausgeben.

;*** Kernal-Teil #2 installieren,ReBoot-Routine in REU kopieren.
;    Programmcode liegt ab ":BASE_GEOS_SYS" im Speicher und wird
;    in die Speicherbank #1 kopiert.
;--- Ausgelagerte Kernal-Funktionen in RAM kopieren.
:InitKernal_2		jsr	Strg_Install_2		;Installationsmeldung ausgeben.

			lda	#<MP3_BANK_1
			ldx	#>MP3_BANK_1
			ldy	#$09
			jsr	InitKernel_1_2

;--- ReBoot-Kernal in RAM kopieren.
			jsr	Strg_Install_3		;Installationsmeldung ausgeben.

			ldx	#$00			;Zeiger auf ReBoot-Datentabelle.
			lda	GEOS_RAM_TYP		;RAM-Typ einlesen.
			cmp	#RAM_SCPU		;SuperCPU ?
			beq	:51			;Ja, weiter...
			inx
			inx
			cmp	#RAM_RL			;RAMLink ?
			beq	:51			;Ja, weiter...
			inx
			inx
			cmp	#RAM_REU		;C=REU ?
			beq	:51			;Ja, weiter...
			inx
			inx
			cmp	#RAM_BBG		;BBGRAM ?
			beq	:51			;Ja, weiter...
			ldx	#$00

::51			lda	Vec_ReBoot +0,x		;Startadresse für ReBoot-Routine
			sta	r0L			;in MegaEditor-Programm einlesen.
			lda	Vec_ReBoot +1,x
			sta	r0H

			lda	#$00
			sta	r1L
			sta	r2L
			sta	r3L
			lda	#>R1_ADDR_REBOOT	;Startadresse in REU.
			sta	r1H
			lda	#>R1_SIZE_REBOOT	;Anzahl Bytes.
			sta	r2H
			jsr	StashRAM		;ReBoot-Routine speichern.
			jsr	VerifyRAM
			and	#%00100000
			bne	:52
			jmp	Strg_OK			;Installationsmeldung ausgeben.

::52			jsr	Strg_LoadError		;Fehler beim Speichertransfer.
			jmp	ROM_BASIC_READY		;FEHLER!, Abbruch...

;*** Kernal-Teil #3 installieren.
;    Programmcode liegt ab ":BASE_GEOS_SYS" im Speicher und wird
;    in die Speicherbank #1 kopiert.
:InitKernal_3		jsr	Strg_Install_4		;Installationsmeldung ausgeben.

			lda	#<MP3_BANK_2
			ldx	#>MP3_BANK_2
			ldy	#$06

;*** Programmdaten in Speicherbank #1 kopieren.
;    Übergabe:		AKKU = LowByte -Tabelle,
;			xReg = HighByte-Tabelle,
;			yReg = Anyahl Datenblöcke.
:InitKernel_1_2		sta	:53 +1			;Tabellenzeiger speichern.
			stx	:53 +2
			sty	:54 +1

			lda	#$00			;Kernal-Funktionen in REU
::51			pha				;kopieren.
			asl
			sta	:52 +1
			asl
			clc
::52			adc	#$ff
			tay
			ldx	#$00
::53			lda	$ffff,y			;Zeiger auf Position in Startdatei
			sta	r0L  ,x			;einlesen.
			iny
			inx
			cpx	#$06
			bcc	:53

			lda	MP3_64K_SYSTEM		;Speicherbank festlegen.
			sta	r3L

			jsr	StashRAM		;Daten in REU kopieren.
			jsr	VerifyRAM
			and	#%00100000
			bne	:55

			pla
			clc
			adc	#$01
::54			cmp	#$ff			;Alle Datenblöcke kopiert ?
			bcc	:51			; => Nein, weiter...
			jmp	Strg_OK			;Installationsmeldung ausgeben.

::55			jsr	Strg_LoadError		;Fehler beim Speichertransfer.
			jmp	ROM_BASIC_READY		;FEHLER!, Abbruch...

;*** GEOS an Speichererweiterung anpassen.
:InitDeviceRAM		jsr	Strg_MgrRAM		;Installationsmeldung ausgeben.

;--- SuperCPU.
			lda	ExtRAM_Type
			cmp	#RAM_SCPU		;SuperCPU/RAMCard ?
			bne	:51			;Nein, weiter...

			php				;Externe Funktionen für RAMCard
			sei				;in Anwenderspeicher der SCPU ab
			lda	CPU_DATA		;$D300 kopieren.
			pha
			lda	#$36
			sta	CPU_DATA
			sta	$d07e

			lda	#$00			;SuperCPU-Variablen
			sta	$d27c			;aktualisieren.
			lda	RamBankFirst +1
			clc
			adc	ramExpSize
			sta	$d27d

			ldy	#Code9L -1
::50			lda	Code9a,y
			sta	$d300 ,y
			dey
			cpy	#$ff
			bne	:50

			sta	$d07f
			pla
			sta	CPU_DATA
			plp

			jsr	i_MoveData		;DoRAMOp-Funktionen für SuperCPU.
			w	Code10a
			w	BASE_RAM_DRV
			w	Code10L
			jmp	Strg_OK			;Installationsmeldung ausgeben.

;--- RAMLink.
::51			cmp	#RAM_RL			;RAMLink ?
			bne	:52			;Nein, weiter...

			jsr	i_MoveData		;DoRAMOp-Funktionen für RAMLink.
			w	Code1a
			w	BASE_RAM_DRV
			w	Code1L
			jmp	Strg_OK			;Installationsmeldung ausgeben.

;--- Commodore REU.
::52			cmp	#RAM_REU		;C=REU ?
			bne	:53			;Nein, weiter...

			jsr	i_MoveData		;DoRAMOp-Funktionen für C=REU.
			w	Code2a
			w	BASE_RAM_DRV
			w	Code2L
			jmp	Strg_OK			;Installationsmeldung ausgeben.

;--- GEORAM/BBGRAM.
::53			cmp	#RAM_BBG		;BBGRAM ?
			bne	:54			;Ja, weiter...

			jsr	i_MoveData		;DoRAMOp-Funktionen für BBGRAM.
			w	Code3a
			w	BASE_RAM_DRV
			w	Code3L

			php
			sei
			lda	CPU_DATA
			pha
			lda	#$36
			sta	CPU_DATA

			LoadW	r0,Code4a		;Routine für DoRAMOp-Funktionen
			LoadW	r1,$de00		;installieren.
			lda	#%00000011		;Zeiger auf Seite #254 in REU
			ldx	#%00111110		;aktivieren (Bank #0, $FE00).
			ldy	#Code4L -1
			jsr	CopyData2BBG		;Daten in BBG kopieren.

			pla
			sta	CPU_DATA
			plp
			jmp	Strg_OK			;Installationsmeldung ausgeben.

::54			jsr	Strg_LoadError		;Erweiterung nicht erkannt, Fehler.
			jmp	ROM_BASIC_READY		;Abbruch zum BASIC.

;*** Daten in REU kopieren.
:CopyData2BBG		sta	$dfff
			stx	$dffe
::51			lda	(r0L),y
			sta	(r1L),y
			dey
			cpy	#$ff
			bne	:51
			rts

;*** SuperCPU patchen.
:InitDeviceSCPU		lda	Device_SCPU		;SCPU verfügbar ?
			bne	:51			;=> Ja, weiter...
			rts				;Ende.

::51			jsr	Strg_MgrSCPU		;Installationsmeldung ausgeben.

			ldy	#$00			;SCPU-Patches aktivieren.
::52			lda	Code6a        ,y
			sta	Patch_BASE_2  ,y
			iny
			cpy	#Code6L
			bcc	:52

			lda	#$4c			;Vektor ":InitForIO" verbiegen.
			sta	InitForIO   +0
			lda	#<sInitForIO
			sta	InitForIO   +1
			lda	#>sInitForIO
			sta	InitForIO   +2

			lda	#$4c			;Vektor ":DoneWithIO" verbiegen.
			sta	DoneWithIO  +0
			lda	#<sDoneWithIO
			sta	DoneWithIO  +1
			lda	#>sDoneWithIO
			sta	DoneWithIO  +2

			lda	#<si_MoveData		;Vektor ":i_MoveData" verbiegen.
			sta	i_MoveData  +1
			lda	#>si_MoveData
			sta	i_MoveData  +2

			lda	#<sMoveData		;Vektor ":MoveData" verbiegen.
			sta	MoveData    +1
			lda	#>sMoveData
			sta	MoveData    +2

			ldy	#$00
::54			lda	Code8a        ,y
			sta	Patch_BASE_4  ,y
			iny
			cpy	#Code8L
			bcc	:54
			jmp	Strg_OK			;Installationsmeldung ausgeben.

if 0=1
;*** HD-Kabel deaktivieren. Ist notwendig, da sonst die
;    kommunikation des TurboDOS versagt.
:InitDeviceHD		lda	Device_RL		;RAMLink verfügbar ?
			beq	:52			;=> Nein, weiter...
			jsr	Strg_MgrHD		;Meldung ausgeben und
			jsr	:51			;Kabel abschalten.
			jmp	Strg_OK

::51			ldx	#<AT_P0			;Zeiger auf "P0"-Befehl.
			ldy	#>AT_P0
			stx	a7L
			sty	a7H
			ldx	#$0b			;RL_DOS-Befehlsabfrage.
			pha				;Dummy-Adresse auf Stack speichern
			pha				;und Fehlerroutine aufufen. RL-DOS
			jmp	($0300)			;testet dann auf die erweiterten
::52			rts				;RL-Befehle und führt diesen aus.

;*** Befehl zum deaktivieren des HD-Kabels.
:AT_P0			b $40,$50,$30,$00		;"P0"-Befehl.
endif

;*** Texte ausgeben.
:Strg_Titel		lda	#$00			;BootText00
			b $2c
:Strg_OK		lda	#$01			;BootText01
			b $2c
:Strg_Error		lda	#$02			;BootText02
			b $2c
:Strg_RamExp_Find	lda	#$03			;BootText10
			b $2c
:Strg_RamExp_SCPU	lda	#$04			;BootText11
			b $2c
:Strg_RamExp_RL		lda	#$05			;BootText12
			b $2c
:Strg_RamExp_REU	lda	#$06			;BootText13
			b $2c
:Strg_RamExp_BBG	lda	#$07			;BootText14
			b $2c
:Strg_RamExp_Menu	lda	#$08			;BootText15
			b $2c
:Strg_RamExp_OK		lda	#$09			;BootText16
			b $2c
:Strg_RamExp_Size	lda	#$0a			;BootText18
			b $2c
:Strg_RamExp_DACC	lda	#$0b			;BootText20
			b $2c
:Strg_RamExp_Exit	lda	#$0c			;BootText20
			b $2c
:Strg_RamExp_Auto	lda	#$0d			;BootText21
			b $2c
:Strg_RamExp_Info	lda	#$0e			;BootText22
			b $2c
:Strg_DvInit_Info	lda	#$0f			;BootText30
			b $2c
:Strg_DvInit_RL		lda	#$10			;BootText31
			b $2c
:Strg_LdGEOS_1		lda	#$11			;BootText40
			b $2c
:Strg_LdGEOS_2		lda	#$12			;BootText41
			b $2c
:Strg_LdGEOS_3		lda	#$13			;BootText42
			b $2c
:Strg_Install_1		lda	#$14			;BootText50
			b $2c
:Strg_Install_2		lda	#$15			;BootText51
			b $2c
:Strg_Install_3		lda	#$16			;BootText52
			b $2c
:Strg_Install_4		lda	#$17			;BootText53
			b $2c
:Strg_MgrRAM		lda	#$18			;BootText60
			b $2c
:Strg_MgrSCPU		lda	#$19			;BootText61
			b $2c
;Strg_MgrHD		lda	#$1a			;BootText61
;			b $2c
:Strg_InitGEOS		lda	#$1a			;BootText70
			b $2c
:Strg_LoadError		lda	#$1b			;BootText80
			asl
			tax
			lda	StrgVecTab1 +0,x	;Zeiger auf Text einlesen.
			ldy	StrgVecTab1 +1,x

:Strg_CurText		php				;BASIC-ROM aktivieren.
			sei
			tax
			lda	CPU_DATA
			pha
			lda	#$37
			sta	CPU_DATA
			txa
			jsr	ROM_OUT_STRING		;Text ausgeben.

			lda	TEXT_OUT_DELAY
			bne	:50

			lda	#%01111111
			sta	cia1base + 0
			lda	cia1base + 1		;Tastatur einlesen.
			and	#%00100000		;CBM gedrückt ?
			bne	:54			; => Nein, weiter...
			dec	TEXT_OUT_DELAY

::50			ldx	#$20			;Warteschleife...
::51			lda	$d012
::52			cmp	$d012
			beq	:52
::53			cmp	$d012
			bne	:53
			dex
			bne	:51

::54			pla
			sta	CPU_DATA
			plp
			rts

;*** Boot-Meldungen einbinden.
			t "-G3_PrntBoot2"

;******************************************************************************
;*** Der folgende Datenbereich wird auch von "GEOS64.MP3" mitverwendet.
;*** Der Datenbereich wird dazu von "GEOS64.MP3" nachgeladen.
;******************************************************************************
.S_KernelData
;******************************************************************************

;*** MegaPatch-Kernal-Routinen.
;    Word = Zeiger auf Programmcode.
;    Word = Zeiger auf Adr. in REU.
;    Word = Anzahl Bytes.
;    Word = Zeiger auf Bankadresse.

;*** Zeiger auf ReBoot-Routinen.
.Vec_ReBoot		w ReBoot_SCPU
			w ReBoot_RL
			w ReBoot_REU
			w ReBoot_BBG

.MP3_BANK_1		w x_EnterDeskTop
			w R2_ADDR_ENTER_DT
			w R2_SIZE_ENTER_DT

			w x_GetNextDay
			w R2_ADDR_GETNXDAY
			w R2_SIZE_GETNXDAY

			w x_DoAlarm
			w R2_ADDR_DOALARM
			w R2_SIZE_DOALARM

			w x_PanicBox
			w R2_ADDR_PANIC
			w R2_SIZE_PANIC

			w x_ToBASIC
			w R2_ADDR_TOBASIC
			w R2_SIZE_TOBASIC

			w x_GetFiles
			w R2_ADDR_GETFILES
			w R2_SIZE_GETFILES

			w x_GetFilesData
			w R2_ADDR_GFILDATA
			w R2_SIZE_GFILDATA

			w x_GetFilesIcon
			w R2_ADDR_GFILMENU
			w R2_SIZE_GFILMENU

			w x_ClrDlgScreen
			w R2_ADDR_DB_SCREEN
			w R2_SIZE_DB_SCREEN

.MP3_BANK_2		w x_TaskSwitch
			w R2_ADDR_TASKMAN_B
			w R2_SIZE_TASKMAN

			w x_ScreenSaver
			w R2_ADDR_SCRSAVER
			w R2_SIZE_SCRSAVER

			w x_GetBackScrn
			w R2_ADDR_GETBSCRN
			w R2_SIZE_GETBSCRN

			w x_SpoolMenu
			w R2_ADDR_SPOOLER
			w R2_SIZE_SPOOLER

			w x_SpoolPrint
			w R2_ADDR_PRNSPOOL
			w R2_SIZE_PRNSPOOL

			w x_Register
			w R2_ADDR_REGISTER
			w R2_SIZE_REGISTER

;******************************************************************************
;*** Der folgende Datenbereich wird auch von "GEOS64.MP3" mitverwendet.
;*** Der Datenbereich wird dazu von "GEOS64.MP3" nachgeladen.
;******************************************************************************
;*** Programmcodes.
.AutoBoot_a		d "obj.AUTO.BOOT64"		;AutoBoot-Routine.
.AutoBoot_b

.Code1a			d "obj.DvRAM_RL"		;RAM-Treiber für CMD.
.Code1b
.Code1L			= (Code1b - Code1a)

.Code2a			d "obj.DvRAM_REU"		;RAM-Treiber für REU.
.Code2b
.Code2L			= (Code2b - Code2a)

.Code3a			d "obj.DvRAM_BBG.1"		;RAM-Treiber für BBGRAM.
.Code3b
.Code3L			= (Code3b - Code3a)

.Code4a			d "obj.DvRAM_BBG.2"		;RAM-Treiber für BBGRAM.
.Code4b
.Code4L			= (Code4b - Code4a)

.Code6a			d "obj.Patch_SCPU"		;Patches für SCPU.
.Code6b
.Code6L			= (Code6b - Code6a)

.Code8a			jmp	sSCPU_OptOn
			jmp	sSCPU_OptOff
			jmp	sSCPU_SetOpt
.Code8b
.Code8L			= (Code8b - Code8a)

.Code9a			d "obj.Patch_SRAM"		;Patches für RAMCard.
.Code9b
.Code9L			= (Code9b - Code9a)

.Code10a		d "obj.DvRAM_SCPU"		;RAM-Treiber für SuperCPU.
.Code10b
.Code10L		= (Code10b - Code10a)

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
.E_KernelData		g BASE_GEOS_SYS
;******************************************************************************

;******************************************************************************
;*** ACHTUNG!
;*** Alle folgenden Routinen werden beim Start teilweise überschrieben!
;******************************************************************************
			t "-G3_PrntBootInf"
			t "-G3_DetectRAM"
			t "-G3_GetRLPEntry"
;******************************************************************************

;*** Speichererweiterung suchen.
:FindRamExp		jsr	Strg_RamExp_Find	;Installationsmeldung ausgeben.

			jsr	Check_RAMCard		;Nach RAMCard suchen und
			jsr	RamEx_SetData		;Test-Ergebnis speichern.

			jsr	Check_RAMLink		;Nach RAMLink suchen und
			jsr	RamEx_SetData		;Test-Ergebnis speichern.

			jsr	Check_REU		;Nach REU suchen und
			jsr	RamEx_SetData		;Test-Ergebnis speichern.

			jsr	Check_BBG		;Nach BBG suchen und
			jsr	RamEx_SetData		;Test-Ergebnis speichern.

			jsr	RamEx_Select		;RAM-Erweiterung wählen.

;*** Informtionen über Speichererweiterung ausgeben.
:RamEx_Info		jsr	Strg_RamExp_OK		;Installationsmeldung ausgeben.

			lda	ExtRAM_Name +0		;Verwendete Speichererweiterung
			ldy	ExtRAM_Name +1		;ausgeben.
			jsr	Strg_CurText

;--- RAMLink ? Ja, DACC-Partition wählen.
			lda	BOOT_RAM_TYPE		;Erst-Start von MP3 ?
			bne	:51			; => Nein, weiter...

			lda	ExtRAM_Type
			cmp	#RAM_RL			;RAMLink installieren ?
			bne	:51			; => Nein, weiter...
			lda	ExtRAM_Part		;Mehr als eine DACC-Partition ?
			bpl	:51			; => Nein, weiter...

			jsr	Strg_RamExp_DACC	;RAMLink-DACC-Partition wählen.
			jmp	RamEx_GetRLPart

;--- Gewählten RAM-Typ und RAM-Größe ausgeben.
::51			lda	#CR
			jsr	BSOUT

			jsr	PrntSpace
			jsr	PrntSpace

			lda	ExtRAM_Size		;Größe des erweiterten
			jsr	PrintSizeExtRAM		;Speichers ausgeben.
			jmp	Strg_RamExp_Size	;Installationsmeldung ausgeben.

;*** Daten für Speichererweiterung setzen.
;    Übergabe:		xReg = $00, RAMUNIT verfügbar.
;			yReg = RAM-Größe.
:RamEx_SetData		txa
			bne	:51

			inc	RAMUNIT_COUNT		;Anzahl RAM_UNITs +1.

			tya
			jsr	PrntRSizeExtRAM
			jsr	PrntDoublePoint
			jmp	Strg_OK			;Installationsmeldung ausgeben.

::51			lda	#$00			;RAM nicht verfügbar.
			jsr	PrntRSizeExtRAM
			jsr	PrntDoublePoint
			jmp	Strg_Error		;Installationsmeldung ausgeben.

;*** Größe des Speichers ausgeben.
:RamEx_Select		lda	RAMUNIT_COUNT		;Anzahl RAM_UNITs ?
			beq	Check_NoRAM		; => Kein RAM, Ende...

			cmp	#$01			;Mehr als eine RAM-Unit ?
			bne	RamEx_Menu		; => Ja, Auswahlmenü.

			ldy	#$00			;Daten der verfügbaren
::51			ldx	#$00			;Speichererweiterung suchen.
::52			lda	RAMUNIT_SCPU,y
			sta	ExtRAM_Type ,x
			iny
			inx
			cpx	#$07
			bcc	:52

			lda	ExtRAM_Type		;Daten gefunden ?
			beq	:53			; => Nein, weiter...
			rts

::53			cpy	#4*7
			bcc	:51

;*** Keine Speichererweiterung gefunden.
:Check_NoRAM		jsr	Strg_RamExp_Exit	;Installationsmeldung ausgeben.
			jmp	ROM_BASIC_READY		;Zurück zum C64-BASIC.

;*** Auswahlmenü für Speichererweiterung ausgeben.
:RamEx_Menu		ldx	BOOT_RAM_TYPE		;Installationsautomatik ?
			beq	RamEx_SlctRAM		; => Nein, weiter...

			ldy	#$00			;Daten für Vorinstallierte
::51			ldx	#$00			;RamUnit einlesen.
::52			lda	RAMUNIT_SCPU,y
			sta	ExtRAM_Type ,x
			iny
			inx
			cpx	#$07
			bcc	:52

			lda	BOOT_RAM_TYPE
			cmp	ExtRAM_Type		;RamUnit gefunden ?
			beq	:53			; => Ja, weiter...

			cpy	#4*7			;Alle RamUnits durchsucht ?
			bcc	:51			; => Nein, weiter...
			jmp	RamEx_SlctRAM		;RAM nicht mehr verfügbar,
							;Auswahlmenu darstellen.

;--- Autoselect-Daten auf Gültigkeit testen.
::53			lda	BOOT_RAM_TYPE		;Vorinstallierte RamUnit vom
			cmp	#RAM_RL			;Typ RAMLink ?
			bne	:54			; => Nein, weiter...

			lda	BOOT_RAM_PART		;Ist Partition ausgewählt ?
			beq	RamEx_SlctRAM		; => Nein, Menü starten.
			sta	r3H
			jsr	GetRLPartEntry		;Partitionsdaten einlesen.

			lda	dirEntryBuf +0
			cmp	#$07			;Partition = DACC ?
			bne	RamEx_SlctRAM		; => Nein, Menü starten.

			lda	dirEntryBuf +21		;Partitionsdaten für
			sta	ExtRAM_Bank +0		;GEOS-DACC definieren.
			lda	dirEntryBuf +20
			sta	ExtRAM_Bank +1

			lda	dirEntryBuf +28
			cmp	#RAM_MAX_SIZE
			bcc	:53a
			lda	#RAM_MAX_SIZE
::53a			sta	ExtRAM_Size

			lda	r3H
			sta	ExtRAM_Part

::54			jmp	Strg_RamExp_Auto	;Installationsmeldung ausgeben.

;*** Auswahlmenü anzeigen.
:RamEx_SlctRAM		jsr	Strg_RamExp_Menu	;Menü ausgeben.

			cli
::51			jsr	GETIN			;Auf Taste 1/2/3/4 warten.
			tax				;Taste gedrückt ?
			beq	:51			; => Nein, weiter...

			cpx	#"1"			;Gültige Taste gedrückt ?
			bcc	:52			; => Nein, warten auf gültige Taste.
			cpx	#"5"
			bcs	:52			; => Nein, warten auf gültige Taste.

			txa				;Daten der gewählten
			sec				;Speichererweiterung einlesen.
			sbc	#$31
			sta	:52 +1
			asl
			asl
			asl
			sec
::52			sbc	#$ff
			tay
::53			ldx	#$00
::54			lda	RAMUNIT_SCPU,y
			sta	ExtRAM_Type ,x
			iny
			inx
			cpx	#$07
			bcc	:54

			lda	ExtRAM_Type		;Ist RamUnit verfügbar ?
			beq	:51			; => Nein, Taste ungültig...
			sei
			rts

;*** Größe des RAMs rechtsbündig ausgeben.
;    Übergabe:		AKKU = Anzahl RAM-Bänke.
:PrntRSizeExtRAM	sta	r0H

::51			lda	$d3			;Cursor positionieren.
			cmp	#22
			bcs	:52

			lda	#"."
			jsr	BSOUT

			jmp	:51

::52			lda	#"("			;Größe des DACC rechtsbündig
			jsr	BSOUT			;ausgeben.

			lda	r0H
			cmp	#255
			beq	:61

			cmp	#1
			bcs	:53
			jsr	PrntSpace

::53			CmpBI	r0H,2
			bcs	:54
			jsr	PrntSpace

::54			CmpBI	r0H,16
			bcs	:55
			jsr	PrntSpace

::55			CmpBI	r0H,157
			bcs	:56
			jsr	PrntSpace

::56			lda	r0H
			jsr	PrintSizeExtRAM
			lda	#")"
			jmp	BSOUT

::61			lda	#<RAM_RL_MENU_TXT
			ldy	#>RAM_RL_MENU_TXT
			jmp	ROM_OUT_STRING		;Text ausgeben.

;*** Trennzeichen ausgeben.
:PrntDoublePoint	lda	#":"
			jmp	BSOUT
:PrntSpace		lda	#" "
			jmp	BSOUT

;*** Größe des RAMs ausgeben.
;    Übergabe:		AKKU = Anzahl RAM-Bänke.
:PrintSizeExtRAM	ldx	#$00			;Größe der RAM-UNIT ausgeben.
			stx	r0L
			ldx	#$06
::51			asl
			rol	r0L
			dex
			bne	:51
			tax
			lda	r0L
			jsr	ROM_OUT_NUMERIC

			jsr	PrntSpace

			lda	#"K"
			jsr	BSOUT
			lda	#"B"
			jmp	BSOUT

;*** Auswahlmenü für RAMLink-DACC-Partition.
:RamEx_GetRLPart	lda	#$00
			sta	r3H
::51			jsr	GetRLPartNext		;Partitionsdaten einlesen.
			jsr	PrintPartName		;Partitionsname ausgeben.

			cli
::52			jsr	GETIN			;Auf Taste warten.
			tax
			beq	:52
			cpx	#" "			;Gültige Taste gedrückt ?
			beq	:51			; => Ja, nächste Partition.
			cpx	#CR
			bne	:52			; => Nein, warten auf gültige Taste.

			lda	dirEntryBuf +28
			cmp	#RAM_MAX_SIZE		;Größer als 4 MByte ?
			bcc	:53			; => Nein, weiter...
			lda	#RAM_MAX_SIZE		;Größe DACC auf 4 MByte begrenzen.
::53			sta	ExtRAM_Size

			ldx	dirEntryBuf +21		;Startadresse DACC-Partition
			stx	ExtRAM_Bank +0		;definieren.
			ldx	dirEntryBuf +20
			stx	ExtRAM_Bank +1

			lda	r3H			;Partitions-Nr. merken.
			sta	ExtRAM_Part
			sei
			rts

;*** Nächste DACC-Partition suchen.
:GetRLPartNext		inc	r3H			;Zeiger auf nächste Partition.
			CmpBI	r3H,32			;Letzte Partition erreicht ?
			bcc	:51			; => Nein, weiter...
			LoadB	r3H,1			;Zeiger auf erste Partition.
::51			jsr	GetRLPartEntry		;Partitionsdaten einlesen.

			lda	dirEntryBuf +0
			cmp	#$07			;DACC-Partition ?
			bne	GetRLPartNext		; => Nein, weitersuchen.

			lda	dirEntryBuf +28
			cmp	#$03			;Partitionsgröße ausreichend ?
			bcc	:51			; => Nein, weiter...
			rts

;*** Gewählte Partition anzeigen.
:PrintPartName		lda	#24			;Cursor positionieren.
			sta	$d6
			lda	#02
			sta	$d3

			lda	#"P"			;Partitions-Nr. ausgeben.
			jsr	BSOUT

			CmpBI	r3H,10
			bcs	:51
			lda	#"0"
			jsr	BSOUT

::51			ldx	r3H
			lda	#$00
			jsr	ROM_OUT_NUMERIC

			jsr	PrntSpace

			ldy	#$00			;Partitionsname ausgeben.
::52			lda	dirEntryBuf +3,y
			cmp	#$a0
			beq	:53
			jsr	BSOUT
			iny
			cpy	#$10
			bcc	:52

::53			lda	dirEntryBuf +28		;Partitionsgröße ausgeben.
			jmp	PrntRSizeExtRAM

;*** Auf RAMCard testen.
:Check_RAMCard		jsr	Strg_RamExp_SCPU	;Installationsmeldung ausgeben.
			jsr	DetectRAMCard
			txa
			bne	:55

			lda	$d27f			;Installiertes RAM ermitteln.
			beq	:55			; => Kein RAM, weiter...

			ldx	$d27d			;Begint erste Speicherbank bei
			ldy	$d27c			;Seite #0 ?
			beq	:53			; => Ja, weiter...
			inx				; => Nein, Start mit nächster Bank.
::53			stx	r0L			;Größe des verfügbaren RAMs
			sec				;berechnen.
			sbc	r0L
			tay

			cmp	#$03			;Mind. drei Speicherbänke frei ?
			bcc	:55			; => Kein RAM, weiter...
			cmp	#RAM_MAX_SIZE		;Mehr RAM als erforderlich ?
			bcc	:54			; => Nein, weiter...
			lda	#RAM_MAX_SIZE		;RAM begrenzen.
::54			sta	RAMSIZE_SCPU		;Größe SuperRAM speichern.

			lda	#$00
			sta	RAMBANK_SCPU +0
			lda	r0L
			sta	RAMBANK_SCPU +1

;--- SuperCPU mit RAMCard gefunden.
			lda	#RAM_SCPU		;Kennung & Größe für SCPU speichern.
			sta	RAMUNIT_SCPU
			ldx	#NO_ERROR
			rts

::55			ldx	#DEV_NOT_FOUND
			rts

;*** Auf RAMLink testen.
:Check_RAMLink		jsr	Strg_RamExp_RL		;Installationsmeldung ausgeben.
			jsr	DetectRAMLink
			txa
			bne	:51

			jsr	GetSizeRAM_RL		; => Ja, RAMLink-Speicher testen.

			cpy	#$00			;Speicher in RAMLink installiert ?
			beq	:51			; => Nein, weiter...
			ldx	#NO_ERROR
			rts

::51			ldx	#DEV_NOT_FOUND
			rts

;*** Größe des RAMLink-Speichers ermitteln.
:GetSizeRAM_RL		ldx	#$00
			stx	r3L
			inx
			stx	r3H

::51			jsr	GetRLPartEntry

			lda	dirEntryBuf +0
			cmp	#$07
			bne	:53

			lda	dirEntryBuf +28
			cmp	#$03
			bcc	:53
			sta	RAMSIZE_RL

			ldx	dirEntryBuf +21
			stx	RAMBANK_RL  +0
			ldx	dirEntryBuf +20
			stx	RAMBANK_RL  +1

			lda	r3H
			sta	RAMPART_RL

			inc	r3L
::53			inc	r3H
			CmpBI	r3H,32
			bcc	:51

			dec	r3L
			beq	:54
			lda	#$ff
			tay
			sty	RAMPART_RL
			bne	:56

::54			lda	RAMSIZE_RL
			tay
			beq	:56
			cmp	#RAM_MAX_SIZE		;Größer als 4 MByte ?
			bcc	:55			; => Nein, weiter...
			lda	#RAM_MAX_SIZE		;Größe DACC auf 4 MByte begrenzen.
::55			sta	RAMSIZE_RL

::56			ldx	#RAM_RL
			stx	RAMUNIT_RL		;RAM-Typ #RAMLink.
			rts				;Ende...

;*** Auf C=REU/CMD-REU testen.
:Check_REU		jsr	Strg_RamExp_REU		;Installationsmeldung ausgeben.
			jsr	DetectREU
			txa
			bne	:51

			jsr	GetSizeRAM_REU		;Test auf REU.

			cpy	#$00			;RAM gefunden ?
			beq	:51			; => Nein, weiter...
			ldx	#NO_ERROR
			rts

::51			ldx	#DEV_NOT_FOUND
			rts

;*** Größe des C=REU-Speichers ermitteln.
:GetSizeRAM_REU		ldx	#$00			;Zeiger auf Bank #0.
::55			stx	r0L			;Aktuelle Bank-Adresse speichern.

			jsr	DoREU_Read		;Testbyte einlesen.

			ldx	r0L			;Bank-Adresse einlesen,
			lda	diskBlkBuf		;Testbyte einlesen und in
			sta	fileHeader,x		;Zwischenspeicher kopieren.
			inx				;Alle Testbytes #0-#255 ausgelesen ?
			bne	:55			; => Nein, weiter...

;--- Prüfbytes (Bank-Adresse) in jede Bank ab Byte #0 speichern.
			dex				;Zeiger auf letzte Speicherbank.
::56			stx	r0L			;Bank-Adresse speichern.
			stx	diskBlkBuf		;Prüfbyte speichern.

			jsr	DoREU_Write		;Prüfbyte in REU übertragen.

			ldx	r0L			;Bank-Adresse einlesen und
			dex				;Zeiger auf nächste Bank setzen.
			cpx	#$ff			;Alle Bänke bearbeitet ?
			bne	:56			; => Nein, weiter...

			inx				;Zeiger auf Bank #0.
::57			stx	r0L			;Bank-Adresse speichern.

			jsr	DoREU_Read		;Prüfbyte aus REU einlesen.

			ldx	r0L			;Bank-Adresse einlesen und mit
			cpx	diskBlkBuf		;Prüfbyte vergleichen.
			bne	:58			; => Fehler, REU-Größe erkannt.
			inx				;Alle Bänke überprüft ?
			bne	:57			; => Nein, weiter...
			dex				;Max. 255x64K adressieren.
::58			stx	RAMSIZE_REU		;REU-Größe speichern.

;--- Original-Inhalt der REU wiederherstellen.
			ldx	#$ff			;Zeiger auf letzte Speicherbank.
::59			stx	r0L			;Bank-Adresse speichern.
			lda	fileHeader,x		;Testbyte einlesen und in
			sta	diskBlkBuf		;Zwischenspeicher übertragen.

			jsr	DoREU_Write		;Testbyte zurück in die REU.

			ldx	r0L			;Bank-Adresse einlesen.
			dex				;Zeiger auf nächste Bank setzen.
			cpx	#$ff			;Alle Bänke bearbeitet ?
			bne	:59			; => Nein, weiter...

;--- REU erkannt, Ende...
			lda	RAMSIZE_REU		;RAM-Größe <> 0Kb ?
			tay				; => Ja, weiter...
			beq	:62
::60			cmp	#RAM_MAX_SIZE		;Größer als 4 MByte ?
			bcc	:61			; => Nein, weiter...
			lda	#RAM_MAX_SIZE		;Größe DACC auf 4 MByte begrenzen.
::61			sta	RAMSIZE_REU

			ldx	#RAM_REU		;RAM-Typ festlegen.
			stx	RAMUNIT_REU
::62			rts

;*** REU-Job ausführen.
:DoREU_Read		lda	#%10110001
			b $2c
:DoREU_Write		lda	#%10110000
:DoREU_Job		pha
			ldx	#$00
			ldy	#$01
			stx	$df02
			lda	#>diskBlkBuf
			sta	$df03
			stx	$df04
			stx	$df05
			lda	r0L
			sta	$df06
			sty	$df07
			stx	$df08
			stx	$df09
			stx	$df0a
			pla
			sta	$df01
::51			lda	$df00
			and	#%01100000
			beq	:51
			rts

;*** Auf GeoRAM/BBGRAM testen.
:Check_BBG		jsr	Strg_RamExp_BBG		;Installationsmeldung ausgeben.
			jsr	DetectBBG
			txa
			bne	:51

			jsr	GetSizeRAM_BBG		;Test auf BBG/GeoRAM.

			cmp	#$00			;RAM gefunden ?
			beq	:51			; => Nein, weiter...
			ldx	#NO_ERROR
			rts

::51			ldx	#DEV_NOT_FOUND
			rts

;*** Größe des BBG-Speichers ermitteln.
:GetSizeRAM_BBG		lda	#$00			;Zeiger auf Seite #0 in BBGRAM.
			sta	r15L
			sta	r15H
			jsr	SetBBG_REC

			ldy	#$00			;Sektorinhalt zwischenspeichern.
::51			lda	$de00,y
			sta	diskBlkBuf,y
			iny
			bne	:51
::52			tya				;Test-Sektor erstellen.
			sta	$de00,y
			sta	fileHeader,y
			iny
			bne	:52
			sta	$de80
			sta	fileHeader+$80

			jsr	TestBBG_Page		;Test-Sektor vergleichen.
			txa				;Übereinstimmung ?
			beq	:54			; => Ja, RAM vorhanden...
			ldy	#$00			;Kein RAM.
			rts

::53			jsr	SetBBG_REC		;Seitenadresse bestimmen.
			jsr	TestBBG_Page		;Test-Sektor vergleichen.
			txa				;Übereinstimmung ?
			beq	:56			; => Ja, RAM-Ende erreicht.

::54			inc	r15L			;Zeiger auf nächste Seite.
			bne	:55
			inc	r15H
::55			jmp	:53			;Weitertesten.

::56			lda	#$00			;Zeiger auf erste Seite
			tax				;zurücksetzen und Test-Sektor wieder
			jsr	SetBBG_RECa		;aus RAM löschen.

			ldy	#$00
::57			lda	diskBlkBuf,y
			sta	$de00,y
			iny
			bne	:57

			lda	r15H
			tay
			beq	:59
			cmp	#RAM_MAX_SIZE		;Max. Größe für BBGRAM erreicht ?
			bcc	:58			; => Nein, weiter...
			lda	#RAM_MAX_SIZE		;Größe der BBGRAM begrenzen.
::58			sta	RAMSIZE_BBG

			ldx	#RAM_BBG
			stx	RAMUNIT_BBG		;RAM-Typ #BBG.
::59			rts

;*** REU-Vektoren definieren.
:SetBBG_REC		lda	r15L
			ldx	r15H
:SetBBG_RECa		pha
			and	#%00111111
			sta	r0L
			pla
			sta	r0H
			txa
			ldx	#$06
::51			lsr
			ror	r0H
			dex
			bne	:51
			lda	r0L
			ldx	r0H
			sta	$dffe
			stx	$dfff
			rts

;*** BBG-Speicherseite vergleichen.
:TestBBG_Page		ldx	#$00
::51			lda	$de00,x
			cmp	fileHeader,x
			bne	:52
			inx
			bne	:51
			rts

::52			ldx	#$ff
			rts

;*** RAMLink-Startpartition suchen.
:FindRL_Part		jsr	Strg_DvInit_Info	;Installationsmeldung ausgeben.

			lda	Boot_Type
			and	#%11110000		;CMD-Geräte-Daten isolieren.
			cmp	#DrvRAMLink		;CMD-RAMLink ?
			beq	:51			; => Ja, weiter...
			rts

::51			jsr	Strg_DvInit_RL		;Installationsmeldung ausgeben.

			php
			sei
			lda	CPU_DATA
			pha
			lda	#$37
			sta	CPU_DATA

			jsr	GetPartInfo		;Daten der aktiven Partition
							;einlesen.
			pla
			sta	CPU_DATA
			plp

;			lda	GP_Data   +21		;RAM-Startadresse speichern.
;			sta	Boot_Part + 0		;HighByte reicht, da der MP3-RL-
			lda	GP_Data   +20		;Treiber die Adresse selbst
			sta	Boot_Part + 1		;ermittelt!

			ldx	GP_Data   + 2		;Partitions-Nr. ausgeben.
			stx	Boot_Part + 0
			lda	#$00
			jmp	ROM_OUT_NUMERIC

;*** Daten an Floppy senden.
:GetPartInfo		lda	#$00
			sta	STATUS			;Status löschen.

			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.
			lda	RL_BootAddr
			jsr	LISTEN			;LISTEN-Signal auf IEC-Bus senden.
			lda	#$ff
			jsr	SECOND			;Sekundär-Adr. nach LISTEN senden.

			bit	STATUS			;Laufwerk vorhanden ?
			bmi	:52			;Nein, Abbruch...

			ldy	#$00
::51			lda	GP_Befehl,y		;Byte aus Speicher
			jsr	CIOUT			;lesen & ausgeben.
			iny
			cpy	#$05
			bne	:51

			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.
			jmp	ReadPartInfo

::52			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.
			ldx	#$ff			;Flag: "Fehler!"
			rts

;*** Daten von Floppy empfangen.
:ReadPartInfo		lda	#$00
			sta	STATUS			;Status löschen.

			jsr	UNTALK			;UNTALK-Signal auf IEC-Bus senden.

			lda	RL_BootAddr
			jsr	TALK			;TALK-Signal auf IEC-Bus senden.
			lda	#$ff
			jsr	TKSA			;Sekundär-Adresse nach TALK senden.

			bit	STATUS			;Laufwerk vorhanden ?
			bmi	:52			;Nein, Abbruch...

			ldy	#$00
::51			jsr	ACPTR			;Byte einlesen und in
			sta	GP_Data,y		;Speicher schreiben.
			iny
			cpy	#31
			bne	:51

			jsr	UNTALK			;UNTALK-Signal auf IEC-Bus senden.
			ldx	#$00			;Flag: "Kein Fehler!"
			rts
::52			jsr	UNTALK			;UNTALK-Signal auf IEC-Bus senden.
			ldx	#$ff			;Flag: "Fehler!"
			rts

;*** RAM-Erkennung.
:RAMUNIT_COUNT		b $00

:RAMUNIT_SCPU		b $00				;$00 = RAMCARD nicht verfügbar.
:RAMSIZE_SCPU		b $00				;DACC-Größe.
:RAMBANK_SCPU		w $0000				;DACC-Startadresse.
:RAMPART_SCPU		b $00				;Dummy-Byte.
:RAMNAME_SCPU		w BootText11

:RAMUNIT_RL		b $00				;$00 = RAMLink nicht verfügbar.
:RAMSIZE_RL		b $00				;DACC-Größe.
:RAMBANK_RL		w $0000				;DACC-Startadresse.
:RAMPART_RL		b $00				;DACC-Partition.
:RAMNAME_RL		w BootText12

:RAMUNIT_REU		b $00				;$00 = C=REU nicht verfügbar.
:RAMSIZE_REU		b $00				;DACC-Größe.
:RAMBANK_REU		w $0000				;DACC-Startadresse.
:RAMPART_REU		b $00				;Dummy-Byte.
:RAMNAME_REU		w BootText13

:RAMUNIT_BBG		b $00				;$00 = BBG-RAM nicht verfügbar.
:RAMSIZE_BBG		b $00				;DACC-Größe.
:RAMBANK_BBG		w $0000				;DACC-Startadresse.
:RAMPART_BBG		b $00				;Dummy-Byte.
:RAMNAME_BBG		w BootText14

:RAM_RL_MENU_TXT	b " -MENU- )",0
