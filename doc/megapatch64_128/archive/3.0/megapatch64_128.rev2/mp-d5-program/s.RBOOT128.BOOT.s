﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "RBOOT128.BOOT"
			t "G3_SymMacExt"
			t "G3_V.Cl.128.Boot"

			o BASE_GEOSBOOT -2		;BASIC-Start beachten!
			p InitBootProc

			z $40
			i
<MISSING_IMAGE_DATA>

if Sprache = Deutsch
			h "ReBoot-Programm / System"
			h "GEOS-MegaPatch 128..."
endif

if Sprache = Englisch
			h "ReBoot-Programm / System"
			h "GEOS-MegaPatch 128..."
endif

;*** Boot-Informationen einbinden.
			t "-G3_BootData"

; Dieses Programm befindet sich sowohl in Bank 0 alsauch in Bank 1
; Beachte geänderte Variablen beim Bankwechsel !
;*** Hardware erkennen.
:MainInit		sei				;IRQ sperren.
			cld				;Dezimal-Flag löschen.
			ldx	#$ff			;Stack-Pointer löschen.
			txs

			LoadB	RAM_Conf_Reg,$07	;Common Area $0000 - $4000 aktiv
			lda	#%00001110		;ROM ab $c000 aktiv + IO
			sta	MMU

			jsr	PrintBootInfo

			ldx	#$ff			;SuperCPU verügbar ?
			lda	$d0bc
			bpl	:51
			inx
::51			stx	Device_SCPU

			ldx	#$ff			;RAMLink verügbar ?
			lda	$e0a9
			cmp	#$78
			beq	:52
			inx
::52			stx	Device_RL

			lda	#$7e
			sta	MMU

			lda	BOOT_RAM_SIZE
			sta	ramExpSize
			beq	NoRAMfound

			lda	BOOT_RAM_BANK +0
			sta	RamBankFirst  +0
			lda	BOOT_RAM_BANK +1
			sta	RamBankFirst  +1

			lda	#%00001110		;ROM ab $c000 aktiv + IO
			sta	MMU

;--- RAMCard.
			lda	BOOT_RAM_TYPE		;Speichererweiterung definiert ?
			beq	NoRAMfound		; => Nein, Abbruch...
			cmp	#RAM_SCPU		;RAMCard für RBoot verwenden ?
			bne	:54			; => Nein, weiter...

			jsr	DetectRAMCard		;Speichererweiterung testen.
			txa				;RAMCard installiert ?
			bne	NoRAMfound		; => Nein, Abbruch...

			sta	$d07e

			lda	#$7e
			sta	MMU

			lda	RamBankFirst +1		;SuperCPU-Variablen aktualisieren.
			clc				;Neues Ende des belegten RAMs nur
			adc	ramExpSize		;dann setzen, wenn Ende unterhalb
			cmp	$d27d			;GEOS-DACC liegt. Passiert nur bei
			bcc	:53			;einem Warmstart des Computers.

			ldx	#$00
			stx	$d27c
			sta	$d27d
::53			sta	$d07f
			jmp	ReBoot_SCPU		;ReBoot für SCPU/RAMCard.

;--- RAMLink.
::54			cmp	#RAM_RL			;RAMLink für RBoot verwenden ?
			bne	:55			; => Nein, weiter...

			jsr	DetectRAMLink		;Speichererweiterung testen.
			txa				;RAMLink installiert ?
			bne	NoRAMfound		; => Nein, Abbruch...
			jmp	ReBoot_RL		;ReBoot für SCPU/RAMCard.

;--- REU.
::55			cmp	#RAM_REU		;REU für RBoot verwenden ?
			bne	:56			; => Nein, weiter...

			jsr	DetectREU		;Speichererweiterung testen.
			txa				;REU installiert ?
			bne	NoRAMfound		; => Nein, Abbruch...
			jmp	ReBoot_REU		;ReBoot für SCPU/RAMCard.

;--- BBGRAM.
::56			cmp	#RAM_BBG		;BBGRAM für RBoot verwenden ?
			bne	NoRAMfound		; => Nein, Abbruch...

			jsr	DetectBBG		;Speichererweiterung testen.
			txa				;BBGRAM installiert ?
			bne	NoRAMfound		; => Nein, Abbruch...
			jmp	ReBoot_BBG		;ReBoot für SCPU/RAMCard.

;*** Keine Speichererweiterung, Ende...
:NoRAMfound		lda	#$00			;Standard-RAM-Bereiche einblenden.
			sta	MMU
			cli

			jsr	Strg_RamExp_Exit	;Installationsmeldung ausgeben.
			jmp	ROM_BASIC_READY		;Zurück zum C64-BASIC.

;*** ReBoot-Routine für RAMCard.
:ReBoot_SCPU		LoadB	RAM_Conf_Reg,$00
			jsr	SetArea1
			jsr	GetRAM_SCPU
			jsr	SetArea2
			jsr	GetRAM_SCPU
			jmp	$c000			;ReBoot initialisieren.

;*** FetchRAM-Routine für ReBoot.
:GetRAM_SCPU		lda	RamBankFirst +1		;Speicherbank mit GEOS
			sta	:51 +2			;Kernel festlegen.

			b $18				;clc
			b $fb				;xce
			b $c2,$30			;rep #$30

			b $a5,$06			;lda $0006
			b $3a				;dea
			b $a6,$04			;ldx $0002
			b $a4,$02			;ldy $0004
			b $8b				;phb
::51			b $54,$00,$00			;mvn $00.$00
			b $ab				;plb
			b $38				;sec
			b $fb				;xce
			rts

;*** ReBoot-Routine für RAMLink.
:ReBoot_RL		lda	BOOT_RAM_PART		;BOOT-Partition setzen und
			sta	r3H			;Partitionsdaten einlesen.
			LoadB	r15L,%00001110		;MMU-Wert für RamLink Transfer
			jsr	GetRLPartEntry

			lda	dirEntryBuf +0
			cmp	#$07			;DACC-Partition ?
			bne	:51			; => Nein, Fehler...

			lda	dirEntryBuf +21		;Stimmt Startadresse ?
			cmp	BOOT_RAM_BANK +0
			bne	:51
			lda	dirEntryBuf +20
			cmp	BOOT_RAM_BANK +1
			bne	:51			; => Nein, Fehler...

			lda	#$7e
			sta	MMU
			LoadB	RAM_Conf_Reg,$00
			jsr	SetArea1
			jsr	GetRAM_RL
			jsr	SetArea2
			jsr	GetRAM_RL
			jmp	$c000			;ReBoot initialisieren.
::51			jmp	NoRAMfound

;*** FetchRAM-Routine für ReBoot.
:GetRAM_RL		lda	MMU			;Konfiguration sichern
			pha
			LoadB	MMU,%00001110		;Ram1 bis $bfff + IO + Kernal
							;I/O-Bereich und Kernal für
							;RAMLink-Transfer aktivieren.
			lda	RAM_Conf_Reg		;Konfiguration sichern
			pha
			and	#%11110000
			ora	#%00000100		;Common Area $0000 bis $0400
			sta	RAM_Conf_Reg

			jsr	EN_SET_REC		;RL-Hardware aktivieren.

			lda	r0L
			sta	$de02
			lda	r0H
			sta	$de03

			lda	r1L
			sta	$de04
			lda	r1H
			clc
			adc	RamBankFirst +0
			sta	$de05
			lda	r3L
			adc	RamBankFirst +1
			sta	$de06

			lda	r2L
			sta	$de07
			lda	r2H
			sta	$de08

			lda	#$00
			sta	    $de09
			sta	    $de0a

			lda	#$91
			sta	    $de01

			jsr	EXEC_REC_REU		;Job ausführen und
			jsr	RL_HW_DIS2		;RL-Hardware abschalten.

			PopB	RAM_Conf_Reg
			PopB	MMU
			rts

;*** ReBoot-Routine für REU.
:ReBoot_REU		lda	#$7e
			sta	MMU
			LoadB	RAM_Conf_Reg,$00
			jsr	SetArea1
			jsr	GetRAM_REU
			jsr	SetArea2
			jsr	GetRAM_REU
			jmp	$c000			;ReBoot initialisieren.

;*** FetchRAM-Routine für ReBoot.
:GetRAM_REU		lda	MHZ
			sta	:mhz+1
			LoadB	MHZ,0
			MoveW	r0 ,$df02
			MoveW	r1 ,$df04
			MoveW  r2 ,$df07
			lda	#$00
			sta	    $df06
			sta	    $df09
			sta	    $df0a

			lda	#$91
			sta	    $df01
::51			lda	    $df00
			and	#$60
			beq	:51
::mhz			lda	#0
			sta	MHZ
			rts

;*** ReBoot-Routine für BBGRAM.
:ReBoot_BBG		lda	#$7e
			sta	MMU
			LoadB	RAM_Conf_Reg,$00
			jsr	SetArea1
			jsr	GetRAM_BBG
			jsr	SetArea2
			jsr	GetRAM_BBG
			jmp	$c000			;ReBoot initialisieren.

;*** FetchRAM-BBG-Routine für ReBoot.
:GetRAM_BBG		lda	#> $de00		;High-Byte für REU-Adresse immer
			sta	r5H			;auf $DExx setzen.
			lda	r1L			;":r5" zeigt jetzt auf das erste
			sta	r5L			;benötigte Byte auf der REU-Seite.
			jsr	DefPageBBG		;Speicher-Seite berechnen.

::52			lda	#$01			;$0100 Bytes als Startwert für
			sta	r7H			;RAM-Routinen.
			lda	#$00
			tay
			sta	r7L
			sec				;Anzahl der zu kopierenden Bytes
			sbc	r5L			;berechnen.
			sta	r7L
			lda	r7H
			sbc	#$00
			sta	r7H

			lda	r7H
			cmp	r2H
			bne	:53
			lda	r7L
			cmp	r2L			;Weniger als 256 Byte kopieren ?
::53			bcc	:54			;Nein, weiter...

			lda	r2H
			sta	r7H
			lda	r2L
			sta	r7L

::54			ldx	r7L			;Bytes aus REU einlesen und inc
::55			lda	(r5L),y			;C64-RAM kopieren.
			sta	(r0L),y
			iny
			dex
			bne	:55

			lda	r0L			;Zeiger auf C64-Adresse korrigieren.
			clc
			adc	r7L
			sta	r0L
			lda	r0H
			adc	r7H
			sta	r0H

			lda	r2L			;Anzahl bereits bearbeiteter Bytes
			sec				;korrigieren.
			sbc	r7L
			sta	r2L
			lda	r2H
			sbc	r7H
			sta	r2H

			lda	#$00			;LOW-Byte Speicherseite auf #0
			sta	r5L			;setzen (Speicherseite ab $DE00).

			inc	r1H			;Zeiger auf nächste Speicherseite.

			jsr	DefPageBBG

			lda	r2L
			ora	r2H			;Alle Bytes kopiert ?
			bne	:52			;Nein, weiter.

			ldx	#$00
			rts

;*** REU-Speicherseite berechnen.
:DefPageBBG		lda	r1H
			pha
			sta	$dffe
			lda	#$00
			asl	r1H
			rol
			asl	r1H
			rol
			sta	$dfff
			pla
			sta	r1H
			rts

;*** Zeiger auf RAM-Speicherbereiche.
:SetArea1		LoadW	r0 ,$9d80
			LoadW	r1 ,$b900
			LoadW	r2 ,$0280
			LoadB	r3L,$00
			LoadB	RAM_Conf_Reg,$07
			lda	BOOT_RAM_BANK +1
			ldx	#$00
			stx	RAM_Conf_Reg
			sta	r3H
			rts

:SetArea2		LoadW	r0 ,$c000
			LoadW	r1 ,$bc40
			LoadW	r2 ,$0100
			LoadB	r3L,$00
			LoadB	RAM_Conf_Reg,$07
			lda	BOOT_RAM_BANK +1
			ldx	#$00
			stx	RAM_Conf_Reg
			sta	r3H
			rts

;*** Texte ausgeben.
:Strg_Titel		lda	#$00			;BootText00
			b $2c
:Strg_RamExp_Exit	lda	#$01			;BootText20
			asl
			tax
			lda	StrgVecTab1 +0,x	;Zeiger auf Text einlesen.
			ldy	StrgVecTab1 +1,x

:Strg_CurText		php				;BASIC-ROM aktivieren.
			sei
			sta	r0L
			sty	r0H

			PushB	MMU
			lda	#$00			;RAM bis $3fff sonst ROM und IO
			sta	MMU

::loop			ldy	#0
			lda	(r0),y
			beq	:end
			jsr	$ffd2			;Text ausgeben.
			inc	r0L
			bne	:loop
			inc	r0H
			jmp	:loop

::end			pla
			sta	MMU
			plp
			rts

;*** Zeiger auf Textausgabe-Strings.
:StrgVecTab1		w BootText00
			w BootText20

;*** Texte für Start-Sequenz.
if Sprache = Deutsch
:BootText20		b CR,	"RBOOT WURDE NOCH NICHT KONFIGURIERT"
			b CR,	"ODER DIE SPEICHERERWEITERUNG WURDE"
			b CR,	"NICHT ERKANNT. START ABGEBROCHEN..."
			b CR,CR,NULL
endif

if Sprache = Englisch
:BootText20		b CR,	"RBOOT WAS NOT CONFIGURED YET OR"
			b CR,	"RAM-EXPANSION NOT DETECTED."
			b CR,	"GEOS-START CANCELLED..."
			b CR,CR,NULL
endif

;******************************************************************************
;*** Zusatzprogramme.
;******************************************************************************
			t "-G3_DetectRAM"
			t "-G3_GetRLPEntry"
			t "-G3_PrntBootInf"
;******************************************************************************
