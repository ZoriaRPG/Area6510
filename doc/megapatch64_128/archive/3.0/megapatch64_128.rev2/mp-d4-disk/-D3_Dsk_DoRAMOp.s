﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp0 = TRUE_C64!TRUE
;******************************************************************************
;*** Einsprungtabelle RAM-Funktionen.
;    Übergabe:		r0	= Startadresse C64-RAM.
;			r1	= Startadresse REU.
;			r2	= Anzahl Bytes.
;			r3L	= Speicherbank.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_VerifyRAM		ldy	#%10010011		;RAM-Bereich Vergleichen.
			b $2c
:xDsk_StashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xDsk_SwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xDsk_FetchRAM		ldy	#%10010001		;RAM-Bereich laden.
:xDsk_DoRAMOp		jsr	xTestRL_RAM		;Auf RAMLink testen.
			txa				;Ist RAMLink verfügbar ?
			bne	:51			;Nein, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			lda	CPU_DATA		;CPU Register einlesen und
			pha				;zwischenspeichern.
			lda	#$36			;I/O-Bereich und Kernal für
			sta	CPU_DATA		;RAMLink-Transfer aktivieren.

			tya
			pha
			jsr	$e0a9			;RL-Hardware aktivieren.
			pla	 			;Transferdaten setzen.
			sta	$de01
			lda	r0L
			sta	$de02
			lda	r0H
			sta	$de03
			lda	r1L
			sta	$de04
			lda	r1H
			sta	$de05
			lda	r3L
			sta	$de06
			lda	r2L
			sta	$de07
			lda	r2H
			sta	$de08
			lda	#$00
			sta	$de0e

			jsr	$fe06			;Job ausführen und
			jsr	$fe0f			;RL-Hardware abschalten.

			pla
			sta	CPU_DATA		;CPU-Register zurücksetzen.
			plp				;IRQ-Status zurücksetzen.
			lda	#%01000000
			ldx	#$00
::51			rts
endif

;******************************************************************************
::tmp1 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp1 = TRUE_C128!TRUE
;******************************************************************************
;*** Einsprungtabelle RAM-Funktionen.
;    Übergabe:		r0	= Startadresse C128-RAM.
;			r1	= Startadresse REU.
;			r2	= Anzahl Bytes.
;			r3L	= Speicherbank.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xDsk_VerifyRAM		ldy	#%10010011		;RAM-Bereich Vergleichen.
			b $2c
:xDsk_StashRAM		ldy	#%10010000		;RAM-Bereich speichern.
			b $2c
:xDsk_SwapRAM		ldy	#%10010010		;RAM-Bereich tauschen.
			b $2c
:xDsk_FetchRAM		ldy	#%10010001		;RAM-Bereich laden.
:xDsk_DoRAMOp		jsr	xTestRL_RAM		;Auf RAMLink testen.
			txa				;Ist RAMLink verfügbar ?
			bne	:51			;Nein, Abbruch...

			php				;IRQ-Status zwischenspeichern und
			jsr	InitRLKonfig		;RL-Konfiguration einschalten

			pha
			jsr	$e0a9			;RL-Hardware aktivieren.
			pla	 			;Transferdaten setzen.
			sta	$de01
			lda	r0L
			sta	$de02
			lda	r0H
			sta	$de03
			lda	r1L
			sta	$de04
			lda	r1H
			sta	$de05
			lda	r3L
			sta	$de06
			lda	r2L
			sta	$de07
			lda	r2H
			sta	$de08
			ldy	#$00
			sty	$de0e
			iny				;C128 Bank 1
			sty	$de10

			jsr	$fe06			;Job ausführen und

			jsr	ExitRLKonfig		;Konfiguration zurücksetzen.
			plp				;IRQ-Status zurücksetzen.
			lda	#%01000000
			ldx	#$00
::51			rts
endif
