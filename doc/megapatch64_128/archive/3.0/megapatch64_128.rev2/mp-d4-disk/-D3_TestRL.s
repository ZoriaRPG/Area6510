﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp0 = TRUE_C64!TRUE
;******************************************************************************
;*** Ist RAMLink / RAM vorhanden ?
;    Übergabe:		-
;    Rückgabe:		xReg	= $00, RAMLink verfügbar.
;    Geändert:		AKKU,xReg
:xTestRL_RAM		pha
			lda	ramExpSize		;Ist RAM verfügbar ?
			beq	NoRAMLink		;Nein, Abbruch...
			pla

;*** Ist RAMLink vorhanden ?
;    Übergabe:		-
;    Rückgabe:		xReg	= $00, RAMLink verfügbar.
;    Geändert:		AKKU,xReg
:xTestRL		pha
			php				;IRQ-Status zwischenspeichern und
			sei				;IRQs sperren.
			ldx	CPU_DATA
			lda	#$36
			sta	CPU_DATA
			lda	$e0a9			;RAMLink-Testbyte einlesen.
			stx	CPU_DATA
			plp
			cmp	#$78			;Ist Testbyte = $78 (SEI-Befehl) ?
			bne	NoRAMLink		;Nein, weiter...
			pla
			ldx	#$00			;RAMLink verfügbar.
			rts

:NoRAMLink		pla
			ldx	#$0d			;Fehler, Keine RAMLink...
			rts
endif

;******************************************************************************
::tmp1 = Flag64_128!RL_NM!RL_81!RL_71!RL_41
if :tmp1 = TRUE_C128!TRUE
;******************************************************************************
;*** Ist RAMLink / RAM vorhanden ?
;    Übergabe:		-
;    Rückgabe:		xReg	= $00, RAMLink verfügbar.
;    Geändert:		AKKU,xReg
:xTestRL_RAM		pha
			lda	ramExpSize		;Ist RAM verfügbar ?
			beq	NoRAMLink		;Nein, Abbruch...
			pla

;*** Ist RAMLink vorhanden ?
;    Übergabe:		-
;    Rückgabe:		xReg	= $00, RAMLink verfügbar.
;    Geändert:		AKKU,xReg
:xTestRL		pha
			php				;IRQ-Status zwischenspeichern
			sei				;und IRQs sperren.
			PushB	MMU			;Konfiguration sichern
			LoadB	MMU,%01001110		;Ram1 bis $bfff + IO + Kernal
			ldx	$e0a9			;RAMLink-Testbyte einlesen.
			PopB	MMU			;Konfiguration wiederherstellen
			plp
			cpx	#$78			;Ist Testbyte = $78 (SEI-Befehl) ?
			bne	NoRAMLink		;Nein, weiter...
			pla
			ldx	#$00			;RAMLink verfügbar.
			rts

:NoRAMLink		pla
			ldx	#$0d			;Fehler, Keine RAMLink...
			rts
endif
