﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0a = C_41!C_71!C_81
::tmp0b = FD_41!FD_71!FD_81!FD_NM!PC_DOS!HD_41!HD_71!HD_81!HD_NM
::tmp0 = :tmp0a!:tmp0b
if :tmp0 = TRUE
;******************************************************************************
;*** Diskettenstatus einlesen.
:xGetDiskError		ldx	#> TD_SendStatus	;Floppy dazu veranlassen den
			lda	#< TD_SendStatus	;Fehlerstatus über den ser. Bus
			jsr	xTurboRoutine		;zu senden.

;*** Diskettenstatus nach READ-Job einlesen.
:GetReadError		lda	#> ErrorCode		;Befehlsbyte über ser. Bus
			sta	$8c			;einlesen.
			lda	#< ErrorCode
			sta	$8b
			jsr	GetErrorData

			ldx	ErrorCode
			ldy	ErrCodes -1,x
			txa
			cmp	#$02			;$01 = Kein Fehler ?
			bcc	:51			;Ja, Ende...
			clc				;Fehlercodes berechnen.
			adc	#$1e			;Codes 31,32,35,38 sind möglich.
			b $2c
::51			lda	#$00
::52			tax
			rts

;*** Fehlerstatus über ser. Bus einlesen.
:GetErrorData		ldy	#$01			;Fehlercode aus Floppy-Programm
			jsr	Turbo_GetBytes		;abfragen.
			pha				;Anzahl Bytes auf Stack retten und
			tay				;Zähler initialisieren.
			jsr	Turbo_GetBytes		;Anzahl Bytes aus FloppyRAM lesen.
			pla				;Anzahl Bytes wieder vom Stack
			tay				;holen und in yReg kopieren.
			rts

;*** Offset für Fehlercodes (- 30).
:ErrCodes		b $01,$05,$02,$08,$08,$01,$05,$01
			b $05,$05,$05
:ErrorCode		b $00
endif
