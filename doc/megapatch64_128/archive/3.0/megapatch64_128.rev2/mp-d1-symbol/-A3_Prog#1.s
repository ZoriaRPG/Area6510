﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Programme/Externes Kernel.
			b $f0,"e.Register",$00
			b $f0,"e.EnterDeskTop",$00
			b $f0,"e.NewToBasic",$00
			b $f0,"e.NewPanicBox",$00
			b $f0,"e.GetNextDay",$00
			b $f0,"e.DoAlarm",$00
			b $f0,"e.GetFiles",$00
			b $f0,"e.GetFiles_Data",$00
			b $f0,"e.GetFiles_Menu",$00
			b $f0,"e.DB_LdSvScreen",$00
			b $f0,"e.TaskMan",$00
			b $f0,"e.SpoolPrinter",$00
			b $f0,"e.SpoolMenu",$00
			b $f0,"e.SS_Starfield",$00
			b $f0,"e.SS_PuzzleIt!",$00
if COMP_SYS = TRUE_C64
			b $f0,"e.SS_Raster",$00
			b $f0,"e.SS_PacMan",$00
			b $f0,"o.SS_64erMove",$00
			b $f0,"e.SS_64erMove",$00
endif
			b $f0,"e.ScreenSaver",$00
			b $f0,"e.GetBackScrn",$00

;--- Patches.
			b $f0,"o.Patch_SRAM",$00	;SCPU-Patch $D200
			b $f0,"o.Patch_SCPU",$00

;--- RAM-Gerätetreiber.
			b $f0,"o.DvRAM_SCPU",$00
			b $f0,"o.DvRAM_REU",$00
			b $f0,"o.DvRAM_RL",$00
			b $f0,"o.DvRAM_BBG.1",$00
			b $f0,"o.DvRAM_BBG.2",$00

;Dateien löschen die über 'd'-OpCode
;in den Objektcode eingebunden wurden.
:DelObjFilesScrS	b $f1
			lda	#DEL_OBJ_FILES
			cmp	#FALSE
			beq	:0

			lda	a1H
			jsr	SetDevice
			jsr	OpenDisk

			LoadW	r0,:10
			jsr	DeleteFile

::0			LoadW	a0,:99
			rts

::10			b "obj.SS_64erMove",$00
::99
