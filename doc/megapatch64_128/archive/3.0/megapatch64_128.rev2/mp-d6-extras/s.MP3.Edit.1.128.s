﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3c83
:AllocBankT_Block	= $3cfe
:AllocBankT_Disk	= $3cf5
:AllocBankT_GEOS	= $3cf2
:AllocBankT_Spool	= $3cfb
:AllocBankT_Task	= $3cf8
:AllocBankUser		= $3ca6
:AllocBank_Block	= $3d29
:AllocBank_Disk		= $3d20
:AllocBank_GEOS		= $3d1d
:AllocBank_Spool	= $3d26
:AllocBank_Task		= $3d23
:AllocateBank		= $3d2b
:AllocateBankTab	= $3d00
:AutoInitSpooler	= $3eea
:AutoInitTaskMan	= $3de9
:BankCodeTab1		= $4441
:BankCodeTab2		= $4445
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4449
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $4455
:BankType_Disk		= $4451
:BankType_GEOS		= $444d
:BankUsed		= $4401
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293a
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $2929
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294b
:BootVarEnd		= $294c
:BootVarStart		= $2880
:CheckDrvConfig		= $2c6a
:CheckForSpeed		= $352f

:Class_GeoPaint		= $439f
:Class_ScrSaver		= $438e
:ClearDiskName		= $3556
:ClearDriveData		= $356d
:ClrBank_Blocked	= $3bef
:ClrBank_Spooler	= $3bec
:ClrBank_TaskMan	= $3be9
:CopyStrg_Device	= $4009
:CountDrives		= $3543
:CurDriveMode		= $43b0
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $4351
:DiskDriver_DISK	= $437a
:DiskDriver_FName	= $437d
:DiskDriver_INIT	= $4379
:DiskDriver_SIZE	= $437b
:DiskDriver_TYPE	= $4378
:DiskFileDrive		= $4377
:Dlg_DrawTitel		= $34f6
:Dlg_IllegalCfg		= $45c0
:Dlg_LdDskDrv		= $4550
:Dlg_LdMenuErr		= $4630
:Dlg_NoDskFile		= $44cd
:Dlg_SetNewDev		= $46c8
:Dlg_Titel1		= $473c
:Dlg_Titel2		= $474c
:DoInstallDskDev	= $3634
:DriveInUseTab		= $43c4
:DriveRAMLink		= $43af
:Err_IllegalConf	= $3480
:ExitToDeskTop		= $2c2d
:FetchRAM_DkDrv		= $3026
:FindCurRLPart		= $49ab
:FindDiskDrvFile	= $308f
:FindDkDvAllDrv		= $30b8
:FindDriveType		= $3963
:FindRTC_SM		= $40db
:FindRTCdrive		= $40a2
:Flag_ME1stBoot		= $43b2
:FreeBank		= $3d54
:FreeBankTab		= $3d3f
:GetDrvModVec		= $330d
:GetFreeBank		= $3cb8
:GetFreeBankTab		= $3cba
:GetInpDrvFile		= $3fef
:GetMaxFree		= $3c20
:GetMaxSpool		= $3c26
:GetMaxTask		= $3c23
:GetPrntDrvFile		= $3fa5
:InitDkDrv_Disk		= $3155
:InitDkDrv_RAM		= $314f
:InitScrSaver		= $3f26
:InstallRL_Part		= $3660
:IsDrvAdrFree		= $38a4
:IsDrvOnline		= $3a72
:LastSpeedMode		= $4477
:LdScrnFrmDisk		= $2de9
:LoadBootScrn		= $2db6
:LoadDiskDrivers	= $311b
:LoadDskDrvData		= $33b3

:LoadInptDevice		= $3fe1
:LoadPrntDevice		= $3f97
:LookForDkDvFile	= $30f6
:NewDrive		= $43ac
:NewDriveMode		= $43ad
:NoInptName		= $4488
:NoPrntName		= $447a
:PrepareExitDT		= $2c3e
:RL_Aktiv		= $4478
:SCPU_Aktiv		= $4479
:SaveDskDrvData		= $3339
:SetClockGEOS		= $4075
:SetSystemDevice	= $300b
:StashRAM_DkDrv		= $3029
:StdClrGrfx		= $2ddb
:StdClrScreen		= $2dc4
:StdDiskName		= $4494
:SwapScrSaver		= $3f67
:SysDrive		= $4363
:SysDrvType		= $4364
:SysFileName		= $4366
:SysRealDrvType		= $4365
:SysStackPointer	= $4362
:SystemClass		= $4340
:SystemDlgBox		= $34ef
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $43ae
:UpdateDiskDriver	= $3194
:VLIR_BASE		= $475a
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $25a6
:VLIR_Types		= $2380
:firstBootCopy		= $43b1
