﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DlgRegBuf		= $fecb
:Mem0_C856		= $c856
:Mem0_C856_Temp		= $c840
:Mem0_D000		= $d000
:Mem0_D000_Temp		= $cffc
:Mem0_E000		= $e000
:Mem0_E000_Temp		= $d000
:Mem0_FC00		= $fc00
:Mem0_FC00_Temp		= $fc00
:Mem0_FF00		= $ff00
:Mem0_FF00_Temp		= $fed1
:Mem0_FFFA		= $fffa
:Mem0_FFFA_Temp		= $ffe2
:VDC_Tab2		= $f4a0
:VDC_Tab3		= $f4b1
:VDC_Tab4		= $f4c2
