﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $2906
:AutoBoot_b		= $2afd
:Code10L		= $4c
:Code10a		= $2eda
:Code10b		= $2f26
:Code1L			= $6d
:Code1a			= $2afd
:Code1b			= $2b6a
:Code2L			= $68
:Code2a			= $2b6a
:Code2b			= $2bd2
:Code3L			= $4a
:Code3a			= $2bd2
:Code3b			= $2c1c
:Code4L			= $e1
:Code4a			= $2c1c
:Code4b			= $2cfd
:Code6L			= $d8
:Code6a			= $2cfd
:Code6b			= $2dd5
:Code8L			= $09
:Code8a			= $2dd5
:Code8b			= $2dde
:Code9L			= $fc
:Code9a			= $2dde
:Code9b			= $2eda
:E_KernelData		= $2f26
:L_KernelData		= $1cfe
:MP3_BANK_1		= $28ac
:MP3_BANK_2		= $28e2
:S_KernelData		= $28a4
:Vec_ReBoot		= $28a4
