﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocBankGEOS		= $3bb4
:AllocBankT_Block	= $3c2f
:AllocBankT_Disk	= $3c26
:AllocBankT_GEOS	= $3c23
:AllocBankT_Spool	= $3c2c
:AllocBankT_Task	= $3c29
:AllocBankUser		= $3bd7
:AllocBank_Block	= $3c5a
:AllocBank_Disk		= $3c51
:AllocBank_GEOS		= $3c4e
:AllocBank_Spool	= $3c57
:AllocBank_Task		= $3c54
:AllocateBank		= $3c5c
:AllocateBankTab	= $3c31
:AutoInitSpooler	= $3d83
:AutoInitTaskMan	= $3cef
:BankCodeTab1		= $42da
:BankCodeTab2		= $42de
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $42e2
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $42ee
:BankType_Disk		= $42ea
:BankType_GEOS		= $42e6
:BankUsed		= $429a
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $2909
:BootInptName		= $2927
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $2916
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $2938
:BootVarEnd		= $2939
:BootVarStart		= $2880
:CheckDrvConfig		= $2c40
:CheckForSpeed		= $349c

:Class_GeoPaint		= $4238
:Class_ScrSaver		= $4227
:ClearDiskName		= $34c0
:ClearDriveData		= $34d7
:ClrBank_Blocked	= $3b38
:ClrBank_Spooler	= $3b35
:ClrBank_TaskMan	= $3b32
:CopyStrg_Device	= $3ea2
:CountDrives		= $34ad
:CurDriveMode		= $4249
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280
:DiskDriver_Class	= $41ea
:DiskDriver_DISK	= $4213
:DiskDriver_FName	= $4216
:DiskDriver_INIT	= $4212
:DiskDriver_SIZE	= $4214
:DiskDriver_TYPE	= $4211
:DiskFileDrive		= $4210
:Dlg_DrawTitel		= $3463
:Dlg_IllegalCfg		= $4455
:Dlg_LdDskDrv		= $43e5
:Dlg_LdMenuErr		= $44c5
:Dlg_NoDskFile		= $4363
:Dlg_SetNewDev		= $455c
:Dlg_Titel1		= $45d0
:Dlg_Titel2		= $45e0
:DoInstallDskDev	= $359e
:DriveInUseTab		= $425d
:DriveRAMLink		= $4248
:Err_IllegalConf	= $33ed
:ExitToDeskTop		= $2c03
:FetchRAM_DkDrv		= $2f93
:FindCurRLPart		= $483f
:FindDiskDrvFile	= $2ffc
:FindDkDvAllDrv		= $3025
:FindDriveType		= $38cd
:FindRTC_SM		= $3f74
:FindRTCdrive		= $3f3b
:Flag_ME1stBoot		= $424b
:FreeBank		= $3c85
:FreeBankTab		= $3c70
:GetDrvModVec		= $327a
:GetFreeBank		= $3be9
:GetFreeBankTab		= $3beb
:GetInpDrvFile		= $3e88
:GetMaxFree		= $3b69
:GetMaxSpool		= $3b6f
:GetMaxTask		= $3b6c
:GetPrntDrvFile		= $3e3e
:InitDkDrv_Disk		= $30c2
:InitDkDrv_RAM		= $30bc
:InitScrSaver		= $3dbf
:InstallRL_Part		= $35ca
:IsDrvAdrFree		= $380e
:IsDrvOnline		= $39dc
:LastSpeedMode		= $430d
:LdScrnFrmDisk		= $2d51
:LoadBootScrn		= $2d25
:LoadDiskDrivers	= $3088
:LoadDskDrvData		= $3320

:LoadInptDevice		= $3e7a
:LoadPrntDevice		= $3e30
:LookForDkDvFile	= $3063
:NewDrive		= $4245
:NewDriveMode		= $4246
:NoInptName		= $431e
:NoPrntName		= $4310
:PrepareExitDT		= $2c14
:RL_Aktiv		= $430e
:SCPU_Aktiv		= $430f
:SaveDskDrvData		= $32a6
:SetClockGEOS		= $3f0e
:SetSystemDevice	= $2f78
:StashRAM_DkDrv		= $2f96
:StdClrGrfx		= $2d43
:StdClrScreen		= $2d33
:StdDiskName		= $432a
:SwapScrSaver		= $3e00
:SysDrive		= $41fc
:SysDrvType		= $41fd
:SysFileName		= $41ff
:SysRealDrvType		= $41fe
:SysStackPointer	= $41fb
:SystemClass		= $41d9
:SystemDlgBox		= $345c
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TurnOnDriveAdr		= $4247
:UpdateDiskDriver	= $3101
:VLIR_BASE		= $45ee
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $2712
:VLIR_Types		= $2380
:firstBootCopy		= $424a
