﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "MakeInstall_64"
			t "G3_SymMacExt"

			c "MInstall_64 V1.1"
			a "Markus Kanet"
			f APPLICATION
			z $40

			o $0400
			p MainInit

if Sprache = Deutsch
			h "Erstellt die MegaPatch Setup-Dateien..."
endif
if Sprache = Englisch
			h "Create MegaPatch setup files..."
endif

;*** MegaPatch-Install einbinden.
			t "-M3_Shared"
