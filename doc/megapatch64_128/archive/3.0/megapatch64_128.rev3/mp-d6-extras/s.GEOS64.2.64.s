﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:ReBoot_BBG		= $3570
:ReBoot_REU		= $32e9
:ReBoot_RL		= $306d
:ReBoot_SCPU		= $2e00
:x_ClrDlgScreen		= $5328
:x_DoAlarm		= $3b64
:x_EnterDeskTop		= $3852
:x_GetFiles		= $3bbe
:x_GetFilesData		= $4e3a
:x_GetFilesIcon		= $4fb9
:x_GetNextDay		= $3b16
:x_PanicBox		= $3a6a
:x_ToBASIC		= $38bf
