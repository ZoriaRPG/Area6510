﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_71
if :tmp0 = TRUE
;******************************************************************************
;*** Sektor-Daten über TurboDOS an Floppy senden.
:Turbo_PutBlock		lda	#< TD_WrSekData
			ldx	#> TD_WrSekData
			bne	Turbo_SetBlock

;*** Sektor-Daten über TurboDOS aus Floppy einlesen.
:Turbo_GetBlock		lda	#< TD_RdSekData
			ldx	#> TD_RdSekData
:Turbo_SetBlock		jsr	xTurboRoutSet_r1
			MoveB	r4L,$8b			;Zeiger auf Daten an
			MoveB	r4H,$8c			;GET/SEND-Routine übergeben.
			ldy	#$00
			rts
endif
