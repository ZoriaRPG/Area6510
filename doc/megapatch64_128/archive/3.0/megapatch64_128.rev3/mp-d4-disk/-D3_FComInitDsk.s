﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
if :tmp0 = TRUE
;******************************************************************************
;*** Diskette initialisieren.
:FCom_InitDisk		jsr	InitForIO
			ldx	#> com_InitDisk
			lda	#< com_InitDisk
			jsr	SendFloppyCom
			txa
			bne	:51
			jsr	$ffae
::51			jmp	TurnOffCurDrive

:com_InitDisk		b "I0:",CR,NULL
endif
