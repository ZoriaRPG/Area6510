﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#120"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

			t "-DD_JumpTab"

;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		ldy	#$03			;3x64K für RAM1541.
			jmp	GetFreeBankTab

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			ldx	r1L			;Startadresse Laufwerkstreiber
			stx	DrvBase +0		;in REU zwischenspeichern.
			ldx	r1H
			stx	DrvBase +1

			lda	DriveAdr
			clc
			adc	#$39
			sta	DRIVE_NAME +3

			jsr	TestDriveMode		;Freien RAM-Speicher testen.
			cpx	#$00			;Ist genügend Speicher frei ?
			beq	:52			; => Ja, weiter.
			bne	:51a

;--- Laufwerk kann nicht installliert werden.
::51			ldx	#DEV_NOT_FOUND
::51a			rts

;--- RAM reservieren.
::52			pha				;RAM-Speicher in REU belegen.
			ldy	#$03
			jsr	AllocateBankTab
			pla
			cpx	#$00			;Speicher reserviert ?
			bne	:51a			; => Nein, Installationsfehler.

			ldx	DriveAdr		;Startadresse RAM-Speicher in
			sta	ramBase   -8,x		;REU zwischenspeichern.

;--- Laufwerk installieren.
			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			sta	driveType   -8,x
			sta	curType
			sta	RealDrvType -8,x
			lda	#SET_MODE_FASTDISK
			sta	RealDrvMode-8,x

;--- Treiber installieren.
			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			jsr	InitForDskDvJob		;Laufwerkstreiber in REU
			jsr	StashRAM		;kopieren.
			jsr	DoneWithDskDvJob

;--- BAM erstellen.
			jmp	CreateBAM

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	lda	driveType   -8,x	;RAM-Laufwerk installiert ?
			bpl	:51			; => Nein, weiter...

			txa				;RAM-Speicher in der REU wieder
			pha				;freigeben.
			lda	ramBase     -8,x
			ldy	#$03
			jsr	FreeBankTab
			pla
			tax

::51			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** RAM-Laufwerk bereits installiert ?
:TestCurBAM		jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	:51			; => Ja, Laufwerk initialisieren.

			LoadW	r0,curDirHead +$ad
			LoadW	r1,BAM_41     +$ad
			ldx	#r0L
			ldy	#r1L			;Auf GEOS-Kennung
			lda	#12			;"GEOS-format" testen.
			jsr	CmpFString		;Kennung vorhanden ?
			bne	:51			; => Ja, Directory nicht löschen.

			ldx	#$00
			b $2c
::51			ldx	#BAD_BAM
			rts

;*** Neue BAM erstellen.
:ClearCurBAM		ldy	#$00			;Speicher für BAM #1 löschen.
			tya
::51			sta	curDirHead,y
			iny
			bne	:51

			ldy	#$bd
::52			dey				;BAM #1 erzeugen.
			lda	BAM_41      ,y
			sta	curDirHead  ,y
			tya
			bne	:52

			jsr	PutDirHead		;BAM auf Diskette speichern.
			txa
			bne	:53

			jsr	ClrDiskSekBuf		;Sektorspeicher löschen.

			lda	#$ff			;Hauptverzeichnis löschen.
			sta	diskBlkBuf +$01
			LoadW	r4 ,diskBlkBuf
			LoadB	r1L,$12
			LoadB	r1H,$01
			jsr	PutBlock
			txa
			bne	:53

			lda	#$13			;Sektor $13/$08 löschen.
			sta	r1L			;Ist Borderblock für DeskTop 2.0!
			lda	#$08
			sta	r1H
			jsr	PutBlock
::53			rts

;*** Sektorspeicher löschen.
:ClrDiskSekBuf		ldy	#$00
			tya
::51			sta	diskBlkBuf,y
			dey
			bne	:51
			rts

;*** BAM für RAM41-Laufwerke.
:BAM_41			b $12,$01,$41,$00,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $15,$ff,$ff,$1f,$15,$ff,$ff,$1f
			b $11,$fc,$ff,$07,$12,$ff,$fe,$07
			b $13,$ff,$ff,$07,$13,$ff,$ff,$07
			b $13,$ff,$ff,$07,$13,$ff,$ff,$07
			b $13,$ff,$ff,$07,$12,$ff,$ff,$03
			b $12,$ff,$ff,$03,$12,$ff,$ff,$03
			b $12,$ff,$ff,$03,$12,$ff,$ff,$03
			b $12,$ff,$ff,$03,$11,$ff,$ff,$01
			b $11,$ff,$ff,$01,$11,$ff,$ff,$01
			b $11,$ff,$ff,$01,$11,$ff,$ff,$01
:DRIVE_NAME		b $52,$41,$4d,$20,$31,$35,$34,$31
			b $a0,$a0,$a0,$a0,$a0,$a0,$a0,$a0
			b $a0,$a0,$52,$44,$a0,$32,$41,$a0
			b $a0,$a0,$a0,$13,$08,$47,$45,$4f;$13/$08 Borderblock!
			b $53,$20,$66,$6f,$72,$6d,$61,$74
			b $20,$56,$31,$2e,$30

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:DrvBase		w $0000

;*** Dialogbox-Texte.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAM1541",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON,REV_ON
			b "Installation RAM1541",NULL
endif

;******************************************************************************
			t "-DD_AskClrBAM"
;******************************************************************************

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
