﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#126"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

;Der Laufwerkstreiber speichert die aktuellen Informationen über
;installierte NativeMode-Laufwerke.
;Beim ersten Start über GEOS.MP3 werden so bereits installierte
;Laufwerke in der Init-Routine gespeichert.
			t "s.MP3.Edit.1.ext"

;******************************************************************************
			t "-DD_JumpTab"
;******************************************************************************
			t "-DD_RDrvNMSize"
			t "-DD_RDrvNMExist"
			t "-DD_RDrvNMPart"
			t "-DD_AskClrBAM"
;******************************************************************************

;*** Prüfen ob Laufwerk installiert werden kann.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		ldy	#$02			;Mind 2x64K erforderlich.
			jmp	GetFreeBankTab

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Diskettenfehler ?
			bne	DeInstallDrvData	; => Ja, Laufwerk initialisieren.

			inx				;Zeiger auf Spur $01/02 setzen
			stx	r1L			;und BAM-Sektor mit Laufwerks-
			inx				;größe einlesen.
			stx	r1H
			jsr	GetBlock_dskBuf
			txa				;Diskettenfehler?
			bne	DeInstallDrvData	;Ja, Speicher kann nicht ermittelt
							;werden => Speicher kann nicht
							;mehr freigegeben werden.
			ldx	curDrive
			lda	ramBase     -8,x	;RAM-Speicher wieder
			ldy	diskBlkBuf  +8		;freigeben.
			jsr	FreeBankTab

:DeInstallDrvData	ldx	curDrive
			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode-8,x
			tax
			rts

;*** Max. freien Speicher ermitteln.
;    Rückgabe:    MinFreeRRAM = Startbank für Laufwerk im RAM.
;                 MaxSizeRRAM = Max. Größe für Laufwerk.
;Dazu wird die max. RAM-größe als Startwert gesetzt und dann der Wert
;so lange rediziert bis der größte Speicherblock für ein RAMNative-Laufwerk
;gefunden wurde.
:GetMaxSize		ldy	ramExpSize		;Max. Größe für Laufwerk
			sty	r2L			;ermitteln.

::51			ldy	r2L
			beq	:53
			jsr	GetFreeBankTab
			cpx	#NO_ERROR
			beq	:52
			dec	r2L
			jmp	:51

;--- Freien Speicher gefunden.
::52			sta	MinFreeRRAM
			sty	MaxSizeRRAM
			rts

;--- Kein Speicher frei.
::53			ldy	#$00
			sty	MaxSizeRRAM
			ldx	#NO_FREE_RAM
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			lda	#"R"			;Kennung für RAM-Laufwerk/GEOS-DACC.
			ldx	#"A"
			ldy	#"M"
			jsr	SetRDrvName

;--- Verfügbares RAM ermitteln.
			ldx	DriveAdr		;Vorgabewert für Größe des
			lda	DskSizeA -8,x		;RAMNative-Laufwerk setzen.
			sta	SetSizeRRAM

			jsr	GetMaxSize		;Max. mögliche Größe ermitteln.
			txa				;Laufwerk möglich?
			bne	:11			;Nein, Abbruch.

			lda	MaxSizeRRAM
			cmp	#$02			;Mind 1x64K verfügbar?
			bcs	:21			; => Ja, weiter...
			ldx	#NO_FREE_RAM
::11			rts

;--- Treiber installieren.
::21			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			sta	RealDrvType -8,x
			and	#%10001111		;Bit #6/#5/#4=Ext.RAM-Type löschen.
			sta	driveType   -8,x
			sta	curType
			lda	#SET_MODE_SUBDIR!SET_MODE_FASTDISK!SET_MODE_CREU
			sta	RealDrvMode-8,x

			jsr	InitForDskDvJob		;Laufwerkstreiber in GEOS-Speicher
			jsr	StashRAM		;kopieren => Aktueller Treiber
			jsr	DoneWithDskDvJob	;immer im erweiterten Speicher!

;--- Größe des Laufwerks bestimmen.
			jsr	GetCurPartSize		;Falls Laufwerk schon einmal
							;installiert, Größe übernehmen.
			jsr	GetPartSize		;Größe festlegen.
			txa				;Abbruch der Installation?
			bne	:42			; => Ja, Ende...

;--- Installation fortsetzen.
::31			lda	MinFreeRRAM		;Speicher für Laufwerk in
			ldy	SetSizeRRAM		;GEOS-DACC reservieren.
			jsr	AllocateBankTab
			cpx	#NO_ERROR		;Speicher reserviert ?
			bne	:43			; => Nein, Installationsfehler.

			ldx	DriveAdr		;Startadresse Laufwerk in
			lda	MinFreeRRAM		;GEOS-DACC zwischenspeichern.
			sta	ramBase     -8,x

;--- Laufwerk initialisieren.
::41			jsr	InitRDrvNM		;RAMNative-Laufwerk initialisieren.
			txa				;Vorgang erfolgreich?
			beq	:44			;Ja, Ende...

			lda	MinFreeRRAM		;Fehler beim erstellen der BAM,
			jsr	DeInstallDrvData	;Laufwerk nicht installiert.

::42			ldx	#DEV_NOT_FOUND
::43			rts

::44			bit	Flag_ME1stBoot		;Erststart GEOS-Editor?
			bmi	:45			;Nein, weiter...
			jsr	UpdateDskDrvData	;Laufwerksdaten speichern.
::45			ldx	#NO_ERROR
			rts

;*** Routine zum schreiben von Sektoren.
:WriteSektor		PushW	r1

			dec	r1L
			lda	r1H
			clc
			adc	#$00
			sta	r1H
			lda	r1L
			ldx	curDrive
			adc	ramBase -8,x
			sta	r3L
			lda	#$00
			sta	r1L

			LoadW	r0,diskBlkBuf
			LoadW	r2,$0100
			jsr	StashRAM

			PopW	r1
			ldx	#NO_ERROR
			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:MinFreeRRAM		b $00

;*** Titelzeile für Dialogbox.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation RAM Native",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation RAM Native",NULL
endif

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
;******************************************************************************
