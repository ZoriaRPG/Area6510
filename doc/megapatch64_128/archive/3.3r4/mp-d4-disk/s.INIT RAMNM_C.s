﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "mod.MDD_#172"
			t "G3_SymMacExt"

if Flag64_128 = TRUE_C64
			t "G3_V.Cl.64.Disk"
endif
if Flag64_128 = TRUE_C128
			t "G3_V.Cl.128.Disk"
endif

;******************************************************************************
			t "-DD_JumpTab"
;******************************************************************************
			t "-R3_DetectCREU"
			t "-R3_GetSizeCREU"
			t "-R3_DoRAM_CREU"
			t "-R3_DoRAMOpCREU"
			t "-DD_RDrvNMSize"
			t "-DD_RDrvNMExist"
			t "-DD_RDrvNMPart"
			t "-DD_AskClrBAM"
;******************************************************************************

;*** Prüfen ob Laufwerk installiert werden kann.
;    Rückgabe:		xReg = $00, Laufwerk kann installiert werden.
:xTestDriveMode		= DetectCREU

;*** Laufwerk deinstallieren.
;    Übergabe:		xReg = Laufwerksadresse.
:xDeInstallDrive	txa
			jsr	SetDevice		;Laufwerk aktivieren.

:DeInstallDrvData	ldx	curDrive
			lda	#$00			;Laufwerksdaten löschen.
			sta	ramBase     -8,x
			sta	driveType   -8,x
			sta	driveData   -8,x
			sta	RealDrvType -8,x
			sta	turboFlags  -8,x
			sta	RealDrvMode -8,x
			tax
			rts

;*** Laufwerk installieren.
;    Übergabe:		AKKU = Laufwerkmodus.
;			xReg = Laufwerksadresse.
;    Rückgabe:		xReg = $00, Laufwerk installiert.
;			     = $0D, Laufwerkstyp nicht verfügbar.
:xInstallDrive		sta	DriveMode		;Laufwerksdaten speichern.
			stx	DriveAdr

			lda	#"R"			;Kennung für RAM-Laufwerk/GEOS-DACC.
			ldx	#"E"
			ldy	#"U"
			jsr	SetRDrvName

;--- C=REU suchen.
			jsr	xTestDriveMode		;C=REU suchen.
			txa				;Installiert?
			beq	:2			; => Ja, weiter...
::1			rts

;--- C=REU-Laufwerk bereits installiert?
::2			lda	DriveMode
			jsr	CheckRDrvExist		;Laufwerk bereits installiert?
			txa
			bne	:1			; => Ja, Ende...

;--- Verfügbares RAM ermitteln.
			jsr	CREU_GET_SIZE		;Größe der C=REU ermitteln.
			txa				;Fehler aufgetreten?
			bne	:11			;Ja, Kein Speicher verfügbar...

			lda	CREU_BANK_COUNT		;Anzahl 64K-Bänke einlesen.
			b $2c
::11			lda	#$00
			cmp	#$00			;Speicher verfügbar?
			beq	:11d			;Nein, Abbruch...
			ldy	ramExpSize		;Zeiger auf erste Bank ermitteln.
			ldx	GEOS_RAM_TYP		;GEOS-DACC-Typ einlesen.
			cpx	#RAM_REU		;C=REU = GEOS-DACC?
			beq	:11c			;Ja, Speicher beginnt hinter DACC.
			ldy	#$00			;Nein, Speicher beginnt bei Bank #0.
::11c			sty	MinFreeRRAM		;Freien Speicher berechnen.
			cmp	MinFreeRRAM
			bcc	:11
			sta	MaxFreeRRAM
			sec
			sbc	MinFreeRRAM
::11d			sta	MaxSizeRRAM
			cmp	#$02			;Mind 2x64K verfügbar?
			bcs	:21			; => Ja, weiter...
			ldx	#NO_FREE_RAM
			rts

;--- Treiber installieren.
::21			jsr	i_MoveData		;Laufwerkstreiber aktivieren.
			w	BASE_DDRV_DATA
			w	DISK_BASE
			w	SIZE_DDRV_DATA

			ldx	DriveAdr		;Laufwerksdaten setzen.
			lda	DriveMode
			sta	RealDrvType -8,x
			and	#%10001111		;Bit #6/#5/#4=Ext.RAM-Type löschen.
			sta	driveType   -8,x
			sta	curType
			lda	#SET_MODE_SUBDIR!SET_MODE_FASTDISK!SET_MODE_CREU
			sta	RealDrvMode-8,x
			lda	MinFreeRRAM		;Erste Speicherbank definieren.
			sta	ramBase     -8,x

			jsr	InitForDskDvJob		;Laufwerkstreiber in GEOS-Speicher
			jsr	StashRAM		;kopieren => Aktueller Treiber
			jsr	DoneWithDskDvJob	;immer im erweiterten Speicher!

;--- Größe des Laufwerks bestimmen.
			jsr	GetCurPartSize		;Falls Laufwerk schon einmal
							;installiert, Größe übernehmen.
			jsr	GetPartSize		;Größe festlegen.
			txa				;Abbruch der Installation?
			bne	:31			; => Ja, Ende...

;--- Laufwerk initialisieren.
			jsr	InitRDrvNM
			txa
			beq	:32

			lda	MinFreeRRAM		;Fehler beim erstellen der BAM,
			jsr	DeInstallDrvData	;Laufwerk nicht installiert.

::31			ldx	#DEV_NOT_FOUND
			b $2c
::32			ldx	#NO_ERROR
			rts

;*** Routine zum schreiben von Sektoren.
;    StashRAM kann nicht verwendet werden, da evtl. GEOS-DACC nicht im
;    RAM gespeichert ist => Falsche RAM-Treiber!
:WriteSektor		PushW	r1

			dec	r1L
			lda	r1H
			clc
			adc	#$00
			sta	r1H
			lda	r1L
			ldx	curDrive
			adc	ramBase -8,x
			sta	r3L
			lda	#$00
			sta	r1L

			LoadW	r0,diskBlkBuf
			LoadW	r2,$0100

			jsr	StashRAM_CREU

			PopW	r1

			lda	#%01000000		;Kein Fehler...
			ldx	#NO_ERROR
			rts

;*** Systemvariablen.
:DriveMode		b $00
:DriveAdr		b $00
:MinFreeRRAM		b $00
:MaxFreeRRAM		b $00

;*** Titelzeile für Dialogbox.
if Sprache = Deutsch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation C=REU Native",NULL
endif

if Sprache = Englisch
:DlgBoxTitle		b PLAINTEXT,BOLDON
			b "Installation C=REU Native",NULL
endif

;******************************************************************************
;*** Endadresse testen.
;******************************************************************************
:END_INIT		g BASE_DDRV_INIT + SIZE_DDRV_INIT
:DSK_INIT_SIZE		= END_INIT - BASE_DDRV_INIT
