﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS-Verzeichniseintrag speichern.
:xSetGDirEntry		jsr	BldGDirEntry		;Verzeichnis-Eintrag erzeugen.
			jsr	GetFreeDirBlk		;Freien Eintrag suchen.
			txa				;Diskettenfehler ?
			bne	SetDirExit		;Ja, Abbruch...

			tya
			clc
			adc	#<diskBlkBuf
			sta	r5L
			lda	#>diskBlkBuf
			adc	#$00
			sta	r5H

			ldy	#$1d
::51			lda	dirEntryBuf,y		;Verzeichnis-Eintrag kopieren.
			sta	(r5L)      ,y
			dey
			bpl	:51

			jsr	SetFileDate
			jmp	PutBlock_dskBuf

;*** Aktuelles Datum in Verzeichnis-
;    eintrag schreiben.
:SetFileDate		ldy	#$17
::51			lda	year -$17,y
			sta	(r5L)    ,y
			iny
			cpy	#$1c
			bne	:51
:SetDirExit		rts
