﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

English users please scroll down!

Willkommen zum MegaPatch für GEOS - Version 3.3
https://gitlab.com/mkslack/Area6510
2019/Jan/08

In der aktuellen Version ist TOPDESK als Desktop-Oberfläche nicht mehr enthalten. TOPDESK, der mit MegaPatch von 2000/2003 veröffentlicht wurde, kann weiterhin verwendet werden. Gleiches gilt für den ursprünglichen DESKTOP von GEOS 2.x. Der Dateiname der Desktop Anwendung ist wie folgt:
	C64	"DESK TOP"  or
	C128	"128 DESKTOP"

Um MegaPatch zu installieren startet man die Datei "SetupMP" von der aktuellen DeskTop-Oberfläche. Bei der Installation folgt man am besten den Bildschirm-Anzeigen und kopiert alle Dateien auf die Ziel-Diskette. Falls möglich sollte man eine leere Diskette zur Installation verwenden, es werden ca. 230Kb(C64) bzw. 240Kb(C128) freier Speicher benötigt.  Für die Desktop-Anwendung wird zusätzlicher Speicher auf der Diskette benötigt. Bei Verwendung einer 1541-Diskette ist die benutzerdefinierte Installation zu verwenden und nur die notwendigen Startprogramme und Laufwerkstreiber installieren.

Nachdem MegaPatch im System installiert wurde wird der DeskTop gestartet.

Nach einem Neustart kann man MegaPatch aus einem laufenden GEOS-V2-System heraus starten, indem man die Datei "GEOS64.MP3" doppelklickt.

Weitere Informationen finden Sie in der Datei "README-DE".

Welcome to MegaPatch for GEOS - Version 3.3
https://gitlab.com/mkslack/Area6510

The current releases no longer contain TOPDESK as desktop application anymore. TOPDESK released with MegaPatch from 2000/2003 can still be used. Same applies to the original DESKTOP from GEOS 2.x. The desktop file must be named like this:
	C64	"DESK TOP"  or
	C128	"128 DESKTOP"

To install MegaPatch on your C64 you have to run the SetupMP-File from your current deskTop.  Please follow the menus and copy all files to your MegaPatch-workdisk. If possible, please use an empty disk/partition when installing MegaPatch, you need about 230Kb(C64) or 240Kb(C128) free disk space. You need additional disk space for the desktop application. If you do only have a 1541 target disk you must use the custom installation and install only required files and disk drivers.

After the files has been copied, MegaPatch will be loaded. After MegaPatch has been installed successfully, your deskTop appears on the screen.

After MegaPatch has been installed you can reload MegaPatch anytime from a running GEOSV2-system using "GEOS.MP3".

You will find more information in the file "README-EN".
