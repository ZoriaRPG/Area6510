﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = FD_NM!HD_NM
if :tmp0 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xReadPartHeader	ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head
			jmp	xReadBlock
endif

;******************************************************************************
::tmp1 = RL_NM
if :tmp1 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xReadPartHeader	ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head		;Sonst Probleme beim erkennen der
			lda	RL_PartNr		;Partitionsgröße!
			sta	r3H
			jmp	xDsk_SekRead
endif

;******************************************************************************
::tmp2 = RD_NM
if :tmp2 = TRUE
;******************************************************************************
;*** Diskette aktivieren.
:xReadPartHeader	ldx	#$01
			stx	r1L
			inx
			stx	r1H
			LoadW	r4 ,dir3Head		;Sonst Probleme beim erkennen der
			jmp	xDsk_SekRead		;Partitionsgröße!
endif
