﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
::tmp0 = C_41!C_71
;Routine entfällt bis auf weiteres, da Routine auf der nächsten
;Seite kürzer ist.
if 0=1
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1L
:xChangeDiskDev		tay
			lda	r1L
			pha
			tya
			pha
			jsr	xEnterTurbo		;TurboDOS aktivieren.
			pla
			cpx	#$00 			;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...

			pha
			ora	#$20			;Neue Geräteadresse berechnen und
			sta	r1L			;zwischenspeichern.

			jsr	InitForIO		;I/O aktivieren.
			ldx	#> TD_NewDrvAdr
			lda	#< TD_NewDrvAdr
			jsr	xTurboRoutSet_r1	;Geräteadresse tauschen.
			jsr	DoneWithIO		;I/O deaktivieren.

			jsr	TurboOff_curDrv		;TurboFlag für aktuelles
							;Laufwerk löschen.
			pla
			tay
			cpy	#12
			bcs	:51
			lda	#$c0			;TurboFlags für neues
			sta	turboFlags -8,y		;Laufwerk setzen.
			sty	curDrive
			sty	curDevice

			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType-8,y

::51			pla
			sta	r1L 			;Abbruch.
			rts
endif

;******************************************************************************
::tmp0a = C_41!C_71
if :tmp0a = TRUE
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg,r1L
:xChangeDiskDev		pha
			ora	#%00100000
			sta	NewDrvAdr1		;Ziel-Adresse #1 berechnen.
			eor	#%01100000
			sta	NewDrvAdr2		;Ziel-Adresse #2 berechnen.

			ldx	curDrive
			cpx	#12
			bcs	:51
			jsr	xExitTurbo

::51			jsr	InitForIO

			ldx	#> Floppy_ADR
			lda	#< Floppy_ADR
			jsr	SendFloppyCom		;"M-W"-Befehl an Floppy senden.
			jsr	$ffae			;Laufwerk abschalten.

			pla
			tay
			cpy	#12
			bcs	:52
			lda	#$00			;TurboFlags für neues
			sta	turboFlags -8,y		;Laufwerk setzen.
			sty	curDrive
			sty	curDevice

			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType-8,y

::52			ldx	#$00
			jmp	DoneWithIO

;*** Variablen für Laufwerkstausch.
:Floppy_ADR		b "M-W",$77,$00,$02
:NewDrvAdr1		b $00
:NewDrvAdr2		b $00
endif

;******************************************************************************
::tmp1a = C_81!FD_41!FD_71!FD_81!FD_NM!HD_41!HD_71!HD_81!HD_NM
::tmp1b = PC_DOS!IEC_NM!S2I_NM
::tmp1 = :tmp1a!:tmp1b
if :tmp1 = TRUE
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xChangeDiskDev		sta	Floppy_U0_x +3		;Neue Adresse merken.

			jsr	xPurgeTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O-Bereich einbleden.

			ldx	#> Floppy_U0_x
			lda	#< Floppy_U0_x
			jsr	SendFloppyCom		;Geräteadresse ändern.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...

			ldy	Floppy_U0_x +3		;Geräteadresse einlesen.
			cpy	#12
			bcs	:50
;			lda	#$00
			sta	turboFlags -8,y		;TurboFlags löschen.
			sty	curDrive		;Neue Adresse an GEOS übergeben.
			sty	curDevice

			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType-8,y

::50			jmp	TurnOffCurDrive		;Laufwerk deaktivieren.
::51			jmp	DoneWithIO

;*** Befehl zum wechseln dr Laufwerksadresse.
:Floppy_U0_x		b "U0",$3e,$08,NULL
endif

;******************************************************************************
::tmp2 = RL_41!RL_71!RL_81!RL_NM
if :tmp2 = TRUE
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xChangeDiskDev		sta	Floppy_U0_x +3		;Neue Adresse merken.

			jsr	xPurgeTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O-Bereich einbleden.

			ldx	#> Floppy_U0_x
			lda	#< Floppy_U0_x
			jsr	SendFloppyCom		;Geräteadresse ändern.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...

			ldy	Floppy_U0_x +3		;Geräteadresse einlesen.
			cpy	#12
			bcs	:51
;			lda	#$00
			sta	turboFlags -8,y		;TurboFlags löschen.
			sty	curDrive		;Neue Adresse an GEOS übergeben.
			sty	curDevice

			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType-8,y

::51			jmp	DoneWithIO

;*** Befehl zum wechseln dr Laufwerksadresse.
:Floppy_U0_x		b "U0",$3e,$08,NULL
endif

;******************************************************************************
::tmp3 = RD_NM!RD_81!RD_71!RD_41!RD_NM_SCPU!RD_NM_CREU!RD_NM_GRAM
if :tmp3 = TRUE
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xChangeDiskDev		sta	curDrive
			sta	curDevice

			tay
			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType -8,y

			ldx	#$00
			rts
endif

;******************************************************************************
::tmp4 = HD_41_PP!HD_71_PP!HD_81_PP!HD_NM_PP
if :tmp4 = TRUE
;******************************************************************************
;*** Geräteadresse ändern.
;    Übergabe:		AKKU	= Neue Geräteadresse.
;    Rückgabe:		-
;    Geändert:		AKKU,xReg,yReg
:xChangeDiskDev		sta	Floppy_U0_x +3		;Neue Adresse merken.

			jsr	xPurgeTurbo		;TurboDOS deaktivieren.
			jsr	InitForIO		;I/O-Bereich einbleden.

			ldx	#> Floppy_U0_x
			lda	#< Floppy_U0_x
			jsr	SendFloppyCom		;Geräteadresse ändern.
			txa				;Laufwerksfehler ?
			bne	:51			;Ja, Abbruch...

			ldy	Floppy_U0_x +3		;Geräteadresse einlesen.
			cpy	#12
			bcs	:50
;			lda	#$00
			sta	turboFlags -8,y		;TurboFlags löschen.
			sty	curDrive		;Neue Adresse an GEOS übergeben.
			sty	curDevice

			lda	DiskDrvType		;Laufwerkstyp aktualisieren.
			sta	RealDrvType-8,y

::50			lda	curDrive
			jsr	$ffb1
			lda	#$ef
			jsr	$ff93
			jsr	$ffae
			ldx	#$00
::51			jmp	DoneWithIO

;*** Befehl zum wechseln dr Laufwerksadresse.
:Floppy_U0_x		b "U0",$3e,$08,NULL
endif
