﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Diskettenfehler.
;*** Zeiger auf Dialogbox-Tabellen.
:DskErrVecTab		w Dlg_DiskError
			w Dlg_GEOS_ID_ERR
			w Dlg_EXTRACT_ERR
			w Dlg_ANALYZE_ERR
			w Dlg_CRCFILE_ERR

;*** Dialogbox: Diskettenfehler.
:Dlg_DiskError		b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DskErrTitel
			b DBTXTSTR ,$10,$20
			w DlgT_01_01
			b DB_USR_ROUT
			w PrntErrCode
			b OK       ,$10,$48
			b NULL

;*** Dialogbox: Diskettenfehler.
:Dlg_GEOS_ID_ERR	b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DskErrTitel
			b DBTXTSTR ,$10,$20
			w DlgT_02_01
			b DBTXTSTR ,$10,$2a
			w DlgT_02_02
			b DB_USR_ROUT
			w PrntErrCode
			b OK       ,$10,$48
			b NULL

;*** Dialogbox: Diskettenfehler.
:Dlg_EXTRACT_ERR	b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DskErrTitel
			b DBTXTSTR ,$10,$20
			w DlgT_03_01
			b DBTXTSTR ,$10,$2a
			w DlgT_03_02
			b DB_USR_ROUT
			w PrntErrCode
			b OK       ,$10,$48
			b NULL

;*** Dialogbox: Diskettenfehler.
:Dlg_ANALYZE_ERR	b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DskErrTitel
			b DBTXTSTR ,$10,$20
			w DlgT_04_01
			b DBTXTSTR ,$10,$2a
			w DlgT_04_02
			b DB_USR_ROUT
			w PrntErrCode
			b OK       ,$10,$48
			b NULL

;*** Dialogbox: Diskettenfehler.
:Dlg_CRCFILE_ERR	b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DskErrTitel
			b DBTXTSTR ,$10,$20
			w DlgT_05_01
			b DBTXTSTR ,$10,$2a
			w DlgT_05_02
			b OK       ,$10,$48
			b NULL

;*** Dialogbox: Diskettenfehler.
:Dlg_InsertDisk		b $81
			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR ,$10,$0b
			w DlgInfoTitel
			b DBTXTSTR ,$10,$20
			w DlgT_06_01
			b DBTXTSTR ,$10,$2c
			w FNameStartMP
			b DBTXTSTR ,$10,$38
			w DlgT_06_02
			b OK       ,$10,$48
			b CANCEL   ,$02,$48
			b NULL
