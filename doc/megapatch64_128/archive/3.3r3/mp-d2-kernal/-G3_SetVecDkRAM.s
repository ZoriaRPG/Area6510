﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Laufwerkstreiber-Operationen durchführen.
.InitForDskDvJob	ldy	curDrive		;yReg unverändert!!!
.InitCurDskDvJob	jsr	DoneWithDskDvJob	;RAM-Register zurücksetzen.

			lda	DskDrvBaseL -8,y	;Zeiger auf Laufwerkstreiber
			sta	r1L			;in REU in ZeroPage kopieren.
			lda	DskDrvBaseH -8,y
			sta	r1H
			rts

;*** Zeiger auf Laufwerkstreiber in REU setzen/löschen.
.DoneWithDskDvJob	ldx	#$06
::1			lda	r0L ,x
			pha
			lda	:2  ,x
			sta	r0L ,x
			pla
			sta	:2  ,x
			dex
			bpl	:1
			rts

;*** Transferdaten für ":SetDevice".
::2			w $9000				;RAM-Adresse Laufwerkstreiber.
			w $0000				;REU-Adresse Laufwerkstreiber.
			w $0d80				;Länge Laufwerkstreiber.
			b $00				;BANK in REU.
