﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $1ae0
:AutoBoot_b		= $1c6f
:Code10L		= $49
:Code10a		= $200d
:Code10b		= $2056
:Code1L			= $57
:Code1a			= $1c6f
:Code1b			= $1cc6
:Code2L			= $48
:Code2a			= $1cc6
:Code2b			= $1d0e
:Code3L			= $49
:Code3a			= $1d0e
:Code3b			= $1d57
:Code4L			= $fd
:Code4a			= $1d57
:Code4b			= $1e54
:Code6L			= $b4
:Code6a			= $1e54
:Code6b			= $1f08
:Code8L			= $09
:Code8a			= $1f08
:Code8b			= $1f11
:Code9L			= $fc
:Code9a			= $1f11
:Code9b			= $200d
:E_KernelData		= $2056
:L_KernelData		= $0ffe
:MP3_BANK_1		= $1a86
:MP3_BANK_2a		= $1abc
:MP3_BANK_2b		= $1ace
:S_KernelData		= $1a7e
:Vec_ReBoot		= $1a7e
