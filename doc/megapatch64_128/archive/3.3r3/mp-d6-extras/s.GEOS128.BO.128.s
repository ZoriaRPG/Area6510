﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoBoot_a		= $2928
:AutoBoot_b		= $2b1f
:Code10L		= $4c
:Code10a		= $2f04
:Code10b		= $2f50
:Code1L			= $6d
:Code1a			= $2b1f
:Code1b			= $2b8c
:Code2L			= $65
:Code2a			= $2b8c
:Code2b			= $2bf1
:Code3L			= $4c
:Code3a			= $2bf1
:Code3b			= $2c3d
:Code4L			= $fd
:Code4a			= $2c3d
:Code4b			= $2d3a
:Code6L			= $c5
:Code6a			= $2d3a
:Code6b			= $2dff
:Code8L			= $09
:Code8a			= $2dff
:Code8b			= $2e08
:Code9L			= $fc
:Code9a			= $2e08
:Code9b			= $2f04
:E_KernelData		= $2f50
:L_KernelData		= $1cfe
:MP3_BANK_1		= $28ce
:MP3_BANK_2a		= $2904
:MP3_BANK_2b		= $2916
:S_KernelData		= $28c6
:Vec_ReBoot		= $28c6
