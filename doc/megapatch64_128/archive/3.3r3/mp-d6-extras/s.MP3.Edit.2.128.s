﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:DirNavEntry		= $4ca2
:DrvNmA			= $59dd
:DrvNmB			= $59ee
:DrvNmC			= $59ff
:DrvNmD			= $5a10
:DrvNmVec		= $59d5
:DrvTypeSD		= $4c9e
:PartNmA		= $5a29
:PartNmB		= $5a3a
:PartNmC		= $5a4b
:PartNmD		= $5a5c
:PartNmVec		= $5a21
:SlctPartBase		= $1c80
:SlctPartSize		= $0500
