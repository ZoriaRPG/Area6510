﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AutoInitSpooler	= $4016
:AutoInitTaskMan	= $3f15
:BankCodeTab1		= $4839
:BankCodeTab2		= $483d
:BankCode_Block		= $08
:BankCode_Disk		= $40
:BankCode_Free		= $00
:BankCode_GEOS		= $80
:BankCode_Move		= $4841
:BankCode_Spool		= $10
:BankCode_Task		= $20
:BankType_Block		= $484d
:BankType_Disk		= $4849
:BankType_GEOS		= $4845
:BankUsed		= $47f9
:BankUsed_2GEOS		= $3cad
:BootBankBlocked	= $28b4
:BootCRSR_Repeat	= $28a9
:BootColsMode		= $28ad
:BootConfig		= $2880
:BootGrfxFile		= $291b
:BootInptName		= $293d
:BootInstalled		= $2894
:BootLoadDkDv		= $28f5
:BootMLineMode		= $28af
:BootMenuStatus		= $28ae
:BootOptimize		= $28ab
:BootPartRL		= $2884
:BootPartRL_I		= $2888
:BootPartType		= $288c
:BootPrntMode		= $28ac
:BootPrntName		= $292c
:BootQWERTZ		= $294e
:BootRAM_Flag		= $2895
:BootRTCdrive		= $28f4
:BootRamBase		= $2890
:BootSaverName		= $2898
:BootScrSaver		= $2896
:BootScrSvCnt		= $2897
:BootSpeed		= $28aa
:BootSpoolCount		= $28b2
:BootSpoolSize		= $28b3
:BootSpooler		= $28b1
:BootTaskMan		= $28b0
:BootUseFastPP		= $294f
:BootVarEnd		= $2950
:BootVarStart		= $2880
:CheckDrvConfig		= $2d7a
:CheckForSpeed		= $3664
:Class_GeoPaint		= $4797
:Class_ScrSaver		= $4786
:ClearDiskName		= $368b
:ClearDriveData		= $36a2
:ClrBank_Blocked	= $3d1b
:ClrBank_Spooler	= $3d18
:ClrBank_TaskMan	= $3d15
:CopyStrg_Device	= $417d
:CountDrives		= $3678
:CurDriveMode		= $47a8
:DiskDataRAM_A		= $2180
:DiskDataRAM_S		= $2280

:DiskDriver_Class	= $4749
:DiskDriver_DISK	= $4772
:DiskDriver_FName	= $4775
:DiskDriver_INIT	= $4771
:DiskDriver_SIZE	= $4773
:DiskDriver_TYPE	= $4770
:DiskFileDrive		= $476f
:Dlg_DrawTitel		= $362b
:Dlg_IllegalCfg		= $49b8
:Dlg_LdDskDrv		= $4948
:Dlg_LdMenuErr		= $4a28
:Dlg_NoDskFile		= $48c5
:Dlg_SetNewDev		= $4ac0
:Dlg_Titel1		= $4b34
:Dlg_Titel2		= $4b44
:DoInstallDskDev	= $3771
:DriveInUseTab		= $47bc
:DriveRAMLink		= $47a7
:Err_IllegalConf	= $359f
:ExitToDeskTop		= $2d3d
:FetchRAM_DkDrv		= $3145
:FindCurRLPart		= $4e17
:FindDiskDrvFile	= $31ae
:FindDkDvAllDrv		= $31d7
:FindDriveType		= $3a8f
:FindRTC_64Net		= $4267
:FindRTC_SM		= $4253
:FindRTCdrive		= $4216
:FindSystemFile		= $35ca
:Flag_ME1stBoot		= $47aa
:GetDrvModVec		= $342c
:GetInpDrvFile		= $4163
:GetMaxFree		= $3d4c
:GetMaxSpool		= $3d52
:GetMaxTask		= $3d4f
:GetPrntDrvFile		= $40ed
:InitDkDrv_Disk		= $3274
:InitDkDrv_RAM		= $326e
:InitQWERTZ		= $4104
:InitScrSaver		= $4052
:InstallRL_Part		= $379d
:IsDrvAdrFree		= $38a1
:IsDrvOnline		= $3b9e
:LastSpeedMode		= $486f
:LdMenuError		= $3616
:LdScrnFrmDisk		= $2f08
:LoadBootScrn		= $2ed5
:LoadDiskDrivers	= $323a
:LoadDskDrvData		= $34d2
:LoadInptDevice		= $4155
:LoadPrntDevice		= $40df
:LoadSysRecord		= $35f6
:LookForDkDvFile	= $3215
:NewDrive		= $47a4
:NewDriveMode		= $47a5
:NoInptName		= $4880
:NoPrntName		= $4872
:PrepareExitDT		= $2d4e
:RL_Aktiv		= $4870
:SCPU_Aktiv		= $4871
:SetClockGEOS		= $41e9

:SetSystemDevice	= $312a
:StashRAM_DkDrv		= $3148
:StdClrGrfx		= $2efa
:StdClrScreen		= $2ee3
:StdDiskName		= $488c
:SwapScrSaver		= $40af
:SysDrive		= $475b
:SysDrvType		= $475c
:SysFileName		= $475e
:SysRealDrvType		= $475d
:SysStackPointer	= $475a
:SystemClass		= $4738
:SystemDlgBox		= $3624
:TASK_BANK0_ADDR	= $2912
:TASK_BANK_ADDR		= $28f6
:TASK_BANK_USED		= $28ff
:TASK_COUNT		= $2908
:TASK_VDC_ADDR		= $2909
:TurnOnDriveAdr		= $47a6
:UpdateDiskDriver	= $32b3
:VLIR_BASE		= $4bc6
:VLIR_Entry		= $23c0
:VLIR_Names		= $2440
:VLIR_SIZE		= $213a
:VLIR_Types		= $2380
:firstBootCopy		= $47a9
