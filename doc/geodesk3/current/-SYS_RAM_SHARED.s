﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Tabellenwert für Speicherbank finden.
;    Übergabe: YReg = 64K-Speicherbank-Nr.
;    Rückgabe: AKKU = %00xxxxxx = Frei.
;                     %01xxxxxx = Anwendung.
;                     %10xxxxxx = Laufwerk.
;                     %11xxxxxx = System.
:GetBankByte		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
::1			cpx	#$00
			beq	:2
			asl
			asl
			dex
			bne	:1
::2			and	#%11000000
			rts
