﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Texte für GEOS-Menü.
;    Hinweis: Ein "_H" am Ende markiert
;    den ersten Eintrag in einem Menü.
;    Hier muss BOLDON gesetzt werden.
:GTx_00_H		b BOLDON
			b "Programme >>",NULL
:GTx_01			b "Dokumente >>",NULL
:GTx_02			b "Einstellungen >>",NULL
:GTx_03			b "Beenden >>",NULL

:GTx_30_H		b BOLDON
			b "Anwendungen",NULL
:GTx_31			b "AutoStart",NULL

:GTx_40_H		b BOLDON
			b "Dokumente",NULL
:GTx_41			b "GeoWrite",NULL
:GTx_42			b "GeoPaint",NULL

:GTx_50_H		b BOLDON
			b "Zurück zu GEOS",NULL
:GTx_51			b "Basic starten",NULL
:GTx_52			b "Programm starten",NULL

:GTx_10_H		b BOLDON
			b "GEOS Editor",NULL
:GTx_11			b "Drucker wechseln",NULL
:GTx_12			b "Eingabegerät wechseln",NULL
:GTx_16			b "Hintergrundbild wechseln",NULL
:GTx_13			b "AppLinks speichern",NULL
:GTx_14			b "Einstellungen speichern",NULL

:GTx_18			b "( ) Gelöschte Dateien",NULL
:GTx_19			b "( ) Farbe anzeigen",NULL

:GTx_20			b "( ) PreLoad Icons",NULL
:GTx_21			b "( ) Cache-Debug",NULL
:GTx_22			b "( ) 64Kb Icon-Cache",NULL

;*** GEOS-Hauptmenü.
:MAX_ENTRY_GEOS		= 4
:MENU_GEOS_Y0		= ((MIN_AREA_BAR_Y-1) - MAX_ENTRY_GEOS*14 -2) & $f8
:MENU_GEOS_Y1		=  (MIN_AREA_BAR_Y-1)
:MENU_GEOS_X0		= MIN_AREA_BAR_X
:MENU_GEOS_X1		= $005f
:MENU_GEOS_W		= (MENU_GEOS_X1 - MENU_GEOS_X0 +1)
:MENU_GEOS_H		= (MENU_GEOS_Y1 - MENU_GEOS_Y0 +1)

:MENU_DATA_GEOS		b MENU_GEOS_Y0
			b MENU_GEOS_Y1
			w MENU_GEOS_X0
			w MENU_GEOS_X1

			b MAX_ENTRY_GEOS!VERTICAL

			w GTx_00_H
			b DYN_SUB_MENU
			w OPEN_MENU_APPL

			w GTx_01
			b DYN_SUB_MENU
			w OPEN_MENU_DOCS

			w GTx_02
			b DYN_SUB_MENU
			w OPEN_MENU_SETUP

			w GTx_03
			b DYN_SUB_MENU
			w OPEN_MENU_EXIT

;*** GEOS/Programme
:MAX_ENTRY_APPL		= 2
:MENU_APPL_Y0		= ((MENU_GEOS_Y0 + 24 - MAX_ENTRY_APPL*14 -2) & $f8)  -0 -0
:MENU_APPL_Y1		=  (MENU_APPL_Y0 + MAX_ENTRY_APPL*16 )                -0 -1
:MENU_APPL_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_APPL_X1		= MENU_APPL_X0 + $006f
:MENU_APPL_W		= (MENU_APPL_X1 - MENU_APPL_X0 +1)
:MENU_APPL_H		= (MENU_APPL_Y1 - MENU_APPL_Y0 +1)

:MENU_DATA_APPL		b MENU_APPL_Y0
			b MENU_APPL_Y1
			w MENU_APPL_X0
			w MENU_APPL_X1

			b MAX_ENTRY_APPL!VERTICAL

			w GTx_30_H
			b MENU_ACTION
			w EXIT_MENU_APPL

			w GTx_31
			b MENU_ACTION
			w EXIT_MENU_APPL

;*** GEOS/Dokumente
:MAX_ENTRY_DOCS		= 3
:MENU_DOCS_Y0		= ((MENU_GEOS_Y0 + 40 - MAX_ENTRY_DOCS*14 -2) & $f8)  -0 -0
:MENU_DOCS_Y1		=  (MENU_DOCS_Y0 + MAX_ENTRY_DOCS*16 )                -0 -1
:MENU_DOCS_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_DOCS_X1		= MENU_DOCS_X0 + $006f
:MENU_DOCS_W		= (MENU_DOCS_X1 - MENU_DOCS_X0 +1)
:MENU_DOCS_H		= (MENU_DOCS_Y1 - MENU_DOCS_Y0 +1)

:MENU_DATA_DOCS		b MENU_DOCS_Y0
			b MENU_DOCS_Y1
			w MENU_DOCS_X0
			w MENU_DOCS_X1

			b MAX_ENTRY_DOCS!VERTICAL

			w GTx_40_H
			b MENU_ACTION
			w EXIT_MENU_DOCS

			w GTx_41
			b MENU_ACTION
			w EXIT_MENU_DOCS

			w GTx_42
			b MENU_ACTION
			w EXIT_MENU_DOCS

;*** GEOS/Beenden
:MAX_ENTRY_EXIT		= 3
:MENU_EXIT_Y0		= (((MIN_AREA_BAR_Y-1) - MAX_ENTRY_EXIT*14 -2) & $f8) -8 -0
:MENU_EXIT_Y1		=  (MIN_AREA_BAR_Y-1)                                 -8 -0
:MENU_EXIT_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_EXIT_X1		= MENU_EXIT_X0 + $006f
:MENU_EXIT_W		= (MENU_EXIT_X1 - MENU_EXIT_X0 +1)
:MENU_EXIT_H		= (MENU_EXIT_Y1 - MENU_EXIT_Y0 +1)

:MENU_DATA_EXIT		b MENU_EXIT_Y0
			b MENU_EXIT_Y1
			w MENU_EXIT_X0
			w MENU_EXIT_X1

			b MAX_ENTRY_EXIT!VERTICAL

			w GTx_50_H
			b MENU_ACTION
			w EXIT_MENU_EXIT

			w GTx_51
			b MENU_ACTION
			w EXIT_MENU_EXIT

			w GTx_52
			b MENU_ACTION
			w EXIT_MENU_EXIT

;*** GEOS/Einstellungen
:MAX_ENTRY_SETUP	= 11
:MENU_SETUP_Y0		= (((MIN_AREA_BAR_Y-1) - MAX_ENTRY_SETUP*14 -2) & $f8) -8 -0
:MENU_SETUP_Y1		=  (MIN_AREA_BAR_Y-1)                                  -8 -0
:MENU_SETUP_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_SETUP_X1		= MENU_SETUP_X0 + $008f
:MENU_SETUP_W		= (MENU_SETUP_X1 - MENU_SETUP_X0 +1)
:MENU_SETUP_H		= (MENU_SETUP_Y1 - MENU_SETUP_Y0 +1)

:MENU_DATA_SETUP	b MENU_SETUP_Y0
			b MENU_SETUP_Y1
			w MENU_SETUP_X0
			w MENU_SETUP_X1

			b MAX_ENTRY_SETUP!VERTICAL

			w GTx_10_H
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_11
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_12
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_16
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_13
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_14
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_18			;Gelöschte Dateien.
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_19			;Farbe anzeigen.
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_22
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_20
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GTx_21
			b MENU_ACTION
			w EXIT_MENU_SETUP
