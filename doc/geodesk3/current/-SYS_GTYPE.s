﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Vektor auf Typ/Farb-Tabelle definieren.
:DefGTypeID		ldy	#$02
			lda	(r15L),y		;CBM-Dateityp einlesen.
			and	#%00000111		;Dateityp isolieren.
			cmp	#$06			;Typ = Verzeichnis?
			bne	:0			; => Nein, weiter...
			lda	#24			;GEOS-Dateityp "Verzeichnis".
			bne	:1

::0			ldy	#$18
			lda	(r15L),y		;GEOS-Dateityp einlesen.
			cmp	#24			;Unbekannter Typ?
			bcc	:1			; => Nein, weiter...
			lda	#23			;Typ "Unknown" setzen.
::1			rts

;*** Zeiger auf GEOS-Dateityp.
:GetGeosType		jsr	DefGTypeID		;Zeiger auf Tabelle mit
			asl				;GEOS-Texten setzen.
			tax
			lda	:tab +0,x		;Zeiger auf Text für
			ldy	:tab +1,x
			rts

;*** Tabelle auf Texte für GEOS-Dateityp.
::tab			w :ID_0
			w :ID_1
			w :ID_2
			w :ID_3
			w :ID_4
			w :ID_5
			w :ID_6
			w :ID_7
			w :ID_8
			w :ID_9
			w :ID_10
			w :ID_11
			w :ID_12
			w :ID_13
			w :ID_14
			w :ID_15
			w :ID_x
			w :ID_17
			w :ID_x
			w :ID_x
			w :ID_x
			w :ID_21
			w :ID_22
			w :ID_x
			w :ID_dir

;*** Texte für GEOS-Dateityp.
::ID_0			b "Nicht GEOS",NULL
::ID_1			b "BASIC",NULL
::ID_2			b "Assembler",NULL
::ID_3			b "Datenfile",NULL
::ID_4			b "System-Datei",NULL
::ID_5			b "DeskAccessory",NULL
::ID_6			b "Anwendung",NULL
::ID_7			b "Dokument",NULL
::ID_8			b "Zeichensatz",NULL
::ID_9			b "Druckertreiber",NULL
::ID_10			b "Eingabetreiber",NULL
::ID_11			b "Laufwerkstreiber",NULL
::ID_12			b "Startprogramm",NULL
::ID_13			b "Temporär",NULL
::ID_14			b "Selbstausführend",NULL
::ID_15			b "Eingabetreiber 128",NULL
::ID_17			b "gateWay-Dokument",NULL
::ID_21			b "geoShell-Kommando",NULL
::ID_22			b "geoFAX Druckertreiber",NULL
::ID_x			b "GEOS ???",NULL
::ID_dir		b "Verzeichnis",NULL

;*** Text für 1581-Partition.
;    Hinweis: Wird aktuell nicht unterstützt.
if FALSE
::ID_81dir		b "< 1581 - Partition >",NULL
endif
