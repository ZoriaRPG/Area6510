﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** DiskImages und Verzeichnisse einlesen.
:READ_SD2IEC_DATA	jsr	CkSD2IECMode		;SD2IEC-Modus testen.
			txa				;Verzeichnis oder DiskImage?
			beq	:sd_dir_mode		; => Verzeichnis-Modus, weiter...

			lda	#<FComExitDImg		;Aktives DiskImage verlassen.
			ldx	#>FComExitDImg
			jsr	SendCom

::sd_dir_mode		lda	DiskImgTyp
			asl
			tay
			lda	DImgTypeList +0,y	;Kennung D64/D71/D81/DNP in
			sta	FComDImgList +5		;Verzeichnis-Befehl eintragen.
			lda	DImgTypeList +1,y
			sta	FComDImgList +6

			lda	#$00			;Anzahl Einträge löschen.
			sta	ListEntries

			sta	cntEntries +0		;Anzahl Dateien = 0.
			sta	cntEntries +1		;Anzahl Verzeichnisse = 0.

			jsr	ADDR_RAM_r15		;Anfang Verzeichnis im RAM.

;*** Verzeichnisse hinzufügen.
:AddDirData		lda	#<FComSDirList		;Liste mit Verzeichnissen einlesen.
			ldx	#>FComSDirList
			ldy	#$ff
			jsr	GetDirList

;*** Dateienn hinzufügen.
:AddFileData		lda	#<FComDImgList		;Liste mit DiskImages einlesen.
			ldx	#>FComDImgList
			ldy	#$00
			jmp	GetDirList

;*** Prüfen ob SD2IEC und DiskImage oder Verzeichnis-Modus.
;Dazu wird das aktuelle Verzeichnis eingelesen und die Anzahl der
;freien Blocks geprüft. Sofern die SD-Karte nicht randvoll mit
;Daten ist werden hier "65535 Blocks free" angezeigt.
;Der Wert "65535" ist nur bei einem SD2IEC möglich.
:CkSD2IECMode		lda	#$00			;Anzahl "BlocksFree" löschen.
			sta	Blocks +0
			sta	Blocks +1

			LoadW	r13,FComCkSDMode	;Nur Verzeichnis-Info einlesen.
			jsr	ADDR_RAM_r15		;Anfang Verzeichnis im RAM.
			jsr	GetDir_BASIC
			txa
			bne	:0

			lda	Blocks +0		;"BlocksFree" = 65535 ?
			cmp	#$ff
			bne	:1
			lda	Blocks +1
			cmp	#$ff
			bne	:1
::0			ldx	#$00			; => Ja, SD2IEC-Verzeichnis.
			b $2c
::1			ldx	#$0e			; => Nein, SD2IEC-DiskImage.
			rts

;*** Daten an Floppy senden.
:SendCom		sta	r0L
			stx	r0H

			jsr	PurgeTurbo		;GEOS-Turbo aus und I/O einschalten.
			jsr	InitForIO
			jsr	:100			;Befehl senden.
			jmp	DoneWithIO

::100			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.

			lda	#$00
			sta	STATUS			;Status löschen.
			lda	curDrive
			jsr	LISTEN			;LISTEN-Signal auf IEC-Bus senden.
			lda	#$ff			;Befehlskanal #15.
			jsr	SECOND			;Sekundär-Adr. nach LISTEN senden.

			lda	STATUS			;Laufwerk vorhanden ?
			bne	:103			;Nein, Abbruch...

			ldy	#$01			;Zähler für Anzahl Bytes einlesen.
			lda	(r0L),y
			sta	r1H
			dey
			lda	(r0L),y
			sta	r1L
			AddVBW	2,r0			;Zeiger auf Befehlsdaten setzen.
			jmp	:102

::101			lda	(r0L),y			;Byte aus Speicher
			jsr	CIOUT			;lesen & ausgeben.
			iny
			bne	:102
			inc	r0H
::102			SubVW	1,r1			;Zähler Anzahl Bytes korrigieren.
			bcs	:101			;Schleife bis alle Bytes ausgegeben.

			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.
			ldx	#$00			;Flag: "Kein Fehler!"
			rts

::103			jsr	UNLSN			;UNLISTEN-Signal auf IEC-Bus senden.
			ldx	#$ff			;Flag: "Fehler!"
			rts
