﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Neuen Druckertreiber laden.
:ChangePrinter		LoadB	r7L,PRINTER		;Dateityp festlegen.
			LoadW	r10,$0000		;Keine GEOS-Klasse.
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	InstallPrinter
			rts

:InstallPrinter		LoadW	r0,dataFileName		;Druckertreiber laden.
:LoadPrinter		LoadW	r6,PrntFileName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString		;Druckername kopieren.

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Druckertreiber einlesen.
			txa
			bne	OpenPrntError		; => Nein, weiter...
			;jmp	OpenPrntOK

;*** Info: Drucker installiert.
:OpenPrntOK		LoadW	r0,:dlgbox		;Hinweis ausgeben:
			jsr	DoDlgBox		;Drucker wurde installiert.
			ldx	#NO_ERROR
			rts

::dlgbox		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$18,$30
			w PrntFileName
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "Der Drucker wurde installiert:"
			b BOLDON,NULL

;*** Fehler: Drucker konnte nicht geöffnet werden.
:OpenPrntError		lda	#$00			;Druckername löschen.
			sta	PrntFileName
			LoadW	r0,:dlgbox		;Hinweis ausgeben:
			jsr	DoDlgBox		;Drucker nicht installiert.
			ldx	#DEV_NOT_FOUND
			rts

::dlgbox		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$2a
			w :2
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "GeoDesk konnte den Drucker",NULL
::2			b "nicht installieren!",NULL

;*** Neuen Eingabetreiber laden.
:ChangeInput		LoadB	r7L,INPUT_DEVICE	;Dateityp festlegen.
			LoadW	r10,$0000		;Keine GEOS-Klasse.
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			bne	:1			; => Nein, weiter...

			LoadW	r0,dataFileName		;Name Eingabetreiber kopieren.
			LoadW	r6,inputDevName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString		;Name Eingabetreiber kopieren.

			LoadW	r7 ,MOUSE_BASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Eingabetreiber einlesen.
			jmp	InitMouse		;Initialisieren.
::1			rts

;*** Datei auswählen.
;    Übergabe:		r7L  = Datei-Typ.
;			r10  = Datei-Klasse.
;    Rückgabe:		In ":dataFileName" steht der Dateiname.
;			xReg = $00, Datei wurde ausgewählt.
:OpenFile		MoveB	r7L,:OpenFile_Type
			MoveW	r10,:OpenFile_Class

::1			ldx	curDrive
			lda	driveType -8,x		;Aktuelles Laufwerk gültig?
			bne	:3			; => Ja, weiter...

			ldx	#8			;Gültiges Laufwerk suchen.
::2			lda	driveType -8,x
			bne	:3
			inx
			cpx	#12
			bcc	:2
			ldx	#$ff
			rts

::3			txa				;Laufwerk aktivieren.
			jsr	SetDevice

;--- Dateiauswahlbox.
::4			MoveB	:OpenFile_Type ,r7L
			MoveW	:OpenFile_Class,r10
			LoadW	r5 ,dataFileName
			LoadB	r7H,255
			LoadW	r0,:Dlg_SlctFile
			jsr	DoDlgBox		;Datei auswählen.

			lda	sysDBData		;Laufwerk wechseln ?
			bpl	:5			; => Nein, weiter...

			and	#%00001111
			jsr	SetDevice		;Neues Laufwerk aktivieren.
			txa				;Laufwerksfehler ?
			beq	:4			; => Nein, weiter...
			bne	:1			; => Ja, gültiges Laufwerk suchen.

::5			cmp	#DISK			;Partition wechseln ?
			beq	:4			; => Ja, weiter...
			ldx	#$ff
			cmp	#CANCEL			;Abbruch gewählt ?
			beq	:6			; => Ja, Abbruch...
			inx
::6			rts

::OpenFile_Type		b $00
::OpenFile_Class	w $0000

::Dlg_SlctFile		b $81
			b DBGETFILES!DBSETDRVICON ,$00,$00
			b CANCEL                  ,$00,$00
			b DISK                    ,$00,$00
			b OK,0,0
			b NULL
