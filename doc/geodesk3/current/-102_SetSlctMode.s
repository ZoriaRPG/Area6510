﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   SetFileSlctMode
;Parameter: AKKU= Auswahlmodus:
;                 $00 -> Nicht ausgewählt.
;                 $FF -> Ausgewählt.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Auswahlflag für alle Dateien setzen/löschen.
;******************************************************************************
:SetFileSlctMode	sta	r10L			;Markierungsmodus merken.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			bne	:do_select		; => Dateien vorhanden, weiter.
			rts				;Keine Dateien, Ende.

::do_select		jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.

			LoadW	r2,32			;Größe Verzeichnis-Eintrag setzen.

			ClrW	r11			;Zähler Dateien im Speicher löschen.

::next_file		ldy	#$02
			lda	(r0L),y			;Dateityp-Byte einlesen.
			cmp	#$ff			;"Weitere Dateien"?
			beq	:exit			; => Ja, Ende...

			ldy	#$00
			lda	r10L			;Markierungsmodus in Speicher
			sta	(r0L),y			;schreiben.

			jsr	StashRAM		;Eintrag in Cache speichern.

			AddVBW	32,r0			;Zeiger auf nächsten Eintrag/Cache.
			AddVBW	32,r1

			IncW	r11			;Zähler auf nächste Datei.

			CmpW	r11,WM_DATA_MAXENTRY
			bne	:next_file		;Nächste Datei markieren.

::exit			rts
