﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* GeoDesk von Disk starten.
;* GeoDesk aus DACC starten.

if .p
			t "TopSym"
			t "TopSym.MP3"
			t "TopSym.GD"
			t "TopMac.GD"
			t "-SYS_APPLINK"
endif

			n "mod.#101.obj"
			t "-SYS_CLASS.h"
			f APPLICATION
			o APP_RAM
			p MainInit
			a "Markus Kanet"

;*** Einsprungtabelle für Modul-Funktionen.
;Sofern hir weitere Routinen hinzugefügt
;werden muss ":GD_JMPTBL_COUNT" in
;":TopSym.GD" angepasst werden!
:VlirJumpTable		jmp	MNU_BOOT
			jmp	MNU_REBOOT

;------------------------------------------------------------------------------
;Reservierter Bereich für System-
;Variablen. Bereich nicht verschieben,
;da Boot/SaveConfig darauf zugreift.
;------------------------------------------------------------------------------
;*** Systemvariablen.
			t "-SYS_VAR"

;*** Programmvariablen.
			t "-101_VarDataGD"
			t "-101_VarDataWM"
;------------------------------------------------------------------------------

;*** Weitere Variablen, werden nicht gespeichert.
			t "-101_VarDataMisc"

;*** Fenstermanager.
			t "-101_WM.extern"
			t "-101_WM.intern"
			t "-101_WM.screen"
			t "-101_WM.scrbar"
			t "-101_WM.drive"
			t "-101_WM.mouse"
			t "-101_WM.icons"

;*** Systemroutinen.
			t "-101_SwitchDrive"
			t "-101_DlgTitle"
			t "-101_DrawClock"
			t "-101_UpdateCore"
			t "-101_LoadVLIR"
			t "-101_UpdateVLIR"
			t "-101_AppLinkDev"
			t "-101_AppLinkData"
			t "-101_ResetFontGD"
			t "-101_CopyFName"
			t "-101_DirDataOp"

;*** System-Icons.
			t "-101_SystemIcons"

;*** Spezieller Zeichensatz (7x8)
.FontG3			v 7,"fnt.GeoDesk"

;*** Beginn Speicher für VLIR-Module.
.VLIR_BASE
