﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS-Editor starten.
:OpenEditor		jsr	FindEditor		;GEOS.Editor suchen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			LoadW	a0,dirEntryBuf -2	;Zeiger auf Verzeichnis-Eintrag.
			jmp	StartFile_a0		;Anwendung starten.

::error			lda	#<Dlg_ErrEditor1
			ldx	#>Dlg_ErrEditor1
			jmp	PrntErrorExit		;Fehler ausgeben => Desktop.

;*** GEOS.Editor auf Disk suchen.
:FindEditor		lda	#<FNameEdit64		;Name für "GEOS64.Editor".
			ldx	#>FNameEdit64
			sta	FNameEditPtr +0
			stx	FNameEditPtr +1

			lda	#%10000000		;GEOS.Editor auf
			jsr	FindEditFile		;RAM-Laufwerken suchen.
			txa
			beq	:1
			lda	#%00000000		;GEOS.Editor auf
			jsr	FindEditFile		;Disk-Laufwerken suchen.
::1			rts

;*** Datei auf den Laufwerken A: bis D: suchen.
:FindEditFile		sta	:2 +1

			ldx	curDrive
			stx	r15L
::1			stx	r15H
			lda	driveType -8,x		;Laufwerk verfügbar?
			beq	:3			; => Nein, weiter...
			and	#%10000000
::2			cmp	#$ff			;Gesuchter Laufwerkstyp?
			bne	:3			; => Nein, weiter...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.

			jsr	OpenDisk		;Diskette öffnen.
			txa				;Disk-Fehler?
			bne	:3			; => Ja, weiter...

			MoveW	FNameEditPtr,r6
			jsr	FindFile		;Editor-Datei-Eintrag suchen.
			txa				;Gefunden?
			beq	:5			; => Ja, Ende...

::3			ldx	r15H			;Zeiger auf nächstes Laufwerk.
			inx
			cpx	r15L			;Alle Laufwerke durchsucht?
			beq	:4			; => Ja, Ende...
			cpx	#12
			bcc	:1
			ldx	#$08
			bne	:1			;Auf nächsten Laufwerk weitersuchen.

::4			ldx	#$ff			;Nicht gefunden.

;--- Falls Editor nicht gefunden, Laufwerk zurücksetzen.
::5			txa
			beq	:7
			pha
			lda	r15L 			;Vorheriges Laufwerk wieder
			jsr	SetDevice		;aktivieren.
			pla
			tax
::7			rts

:FNameEdit64		;b "GD.SETUP",NULL
			b "GEOS64.Editor",NULL
:FNameEditPtr		w $0000

;*** Dialogboxen.
:Dlg_ErrEditor1		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w :3
			b DBTXTSTR   ,$0c,$3c
			w :4
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk konnte die Datei",NULL
::3			b BOLDON
			b "GEOS64.Editor",NULL
::4			b PLAINTEXT
			b "nicht finden!",NULL
