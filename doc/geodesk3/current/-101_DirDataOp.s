﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   SET_LOAD_DISK
;Parameter: -
;Rückgabe:  GD_RELOAD_DIR = $80 => Verzeichnis von Disk einlesen.
;Verändert: A
;Funktion:  Flag setzen "Verzeichnis von Disk neu einlesen".
;******************************************************************************
.SET_LOAD_DISK		lda	#%1000 0000		;Dateien immer von Disk einlesen.
			b $2c

;******************************************************************************
;Routine:   SET_TEST_CACHE
;Parameter: -
;Rückgabe:  GD_RELOAD_DIR = $40 => Verzeichnis von Cache oder Disk einlesen.
;Verändert: A
;Funktion:  Flag setzen "Verzeichnis von Cache oder Disk einlesen".
;******************************************************************************
.SET_TEST_CACHE		lda	#%0100 0000		;Dateien aus Cache oder von Disk.
			b $2c

;******************************************************************************
;Routine:   SET_SORT_MODE
;Parameter: -
;Rückgabe:  GD_RELOAD_DIR = $3F => Verzeichnis im Speicher sortieren.
;Verändert: A
;Funktion:  Flag setzen "Verzeichnis im Speicher sortieren".
;******************************************************************************
.SET_SORT_MODE		lda	#%0011 1111		;Nur Dateien sortieren.
			b $2c

;******************************************************************************
;Routine:   SET_LOAD_CACHE
;Parameter: -
;Rückgabe:  GD_RELOAD_DIR = $00 => Verzeichnis aus Cache einlesen.
;Verändert: A
;Funktion:  Flag setzen "Verzeichnis aus Cache einlesen".
;******************************************************************************
.SET_LOAD_CACHE		lda	#%0000 0000		;Dateien aus Cache einlesen.
			sta	GD_RELOAD_DIR
			rts

;******************************************************************************
;Routine:   SET_POS_CACHE
;Parameter: WM_WCODE = Fenster-Nr.
;           r14  = Nr. Eintrag in Dateitabelle.
;Rückgabe:  r14  = Zeiger auf Verzeichnis-Cache.
;           r13  = Zeiger auf Icon-Cache oder $0000=Kein Cache.
;           r12L = Speicherbank Icon-Cache.
;           r12H = Speicherbank Verzeichnis-Cache.
;Verändert: A,X,Y,r6-r8,r12-r14
;Funktion:  Zeiger auf Datei-Eintrag im Cache berechnen.
;******************************************************************************
.SET_POS_CACHE		PushW	r14			;Zeiger auf Eintrag-Nr. speichern.

			LoadB	r12L,32			;Größe Verzeichnis-Eintrag.

			ldx	#r14L			;Position des Datei-Eintrages im
			ldy	#r12L			;RAM-Cache berechnen.
			jsr	BMult

			PopW	r13			;Eintrag-Nr. einlesen.

			LoadB	r12L,64			;Größe Icon-Eintrag.

			ldx	#r13L			;Position des Icon-Eintrages im
			ldy	#r12L			;RAM-Cache berechnen.
			jsr	BMult

			lda	WM_WCODE
			asl
			tax

			clc				;Zeiger auf Verzeichnis-Cache
			lda	r14L			;in REU berechnen.
			adc	vecDirDataRAM +0,x
			sta	r14L
			lda	r14H
			adc	vecDirDataRAM +1,x
			sta	r14H

			clc				;Zeiger auf Icon-Cache
			lda	r13L			;in REU berechnen.
			adc	vecIconDataRAM +0,x
			sta	r13L
			lda	r13H
			adc	vecIconDataRAM +1,x
			sta	r13H

			lda	GD_SYSDATA_BUF		;64K-Speicher Verzeichnis-Cache.
			sta	r12H

			lda	GD_ICONDATA_BUF		;64K-Speicher Icon-Cache.
			sta	r12L

			rts

;******************************************************************************
;Routine:   SET_POS_RAM
;Parameter: XReg = Zero-Page-Adresse Faktor #1.
;           YReg = Zero-Page-Adresse Faktor #2.
;                  Der Wert selbst wird durch die Routine gesetzt.
;Rückgabe:  Zero-Page Faktor#1 erhält Adresse im RAM.
;Verändert: A,X,Y,r6-r8
;Funktion:  Zeiger auf Eintrag im Speicher berechnen.
;******************************************************************************
.SET_POS_RAM		stx	:1 +1

			lda	#$20			;Größe Verzeichnis-Eintrag.
			sta	zpage,y
			jsr	BMult			;Anzahl Einträge x 32 Bytes.

::1			ldx	#$ff
			lda	zpage +0,x
			clc
			adc	#<BASE_DIR_DATA
			sta	zpage +0,x
			lda	zpage +1,x
			adc	#>BASE_DIR_DATA
			sta	zpage +1,x
			rts

;******************************************************************************
;Routine:   ADDR_CACHE
;Parameter: XReg = Zero-Page-Adresse für Startadresse Verzeichnis-Cache.
;Rückgabe:  Zero-Page-Adresse enthält Zeiger auf Verzeichnis-Cache.
;Verändert: A,X
;Funktion:  Startadresse Verzeichnis-Cache in REU setzen.
;******************************************************************************
.ADDR_CACHE_r1		ldx	#r1L
.ADDR_CACHE		lda	WM_WCODE
			asl
			tay
			lda	vecDirDataRAM +0,y
			sta	zpage +0,x
			lda	vecDirDataRAM +1,y
			sta	zpage +1,x
			rts

;******************************************************************************
;Routine:   ADDR_ICON
;Parameter: XReg = Zero-Page-Adresse für Startadresse Icon-Cache.
;Rückgabe:  Zero-Page-Adresse enthält Zeiger auf Icon-Cache.
;Verändert: A,X
;Funktion:  Startadresse Icon-Cache in REU setzen.
;******************************************************************************
.ADDR_ICON_r1		ldx	#r1L
.ADDR_ICON		lda	WM_WCODE
			asl
			tay
			lda	vecIconDataRAM +0,y
			sta	zpage +0,x
			lda	vecIconDataRAM +1,y
			sta	zpage +1,x
			rts

;******************************************************************************
;Routine:   ADDR_RAM_rXX
;Parameter: XReg = Zero-Page-Adresse für ":BASE_DIR_DATA".
;Rückgabe:  Zero-Page-Adresse enthält Zeiger auf ":BASE_DIR_DATA".
;Verändert: A,X
;Funktion:  Zeiger auf ":BASE_DIR_DATA" im Speicher setzen.
;******************************************************************************
.ADDR_RAM_r15		ldx	#r15L
.ADDR_RAM_x		lda	#<BASE_DIR_DATA
			sta	zpage +0,x
			lda	#>BASE_DIR_DATA
			sta	zpage +1,x
			rts

;******************************************************************************
;Routine:   SET_CACHE_DATA
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  r0  = Zeiger auf ":BASE_DIR_DATA".
;           r1  = Zeiger auf Cache in DACC.
;           r2  = Cache-Größe.
;           r3L = 64Kb Speicherbank für Cache.
;Verändert: A,X,r0-r3L
;Funktion:  Zeiger auf ":BASE_DIR_DATA" im Speicher setzen.
;******************************************************************************
.SET_CACHE_DATA		ldx	#r0L
			jsr	ADDR_RAM_x

			jsr	ADDR_CACHE_r1

			LoadW	r2,MAX_DIR_ENTRIES *32

			lda	GD_SYSDATA_BUF
			sta	r3L
			rts
