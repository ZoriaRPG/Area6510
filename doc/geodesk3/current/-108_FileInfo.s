﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Datei-Eigenschaften anzeigen.
:xFILE_INFO		jsr	COPY_FILE_NAMES		;Dateinamen in Speicher kopieren.

			LoadB	curFile,$00		;Zeiger auf erste Datei.
			LoadW	curFileVec,filesBuf

			jsr	GetFileData		;Datei-Info einlesen.

;--- Register-Menü initialisieren.
;			ClrB	regUpdate		;Flag löschen "Update Register".
			ClrB	reloadDir		;Flag löschen "Verzeichnis laden".

			jsr	SetADDR_Register	;RegisterMenü-Routine einlesen.
			jsr	FetchRAM

			jsr	RegisterSetFont		;Register-Font aktivieren.

			LoadW	r0,RegMenu1
			jsr	DoRegister		;Register-Menü starten.

;--- Icon-Menü "Beenden" initialisieren.
			lda	C_WinIcon		;Farbe für "X"-Icon setzen.
			jsr	i_UserColor
			b	(R1SizeX0/8) +1
			b	(R1SizeY0/8) -1
			b	IconExit_x
			b	IconExit_y/8

			LoadW	r0,IconMenu
			jmp	DoIcons			;Icon-Menü aktivieren.

;*** Zurück zum DeskTop.
:Exit			bit	GD_INFO_SAVE		;Datei-Info automatisch speichern?
			bpl	:1			; => Nein, weiter...
			jsr	doSaveData		;Datei-Info speichern.

::1			jsr	StopTextEdit		;Datei-Infotext-Eingabe beenden.
			jsr	ExitRegisterMenu	;Register-Menü beenden.

			bit	reloadDir		;Verzeichnis neu laden?
			bpl	:2			; => Nein, weiter...

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen, da
							;evtl. Dateien umbenannt.

::2			jmp	MNU_UPDATE		;Zurück zum Hauptmenü.

;*** Datei-Informationen einlesen.
:GetFileData		jsr	i_FillRam		;Datei-Informationen löschen.
			w	(FILE_VAR_END - FILE_VAR_START)
			w	FILE_VAR_START
			b	$00

			lda	curFileVec +0		;Zeiger auf aktuellen
			sta	r6L			;Dateinamen setzen.
			lda	curFileVec +1
			sta	r6H

			ldy	#0			;Aktuellen Dateinamen in
::0			lda	(r6L),y			;Zwischenspeicher kopieren.
			sta	curFileName,y
			iny
			cpy	#16
			bcc	:0

			jsr	FindFile		;Datei auf Disk suchen.
			txa				;Fehler?
			beq	:copy_data		; => Nein, weiter...

::no_data		lda	#$ff			;Datei-Eintrag "ungültig".
			sta	curDirEntry +0
			sta	curDirEntry +1
			rts

::copy_data		ldy	#30 -1			;Verzeichnis-Eintrag in
::1			lda	dirEntryBuf,y		;Zwischenspeicher kopieren.
			sta	curDirEntry +2,y
			dey
			bpl	:1

			lda	curDirEntry +30		;Größe in Blocks einlesen.
			sta	r0L
			lda	curDirEntry +31
			sta	r0H

			lda	r0L
			pha
			ldx	#r0L
			ldy	#$02
			jsr	DShiftRight		;Blocks in KBytes umrechnen.
			pla
			and	#%00000011		;Auf volle KByte aufrunden?
			beq	:2			; => Bereits volle KByte, weiter...

			IncW	r0

::2			lda	r0L			;Größe in KByte speichern.
			sta	curDirEntry +0
			lda	r0H
			sta	curDirEntry +1

			lda	curDirEntry +2		;CBM-Dateityp einlesen.
			and	#%0000 0111		;Dateityp-Bits isolieren.
			asl				;Zeiger auf Text für Dateityp
			asl				;berechnen.
			tax
			ldy	#$00
::3			lda	cbmFType,x		;CBM-Dateityp als Text in
			sta	curFileType,y		;Zwischenspeicher kopieren.
			inx
			iny
			cpy	#3
			bcc	:3

::fileSEQ		lda	#<fileStructSEQ		;Zeiger auf Text "SEQ" für
			ldx	#>fileStructSEQ		;Sequentielle Dateistruktur.
			ldy	curDirEntry +23		;Dateistruktur = SEQ?
			beq	:5			; => Ja, weiter...

::fileVLIR		lda	#<fileStructVLIR	;Zeiger auf Text "SEQ" für
			ldx	#>fileStructVLIR	;GEOS-VLIR Dateistruktur.

::5			sta	RTabMenu1_1a +0		;Zeiger auf Text für
			stx	RTabMenu1_1a +1		;Dateistruktur speichern.

			lda	#BOX_STRING_VIEW
			sta	RTabMenu1_2e		;GEOS-Autor deaktivieren.
			lda	#BOX_ICON_VIEW
			sta	RTabMenu1_2c		;GEOS-Dateityp deaktivieren.
			sta	RTabMenu1_2f		;GEOS-ScreenMode deaktivieren.

			lda	curDirEntry +24		;GEOS-Datei?
			beq	:exit			; => Nein, Ende...
			lda	curDirEntry +21		;Infoblock definiert?
			beq	:exit			; => Nein, Ende...

			jsr	readGeosHeader		;GEOS-InfoBlock einlesen.

			lda	#$ff			;GEOS-Datei.
			b $2c
::exit			lda	#$00			;CBM-Datei/Verzeichnis.
			sta	curFileGEOS

			jmp	DefGEOSType		;GEOS-Dateityp/Verzeichnis setzen.

;*** Daten aus GEOS-InfoBlock einlesen.
:readGeosHeader		LoadW	r9,dirEntryBuf		;Zeiger auf Verzeichnis-Eintrag.
			jsr	GetFHdrInfo		;InfoBlock einlesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			jsr	i_MoveData		;InfoBlock in Zwischenspeicher.
			w	fileHeader
			w	curFHdrInfo
			w	256

			jsr	i_MoveData		;GEOS-Klasse in Zwischenspeicher.
			w	fileHeader +$4d
			w	curClass
			w	12

			jsr	i_MoveData		;Klasse/Version in Zwischenspeicher.
			w	fileHeader +$4d +12
			w	curClassVer
			w	4

			lda	#BOX_STRING
			sta	RTabMenu1_2e		;GEOS-Autor aktivieren.
			lda	#BOX_ICON
			sta	RTabMenu1_2c		;GEOS-Dateityp aktivieren.
			sta	RTabMenu1_2f		;GEOS-ScreenMode aktivieren.

			jsr	DefScrnMode		;Bildschirm-Modus einlesen.

			jsr	DefGEOSload		;GEOS-Lade/Ende/Start-Adressen.
			jsr	DefGEOSend
			jsr	DefGEOSrun

::exit			rts

;*** Datei-Informationen speichern.
;Am Ende Register-Karte neu laden.
:SaveFileData		jsr	doSaveData		;Datei-Informationen speichern.
			jmp	ResetFileInfo		;Register-Karte zurücksetzen.

;*** Datei-Informationen speichern.
:doSaveData		lda	curDirEntry +0		;Datei-Info gültig?
			cmp	#$ff
			bne	:1
			lda	curDirEntry +1
			cmp	#$ff
			beq	:exit			; => Nein, Ende

::1			LoadW	r6,curFileName		;Zeiger auf Dateiname.

			lda	curFileVec +0		;Zeiger auf Original-Dateiname.
			sta	r7L
			lda	curFileVec +1
			sta	r7H

			ldx	#r6L
			ldy	#r7L
			jsr	CmpString		;Wurde Name geändert?
			beq	:skip_file_name		; => Nein, weiter...

			jsr	FindFile		;Neue Datei suchen.
			txa				;Gefunden?
			bne	:rename_ok		; => Nein, OK. Eeiter...

			LoadW	r0,Dlg_ErrRename	;Fehler anzeigen "File exist!".
			jsr	DoDlgBox

::skip_file_name	lda	#$ff			;Datei nicht umbenennen.
			b $2c
::rename_ok		lda	#$00			;Datei nicht umbenennen.
			sta	renameFile		;Rename-Flag speichern.

			lda	curFileVec +0		;Verzeichnis-Eintrag zu
			sta	r6L			;Original-Dateiname suchen.
			lda	curFileVec +1
			sta	r6H

			jsr	FindFile		;Neue Datei suchen.
			txa				;Gefunden?
			bne	:exit			; => Nein, Ende...

::copy_data		LoadB	reloadDir,$ff		;Dateien geändert, Verzeichnis
							;in GeoDesk neu einlesen.

::rename_file		bit	renameFile		;Datei umbenennen?
			bmi	:2			; => Nein, weiter...

			jsr	updateFileName		;Dateiname ändern.

::2			jsr	updateDirEntry		;CBM-Daten speichern.

			bit	curFileGEOS		;GEOS-Datei?
			bmi	:3			; => Nein, weiter...

			bit	renameFile		;Datei umbenennen?
			bmi	:exit			; => Nein, weiter...

			jmp	updateDirHeader		;Verzeichnis-Header umbenennen.
::3			jmp	updateGeosHeader	;GEOS-InfoBlock aktualisieren.
::exit			rts

;*** Dateiname aktualisieren.
;    Übergabe: r5 = Zeiger auf 30Byte-Verzeichnis-Eintrag.
;              r7 = Zeiger auf Dateiliste.
;              curFileName = Neuer Dateiname.
:updateFileName		ldy	#0			;Dateiname in aktueller
::1			lda	curFileName,y		;Dateiliste aktualisieren.
			sta	(r7L),y
			iny
			cpy	#16
			bcc	:1

			ldx	#0			;Dateiname im Verzeichnis-
			ldy	#3			;Eintrag aktualisieren.
::2			lda	curFileName,x
			beq	:3
			sta	(r5L),y
			iny
			inx
			cpx	#16
			bcc	:2
			beq	:5
::3			lda	#$a0			;Auf 16 Zeichen mit $A0
::4			sta	(r5L),y			;auffüllen.
			iny
			inx
			cpx	#16
			bcc	:4
::5			rts

;*** Datei-Informationen aktualisieren.
;    Übergabe: r1L/r1H = Track/Sektor Verzeichnis-Eintrag.
;              r5      = Zeiger auf 30Byte-Verzeichnis-Eintrag.
;              curDirEntry = Kopie Verzeichnis-Eintrag.
:updateDirEntry		ldy	#0
			lda	curDirEntry +2		;Dateityp-Flag mit "Geschlossen" und
			sta	(r5L),y			;"Schreibschutz"-Status speichern.

			ldx	#0
			ldy	#22			;GEOS-Dateityp, Datum und
::1			lda	curDirEntry +24,x	;Uhrzeit speichern.
			sta	(r5L),y
			iny
			inx
			cpx	#6
			bcc	:1

::6			;LoadW	r4,diskBlkBuf
			jmp	PutBlock

;*** GEOS-InfoBlock aktualisieren.
;    Übergabe: curDirEntry = Kopie Verzeichnis-Eintrag.
:updateDirHeader	lda	curDirEntry +2		;CBM-Dateityp einlesen.
			and	#%0000 0111		;Dateityp-Bits isolieren.
			cmp	#$06			;Verzeichnis?
			bne	:exit			; => Nein, Ende.

			lda	curDirEntry +3
			sta	r1L
			lda	curDirEntry +4
			sta	r1H
			LoadW	r4,diskBlkBuf		;Zeiger auf Zwischenspeicher.
			jsr	GetBlock		;Verzeichnis-Header einlesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			ldx	#0
::3			lda	curFileName,x		;Dateiname=Diskname in
			beq	:4			;Verzeichnis-Header kopieren.
			sta	diskBlkBuf +4,x
			inx
			cpx	#16
			bcc	:3
			beq	:6
::4			lda	#$a0			;Auf 16 Zeichen mit $A0
::5			sta	diskBlkBuf +4,x		;auffüllen.
			inx
			cpx	#16
			bcc	:5

::6			jsr	PutBlock		;Verzeichnis-Eintrag speichern.
::exit			rts

;*** GEOS-InfoBlock aktualisieren.
;    Übergabe: dirEntryBuf = Verzeichnis-Eintrag.
:updateGeosHeader	LoadW	r9,dirEntryBuf		;Zeiger auf Verzeichnis-Eintrag.
			jsr	GetFHdrInfo		;InfoBlock einlesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			ldy	#$02			;InfoBlock aus Zwischenspeicher
::1			lda	curFHdrInfo,y		;In Sektor kopieren.
			sta	fileHeader,y
			iny
			bne	:1

			lda	dirEntryBuf +19
			sta	r1L
			lda	dirEntryBuf +20
			sta	r1H
			jsr	PutBlock		;InfoBlock speichern.
::exit			rts

;*** Seite wechseln.
:SwitchPage		bit	GD_INFO_SAVE
			bpl	:100
			jsr	doSaveData

::100			sec				;Y-Koordinate der Maus einlesen.
			lda	mouseYPos		;Testen ob Maus innerhalb des
			sbc	#PosSlctPage_y		;"Eselsohrs" angeklickt wurde.
			bcs	:102
::101			rts				;Nein, Rücksprung.

::102			tay
			sec
			lda	mouseXPos+0
			sbc	#<PosSlctPage_x
			tax
			lda	mouseXPos+1
			sbc	#>PosSlctPage_x
			bne	:101
			cpx	#16			;Ist Maus innerhalb "Eselsohr" ?
			bcs	:101			;Nein, Rücksprung.
			cpy	#16
			bcs	:101
			sty	r0L
			txa				;Feststellen: Seite vor/zurück ?
			eor	#%00001111
			cmp	r0L
			bcs	:111			;Seite vor.
			bcc	:121			;Seite zurück.

;*** Weiter auf nächste Seite.
::111			ldx	curFile
			inx
			cpx	slctFiles
			bcc	:131
			ldx	#$00
			beq	:131

;*** Zurück zur letzten Seite.
::121			ldx	curFile
			bne	:122
			ldx	slctFiles
::122			dex

::131			stx	curFile

			stx	r0L
			LoadB	r1L,17
			ldx	#r0L
			ldy	#r1L
			jsr	BBMult

			lda	r0L
			clc
			adc	#<filesBuf
			sta	curFileVec +0
			lda	r0H
			adc	#>filesBuf
			sta	curFileVec +1

;*** Datei-Informationen einlesen.
:ResetFileInfo		jsr	GetFileData		;Datei-Info einlesen.

;--- Hinweis:
;regUpdate ist nur notwendig wenn
;die InfoBox der letzte Eintrag in der
;Register-Tabelle ist.
;ToDo: Evtl. Fehler in Register-Code?
:UpdateRegData
;			LoadB	regUpdate,$ff
			jsr	RegisterNextOpt
;			ClrB	regUpdate

			rts

;*** Datum auf Gültigkeit testen.
:chkDateDay		lda	curDirEntry +27		;Tag einlesen.
			beq	:1			; => Tag ungültig...

			cmp	#31 +1			;Tag > 31?
			bcc	:2			; => Nein, weiter...

			lda	#31			;max.Wert für Tag setzen.
			b $2c
::1			lda	#1			;min.Wert für Tag setzen.
			sta	curDirEntry +27		;Korrigierter Wert für Tag.
::2			rts

:chkDateMonth		lda	curDirEntry +26		;Monat einlesen.
			beq	:1			; => Monat ungültig...

			cmp	#12 +1			;Monat > 12?
			bcc	:2			; => Nein, weiter...

			lda	#12			;max.Wert für Monat setzen.
			b $2c
::1			lda	#1			;min.Wert für Monat setzen.
			sta	curDirEntry +26		;Korrigierter Wert für Monat.
::2			rts

;*** Jahreszahl testen.
;Hinweis: Ist immer gültig.
;hier könnte aber das Jahrtausend
;für die Anzeige gesetzt werden.
:chkDateYear		rts

;*** Uhrzeit auf Gültigkeit testen.
:chkDateHour		lda	curDirEntry +28		;Stunde einlesen.
			cmp	#23 +1			;Stunde > 23?
			bcc	:1			; => Nein, weiter...

			lda	#23			;max.Wert für Stunde setzen.
			sta	curDirEntry +28		;Korrigierter Wert für Stunde.

::1			rts

:chkDateMinute		lda	curDirEntry +29		;Minute einlesen.
			cmp	#59 +1			;Minute > 59?
			bcc	:1

			lda	#59			;max.Wert für Minute setzen.
			sta	curDirEntry +29		;Korrigierter Wert für Minute.

::1			rts

;*** Aktuelle uhrzeit auf Datei anwenden.
:SetCurTime		lda	day			;Datum aktualisieren.
			sta	curDirEntry +27
			lda	month
			sta	curDirEntry +26
			lda	year
			sta	curDirEntry +25

			lda	hour			;Uhrzeit aktualisieren.
			sta	curDirEntry +28
			lda	minutes
			sta	curDirEntry +29

			jmp	UpdateRegData		;Registerkarte aktualisieren.

;*** Texteingabe für InfoText beenden.
:InitRegTab		bit	r1L			;RegisterKarte aufbauen?
			bmi	:exit			; => Nein, Ende...
			jsr	StopTextEdit		;Texteingabe beenden.
::exit			rts

;*** Infotext eingeben
:DefInfoText
;			bit	regUpdate		;RegisterKarte zurücksetzen?
;			bmi	StopTextEdit		; => Ja, Texteingabe beenden...

			bit	curFileGEOS		;GEOS-Datei?
			bpl	StopTextEdit		; => Nein, Texteingabe beenden...

			LoadW	r0,curFHdrInfo +160
			jsr	InputText		;Texteingabe-Routine starten.
			jmp	RegisterSetFont		;Register-Font aktivieren.

;*** Texteingabe abschließen.
:StopTextEdit		jsr	InitForIO		;I/O-Bereich aktivieren.
			lda	C_Mouse			;Farbe für Cursor zurücksetzen.
			sta	$d027
			jsr	DoneWithIO		;I/O-Bereich ausblenden.

			jsr	PromptOff		;Cursor abschalten.

			lda	#$00			;Tastenabfrage löschen.
			sta	keyVector +0
			sta	keyVector +1

			lda	alphaFlag		;Cursor ausblenden.
			and	#%01111111
			sta	alphaFlag

::exit			jmp	RegisterSetFont		;Register-Font aktivieren.

;*** InfoText löschen.
:ClrInfoText		ldy	#$a0			;Zeiger auf erstes Zeichen InfoText.
			lda	#$00			;InfoText löschen.
::1			sta	curFHdrInfo,y
			iny
			bne	:1

			LoadW	r15,RTabMenu1_3b	;InfoText-Option aktualisieren.
			jmp	RegisterUpdate

;*** InfoBlock-Icon ausgeben.
:DefInfoIcon		lda	curDirEntry +2		;CBM-Dateityp einlesen.
			and	#%0000 0111		;Dateityp-Bits isolieren.
			cmp	#$06			;Verzeichnis?
			bne	:1			; => Nein, weiter...

			ldx	#<Icon_Map		;Zeiger auf Verzeichnis-Icon.
			ldy	#>Icon_Map
			jsr	drawIcon		;Icon ausgeben.

			LoadW	r0,textDIR		;"DIR" als Icon-Kennung ausgeben.
			SubVB	3,r1H
			AddVBW	11,r11
			jmp	PutString

::1			bit	curFileGEOS		;GEOS-Datei?
			bpl	:2			; => Nein, weiter...

			ldx	#<curFHdrInfo +4	;Zeiger auf InfoBlock-Icon.
			ldy	#>curFHdrInfo +4
			jsr	drawIcon		;Icon ausgeben.

			LoadW	r0,textGEOS		;"GEOS" als Icon-Kennung ausgeben.
			SubVB	3,r1H
			AddVBW	8,r11
			jmp	PutString

::2			ldx	#<Icon_CBM		;Zeiger auf CBM-Icon.
			ldy	#>Icon_CBM
			jsr	drawIcon		;Icon ausgeben.

			LoadW	r0,textCBM		;"CBM" als Icon-Kennung ausgeben.
			SubVB	3,r1H
			AddVBW	10,r11
			jmp	PutString

;*** Datei-Icon ausgeben.
:drawIcon		stx	r0L			;Zeiger auf Bitmap-Daten speichern.
			sty	r0H

			PushB	r2H			;Register zwischenspeichern.
			PushW	r3

			ldx	#r3L			;X-Koordinate in CARDs umrechnen.
			ldy	#3
			jsr	DShiftRight

			lda	r3L			;Größe für Icon-Leinwand
			clc				;berechnen.
			adc	#$01
			sta	r1L
			lda	r2L
			clc
			adc	#$08
			sta	r1H
			lda	#3
			sta	r2L
			lda	#21
			sta	r2H
			jsr	BitmapUp		;Bitmap darstellen.

			PopW	r11			;Register zurücksetzen als
			PopB	r1H			;Position für Icon-Kennung.

::exit			rts

;*** Neuen Bildschirm-Modus setzen.
;Dabei wird zwischen folgenden Werten
;gewechselt:
; $00 = Nur 40 Zeichen.
; $40 = 40- und 80-Zeichen.
; $80 = Nur GEOS64.
; $c0 = Nur 80 Zeichen.
:SetScrnMode		lda	curFHdrInfo +96		;Bildschirm-Modus einlesen.
			bne	:1
			lda	#%01000000		;Neuer Modus: "40- und 80-Zeichen".
			bne	:set_new_mode

::1			cmp	#%01000000
			bne	:2
			lda	#%10000000		;Neuer Modus: "Nur GEOS64".
			bne	:set_new_mode

::2			cmp	#%10000000
			bne	:3
			lda	#%11000000		;Neuer Modus: "Nur 80-Zeichen".
			bne	:set_new_mode

::3			lda	#%00000000		;Neuer Modus: "Nur 40-Zeichen".

::set_new_mode		sta	curFHdrInfo +96		;Neuen Modus speichern.
			jsr	DefScrnMode		;Text für RegisterMenü erzeugen.

			LoadW	r15,RTabMenu1_2d	;ScreenMode-Option aktualisieren.
			jmp	RegisterUpdate

;*** Text für Bildschirm-Modus für Registerenü erzeugen.
:DefScrnMode		lda	curFHdrInfo +96		;Bildschirm-Modus einlesen.
			jsr	GetScreenMode		;Zeiger auf Text setzen.
			sta	r0L			;Zeiger zwischenspeichern.
			sty	r0H

			LoadW	r1,curScrnMode		;Zeiger auf Zwischenspeicher.

			ldx	#r0L			;Textstring für Bildschirm-Modus in
			ldy	#r1L			;Zwischenspeicher kopieren.
			jmp	CopyString

;*** Neuen GEOS-Dateityp setzen.
:SetGEOSType		bit	curFileGEOS		;GEOS-Datei?
			bpl	:exit			; => Nein, weiter...

			ldx	curDirEntry +24		;GEOS-Dateityp einlesen.
			inx
			cpx	#15			;Unbekannter Typ?
			bcc	:1			; => Nein, weiter...
			ldx	#0			;Typ "Unknown" setzen.
::1			stx	curDirEntry +24		;GEOS-Dateityp speichern und
			stx	curFHdrInfo +69		;auch im InfoBlock anpassen.
			jsr	DefGEOSType		;Text für GEOS-Dateityp erzeugen.

			LoadW	r15,RTabMenu1_2b	;GEOS-Typ-Option aktualisieren.
			jmp	RegisterUpdate
::exit			rts

;*** Text für GEOS-Dateityp für Registerenü erzeugen.
:DefGEOSType		LoadW	r15,curDirEntry		;Zeiger auf Text für
			jsr	GetGeosType		;GEOS-Dateityp einlesen.
			sta	r0L			;Zeiger zwischenspeichern.
			sty	r0H

			LoadW	r1,curGEOSType		;Zeiger auf Zwischenspeicher.

			ldx	#r0L			;Textstring für Bildschirm-Modus in
			ldy	#r1L			;Zwischenspeicher kopieren.
			jmp	CopyString

;*** GEOS-Ladeadresse einlesen.
:DefGEOSload		lda	curFHdrInfo +$47
			sta	r0L
			lda	curFHdrInfo +$48
			sta	r0H
			LoadW	r1,curAdrGEOSload
			jmp	Word2ASCII

;*** GEOS-Endadresse einlesen.
:DefGEOSend		lda	curFHdrInfo +$49
			sta	r0L
			lda	curFHdrInfo +$4a
			sta	r0H
			LoadW	r1,curAdrGEOSend
			jmp	Word2ASCII

;*** GEOS-Startadresse einlesen.
:DefGEOSrun		lda	curFHdrInfo +$4b
			sta	r0L
			lda	curFHdrInfo +$4c
			sta	r0H
			LoadW	r1,curAdrGEOSrun
			jmp	Word2ASCII

;*** WORD nach ASCII konvertieren.
;    Übergabe: r0 = Hex-Zahl als WORD.
;    Rückgabe: r1 = 4 ASCII-zeichen für Hex-Zahl/WORD.
:Word2ASCII		lda	r0L			;LOW-Byte zwischenspeichern.
			pha

			lda	r0H			;HIGH-Byte einlesen und
			jsr	HEX2ASCII		;nach ASCII wandeln.

			ldy	#$01
			sta	(r1L),y			;LOW-Nibble HIGH-Byte.
			dey
			txa
			sta	(r1L),y			;HIGH-Nibble HIGH-Byte.

			pla				;LOW-Byte einlesen und
			jsr	HEX2ASCII		;nach ASCII wandeln.

			ldy	#$03
			sta	(r1L),y			;LOW-Nibble LOW-Byte.
			dey
			txa
			sta	(r1L),y			;HIGH-Nibble LOW-Byte.
			rts

;*** HEX-Zahl nach ASCII wandeln.
;    Übergabe: AKKU = Hex-Zahl.
;    Rückgabe: AKKU/XREG = LOW/HIGH-Nibble Hex-Zahl.
:HEX2ASCII		pha				;HEX-Wert speichern.
			lsr				;HIGH-Nibble isolieren.
			lsr
			lsr
			lsr
			jsr	:1			;HIGH-Nibble nach ASCII wandeln.
			tax				;Ergebnis zwischenspeichern.

			pla				;HEX-Wert zurücksetzen und
							;nach ASCII wandeln.
::1			and	#%00001111
			clc
			adc	#"0"
			cmp	#$3a			;Zahl größer 10?
			bcc	:2			;Ja, weiter...
			clc				;Hex-Zeichen nach $A-$F wandeln.
			adc	#$07
::2			rts

;*** InfoText in Zwischenspeicher #1-#3 kopieren.
:doSaveBuf1		lda	#<bufInfoText1		;Adresse Zwischenspeicher #1.
			ldx	#>bufInfoText1
			bne	doSaveBuf

:doSaveBuf2		lda	#<bufInfoText2		;Adresse Zwischenspeicher #2.
			ldx	#>bufInfoText2
			bne	doSaveBuf

:doSaveBuf3		lda	#<bufInfoText3		;Adresse Zwischenspeicher #3.
			ldx	#>bufInfoText3

:doSaveBuf		bit	curFileGEOS		;GEOS-Datei?
			bpl	:exit			; => Nein, Ende...

			sta	r1L			;Zeiger auf Zwischenspeicher
			stx	r1H			;sichern.

			LoadW	r0,curFHdrInfo +160
			jmp	CopyInfoText		;InfoText in Zwischenspeicher.
::exit			rts

;*** InfoText aus Zwischenspeicher #1-#3 einlesen.
:doLoadBuf1		lda	#<bufInfoText1		;Adresse Zwischenspeicher #1.
			ldx	#>bufInfoText1
			bne	doLoadBuf

:doLoadBuf2		lda	#<bufInfoText2		;Adresse Zwischenspeicher #2.
			ldx	#>bufInfoText2
			bne	doLoadBuf

:doLoadBuf3		lda	#<bufInfoText3		;Adresse Zwischenspeicher #3.
			ldx	#>bufInfoText3

:doLoadBuf		bit	curFileGEOS		;GEOS-Datei?
			bpl	:exit			; => Nein, Ende...

			sta	r0L			;Zeiger auf Zwischenspeicher
			stx	r0H			;sichern.

			LoadW	r1,curFHdrInfo +160
			jmp	CopyInfoText		;InfoText aus Zwischenspeicher.
::exit			rts

;*** InfoText von/nach InfoBlock kopieren.
:CopyInfoText		ldy	#$00
::1			lda	(r0L),y			;Zeichen einlesen.
			beq	:2			; => Ende...
			sta	(r1L),y
			iny
			cpy	#95
			bcc	:1

::2			lda	#$00			;Rest von InfoText löschen.
::3			sta	(r1L),y
			iny
			cpy	#96
			bcc	:3

			LoadW	r15,RTabMenu1_3b
			jmp	RegisterUpdate		;InfoText-Option aktualisieren.

;--- SYS_COPYFNAME:
;slctFiles		b $00
;filesBuf		s MAX_DIR_ENTRIES * 17

;*** Variablen.
:renameFile		b $00				;$FF = Datei umbenennen.
;regUpdate		b $00				;Register-Karte aktualisieren.
:reloadDir		b $00				;GeoDesk/Verzeichnis neu laden.

;*** Aktuelle Datei.
:curFile		b $00				;Nummer aktuelle Datei.
:curFileVec		w $0000				;Zeiger auf aktuelle Datei.

;*** Zwischenspeicher für RegisterMenü.
:FILE_VAR_START
:curDirEntry		s 32				;Verzeichnis-Eintrag.
:curFHdrInfo		s 256				;GEOS-InfoBlock.
:curFileGEOS		b $00				;$FF = GEOS-Datei.
:curFileName		s 17				;Dateiname.
:curFileType		s $04				;CBM-Dateityp.
:curGEOSType		s 24				;GEOS-Dateityp.
:curScrnMode		s 20				;GEOS-Bildschirm-Modus.
:curClass		s 13				;GEOS-Klasse.
:curClassVer		s 5				;GEOS-Klasse/Version.
:curAdrGEOSload		s $05				;GEOS-Ladeadresse.
:curAdrGEOSend		s $05				;GEOS-Endadresse.
:curAdrGEOSrun		s $05				;GEOS-Startadresse.
:FILE_VAR_END

;*** Zwischenspeicher für InfoText.
:bufInfoText1		s 96
:bufInfoText2		s 96
:bufInfoText3		s 96

;*** Text für Datei-Modus.
:textCBM		b "CBM",NULL
:textGEOS		b "GEOS",NULL
:textDIR		b "DIR",NULL

;*** Texte für GEOS-Dateistruktur.
:fileStructSEQ		b "SEQ",NULL
:fileStructVLIR		b "VLIR",NULL

;*** Fehler: Datei kann nicht umbenannt werden.
:Dlg_ErrRename		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2c
			w :3
			b DBTXTSTR   ,$30,$2c
			w curFileName
			b DBTXTSTR   ,$0c,$3c
			w :4
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Umbenennen der Datei fehlgeschlagen:",NULL
::3			b "Datei: ",BOLDON,NULL
::4			b PLAINTEXT
			b "Die Datei existiert bereits!",NULL

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Register-Tabelle.
:R1SizeY0		= $28
:R1SizeY1		= $af
:R1SizeX0		= $0028
:R1SizeX1		= $0117

:RegMenu1		b R1SizeY0			;Register-Größe.
			b R1SizeY1
			w R1SizeX0
			w R1SizeX1

			b 3				;Anzahl Einträge.

			w RTabName1_1			;Register: "CBM".
			w RTabMenu1_1

			w RTabName1_2			;Register: "GEOS".
			w RTabMenu1_2

			w RTabName1_3			;Register: "GEOS-INFO".
			w RTabMenu1_3

;*** Registerkarten-Icons.
:RTabName1_1		w RTabIcon1
			b RCardIconX_1,R1SizeY0 -$08
			b RTabIcon1_x,RTabIcon1_y

:RTabName1_2		w RTabIcon2
			b RCardIconX_2,R1SizeY0 -$08
			b RTabIcon2_x,RTabIcon2_y

:RTabName1_3		w RTabIcon3
			b RCardIconX_3,R1SizeY0 -$08
			b RTabIcon3_x,RTabIcon3_y

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Icons zum laden/speichern der Datei-Eigenschaften.
:RIcon_Save		w SaveInfo
			b $00,$00
			b SaveInfo_x,SaveInfo_y
			b $01

:RIcon_Load		w ResetInfo
			b $00,$00
			b ResetInfo_x,ResetInfo_y
			b $01

;*** Icon für Seitenwechsel.
:RIcon_Page		w SlctPage
			b $00,$00
			b SlctPage_x,SlctPage_y
			b $01

:PosSlctPage_x		= (R1SizeX1 +1) -$10
:PosSlctPage_y		= (R1SizeY1 +1) -$10

;*** Icons für Optionen.
:RIcon_Slct		w RIconDown
			b $00,$00
			b RIconDown_x,RIconDown_y
			b $01

:RIcon_Opt		w RIconOpt
			b $00,$00
			b RIconOpt_x,RIconOpt_y
			b $01

;*** Icons für InfoText-Zwischenspeicher.
:RIcon_SaveBuf1		w SaveBuf1
			b $00,$00
			b SaveBuf1_x,SaveBuf1_y
			b $01

:RIcon_LoadBuf1		w LoadBuf1
			b $00,$00
			b LoadBuf1_x,LoadBuf1_y
			b $01

:RIcon_SaveBuf2		w SaveBuf2
			b $00,$00
			b SaveBuf2_x,SaveBuf2_y
			b $01

:RIcon_LoadBuf2		w LoadBuf2
			b $00,$00
			b LoadBuf2_x,LoadBuf2_y
			b $01

:RIcon_SaveBuf3		w SaveBuf3
			b $00,$00
			b SaveBuf3_x,SaveBuf3_y
			b $01

:RIcon_LoadBuf3		w LoadBuf3
			b $00,$00
			b LoadBuf3_x,LoadBuf3_y
			b $01

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Daten für Register "CBM".
:DIGIT_2_BYTE		= $02 ! NUMERIC_RIGHT ! NUMERIC_SET0 ! NUMERIC_BYTE
:RPos1_x  = R1SizeX0 +$08
:RPos1_y  = R1SizeY0 +$20
:RWidth1  = $0050
:RLine1_1 = $00
:RLine1_2 = $10
:RLine1_3 = $20
:RLine1_4 = $30
:RLine1_5 = $40
:RLine1_6 = $50

:RTabMenu1_1		b 18

			b BOX_ICON			;----------------------------------------
				w $0000
				w SaveFileData
				b R1SizeY0 +$08
				w RPos1_x
				w RIcon_Save
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w ResetFileInfo
				b R1SizeY0 +$08
				w RPos1_x +$18
				w RIcon_Load
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w SwitchPage
				b PosSlctPage_y
				w PosSlctPage_x
				w RIcon_Page
				b $00

			b BOX_OPTION			;----------------------------------------
				w R1T00
				w $0000
				b R1SizeY0 +$10
				w RPos1_x +RWidth1 +$78
				w GD_INFO_SAVE
				b %11111111

			b BOX_USER_VIEW			;----------------------------------------
				w $0000
				w InitRegTab
				b R1SizeY0 +$08
				b R1SizeY0 +$08 +$0f
				w RPos1_x
				w RPos1_x +$0f

			b BOX_STRING			;----------------------------------------
				w R1T01
				w $0000
				b RPos1_y +RLine1_1
				w RPos1_x +RWidth1
				w curFileName
				b 16
			b BOX_NUMERIC			;----------------------------------------
				w R1T02
				w chkDateDay
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1
				w curDirEntry +27
				b DIGIT_2_BYTE
			b BOX_NUMERIC			;----------------------------------------
				w R1T02a
				w chkDateMonth
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1 +$18
				w curDirEntry +26
				b DIGIT_2_BYTE
			b BOX_NUMERIC			;----------------------------------------
				w R1T02b
				w chkDateYear
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1 +$30
				w curDirEntry +25
				b DIGIT_2_BYTE

			b BOX_NUMERIC			;----------------------------------------
				w $0000
				w chkDateHour
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1 +$48
				w curDirEntry +28
				b DIGIT_2_BYTE
			b BOX_NUMERIC			;----------------------------------------
				w R1T03
				w chkDateMinute
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1 +$60
				w curDirEntry +29
				b DIGIT_2_BYTE

			b BOX_ICON			;----------------------------------------
				w $0000
				w SetCurTime
				b RPos1_y +RLine1_2
				w RPos1_x +RWidth1 +$78
				w RIcon_Opt
				b $00

			b BOX_NUMERIC_VIEW		;----------------------------------------
				w R1T04
				w $0000
				b RPos1_y +RLine1_3
				w RPos1_x +RWidth1
				w curDirEntry +30
				b $05 ! NUMERIC_WORD ! NUMERIC_RIGHT
			b BOX_NUMERIC_VIEW		;----------------------------------------
				w R1T05
				w $0000
				b RPos1_y +RLine1_4
				w RPos1_x +RWidth1
				w curDirEntry +0
				b $05 ! NUMERIC_WORD ! NUMERIC_RIGHT
			b BOX_STRING_VIEW		;----------------------------------------
				w R1T06
				w $0000
				b RPos1_y +RLine1_5
				w RPos1_x +RWidth1
				w curFileType
				b $03
			b BOX_STRING_VIEW		;----------------------------------------
				w R1T06a
				w $0000
				b RPos1_y +RLine1_5
				w RPos1_x +RWidth1 +$20 +$38
:RTabMenu1_1a			w $ffff
				b $05
			b BOX_OPTION			;----------------------------------------
				w R1T07
				w $0000
				b RPos1_y +RLine1_6
				w RPos1_x +RWidth1 +$78
				w curDirEntry +2
				b %01000000
			b BOX_OPTION			;----------------------------------------
				w R1T08
				w $0000
				b RPos1_y +RLine1_6
				w RPos1_x +RWidth1
				w curDirEntry +2
				b %10000000

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
:R1T00			w RPos2_x +$18 +$18 +$10
			b R1SizeY0 +$0f
			b "Datei-Eigenschaften"
			b GOTOXY
			w RPos2_x +$18 +$18 +$10
			b R1SizeY0 +$17
			b "automatisch speichern:",NULL

:R1T01			w RPos1_x
			b RPos1_y +RLine1_1 +$06
			b "Dateiname:",NULL

:R1T02			w RPos1_x
			b RPos1_y +RLine1_2 +$06
			b "Datum/Zeit:",NULL
:R1T02a			w RPos1_x +RWidth1 +$12
			b RPos1_y +RLine1_2 +$06
			b ".",NULL
:R1T02b			w RPos1_x +RWidth1 +$18 +$12
			b RPos1_y +RLine1_2 +$06
			b ".",NULL

:R1T03			w RPos1_x +RWidth1 +$18 +$18 +$10 +$18 +$02
			b RPos1_y +RLine1_2 +$06
			b ":",NULL

:R1T04			w RPos1_x
			b RPos1_y +RLine1_3 +$06
			b "Größe:"
			b GOTOXY
			w RPos1_x +RWidth1 +$28 +$04
			b RPos1_y +RLine1_3 +$06
			b "Blocks",NULL

:R1T05			w RPos1_x +RWidth1 +$28 +$04
			b RPos1_y +RLine1_4 +$06
			b "KByte",NULL

:R1T06			w RPos1_x
			b RPos1_y +RLine1_5 +$06
			b "Dateityp:",NULL
:R1T06a			w RPos1_x +RWidth1 +$20
			b RPos1_y +RLine1_5 +$06
			b "Struktur:",NULL

:R1T07			w RPos1_x +RWidth1 +$20
			b RPos1_y +RLine1_6 +$06
			b "Schreibschutz:",NULL

:R1T08			w RPos1_x
			b RPos1_y +RLine1_6 +$06
			b "Geschlossen:",NULL

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Daten für Register "GEOS".
:RPos2_x  = R1SizeX0 +$08
:RPos2_y  = R1SizeY0 +$28
:RWidth2  = $0040
:RLine2_1 = $00
:RLine2_2 = $10
:RLine2_3 = $20
:RLine2_4 = $30
:RLine2_5 = $40

:RTabMenu1_2		b 15

			b BOX_ICON			;----------------------------------------
				w $0000
				w SaveFileData
				b R1SizeY0 +$08
				w RPos2_x
				w RIcon_Save
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w ResetFileInfo
				b R1SizeY0 +$08
				w RPos2_x +$18
				w RIcon_Load
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w SwitchPage
				b PosSlctPage_y
				w PosSlctPage_x
				w RIcon_Page
				b $00
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T01
				w $0000
				b R1SizeY0 +$10
				w RPos2_x +$18 +$18 +$10
				w curFileName
				b 16

			b BOX_USER_VIEW			;----------------------------------------
				w $0000
				w InitRegTab
				b R1SizeY0 +$08
				b R1SizeY0 +$08 +$0f
				w RPos2_x
				w RPos2_x +$0f

:RTabMenu1_2b		b BOX_STRING_VIEW		;----------------------------------------
				w R2T02
				w $0000
				b RPos2_y +RLine2_1
				w RPos2_x +RWidth2
				w curGEOSType
				b 16
:RTabMenu1_2c		b BOX_ICON			;----------------------------------------
				w $0000
				w SetGEOSType
				b RPos2_y +RLine2_1
				w RPos2_x +RWidth2 +$80
				w RIcon_Slct
				b $00
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T03
				w $0000
				b RPos2_y +RLine2_2
				w RPos2_x +RWidth2
				w curClass
				b 12
			b BOX_STRING_VIEW		;----------------------------------------
				w $0000
				w $0000
				b RPos2_y +RLine2_2
				w RPos2_x +RWidth2 +$68
				w curClassVer
				b 4
:RTabMenu1_2e		b BOX_STRING			;----------------------------------------
				w R2T04
				w $0000
				b RPos2_y +RLine2_3
				w RPos2_x +RWidth2
				w curFHdrInfo +$61
				b 19
:RTabMenu1_2d		b BOX_STRING_VIEW		;----------------------------------------
				w R2T05
				w $0000
				b RPos2_y +RLine2_4
				w RPos2_x +RWidth2
				w curScrnMode
				b 16
:RTabMenu1_2f		b BOX_ICON			;----------------------------------------
				w $0000
				w SetScrnMode
				b RPos2_y +RLine2_4
				w RPos2_x +RWidth2 +$80
				w RIcon_Slct
				b $00
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T06
				w $0000
				b RPos2_y +RLine2_5
				w RPos2_x +RWidth2 +$08
				w curAdrGEOSload
				b 4
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T06a
				w $0000
				b RPos2_y +RLine2_5
				w RPos2_x +RWidth2 +$08 +$30
				w curAdrGEOSend
				b 4
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T06b
				w $0000
				b RPos2_y +RLine2_5
				w RPos2_x +RWidth2 +$08 +$30 +$30
				w curAdrGEOSrun
				b 4

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
:R2T01			w RPos2_x +$18 +$18 +$10
			b R1SizeY0 +$0d
			b "Aktuelle Datei:",NULL

:R2T02			w RPos2_x
			b RPos2_y +RLine2_1 +$06
			b "Dateityp:",NULL

:R2T03			w RPos2_x
			b RPos2_y +RLine2_2 +$06
			b "Klasse:",NULL

:R2T04			w RPos2_x
			b RPos2_y +RLine2_3 +$06
			b "Autor:",NULL

:R2T05			w RPos2_x
			b RPos2_y +RLine2_4 +$06
			b "Modus:",NULL

:R2T06			w RPos2_x
			b RPos2_y +RLine2_5 +$06
			b "Adressen:"
			b GOTOXY
			w RPos2_x +RWidth2
			b RPos2_y +RLine2_5 +$06
			b "L",NULL
:R2T06a			w RPos2_x +RWidth2 +$08 +$27
			b RPos2_y +RLine2_5 +$06
			b "E",NULL
:R2T06b			w RPos2_x +RWidth2 +$08 +$30 +$27
			b RPos2_y +RLine2_5 +$06
			b "S",NULL

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Daten für Register "GEOS-INFO".
:RPos3_x   = R1SizeX0 +$08
:RPos3_y   = R1SizeY0 +$28
:RWidth3  = $00b8
:RLine3_1 = $00
:RLine3_2 = $48

:RTabMenu1_3		b 14

			b BOX_ICON			;----------------------------------------
				w $0000
				w SaveFileData
				b R1SizeY0 +$08
				w RPos3_x
				w RIcon_Save
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w ResetFileInfo
				b R1SizeY0 +$08
				w RPos3_x +$18
				w RIcon_Load
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w SwitchPage
				b PosSlctPage_y
				w PosSlctPage_x
				w RIcon_Page
				b $00
			b BOX_STRING_VIEW		;----------------------------------------
				w R2T01
				w $0000
				b R1SizeY0 +$10
				w RPos3_x +$18 +$18 +$10
				w curFileName
				b 16

			b BOX_USER_VIEW			;----------------------------------------
				w $0000
				w InitRegTab
				b R1SizeY0 +$08
				b R1SizeY0 +$08 +$0f
				w RPos3_x
				w RPos3_x +$0f

			b BOX_USEROPT			;----------------------------------------
				w R3T02
				w DefInfoIcon
				b RPos3_y +RLine3_1 +$08
				b RPos3_y +RLine3_1 +$08 +$30 -1
				w RPos3_x +RWidth3
				w RPos3_x +RWidth3 +$28 -1
:RTabMenu1_3b		b BOX_USEROPT			;----------------------------------------
				w R3T01
				w DefInfoText
				b RPos3_y +RLine3_1 +$08
				b RPos3_y +RLine3_1 +$08 +$30 -1
				w RPos3_x
				w RPos3_x +$a8 -1
			b BOX_ICON			;----------------------------------------
				w $0000
				w ClrInfoText
				b RPos3_y +RLine3_1
				w RPos3_x +$a0
				w RIcon_Opt
				b $00

			b BOX_ICON			;----------------------------------------
				w R3T03
				w doSaveBuf1
				b RPos3_y +RLine3_2
				w RPos3_x +$00
				w RIcon_SaveBuf1
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w doLoadBuf1
				b RPos3_y +RLine3_2
				w RPos3_x +$18
				w RIcon_LoadBuf1
				b $00

			b BOX_ICON			;----------------------------------------
				w $0000
				w doSaveBuf2
				b RPos3_y +RLine3_2
				w RPos3_x +$40
				w RIcon_SaveBuf2
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w doLoadBuf2
				b RPos3_y +RLine3_2
				w RPos3_x +$58
				w RIcon_LoadBuf2
				b $00

			b BOX_ICON			;----------------------------------------
				w $0000
				w doSaveBuf3
				b RPos3_y +RLine3_2
				w RPos3_x +$80
				w RIcon_SaveBuf3
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w doLoadBuf3
				b RPos3_y +RLine3_2
				w RPos3_x +$98
				w RIcon_LoadBuf3
				b $00

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
:R3T01			w RPos3_x
			b RPos3_y +RLine3_1 +$04
			b "Infotext:",NULL

:R3T02			w RPos3_x +RWidth3
			b RPos3_y +RLine3_1 +$04
			b "Icon:",NULL

:R3T03			w RPos3_x
			b RPos3_y +RLine3_2 -$04
			b "Infotext-Zwischenspeicher:",NULL

;*** Icons für Registerkarten.
:RTabIcon1
<MISSING_IMAGE_DATA>

:RTabIcon1_x		= .x
:RTabIcon1_y		= .y

:RTabIcon2
<MISSING_IMAGE_DATA>

:RTabIcon2_x		= .x
:RTabIcon2_y		= .y

:RTabIcon3
<MISSING_IMAGE_DATA>

:RTabIcon3_x		= .x
:RTabIcon3_y		= .y

;*** X-Koordinate der Register-Icons.
:RCardIconX_1		= (R1SizeX0/8) +3
:RCardIconX_2		= RCardIconX_1 + RTabIcon1_x
:RCardIconX_3		= RCardIconX_2 + RTabIcon2_x

;*** Register-System-Icons.
:RIconDown
<MISSING_IMAGE_DATA>

:RIconDown_x		= .x
:RIconDown_y		= .y

:RIconSlct
<MISSING_IMAGE_DATA>

:RIconSlct_x		= .x
:RIconSlct_y		= .y

:RIconOpt
<MISSING_IMAGE_DATA>

:RIconOpt_x		= .x
:RIconOpt_y		= .y

;*** Register-Funktions-Icons.
:ResetInfo
<MISSING_IMAGE_DATA>
:ResetInfo_x		= .x
:ResetInfo_y		= .y

:SaveInfo
<MISSING_IMAGE_DATA>
:SaveInfo_x		= .x
:SaveInfo_y		= .y

:SlctPage
<MISSING_IMAGE_DATA>
:SlctPage_x		= .x
:SlctPage_y		= .y

:SaveBuf1
<MISSING_IMAGE_DATA>
:SaveBuf1_x		= .x
:SaveBuf1_y		= .y

:LoadBuf1
<MISSING_IMAGE_DATA>
:LoadBuf1_x		= .x
:LoadBuf1_y		= .y

:SaveBuf2
<MISSING_IMAGE_DATA>
:SaveBuf2_x		= .x
:SaveBuf2_y		= .y

:LoadBuf2
<MISSING_IMAGE_DATA>
:LoadBuf2_x		= .x
:LoadBuf2_y		= .y

:SaveBuf3
<MISSING_IMAGE_DATA>
:SaveBuf3_x		= .x
:SaveBuf3_y		= .y

:LoadBuf3
<MISSING_IMAGE_DATA>
:LoadBuf3_x		= .x
:LoadBuf3_y		= .y

;******************************************************************************
;*** Icon-Menü.
;******************************************************************************
;*** Icon-Menü "Beenden".
:IconMenu		b $01
			w $0000
			b $00

			w IconExit
			b (R1SizeX0/8) +1,R1SizeY0 -$08
			b IconExit_x,IconExit_y
			w Exit

;*** Icon zum schließen des Menüs.
:IconExit
<MISSING_IMAGE_DATA>

:IconExit_x		= .x
:IconExit_y		= .y

