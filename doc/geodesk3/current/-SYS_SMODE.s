﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Zeiger auf GEOS-Bildschirm-Modus.
;Übergabe: AKKU = Bildschirm-Modus.
;Rückgabe: XREG/YYREG = Zeiger auf Text für Bildschirm-Modus.
:GetScreenMode		lsr
			lsr
			lsr
			lsr
			lsr
			tax
			lda	:tab +0,x
			ldy	:tab +1,x
			rts

;*** Text für Bildschirm-Modus.
::tab			w :40
			w :40_80
			w :64
			w :80

::40			b "40 Zeichen",NULL
::40_80			b "40 & 80 Zeichen",NULL
::64			b "GEOS 64",NULL
::80			b "80 Zeichen",NULL

