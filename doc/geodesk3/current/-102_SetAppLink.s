﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** AppLink für MyComputer/Laufwerk.
:AL_DnD_Drive		lda	#AL_ID_DRIVE

;*** AppLink für Laufwerksfenster als Laufwerk/Verzeichnis.
:AL_DnD_DrvSubD		pha

			lda	curDirHead +32		;Track/Sektor für Verzeichnis-
			sta	r10L			;Header.
			lda	curDirHead +33
			sta	r10H

			lda	#<curDirHead		;Zeiger auf BAM im Speicher.
			sta	r11L
			lda	#>curDirHead
			sta	r11H

			ldx	#r12L			;Zeiger auf Disknamen setzen.
			jsr	GetPtrCurDkNm

			pla				;AppLink-ID für AppLink-Tabelle.
			ldx	r12L			;Zeiger auf Diskname.
			ldy	r12H
			jmp	AL_SET_LNKDATA_N	;AppLink für Verzeichnis erstellen.

;*** AppLink für MyComputer/Drucker.
:AL_DnD_Printer		lda	#AL_ID_PRNT		;AppLink-ID für AppLink-Tabelle.
			ldx	#<PrntFileName		;Zeiger auf Druckername.
			ldy	#>PrntFileName
			jmp	AL_SET_LNKDATA_S	;AppLink für Drucker erstellen.

;*** AppLink für Laufwerksfenster/Verzeichnis.
:AL_DnD_SubDir		lda	r1L			;Track/Sektor für Verzeichnis-
			sta	r10L			;Header.
			lda	r1H
			sta	r10H

			lda	#<diskBlkBuf		;Zeiger auf BAM im Speicher.
			sta	r11L
			lda	#>diskBlkBuf
			sta	r11H

			lda	r0L
			clc
			adc	#$05
			tax
			lda	r0H
			adc	#$00
			tay
			lda	#AL_ID_SUBDIR		;AppLink-ID für AppLink-Tabelle.
			jmp	AL_SET_LNKDATA_N	;AppLink für Verzeichnis erstellen.

;*** AppLink für Laufwerksfenster/Dateien.
:AL_DnD_Files		lda	r0L
			clc
			adc	#$05
			tax
			lda	r0H
			adc	#$00
			tay
			lda	#AL_ID_FILE		;AppLink-ID für AppLink-Tabelle.
			jmp	AL_SET_LNKDATA_S	;AppLink für Datei erstellen.

;*** AppLink für Laufwerksfenster/Drucker.
:AL_DnD_FilePrnt	LoadW	r12,FNameBuf		;Zeiger auf Zwischenspeicher für
			ldx	#r0L			;Druckername.
			ldy	#r12L
			jsr	SysCopyFName

			lda	#AL_ID_PRNT		;AppLink-ID für AppLink-Tabelle.
			ldx	r12L			;Zeiger auf Druckername.
			ldy	r12H
			jmp	AL_SET_LNKDATA_S	;AppLink für Drucker erstellen.

;*** AppLink-Daten schreiben:
;    Drucker oder Dateien.
;    Übergabe: AKKU = AppLink-ID ":AL_ID_..."
;              X/Y  = Low/High-Zeiger auf Name.
:AL_SET_LNKDATA_S	pha
			lda	#$00
			sta	r10L			;Kein Track/Sektor für
			sta	r10H			;Verzeichnis-Header.
			sta	r11L			;Kein Zeiger auf BAM erforderlich.
			sta	r11H
			pla

;*** AppLink-Daten schreiben:
;    Laufwerk oder Verzeichnis.
;    Übergabe: AKKU = AKKU = AppLink-ID ":AL_ID_..."
;              X/Y  = Low/High-Zeiger auf Name.
;              r10 = Track/Sektor Verzeichnis-Header.
;              r11 = Zeiger auf Puffer mit dirHead/BAM.
:AL_SET_LNKDATA_N	sta	:applink_id		;AppLink-ID speichern.
			stx	:fname_vec +0		;Zeiger auf Dateiname speichern.
			sty	:fname_vec +1

			jsr	AL_SET_INIT		;Zeiger auf Speicher für Link/Icon.

			MoveW	:fname_vec,r3		;Zeiger auf Name für AppLink.
			jsr	AL_SET_FNAME		;Dateiname kopieren.
			jsr	AL_SET_TITLE		;Dateiname als Titel setzen.

			jsr	AL_SET_ICON		;Icon für AppLink speichern.

			ldy	:applink_id
			lda	:al_set_color_l,y
			ldx	:al_set_color_h,y
			jsr	AL_SET_COLOR		;Farbe für AppLink-Icon.

			jsr	AL_SET_XYPOS		;Position für AppLink auf DeskTop.

			ldy	:applink_id
			lda	:al_set_type,y
			jsr	AL_SET_TYPE		;AppLink-Typ speichern.

			jsr	AL_SET_DRIVE		;Laufwerks-Adresse speichern.
			jsr	AL_SET_RDTYP		;RealDrvType speichern.

			ldx	curDrive		;CMD-Partition und NativeMode.
			lda	RealDrvMode -8,x
			pha
			and	#SET_MODE_PARTITION
			beq	:no_part		; => Kein CMD-Laufwerk.
			jsr	AL_SET_PART
::no_part		pla
			and	#SET_MODE_SUBDIR
			beq	:no_subdir		; => Kein NativeMode-Laufwerk.

			lda	r10L			;Track/Sektor für
			ldx	r10H			;Verzeichnis-Header speichern.
			jsr	AL_SET_SDIR

			ldy	#36			;Zeiger auf Verzeichnis-Eintrag
			lda	(r11L),y		;für aktuelles Verzeichnis.
			pha				;Die Angaben werden später dazu
			iny				;Verwendet um zu prüfen ob das
			lda	(r11L),y		;Verzeichnis noch existiert.
			tax
			iny
			lda	(r11L),y
			tay
			pla
			jsr	AL_SET_SDIRE		;AppLink-Verzeichnis-Eintrag.

::no_subdir		ldy	:applink_id		;AppLink-ID einlesen.
			lda	:al_set_wmode,y		;Fensteroptionen speichern?
			beq	:end			; => Nein, weiter...
			jsr	AL_SET_WMODE		;AppLink/Fensteroptionen speichern.

::end			inc	LinkData		;Anzahl AppLinks +1.

			jsr	MainDrawDesktop		;DeskTop+AppLinks neu zeichen.
			jmp	WM_DRAW_ALL_WIN		;Fenster neu zeichnen.

;*** Variablen.
::applink_id		b $00
::fname_vec		w $0000

;*** AppLink-Typen.
::al_set_type		b AL_TYPE_FILE
			b AL_TYPE_DRIVE
			b AL_TYPE_PRNT
			b AL_TYPE_SUBDIR

;*** Fenster-Optionen speichern.
::al_set_wmode		b AL_WMODE_FILE
			b AL_WMODE_DRIVE
			b AL_WMODE_PRNT
			b AL_WMODE_SUBDIR

;*** Farben für AppLink-Icons.
::al_set_color_l	b <Color_Std
			b <Color_Drive
			b <Color_Prnt
			b <Color_SDir
::al_set_color_h	b >Color_Std
			b >Color_Drive
			b >Color_Prnt
			b >Color_SDir
