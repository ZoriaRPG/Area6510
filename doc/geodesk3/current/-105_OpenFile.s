﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Anwendung starten auf die ":a0" zeigt.
:StartFile_a0		LoadW	r1,AppFName
			ldx	#a0L 			;Dateiname aus Verzeichnis-Eintrag
			ldy	#r1L			;in Puffer kopieren.
			jsr	SysCopyFName

			ldy	#$18			;Dateityp auswerten.
			lda	(a0L),y
			beq	:start_basic		; => BASIC-Programm.
			cmp	#APPLICATION		;Anwendung?
			beq	:start_appl		; => Ja, weiter...
			cmp	#AUTO_EXEC		;AutoExec?
			beq	:start_appl		; => Ja, weiter...
			cmp	#APPL_DATA		;Dokument?
			beq	:start_doc		; => Ja, weiter...
			cmp	#DESK_ACC		;Hilfsmittel?
			beq	:start_da		; => Ja, weiter...
			jmp	OpenFileError		; => Keine gültige Datei.

::start_appl		jmp	OpenAppl
::start_doc		jmp	OpenDoc
::start_auto		jmp	OpenAppl
::start_da		jmp	OpenDA
::start_basic		jmp	OpenBASIC

;*** Allgemeiner Dateifehler.
:OpenFileError		lda	#<Dlg_ErrOpen1
			ldx	#>Dlg_ErrOpen1
			bne	PrntErrorExit

;*** Nicht mit GEOS64 kompatibel.
:OpenG64Error		lda	#<Dlg_ErrOpen2
			ldx	#>Dlg_ErrOpen2

;*** Fehlermeldung ausgebenund zurück zum DeskTop.
:PrntErrorExit		sta	r0L
			stx	r0H
			jsr	DoDlgBox		;Fehler ausgeben.
			jmp	MNU_RESTART		;Menü/FensterManager neu starten.

;*** Datei laden vorbereiten.
:PrepGetFile		jsr	ResetScreen		;Bildschirm löschen.
			;jsr	WM_RESET_SCREEN

:PrepGetFileDA		jsr	UseSystemFont		;Standardzeichensatz.

			jsr	i_FillRam		;dlgBoxRamBuf löschen.
			w	417
			w	dlgBoxRamBuf
			b	$00

			ldx	#r15H			;ZeroPage löschen.
			lda	#$00
::loop			sta	$0000,x
			dex
			cpx	#r0L
			bcs	:loop
			rts

;*** Name der Anwendung die gestartet werden soll.
:AppFName		s 17
:DocFName		s 17
:AppClass		s 17
:DocDName		s 17
:ApplDrives		s $04

;*** Dialogboxen.
:Dlg_ErrOpen1		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk konnte die Datei"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "nicht öffnen!",NULL

;*** Dialogboxen.
:Dlg_ErrOpen2		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Kann die Datei nicht öffnen!"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "Nicht mit GEOS64 kompatibel.",NULL

;*** Anwendung oder AutoExec starten.
:OpenAppl		jsr	ChkFlag_40_80		;40/80Z-Flag testen.
			txa				;Fehler?
			bne	:error_40_80		; => Ja, Abbruch...

			sei				;GEOS-Reset #0.
			cld
			ldx	#$ff
			txa

			jsr	GEOS_InitSystem		;GEOS-Reset #1.
			jsr	PrepGetFile

			lda	#>EnterDeskTop -1	;Bei Fehler zurück zum
			pha				;DeskTop.
			lda	#<EnterDeskTop -1
			pha

;			LoadB	r0L,$00			;Wird durch ":PrepGetFile" gelöscht.
			LoadW	r6,AppFName
			jmp	GetFile			;Datei laden/starten.
::error_40_80		jmp	OpenG64Error

;*** Dokument starten.
:OpenDoc		ldx	#r14L			;Zeiger auf Aktuelle Diskette.
			jsr	GetPtrCurDkNm

			ldy	#15
::0			lda	(r14L),y		;Diskname kopieren.
			sta	DocDName,y
			lda	AppFName,y		;Dateiname ist ein Dokument.
			sta	DocFName,y		;Name kopieren.
			dey
			bpl	:0

			jsr	LoadDokInfos		;Dokument-Daten einlesen.
			txa				;Fehler?
			beq	:1			; => Nein, weiter...
::error			jmp	OpenFileError		; => Abbruch.
::error_40_80		jmp	OpenG64Error

::1			jsr	IsApplOnDsk		;Anwendung suchen.
			cpx	#INCOMPATIBLE
			beq	:error_40_80
			txa				;Gefunden?
			bne	:error			; => Nein, Abbruch...

			ldy	#$0f			;Dokument- und Diskname
::2			lda	DocDName,y		;in Systemspeicher kopieren.
			sta	dataDiskName,y
			lda	DocFName,y
			sta	dataFileName,y
			dey
			bpl	:2

			sei				;GEOS-Reset #0.
			cld
			ldx	#$ff
			txa

			jsr	GEOS_InitSystem		;GEOS-Reset #1.
			jsr	PrepGetFile

			lda	#>EnterDeskTop -1	;Bei Fehler zurück zum
			pha				;DeskTop.
			lda	#<EnterDeskTop -1
			pha

			LoadW	r2,dataDiskName
			LoadW	r3,dataFileName
			LoadW	r6,AppFName
			LoadB	r0L,%10000000
			jmp	GetFile			;Anwendung+Dokument starten.

;*** Hilfsmittel laden.
:OpenDA			jsr	ChkFlag_40_80		;40/80Z-Flag testen.
			txa				;Fehler?
			bne	:error_40_80		; => Ja, Abbruch...

			jsr	PrepGetFileDA		;":dlgBoxRamBuf" löschen.

			LoadW	r6,AppFName
			LoadB	r0L,%00000000
;			LoadB	r10L,$00		;Wird durch ":PrepGetFile" gelöscht.
			jsr	GetFile			;DA laden.
			txa
			bne	:error
			jmp	MNU_INITWM		;Menü/FensterManager neu starten.
::error			jmp	OpenFileError
::error_40_80		jmp	OpenG64Error

;*** BASIC-Anwendung starten.
:OpenBASIC		LoadW	r0,Dlg_ExitBASIC
			jsr	DoDlgBox

			lda	sysDBData		;Abbruch ?
			cmp	#CANCEL
			beq	:exit			; => Nein, weiter...

			LoadW	r6,AppFName		;Zeiger auf Dateiname.
			jmp	ExitBApplRUN		;BASIC-Datei laden/starten.
::exit			jmp	MNU_RESTART		;Menü/FensterManager neu starten.

;*** Dialogboxen.
:Dlg_ExitBASIC		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b YES        ,$01,$50
			b CANCEL     ,$11,$50
			b NULL

::2			b PLAINTEXT
			b "GEOS beenden und die BASIC-Datei"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "laden und starten?",NULL

;*** Dokument-Infos einlesen.
:LoadDokInfos		LoadW	r6,DocFName		;Dateieintrag suchen.
			jsr	FindFile
			txa				;Diskettenfehler ?
			bne	:2			;Nein, weiter...

			LoadW	r9,dirEntryBuf		;Infoblock einlesen.
			jsr	GetFHdrInfo
			txa
			bne	:2

			ldy	#11			;"Class" der Application einlesen.
::1			lda	fileHeader+$75,y
			sta	AppClass,y
			dey
			bpl	:1

			ldx	#NO_ERROR		;Kein Fehler...
::2			rts				;Ende.

;*** Applikation suchen.
:IsApplOnDsk		lda	#%10000000		;Anwendung auf
			jsr	FindApplFile		;RAM-Laufwerken suchen.
			txa
			beq	:1
			lda	#%00000000		;Anwendung auf
			jsr	FindApplFile		;Disk-Laufwerken suchen.
::1			rts

;*** Anwendung auf den Laufwerken A: bis D: suchen.
:FindApplFile		sta	:2 +1			;Laufwerkstyp speichern.

			ldx	curDrive
			stx	r15L
::1			stx	r15H
			lda	driveType -8,x		;Laufwerk verfügbar?
			beq	:3			; => Nein, weiter...
			and	#%10000000
::2			cmp	#$ff			;Gesuchter Laufwerkstyp?
			bne	:3			; => Nein, weiter...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.

			jsr	OpenDisk		;Diskette öffnen.
			txa				;Disk-Fehler?
			bne	:3			; => Ja, weiter...

			LoadW	r6,AppFName		;Anwendung  suchen.
			LoadB	r7L,APPLICATION
			LoadB	r7H,1
			LoadW	r10,AppClass
			jsr	FindFTypes
			txa				;Fehler?
			bne	:3			; => Ja, Abbruch...
			lda	r7H			;Datei gefunden?
			bne	:3			; => Nein, weiter suchen...

			jsr	ChkFlag_40_80		;Gefundene Anwendung für GEOS64?
			txa
			beq	:5			; => Ja, Ende...

::3			ldx	r15H			;Zeiger auf nächstes Laufwerk.
			inx
			cpx	r15L			;Alle Laufwerke durchsucht?
			beq	:4			; => Ja, Ende...
			cpx	#12
			bcc	:1
			ldx	#$08
			bne	:1			;Auf nächsten Laufwerk weitersuchen.

::4			ldx	#$ff			;Nicht gefunden.

;--- Falls Anwendung nicht gefunden, Laufwerk zurücksetzen.
::5			txa				;Fehler?
			beq	:7			; => Nein, weiter...
			pha
			lda	r15L 			;Vorheriges Laufwerk wieder
			jsr	SetDevice		;aktivieren.
			pla
			tax
::7			rts

;*** C128/40/80Z-Modus testen.
:ChkFlag_40_80		LoadW	r6,AppFName		;Datei suchen.
			jsr	FindFile
			txa				;Fehler?
			bne	:4			; => Ja, Abbruch.

			LoadW	r9,dirEntryBuf		;Infoblock einlesen.
			jsr	GetFHdrInfo
			txa				;Info-Block gefunden ?
			bne	:4			; => Nein, BASIC-File, Abbruch...

			lda	fileHeader+$60		;40/80Z-Flag einlesen.
			ldx	c128Flag		;C64/C128?
			bne	:1			; => C128, Weiter...
;--- Ergänzung: 15.03.19/M.Kanet
;Unter GEOS gibt es kein Flag für "Nur GEOS128". Eine Anwendung die für den
;40+80Z-Modus entwickelt wurde kann auch für GEOS64 existieren. Es kann aber
;auch eine reine GEOS128-Anwendung sein.
;Unter GEOS64 werden daher GEOS64, 40ZOnly und 40/80Z akzeptiert.
			cmp	#$c0			;Nur 80Z?
if TRUE
;--- Keine Unterstützung für GEOS128.
;Daher nur auf "Nur GEOS128" testen.
			bne	:2			; => Nein, weiter...
::1			ldx	#INCOMPATIBLE		; => Nur GEOS128, Abbruch.
			b $2c
::2			ldx	#NO_ERROR		; => Anwendung OK.
::4			rts
endif

;--- Keine Unterstützung für GEOS128.
;Code aus GeoDOS64 vorerst deaktiviert.
if FALSE
			beq	:2			; => GEOS128-App auf GEOS64,Abbruch.
			bne	:3			;Evtl. GEOS64 App... weiter...

;--- Ergänzung: 15.03.19/M.Kanet
;Unter GEOS128 werden 40ZOnly, 80ZOnly und 40/80Z akzeptiert.
::1			cmp	#%00000000		;40/80Z-Flag einlesen.
			beq	:40
			cmp	#%01000000		;40/80Z ?
			beq	:80			;Ja -> 80Z-Modus setzen.
			cmp	#%10000000		;40/80Z ?
			beq	:2			;Ja -> 80Z-Modus setzen.
			cmp	#%11000000		;Nur 80Z ?
			beq	:80			;Ja -> 80Z-Modus setzen.
::2			ldx	#INCOMPATIBLE		; -> Nur GEOS64, Abbruch.
			bne	:4
::80			lda	#%10000000		;80Z-Modus setzen.
			b $2c
::40			lda	#%00000000		;40Z-Modus setzen.
			sta	appScrnMode
::3			ldx	#NO_ERROR
::4			rts

:appScrnMode		b $00
endif

;*** Anwendung wählen.
:SelectAppl		lda	#APPLICATION		;GEOS-Anwednungen.
			b $2c
:SelectAuto		lda	#AUTO_EXEC		;GEOS-Autostart.
			sta	r7L

			lda	#$00			;GEOS-Klasse löschen.
			sta	r10L
			sta	r10H

;*** Anwendung/Dokument wählen.
:SelectAnyFile		jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	:openfile
::exit			jmp	MNU_RESTART		;Menü/FensterManager neu starten.

::openfile		LoadW	r6,dataFileName
			jsr	FindFile		;Datei suchen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			LoadW	a0,dirEntryBuf -2	;Datei öffnen.
			jmp	StartFile_a0

;*** Dokument wählen.
:SelectDocument		ldx	#0			;Alle Dokumente.
			b $2c
:SelectDocWrite		ldx	#2			;GeoWrite-Dokumente.
			b $2c
:SelectDocPaint		ldx	#4			;GeoPaint-Dokumente.
			lda	ApplClass +0,x		;GEOS-Klasse fürr Dokumente setzen.
			sta	r10L
			lda	ApplClass +1,x
			sta	r10H
			LoadB	r7L,APPL_DATA		;GEOS-Dokumente.
			jmp	SelectAnyFile		;Datei auswählen.

;*** Liste der Anwendungsklassen.
:ApplClass		w $0000
			w AppClassWrite
			w AppClassPaint
:AppClassWrite		b "Write Image ",NULL
:AppClassPaint		b "Paint Image ",NULL
