﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateien löschen.
:xFILE_DELETE		jsr	WM_GET_MSE_STATE	;Tastenstatus abfragen.
			and	#%0010 0000		;C= Taste gedrückt?
			bne	:1			; => Nein, weiter...

			lda	#$00			;C= Taste gedrückt...
			sta	GD_DEL_MENU		;AutoDelete-Flag löschen.

::1			jsr	COPY_FILE_NAMES		;Dateinamen in Speicher kopieren.

			LoadB	curFile,$00		;Zeiger auf erste Datei.
			LoadW	curFileVec,filesBuf

			ldx	slctFiles		;Dateien ausgewählt?
			beq	ExitDelFiles		; => Nein, Ende...
			dex				;Mehr als 1 Datei?
			beq	:do_single		; => Ja, weiter...

;--- Mehrere Dateien löschen.
::do_multiple		jsr	i_MoveData		;Vorgabe: "Mehrere Dateien löschen".
			w	multipleFiles
			w	curFileName
			w	16

			lda	#$ff
			bne	:2

;--- Einzelne Datei löschen.
::do_single		jsr	i_MoveData		;Vorgabe: "Einzelne Datei löschen".
			w	filesBuf
			w	curFileName
			w	16

			lda	#$00

;--- Register-Menü initialisieren.
::2			sta	curFile			;Einzelne Datei/Mehrere Dateien.

			bit	GD_DEL_MENU		;Ohne Nachfragen löschen?
			bpl	:3			; => Nein, weiter...

			jsr	doDeleteJob		;Dateien löschen.
			jmp	ExitNoRegMenu		;Zurück zum DeskTop.

;--- Register-Menü anzeigen.
::3			ClrB	reloadDir		;Flag löschen "Verzeichnis laden".

			jsr	SetADDR_Register	;RegisterMenü-Routine einlesen.
			jsr	FetchRAM

			jsr	RegisterSetFont		;Register-Font aktivieren.

			LoadW	r0,RegMenu1
			jsr	DoRegister		;Register-Menü starten.

;--- Icon-Menü "Beenden" initialisieren.
			lda	C_WinIcon		;Farbe für "X"-Icon setzen.
			jsr	i_UserColor
			b	(R1SizeX0/8) +1
			b	(R1SizeY0/8) -1
			b	IconExit_x
			b	IconExit_y/8

			LoadW	r0,IconMenu
			jmp	DoIcons			;Icon-Menü aktivieren.

;*** Zurück zum DeskTop.
:ExitDelFiles		jsr	ExitRegisterMenu	;Register-Menü beenden.

;--- kein Register-Menü aktiv.
:ExitNoRegMenu		bit	reloadDir		;Verzeichnis neu laden?
			bpl	:1			; => Nein, weiter...

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen, da
							;evtl. Dateien gelöscht.

::1			jmp	MNU_UPDATE		;Zurück zum Hauptmenü.

;*** Icon "Löschen" gewählt.
:doDelete		jsr	doDeleteJob		;Dateien löschen.

;*** "Abbruch", zurück zum DeskTop.
;Das Register-Menü kann nicht durch ein
;Icon/Option direkt beendet werden, da
;hier das Registermenü noch aktiv ist.
;
;Das Menü muss in diesem Fall über die
;MainLoop beendet werden, analog zur
;Verwendung eines DoIcon-Menüs.
;
;Dazu appMain auf die eigentliche EXIT-
;Routine setzen und zum RegisterMenü
;zurückkehren.
;
:cancelDelete		LoadW	appMain,ExitDelFiles
			rts

;*** Dateien/Verzeichnisse löschen.
:doDeleteJob		jsr	WM_LOAD_BACKSCR		;Bildschirm zurücksetzen.

			jsr	prntInfoArea		;Status-Anzeige vorbereiten.

			LoadB	reloadDir,$ff		;GeoDesk: Verzeichnis neu laden.
			ClrB	delDirFiles		;Abfrage: Verzeichnisse löschen?

			LoadW	r15,filesBuf		;Zeiger auf Anfang Dateiliste.

			MoveB	slctFiles,r14H		;Dateizähler löschen.

;--- Schleife: Einträge löschen.
::loop			ClrB	dirNotEmpty		;Flag: Verzeichnis leer.

			MoveW	r15,r6			;Zeiger auf Dateiname.
			jsr	FindFile		;Aktuelle Datei suchen.
			txa				;Datei Gefunden?
			bne	:error			; => Nein, Abbruch...

			MoveW	r15,r0			;Zeiger auf Dateiname.

			ldx	WM_WCODE
			ldy	WIN_DRIVE,x		;Aktuelles Laufwerk einlesen.
			lda	RealDrvMode -8,y	;Laufwerksmodus einlesen.
			and	#SET_MODE_SUBDIR	;Native-Mode Laufwerk?
			beq	:is_file		; => Nein, weiter...

			lda	dirEntryBuf
			and	#%0000 0111		;Dateityp isolieren.
			cmp	#$06			;Verzeichnis?
			beq	:is_dir			; => Ja, weiter...

;--- Datei löschen.
::is_file		jsr	doDeleteFile		;Datei löschen.
			jmp	:test_error

;--- Verzeichnis löschen.
::is_dir		lda	dirEntryBuf +1		;Tr/Se für Verzeichnis-Header
			sta	curDirHeader +0		;als Startverzeichnis speichern.
			lda	dirEntryBuf +2
			sta	curDirHeader +1
			jsr	doDeleteDir		;Verzeichnis löschen.

;--- Datei/Verzeichnis gelöscht.
::test_error		txa				;Fehler?
			beq	:next_file		; => Nein, weiter...
			cpx	#$ff			;Abbruch bei "Schreibschutz"?
			beq	:exit			; => Ja, Ende...

;--- Diskfehler ausgeben.
::error			jsr	HEX2ASCII		;Fehlercode nach HEX wandeln.
			sta	errHexCode +0		;HEX-Werte in Fehlermeldung
			stx	errHexCode +1		;speichern.
			LoadW	r0,Dlg_ErrDelete
			jsr	DoDlgBox		;Fehlermeldung ausgeben.
			rts				;"Datei löschen" beenden.

;--- Weiter mit nächster Datei.
::next_file		AddVBW	17,r15			;Zeiger auf nächste Datei.
			dec	r14H			;Alle Dateien gelöscht?
			beq	:exit			; => Ja, Ende...
			jmp	:loop			; => Nein, weiter...

;--- Ende.
::exit			rts				;"Datei löschen" beenden.

;*** CBM-Datei löschen.
;    Übergabe: dirEntryBuf = Verzeichnis-Eintrag.
:doDeleteFile		jsr	copyDirEntry		;Verzeichnis-Eintrag in
							;Zwischenspeicher kopieren.

			jsr	copyFileName		;Dateiname kopieren.

			jsr	testWrProtOn		;Schreibschutz testen.
			txa				;Fehler?
			beq	:2			; => Nein, weiter...
			bmi	:1			; => Abbruch, Ende...
			ldx	#NO_ERROR		;Schreibschutz-Datei nicht löschen.
::1			rts				;Ende.

;--- Datei löschen.
::2			ldx	#$00			;Flag "Datei löschen".
;			jmp	freeCurFile		;Aktuelle Datei löschen.

;*** Aktuellen Verzeichnis-Eintrag löschen.
;    Übergabe: dirEntryBuf = Verzeichnis-Eintrag.
;              XREG = $00/Datei, $FF/Verzeichnis.
;              r1L/r1H = Tr/Se Verzeichnis-Sektor.
;              r5 = Zeiger auf Verzeichnis-Eintrag in Verzeichnis-Sektor.
;Hinweis: r1/r5 dürfen nicht verändert
;werden (:doDeleteDir/:GetNxtDirEntry).
:freeCurFile		PushB	r1L			;Zeiger auf Verzeichnis-Eintrag
			PushB	r1H			;zwischenspeichern.
			PushW	r5

			jsr	prntCurFile		;Verzeichnis/Datei anzeigen.

			LoadW	r9,dirEntryBuf		;Zeiger auf Datei-Eintrag.
			jsr	FreeFile		;Datei löschen.

			PopW	r5			;Zeiger auf Verzeichnis-Eintrag
			PopB	r1H			;zurücksetzen.
			PopB	r1L

			txa				;Fehler?
			beq	:2			; => Ja, Abbruch...

			lda	dirEntryBuf +22		;GEOS-Dateityp einlesen.
			cmp	#TEMPORARY		;Typ Swap_File ?
			bne	:1			; => Nein, weiter...
			cpx	#BAD_BAM		;Fehler "BAD_BAM" ?
			bne	:1			; => Nein, weiter...
			ldx	#NO_ERROR		; => Kein Fehler.
::1			rts

::2			LoadW	r4,diskBlkBuf		;Zeiger auf Zwischenspeicher setzen.
			jsr	GetBlock		;Verzeichnis-Sektor einlesen.
			txa				;Fehler?
			bne	:1			; => Ja, Abbruch...

			tay				;Dateityp "Gelöscht" setzen.
			sta	(r5L),y
			jmp	PutBlock		;Verzeichnis-Sektor schreiben.

;*** Verzeichnis löschen.
;    Übergabe: dirEntryBuf = Verzeichnis-Eintrag.
;Hinweis: Die Routine löscht rekursiv
;alle Dateien und weiteren Unterver-
;zeichnisse im gewählten Verzeichnis.
:doDeleteDir		bit	GD_DEL_MENU		;Dateien ohne Nachfrage löschen?
			bpl	:initDelDir		; => Nein, weiter...

			bit	GD_DEL_EMPTY		;Nur leere Verzeichnisse löschen?
			bmi	:initDelDir		; => Ja, weiter...

			lda	delDirFiles		;Verzeichnis-Inhalte löschen?
			bne	:initDelDir		; => Bereits definiert, weiter...

;--- Abfrage: Dateien in Verzeichnissen automatisch löschen?
			LoadB	delDirFiles,$ff		;Inhalte in Verz. nicht löschen.

			LoadW	r0,Dlg_AskDelDir	;Abfrage:
			jsr	DoDlgBox		;Verzeichnis-Inhalte löschen?

			lda	sysDBData		;Ergebnis auswerten.
			cmp	#YES			;"Automatisch löschen"?
			bne	:initDelDir		; => Nein, weiter...

			LoadB	delDirFiles,$7f		;Verzeichnis-Inhalte löschen.

;--- Inhalte in Verzeichnis löschen.
::initDelDir		lda	dirEntryBuf +1		;Tr/Se auf Verzeichnis-Header
			sta	r1L			;einlesen.
			lda	dirEntryBuf +2
			sta	r1H
			jsr	OpenSubDir		;Unterverzeichnis öffnen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

;--- Zeiger auf Anfang Verzeichnis.
			jsr	Get1stDirEntry		;Zeiger erster Verzeichnis-Eintrag.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:delDirEntry		; => Ja, weiter...

;--- Verzeichnis-Eintrag auswerten.
::loop			ldy	#$00
			lda	(r5L),y			;Dateityp einlesen.
			and	#%0000 0111		;"Gelöscht"?
			beq	:next_file		; => Ja, nächste Datei...

			lda	delDirFiles		;Verzeichnisinhalt löschen?
			bmi	:skip_dir		; => Nein, nächste Datei.

			bit	GD_DEL_EMPTY		;Nur "Leere Verzeichnisse" löschen?
			bpl	:deleteDir		; => Nein, weiter...

;--- Verzeichnis nicht leer.
;Aktuelles Verzeichnis nicht löschen.
::skip_dir		jsr	reloadDirEntry		;Zeiger auf Verzeichnis-Eintrag.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			jsr	openParentDir		;Eltern-Verzeichnis öffnen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...
			jmp	:exitDirectory		;Das Verzeichnis überspringen und
							;weiter mit nächstem Datei-Eintrag.

;--- Verzeichnis nicht leer.
;Modus: Auch nicht-leere-Verzeichnisse
;       löschen.
::deleteDir		jsr	copyDirEntry		;Verzeichnis-Eintrag in
							;Zwischenspeicher kopieren.

			jsr	copyFileName		;Dateiname kopieren.

			jsr	testWrProtOn		;Schreibschutz testen.
			txa
			beq	:1
			bmi	:error
			bne	:next_file

::1			lda	dirEntryBuf
			and	#%0000 0111		;Dateityp einlesen.
			cmp	#$06			;Verzeichnis?
			bne	:2			; => Nein, Datei löschen.

;--- Weiteres Unterverzeichnis löschen.
			jmp	doDeleteDir		;Verzeichnis Rekursiv löschen.

;--- Disk-Fehler, Abbruch.
::error			rts

;--- Datei im Verzeichnis löschen.
::2			ldx	#$ff
			jsr	freeCurFile		;Aktuelle Datei löschen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

;--- Weiter mit nächsten Eintrag,
::next_file		jsr	GetNxtDirEntry
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			bne	:loop			; => Nein, weiter...

;--- Verzeichnis-Eintrag löschen.
;An dieser Stelle die alle Dateien im
;aktuellen Unterverzeichnis gelöscht.
;Hier wird jetzt das Unterverzeichnis
;selbst gelöscht.
::delDirEntry		jsr	testDirEmpty		;Auf leeres Verzeichnis prüfen.

			jsr	reloadDirEntry		;Verzeichnis-Eintrag einlesen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			ldx	dirNotEmpty		;Verzeichnis leer?
			bmi	:3			; => Nein, löschen überspringen.

			jsr	doDeleteFile		;Verzeichnis-Eintrag selbst löschen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

::3			jsr	openParentDir		;Eltern-Verzeichnis öffnen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

;--- Verzeichnis-Ende erreicht.
::exitDirectory		ldy	#$01			;Zuerst gewähltes Verzeichnis
			lda	(r5L),y			;gelöscht oder Unterverzeichnis?
			cmp	curDirHeader +0
			bne	:9
			iny
			lda	(r5L),y
			cmp	curDirHeader +1
			beq	:exit			; => Verzeichnis gelöscht.

::9			jmp	:next_file		; => Weiter im Unterverzeichnis.

;--- Verzeichnis bearbeitet.
::exit			rts				;Ende.

;*** Aktuelles Verzeichnis testen.
;Wenn Verzeichnis nicht leer, dann
;Hinweis anzeigen.
:testDirEmpty		bit	dirNotEmpty		;Verzeichnis leer?
			bpl	:1			; => Ja, weiter...

			jsr	copyDirName		;Verzeichnis-name kopieren.

			LoadW	r0,Dlg_dirNEmpty	;Fehler ausgeben:
			jsr	DoDlgBox		;"Verzeichnis nicht leer".

::1			rts				;Ende.

;*** Zeiger auf Verzeichnis-Eintrag.
;Setzt r1L/r1H auf Tr/Se für Sektor des
;Verzeichnis-Eintrages für das aktuelle
;Unterverzeichnis und lädt den dazu-
;gehörigen Verzeichnis-Sektor.
:reloadDirEntry		lda	curDirHead +36		;Zeiger auf Tr/Se im Verzeichnis-
			sta	r1L			;Eintrag Elternverzeichnis setzen.
			lda	curDirHead +37
			sta	r1H

			lda	curDirHead +38		;Zeiger auf Byte für Verzeichnis-
			sta	r5L			;Eintrag in Sektor setzen.
			lda	#>diskBlkBuf
			sta	r5H

			LoadW	r4,diskBlkBuf		;Zeiger auf Zwischenspeicher.
			jmp	GetBlock		;Verzeichnis-Sektor einlesen.

;*** Eltern-Verzeichnis öffnen.
;Hinweis: r1/r5 dürfen nicht verändert
;werden (:doDeleteDir/:GetNxtDirEntry).
:openParentDir		PushB	r1L			;Zeiger auf Verzeichnis-Eintrag
			PushB	r1H			;zwischenspeichern.
			PushW	r5

			lda	curDirHead +34		;Zurück zum vorherigen
			sta	r1L			;Verzeichnis.
			lda	curDirHead +35
			sta	r1H
			jsr	OpenSubDir

			PopW	r5			;Zeiger auf Verzeichnis-Eintrag
			PopB	r1H			;zurücksetzen.
			PopB	r1L

			rts

;*** Status-Zeile initialisieren.
:statinfo_posx1		= TASKBAR_MIN_X +8
:statinfo_width1	= 24
:statinfo_posx2		= TASKBAR_MAX_X -48
:statinfo_width2	= 32
:statinfo_posy		= TASKBAR_MAX_Y -5
:prntInfoArea		jsr	ResetFontGD		;GD-Font aktivieren.

			lda	#$00			;Füll-Muster setzen.
			jsr	SetPattern

			jsr	i_FrameRectangle	;Rahmen um Anzeige-Bereich.
			b	TASKBAR_MIN_Y
			b	TASKBAR_MAX_Y
			w	TASKBAR_MIN_X
			w	TASKBAR_MAX_X
			b	%11111111

			jsr	i_Rectangle		;Anzeige-Bereich löschen.
			b	TASKBAR_MIN_Y +1
			b	TASKBAR_MAX_Y -1
			w	TASKBAR_MIN_X +1
			w	TASKBAR_MAX_X -1

			LoadW	r11,TASKBAR_MIN_X +8
			LoadB	r1H,statinfo_posy
			LoadW	r0,delInfoFile
			jsr	PutString		;"Datei:"

			LoadW	r11,statinfo_posx2
;			LoadB	r1H,statinfo_posy
			LoadW	r0,delInfoRemain
			jmp	PutString		;"Auswahl:"

;*** Status-Zeile zurücksetzen.
:clrInfoArea		jsr	ResetFontGD		;GD-Font aktivieren.

			lda	#$00			;Füll-Muster setzen.
			jsr	SetPattern

			jsr	i_Rectangle		;Bereich für Dateiname löschen.
			b	TASKBAR_MIN_Y +1
			b	TASKBAR_MAX_Y -1
			w	statinfo_posx1 +statinfo_width1
			w	statinfo_posx2 -1

			jsr	i_Rectangle		;Bereich für Anzahl verbleibender
			b	TASKBAR_MIN_Y +1
			b	TASKBAR_MAX_Y -1
			w	statinfo_posx2 +statinfo_width2
			w	TASKBAR_MAX_X -1

			rts

;*** Dateiname ausgeben.
:prntCurFile		cpx	#$ff			;Datei/Verzeichnis?
			beq	prntCurDir		; => Verzeichnis, weiter...

			jsr	clrInfoArea		;Anzeigebereich löschen.

			LoadW	r0,curFileName		;Zeiger auf Dateiname.

			LoadW	r11,statinfo_posx1 +statinfo_width1
			LoadB	r1H,statinfo_posy
			jsr	PutString

:prntRemainFiles	LoadW	r11,statinfo_posx2 +statinfo_width2
;			LoadB	r1H,statinfo_posy

			MoveB	r14H,r0L
			ClrB	r0H
			lda	#$00 ! SET_LEFTJUST ! SET_SUPRESS
			jmp	PutDecimal

;*** Dateiname ausgeben.
:prntCurDir		jsr	clrInfoArea		;Anzeigebereich löschen.

			jsr	copyDirName		;Zeiger auf Verzeichnis-Name.

			LoadW	r11,statinfo_posx1 +statinfo_width1
			LoadB	r1H,statinfo_posy
			jsr	PutString		;Verzeichnis-Name ausgeben.

			lda	#"/"
			jsr	PutChar

			LoadW	r0,curFileName		;Zeiger auf Dateiname.
			jsr	PutString		;Datei-Name auusgeben.

			jmp	prntRemainFiles		;Verbleibende Dateien anzeigen.

;*** Verzeichnis-Eintrag kopieren.
;    Übergabe: r5 = Verzeichnis-Eintrag.
:copyDirEntry		ldy	#30 -1			;Verzeichnis-Eintrag in
::1			lda	(r5L),y			;Zwischenspeicher kopieren.
			sta	dirEntryBuf,y
			dey
			bpl	:1
			rts

;*** Datei-/Verzeichnis-Name kopieren.
;    Übergabe: dirEntryBuf = Verzeichnis-Eintrag.
;    Rückgabe: curFileName = Datei-/Verzeichnis-Name.
:copyFileName		LoadW	r10,dirEntryBuf +3
			LoadW	r11,curFileName
			ldx	#r10L
			ldy	#r11L
			jmp	SysCopyName

;*** Aktuellen Disk-/Verzeichnis-Namen kopieren.
:copyDirName		ldx	#r4L			;Zeiger auf aktuellen
			jsr	GetPtrCurDkNm		;Disk-/Verzeichnis-Namen setzen.

			LoadW	r0,curDirName
			ldx	#r4L
			ldy	#r0L
			jmp	SysCopyName		;Name in Zwischenspeicher kopieren.

;*** HEX-Zahl nach ASCII wandeln.
;    Übergabe: AKKU = Hex-Zahl.
;    Rückgabe: AKKU/XREG = LOW/HIGH-Nibble Hex-Zahl.
:HEX2ASCII		pha				;HEX-Wert speichern.
			lsr				;HIGH-Nibble isolieren.
			lsr
			lsr
			lsr
			jsr	:1			;HIGH-Nibble nach ASCII wandeln.
			tax				;Ergebnis zwischenspeichern.

			pla				;HEX-Wert zurücksetzen und
							;nach ASCII wandeln.
::1			and	#%00001111
			clc
			adc	#"0"
			cmp	#$3a			;Zahl größer 10?
			bcc	:2			;Ja, weiter...
			clc				;Hex-Zeichen nach $A-$F wandeln.
			adc	#$07
::2			rts

;*** Auf Schreibschutz testen.
:testWrProtOn		lda	dirEntryBuf		;Dateityp einlesen.
			and	#%0100 0000		;Schreibschtu aktiv?
			beq	:no_error		; => Nein, weiter...

			PushB	r1L			;Zeiger auf Verzeichnis-Eintrag
			PushB	r1H			;zwischenspeichern.
			PushW	r5

			LoadW	r0,Dlg_ErrWrProt	;Fehler ausgeben:
			jsr	DoDlgBox		;"Datei schreibgeschützt".

			PopW	r5			;Zeiger auf Verzeichnis-Eintrag
			PopB	r1H			;zurücksetzen.
			PopB	r1L

			lda	sysDBData		;Auf Schreibschutz testen.
			cmp	#YES			;Schreibschutz ignorieren?
			beq	:no_error		; => Ja, Datei löschen.

			cmp	#NO			;Schreibschutz übernehmen?
			bne	:cancel			; => Nein, weiter...

			lda	#$ff
			sta	dirNotEmpty		;Verzeichnis-Inhalte nicht löschen.

::skip_file		ldx	#$7f			;Rückmeldung: "Nicht löschen".
			rts

::cancel		ldx	WM_WCODE
			ldy	WIN_DRIVE,x
			lda	RealDrvMode -8,y
			and	#%0100 0000
			beq	:1

			lda	curDirHead +32		;Zurück zum aktuellen
			sta	WIN_SDIR_T,x		;Verzeichnis.
			lda	curDirHead +33
			sta	WIN_SDIR_S,x

::1			ldx	#$ff			;Rückmeldung: "Abbruch".
			rts

::no_error		ldx	#NO_ERROR		;Rückmeldung: "Löschen".
			rts

;*** Seite wechseln.
:SwitchPage		sec				;Y-Koordinate der Maus einlesen.
			lda	mouseYPos		;Testen ob Maus innerhalb des
			sbc	#PosSlctPage_y		;"Eselsohrs" angeklickt wurde.
			bcs	:102
::101			rts				;Nein, Rücksprung.

::102			tay
			sec
			lda	mouseXPos+0
			sbc	#<PosSlctPage_x
			tax
			lda	mouseXPos+1
			sbc	#>PosSlctPage_x
			bne	:101
			cpx	#16			;Ist Maus innerhalb "Eselsohr" ?
			bcs	:101			;Nein, Rücksprung.
			cpy	#16
			bcs	:101
			sty	r0L
			txa				;Feststellen: Seite vor/zurück ?
			eor	#%00001111
			cmp	r0L
			bcs	:111			;Seite vor.
			bcc	:121			;Seite zurück.

;*** Weiter auf nächste Seite.
::111			ldx	curFile
			cpx	#$ff
			beq	:112
			inx
			cpx	slctFiles
			bcc	:131
::112			ldx	#$00
			beq	:131

;*** Zurück zur letzten Seite.
::121			ldx	curFile
			beq	:122
			cpx	#$ff
			beq	:122
			bne	:123
::122			ldx	slctFiles
::123			dex

::131			stx	curFile

			stx	r0L
			LoadB	r1L,17
			ldx	#r0L
			ldy	#r1L
			jsr	BBMult

			lda	r0L
			clc
			adc	#<filesBuf
			sta	curFileVec +0
			lda	r0H
			adc	#>filesBuf
			sta	curFileVec +1

;*** Datei-Informationen einlesen.
:ResetFileInfo		lda	curFileVec +0		;Zeiger auf aktuellen
			sta	r6L			;Dateinamen setzen.
			lda	curFileVec +1
			sta	r6H

			ldy	#0			;Aktuellen Dateinamen in
::1			lda	(r6L),y			;Zwischenspeicher kopieren.
			sta	curFileName,y
			iny
			cpy	#16
			bcc	:1

			jmp	RegisterNextOpt

;--- SYS_COPYFNAME:
;slctFiles		b $00
;filesBuf		s MAX_DIR_ENTRIES * 17

;*** Variablen.
:reloadDir		b $00				;GeoDesk/Verzeichnis neu laden.
:dirNotEmpty		b $00
:delDirFiles		b $00

;*** Aktuelle Datei.
:curFile		b $00				;Nummer aktuelle Datei.
:curFileVec		w $0000				;Zeiger auf aktuelle Datei.
:curFileName		s 17
:curDirHeader		b $00,$00			;Tr/Se für aktuellen Dir-Header.
:curDirName		s 17

;*** Texte.
:multipleFiles		b "MEHRFACH-AUSWAHL",NULL
:delInfoFile		b "Datei: ",NULL
:delInfoDir		b "Verzeichnis: ",NULL
:delInfoRemain		b "Auswahl: ",NULL

;*** Fehler: Datei ist Schreibgeschützt.
:Dlg_ErrWrProt		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$18,$2e
			w :3
			b DBTXTSTR   ,$3c,$2e
			w curFileName
			b DBTXTSTR   ,$0c,$3e
			w :4
			b NO         ,$01,$50
			b CANCEL     ,$11,$50
			b YES        ,$09,$50
			b NULL

::2			b PLAINTEXT
			b "Die Datei ist schreibgeschützt!",NULL
::3			b BOLDON
			b "Datei: ",PLAINTEXT,NULL
::4			b PLAINTEXT
			b "Schreibschutz ignorieren?",NULL

;*** Fehler: Datei kann nicht gelöscht werden.
:Dlg_ErrDelete		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$18,$2e
			w :3
			b DBTXTSTR   ,$3c,$2e
			w curFileName
			b DBTXTSTR   ,$0c,$3e
			w :4
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Löschen der Datei fehlgeschlagen!",NULL
::3			b BOLDON
			b "Datei: ",PLAINTEXT,NULL
::4			b "Fehlercode: $"
:errHexCode		b "00",PLAINTEXT,NULL

;*** Fehler: Verzeichnis kann nicht gelöscht werden.
:Dlg_dirNEmpty		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w curDirName
			b DBTXTSTR   ,$0c,$3e
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Das Verzeichnis ist nicht leer!",BOLDON,NULL
::3			b PLAINTEXT
			b "Verzeichnis wird nicht gelöscht.",NULL

;*** Frage: Verzeichnis rekursiv löschen?
:Dlg_AskDelDir		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2e
			w :3
			b NO         ,$11,$50
			b YES        ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Dateien in Unterverzeichnissen",NULL
::3			b "automatisch löschen?",NULL

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Register-Tabelle.
:R1SizeY0		= $28
:R1SizeY1		= $9f
:R1SizeX0		= $0028
:R1SizeX1		= $010f

:RegMenu1		b R1SizeY0			;Register-Größe.
			b R1SizeY1
			w R1SizeX0
			w R1SizeX1

			b 1				;Anzahl Einträge.

			w RTabName1_1			;Register: "LÖSCHEN".
			w RTabMenu1_1

;*** Registerkarten-Icons.
:RTabName1_1		w RTabIcon1
			b RCardIconX_1,R1SizeY0 -$08
			b RTabIcon1_x,RTabIcon1_y

;*** Icons "Löschen"/"Abbruch".
:RIcon_Delete		w IconDelete
			b $00,$00
			b IconDelete_x,IconDelete_y
			b $01

:RIcon_Cancel		w IconCancel
			b $00,$00
			b IconCancel_x,IconCancel_y
			b $01

;*** Icon für Seitenwechsel.
:RIcon_Page		w IconSlctPage
			b $00,$00
			b IconSlctPage_x,IconSlctPage_y
			b $01

:PosSlctPage_x		= (R1SizeX1 +1) -$10
:PosSlctPage_y		= (R1SizeY1 +1) -$10

;******************************************************************************
;*** Register-Menü.
;******************************************************************************
;*** Daten für Register "LÖSCHEN".
:RPos1_x  = R1SizeX0 +$08
:RPos1_y  = R1SizeY0 +$18
:RWidth1  = $0050
:RLine1_1 = $00
:RLine1_2 = $10
:RLine1_3 = $20

:RTabMenu1_1		b 6

			b BOX_ICON			;----------------------------------------
				w $0000
				w SwitchPage
				b PosSlctPage_y
				w PosSlctPage_x
				w RIcon_Page
				b $00

			b BOX_ICON			;----------------------------------------
				w R1T00
				w doDelete
				b (R1SizeY1 +1) -$18
				w R1SizeX0 +$18
				w RIcon_Delete
				b $00
			b BOX_ICON			;----------------------------------------
				w $0000
				w cancelDelete
				b (R1SizeY1 +1) -$18
				w R1SizeX0 +$58
				w RIcon_Cancel
				b $00

			b BOX_STRING			;----------------------------------------
				w R1T01
				w $0000
				b RPos1_y +RLine1_1
				w RPos1_x +RWidth1
				w curFileName
				b 16

			b BOX_OPTION			;----------------------------------------
				w R1T02
				w $0000
				b RPos1_y +RLine1_2
				w RPos1_x
				w GD_DEL_EMPTY
				b %11111111

			b BOX_OPTION			;----------------------------------------
				w R1T03
				w $0000
				b RPos1_y +RLine1_3
				w RPos1_x
				w GD_DEL_MENU
				b %11111111

:R1T00			w RPos1_x
			b RPos1_y +RLine1_1 -$0a
			b "Ausgewählte Datei(en) löschen?",NULL

:R1T01			w RPos1_x
			b RPos1_y +RLine1_1 +$06
			b "Dateiname:",NULL

:R1T02			w RPos1_x +$10
			b RPos1_y +RLine1_2 +$06
			b "Nur leere Verzeichnisse löschen.",NULL

:R1T03			w RPos1_x +$10
			b RPos1_y +RLine1_3 +$06
			b "Dateien ohne Nachfragen löschen."
			b GOTOXY
			w RPos1_x +$10
			b RPos1_y +RLine1_3 +$12
			b "Im PopUp-Menü die C=Taste drücken"
			b GOTOXY
			w RPos1_x +$10
			b RPos1_y +RLine1_3 +$1b
			b "um Nachfragen einzuschalten.",NULL

;*** Icons für Registerkarten.
:RTabIcon1
<MISSING_IMAGE_DATA>

:RTabIcon1_x		= .x
:RTabIcon1_y		= .y

;RTabIcon2

;RTabIcon2_x		= .x
;RTabIcon2_y		= .y

;*** X-Koordinate der Register-Icons.
:RCardIconX_1		= (R1SizeX0/8) +3
;RCardIconX_2		= RCardIconX_1 + RTabIcon1_x

;*** Register-Funktions-Icons.
:IconCancel
<MISSING_IMAGE_DATA>
:IconCancel_x		= .x
:IconCancel_y		= .y

:IconDelete
<MISSING_IMAGE_DATA>
:IconDelete_x		= .x
:IconDelete_y		= .y

:IconSlctPage
<MISSING_IMAGE_DATA>

:IconSlctPage_x		= .x
:IconSlctPage_y		= .y

;******************************************************************************
;*** Icon-Menü.
;******************************************************************************
;*** Icon-Menü "Beenden".
:IconMenu		b $01
			w $0000
			b $00

			w IconExit
			b (R1SizeX0/8) +1,R1SizeY0 -$08
			b IconExit_x,IconExit_y
			w ExitDelFiles

;*** Icon zum schließen des Menüs.
:IconExit
<MISSING_IMAGE_DATA>

:IconExit_x		= .x
:IconExit_y		= .y

