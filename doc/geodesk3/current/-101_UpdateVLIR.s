﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Aktuelles Programm-Modul speichern.
;Sichert u.a. Variablen des Fenstermanagers.
:UPDATE_CURMOD		ldy	GD_VLIR_ACTIVE		;Aktuelles VLIR-Modul sichern.
			b $2c

;*** Haupt-Modul speichern.
:UPDATE_MAINMOD		ldy	#$01			;Haupt-Menü sichern.
			b $2c

;*** Variablen speichern.
:UPDATE_APPVAR		ldy	#$00			;Variablen ab APP_RAM sichern.

			pha				;AKKU/XReg sichern.
			txa				;Beinhaltet ggf. Programm-Modul
			pha				;und Einsprungadresse.

			tya
			jsr	SetVlirVecRAM		;Sichert u.a. Variablen des
			jsr	StashRAM		;Fenstermanagers.

			pla
			tax
			pla

			rts
