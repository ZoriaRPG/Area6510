﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Gemeinsam genutzte Variablen.

;--- GetFileData: Datenlaufwerk.
.getFileWin		b $00
.getFileDrv		b $00
.getFilePart		b $00
.getFileSDir		b $00,$00

;--- SortDir: Eintrag tauschen.
;--- DoFileEntry: Verzeichnis-Eintrag.
.dataBufDir		s 32
.dataBufIcon		s 64

;--- Dialogbox-Titel.
.Dlg_Titel_Info		b PLAINTEXT,BOLDON
			b "INFORMATION"
			b NULL

.Dlg_Titel_Error	b PLAINTEXT,BOLDON
			b "FEHLER"
			b NULL
