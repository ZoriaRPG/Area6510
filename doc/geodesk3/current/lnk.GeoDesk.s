﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n   "GeoDesk64"			;Name VLIR-Datei.

			h   "Eine neue Generation der"
			h   "      DeskTop-Oberflächen..."
			h   ""
			h   "Für C64 und MegaPatch64!"

;Beim hinzufügen eines VLIR-Moduls muss
;:GD_VLIR_COUNT in TopSym.GD angepasst
;werden!

			m
			-   "mod.#100.obj"		;Hauptmodul.
			-   "mod.#101.obj"		;WindowManager.
			-   "mod.#102.obj"		;DeskTop.
			-   "mod.#103.obj"		;Partition wechseln.
			-   "mod.#104.obj"		;AppLink laden/speichern.
			-   "mod.#105.obj"		;Datei öffnen.
			-   "mod.#106.obj"		;Dateien einlesen.
			-   "mod.#107.obj"		;Konfiguration speichern.
			-   "mod.#108.obj"		;Konfiguration speichern.
			-   "mod.#109.obj"		;Konfiguration speichern.
			/
