﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

.SYSVAR_START

;*** 64K-Speicherbank für ScreenBuffer:
;$0000-$1fff = ScreenBuffer Fenster #1/Grafik.
;$2000-$3fff = ScreenBuffer Fenster #2/Grafik.
;$4000-$5fff = ScreenBuffer Fenster #3/Grafik.
;$6000-$7fff = ScreenBuffer Fenster #4/Grafik.
;$8000-$9fff = ScreenBuffer Fenster #5/Grafik.
;$a000-$bfff = ScreenBuffer Fenster #6/Grafik.
;$c000-$dfff = ScreenBuffer Fenster #7/Grafik.
;$e000-$e3ff = ScreenBuffer Fenster #1/Farbe.
;$e400-$e7ff = ScreenBuffer Fenster #2/Farbe.
;$e800-$ebff = ScreenBuffer Fenster #3/Farbe.
;$ec00-$efff = ScreenBuffer Fenster #4/Farbe.
;$f000-$f3ff = ScreenBuffer Fenster #5/Farbe.
;$f400-$f7ff = ScreenBuffer Fenster #6/Farbe.
;$f800-$fbff = ScreenBuffer Fenster #7/Farbe.
.GD_SCRN_STACK		b $00				;64K Speicher für ScreenBuffer #1-7.

;*** 64K-Speicher für Systemdaten:
;--- Bildschirmspeicher wird für RecoverRectangle/Menüs verwendet.
;$0000-$1f3f = Bildschirmspeicher /Grafik.
;$0140-$2327 = Bildschirmspeicher /Farbe.
;--- Verzeichnis-Daten Fenster #1 bis #6.
;Hinweis: Fenster #0 ist der DeskTop, max. 6 Dateifenster möglich.
;$2400-$37ff = Verzeichnis-Daten Fenster #1.
;$3800-$4bff = Verzeichnis-Daten Fenster #2.
;$4c00-$5fff = Verzeichnis-Daten Fenster #3.
;$6000-$73ff = Verzeichnis-Daten Fenster #4.
;$7400-$87ff = Verzeichnis-Daten Fenster #5.
;$8800-$9bff = Verzeichnis-Daten Fenster #6.
;--- Speicher für BAM-CRC-Daten (4x256 Bytes).
;$9c00-$9fff = BAM-CRC für Fenster #1.
;$a000-$a3ff = BAM-CRC für Fenster #2.
;$a400-$a7ff = BAM-CRC für Fenster #3.
;$a800-$abff = BAM-CRC für Fenster #4.
;$ac00-$afff = BAM-CRC für Fenster #5.
;$b000-$b3ff = BAM-CRC für Fenster #6.
;$b400-$ffff = Frei.
.GD_SYSDATA_BUF		b $00				;64K Speicher für Systemdaten.
.vecDirDataRAM		w $ffff				;Fenster #0 = Desktop.
			w $2400				;Dateifenster #1.
			w $3800				;Dateifenster #2.
			w $4c00				;Dateifenster #3.
			w $6000				;Dateifenster #4.
			w $7400				;Dateifenster #5.
			w $8800				;Dateifenster #6.
.vecBAMDataRAM		w $ffff				;Fenster #0 = Desktop.
			w $9c00				;BAM-CRC Fenster #1.
			w $a000				;BAM-CRC Fenster #2.
			w $a400				;BAM-CRC Fenster #3.
			w $a800				;BAM-CRC Fenster #4.
			w $ac00				;BAM-CRC Fenster #5.
			w $b000				;BAM-CRC Fenster #6.

;*** 64K-Speicher für Icon-Daten:
;--- Icon/Grafik-Daten für Dateien.
;Hinweis: Fenster #0 ist der DeskTop, max. 6 Dateifenster möglich.
;$0000-$27ff = Icon-Daten für Fenster #1.
;$2800-$4fff = Icon-Daten für Fenster #2.
;$5000-$77ff = Icon-Daten für Fenster #3.
;$7800-$9fff = Icon-Daten für Fenster #4.
;$a000-$c7ff = Icon-Daten für Fenster #5.
;$c800-$efff = Icon-Daten für Fenster #6.
.GD_ICONDATA_BUF	b $00				;64K Speicher für Systemdaten.
.vecIconDataRAM		w $ffff				;Fenster #0 = Desktop.
			w $0000				;Dateifenster #1.
			w $2800				;Dateifenster #2.
			w $5000				;Dateifenster #3.
			w $7800				;Dateifenster #4.
			w $a000				;Dateifenster #5.
			w $c800				;Dateifenster #6.

;*** 64K-Speicher für GeoDesk:
;$0000-$e3ff = GeoDesk.
;--- Bisher nur schreibend in :UpdateCore genutzt:
;$e400-$ffff = Verzeichnis-Daten.
;ToDo: Kann evtl. entfallen wenn Cache in GD_SYSTEM_BUF aktiviert wird.
.GD_RAM_GDESK		b $00				;64K Speicher für GeoDesk.

;*** Systeminformationen.
.GD_CLASS		t "-SYS_CLASS"

;*** AppLink-Konfigurationsdatei.
.GD_APPLINK		b "GeoDesk.lnk",NULL

;*** Speicher für VLIR-Informationen RAM-GeoDesk.
:GD_DACC_ADDR		s GD_VLIR_COUNT * 4

;*** VLIR-Module.
.GD_VLIR_ACTIVE		b $00

;*** Name Hauptprogramm.
.GD_SYS_NAME		s 17

;*** Laufwerk, von dem GeoDesk gestartet wurde.
.BootDrive		b $00
.BootPart		b $00
.BootType		b $00
.BootMode		b $00
.BootSDir		b $00,$00
.BootRBase		b $00

;*** Zwischenspeicher für Laufwerkswechsel.
.TempDrive		b $00
.TempMode		b $00
.TempPart		b $00
.TempSDir		b $00,$00

;*** Laufwerk für AppLink-Konfigurationsdatei.
.LinkDrive		b $00
.LinkPart		b $00
.LinkType		b $00
.LinkMode		b $00
.LinkSDir		b $00,$00
.LinkRBase		b $00

.SYSVAR_END
.SYSVAR_SIZE = SYSVAR_END - SYSVAR_START
