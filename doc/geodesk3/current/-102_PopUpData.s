﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;
;Hinweis:
;
;Für neue Untermenüs muss:
; * Menutext bei PTx_... angelegt werden.
; * Die Menüdaten als WORD bei ":PDat_Vec" ergänzt werden.
; * Die Funktionsroutinen als WORD bei ":PSys_Vec" ergänzt werden.
; * Die Breite des Menüs in "::PDat_Width" ergänzt werden.
;

;*** Texte für Menüs.
;    Hinweis: Ein "_H" am Ende markiert
;    den ersten Eintrag in einem Menü.
;    Hier muss BOLDON gesetzt werden.
:PTx_050_H		b BOLDON
			b "Überlappend",NULL
:PTx_051		b "Nebeneinander",NULL
:PTx_052_H		b BOLDON
			b "Neu laden",NULL
:PTx_053		b "Neue Ansicht",NULL
:PTx_054		b "( ) Hintergrundbild",NULL
:PTx_055		b "( ) AppLink sperren",NULL
:PTx_056		b "Hintergrund wechseln",NULL
:PTx_057		b "Fenster schließen",NULL

:PTx_100_H		b BOLDON
			b "Öffnen",NULL
:PTx_101		b "Partition wechseln",NULL
:PTx_102_H		b BOLDON
			b "Installieren",NULL
:PTx_103_H		b BOLDON
			b "Drucker",NULL
:PTx_104		b "Löschen",NULL
:PTx_105		b "Drucker wechseln",NULL
:PTx_106_H		b BOLDON
			b "Hauptverzeichnis",NULL
:PTx_107		b "Verzeichnis zurück",NULL
:PTx_108		b "( ) Titel anzeigen",NULL
:PTx_109_H		b BOLDON
			b "Laufwerk öffnen",NULL
:PTx_110		b "  >> Laufwerk A:",NULL
:PTx_111		b "  >> Laufwerk B:",NULL
:PTx_112		b "  >> Laufwerk C:",NULL
:PTx_113		b "  >> Laufwerk D:",NULL
:PTx_114_H		b BOLDON
			b "( ) Größe in KByte",NULL
:PTx_115		b "( ) Textmodus",NULL
:PTx_116		b "( ) Details zeigen",NULL
:PTx_117		b "( ) Anzeige bremsen",NULL
:PTx_119_H		b BOLDON
			b "Eingabegerät",NULL
:PTx_120		b "( ) Dateifilter",NULL
:PTx_121		b "Eigenschaften",NULL

:PTx_130		b ">> Sortieren",NULL
:PTx_131_H		b BOLDON
			b "Dateiname",NULL
:PTx_132		b "Dateigröße",NULL
:PTx_133		b "Datum Alt->Neu",NULL
:PTx_134		b "Datum Neu->Alt",NULL
:PTx_135		b "Dateityp",NULL
:PTx_136		b "GEOS-Dateityp",NULL

:PTx_140		b ">> Auswahl",NULL
:PTx_141_H		b BOLDON
			b "Alle auswählen",NULL
:PTx_142		b "Auswahl aufheben",NULL

:PTx_200		b "AppLink löschen",NULL
:PTx_201		b "AppLink erstellen",NULL
:PTx_202		b "Umbenennen",NULL

:PTx_300		b ">> Anzeige",NULL
:PTx_301		b ">> Diskette",NULL
:PTx_302		b ">> Datei",NULL

:PTx_400_H		b BOLDON
			b "Löschen",NULL
:PTx_401		b "Formatieren",NULL
:PTx_402		b "Aufräumen",NULL
:PTx_403		b "Eigenschaften",NULL

:PTx_500_H		b BOLDON
			b "Alle Dateien",NULL
:PTx_501		b "Anwendungen",NULL
:PTx_502		b "Autostart",NULL
:PTx_503		b "Dokumente",NULL
:PTx_504		b "Hilfsmittel",NULL
:PTx_505		b "Zeichensatz",NULL
:PTx_506		b "Druckertreiber",NULL
:PTx_507		b "Eingabetreiber",NULL
:PTx_508		b "BASIC-Programme",NULL

;*** Zeiger auf Menüdaten.
:PDat_Vec		w PDat_000			;PopUp auf DeskTop.
			w PDat_001			;PopUp auf AppLink.
			w PDat_002			;PopUp auf Arbeitsplatz.
			w PDat_003			;PopUp auf AppLink/Drucker.
			w PDat_004			;PopUp auf AppLink/Laufwerk/CMD/SD.
			w PDat_005			;PopUp auf Arbeitsplatz/Drucker.
			w PDat_006			;PopUp auf Datei in Fenster.
			w PDat_007			;PopUp auf Fenster/Lfwk.1541/71/81.
			w PDat_008			;PopUp auf Arbeitsplatz/Lfwk.CMD.
			w PDat_009			;PopUp auf Titel/Lfwk.Native.
			w PDat_010			;PopUp auf AppLink/Verzeichnis.
			w PDat_011			;Untermenü "Anzeige" auf Fens/Lfwk.
			w PDat_012			;Untermenü "Diskette" auf Fens/Lfwk.
			w PDat_013			;PopUp auf Arbeitsplatz/Eingabe.
			w PDat_014			;Untermenü "Dateifilter".
			w PDat_015			;Untermenü "Sortieren".
			w PDat_016			;Untermenü "Auswahl".
			w PDat_017			;PopUp Titelzeile CMD/Native.
			w PDat_018			;PopUp auf AppLink/Laufwerk/Std.
			w PDat_019			;PopUp auf Arbeitsplatz/Lfwk.Std.

;*** Zeiger auf Funktionstabellen.
;Die Tabellen entalten je ein WORD für
;jede Routine die aufgerufen werden
;soll, oder $0000 für ein Untermenü.
:PSys_Vec		w PSys_000
			w PSys_001
			w PSys_002
			w PSys_003
			w PSys_004
			w PSys_005
			w PSys_006
			w PSys_007
			w PSys_008
			w PSys_009
			w PSys_010
			w PSys_011
			w PSys_012
			w PSys_013
			w PSys_014
			w PSys_015
			w PSys_016
			w PSys_017
			w PSys_018
			w PSys_019

;*** Breite der GEOS-Menüs in Pixel.
;Wegen der Menüfarben muss der Wert auf
;$x7 oder $xF = volles CARD enden.
:PDat_Width		b $7f				;000
			b $5f				;001
			b $7f				;002
			b $6f				;003
			b $6f				;004
			b $6f				;005
			b $5f				;006
			b $7f				;007
			b $6f				;008
			b $7f				;009
			b $5f				;010
			b $7f				;011
			b $5f				;012
			b $5f				;013
			b $6f				;014
			b $5f				;015
			b $6f				;016
			b $6f				;017
			b $6f				;018
			b $6f				;019

;*** PopUp auf DeskTop.
:PDat_000		b $00,$00
			w $0000,$0000

			b 7!VERTICAL

			w PTx_050_H
			b MENU_ACTION
			w PExit_000

			w PTx_051
			b MENU_ACTION
			w PExit_000

			w PTx_056
			b MENU_ACTION
			w PExit_000

			w PTx_054
			b MENU_ACTION
			w PExit_000

			w PTx_108
			b MENU_ACTION
			w PExit_000

			w PTx_055
			b MENU_ACTION
			w PExit_000

			w PTx_057
			b MENU_ACTION
			w PExit_000

:PSys_000		w WM_FUNC_SORT
			w WM_FUNC_POS
			w MNU_OPEN_BACKSCR
			w PF_BACK_SCREEN
			w PF_VIEW_ALTITLE
			w PF_LOCK_APPLINK
			w WM_CLOSE_ALL_WIN

;*** PopUp auf AppLink.
:PDat_001		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_001

			w PTx_200
			b MENU_ACTION
			w PExit_001

			w PTx_202
			b MENU_ACTION
			w PExit_001

:PSys_001		w AL_OPEN_ENTRY
			w AL_DEL_ENTRY
			w AL_RENAME_ENTRY

;*** PopUp auf Arbeitsplatz.
:PDat_002		b $00,$00
			w $0000,$0000

			b 5!VERTICAL

			w PTx_109_H
			b MENU_ACTION
			w PExit_002

			w PTx_110
			b MENU_ACTION
			w PExit_002

			w PTx_111
			b MENU_ACTION
			w PExit_002

			w PTx_112
			b MENU_ACTION
			w PExit_002

			w PTx_113
			b MENU_ACTION
			w PExit_002

:PSys_002		w AL_OPEN_ENTRY
			w PF_OPEN_DRV_A
			w PF_OPEN_DRV_B
			w PF_OPEN_DRV_C
			w PF_OPEN_DRV_D

;*** PopUp auf AppLink/Drucker.
:PDat_003		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PTx_102_H
			b MENU_ACTION
			w PExit_003

			w PTx_105
			b MENU_ACTION
			w PExit_003

			w PTx_200
			b MENU_ACTION
			w PExit_003

			w PTx_202
			b MENU_ACTION
			w PExit_003

:PSys_003		w AL_OPEN_PRNT
			w AL_SWAP_PRINTER
			w AL_DEL_ENTRY
			w AL_RENAME_ENTRY

;*** PopUp auf AppLink/Laufwerk.
:PDat_004		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_004

			w PTx_101
			b MENU_ACTION
			w PExit_004

			w PTx_200
			b MENU_ACTION
			w PExit_004

			w PTx_202
			b MENU_ACTION
			w PExit_004

:PSys_004		w AL_OPEN_ENTRY
			w AL_OPEN_DSKIMG
			w AL_DEL_ENTRY
			w AL_RENAME_ENTRY

;*** PopUp auf Arbeitsplatz/Drucker.
:PDat_005		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PTx_103_H
			b MENU_ACTION
			w PExit_005

			w PrntFileName
			b MENU_ACTION
			w PExit_005

:PSys_005		w AL_SWAP_PRINTER
			w AL_SWAP_PRINTER

;*** PopUp auf Arbeitsplatz/Eingabe.
:PDat_013		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PTx_119_H
			b MENU_ACTION
			w PExit_013

			w inputDevName
			b MENU_ACTION
			w PExit_013

:PSys_013		w AL_OPEN_INPUT
			w AL_OPEN_INPUT

;*** PopUp auf Datei in Fenster.
:PDat_006		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_006

			w PTx_121
			b MENU_ACTION
			w PExit_006

			w PTx_104
			b MENU_ACTION
			w PExit_006

:PSys_006		w PF_OPEN_FILE
			w PF_FILE_INFO
			w PF_DEL_FILE

;*** PopUp auf Fenster/Laufwerk.
:PDat_007		b $00,$00
			w $0000,$0000

			b 8!VERTICAL

			w PTx_052_H			;Neu laden.
			b MENU_ACTION
			w PExit_007

			w PTx_053			;Neue Ansicht.
			b MENU_ACTION
			w PExit_007

			w PTx_140			;>> Auswahl.
			b DYN_SUB_MENU
			w OPEN_MENU_SELECT

			w PTx_120			;>> Dateifilter.
			b DYN_SUB_MENU
			w OPEN_MENU_FILTER

			w PTx_130			;>> Sortieren.
			b DYN_SUB_MENU
			w OPEN_MENU_SORT

			w PTx_300			;>> Anzeige.
			b DYN_SUB_MENU
			w OPEN_MENU_VOPT

			w PTx_301			;>> Diskette.
			b DYN_SUB_MENU
			w OPEN_MENU_DISK

			w PTx_201			;Applink erstellen.
			b MENU_ACTION
			w PExit_007

:PSys_007		w PF_RELOAD_DISK
			w PF_NEW_VIEW
			w $0000
			w $0000
			w $0000
			w $0000
			w $0000
			w PF_CREATE_AL

;*** PopUp auf Arbeitsplatz/Laufwerk CMD- oder SD2IEC-Laufwerk.
:PDat_008		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_008

			w PTx_053
			b MENU_ACTION
			w PExit_008

			w PTx_101
			b MENU_ACTION
			w PExit_008

:PSys_008		w PF_OPEN_DRIVE
			w PF_OPEN_NEWVIEW
			w PF_MYCOMP_PART

;*** PopUp auf Titel/Native.
:PDat_009		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PTx_106_H			;Hauptverzeichnis.
			b MENU_ACTION
			w PExit_009

			w PTx_107			;Ein Verzeichnis zurück.
			b MENU_ACTION
			w PExit_009

:PSys_009		w PF_OPEN_ROOT
			w PF_OPEN_PARENT

;*** PopUp auf AppLink/Verzeichnis.
:PDat_010		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_010

			w PTx_200
			b MENU_ACTION
			w PExit_010

			w PTx_202
			b MENU_ACTION
			w PExit_010

:PSys_010		w PF_OPEN_SDIR
			w AL_DEL_ENTRY
			w AL_RENAME_ENTRY

;*** Untermenü "Anzeige" auf Fenster/Laufwerk.
:PDat_011		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PTx_114_H			;Größe in KByte/Blocks.
			b MENU_ACTION
			w PExit_011

			w PTx_115			;Textmodus.
			b MENU_ACTION
			w PExit_011

			w PTx_116			;Details zeigen.
			b MENU_ACTION
			w PExit_011

			w PTx_117			;Anzeige bremsen.
			b MENU_ACTION
			w PExit_011

:PSys_011		w PF_VIEW_SIZE
			w PF_VIEW_ICONS
			w PF_VIEW_DETAILS
			w PF_VIEW_SLOWMOVE

;*** Untermenü "Diskette" auf Fenster/Laufwerk.
:PDat_012		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PTx_400_H
			b MENU_ACTION
			w PExit_012

			w PTx_401
			b MENU_ACTION
			w PExit_012

			w PTx_402
			b MENU_ACTION
			w PExit_012

			w PTx_403
			b MENU_ACTION
			w PExit_012

:PSys_012		w $0000
			w $0000
			w $0000
			w $0000

;*** Untermenü "Anzeige/Dateifilter" auf Fenster/Laufwerk.
:PDat_014		b $00,$00
			w $0000,$0000

			b 9!VERTICAL

			w PTx_500_H
			b MENU_ACTION
			w PExit_014

			w PTx_501
			b MENU_ACTION
			w PExit_014

			w PTx_502
			b MENU_ACTION
			w PExit_014

			w PTx_503
			b MENU_ACTION
			w PExit_014

			w PTx_504
			b MENU_ACTION
			w PExit_014

			w PTx_505
			b MENU_ACTION
			w PExit_014

			w PTx_506
			b MENU_ACTION
			w PExit_014

			w PTx_507
			b MENU_ACTION
			w PExit_014

			w PTx_508
			b MENU_ACTION
			w PExit_014

:PSys_014		w PF_FILTER_ALL
			w PF_FILTER_APPS
			w PF_FILTER_EXEC
			w PF_FILTER_DOCS
			w PF_FILTER_DA
			w PF_FILTER_FONT
			w PF_FILTER_PRNT
			w PF_FILTER_INPT
			w PF_FILTER_BASIC

;*** Untermenü "Sortieren" auf Fenster/Laufwerk.
:PDat_015		b $00,$00
			w $0000,$0000

			b 6!VERTICAL

			w PTx_131_H
			b MENU_ACTION
			w PExit_015

			w PTx_132
			b MENU_ACTION
			w PExit_015

			w PTx_133
			b MENU_ACTION
			w PExit_015

			w PTx_134
			b MENU_ACTION
			w PExit_015

			w PTx_135
			b MENU_ACTION
			w PExit_015

			w PTx_136
			b MENU_ACTION
			w PExit_015

:PSys_015		w PF_SORT_NAME
			w PF_SORT_SIZE
			w PF_SORT_DATE_OLD
			w PF_SORT_DATE_NEW
			w PF_SORT_TYPE
			w PF_SORT_GEOS

;*** Untermenü "Auswahl" auf Fenster/Laufwerk.
:PDat_016		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PTx_141_H
			b MENU_ACTION
			w PExit_016

			w PTx_142
			b MENU_ACTION
			w PExit_016

:PSys_016		w PF_SELECT_ALL
			w PF_SELECT_NONE

;*** PopUp auf Titel/CMD-Native.
:PDat_017		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_106_H			;Hauptverzeichnis.
			b MENU_ACTION
			w PExit_017

			w PTx_107			;Ein Verzeichnis zurück.
			b MENU_ACTION
			w PExit_017

			w PTx_101			;Partition wechseln.
			b MENU_ACTION
			w PExit_017

:PSys_017		w PF_OPEN_ROOT
			w PF_OPEN_PARENT
			w PF_SWAP_DSKIMG

;*** PopUp auf AppLink/Laufwerk/Standard.
:PDat_018		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_018

			w PTx_200
			b MENU_ACTION
			w PExit_018

			w PTx_202
			b MENU_ACTION
			w PExit_018

:PSys_018		w AL_OPEN_ENTRY
			w AL_DEL_ENTRY
			w AL_RENAME_ENTRY

;*** PopUp auf Arbeitsplatz/Laufwerk.
:PDat_019		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PTx_100_H
			b MENU_ACTION
			w PExit_019

			w PTx_053
			b MENU_ACTION
			w PExit_019

:PSys_019		w PF_OPEN_DRIVE
			w PF_OPEN_NEWVIEW
