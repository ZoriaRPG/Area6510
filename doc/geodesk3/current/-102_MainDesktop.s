﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   MainBoot
;Funktion:  Einsprung aus BOOT-Routine:
;           -GeoDesk initialisieren.
;           -AppLinks einlesen.
;           -DeskTop zeichnen.
;           -TaskBar/Uhr zeichnen.
;           -Fenstermanager starten.
;           -Variablen speichern.
;******************************************************************************
:MainBoot		jsr	LNK_LOAD_DATA		;AppLink-Daten einlesen.

			jsr	ResetFontGD		;GeoDesk-Zeichensatz aktivieren.

			jsr	SET_TEST_CACHE		; => BAM testen/Cache oder Disk.

			LoadW	r0,WIN_DESKTOP
			jsr	WM_COPY_WIN_DATA	;DeskTop-Daten setzen.

			jsr	MainDrawDesktop		;DeskTop neu zeichnen.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jmp	UpdateCore		;GeoDesk-Systemvariablen speichern.
							;Damit stehen die Systemvariablen
							;auch nach einem RBOOT im Speicher.

;******************************************************************************
;Routine:   MainReBoot
;Funktion:  Einsprung über EnterDeskTop:
;           -GeoDesk initialisieren.
;           -Laufwerke testen.
;           -DeskTop zeichnen.
;           -TaskBar/Uhr zeichnen.
;           -Fenstermanager starten.
;           -Fenster neu zeichnen.
;******************************************************************************
:MainReBoot		jsr	ResetFontGD		;GeoDesk-Zeichensatz aktivieren.

			jsr	CheckWinStatus		;Laufwerke auf Gültigkeit testen.

;--- Geöffnete Fenster neu zeichnen.
			jsr	MainDrawDesktop		;DeskTop neu zeichnen.

			lda	#MAX_WINDOWS -1		;Zeiger auf letztes Fenster.
::1			pha
			tax
			lda	WM_STACK,x		;Fenster aktiv?
			beq	:2			; => MyComp... Weiter...
			bmi	:2			; => Nein, Weiter...

			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	OpenWinDrive		;Fensterlaufwerk öffnen.
			txa				;Fehler?
			bne	:2			; => Nein, weiter...#

if TRUE
;--- BAM auf Veränderungen testen.
;Wenn verändert, dann Disk neu einlesen, sonst aus Cache einlesen.
::3			jsr	SET_TEST_CACHE		; => BAM testen/Cache oder Disk.
endif
if FALSE
;--- Alternativ: Dateien immer von Disk einlesen.
::3			jsr	SET_LOAD_DISK		; => Dateien von Disk einlesen.
endif
							;Erforderlich damit bei Rückkehr
							;von GEOS/DeskTop beim neu zeichnen
							;der Fensters alle Dateien im
							;Speicher sind.

			jsr	WM_CALL_REDRAW		;Fenster zeichnen.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten aktualisieren.

::2			pla
			sec
			sbc	#$01			;Alle Fenster gezeichnet?
			bpl	:1			; => Nein, weiter...

			jmp	SET_LOAD_CACHE		;GetFiles-Modus zurücksetzen.

;******************************************************************************
;Routine:   MainUpdate
;Funktion:  Einsprung aus Disk-/Dateifunktionen:
;           -DeskTop zeichnen.
;           -TaskBar/Uhr zeichnen.
;           -Fenstermanager starten.
;           -Verzeichnis aus Cache einlesen.
;           -Datei-Auswahl aufheben.
;           -Fenster aus ScreenBuffer laden.
;           -Oberstes Fenster neu zeichnen.
;******************************************************************************
:MainUpdate		jsr	MainDrawDesktop		;DeskTop neu zeichnen.
			jsr	RESET_DIR_DATA		;Verzeichnis-Daten einlesen.
			jmp	WM_UPDATE		;Oberstes Fenster aktualisieren.

;******************************************************************************
;Routine:   MainReStart
;Funktion:  Rückkehr zum FensterManager:
;           -TaskBar/Uhr aktivieren.
;           -Fenstermanager starten.
;******************************************************************************
:MainReStart		jsr	ReStartTaskBar		;TaskBar starten.
			jmp	InitForWM		;Fenstermanager starten.

;******************************************************************************
;Routine:   MainInitWM
;Funktion:  Rückkehr zum FensterManager:
;           -DeskTop zeichnen.
;           -TaskBar/Uhr zeichnen.
;           -Fenstermanager starten.
;           -Verzeichnis aus Cache einlesen.
;           -Alle Fenster aus ScreenBuffer laden.
;******************************************************************************
:MainInitWM		jsr	MainDrawDesktop		;DeskTop neu zeichnen.
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** Fenster/Verzeichnis-Daten aktivieren.
:RESET_DIR_DATA		lda	WM_STACK		;DeskTop aktiv?
			beq	:1			; => Ja, Ende...

			sta	WM_WCODE		;Oberstes Fenster aktivieren.
			jsr	WM_LOAD_WIN_DATA

			jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.
			jsr	FetchRAM		;Verzeichnis aus Cache einlesen.

			lda	#$00
			jsr	SetFileSlctMode		;Datei-Auswahl aufheben.

::1			rts

;*** Gültigkeit der Laufwerksfenster testen.
;Hinweis: Wurden Laufwerke z.B. mit
;         dem GEOS.Editor gewechselt,
;         dann muss hier sichergestellt
;         werden das die Laufwerksdaten
;         noch zu den Fensterdaten
;         passen.
;         Nein => Fenster schließen.
:CheckWinStatus		ldx	#MAX_WINDOWS -1		;Max. Fenster-Nr. einlesen.
::1			lda	WM_STACK,x		;Fenster-Nr. einlesen.
			beq	:2			; => Desktop, weiter...
			bmi	:2			; => Leer, weiter...

			ldy	WIN_DRIVE,x		;Laufwerk definiert?
			beq	:2			; => Nein, weiter...

			lda	WIN_REALTYPE,x
			cmp	RealDrvType -8,y	;Laufwerkstyp verändert?
			beq	:2			; => Nein, weiter...

			txa				;Zähler für Fenster-Nr.
			pha				;speichern.

			sta	WM_WCODE		;Fenster-Nr. setzen.
			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			lda	WM_WCODE		;Fenster schließen.
			jsr	WM_CLOSE_WINDOW

			pla				;Zähler für Fenster-Nr.
			tax				;zurücksetzen.

::2			dex				;Alle Fenster geprüft?
			bpl	:1			; => Nein, weiter...
			rts

;*** Desktop zeichnen.
:MainDrawDesktop	jsr	WM_CLEAR_SCREEN		;BIldschirm zeichnen.
			jsr	AL_DRAW_FILES		;AppLinks zeichnen.
			jsr	InitTaskBar		;TaskBar darstellen.
			jsr	InitForWM		;Fenstermanager starten.

			lda	#$00			;DeskTop als erstes Fenster setzen.
			sta	WM_WCODE

			jmp	WM_SAVE_SCREEN		;Bildschirm speichern.
