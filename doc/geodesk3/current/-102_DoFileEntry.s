﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Einzelnen Datei-Eintrag ausgeben.
;    Aufruf aus Fenster-Manager.
;    Übergabe: r0 = Aktueller Eintrag.
;              r1L/r1H = XPos/YPos.
;              r2L/r2H = MaxX/MaxY.
;              r3L/r3H = GridX/GridY
:GD_PRINT_ENTRY		MoveW	r0,r15			;Zeiger auf Verzeichnis-Eintrag
			ldx	#r15L			;im RAM berechnen.
			ldy	#r9L
			jsr	SET_POS_RAM

			jsr	ReadFName		;Dateiname kopieren.

			ldx	WM_WCODE
			lda	WMODE_VICON,x		;Anzeige-Modus einlesen.
			bne	:text_mode		; => Keine Icons anzeigen.

;--- Icon-Ausgabe.
			lda	r1L			;Für Icon-Anzeige XPos +3 Cards.
			clc
			adc	#$03
			cmp	r2L			;Platz für weiteres Icon?
			bcc	:icon_mode		; => Ja, weiter...
			ldx	#$00			; => Kein Icon ausgegeben.
			rts

::icon_mode		sta	r1L

;--- Farbe für Datei-Icon ermitteln.
			jsr	DefGTypeID		;Zeiger auf Farb-Tabelle setzen.
			tax

;--- Icons in Farbe oder S/W?
			bit	GD_COL_MODE		;Farb-Modus aktiv?
			bmi	:2			; => Nein, weiter...
			lda	fileColorTab,x		;Icon-Farbe aus Tabelle einlesen.
			bne	:3

;--- Icons in S/W, Debug aktiv?
::2			bit	GD_COL_DEBUG		;Debug-Modus aktiv?
			bpl	:icon_disk		; => Nein, weiter...

			ldy	#$01
			lda	(r15L),y		;Icon im Cache?
			bne	:icon_disk		; => Nein, weiter...

::icon_cache		lda	GD_COL_CACHE		;Debug-Modus: Farbe für
			jmp	:3			;"Icon im Cache" setzen.

::icon_disk		lda	GD_COL_DISK
::3			ora	C_WinBack		;Mit Hintergrundfarbe verknüpfen.

			pha
			jsr	GetFileIcon_r0		;Datei-Icon einlesen.
			pla
			sta	r3L			;Farbwert speichern.

			LoadB	r2L,$03			;Breite Icon in Cards.
			LoadB	r2H,$15			;Höhe Icon in Pixel.
;			LoadB	r3L,$01			;Farbe für Icon (Bereits gesetzt).
			LoadB	r3H,$04			;DeltyY in Cards für Ausgabe Name.
			LoadW	r4 ,FNameBuf		;Zeiger auf Dateiname.
			jsr	GD_FICON_NAME

			lda	#$ff			;Weitere Einträge in Zeile möglich.
			jmp	:invert_entry		;Ggf. Eintrag invertieren.

;--- Text-Ausgabe.
::text_mode		lda	r1H			;Y-Koordinate für
			clc				;Textausgabe berechnen.
			adc	#$06
			sta	r1H

			ldx	WM_WCODE
			lda	WMODE_VINFO,x		;Detail-Modus aktiv?
			bne	:info_mode		; => Ja, weiter...

			lda	r1L			;Platz in Zeile für einen
			clc				;weiteren Dateieintrag?
			adc	r3L
			cmp	r2L
			bcc	:print_entry		; => Ja, weiter...
			beq	:print_entry		; => Ja, weiter...
			ldx	#$00			; => Kein Eintrag ausgegeben.
			rts

::print_entry		lda	r1L			;X-Koordinate für Textausgabe
			pha				;von CARDs nach Pixel wandeln.
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			LoadW	r0,FNameBuf
			jsr	PutString		;Dateiname ausgeben.

			pla				;X-Koordinate zurücksetzen.
			sta	r1L

			lda	r1H			;Y-Koordinate zurücksetzen.
			sec
			sbc	#$06
			sta	r1H

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$ff			;Weitere Einträge in Zeile möglich.
			jmp	:invert_entry		;Ggf. Eintrag invertieren.

;--- Text-Ausgabe/Details.
::info_mode		lda	r2L
			pha

			lda	r1L			;X-Koordinate für Textausgabe
			pha				;von CARDs nach Pixel wandeln.
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			PushW	r11			;X-Koordinate zwischenspeichern.
			LoadW	r0,FNameBuf		;Dateiname ausgeben.
			jsr	PutString
			PopW	r11			;X-Koordinate zurücksetzen.

			jsr	DrawDetails		;Details zu Datei-Eintrag ausgeben.

			pla				;X-Koordinate zurücksetzen.
			sta	r1L

			lda	r1H			;Y-Koordinate zurücksetzen.
			sec
			sbc	#$06
			sta	r1H

			pla
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y		;Zeiger auf nächste Zeile.
			sta	r2H

			lda	#$7f			;Zeilenende erreicht.

;--- Aktueller Eintrag ausgewählt?
;    Wenn ja, dann Eintrag invertieren.
::invert_entry		pha

			ldy	#$00
			lda	(r15L),y		;Eintrag ausgewählt?
			beq	:not_selected		; => Nein, weiter...

			jsr	WM_CONVERT_CARDS	;Koordinaten nach Pixel wandeln.

::text_x_max		CmpW	r4,rightMargin		;Rechter Rand überschritten?
			bcc	:test_y_max		; => Nein, Weiter...
			MoveW	rightMargin,r4		;Fensterbegrenzung setzen.

::test_y_max		CmpB	r2H,windowBottom	;Unterer Rand überschritten?
			bcc	:do_invert		; => Nein, Weiter...
			MoveB	windowBottom,r2H	;Fensterbegrenzung setzen.

::do_invert		jsr	InvertRectangle		;Eintrag invertieren.

::not_selected		pla
			tax
			rts

;*** Dateiname kopieren.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
;    Rückgabe: FNameBuf = Dateiname.
:ReadFName		LoadW	r9,FNameBuf
			ldx	#r15L
			ldy	#r9L
			jmp	SysCopyFName

;*** Zeiger auf Datei-Icon setzen.
;    Übergabe: r0  = Eintrag-Nr.
;              r15 = Zeiger auf Verzeichnis-Eintrag.
;    Rückgabe: r0  = Zeiger auf Datei-Icon.
:GetFileIcon_r0		ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl aktiv?
			beq	:file_mode		; => Nein, weiter...

			ldy	#$02
			lda	(r15L),y
			sec
			sbc	#$01
			asl
			tay
			lda	:tab +0,y
			sta	r0L
			lda	:tab +1,y
			sta	r0H
			rts

::tab			w Icon_41_71
			w Icon_41_71
			w Icon_81_NM
			w Icon_81_NM
			w $0000
			w Icon_Map			;Verzeichnis bei SD2IEC.

::file_mode		PushB	r1L			;r1L/r1H enthält XPos/YPos.
			PushB	r1H			;Register r1L/r1H sichern.

			MoveW	r0,r14
			jsr	SET_POS_CACHE		;Zeiger auf Cache setzen.

			LoadW	r0,dataBufDir
			MoveW	r14,r1
			LoadW	r2,32
			MoveB	r12H,r3L		;Speicherbank.
			jsr	FetchRAM		;Cache-Eintrag einlesen.

			;bit	GD_ICON_PRELOAD		;Alle Icons in Cache laden?
			;bmi	:test_cache		; => Ja, weiter...
			lda	dataBufDir +1		;Icon bereits im Cache?
			bne	:load_from_disk		; => Nein, Icon von Disk laden.

;--- Verzeichnis-Eintrag und Icon aus Cache.
::test_cache		LoadW	r0,dataBufIcon
			MoveW	r13,r1
			LoadW	r2,64
			MoveB	r12L,r3L		;Speicherbank.
			jsr	FetchRAM		;Cache-Eintrag einlesen.

			lda	#<dataBufIcon		;Zeiger auf Datei-Icon in Puffer.
			ldx	#>dataBufIcon
			bne	:exit_set_entry

;--- Datei-Icon von Disk/Cache laden.
::load_from_disk	jsr	GetVecFileIcon		;Datei-Icon von Disk laden.

;--- Zeiger auf Datei-Icon setzen, Ende.
::exit_set_entry	sta	r0L			;Zeiger auf Datei-Icon
			stx	r0H			;speichern.

			PopB	r1H			;Register r1L/r1H zurücksetzen.
			PopB	r1L

			rts

;*** Datei-Icon von Disk/aus Cache einlesen.
;    Übergabe: r12L = Cache/Speicherbank Icon-Eintrag.
;              r12H = Cache/Speicherbank Verzeichnis-Eintrag.
;              r13  = Zeiger auf Cache/Icon-Eintrag
;              r14  = Zeiger auf Cache/Verzeichnis-Eintrag.
;              r15  = Zeiger auf Speicher/Verzeichnis-Eintrag.
;    Rückgabe: AKKU/XReg  = Zeiger auf Datei-Icon.
:GetVecFileIcon		ldy	#$02
			lda	(r15L),y		;Dateityp = Gelöscht?
			beq	:1			; => Ja, weiter...
			cmp	#$ff			;"More files..." ?
			bne	:2			; => Nein, weiter...

			lda	#<Icon_MoreFiles	;Icon "Weitere Dateien".
			ldx	#>Icon_MoreFiles
			rts

::1			lda	#<Icon_Deleted		;Icon "Gelöscht".
			ldx	#>Icon_Deleted
			rts

::2			and	#%00001111
			cmp	#$06			;Dateityp = Verzeichnis?
			bne	:4			; => Nein, weiter...
			lda	#<Icon_Map		;Icon "Verzeichnis".
			ldx	#>Icon_Map
			rts

::3			lda	#<Icon_CBM		;Icon "CBM".
			ldx	#>Icon_CBM
			rts

::4			ldy	#$15			;Spur/Sektor Infoblock einlesen.
			lda	(r15L),y		;Infoblock definiert?
			beq	:3			; => Nein, keine GEOS-Datei.
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader
			jsr	GetBlock		;Info-Block einlesen.
			txa				;Fehler?
			bne	:3			; => Ja, kein Infoblock => CBM.

			jsr	SaveIcon2Cache		;Icon in Cache speichern.

			lda	#<fileHeader +4		;Zeiger auf Icon in Infoblock.
			ldx	#>fileHeader +4
			rts

;*** Icon in fileHeader in Cache speichern.
;    Übergabe: r12H = Cache/Speicherbank.
;              r13  = Zeiger auf Cache/Icon-Eintrag
;              r14  = Zeiger auf Cache/Verzeichnis-Eintrag.
;              r15  = Zeiger auf Datei-Eintrag/Speicher.
:SaveIcon2Cache		bit	GD_ICON_PRELOAD		;PreLoad-Option aktiv?
			bmi	:1			; => Ja, Ende...
			bit	GD_ICON_CACHE		;Icon-Cache aktiv?
			bmi	:2			; => Ja, weiter...
::1			rts				;Ende, Icon bereits im Cache.

::2			LoadW	r0,fileHeader +4
			MoveW	r13,r1
			LoadW	r2,64
			MoveB	r12L,r3L
			jsr	StashRAM		;Datei-Icon in Cache speichern.

			ldy	#$01			;Kennung "Icon im Cache" in
			lda	#$00			;Verzeichnis-Eintrag setzen.
			sta	(r15L),y		;(Byte#1 = $00)

			MoveW	r15,r0			;Zeiger auf Verzeichnis-Eintrag.
			MoveW	r14,r1			;Verzeichnis-Eintrag im Cache.
			LoadW	r2,2			;Nur Byte #0/1 sichern.
			MoveB	r12H,r3L		;Speicherbank.
			jmp	StashRAM		;Verzeichnis-Eintrag sichern.

;*** Textausgabe/Details ausgeben.
:DrawDetails		AddVBW	$40,r11			;X-Koordinate für Details setzen.

			ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitions-Modus aktiv?
			beq	:file_mode		; => Nein, weiter...

;--- Partitionen/DiskImages.
			jsr	Detail_Size		;Partitionsgröße ausgeben.

			lda	#" "			;Abstandhalter ausgeben.
			jsr	SmallPutChar
			lda	#" "
			jsr	SmallPutChar

			lda	curType			;DiskImage-Typ ausgeben.
			and	#%0000 0111
			asl
			tay
			lda	:tab1 +0,y
			sta	r0L
			lda	:tab1 +1,y
			sta	r0H
			jmp	PutString

::tab1			w :ID0
			w :ID1
			w :ID2
			w :ID3
			w :ID4
			w :ID0
			w :ID0
			w :ID0

::ID0			b "?",NULL
::ID1			b "1541",NULL
::ID2			b "1571",NULL
::ID3			b "1581",NULL
::ID4			b "Native",NULL

;--- Standard-Datei-Modus.
::file_mode		ldy	#$02			;Anzahl Blocks einlesen.
			lda	(r15L),y
			cmp	#$ff			;Eintrag "Weitere Dateien"?
			beq	:exit			; => Ja, Ausgabe beenden.

			lda	#$00
::1			pha
			tax
			lda	:sort,x			;Detailtyp einlesen.
			asl				;Zeiger auf Routine zur Detail-
			tay				;Ausgabe berechnen.
			lda	:tab2 +0,y
			ldx	:tab2 +1,y
			jsr	CallRoutine		;Detail-Informationen ausgeben.

			AddVBW	6,r11			;Abstandhalter.

			pla
			clc
			adc	#$01			;Zeiger auf nächstes Details setzen.
			cmp	#$06			;Alle Details ausgegeben?
			bcc	:1			; => Nein, weiter...
::exit			rts

::sort			b $00,$01,$02,$03,$04,$05

::tab2			w $0000				;Name bereits ausgegeben.
			w Detail_Size			;Datei/Größe.
			w Detail_Date			;Datei/Datum.
			w Detail_Time			;Datei/Uhrzeit.
			w Detail_GType			;GEOS-Dateityp.
			w Detail_CType			;Commodore-Dateityp.

;*** Zweistellige Zahl ausgeben.
;    Übergabe: XReg = Zahl.
;              YReg = Zahlentrenner, z.B. "."(Datum) oder ":"(Zeit).
:Detail_Num		lda	r11H			;X-Koordinate sichern.
			pha
			lda	r11L
			pha

			tya				;Zahlentrenner sichern.
			pha

			txa
			jsr	DEZtoASCII		;Zahl nach ASCII wandeln.
			pha				;LOW-Nibble sichern.
			txa
			jsr	SmallPutChar		;10er ausgeben.
			pla
			jsr	SmallPutChar		;1er ausgeben.

			pla
			jsr	SmallPutChar		;Zahlentrenner ausgeben.

			pla				;X-Koordinate auf nächste
			clc				;Position setzen.
			adc	#11
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** Dateigröße ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
:Detail_Size		ldy	#$1e			;Anzahl Blocks einlesen.
			lda	(r15L),y
			sta	r0L
			iny
			lda	(r15L),y
			sta	r0H

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Anzeige in KBytes?
			beq	:1			; => Nein, weiter...

			lda	r0L
			pha
			ldx	#r0L
			ldy	#$02
			jsr	DShiftRight		;Blocks in KBytes umrechnen.
			pla
			and	#%00000011		;Auf volle KByte aufrunden?
			beq	:1			; => Bereits volle KByte, weiter...

			IncW	r0			;Anzahl KBytes +1.
							;Sonst 0-2 Blocks = 0Kbyte.

::1			lda	#$20 ! SET_RIGHTJUST ! SET_SUPRESS
			jsr	PutDecimal		;Breite für Größenausgabe.

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Anzeige in KBytes?
			beq	:2			; => Nein, weiter...
			lda	#"K"
			jsr	SmallPutChar		;"K"byte-Suffix ausgeben.

::2			rts

;*** Dateidatum ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
:Detail_Date		ldy	#$1b
			lda	(r15L),y		;Tag.
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$1a
			lda	(r15L),y		;Monat.
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$19
			lda	(r15L),y		;Jahr.
			tax
			ldy	#" "
			jmp	Detail_Num

;*** Dateizeit ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
:Detail_Time		ldy	#$1c
			lda	(r15L),y		;Stunde.
			tax
			ldy	#":"
			jsr	Detail_Num

			ldy	#$1d
			lda	(r15L),y		;Minute.
			tax
			ldy	#" "
			jmp	Detail_Num

;*** GEOS-Dateityp ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
:Detail_GType		lda	r11H			;X-Koordinate sichern.
			pha
			lda	r11L
			pha

			jsr	GetGeosType		;Zeiger auf Text für
			sta	r0L			;GEOS-Dateityp einlesen.
			sty	r0H
			jsr	PutString		;GEOS-Dateityp ausgeben.

			pla				;Spaltenbreite setzen.
			clc
			adc	#$50
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** CBM-Dateityp ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
:Detail_CType		lda	r11H			;X-Koordinate sichern.
			pha
			lda	r11L
			pha

			ldy	#$02
			lda	(r15L),y		;CBM-Dateityp einlesen.
			pha
			and	#%00000111		;Datei-Typ isolieren.
			asl
			asl
			tay
			lda	cbmFType+2,y		;Zeichen für CBM-Dateityp
			pha				;einlesen und ausgaben.
			lda	cbmFType+1,y
			pha
			lda	cbmFType+0,y
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla

			pha				;Datei "geöffnet" ?
			bmi	:3			; => Nein, weiter...
			lda	#"*"
			jsr	SmallPutChar		;"Datei geöffnet"-Kennung.

::3			pla
			and	#%01000000		;Datei schreibgeschützt?
			beq	:4			; => Nein, weiter...
			lda	#"<"
			jsr	SmallPutChar		;"Datei schreibgeschützt"-Kennung.

::4			pla				;Spaltenbreite setzen.
			clc
			adc	#$18
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** Variablen.
:FNameBuf		s 17				;Puffer für Dateiname.

;*** Farben für GEOS-Datei-Icons.
if FALSE
;    Hintergrund-Farb-Nibble immer $x0.
;
;    Hinweis: Standard-Farbtabelle.
;             Farbe nach Dateityp.
;
; $0x = Schwarz   $8x = Orange
; $1x = Weiß      $9x = Braun
; $2x = Rot       $Ax = Hellrot
; $3x = Türkis    $Bx = Dunkelgrau
; $4x = Violett   $Cx = Grau
; $5x = Grün      $Dx = Hellgrün
; $6x = Blau      $Ex = Hellblau
; $7x = Gelb      $Fx = Hellgrau
;
:fileColorTab		b $00				;$00-Nicht GEOS.
			b $00				;$01-BASIC-Programm.
			b $30				;$02-Assembler-Programm.
			b $30				;$03-Datenfile.
			b $20				;$04-Systemdatei.
			b $60				;$05-Hilfsprogramm.
			b $60				;$06-Anwendung.
			b $30				;$07-Dokument.
			b $70				;$08-Zeichensatz.
			b $50				;$09-Druckertreiber.
			b $50				;$0A-Eingabetreiber.
			b $20				;$0B-Laufwerkstreiber.
			b $20				;$0C-Startprogramm.
			b $00				;$0D-Temporäre Datei (SWAP FILE).
			b $60				;$0E-Selbstausführend (AUTO_EXEC).
			b $50				;$0F-Eingabetreiber C128.
			b $70				;$10-Unbekannt.
			b $60				;$11-gateWay-Dokument.
			b $70				;$12-Unbekannt.
			b $70				;$13-Unbekannt.
			b $70				;$14-Unbekannt.
			b $60				;$15-geoShell-Befehl.
			b $50				;$16-geoFax-Dokument.
			b $70				;$17-Unbekannt.
			b $b0				;$18-Verzeichnis.
endif

;*** Farben für GEOS-Datei-Icons.
if TRUE
;    Hintergrund-Farb-Nibble immer $x0.
;
;    Hinweis: Überarbeitete Farbtabelle.
;             Farbe nach Sytemtyp.
;
; Nicht-GEOS      $0x
; Anwendungen     $6x
; Dokumente       $5x
; System          $2x
; Zeichensatz     $Dx
; Treiber         $4x
; Sonstiges       $Cx
; Verzeichnisse   $Bx
;
; $0x = Schwarz   $8x = Orange
; $1x = Weiß      $9x = Braun
; $2x = Rot       $Ax = Hellrot
; $3x = Türkis    $Bx = Dunkelgrau
; $4x = Violett   $Cx = Grau
; $5x = Grün      $Dx = Hellgrün
; $6x = Blau      $Ex = Hellblau
; $7x = Gelb      $Fx = Hellgrau
;
:fileColorTab		b $00				;$00-Nicht GEOS.
			b $60				;$01-BASIC-Programm.
			b $60				;$02-Assembler-Programm.
			b $c0				;$03-Datenfile.
			b $20				;$04-Systemdatei.
			b $60				;$05-Hilfsprogramm.
			b $60				;$06-Anwendung.
			b $50				;$07-Dokument.
			b $d0				;$08-Zeichensatz.
			b $40				;$09-Druckertreiber.
			b $40				;$0A-Eingabetreiber.
			b $40				;$0B-Laufwerkstreiber.
			b $20				;$0C-Startprogramm.
			b $c0				;$0D-Temporäre Datei (SWAP FILE).
			b $60				;$0E-Selbstausführend (AUTO_EXEC).
			b $40				;$0F-Eingabetreiber C128.
			b $c0				;$10-Unbekannt.
			b $40				;$11-gateWay-Dokument.
			b $c0				;$12-Unbekannt.
			b $c0				;$13-Unbekannt.
			b $c0				;$14-Unbekannt.
			b $40				;$15-geoShell-Befehl.
			b $50				;$16-geoFax-Dokument.
			b $c0				;$17-Unbekannt.
			b $b0				;$18-Verzeichnis.
endif
