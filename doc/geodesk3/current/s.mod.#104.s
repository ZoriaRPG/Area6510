﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* AppLink-Daten laden.
;* AppLink-Daten speichern.
;* AppLink umbenennen.

if .p
			t "TopSym"
			t "TopSym.MP3"
			t "TopSym.GD"
			t "TopMac.GD"
			t "s.mod.#101.ext"
			t "-SYS_APPLINK"
endif

			n "mod.#104.obj"
			t "-SYS_CLASS.h"
			f DATA
			o VLIR_BASE
			p MainInit
			a "Markus Kanet"

:VlirJumpTable		jmp	xLNK_LOAD_DATA
			jmp	xLNK_SAVE_DATA
			jmp	xLNK_RENAME

;*** Programmroutinen.
			t "-104_AppLink"

;******************************************************************************
			g BASE_DIR_DATA
;******************************************************************************
