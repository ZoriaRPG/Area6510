﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
; Funktion		: Scrollbalken, aus GeoDOS importiert.
; Datum			: 02.07.97
; Aufruf		: JSR  InitBalken
; Übergabe		: r0 = Zeiger auf Datentabelle.
;			  b    Zeiger auf xPos (in CARDS!)
;			  b    Zeiger auf yPos (in PIXEL!)
;			  b    max. Länge des Balken (in PIXEL!)
;			  w    max. Anzahl Einträge in Tabelle.
;			  w    max. Einträge auf einer Seite.
;			  w    Tabellenzeiger = Nr. der ersten Datei auf der Seite!
;
;'InitBalken'		Muß als erstes aufgerufen werden um die Daten (r0-r2) für
;			den Anzeigebalken zu definieren und den Balken auf dem
;			Bildschirm auszugeben.
;'WriteSB_Data'		Kopiert die Daten aus der Datentabelle in die interne
;			Datentabelle.
;'SetPosBalken'		Setzt den Füllbalken auf neue Position. Dazu muß im AKKU die
;			neue Position des Tabellenzeigers übergeben werden.
;'PrintBalken'		Zeichnet den Anzeige- und Füllbalken erneut. Dazu muß aber
;			vorher mindestens 1x 'InitBalken' aufgerufen worden sein!
;'ReadSB_Data'		Übergibt folgende Werte an die aufrufende Routine:
;			r0L = SB_XPos      Byte  X-Position Balken in CARDS.
;			r0H = SB_YPos      Byte  Y-Position in Pixel.
;			r1L = SB_MaxYlen   Byte  Länge des Balkens.
;			r1H = SB_MaxEntry  Byte  Anzahl Einträge in Tabelle.
;			r2L = SB_MaxEScr   Byte  Anzahl Einträge auf Seite.
;			r2H = SB_PosEntry  Byte  Aktuelle Position in Tabelle.
;			r3  = SB_PosTop    Word  Startadresse im Grafikspeicher.
;			r4L = SB_Top       Byte  Oberkante Füllbalken.
;			r4H = SB_End       Byte  Unterkante Füllbalken.
;			r5L = SB_Length    Byte  Länge Füllbalken.
;'IsMseOnPos'		Mausklick auf Anzeigebalken auswerten. Ergebnis im AKKU:
;			$01 = Mausklick Oberhalb Füllbalken.
;			$02 = Mausklick auf Füllbalken.
;			$03 = Mausklick Unterhalb Füllbalken.
;'StopMouseMove'	Schränkt Mausbewegung ein.
;'SetRelMouse'		Setzt neue Mausposition. Wird beim Verschieben des
;			Füllbalkens benötigt. Vorher muß ein 'JSR SetPosBalken'
;			erfolgen!
;******************************************************************************

;*** Balken initialiseren.
:InitBalken		ldy	#$08			;Paraeter speichern.
::1			lda	(r0L),y
			sta	SB_XPos,y
			dey
			bpl	:1

			jsr	Anzeige_Ypos		;Position Anzeigebalken berechnen.
			jsr	Balken_Ymax		;Länge des Füllbalkens anzeigen.
			jmp	Balken_Ypos		;Y-Position Füllbalken berechnen.

;*** Neue Balkenposition defnieren und anzeigen.
:SetPosBalken		sta	SB_PosEntry +0		;Neue Position Füllbalken setzen.
			stx	SB_PosEntry +1

;*** Balken ausgeben.
:PrintBalken		jsr	Balken_Ypos		;Y-Position Füllbalken berechnen.
			lda	SB_MaxYlen
			sec
			sbc	SB_Top
			bcc	:1
			cmp	SB_Length
			bcs	PrintCurBalken

::1			lda	SB_MaxYlen
			sec
			sbc	SB_Length
			sta	SB_Top

:PrintCurBalken		MoveW	SB_PosTop,r0		;Grafikposition berechnen.

			ClrB	r1L			;Zähler für Balkenlänge löschen.
			lda	SB_YPos			;Zeiger innerhalb Grafik-CARD be-
			and	#%00000111		;rechnen (Wert von $00-$07).
			tay

::1			lda	#%01010101
			sta	r1H
			lda	r1L
			lsr
			bcc	:1a
			asl	r1H

::1a			lda	SB_Length		;Balkenlänge = $00 ?
			beq	:4			;Ja, kein Füllbalken anzeigen.

			ldx	r1L
			cpx	SB_Top			;Anfang Füllbalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.
			bcc	:4			;Kleiner, dann Hintergrund ausgeben.
			cpx	SB_End			;Ende Füllbalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.
			bcs	:4			;Größer, dann Hintergrund ausgeben.
			inx
			cpx	SB_MaxYlen		;Ende Anzeigebalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.

::2			lda	r1H
			and	#%10000001
			ora	#%01100110		;Wert für Füllbalken.
			bne	:5

::3			lda	r1H
			ora	#%01111110
			bne	:5

::4			lda	r1H
::5			sta	(r0L),y			;Byte in Grafikspeicher schreiben.
			inc	r1L
			CmpB	r1L,SB_MaxYlen		;Gesamte Balkenlänge ausgegeben ?
			beq	:6			;Ja, Abbruch...

			iny
			cpy	#8			;8 Byte in einem CARD gespeichert ?
			bne	:1			;Nein, weiter...

			AddVW	320,r0			;Zeiger auf nächstes CARD berechnen.
			ldy	#$00
			beq	:1			;Schleife...
::6			rts				;Ende.

;*** Position des Anzeigebalken berechnen.
:Anzeige_Ypos		MoveB	SB_XPos,r0L		;Zeiger auf X-CARD berechnen.
			LoadB	r0H,NULL
			ldx	#r0L
			ldy	#$03
			jsr	DShiftLeft
			AddVW	SCREEN_BASE,r0		;Zeiger auf Grafikspeicher.

			lda	SB_YPos			;Zeiger auf Y-Position
			lsr				;berechnen.
			lsr
			lsr
			tay
			beq	:2
::1			AddVW	40*8,r0
			dey
			bne	:1
::2			MoveW	r0,SB_PosTop		;Grafikspeicher-Adresse merken.
			rts

;*** Länge des Balken berechnen.
:Balken_Ymax		CmpW	SB_MaxEScr,SB_MaxEntry
			bcc	:0			;Balken möglich ?

			lda	#$00			;Nein, weiter...
			beq	:1

::0			MoveB	SB_MaxYlen,r0		;Länge Balken berechnen.
			LoadB	r0H,NULL
			MoveW	SB_MaxEScr,r1
			jsr	Mult_r0r1

			MoveW	SB_MaxEntry,r1
			jsr	Div_r0r1

			CmpBI	r0L,8			;Balken kleiner 8 Pixel ?
			bcs	:1			;Nein, weiter...
			lda	#$08			;Mindestgröße für Balken.
::1			sta	SB_Length
			rts

;*** Position des Balken berechnen.
:Balken_Ypos		ldx	#NULL
			ldy	SB_Length
			CmpW	SB_MaxEScr,SB_MaxEntry
			bcs	:1

			MoveW	SB_PosEntry,r0
			lda	SB_MaxYlen
			sec
			sbc	SB_Length
			sta	r1L
			LoadB	r1H,NULL
			jsr	Mult_r0r1

			MoveW	SB_MaxEntry,r1
			SubW	SB_MaxEScr ,r1
			jsr	Div_r0r1

			lda	r0L
			tax
			clc
			adc	SB_Length
			tay
::1			stx	SB_Top
			dey
			sty	SB_End
			rts

:Mult_r0r1		ldx	#r0L			;Multiplikation durchführen.
			ldy	#r1L
			jmp	DMult

:Div_r0r1		ldx	#r0L
			ldy	#r1L
			jmp	Ddiv

;*** Balken initialiseren.
:ReadSB_Data		ldx	#$0d
::1			lda	SB_XPos,x
			sta	r0L,x
			dex
			bpl	:1
			rts

;*** Mausklick überprüfen.
:IsMseOnPos		lda	mouseYPos
			sec
			sbc	SB_YPos
			cmp	SB_Top
			bcc	:3
::1			cmp	SB_End
			bcc	:2
			lda	#$03
			b $2c
::2			lda	#$02
			b $2c
::3			lda	#$01
			rts

;*** Mausbewegung kontrollieren.
:StopMouseMove		lda	mouseXPos +0
			sta	mouseLeft +0
			sta	mouseRight+0
			lda	mouseXPos +1
			sta	mouseLeft +1
			sta	mouseRight+1
			lda	mouseYPos
			jmp	SetNewRelMse

:SetRelMouse		lda	#$ff
			clc
			adc	SB_Top

:SetNewRelMse		sta	mouseTop
			sta	mouseBottom
			sec
			sbc	SB_Top
			sta	SetRelMouse+1
			rts

;*** Variablen.
:SB_XPos		b $00				;r0L
:SB_YPos		b $00				;r0H
:SB_MaxYlen		b $00				;r1L
:SB_MaxEntry		w $00				;r1H
:SB_MaxEScr		w $00				;r2L
:SB_PosEntry		w $00				;r2H

:SB_PosTop		w $0000				;r3
:SB_Top			b $00				;r4L
:SB_End			b $00				;r4H
:SB_Length		b $00				;r5L
