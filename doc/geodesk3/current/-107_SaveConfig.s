﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Konfiguration speichern.
:xSAVE_CONFIG		jsr	OpenBootDrive		;Boot-Laufwerk öffnen.
			txa				;Diskettenfehler ?
			bne	:error_sys		; => Ja, Abbrucb...

			lda	#APPLICATION		;GeoDesk suchen.
			sta	r7L
			lda	#$01
			sta	r7H
			LoadW	r6,GD_SYS_NAME
			LoadW	r10,GD_CLASS
			jsr	FindFTypes
			txa				;Diskettenfehler ?
			bne	:error_sys		; => Ja, Abbruch...

			lda	r7H			;Modul gefunden ?
			bne	:error_sys		; => Nein, Abbruch...

::open			LoadW	r0,GD_SYS_NAME		;VLIR-Datei öffnen.
			jsr	OpenRecordFile
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...

			lda	#$00			;Zeiger auf Boot-Modul.
			jsr	PointRecord
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...

			LoadW	r2,$1000
			LoadW	r7,VLIR_BOOT_START
			jsr	ReadRecord		;Boot-Modul einlesen.
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...

			jsr	i_MoveData		;Konfiguration speichern.
			w	GD_VAR_START
			w	VLIR_BOOT_START +SYSVAR_SIZE
			w	GD_VAR_SIZE

			lda	r7L			;Größe Boot-VLIR-Modul ermitteln.
			sec
			sbc	#<VLIR_BOOT_START
			sta	r2L
			lda	r7H
			sbc	#>VLIR_BOOT_START
			sta	r2H

			LoadW	r7,VLIR_BOOT_START
			jsr	WriteRecord		;Boot-Modul speichern.
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...
			jsr	CloseRecordFile
			txa				;Diskettenfehler?
			beq	:exit			; => Nein, Ende...

;--- Fehler beim speichern.
::vlir_error		lda	#<Dlg_SaveError
			ldx	#>Dlg_SaveError
			bne	:errdlg

;--- GeoDosk nicht gefunden.
::error_sys		lda	#<Dlg_GeoDeskNFnd
			ldx	#>Dlg_GeoDeskNFnd
			;bne	:errdlg

;--- Fehler ausgeben.
::errdlg		sta	r0L			;Zeiger auf DialogBox-Daten setzen.
			stx	r0H
			jsr	DoDlgBox		;DialogBox aufrufen.
::exit			jmp	MNU_RESTART		;Menü/FensterManager neu starten.

;*** Fehler: GeoDesk nicht gefunden.
:Dlg_GeoDeskNFnd	b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$2a
			w :2
			b DBTXTSTR   ,$0c,$3a
			w :3
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "GeoDesk konnte die System-",NULL
::2			b "Datei nicht finden!",NULL
::3			b "Konfiguration nicht gespeichert!",NULL

;*** Fehler: Fehler beim speichern.
:Dlg_SaveError		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$2a
			w :2
			b DBTXTSTR   ,$0c,$3a
			w :3
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "Disk-Fehler beim speichern",NULL
::2			b "der GeoDesk-Konfiguration!",NULL
::3			b "Konfiguration nicht gespeichert!",NULL
