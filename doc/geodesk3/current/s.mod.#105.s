﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* Datei öffnen.
;* Editor öffnen.
;* Drucker wechseln.
;* Eingabegerät wechseln.
;* Dialog: Fehler Drucker-Installation.
;* Dialog: Drucker-Installation OK.
;* Anwendung wählen.
;* AutoExec wählen.
;* Dokumente wählen.
;* geoWrite-Dokument wählen.
;* geoPaint-Dokument wählen.
;* Hintergrundbild wählen.
;* Nach GEOS beenden.
;* Nach BASIC beenden.
;* BASIC-Programm starten.

if .p
			t "TopSym"
			t "TopSym.MP3"
			t "TopSym.GD"
			t "TopMac.GD"
			t "s.mod.#101.ext"
endif

			n "mod.#105.obj"
			t "-SYS_CLASS.h"
			f DATA
			o VLIR_BASE
			p MainInit
			a "Markus Kanet"

:VlirJumpTable		jmp	StartFile_a0
			jmp	OpenEditor
			jmp	ChangePrinter
			jmp	ChangeInput
			jmp	OpenPrntError
			jmp	OpenPrntOK
			jmp	SelectAppl
			jmp	SelectAuto
			jmp	SelectDocument
			jmp	SelectDocWrite
			jmp	SelectDocPaint
			jmp	SelectBackScrn
			jmp	ExitGEOS
			jmp	ExitBASIC
			jmp	ExitBAppl

;*** Speicherverwaltung.
			t "-SYS_RAM_FREE"
			t "-SYS_RAM_SHARED"

;*** Programmroutinen.
			t "-105_OpenFile"
			t "-105_OpenEditor"
			t "-105_OpenDevice"
			t "-105_ExitBASIC"
			t "-105_BackScreen"

;******************************************************************************
			g BASE_DIR_DATA
;******************************************************************************
