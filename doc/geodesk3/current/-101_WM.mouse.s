﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   WM_CHK_MOUSE
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mausklick auswerten.
;Hinweis:   Routine wird innerhalb der MainLoop ausgeführt!
;******************************************************************************
:WM_CHK_MOUSE		lda	mouseData		;Mausbutton gedrückt ?
			bmi	:1			; => Nein, Ende...

			jsr	WM_FIND_WINDOW		;Fenster suchen.
			txa				;Wurde Fenster gefunden ?
			beq	:2			; => Ja, weiter...

			lda	mouseOldVec +0		;Mausabfrage extern fortsetzen.
			ldx	mouseOldVec +1
			jmp	CallRoutine
::1			rts

;--- Mausklick auf Fenster.
;ACHTUNG! YReg enthält Fenster-Nr.!
::2			sta	WM_TITEL_STATUS		;Rechtsklick/Titel löschen.

			jsr	WM_GET_MSE_STATE	;Tastenstatus abfragen.

			lda	WM_STACK		;Nr. des obersten Fensters einlesen
			sta	mouseCurTopWin		;und zwisichenspeichern.

;--- Fenster-Daten einlesen.
			lda	WM_STACK,y		;Neue Fenster-Nr. einlesen und
			sta	WM_WCODE		;Fensterdaten kopieren.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_WCODE		;Desktop ?
			beq	:3			; => Ja, keine Fenster-Icons.

			jsr	WM_MOVE_WIN_UP		;Fenster nach oben sortieren.

			jsr	WM_DEF_MOVER_DAT	;Daten für Scrollbalken definieren.
;			jsr	WriteSB_Data
			jsr	InitBalken		;Scrollbalken initialisieren.

;--- Klick innerhalb Fenster ?
			php
			sei				;Interrupt sperren.

			jsr	WM_GET_SLCT_SIZE	;Fenstergröße ermitteln.

			AddVBW	8,r3			;Titelzeile, Statuszeile und
			SubVW	8,r4			;Rahmen links/rechts von der
			AddVB	8,r2L			;Fenstergröße abziehen.
			SubVB	8,r2H
			jsr	IsMseInRegion		;Klick auf Fensterinhalt prüfen.

			plp				;IRQ-Status zurücksetzen.

			tax				;Mausklick innerhalb Dateifenster ?
			beq	:no_win_slct		; => Nein, weiter...

::3			jmp	WM_WIN_SLCT		; => Ja, Fenster angeklickt.

;--- Klick auf Fenster-Icons ?
::no_win_slct		lda	mseClkState
			cmp	#254			;Rechter Mausklick ?
			bne	:4			; => Nein, weiter...

			lda	mouseYPos
			cmp	r2L			;Mausklick auf Titelzeile?
			bcs	:4			; => Nein, weiter...

::wait_no_msekey	lda	mouseData		;Maustaste gedrückt?
			bpl	:wait_no_msekey		; => Ja, warten...

			dec	WM_TITEL_STATUS		;Rechtsklick/Titel löschen.
			jmp	WM_WIN_SLCT		; => Ja, Fenster angeklickt.

::4			jsr	WM_MOVE_WIN_UP		;Fenster nach oben sortieren.

;--- System-Icons Teil#1 testen.
;Falls angelickt muss hier anschließend
;die Routine ":WM_CALL_GETFILES"
;aufgerufen werden. Damit wird hier
;sichergestellt das die richtigen
;Dateien im Speicher liegen.
::test_winfunc1		LoadW	r14,:tab1
			LoadB	r15H,12
			jsr	WM_FIND_JOBS		;System-Icons auswerten.
			txa				;Wurde Fenster-Icon gewählt ?
			bne	:test_winfunc2		; => Nein, weiter...
			jmp	WM_CALL_GETFILES	;Routine "Dateien laden" aufrufen.

;--- System-Icons Teil#1 testen.
;Hier muss ":WM_CALL_GETFILES" nicht
;aufgerufen werden da die ScrollBar-
;Routinen automatisch GetFiles
;verwenden.
::test_winfunc2		LoadW	r14,:tab2
			LoadB	r15H,3
			jmp	WM_FIND_JOBS		;ScrollBar-Icons auswerten.

;*** Funktionen für Fenster-Icons.
::tab1			w WM_DEF_AREA_CL		;Fenster schließen.
			w WM_FUNC_CLOSE

			w WM_DEF_AREA_DN		;Fenster nach unten.
			w WM_FUNC_DOWN

			w WM_DEF_AREA_STD		;Fenster auf Standard-Größe.
			w WM_FUNC_STD

			w WM_DEF_AREA_MN		;Fenstergröße zurücksetzen.
			w WM_FUNC_MIN

			w WM_DEF_AREA_MX		;Fenster maximieren.
			w WM_FUNC_MAX

			w WM_DEF_AREA_UL		;Fenster links/oben vergrößern.
			w WM_FUNC_SIZE_UL

			w WM_DEF_AREA_UR		;Fenster rechts/oben vergrößern.
			w WM_FUNC_SIZE_UR

			w WM_DEF_AREA_DL		;Fenster links/unten vergrößern.
			w WM_FUNC_SIZE_DL

			w WM_DEF_AREA_DR		;Fenster rechts/unten vergrößern.
			w WM_FUNC_SIZE_DR

			w WM_DEF_AREA_MV		;Fenster über Titelzeile schieben.
			w WM_FUNC_SIZE_MV

;*** Funktionen für Scrollbalken.
::tab2			w WM_DEF_AREA_WUP		;Nach oben scrollen.
			w WM_FUNC_MOVE_UP

			w WM_DEF_AREA_WDN		;Nach unten scrollen.
			w WM_FUNC_MOVE_DN

			w WM_DEF_AREA_BAR		;Fensterbalken schieben.
			w WM_FUNC_MOVER

;*** Variablen.
:mouseCurTopWin		b $00

;******************************************************************************
;Routine:   WM_GET_MSE_STATE
;Parameter: -
;Rückgabe:  AKKU = 254 -> Rechte Maustaste.
;Verändert: A
;Funktion:  Mausklick auswerten.
;******************************************************************************
.WM_GET_MSE_STATE	php
			sei
			lda	CPU_DATA
			pha
			lda	#$35
			sta	CPU_DATA
			lda	$dc01			;Tasten-Status abfragen und
			sta	mseClkState		;speichern.
			pla
			sta	CPU_DATA
			lda	mseClkState
			plp
			rts

:mseClkState		b $00

;******************************************************************************
;Routine:   WM_WIN_SLCT
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mausklick auswerten.
;Hinweis:   Fenster angeklickt.
;           Linke Maustaste, Rechte Maustaste oder
;           Linke Maustaste+CBM testen.
;******************************************************************************
:WM_WIN_SLCT		lda	mseClkState		;Tasten-Status abfragen.
			cmp	#207			;CBM + Rechter Mausklick ?
			beq	:select_single		; => Ja, Einzel-Datei wählen.
			cmp	#253			;Linker Mausklick ?
			beq	:select_multi		; => Ja, Mehrfach-Dateiauswahl.
::skip_select		cmp	#254			;Rechter Mausklick ?
			bne	:test_desktop		; => Nein, weiter...
			jsr	WM_CALL_GETFILES	;Dateien einlesen.
			jmp	WM_CALL_RIGHTCLK	;Mausklick auswerten.

;--- Klick auf DeskTop ?
::test_desktop		lda	WM_WCODE		;Klick auf DeskTop?
			bne	:no_desktop		; => Nein, weiter...
			jmp	WM_CALL_EXEC		;DeskTop-Klick auswerten.

;--- Wechsel zu anderem Fenster.
::no_desktop		jsr	WM_SWITCH_TOPWIN	;Fenster nach oben holen und die
			jsr	WM_CALL_GETFILES	;Dateien für Fenster einlesen.

			jsr	WM_TEST_ENTRY		;Datei-Icon ausgewählt ?
			bcc	:no_icon_slct		; => Nein, weiter...
			jmp	WM_CALL_EXEC		;Datei-Klick auswerten.

::no_icon_slct		jsr	WM_TEST_MOVE		;Datei verschieben?
			bcs	:select_multi		; => Nein, weiter...
			jmp	WM_CALL_GETFILES	;Dateien einlesen.

::select_multi		ldx	WM_WCODE
			ldy	WIN_DATAMODE,x		;Partitionen oder DiskImages?
			bne	:exit			; => Ja, Keine Auswahl möglich.
			jmp	WM_CALL_MSLCT		; => Mehrfach-Auswahl.

::select_single		ldx	WM_WCODE
			ldy	WIN_DATAMODE,x		;Partitionen oder DiskImages?
			bne	:exit			; => Ja, Keine Auswahl möglich.
			jmp	WM_CALL_SSLCT		; => Einzel-Auswahl.
::exit			rts

;******************************************************************************
;Routine:   WM_SWITCH_TOPWIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster nach oben holen.
;******************************************************************************
:WM_SWITCH_TOPWIN	lda	WM_WCODE		;Ziel-Fenster = oberstes Fenster?
			cmp	mouseCurTopWin
			bne	:reload			; => Nein, Fenster nach oben.
			rts

::reload		jsr	WM_WIN2TOP		;Fenster nach oben sortieren.

			jsr	WM_LOAD_SCREEN		;Fenster aus ScreenBuffer laden.

;*** Fenster nach oben sortieren.
:WM_MOVE_WIN_UP		lda	WM_WCODE		;Ist Ziel-Fenster bereits
			cmp	WM_STACK		;das oberste Fenster?
			beq	:exit			; => Ja, Ende...

			jsr	WM_WIN_BLOCKED		;Ist Ziel-Fenster durch andere
			txa				;Fenster verdeckt ?
			pha
			lda	WM_WCODE
			jsr	WM_WIN2TOP		;Fenster umsortieren.
			pla				;Ist Fenster verdeckt ?
			beq	:exit			; => Nein, weiter...

			jsr	WM_LOAD_SCREEN		;Fenster aus ScreenBuffer laden.

::exit			rts

;******************************************************************************
;Routine:   WM_FIND_JOBS
;Parameter: r14  = Zeiger auf Jobtabelle.
;           r15H = Anzahl Jobs.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Sucht Job für ausgewähltes System-Icon.
;******************************************************************************
:WM_FIND_JOBS		php
			sei

			lda	#$00			;Job-Zähler zurücksetzen.
			sta	r15L

::1			lda	r15L
			cmp	r15H			;Alle Jobs durchsucht?
			beq	:4			; => Ja, Abbruch...

			asl				;Daten für Icon einlesen.
			asl
			tay
			ldx	#$00
::2			lda	(r14L),y
			sta	r0L   ,x
			iny
			inx
			cpx	#$04
			bcc	:2

			lda	r0L			;Routine zum setzen der
			ldx	r0H			;Icon-Position/Größe aufrufen.
			jsr	CallRoutine
			jsr	IsMseInRegion
			tax				;Ist Mauszeiger auf Icon?
			beq	:3			; => Nein, weiter...

			plp				;IRQ-Status zurücksetzen.

			lda	r1L			;Job für Icon aufrufen.
			ldx	r1H
			jmp	CallRoutine

::3			inc	r15L			;Zähler auf nächstes Icon.
			jmp	:1

::4			ldx	#JOB_NOT_FOUND		;Fehler, Job nicht gefunden.

::5			plp				;IRQ-Status zurücksetzen.
			rts

;******************************************************************************
;Routine:   WM_FUNC_CLOSE
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgewähltes Fenster schließen.
;******************************************************************************
:WM_FUNC_CLOSE		lda	WM_WCODE
			jsr	WM_CLOSE_WINDOW
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_FUNC_MAX
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgewähltes Fenster maximieren.
;******************************************************************************
:WM_FUNC_MAX		ldx	WM_WCODE
			lda	WMODE_MAXIMIZED,x
			bne	:1
			dec	WMODE_MAXIMIZED,x
			jsr	WM_UPDATE		;Obersetes Fenster neu zeichnen.
::1			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_FUNC_MIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgewähltes Fenster zurücksetzen.
;******************************************************************************
:WM_FUNC_MIN		ldx	WM_WCODE
			lda	WMODE_MAXIMIZED,x
			beq	:1
			inc	WMODE_MAXIMIZED,x
			jsr	WM_UPDATE		;Obersetes Fenster neu zeichnen.
::1			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_FUNC_STD
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgewähltes Fenster auf Standardgröße zurücksetzen.
;******************************************************************************
:WM_FUNC_STD		ldx	WM_WCODE		;"Maximiert"-Flag löschen.
			lda	#$00
			sta	WMODE_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.
			jsr	WM_DEF_STD_WSIZE	;Standardgröße setzen.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.
			jsr	WM_UPDATE		;Obersetes Fenster neu zeichnen.
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_FUNC_DOWN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgewähltes Fenster nach unten verschieben.
;******************************************************************************
:WM_FUNC_DOWN		lda	WM_WCOUNT_OPEN
			cmp	#$03			;Nur 1 Fenster + Desktop?
			bcc	:4			; => Ja, Ende...

			ldx	#$00			;Fenster im Stack nach
			ldy	#$00			;unten verschieben.
::1			lda	WM_STACK ,x
			beq	:3
			cmp	WM_WCODE
			beq	:2
			sta	WM_STACK ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1

::3			lda	WM_WCODE		;Fenster-Nr. an letzte Stelle
			sta	WM_STACK ,y		;im Stack schreiben.

			jsr	WM_DRAW_ALL_WIN		;Alle Fenster aus ScreenBuffer
							;neu darstellen.

::4			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_FUNC_SIZE_...
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Größe für ausgewähltes Fenster ändern.
;******************************************************************************
:WM_FUNC_SIZE_UL	lda	#<WM_FJOB_SIZE_UL
			ldx	#>WM_FJOB_SIZE_UL
			bne	WM_FUNC_RESIZE

:WM_FUNC_SIZE_UR	lda	#<WM_FJOB_SIZE_UR
			ldx	#>WM_FJOB_SIZE_UR
			bne	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DL	lda	#<WM_FJOB_SIZE_DL
			ldx	#>WM_FJOB_SIZE_DL
			bne	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DR	lda	#<WM_FJOB_SIZE_DR
			ldx	#>WM_FJOB_SIZE_DR

;*** Fenstergröße ändern.
:WM_FUNC_RESIZE		jsr	WM_EDIT_WIN		;Gummi-Band erzeugen.

			ldx	WM_WCODE		;"Maximiert"-Flag löschen.
			lda	#$00
			sta	WMODE_MAXIMIZED,x

			jsr	WM_SET_WIN_SIZE		;Neue Fenstergröße setzen.

			jmp	WM_UPDATE		;Aktuelles Fenster neu zeichnen.

;******************************************************************************
;Routine:   WM_FJOB_SIZE_...
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Gummi-Band vergrößern oder verkleinern.
;******************************************************************************
:WM_FJOB_SIZE_UL	jsr	WM_FJOB_TEST_Y0		;Nach oben...
			jsr	WM_FJOB_TEST_X0		;Nach links...

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2L
			rts

:WM_FJOB_SIZE_UR	jsr	WM_FJOB_TEST_Y0		;Nach oben...
			jsr	WM_FJOB_TEST_X1		;Nach rechts...

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2L
			rts

:WM_FJOB_SIZE_DL	jsr	WM_FJOB_TEST_Y1		;Nach unten...
			jsr	WM_FJOB_TEST_X0		;Nach links...

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2H
			rts

:WM_FJOB_SIZE_DR	jsr	WM_FJOB_TEST_Y1		;Nach unten...
			jsr	WM_FJOB_TEST_X1		;Nach rechts...

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2H
			rts

;******************************************************************************
;Routine:   WM_FJOB_TEST_...
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Prüfen ob Größenänderung möglich.
;           Falls nein, Maus-Position korrigieren.
;******************************************************************************
:WM_FJOB_TEST_Y0	lda	r2H
			sec
			sbc	mouseYPos
			cmp	#MIN_SIZE_WIN_Y 		;Nach oben möglich?
			bcs	:1			; => Ja, weiter...
			MoveB	r2L,mouseYPos		;Mausposition zurücksetzen.
::1			rts

:WM_FJOB_TEST_Y1	lda	mouseYPos
			sec
			sbc	r2L
			cmp	#MIN_SIZE_WIN_Y 		;Nach unten möglich?
			bcs	:1			; => Ja, weiter...
			MoveB	r2H,mouseYPos		;Mausposition zurücksetzen.
::1			rts

:WM_FJOB_TEST_X0	lda	r4L
			sec
			sbc	mouseXPos +0
			tax
			lda	r4H
			sbc	mouseXPos +1
			bne	:1
			cpx	#MIN_SIZE_WIN_X 		;Nach links möglich?
			bcs	:1			; => Ja, weiter...
			MoveW	r3 ,mouseXPos		;Mausposition zurücksetzen.
::1			rts

:WM_FJOB_TEST_X1	lda	mouseXPos +0
			sec
			sbc	r3L
			tax
			lda	mouseXPos +1
			sbc	r3H
			bne	:1
			cpx	#MIN_SIZE_WIN_X 		;Nach rechts möglich?
			bcs	:1			; => Ja, weiter...
			MoveW	r4 ,mouseXPos		;Mausposition zurücksetzen.
::1			rts

;******************************************************************************
;Routine:   WM_FUNC_SIZE_MV
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster verschieben.
;******************************************************************************
:WM_FUNC_SIZE_MV	jsr	WM_GET_SLCT_SIZE

			MoveB	r2L,mouseYPos
			MoveW	r3 ,mouseXPos

			lda	r4L
			sec
			sbc	r3L
			sta	r13L
			lda	r4H
			sbc	r3H
			sta	r13H

			lda	r2H
			sec
			sbc	r2L
			sta	r14L

			lda	#<WM_FJOB_TEST_MOV
			ldx	#>WM_FJOB_TEST_MOV
			jsr	WM_EDIT_WIN

			jsr	WM_SET_CARD_XY

			PushB	r2L
			PushW	r3
			PushW	r13
			PushB	r14L

			lda	r2L
			lsr
			lsr
			lsr
			pha
			lda	r3H
			lsr
			lda	r3L
			ror
			lsr
			lsr
			pha
			jsr	WM_DRAW_NO_TOP
			pla
			sta	DB_DELTA_X
			pla
			sta	DB_DELTA_Y

			jsr	WM_LOAD_SCREEN

			PopB	r14L
			PopW	r13
			PopW	r3
			PopB	r2L

			MoveW	r3  ,r4
			AddW	r13 ,r4
			MoveB	r2L ,r2H
			AddB	r14L,r2H
			jsr	WM_SET_WIN_SIZE		;Neue Fensterposition setzen.

			jsr	WM_DRAW_MOVER		;Scrollbalken aktualisieren.

			jmp	WM_SAVE_SCREEN		;Fenster in ScreenBuffer speichern.

;******************************************************************************
;Routine:   WM_FJOB_TEST_MOV
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Testen ob Fenster verschoben werden kann.
;           Wenn ja, dann neue X-/Y-Position für Gummi-Band setzen.
;******************************************************************************
:WM_FJOB_TEST_MOV	jsr	WM_FJOB_TEST_MX		;X-Verschieben testen.
			jsr	WM_FJOB_TEST_MY		;Y-Verschieben testen.

			lda	mouseXPos +0		;Neue Position für Gummi-Band
			sta	r3L			;berechnen.
			clc
			adc	r13L
			sta	r4L
			lda	mouseXPos +1
			sta	r3H
			adc	r13H
			sta	r4H

			lda	mouseYPos
			sta	r2L
			clc
			adc	r14L
			sta	r2H
			rts

;******************************************************************************
;Routine:   WM_FJOB_TEST_M...
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Testen ob Fenster verschoben werden kann.
;           Falls nein, Maus-Position korrigieren.
;******************************************************************************
:WM_FJOB_TEST_MY	lda	mouseYPos
			clc
			adc	r14L
			cmp	#MAX_AREA_WIN_Y 		;Nach unten möglich?
			bcc	:1			; => Ja, weiter...
			MoveB	r2L,mouseYPos		;Mausposition zurücksetzen.
::1			rts

:WM_FJOB_TEST_MX	lda	mouseXPos +0
			clc
			adc	r13L
			tax
			lda	mouseXPos +1
			adc	r13H
			cmp	#> MAX_AREA_WIN_X
			bne	:1
			cpx	#< MAX_AREA_WIN_X 	;Nach rechts möglich?
			bcc	:1			; => Ja, weiter...
			MoveW	r3 ,mouseXPos		;Mausposition zurücksetzen.
::1			rts

;******************************************************************************
;Routine:   WM_EDIT_WIN
;Parameter: AKKU/XREG = Zeiger auf Test-Routine.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Gewähltes Fenster mit Gummi-Band vergrößern/verschieben.
;           Wenn ja, dann neue X-/Y-Position für Gummi-Band setzen.
;******************************************************************************
:WM_EDIT_WIN		sta	:3 +1			;Vektor auf Testroutine speichern.
			stx	:3 +2

			php
			cli				;Interrupt freigeben (Mausbewegung).

			lda	WM_STACK		;Fenstergröße einlesen.
			jsr	WM_GET_WIN_SIZE

			lda	#(SCRN_HEIGHT - TASKBAR_HEIGHT) -1
			sta	mouseBottom		;Untere Fenstergrenze setzen.

::1			jsr	WM_DRAW_FRAME		;Gummi-Band zeichnen.

::2			lda	mouseData		;Maustaste noch gedrückt?
			bmi	:4			; => Nein, Ende...
			lda	inputData		;Mauszeiger bewegt?
			bmi	:2			; => Nein, warten...

			jsr	WM_DRAW_FRAME		;Gummi-Band löschen.

::3			jsr	$ffff			;Neue Position möglich?
			jmp	:1			;Weiter mit Mausabfrage.

::4			jsr	WM_DRAW_FRAME		;Gummi-Band löschen.

			LoadB	pressFlag,$00		;Tastenstatus löschen.

			lda	#SCRN_HEIGHT -1
			sta	mouseBottom		;Untere Fenstergrenze setzen.

			plp				;IRQ-Status zurücksetzen.
			rts

;******************************************************************************
;Routine:   WM_FUNC_MOVE...
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fensterinhalt nach oben/unten verschieben.
;******************************************************************************
:WM_FUNC_MOVER		lda	#$7f
			b $2c
:WM_FUNC_MOVE_UP	lda	#$00
			b $2c
:WM_FUNC_MOVE_DN	lda	#$ff
			sta	WM_MOVE_MODE

::1			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			PushW	WM_DATA_CURENTRY	;Zähler auf aktuellen Eintrag
							;zwischenspeichern.

			jsr	WM_CALL_MOVE		;Fensterinhalt verschieben.

			PopW	r0			;Zähler auf aktuellen Eintrag
							;zwischenspeichern.

			txa				;Fensterinhalt verschieben möglich?
			bne	:1a			; => Nein, weiter...

			CmpW	WM_DATA_CURENTRY,r0
			beq	:2			;Inhalt nicht verändert, Ende...

			jsr	WM_WIN_MARGIN		;Grenzen für Textausgabe setzen.

			MoveB	windowTop   ,r2L	;Grenzen als Fläche für
			MoveB	windowBottom,r2H	;Grafikroutine übernehmen.
			MoveW	leftMargin  ,r3
			MoveW	rightMargin ,r4

			lda	#$00			;Fensterbereich löschen.
			jsr	SetPattern
			jsr	Rectangle

			jsr	WM_CALL_DRAWROUT	;Fensterinhalt aktualisieren.

::1a			jsr	WM_DRAW_MOVER		;Scrollbalken aktualisieren.

			lda	GD_SLOWSCR		;Anzeige bremsen?
			bpl	:1b			; => Nein, weiter...
			jsr	SCPU_Pause
::1b			lda	mouseData		;Dauerfunktion ?
			bpl	:1			; => Ja, weiter...

::2			jsr	WM_SAVE_SCREEN		;Fenster in Screenuffer speichern.

			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_CALL_MSLCT
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mehrfach-Dateiauswahl.
;Hinweis:   $0000 = Keine Auswahl möglich.
;           $FFFF = Standard-Auswahl.
;******************************************************************************
:WM_CALL_MSLCT		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			lda	WM_DATA_WINMSLCT +0
			ldx	WM_DATA_WINMSLCT +1
			cmp	#$ff			;Standard-Auswahlfunktion?
			bne	callExtSelect		; => Nein, externe Rotuine aufrufen.
			cpx	#$ff
			bne	callExtSelect
			jmp	WM_SLCT_MULTI		;Mehrere Dateien auswählen.

;******************************************************************************
;Routine:   WM_CALL_SSLCT
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Einzel-Dateiauswahl.
;Hinweis:   $0000 = Keine Auswahl möglich.
;           $FFFF = Standard-Auswahl.
;******************************************************************************
:WM_CALL_SSLCT		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			lda	WM_DATA_WINSSLCT +0
			ldx	WM_DATA_WINSSLCT +1
			cmp	#$ff			;Standard-Auswahlfunktion?
			bne	callExtSelect		; => Nein, externe Rotuine aufrufen.
			cpx	#$ff
			bne	callExtSelect
			jmp	WM_SLCT_SINGLE		;Eine Datei auswählen.

:callExtSelect		ldy	WM_WCODE		;Externe Routine Einzel-
			jmp	CallRoutine		;Auswahl aufrufen.

;******************************************************************************
;Routine:   WM_SLCT_SINGLE
;Parameter: WM_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Einzel-Dateiauswahl.
;******************************************************************************
:WM_SLCT_SINGLE		php
			sei				;Interrupt sperren.

			jsr	InitFPosData		;Daten für Ausgabe initialisieren.

::next_column		lda	CurXPos			;Aktuelle X-Position innerhalb
			cmp	MaxXPos			;des gültigen Bereichs?
			bcc	:3			; => Ja, weiter...

::reset_column		jsr	restColumnData		;Spaltendaten zurücksetzen.
			bcc	:next_row		; => Nächste Zeile, weiter...

::end_output		plp
			rts				;Ende...

::next_row		sta	CurYPos			;Neue Y-Position speichern.
			inc	CountY			;Zähler für Zeilen korrigieren.

			ldx	WM_DATA_ROW		;Anzahl Zeilen begrenzt?
			beq	:2b			; => Nein, weiter...
			cmp	WM_DATA_ROW		;Max. Anzahl Zeilen erreicht?
			bcs	:end_output		; => Ja, Ende...

::2b			lda	CurXPos			;Aktuelle X-Position setzen.
::3			jsr	setEntryData		;Daten für aktuellen Eintrag setzen.

			jsr	WM_SIZE_ENTRY_F		;Größe Eintrag ermitteln.
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS	;Position in CARDs umwandeln.
			jsr	IsMseInRegion		;Mauszeiger innerhalb Icon?
			tay
			pla
			tax
			tya
			beq	:4a			; => Nein, weiter...

			plp				;IRQ-Status zurücksetzen.

			MoveW	CurEntry,r14		;Eintrag-Nr. einlesen.
			jsr	WM_INVERT_FILE		;Verzeichnis-Eintrag invertieren.
			jsr	WM_SAVE_SCREEN		;ScreenBuffer aktualisieren.
			jmp	WM_SAVE_WIN_DATA	;Fensterdaten aktualisieren.

::4a			IncW	CurEntry		;Zähler auf nächsten Eintrag.

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:end_output		; => Weitere Einträge ausgeben.

			cpx	#$7f			;War Eintrag im sichtbaren Bereich?
			beq	:5a			; => Nein, Eintrag in nächster
							;    Zeile darstellen.

			jsr	setNextColumn		;X-Position für nächsten Eintrag.

			lda	WM_DATA_COLUMN		;Max. Anzahl Spalten definiert?
			beq	:5			; => Nein, weiter...
			lda	CountX
			cmp	WM_DATA_COLUMN		;Max. Anzahl Spalten erreicht?
			bcc	:5a			; => Nein, weiter...

::5			jmp	:next_column		;Nächste Spalte.
::5a			jmp	:reset_column		;Nächste Zeile.

;******************************************************************************
;Routine:   WM_SLCT_MULTI
;Parameter: WM_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mehrfach-Dateiauswahl.
;******************************************************************************
:WM_SLCT_MULTI		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_GET_SLCT_AREA	;Auswahlrahmen erstellen.

			MoveB	r2L,SlctY0		;Größe Auswahlbereich speichern.
			MoveB	r2H,SlctY1
			MoveW	r3 ,SlctX0
			MoveW	r4 ,SlctX1
			MoveB	r5L,SlctMode		;Auswahl-Modus vollständig/kreuzen.

			;jsr	OpenWinDrive		;Laufwerk bereits aktiv...

			jsr	InitFPosData		;Daten für Ausgabe initialisieren.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			tax				;Dateien vorhanden?
			beq	:end_output		; => Nein, Ende...

::next_column		lda	CurXPos			;Aktuelle X-Position innerhalb
			cmp	MaxXPos			;des gültigen Bereichs?
			bcc	:3			; => Ja, weiter...

::reset_column		jsr	restColumnData		;Spaltendaten zurücksetzen.
			bcc	:next_row		; => Nächste Zeile, weiter...

::end_output		jsr	WM_SAVE_SCREEN		;Fenster in ScreenBuffer speichern.
			jmp	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

::next_row		sta	CurYPos			;Neue Y-Position speichern.
			inc	CountY			;Zähler für Zeilen korrigieren.

			ldx	WM_DATA_ROW		;Anzahl Zeilen begrenzt?
			beq	:2b			; => Nein, weiter...
			cmp	WM_DATA_ROW		;Max. Anzahl Zeilen erreicht?
			bcs	:end_output		; => Ja, Ende...

::2b			lda	CurXPos
::3			jsr	setEntryData		;Daten für aktuellen Eintrag setzen.

			lda	CurEntry +0		;Zeiger auf aktuellen Eintrag.
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			jsr	WM_SIZE_ENTRY_F 		;Größe Eintrag ermitteln.
			txa
			beq	:4

			pha
			jsr	WM_TEST_SELECT		;Eintrag ausgewählt?
			pla
			bcc	:3a			; => nein, weiter...

			pha
			MoveW	CurEntry,r14		;Eintrag-Nr. einlesen.
			jsr	WM_INVERT_FILE		;Verzeichnis-Eintrag invertieren.
			pla

::3a			IncW	CurEntry 		;Zähler auf nächsten Eintrag.

::3b			tax

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:end_output		; => Weitere Einträge ausgeben.

			cpx	#$7f			;War Eintrag im sichtbaren Bereich?
			beq	:5a			; => Nein, Eintrag in nächster
							;    Zeile darstellen.

			jsr	setNextColumn		;X-Position für nächsten Eintrag.

			lda	WM_DATA_COLUMN		;Max. Anzahl Spalten definiert?
			beq	:5			; => Nein, weiter...
			lda	CountX
			cmp	WM_DATA_COLUMN		;Max. Anzahl Spalten erreicht?
			bcc	:5a			; => Nein, weiter...

::5			jmp	:next_column		;Nächste Spalte.
::5a			jmp	:reset_column		;Nächste Zeile.

;******************************************************************************
;Routine:   WM_GET_SLCT_AREA
;Parameter: -
;Rückgabe:  r2L-r4 = Gewählter Rahmenbereich.
;           r5L    = Markierungsmodus:
;                    $00 = Links nach rechts.
;                    $ff = Rechts nach Links.
;Verändert: A,X,Y,r2-r5L
;Funktion:  Größe für Auswahl-Rahmen berechnen.
;******************************************************************************
:WM_GET_SLCT_AREA	jsr	:SET_MOUSE_FRAME	;Rahmenposition setzen.

			lda	mouseXPos +0		;Aktuelle Mausposition speichern.
			sta	:ClickX   +0
			lda	mouseXPos +1
			sta	:ClickX   +1
			lda	mouseYPos
			sta	:ClickY

			AddVBW	8,mouseXPos		;Mindestgröße für Auswahl-Rahmen
			AddVB	8,mouseYPos		;Festlegen.

::1			MoveW	mouseXPos,:MouseX	;Zeiger auf Anfangs-Position
			MoveB	mouseYPos,:MouseY	;für Auswahl-Rahmen setzen.
			jsr	:SET_FRAME		;End-Position setzen.
			jsr	WM_DRAW_FRAME		;Auswahl-Rahmen zeichnen.

::2			lda	mouseData		;Maustaste noch gedrückt?
			bmi	:3			; => Nein, Ende...
			lda	inputData		;Mausbewegung?
			bmi	:2			; => Nein, warten...

			jsr	:SET_FRAME		;End-Position setzen.
			jsr	WM_DRAW_FRAME		;Auswahl-Rahmen löschen.
			jmp	:1

::3			jsr	:SET_FRAME		;End-Position setzen.
			jsr	WM_DRAW_FRAME		;Auswahl-Rahmen löschen.

;--- Auswahlmodus festlegen.
;Auswahl-Rahmen von links nach rechts:
;Einträge müssen vollständig innerhalb
;des Auswahl-Rahmens liegen.
			ldx	#$00			;Auswahlmodus "Vollständig".
			CmpW	:ClickX,:MouseX		;Maus < Anfangs-Position?
			bcc	:4			; => Ja, Ende...
;Auswahl-Rahmen von rechts nach links:
;Einträge müssen teilweise innerhalb
;des Auswahl-Rahmens liegen.
			dex				;Auswahlmodus "Kreuzen".
::4			stx	r5L			;Auswahlmodus speichern.
			jmp	WM_NO_MOUSE_WIN

;--- Rahmenposition setzen.
::SET_FRAME		CmpW	:MouseX,:ClickX
			bcs	:11
			MoveW	:MouseX,r3
			MoveW	:ClickX,r4
			jmp	:12

::11			MoveW	:ClickX,r3
			MoveW	:MouseX,r4

::12			CmpB	:MouseY,:ClickY
			bcs	:13
			MoveB	:MouseY,r2L
			MoveB	:ClickY,r2H
			jmp	:14

::13			MoveB	:ClickY,r2L
			MoveB	:MouseY,r2H
::14			rts

;--- Mausgrenzen setzen.
::SET_MOUSE_FRAME	lda	WM_WCODE		;Fenster-Größe einlesen.
			jsr	WM_GET_WIN_SIZE

			lda	r3L			;Linker Rand.
			clc
			adc	#$08
			sta	mouseLeft +0
			lda	r3H
			adc	#$00
			sta	mouseLeft +1

			lda	r4L			;Rechter Rand.
			sec
			sbc	#$08
			sta	mouseRight +0
			lda	r4H
			sbc	#$00
			sta	mouseRight +1

			lda	r2L			;Oberer Rand.
			clc
			adc	#$08
			sta	mouseTop

			lda	r2H			;Unterer Rand.
			sec
			sbc	#$08
			sta	mouseBottom
			rts

;--- Variablen.
::ClickX		w $0000
::ClickY		b $00
::MouseX		w $0000
::MouseY		b $00

;******************************************************************************
;Routine:   WM_TEST_SELECT
;Parameter: SlctMode = Auswahlmodus.
;                      $00 = Links->Rechts.
;                      $FF = Rechts->Links.
;           r2L = Y-Position/oben für Auswahlfenster.
;           r2H = Y-Position/unten für Auswahlfenster.
;           r3  = X-Position/links für Auswahlfenster.
;           r4  = X-Position/rechts für Auswahlfenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mehrfach-Dateiauswahl: Prüfen ob Eintrag im Fenster.
;           Dabei wird unterschieden zwischen der Auswahl von
;           Links->Rechts und Rechts->Links:
;           Links->Rechts: Eintrag muss komplett im Rahmen sein.
;           Rechts->links: Eintrag muss teilweise im Rahmen sein.
;******************************************************************************
:WM_TEST_SELECT		jsr	WM_CONVERT_CARDS

			CmpB	r2L,SlctY1		;Y-Position teilweise in Auswahl?
			bcs	:not_selected		; => Nein, ignorieren.
			CmpB	r2H,SlctY0
			bcc	:not_selected

			bit	SlctMode		;Auswahl-Modus testen.
			bmi	:1			;Auswahl Rechts->Links, weiter...

			CmpB	r2L,SlctY0		;Y-Position komplett in Auswahl?
			bcc	:not_selected		; => Nein, ignorieren.
			CmpB	r2H,SlctY1
			bcs	:not_selected

::1			CmpW	r3 ,SlctX1		;X-Position teilweise in Auswahl?
			bcs	:not_selected		; => Nein, ignorieren.
			CmpW	r4 ,SlctX0
			bcc	:not_selected

			bit	SlctMode		;Auswahl-Modus testen.
			bmi	:selected		;Auswahl Rechts->Links, weiter...

			CmpW	r3 ,SlctX0		;X-Position komplett in Auswahl?
			bcc	:not_selected		; => Nein, ignorieren.
			CmpW	r4 ,SlctX1
			bcs	:not_selected

;--- Datei gewählt.
::selected		sec
			rts

;--- Nicht gewählt.
::not_selected		clc
			rts

;******************************************************************************
;Routine:   WM_INVERT_FILE
;Parameter: WM_WCODE = Fenster-Nr.
;           r2L = Y-Koordinate für Eintrag/oben.
;           r2H = Y-Koordinate für Eintrag/unten.
;           r3  = X-Koordinate für Eintrag/links.
;           r4  = X-Koordinate für Eintrag/rechts.
;           r14  = Eintrag-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Eintrag invertieren wenn innerhalb Auswahlbereich.
;******************************************************************************
:WM_INVERT_FILE		MoveW	r14,r15

			ldx	#r15L
			ldy	#r5L
			jsr	SET_POS_RAM		;Zeiger auf Eintrag berechnen.

			ldy	#$02
			lda	(r15L),y		;Dateityp-byte einlesen.
			cmp	#$ff			;"Weitere Dateien"?
			beq	:5			; => Ja, nicht auswählen...

			CmpW	r4,rightMargin		;Rechte Grenze innerhalb Bildschirm?
			bcc	:1			; => Ja, weiter...
			MoveW	rightMargin,r4		;Rechte Grenze setzen.

::1			CmpB	r2H,windowBottom	;Untere Grenze innerhalb Bildschirm?
			bcc	:2			; => Ja, weiter...
			MoveB	windowBottom,r2H	;Untere Grenze setzen.

::2			jsr	InvertRectangle		;Bereich invertieren.

			jsr	WM_SLCTMODE		;Datei aus-/abwählen.

			ldx	WM_WCODE		;Fenster-Nr. einlesen.
			lda	r4L			;Icon ausgewählt?
			bne	:4			; => Ja, weiter...

			lda	WMODE_SLCT_L,x		;Anzahl ausgewählte Dateien -1.
			bne	:3
			dec	WMODE_SLCT_H,x
::3			dec	WMODE_SLCT_L,x
			rts

::4			inc	WMODE_SLCT_L,x		;Anzahl ausgewählte Dateien +1.
			bne	:5
			inc	WMODE_SLCT_H,x
::5			rts

;******************************************************************************
;Routine:   WM_SLCTMODE
;Parameter: r14 = Eintrag-Nr.
;           r15 = Zeiger auf Verzeichnis-Eintrag im Speicher.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ändert Status "Datei ausgewählt" und speichert
;           neuen Status im Speicher/Cache.
;******************************************************************************
:WM_SLCTMODE		ldy	#$00
			lda	(r15L),y		;Status "Datei ausgewählt" umkehren.
			eor	#$7f
			sta	(r15L),y
			sta	r4L

			jsr	SET_POS_CACHE		;Zeiger auf Eintrag im Cache.
			bcc	:exit			; => Kein Cache, Ende...

			LoadW	r0 ,r4
			MoveW	r14,r1
			LoadW	r2 ,1
			MoveB	r12H,r3L		;Speicherbank wird durch
							;":SET_POS_CACHE" gesetzt.
			jmp	StashRAM

::exit			rts

;******************************************************************************
;Routine:   WM_SIZE_ENTRY_F/S
;Parameter: WM_WCODE = Fenster-Nr.
;           r1L = X-Position für Ausgabe.
;           r1H = Y-Position für Ausgabe.
;           r2L = Max. X-Position für Ausgabe.
;           r2H = Max. Y-Position für Ausgabe.
;           r3L = X-Abstand.
;           r3H = Y-Abstand.
;Rückgabe:  r2L = Breite für Eintrag in CARDs.
;           r2H = Höhe für Eintrag in Pixel.
;           XREG = $00 => Eintrag nicht anzeigen.
;                  $7F => Eintrag in nächster Zeile anzeigen.
;                  $FF => Eintrag anzeigen.
;Verändert: A,X,Y,r0-r15
;Funktion:  Definiert Größe für Eintrag bei Datei-Auswahl.
;           WM_SIZE_ENTRY_F:
;           Im Infomodus ist die Breite = Ganze Zeile.
;           WM_SIZE_ENTRY_S:
;           Im Infomodus ist die Breite = Dateiname.
;******************************************************************************
:WM_SIZE_ENTRY_F	ldy	#$00			;Breite Info-Modus = Ganze Zeile.
			b $2c

:WM_SIZE_ENTRY_S	ldy	#$ff			;Breite Info-Modus = Dateiname.

			ldx	WM_WCODE
			lda	WMODE_VICON,x		;Icon-Modus?
			bne	:textMode		; => Nein, weiter...

;--- Icon-Modus.
::iconMode		lda	r1L
			clc
			adc	#$03
			cmp	r2L			;Eintrag innerhalb der Zeilen?
			bcc	:1			; => Ja, weiter...
			ldx	#$00			;Eintrag nicht anzeigen.
			rts

::1			sta	r1L

			LoadB	r2L,$03			;Größe für Eintrag setzen.
			LoadB	r2H,$15			;Icon = 3CARDs breit, 21 Pixel hoch.

			ldx	#$ff			;Eintrag anzeigen.
			rts

;--- Text-Modus.
::textMode		ldx	WM_WCODE
			lda	WMODE_VINFO,x		;Details anzeigen?
			bne	:infoMode		; => Ja, weiter...

			lda	r1L
			clc
			adc	r3L
			cmp	r2L			;Eintrag innerhalb der Zeilen?
			bcc	:2			; => Ja, weiter...
			beq	:2			; => Ja, weiter...

			ldx	#$00			;Eintrag nicht anzeigen.
			rts

::2			jsr	WM_GET_GRID_X		;Größe für Eintrag setzen.
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$ff			;Eintrag anzeigen.
			rts

;--- Info-Modus.
::infoMode		tya				;Breiten-Modus abfragen.
			bne	:infoModeSmall		; => Nur Dateiname.

			lda	r2L			;Größe für Gnaze Zeile setzen.
			sec
			sbc	r1L
			sta	r2L
			jmp	:infoModeSet

::infoModeSmall		jsr	WM_GET_GRID_X		;Größe für Dateiname setzen.

::infoModeSet		sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$7f			;Eintrag in nächster Zeile zeigen.
			rts

;******************************************************************************
;Routine:   WM_TEST_ENTRY
;Parameter: WM_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Prüft ob ein Eintrag mit der Maus angeklickt wurde.
;******************************************************************************
.WM_TEST_ENTRY		lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			tax				;Dateien vorhanden?
			beq	:not_selected		; => Nein, Ende...

			php
			sei				;Interrupt sperren.

			jsr	InitFPosData		;Daten für Ausgabe initialisieren.

::next_column		lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::reset_column		jsr	restColumnData		;Spaltendaten zurücksetzen.
			bcc	:next_row		; => Nächste Zeile, weiter...

::end_output		plp
::not_selected		clc
			rts

::next_row		sta	CurYPos			;Neue Y-Position speichern.
			inc	CountY			;Zähler für Zeilen korrigieren.

			ldx	WM_DATA_ROW		;Anzahl Zeilen begrenzt?
			beq	:2b			; => Nein, weiter...
			cmp	WM_DATA_ROW		;Max. Anzahl Zeilen erreicht?
			bcs	:end_output		; => Ja, Ende...

::2b			lda	CurXPos
::3			jsr	setEntryData		;Daten für aktuellen Eintrag setzen.

			jsr	WM_SIZE_ENTRY_S		;Größe Eintrag ermitteln.
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS	;Position in CARDs umwandeln.
			jsr	IsMseInRegion		;Mauszeiger innerhalb Icon?
			tay
			pla
			tax
			tya
			beq	:4a			; => Nein, weiter...

			ldx	CurEntry +0		;Zeiger auf Eintrag.
			ldy	CurEntry +1
			plp				;IRQ-Status zurücksetzen.
			sec				;Eintrag ausgewählt.
			rts

::4a			IncW	CurEntry		;Zähler auf nächsten Eintrag.

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:end_output		; => Weitere Einträge ausgeben.

			cpx	#$7f			;War Eintrag im sichtbaren Bereich?
			beq	:5a			; => Nein, Eintrag in nächster
							;    Zeile darstellen.

			jsr	setNextColumn		;X-Position für nächsten Eintrag.

			lda	WM_DATA_COLUMN		;Max. Anzahl Spalten definiert?
			beq	:5			; => Nein, weiter...
			lda	CountX
			cmp	WM_DATA_COLUMN		;Max. Anzahl Spalten erreicht?
			bcc	:5a			; => Nein, weiter...

::5			jmp	:next_column		;Nächste Spalte.
::5a			jmp	:reset_column		;Nächste Zeile.

;*** Variablen für Dateiauswahl/Dateianzeige.
:CurXPos		b $00
:CurYPos		b $00
:MinXPos		b $00
:MaxXPos		b $00
:MinYPos		b $00
:MaxYPos		b $00
:CurEntry		w $0000
:CurGridX		b $00
:CurGridY		b $00
:CountX			b $00
:CountY			b $00
:SlctY0			b $00
:SlctY1			b $00
:SlctX0			w $0000
:SlctX1			w $0000
:SlctMode		b $00

