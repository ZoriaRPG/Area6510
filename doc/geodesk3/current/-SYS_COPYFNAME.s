﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Datei-Eigenschaften anzeigen.
:COPY_FILE_NAMES	ldx	#r0L
			jsr	ADDR_RAM_x		;Zeiger auf Anfang Verzeichnis.

			LoadW	r1,filesBuf		;Speicher für ausgewählte Dateien.

			ldx	#$00			;Anzahl ausgewählte Dateien
			stx	slctFiles		;löschen.

			stx	r3L			;Zeiger auf erste Datei.
			stx	r3H

::loop			ldy	#$00
			lda	(r0L),y			;Datei ausgewählt?
			beq	:next_file		; => Nein, weiter...

			ldy	#$02
			lda	(r0L),y			;Dateityp einlesen?
			cmp	#$ff			;Eintrag "Weitere Dateien>"?
			beq	:next_file

			clc				;Zeiger auf Anfang Dateiname
			lda	r0L			;setzen.
			adc	#<5
			sta	r2L
			lda	r0H
			adc	#>5
			sta	r2H

			ldx	#r2L			;Dateiname in Tabelle kopieren.
			ldy	#r1L
			jsr	SysCopyName

			AddVBW	17,r1			;Anzahl Dateien +1.
			inc	slctFiles

::next_file		AddVBW	32,r0			;Zeiger auf nächsten Eintrag.

			IncW	r3			;Datei-Zähler +1.
			CmpW	r3,WM_DATA_MAXENTRY
			bcc	:loop			; => Weiter mit nächster Datei.
			rts

;*** Variablen.
:slctFiles		b $00
:filesBuf		s MAX_DIR_ENTRIES * 17

