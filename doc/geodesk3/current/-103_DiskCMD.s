﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** CMD-Partitionen über $-Befehl.
if readPartModeCMD  = FALSE
:READ_PART_DATA		lda	DiskImgTyp
			asl
			tay
			lda	PartTypeList +0,y	;Kennung 41/71/81/NA in
			sta	FComPartList +6		;Verzeichnis-Befehl eintragen.
			lda	PartTypeList +1,y
			sta	FComPartList +7

			lda	#$00			;Anzahl Einträge löschen.
			sta	ListEntries

			sta	cntEntries +0		;Anzahl Dateien = 0.
			sta	cntEntries +1		;Anzahl Verzeichnisse = 0.

			jsr	ADDR_RAM_r15		;Zeiger auf Speicher für Daten.

;*** Partitionen hinzufügen.
:AddPartData		lda	#<FComPartList		;Liste mit Verzeichnissen einlesen.
			ldx	#>FComPartList
			ldy	#$7f
			jsr	GetDirList

:READ_PART_SIZE		jsr	PurgeTurbo		;GEOS-Turbo aus und I/O einschalten.
			jsr	InitForIO

			lda	ListEntries
			sta	r3L

			LoadW	r4,partEntryBuf
			jsr	ADDR_RAM_r15		;Zeiger auf Speicher für Daten.

::read_part		jsr	getPartSize		;Partitionsgröße einlesen.

::setSizeData		ldy	#$1e
			sta	(r15L),y
			iny
			txa
			sta	(r15L),y

			AddVBW	32,r15
			dec	r3L
			bne	:read_part

			ldx	#NO_ERROR		;Kein Fehler.
			jmp	DoneWithIO		;I/O abchalten.
endif

;*** CMD-Partitionen über GEOS/MP3.
if readPartModeCMD  = TRUE
:READ_PART_DATA		LoadW	r4,partTypeBuf		;Zeiger auf Zwischenspeicher.
			jsr	GetPTypeData		;CMD-Partitionsdaten abrufen.

			jsr	PurgeTurbo		;GEOS-Turbo aus und I/O einschalten.
			jsr	InitForIO

			lda	#$00			;Anzahl Einträge löschen.
			sta	ListEntries

			LoadB	r3H,1			;Zeiger auf Partition #1.
			LoadW	r4,partEntryBuf		;Zeiger auf Zwischenspeicher.

			jsr	ADDR_RAM_r15		;Zeiger auf Speicher für Daten.

::read_part		ldx	r3H
			lda	partTypeBuf,x		;Partitionstyp einlesen.
			cmp	DiskImgTyp		;Mit Laufwerkstyp vergleichen.
			bne	:next_part		; => Fehler, weiter...

			jsr	ReadPDirEntry		;Partitionseintrag einlesen.
			txa				;Fehler?
			bne	:next_part		; => Ja, nächste Partition.

			lda	partEntryBuf		;Partition definiert?
			beq	:next_part		; => Nein, nächste Partition.

			ldy	#$02
			lda	DiskImgTyp		;Partitions-Typ speichern.
			sta	(r15L),y
			iny
			lda	r3H			;Partitions-Nr. speichern.
			sta	(r15L),y

			ldy	#$05
			ldx	#$03
::loop_name		lda	partEntryBuf,x		;Partitions-Name speichern.
			cmp	#$a0
			beq	:exit_loop_name
			sta	(r15L),y
			iny
			inx
			cpx	#$03+16
			bcc	:loop_name

::exit_loop_name	jsr	getPartSize		;Partitionsgröße einlesen.

			ldy	#$1e			;Partitionsgröße in
			sta	(r15L),y		;Eintrag kopieren.
			iny
			txa
			sta	(r15L),y

			jsr	ChkListFull		;Zeiger auf nächsten Eintrag.
			txa				;Liste voll?
			bne	:exit			; => Ja, Ende...

::next_part		inc	r3H			;Zeiger auf nächsten Eintrag.
			lda	r3H
			cmp	#255			;Alle Partitionen geeprüft?
			bcc	:read_part		; => Nein, weitere...

::exit			ldx	#NO_ERROR		;Kein Fehler.
			jmp	DoneWithIO		;I/O abchalten.

;*** Speicher für Partitonstypen.
;Je 1 Byte für jede Partition, Wert von
;$00-$04 für Partitionstyp.
:partTypeBuf		s 256

endif

;*** CMD-Partitionsgröße ermitteln.
:getPartSize		ldy	#$02
			lda	(r15L),y		;Partitionstyp einlesen.
			cmp	#$04			;NativeMode?
			beq	:native			; => Nein, weiter...

			sec				;Zeiger auf Partitionsgröße
			sbc	#$01			;berechnen.
			asl
			tay
			lda	partSizeData+0,y	;Low-Byte.
			ldx	partSizeData+1,y	;High-Byte.
			bne	:exit

::native		ldy	#$03			;Bei NativeMode Cluster-Anzahl
			lda	(r15L),y		;einlesen.
			sta	r3H
			jsr	ReadPDirEntry
			txa				;Fehler?
			beq	:get_native_size	; => Nein, weiter...

			lda	#$00
			tax				;Partitionsgröße löschen.
			beq	:exit			; => Ende...

::get_native_size	lda	partEntryBuf+29		;Partitionsgröße in 512Byte Cluster
			asl				;in 256Byte Blocks umrechnen.
			pha
			lda	partEntryBuf+28
			rol
			tax
			pla
::exit			rts
