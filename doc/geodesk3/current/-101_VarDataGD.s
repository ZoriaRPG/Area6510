﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Erweiterte Systemvariablen.
.GD_VAR_START
.GD_RELOAD_DIR		b $00				;$80 = Dateien von Disk laden.
							;$40 = BAM testen/Cache oder Disk.
							;$3F = Nur Dateien sortieren.
							;$00 = Dateien aus Cache.
.GD_COL_MODE		b $ff				;$FF = S/W-Modus.
.GD_COL_DEBUG		b $00				;$FF = Debug-Modus, Cache=Farbig.
.GD_COL_CACHE		b $20				;$20 = Farbe für Icons aus Cache.
.GD_COL_DISK		b $00				;$00 = S/W-Datei-Icons.
.GD_LNK_LOCK		b $00				;$00 = Drag'n'Drop für AppLinks.
							;$FF = AppLinks gesperrt.
.GD_LNK_TITLE		b $00				;$00 = Keine Titel anzeigen.
							;$FF = Titel anzeigen
.GD_BACKSCRN		b $ff				;$00 = Kein Hintergrundbild.
							;$FF = Hintergrundbild verwenden.
.GD_SLOWSCR		b $00				;$00 = SlowScroll deaktiviert.
							;$FF = SlowScroll aktiv.
.GD_VIEW_DEL		b $00				;$00 = Gelöschte Dateien aus.
							;$FF = Gelöschte Dateien ein.
.GD_ICON_CACHE		b $ff				;$00 = Kein Icon-Cache aktiv.
							;$FF = 64K-Icon-Cache aktiv.
.GD_ICON_PRELOAD	b $00				;$00 = Icon nicht in Speicher laden.
							;$FF = Icon in Speicher laden.
.GD_INFO_SAVE		b $00				;$00 = AutoSave bei Eigenschaften inaktiv.
							;$FF = Eigenschaften automatisch speichern.
.GD_DEL_MENU		b $00				;$00 = Vor dem Löschen/Dateien nachfragen.
							;$FF = Dateien automatisch löschen.
.GD_DEL_EMPTY		b $ff				;$00 = Nicht-leere Verzeichnisse löschen.
							;$FF = Nur leere Verzeichnisse löschen.

.GD_VAR_END
