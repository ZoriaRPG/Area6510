﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Aktuelle Uhrzeit ausgeben.
.DrawClock		jsr	ResetFontGD		;Zeichensatz aktivieren.

;--- Bildschirmausgabe vorbereiten.
			jsr	WM_NO_MARGIN		;Textgrenzen löschen.

;--- Position für Datum setzen.
			LoadW	r11,MAX_AREA_BAR_X-$2f +3
			LoadB	r1H,MIN_AREA_BAR_Y +7

;--- Datum ausgeben.
			lda	day
			jsr	:prntNum
			lda	#"."
			jsr	SmallPutChar
			lda	month
			jsr	:prntNum
			lda	#"."
			jsr	SmallPutChar
			lda	millenium
			jsr	:prntNum
			lda	year
			jsr	:prntNum

			lda	#" "
			jsr	SmallPutChar

;--- Position für Uhrzeit setzen.
			LoadW	r11,MAX_AREA_BAR_X-$2f +3
			LoadB	r1H,MIN_AREA_BAR_Y +14

;--- Uhrzeit ausgeben.
			lda	hour
			jsr	:prntNum
			lda	#":"
			jsr	SmallPutChar
			lda	minutes
			jsr	:prntNum
			lda	#"."
			jsr	SmallPutChar
			lda	seconds
			jsr	:prntNum

			lda	#" "
			jmp	SmallPutChar

;--- Dezimal-Zahl 00-99 ausgeben.
::prntNum		jsr	DEZtoASCII		;Zahl von DEZ nach ASCII wandeln.
			pha
			txa
			jsr	SmallPutChar		;10er ausgeben.
			pla
			jmp	SmallPutChar		;1er ausgeben.

;*** DEZIMAL nach ASCII wandeln.
.DEZtoASCII		ldx	#"0"			;Startwert 10er.
::1			cmp	#10			;Zahl kleiner 10?
			bcc	:2			; => Ja, Ende...
			inx				;10er +1
			sbc	#10
			bcs	:1
::2			adc	#"0"			;ASCII-Wert für 1er definieren.
			rts
