﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS/Einstellungen - Setup starten.
:MENU_SETUP_EDIT	jsr	UpdateCore		;GeoDesk-Systemvariablen speichern.
			jmp	MNU_OPEN_EDITOR		;GEOS-Editor aufrufen.

;*** GEOS/Einstellungen - Drucker wechseln.
:MENU_SETUP_PRNT	lda	#$08			;Suche nach Druckertreiber
			jsr	SetDevice		;auf Laufwerk 8/A: starten.
			jsr	SLCT_PRINTER		;Druckertreiber wählen.
			txa				;Abbruch?
			bne	:exit			; => Ja, Ende...
			jmp	UpdateMyComputer	;"MyComputer" aktualisieren.
::exit			rts

;*** GEOS/Einstellungen - Eingabegerät wechseln.
:MENU_SETUP_INPT	lda	#$08			;Suche nach Druckertreiber
			jsr	SetDevice		;auf Laufwerk 8/A: starten.
			jsr	SLCT_INPUT		;Eingabetreiber wählen.
			txa				;Abbruch?
			bne	:exit			; => Ja, Ende...
			jmp	UpdateMyComputer	;"MyComputer" aktualisieren.
::exit			rts

;*** GEOS/Einstellungen - Gelöschte Dateien anzeigen.
:MENU_SETUP_VDEL	lda	GD_VIEW_DEL
			eor	#$ff
			sta	GD_VIEW_DEL
			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_REDRAW_ALL		;Alle Fenster neu zeichnen.

;*** GEOS/Einstellungen - Farbe anzeigen".
:MENU_SETUP_COLOR	lda	GD_COL_MODE
			eor	#$ff
			sta	GD_COL_MODE
			jmp	WM_REDRAW_ALL		;Alle Fenster neu zeichnen.

;*** GEOS/Einstellungen - Cache/Debug-Modus.
:MENU_SETUP_PLOAD	lda	GD_ICON_PRELOAD
			eor	#$ff
			sta	GD_ICON_PRELOAD
			rts

;*** GEOS/Einstellungen - Cache/Debug-Modus.
:MENU_SETUP_DEBUG	lda	GD_COL_DEBUG
			eor	#$ff
			sta	GD_COL_DEBUG
			jmp	WM_REDRAW_ALL		;Alle Fenster neu zeichnen.

;*** GEOS/Einstellungen - Cache/Debug-Modus.
:MENU_SETUP_CACHE	bit	GD_ICON_CACHE		;Icon-Cache aktiv?
			bmi	:disable_cache
			jsr	FindFreeBank		;64K für Icon-Daten.
			cpx	#NO_ERROR		;Speicher gefunden?
			beq	:icon_cache		; => Ja, weiter...

			LoadW	r0,Dlg_RamCacheErr
			jsr	DoDlgBox

::disable_cache		lda	#$00			;Kein Icon-Cache aktiv.
			sta	GD_ICON_CACHE
			sta	GD_ICONDATA_BUF
			rts

::icon_cache		sty	GD_ICONDATA_BUF
			jsr	AllocateBank		;Speicher reservieren.

			lda	#$ff			;Icon-Cache aktiv.
			sta	GD_ICON_CACHE

;--- Alle Dateien neu einlesen.
;Damit wird ggf. der Status "Icon im Cache" gelöscht.
::continue		jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_REDRAW_ALL		;Alle Fenster neu zeichnen.

;*** Nicht genügend freier Speicher.
:Dlg_RamCacheErr	b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$30
			w :3
			b DBTXTSTR   ,$0c,$3a
			w :4
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Nicht genügend Speicher frei!",NULL
::3			b "Für den Icon-Cache werden 64Kb",NULL
::4			b "freier DACC-Speicher benötigt.",NULL

;*** PopUp/Arbeitsplatz - Laufwerk öffnen.
:PF_OPEN_DRV_A		ldx	#8
			b $2c
:PF_OPEN_DRV_B		ldx	#9
			b $2c
:PF_OPEN_DRV_C		ldx	#10
			b $2c
:PF_OPEN_DRV_D		ldx	#11
			lda	driveType -8,x		;Laufwerk verfügbar?
			beq	:exit			; => Nein, Ende...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.
			txa				;Laufwerksfehler?
			bne	:exit			; => Ja, Abbruch...

			ldx	curDrive
			lda	RealDrvMode -8,x	;Laufwerkstyp einlesen.
			and	#SET_MODE_SUBDIR	;NativeMode?
			beq	:open_win		; => Nein, weiter...
			jsr	OpenRootDir		;Hauptverzeichnis öffnen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

::open_win		lda	curDrive		;Neues Fenster für
			sec	 																								;Laufwerk öffnen.
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveNewWin
::exit			rts

;*** PopUp/DeskTop - AppLink-Titel anzeigen.
:PF_VIEW_ALTITLE	lda	GD_LNK_TITLE
			eor	#$ff
			sta	GD_LNK_TITLE
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** PopUp/DeskTop - Hintergrund anzeigen.
:PF_BACK_SCREEN		lda	sysRAMFlg		;Hintergrundbild-Modus im
			eor	#%00001000		;GEOS-System wechseln.
			sta	sysRAMFlg
			ldx	#$00
			and	#%00001000		;Hintergrundbild anzeigen?
			beq	:1			; => Nein, weiter...
			dex
::1			stx	GD_BACKSCRN		;Flag für Hintergrundbild speichern.
			jsr	MainDrawDesktop		;Desktop neu zeichnen.
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** PopUp/DeskTop - AppLinks sperren.
:PF_LOCK_APPLINK	lda	GD_LNK_LOCK
			eor	#$ff
			sta	GD_LNK_LOCK
			rts

;*** PopUp/AppLink - Verzeichnis öffnen.
:PF_OPEN_SDIR		MoveW	AL_VEC_FILE,r14		;Zeiger auf Eintrag kopieren.
			jmp	AL_OPEN_SDIR		;Verzeichnis öffnen.

;*** PopUp/Titelzeile - Partition wechseln.
:PF_SWAP_DSKIMG		jsr	GET_PART_DATA		;Partitionsdaten einlesen.
			txa				;Fenster neu laden?
			beq	:ok			; => Ja, weiter...

			ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl-Modus testen.
			and	#%0100 0000		;SD2IEC-DiskImages?
			beq	:SD2IEC			; => Nein, weiter...
::error			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

::ok			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
::SD2IEC		jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp/Fenster - Datei/Verzeichnis Öffnen.
:PF_OPEN_FILE		MoveW	vecDirEntry,r0		;Zeiger auf Verzeichnis-Eintrag.
			jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;*** PopUp/Fenster - Datei-Eigenschaften.
:PF_FILE_INFO		MoveW	vecDirEntry,a0		;Zeiger auf Verzeichnis-Eintrag.

			ldy	#$00			;Datei im Speicher für die
			lda	#$7f			;Datei-Funktionen als "Ausgewählt"
			sta	(a0L),y			;markieren.

			jsr	UpdateCore		;Variablen sichern.
			jmp	MNU_FILE_INFO		;Datei-Informationen anzeigen.

;*** PopUp/Fenster - Datei/Verzeichnis Löschen.
:PF_DEL_FILE		MoveW	vecDirEntry,a0		;Zeiger auf Verzeichnis-Eintrag.

			ldy	#$00			;Datei im Speicher für die
			lda	#$7f			;Datei-Funktionen als "Ausgewählt"
			sta	(a0L),y			;markieren.

			jsr	UpdateCore		;Variablen sichern.
			jmp	MNU_FILE_DELETE		;Dateien löschen.

;*** PopUp/Fenster/Native - ROOT öffnen.
:PF_OPEN_ROOT		ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl-Modus testen.
			and	#%0100 0000		;SD2IEC-DiskImages?
			beq	:native			; => Nein, weiter...

;--- SD2IEC: Hauptverzeichnis öffnen.
			jsr	OPEN_SD_ROOT		;Hauptverzeichnis öffnen.
			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;--- Native: Hauptverzeichnis öffnen.
::native		lda	#$01
			sta	WIN_SDIR_T,x
			sta	WIN_SDIR_S,x
			jsr	OpenRootDir
			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp/Fenster/Native - Verzeichnis zurück.
:PF_OPEN_PARENT		ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl-Modus testen.
			and	#%0100 0000		;SD2IEC-DiskImages?
			beq	:native			; => Nein, weiter...

;--- SD2IEC: Elternverzeichnis öffnen.
			jsr	OPEN_SD_DIR		;Elternverzeichnis öffnen.
			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;--- Native: Elternverzeichnis öffnen.
::native		lda	curDirHead+34		;Hauptverzeichnis ?
			beq	:exit			; => Ja, Ende...
			sta	r1L			;Track/Sektor für
			ldx	WM_WCODE		;Elternverzeichnis kopieren und
			sta	WIN_SDIR_T,x		;in Fensterdaten speichern.
			lda	curDirHead+35
			sta	r1H
			sta	WIN_SDIR_S,x
			jsr	OpenSubDir		;Unterverzeichnis öffnen.
			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.
::exit			rts

;*** PopUp/Fenster - Neue Ansicht.
:PF_NEW_VIEW		jsr	OpenWinDrive		;Laufwerksfenster öffnen.

			jsr	WM_IS_WIN_FREE		;Freie Fenster-Nr. suchen.
			cpx	#NO_ERROR		;Fenster verfügbar?
			bne	:exit			; => Nein, Ende...

			tay				;Fenster-Einstellungen in neues
			ldx	WM_WCODE		;Fenster kopieren.
			lda	WMODE_VICON,x
			sta	WMODE_VICON,y
			lda	WMODE_VSIZE,x
			sta	WMODE_VSIZE,y
			lda	WMODE_VINFO,x
			sta	WMODE_VINFO,y
			lda	WMODE_FILTER,x
			sta	WMODE_FILTER,y
			lda	WMODE_SORT,x
			sta	WMODE_SORT,y

			lda	WIN_DRIVE,x		;Laufwerksadresse aus Fenster
			sec				;einlesen und neues Fenster öffnen.
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveUsrWin
::exit			rts

;*** PopUp Fenster - Neu laden.
:PF_RELOAD_DISK		jsr	OpenWinDrive		;Laufwerksfenster öffnen.

			ldx	WM_WCODE
			lda	#$00
			sta	WIN_DIR_TR,x		;Start-Position Verzeichnis
			sta	WIN_DIR_SE,x		;zurücksetzen.
			sta	WIN_DIR_POS,x
			sta	WIN_DIR_NR_L,x
			sta	WIN_DIR_NR_H,x

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.

			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp/Fenster/Anzeige - KByte/Blocks.
:PF_VIEW_SIZE		ldy	WM_WCODE
			lda	WMODE_VSIZE,y
			eor	#$ff
			sta	WMODE_VSIZE,y
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp/Fenster/Anzeige - Icons anzeigen.
:PF_VIEW_ICONS		ldx	WM_WCODE
			lda	#$00			;Text-Modus löschen.
			sta	WMODE_VINFO,x
			lda	WMODE_VICON,x		;Icon-Modus wechseln.
			eor	#$ff
			sta	WMODE_VICON,x
			jsr	INIT_WIN_GRID		;Raster für Fenster neu berechnen.
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp/Fenster/Anzeige - Textmodus/Details.
:PF_VIEW_DETAILS	ldx	WM_WCODE
			lda	#$ff			;Textmodus aktivieren.
			sta	WMODE_VICON,x
			lda	WMODE_VINFO,x		;Detail-Modus wechseln.
			eor	#$ff
			sta	WMODE_VINFO,x
			jsr	INIT_WIN_GRID		;Raster für Fenster neu berechnen.
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp/Fenster/Anzeige - SlowMode.
:PF_VIEW_SLOWMOVE	lda	GD_SLOWSCR
			eor	#$ff
			sta	GD_SLOWSCR
			rts

;*** PopUp/Fenster - Dateifilter.
:PF_FILTER_ALL		lda	#$00
			b $2c
:PF_FILTER_BASIC	lda	#$80
			b $2c
:PF_FILTER_APPS		lda	#$80 ! APPLICATION
			b $2c
:PF_FILTER_EXEC		lda	#$80 ! AUTO_EXEC
			b $2c
:PF_FILTER_DOCS		lda	#$80 ! APPL_DATA
			b $2c
:PF_FILTER_DA		lda	#$80 ! DESK_ACC
			b $2c
:PF_FILTER_FONT		lda	#$80 ! FONT
			b $2c
:PF_FILTER_PRNT		lda	#$80 ! PRINTER
			b $2c
:PF_FILTER_INPT		lda	#$80 ! INPUT_DEVICE

			ldx	WM_WCODE
			sta	WMODE_FILTER,x		;Neuen Filter-Modus setzen.

			lda	#$00			;Dateien neu einlesen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp/Fenster - Dateien sortieren.
:PF_SORT_NAME		lda	#$01
			b $2c
:PF_SORT_SIZE		lda	#$02
			b $2c
:PF_SORT_DATE_OLD	lda	#$03
			b $2c
:PF_SORT_DATE_NEW	lda	#$04
			b $2c
:PF_SORT_TYPE		lda	#$05
			b $2c
:PF_SORT_GEOS		lda	#$06
			ldx	WM_WCODE
			sta	WMODE_SORT,x		;Neuen Sortiermodus setzen.

			lda	#$00			;Zeiger auf Verzeichnis-Anfang.
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	SET_SORT_MODE		;Flag für ":GetFiles" setzen:
							;"Nur Dateien sortieren".

			;jsr	WM_MOVE_WIN_UP		;Fenster nach oben sortieren.
							;Wird jetzt durch Mausabfrage
							;bereits erledigt.

			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp/Fenster - Alle Dateien auswählen.
:PF_SELECT_ALL		lda	#$ff
			b $2c

;*** PopUp/Fenster - Auswahl aufheben.
:PF_SELECT_NONE		lda	#$00

			jsr	SetFileSlctMode		;Dateien markieren.
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp/Laufwerk - Öffnen.
:PF_OPEN_DRIVE		ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveCurWin		;Laufwerk öffnen.

;*** PopUp/Laufwerk - Neues Fenster.
:PF_OPEN_NEWVIEW	ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveNewWin		;Neues Fenster öffnen.

;*** PopUp/Laufwerk - Partition wechseln.
:PF_MYCOMP_PART		ldy	MyComputerEntry +1
			bne	:exit
			ldx	MyComputerEntry +0
			cpx	#$04			;Laufwerk güktig?
			bcs	:exit			; => Nein, weiter...

			lda	RealDrvMode,x		;CMD-/SD2IEC-Laufwerk?
			and	#SET_MODE_PARTITION!SET_MODE_SD2IEC
			beq	:exit			; => Nein, Ende...

			ldx	WM_WCODE
			lda	#$ff
			sta	WIN_DATAMODE,x		;Partitions-Modus setzen.

			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveCurWin		;Laufwerk öffnen.
::exit			rts

;*** PopUp/Laufwerk - AppLink erstellen.
:PF_CREATE_AL		lda	curDirHead +34		;Ist Hauptverzeichnis aktiv?
			ora	curDirHead +35
			bne	:set_directory		; => Nein, weiter...

::set_drive		lda	#$01			;AppLink als Laufwerk erstellen.
			ldx	#<Icon_Drive +1
			ldy	#>Icon_Drive +1
			bne	:create_applink

::set_directory		lda	#$03			;AppLink als Verzeichnis erstellen.
			ldx	#<Icon_Map +1
			ldy	#>Icon_Map +1

::create_applink	pha				;AppLink-Typ speichern.

			stx	r4L			;Zeiger auf Icon für
			sty	r4H			;AppLink speichern.

			LoadB	r3L,1			;Sprite für D`n`D erzeugen.
			jsr	DrawSprite

			pla
			jmp	AL_DnD_DrvSubD		;AppLink erstellen.

