﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Neue Laufwerks-Ansicht in "MyComputer" öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
;Im Unterschied zu :OpenDriveNewWin
;wird hier bei Native immer das Haupt-
;Verzeichnis geöffnet.
;Hier muss auch kein leeres Fenster
;gesucht werden da "MyComputer" dazu
;wiederverwendet wird.
:OpenDriveCurWin	cpy	#$00			;Eintrag in MyComputer >256?
			bne	:exit			; => Ja, Abbruch...
			cpx	#$04			;Laufwerk ausgewählt?
			bcs	:exit			; => Ja, Abbruch...
			txa
			clc				;Laufwerksadresse berechnen.
			adc	#$08
			jsr	SetDevice		;Laufwerk aktivieren.

			ldx	curDrive
			lda	RealDrvMode -8,x	;Laufwerksmodus einlesen.
			and	#SET_MODE_SUBDIR	;Native-Laufwerk?
			beq	:open_disk 		; => Nein, weiter...

;--- Bei Native ROOT öffnen.
::open_root		jsr	OpenRootDir		;Hauptverzeichnis öffnen.
			txa				;Fehler?
			beq	:open_window		; => Nein, dann Fenster öffnen.
			bne	:error_open		; => Ja, Abbruch...

;--- Diskette öffnen.
::open_disk		jsr	OpenDisk		;Diskette öffnen.
			txa				;Fehler?
			beq	:open_window		; => Ja, Abbruch...

;--- Fehler beim öffnen des Mediums.
::error_open		ldy	WM_MYCOMP		;Fenster-Nr. einlesen.
			jsr	testErrSD2IEC		;SD2IEC-Laufwerk?
			bne	:open_window		; => Ja, weiter...
			jmp	Error_NoDisk

;--- Fenster öffnen.
::open_window		lda	WM_MYCOMP		;Fenster-Nr. für "MyComputer".
			jsr	WM_WIN2TOP		;Fenster nach oben holen.
			jsr	SaveWinDrive		;Laufwerk in Fenster speichern.

			jsr	WM_LOAD_WIN_DATA	;Fenster-Daten laden.

			LoadW	r0,WIN_FILES		;Zeiger auf Fenster-Daten.

			ldy	#$01			;Fenster-Größe von "MyComputer"
::loop			lda	WM_DATA_BUF,y		;für neues Laufwerksfenster
			sta	(r0L),y			;kopieren.
			iny
			cpy	#$07
			bcc	:loop

			lda	WM_MYCOMP		;Fenster-Nr. für "MyComputer".
			pha
			lda	#$00			;"MyComputer" schließen.
			sta	WM_MYCOMP

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.

			pla				;Fenster-Nr. wieder herstellen.
			jsr	WM_USER_WINDOW		;Fenster erneut öffnen.

			ldy	#$01			;Standard-Fenstergröße löschen.
			lda	#$00
::38			sta	WIN_FILES,y
			iny
			cpy	#$07
			bcc	:38

::exit			rts

::SelectDrive		b $00
::SelectedWindow	b $00

;*** Neue Laufwerks-Ansicht öffnen,
;    Fenster-Optionen sind bereits
;    initialisiert => Nicht löschen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
:OpenDriveUsrWin	lda	#$ff			;Flag setzen: WMODE-Daten
			b $2c				;nicht löschen.

;*** Neue Laufwerks-Ansicht öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
:OpenDriveNewWin	lda	#$00			;WMODE-Daten löschen.
			sta	:WM_INIT_WINOPT		;WMODE-Flag speichern.

			cpy	#$00			;Zähler prüfen:
			bne	:exit			;Nur Einträge 0-3 = Laufwerk A-D
			cpx	#$04			;sind erlaubt.
			bcs	:exit

			txa
			pha
			jsr	WM_IS_WIN_FREE		;Leeres Fenster suchen.
			sta	:WM_NV_WINDOW
			pla
			cpx	#NO_ERROR		;Fehler gefunden?
			bne	:exit			; => Nein, Abbruch...

			clc
			adc	#$08
			jsr	SetDevice		;Laufwerk aktivieren.
			txa				;Fehler?
			bne	:error_open		; => Ja, Abbruch.

			ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsmodus gesetzt?
			bne	:open_window		; => Ja, weiter...

			jsr	OpenDisk		;Diskette öffnen.
			txa				;Fehler?
			beq	:open_window		; => Nein, weiter...

;--- Fehler beim öffnen des Mediums.
::error_open		ldy	:WM_NV_WINDOW		;Fenster-Nr. einlesen.
			jsr	testErrSD2IEC		;SD2IEC-Laufwerk?
			bne	:open_window		; => Ja, weiter...
			jmp	Error_NoDisk		;Fehler ausgeben.

;--- Fenster öffnen.
::open_window		ldx	:WM_NV_WINDOW		;Laufwerksdaten für
			jsr	SaveUserWinDrive	;neues Fenster speichern.

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.

			LoadW	r0,WIN_FILES		;Zeiger auf Fenster-Daten.
			lda	:WM_INIT_WINOPT
			sta	r1L
			jmp	WM_OPEN_WINDOW		;Neues Fenster öffnen.
::exit			rts

::WM_NV_WINDOW		b $00
::WM_INIT_WINOPT	b $00

;*** Bei DiskFehler auf SD2IEC testen.
;    Übergabe: YReg = Fenster-Nr.
;    Rückgabe: Z-Flag = 1 => Kein SD2IEC.
;Evtl. befindet sich das Laufwerknicht
;in einem DiskImage => DiskImage-Browser starten.
:testErrSD2IEC		ldx	curDrive
			lda	RealDrvMode -8,x	;Laufwerksmodus einlesen.
			and	#SET_MODE_SD2IEC	;Typ SD2IEC?
			beq	:1			; => Nein, weiter...
			lda	#$ff			;DiskImage-Modus setzen.
			sta	WIN_DATAMODE,y
::1			rts

;*** Fehler: Keine Diskette im Laufwerk.
:Error_NoDisk		LoadW	r0,Dlg_ErrNoDisk
			jmp	DoDlgBox

;*** Fehler: Keine Diskette in Laufwerk.
:Dlg_ErrNoDisk		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$30
			w :2
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "Laufwerk kann nicht geöffnet werden:",NULL
::2			b "Keine Diskette im Laufwerk!",NULL

;*** Laufwerksdaten in Fenster-Daten speichern.
:SaveWinDrive		ldx	WM_WCODE		;Fenster-Nr. einlesen.
:SaveUserWinDrive	lda	curDrive		;Aktuelles Laufwerk einlesen.
			sta	WIN_DRIVE,x		;Laufwerk speichern.

			tay
			lda	RealDrvType-8,y		;Laufwerkmodus einlesen und
			sta	WIN_REALTYPE,x		;in Fensterdaten speichern.

			lda	RealDrvMode-8,y		;CMD-Laufwerk?
			pha
			and	#SET_MODE_PARTITION
			beq	:no_part		; => Nein, weiter...

			lda	drivePartData-8,y	;Partition einlesen.
			b $2c
::no_part		lda	#$00			;Keine Partition.
			sta	WIN_PART,x		;Partitions-Daten speichern.

			pla				;Native-Laufwerk?
			and	#SET_MODE_SUBDIR
			tay
			beq	:no_native		; => Nein, weiter...

			lda	curDirHead +32		;Zeiger auf Verzeichnis-Header
			ldy	curDirHead +33		;einlesen.
::no_native		sta	WIN_SDIR_T,x		;Verzeichnis-Daten speichern.
			tya
			sta	WIN_SDIR_S,x
			rts

;*** Laufwerks-Fenster initialisieren.
:InitWIN		lda	#$00			;Datei-Zähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			ldx	WM_WCODE		;Anzahl gewählte Dateien löschen.
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			sta	WIN_DIR_TR,x		;Start-Position Verzeichnis
			sta	WIN_DIR_SE,x		;zurücksetzen.
			sta	WIN_DIR_POS,x
			sta	WIN_DIR_NR_L,x
			sta	WIN_DIR_NR_H,x

;*** Fenster-Grid initialisieren.
;    Übergabe: XReg = Fenster-Nr.
:INIT_WIN_GRID		;ldx	WM_WCODE
			lda	WMODE_VICON,x		;Icon-Modus aktiv?
			bne	:text_mode		; => Nein, Info/Text-Modus...

::icon_mode		lda	#$ff			;Standard-Verschieben für Icons.
			ldx	#$00			;Standard für Spaltenbreite.
			ldy	#$00			;Standard für Zeilenhöhe.
			beq	SET_WIN_GRID		;Grid-Daten setzen.

::text_mode		lda	WMODE_VINFO,x		;Text-Modus aktiv?
			bne	:info_mode		; => Nein, Info-Modus.

			lda	#$ff			;Standard-Verschieben für Icons.
			ldx	#$00			;Standard für Spaltenbreite.
			ldy	#$08			;8 Pixel Zeilenhöhe.
			bne	SET_WIN_GRID		;Grid-Daten setzen.

::info_mode		lda	#$ee			;Verschieben für Text-Modus.
			ldx	#$01			;Max. 1 Spalte.
			ldy	#$08			;8 Pixel Zeilenhöhe.
			;jmp	SET_WIN_GRID		;Grid-Daten setzen.

;*** Fenster-Grid ändern.
:SET_WIN_GRID		pha				;Grid-Werte sichern.
			txa
			pha
			tya
			pha

			jsr	WM_LOAD_WIN_DATA	;Fenster-Daten einlesen.

			pla
			sta	WM_DATA_GRID_Y		;Zeilenhöhe setzen.

			pla
			sta	WM_DATA_COLUMN		;Anzahl Spalten setzen.

			pla				;Verschiebe-Routine festlegen.
			sta	WM_DATA_WINMOVE+0
			sta	WM_DATA_WINMOVE+1

			jmp	WM_SAVE_WIN_DATA	;Fenster-Daten speichern.

;*** Aktuelles Fenster schließen.
:ExitWIN		ldx	WM_WCODE		;Fenster-Nr. einlesen.

			lda	#$00			;Partitions-Modus für aktuelles
			sta	WIN_DATAMODE,x		;Fenster zurücksetzen.

			rts

;*** Laufwerks-Fenster: Disk-Namen anzeigen.
;    Übergabe: r1H = Zeile (Pixel).
;              r11 = Spalte (Pixel).
:PrntCurDkName		ldx	WM_WCODE		;Fenster-Nr. einlesen.
			lda	WIN_DATAMODE,x		;Standard-Fenster-Modus?
			beq	:default		; => Ja, weiter...

			lda	#<:partSelect		; => SD2IEC-DiskImages...
			ldx	#>:partSelect
			jmp	:create_title

;--- Standard-Diskname ausgeben.
::default		ldy	WIN_DRIVE,x		;Laufwerksadresse einlesen.
			lda	RealDrvMode-8,y		;CMD-Laufwerk?
			and	#SET_MODE_PARTITION
			beq	:no_part		; => Nein, weiter...

			lda	drivePartData-8,y	;Partitions-Nr. einlesen.

::no_part		ldy	#"0"			;Partitions-Nr. nach
			ldx	#"0"			;ASCII wandeln.
::1			cmp	#100
			bcc	:2
			sbc	#100
			iny
			bne	:1
::2			cmp	#10
			bcc	:3
			sbc	#10
			inx
			bne	:2
::3			adc	#"0"
			sty	:DiskName +2		;ASCII-Wert der Partition
			stx	:DiskName +3		;in die Titel-Zeile schreiben.
			sta	:DiskName +4		;000 = Kein CMD-Laufwerk.

;--- Diskname kopieren.
			ldx	#r0L			;Zeiger auf Disknamen setzen.
			jsr	GetPtrCurDkNm

			LoadW	r4,:DiskName +6		;Zeiger auf Titelzeile.

			ldx	#r0L			;Diskname aus BAM in Titelzeile
			ldy	#r4L			;kopieren.
			jsr	SysCopyName

			lda	#<:DiskName
			ldx	#>:DiskName
			jsr	:create_title

;--- Verzeichnis-Position ausgeben.
			ldx	WM_WCODE		;Verzeichnis-Anfang?
			lda	WIN_DIR_NR_L,x
			ora	WIN_DIR_NR_H,x
			beq	:exit			; => Ja, keine Position ausgeben.

			LoadW	r0,:file_position	;Text "Position:" ausgeben.
			jsr	PutString

			ldx	WM_WCODE		;Aktuelle Position einlesen und
			lda	WIN_DIR_NR_L,x		;ausgeben.
			sta	r0L
			lda	WIN_DIR_NR_H,x
			sta	r0H
			lda	#%11000000
			jmp	PutDecimal
::exit			rts

;--- Fenster-Titel mit Laufwerk ausgeben.
::create_title		sta	r0L			;Teiger auf Titelzeile.
			stx	r0H

			lda	curDrive		;Laufwerks-Adresse einlesen.
			tax
			clc
			adc	#$39			;Laufwerk nach ASCII wandeln und
			ldy	#$00			;in Titelzeile schreiben.
			sta	(r0L),y

			jmp	PutString

::DiskName		b $00				;Laufwerk.
			b "/"
			b "000"				;Partition.
			b ":"
			s 17				;Disk-/Verzeichnis-Name.

::partSelect		b $00
			b ": PARTITION WÄHLEN"
			b NULL

::file_position		b " / Position: ",NULL

;*** Laufwerks-Fenster: Disk-Namen anzeigen.
:PrntCurDkInfo		ldx	WM_WCODE		;Fenster-Nr. einlesen.
			lda	WIN_DATAMODE,x		;Standard-Fenster-Modus?
			beq	:default		; => Ja, weiter...

;--- Partitions-/DiskImage-Auswahl.
			lda	curType			;Laufwerksmodus einlesen.
			and	#%0000 0111
			asl
			tax
			lda	:diskMode +0,x		;Laufwerkstyp einlesen und
			sta	:partMode +0		;in Statuszeile kopieren.
			lda	:diskMode +1,x
			sta	:partMode +1

			LoadW	r0,:partInfo		;Modus ausgeben.
			jsr	PutString
			jmp	:prnt_file_count

;--- Standard-Dateifenster.
::default		PushB	r1H			;Y-Koordinate Textausgabe sichern.

			jsr	OpenWinDrive		;Laufwerk öffnen.
			jsr	CalcBlksFree		;Freie Blöcke ermitteln.

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			beq	:0			; => Blocks, weiter...

			ldx	#r3L			;Freie und Max.Anzahl an
			ldy	#$02			;Blocks in KByte umrechnen.
			jsr	DShiftRight
			ldx	#r4L
			ldy	#$02
			jsr	DShiftRight

::0			PopB	r1H			;Y-Koordinate zurücksetzen.

			PushB	r3H
			PushB	r3L

;--- Speicher frei ausgeben.
			lda	r4H			;Anzahl freie Blocks für die
			sta	r0H			;Berechnung der belegten Blocks
			pha				;speichern und für die Ausgabe
			lda	r4L			;vorbereiten.
			sta	r0L
			pha

			lda	#%11000000		;Freie Blocks ausgeben.
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			bpl	:free_block		; => Blocks, weiter...
::free_kbyte		ldx	#0
			b $2c
::free_block		ldx	#4
			jsr	:prnt_strg		;"frei" ausgeben.

;--- Speicher belegt.
			PopW	r4			;Freie Blocks zurücksetzen.

			pla				;Belegten Speicher berechnen.
			sec
			sbc	r4L
			sta	r0L
			pla
			sbc	r4H
			sta	r0H
			lda	#%11000000		;Belegten Speicher ausgeben.
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			bpl	:used_block		; => Blocks, weiter...
::used_kbyte		ldx	#2			;KByte belegt ausgeben.
			b $2c
::used_block		ldx	#6			;Blocks belegt ausgeben.
			jsr	:prnt_strg

;--- Anzahl Dateien ausgeben.
			jsr	:prnt_file_count

			ldx	#8			;Anzahl Dateien ausgeben.

;--- Kb/Blocks frei/belegt, Anzahl Dateien.
::prnt_strg		lda	:tx_adr +0,x		;Textmeldung ausgeben.
			sta	r0L
			lda	:tx_adr +1,x
			sta	r0H
			jmp	PutString

;--- Anzahl Dateien/Partitionen ausgeben.
::prnt_file_count	lda	WM_DATA_MAXENTRY +0
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sta	r0H
			lda	#%11000000		;Anzahl Einträge ausgeben.
			jmp	PutDecimal		;Dateien oder Partitionen.

::tmp_Text0		b " Kb frei, ",NULL
::tmp_Text1		b " Kb belegt, ",NULL
::tmp_Text2		b " Blocks frei, ",NULL
::tmp_Text3		b " Blocks belegt, ",NULL
::tmp_Text10		b " Datei(en) ",NULL

::tx_adr		w :tmp_Text0
			w :tmp_Text1
			w :tmp_Text2
			w :tmp_Text3
			w :tmp_Text10

;--- Status-Zeile für Partitionen/DiskImages.
::partInfo		b "Modus: "
::partMode		b "??"
			b "/"
			b NULL

