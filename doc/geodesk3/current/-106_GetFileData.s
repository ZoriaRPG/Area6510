﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateien für Laufwerksfenster einlesen.
;--- Enthält aktuelles Fenster bereits Daten?
:xGET_ALL_FILES		jsr	OpenWinDrive		;Laufwerk öffnen.
			txa				;Fehler?
			beq	:get_files		; => Ja, Abbruch...

;--- Laufwerksfehler, Abbruch.
::diskerr		lda	#$00			;Dateizähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			jsr	SET_LOAD_CACHE		;GetFiles-Flag zurücksetzen.
			jmp	WM_SAVE_WIN_DATA	;Fensterdaten aktualisieren.

;--- Dateien einlesen.
::get_files		lda	#$3f			;AKKU=$3F für BIT-Vergleich.
			bit	GD_RELOAD_DIR		;GetFiles-Modus testen.
			bmi	:get_files_disk		; => Dateien von Disk einlesen.
			bvs	:test_cache		; => BAM testen/Cache, sonst Disk.
			beq	:get_files_std		; => Standardmodus: Cache einlesen.
;Damit Z-Flag=0 für Sortiermodus=$3F
;muss der AKKU=$3F sein! Grund:
;Der BIT-Befehl führt eine Bit-weise
;UND-Verknüpfung mit AKKU+ADRESSE durch.
			jmp	:test_sort_dir		; => Nur Dateien sortieren.

;--- Dateien einlesen.
::get_files_std		lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			beq	:get_files_disk		; => Keine Dateien im Speicher.

;--- Prüfen ob die Laufwerksdaten geändert wurden.
::test_drive		ldx	WM_WCODE
			cpx	getFileWin		;Gleiches Fenster aktiv?
			bne	:update_cache		; => Nein, neu einlesen.

			lda	WIN_DRIVE,x
			cmp	getFileDrv		;Anderes Laufwerk?
			bne	:update_cache		; => Ja, neu einlesen.

			tay
			lda	RealDrvMode -8,y	;CMD-Laufwerk?
			bpl	:1			; => Nein, weiter...
			lda	WIN_PART,x
			cmp	getFilePart		;Andere Partition?
			bne	:update_cache		; => Ja, neu einlesen.

::1			lda	RealDrvMode -8,y
			and	#%01000000		;NativeMode-Laufwerk?
			beq	:2			; => Nein, weiter...

			lda	WIN_SDIR_T,x
			cmp	getFileSDir +0		;Anderes Unterverzeichnis?
			bne	:update_cache		; => Ja, neu einlesen.
			lda	WIN_SDIR_S,x
			cmp	getFileSDir +1
			bne	:update_cache		; => Ja, neu einlesen.
::2			rts

;--- BAM testen. Wenn OK=Cache, sonst von Disk laden.
::test_cache		jsr	TestCacheBAM		;BAM auf Veränderung testen.
			txa				;BAM geändert?
			bne	:get_files_disk		; => Ja, Dateien von Disk einlesen.
			beq	:get_files_cache	; => Nein, von Cache laden.

;--- Dateien aus Cache laden.
::update_cache		ldx	getFileWin
			beq	:get_files_cache
			PushB	WM_WCODE
			stx	WM_WCODE
			jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.
			jsr	StashRAM		;Cache aktualisieren.
			PopB	WM_WCODE

::get_files_cache	jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.
			jsr	FetchRAM		;Verzeichnis aus Cache einlesen und
			jmp	:test_sort_dir		;ggf. sortieren.

;--- Dateien von Disk einlesen.
::get_files_disk	jsr	i_FillRam		;Verzeichnis-Speicher löschen.
			w	(OS_VARS - BASE_DIR_DATA)
			w	BASE_DIR_DATA
			b	$00

			jsr	GetFilesDisk		;Dateien von Disk laden.

			jsr	SaveCacheCRC		;BAM-CRC erzeugen.

			ldx	WM_WCODE		;Dateiauswahl löschen.
			lda	#$00
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			lda	WMODE_SORT,x		;Fenster sortieren?
			beq	:save_drv_data		; => Nein, Ende...

			and	#%01111111		;Flag löschen: Fenster sortiert.
			sta	WMODE_SORT,x
			bpl	:sort_dir

::test_sort_dir		ldx	WM_WCODE
			lda	WMODE_SORT,x		;Fenster sortieren?
			beq	:save_drv_data		; => Nein, Ende...
			bmi	:save_drv_data		; => Bereits sortiert, Ende...

::sort_dir		jsr	xSORT_ALL_FILES		;Falls "Neue Ansicht" oder wenn kein
							;Cache aktiv, dann ist eventuell
							;Sortiermodus für Fenster gesetzt.

;--- Laufwerksdaten für Dateien im RAM speichern.
::save_drv_data		ldx	WM_WCODE
			stx	getFileWin

			lda	WIN_DRIVE,x
			sta	getFileDrv		;Laufwerk speichern.

			tay
			lda	RealDrvMode -8,y	;CMD-Laufwerk?
			bpl	:3			; => Nein, weiter...
			lda	WIN_PART,x
			sta	getFilePart		;Partition speichern.

::3			lda	RealDrvMode -8,y
			and	#%01000000		;NativeMode-Laufwerk?
			beq	:4			; => Nein, weiter...

			lda	WIN_SDIR_T,x		;Zeiger auf Verzeichnis
			sta	getFileSDir +0		;speichern.
			lda	WIN_SDIR_S,x
			sta	getFileSDir +1

::4			jsr	SET_LOAD_CACHE		;GetFiles-Flag zurücksetzen.

			ldx	#NO_ERROR
			rts

;*** Dateien von Disk laden.
:GetFilesDisk		lda	#$00			;Dateizähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1

			jsr	ADDR_RAM_r15		;Zeiger auf Verzeichnis im RAM.

			jsr	Set1stDirSek		;Ersten Verzeichnis-Eintrag lesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:no_more_files		; => Ja, Ende...

::test_next_entry	jsr	TestDirEntry		;Verzeichnis-Eintrag testen und
							;ggf. in Speicher kopieren.
::next_entry		jsr	GetNxtDirEntry		;Nächsten Eintrag lesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:no_more_files		; => Ja, Ende...

			lda	WM_DATA_MAXENTRY +1
			cmp	#>MAX_DIR_ENTRIES -1
			bne	:1
			lda	WM_DATA_MAXENTRY +0
			cmp	#<MAX_DIR_ENTRIES -1
			beq	:buffer_full		;"Weitere Dateien" hinzufügen.
			bcs	:end_read_data		;Speicher voll, Ende...
::1			jmp	:test_next_entry	; => Weiter mit nächsten Eintrag.

;--- Speicher voll, "Weitere Dateien" hinzufügen.
::buffer_full		ldx	WM_WCODE		;Aktuellen Verzeichnis-Sektor und
			lda	r1L 			;Eintrag speichern.
			sta	WIN_DIR_TR,x
			lda	r1H
			sta	WIN_DIR_SE,x
			lda	r5L
			sta	WIN_DIR_POS,x

			ldy	#$1f			;Eintrag "Weitere Dateien"
::2			lda	:more_files,y		;erstellen.
			sta	(r15L),y
			dey
			bpl	:2

			IncW	WM_DATA_MAXENTRY
			jmp	:end_read_data

;--- Komplettes Verzeichnis eingelesen.
::no_more_files		ldx	WM_WCODE		;Keine weiteren Dateien.
			lda	#$00 			;Daten für "Weitere Dateien"
			sta	WIN_DIR_TR,x		;zurücksetzen.
			sta	WIN_DIR_SE,x
			sta	WIN_DIR_POS,x

;--- Verzeichnis eingelesen.
::end_read_data		lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			beq	:exit			; => Keine Dateien vorhanden...

			ldx	WM_WCODE
			lda	WMODE_VICON,x		;Anzeige-Modus einlesen.
			bne	:10			; => Keine Icons anzeigen.

			bit	GD_ICON_CACHE		;Icon-Cache aktiv?
			bpl	:10			; => Nein, weiter...

			bit	GD_ICON_PRELOAD		;Icons vorab laden?
			bpl	:10			; => Nein, weiter...

			jsr	LoadIconToCache		;Icons in Cache kopieren.

;--- Hinweis:
;":LoadIconToCache" setzt das Flag
;"Icon im Cache" im Verzeichnis-Eintrag!
::10			jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.
			jsr	StashRAM		;Verzeichnis in Cache speichern.

::exit			jmp	WM_SAVE_WIN_DATA	; => Ende...

;--- Eintrag für "Weitere Dateien".
::more_files		b $00,$ff
			b $ff
			b $00,$00
			b ">Mehr Dateien...",NULL
			b $00,$00
			b $00
			b $00
			b 80,1,1
			b 0,0
			b 0,0

;*** Zeiger auf Anfang Verzeichnis setzen.
:Set1stDirSek		ldx	WM_WCODE
			lda	WIN_DIR_TR,x		;PagingMode aktiv?
			bne	:1			; => Ja, weiter...
			jmp	Get1stDirEntry		;Ersten Verzeichnis-Eintrag lesen.

::1			jsr	Get1stDirEntry		;Register auf Anfang setzen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			ldx	WM_WCODE		;Zeiger auf Verzeichnis-Sektor
			lda	WIN_DIR_TR,x		;setzen.
			sta	r1L
			lda	WIN_DIR_SE,x
			sta	r1H

			lda	WIN_DIR_POS,x		;Zeiger auf Verzeichnis-Eintrag in
			sta	r5L			;Verzeichnis-Sektor setzen.
			lda	#>diskBlkBuf
			sta	r5H

			LoadW	r4,diskBlkBuf		;Zeiger auf Verzeichnis-Sektor.
			jsr	GetBlock		;Verzeichnis-Sektor einlesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			ldy	#$00			;Verzeichnis-Ende nicht erreicht.
			ldx	#NO_ERROR		;Kein Fehler.
::exit			rts

;*** Verzeichnis-Eintrag testen und in Speicher übernehmen.
;    Übergabe: r5 = Zeiger auf 30-Byte-Verzeichnis-Eintrag.
;              diskBlkBuf = Verzeichnis-Sektor.
;              WM_DATA_MAXENTRY = Verzeichnis-Eintrag-Nr.
:TestDirEntry		ldy	#$00
			lda	(r5L),y			;Dateityp einlesen.
			bne	:test_sdir		; => Echte Datei, weiter...
			iny
			lda	(r5L),y			;Dagtenspur gesetzt?
			beq	:exit			; => Nein, kein Datei-Eintrag.

::test_del_file		lda	GD_VIEW_DEL		;Gelöschte Dateien anzeigen?
			beq	:exit			; => Nein, Ende...

::test_sdir		and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;CMD-Verzeichnis?
			beq	:copy_entry		; => Ja, weiter...

::test_filter		ldx	WM_WCODE
			lda	WMODE_FILTER,x		;Filter aktiv?
			bpl	:copy_entry		; => Nein, weiter...

			ldy	#$16
			lda	(r5L),y			;GEOS-Dateityp einlesen.
			ora	#%10000000		;Bit#7 für Vergleich setzen.
			cmp	WMODE_FILTER,x		;Datei für Filter gültig?
			bne	:exit			; => Nein, nächster Eintrag.

::copy_entry		lda	#$00
			tay
			sta	(r15L),y		;Flag für "Dateiauswahl" löschen.
			iny
			lda	#$ff
			sta	(r15L),y		;Flag für "Icon im Cache" löschen.

::loop1			dey
			lda	(r5L),y			;Verzeichnis-Eintrag kopieren.
			iny
			iny
			sta	(r15L),y
			cpy	#32 -1			;Verzeichnis-Eintrag kopiert?
			bcc	:loop1			; => Nein, weiter...

			AddVBW	32,r15			;Zeiger auf nächsten Eintrag.

			IncW	WM_DATA_MAXENTRY

::exit			rts

;*** Datei-Icons in Cache kopieren.
:LoadIconToCache	lda	#$00			;Datei-Zähler löschen.
			sta	r11L
			sta	r11H

			sta	r14L			;Zeiger auf ersten Eintrag
			sta	r14H			;im Cache setzen.
			jsr	SET_POS_CACHE

			jsr	ADDR_RAM_r15		;Anfang Verzeichnis im RAM.

::test_entry		jsr	GetVecIcon_r0		;Icon einlesen.

			lda	r0L
			ora	r0H			;Icon eingelesen?
			beq	:next_entry		; => Nein, weiter...

::save_entry		MoveW	r13,r1
			LoadW	r2,64			;Größe Icon-Eintrag.
			MoveB	r12L,r3L		;Speicherbank (:SET_POS_CACHE).
			jsr	StashRAM		;Icon in Cache kopieren.

::next_entry		AddVBW	32,r15			;Zeiger auf nächsten
							;Verzeichnis-Eintrag im RAM.

			AddVBW	64,r13			;Zeiger auf nächsten
							;Icon-Eintrag im RAM.

			IncW	r11			;Datei-Zähler erhöhen.

			lda	r11H			;Alle Einträge kopiert?
			cmp	WM_DATA_MAXENTRY +1
			bne	:2
			lda	r11L
			cmp	WM_DATA_MAXENTRY +0
			beq	:exit
::2			jmp	:test_entry		; => Nein, weiter...

::exit			rts				;Ende.

;*** Zeiger auf Datei-Icon einlesen.
;Hier werden nur GEOS-Infoblock-Icons eingelesen!
;    Übergabe: r15 = 32-Byte Verzeichnis-Eintrag.
;    Rückgabe: r0  = $0000 = Kein Icon.
;                  > $0000 = Zeiger auf GEOS-Icon.
:GetVecIcon_r0		lda	#$00			;Zeiger auf Icon löschen.
			sta	r0L
			sta	r0H

			ldy	#$02
			lda	(r15L),y		;Datei "gelöscht"?
			beq	:exit			; => Ja, Ende...

			cmp	#$ff			;Eintrag "Weitere Dateien"?
			beq	:exit			; => Ja, Ende...

			and	#%00001111
			cmp	#$06			;Dateityp = "Verzeichnis"?
			beq	:exit			; => Ja, Ende...

			ldy	#$18
			lda	(r15L),y		;GEOS-Datei?
			beq	:exit			; => Nein, Ende...

			ldy	#$15
			lda	(r15L),y		;GEOS-Datei mit FileHeader?
			beq	:exit			; => Nein, Ende...
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader		;Zeiger auf Zwischenspeicher.

			ldy	#$01
			lda	#$00
			sta	(r15L),y		;Flag "Icon im Cache" setzen.

			jsr	GetBlock		;GEOS-FileHeader einlesen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			lda	#<fileHeader+4		;Zeiger auf Icon-Daten.
			ldx	#>fileHeader+4
			bne	:set_icon_vec

::error			lda	#<Icon_Deleted		;Fehler File-Header:
			ldx	#>Icon_Deleted		;Ersatz-icon "Gelöscht" verwenden.

::set_icon_vec		sta	r0L			;Zeiger auf Icon-Daten speichern.
			stx	r0H
::exit			rts

;*** Prüfen ob Dateien aus Cache eingelesen werden können.
;Wird :GD_RELOAD_DIR auf $FF gesetzt werden die Daten immer
;von Disk eingelesen. Evtl. Sinnvoll nach einem Reboot in den
;Desktop wenn andere Programme Daten auf Disk geändert haben.
;Aktuell wird die BAM geprüft, das funktioniert auf NativeMode
;aber nur bedingt (es werden nicht alle BAM-Sektoren getestet).
:TestCacheBAM		;lda	GD_RELOAD_DIR		;Dateien von Disk lesen?
			;bne	:cache_fault		; => Ja, weiter...

::verify1541		jsr	SetBAMCache1Sek		;Zeiger auf ersten BAM Sektor.
			bne	:verify1571
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verify1571		jsr	SetBAMCache2Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verify1581		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verify1581		jsr	SetBAMCache3Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verifyNative		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verifyNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y
			bne	:cache_ok

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:cache_fault		; => Ja, Disk öffnen.

			jsr	SetBAMCache4Sek		;BAM-CRC erstellen.
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehl
			beq	:cache_ok		; => Nein, Cache gültig...

;--- Verzeichnis geändert,
;    Dateien von Disk laden.
::cache_fault		ldx	#$ff
			rts

;--- Verzeichnis nicht geändert,
;    Dateien aus Cache laden.
::cache_ok		ldx	#$00
			rts

;*** Cache-Modus: Aktuelle BAM sichern.
:SaveCacheCRC
::upd1541		jsr	SetBAMCache1Sek		;Ersten BAM-Sektor sichern?
			bne	:upd1571		; => Nein, weiter...
			jsr	StashRAM

::upd1571		jsr	SetBAMCache2Sek		;Zweiten BAM-Sektor sichern?
			bne	:upd1581		; => Nein, weiter...
			jsr	StashRAM

::upd1581		jsr	SetBAMCache3Sek		;Dritten BAM-Sektor sichern?
			bne	:updNative		; => Nein, weiter...
			jsr	StashRAM

::updNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y	;Native-BAM sichern?
			bne	:exit			; => Nein, Ende...

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:exit			; => Ja, Abbruch...

			jsr	SetBAMCache4Sek		;Native-BAM sichern?.
			jmp	StashRAM
::exit			rts

;*** CRC für Native-Mode BAM erstellen.
:CreateNM_CRC		jsr	i_FillRam		;CRC-Puffer löschen.
			w	256
			w	diskBlkBuf
			b	$00

			jsr	EnterTurbo		;I/O und GEOS-Turbo aktivieren.
			jsr	InitForIO

			ldx	#$02			;Zeiger auf ersten BAM-Sektor.
::1			txa
			pha
			jsr	GetBAMBlock		;BAM-Sektor einlesen.
			pla
			cpx	#NO_ERROR		;Fehler?
			bne	:exit1			; => Ja, Abbruch...

			ldy	#<dir2Head		;Zeiger auf BAM-Sektor.
			sty	r0L
			ldy	#>dir2Head
			sty	r0H
			stx	r1L
			inx
			stx	r1H
			pha
			jsr	CRC			;CRC für BAM-Sektor berechnen.w
			pla
			tax
			asl
			tay
			lda	r2L			;CRC in Puffer kopieren.
			sta	diskBlkBuf +0,y
			lda	r2H
			sta	diskBlkBuf +1,y

			cpx	#$02			;Erster BAM-Sektor?
			bne	:2			; => Nein, weiter...
			lda	dir2Head +8		;Anzahl Tracks einlesen und
			sta	BAMTrackMaxNM		;zwischenspeichern.

::2			inx
			lda	BAMTrackCntNM-2,x
			cmp	BAMTrackMaxNM		;Max. Anzahl Tracks erreicht?
			bcs	:3			; => Ja, Ende...
			cpx	#33			;Max. Anzahl BAM-Sektoren erreicht?
			bcc	:1			; => Nein, weiter...

;--- BAM-CRC erstellt, Ende.
::3			ldx	#NO_ERROR		;Ende...
::exit1			jmp	DoneWithIO

;*** Zeiger auf Cache-Bereich für BAM-Sektoren.
;--- 1541/1571/1581/Native.
:SetBAMCache1Sek	ldx	curDrive
			lda	driveType -8,x
			and	#%00000111		;Laufwerksmodus isolieren.
			sta	driveTypeCopy

			tay
;			ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo1,y
			bne	SetBAMCacheExit
			lda	#<curDirHead		;Zeiger auf ersten BAM-Sektor.
			ldx	#>curDirHead
			ldy	#$00
			beq	SetBAMCacheInit

;--- 1571/1581.
:SetBAMCache2Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo2,y
			bne	SetBAMCacheExit
			lda	#<dir2Head		;1571, 1581 oder Native.
			ldx	#>dir2Head		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$01
			bne	SetBAMCacheInit

;--- 1581.
:SetBAMCache3Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo3,y
			bne	SetBAMCacheExit

			cpy	#DrvNative		;NativeMode ?
			bne	:1			; => Nein, Abbruch...
			lda	#<DISK_BASE		; => Native, Variablen testen.
			ldx	#>DISK_BASE
			bne	:2

::1			lda	#<dir3Head		; => 1581, dir3Head testen.
			ldx	#>dir3Head
::2			ldy	#$02
			bne	SetBAMCacheInit

;--- Native.
:SetBAMCache4Sek	lda	#<diskBlkBuf		;1571, 1581 oder Native.
			ldx	#>diskBlkBuf		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$03

:SetBAMCacheInit	sta	r0L			;Zeiger auf CRC-Speicher im RAM.
			stx	r0H

			lda	WM_WCODE		;Zeiger auf CRC-Speicher in
			asl				;GEOS-DACC berechnen.
			tax
			clc
			lda	#$00
			adc	vecBAMDataRAM +0,x
			sta	r1L
			tya
			adc	vecBAMDataRAM +1,x
			sta	r1H

			LoadW	r2,256			;Größe CRC-Speicher.

			lda	GD_SYSDATA_BUF		;64Kb Speicherbank für
			sta	r3L			;CRC-Daten setzen.

			ldx	#$00			;XReg = $00, BAM-Sektor verfügbar.
:SetBAMCacheExit	rts				;XReg = $FF, nicht verfügbar.

;*** Temporäe Kopie von driveType.
:driveTypeCopy		b $00

;*** Tabelle mit BAM-Sektor-info.
;$00 = BAM-Sektor vorhanden.
;$FF = BAM-Sektor nicht vorhanden.
;Type: NoDRV,1541,1571,1581,
;      Native,NoDRV,NoDRV,NoDRV
:BAMCopySekInfo1	b $ff,$00,$00,$00,$00,$ff,$ff,$ff
:BAMCopySekInfo2	b $ff,$ff,$00,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo3	b $ff,$ff,$ff,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo4	b $ff,$ff,$ff,$ff,$00,$ff,$ff,$ff

;*** Datentabelle mit Max. Track je BAM-Sektor.
:BAMTrackCntNM		b $01,$08,$10,$18,$20,$28,$30,$38
			b $40,$48,$50,$58,$60,$68,$70,$78
			b $80,$88,$90,$98,$a0,$a8,$b0,$b8
			b $c0,$c8,$d0,$d8,$e0,$e8,$f0,$f8

;*** Puffer für Max. Tracks/NativeMode.
:BAMTrackMaxNM		b $00
