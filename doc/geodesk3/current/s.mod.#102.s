﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* GeoDesk initialisieren.
;* GeoDesk über EnterDeskTop starten.
;* GeoDesk aktualisieren.
;* GeoDesk Fenstermanager starten.
;* GeoDesk Fenstermanager neu starten.

if .p
			t "TopSym"
			t "TopSym.MP3"
			t "TopSym.GD"
			t "TopMac.GD"
			t "-SYS_APPLINK"
			t "s.mod.#101.ext"
endif

			n "mod.#102.obj"
			t "-SYS_CLASS.h"
			f DATA
			o VLIR_BASE
			p MainInit
			a "Markus Kanet"

:VlirJumpTable		jmp	MainBoot
			jmp	MainReBoot
			jmp	MainUpdate
			jmp	MainReStart
			jmp	MainInitWM

;*** Programmvariablen.
:vecDirEntry		w $0000
:MyComputerEntry	w $0000

;*** Programmrroutinen.
			t "-102_MainDesktop"
			t "-102_TaskBar"
			t "-102_StartFile"
			t "-102_AppLink"
			t "-102_SetAppLink"
			t "-102_PopUpMenu"
			t "-102_PopUpFunc"
			t "-102_PopUpData"
			t "-102_GMenuData"
			t "-102_SetSlctMode"
			t "-102_DoFileEntry"
			t "-102_DrawIcon"
			t "-102_DragNDrop"
			t "-102_WinOpenComp"
			t "-102_WinOpenDrv"
			t "-102_WinMseCheck"
			t "-102_WinDataTab"

;*** Speicherverwaltung.
			t "-SYS_RAM_FREE"
			t "-SYS_RAM_ALLOC"
			t "-SYS_RAM_SHARED"

;*** Systemroutinen.
			t "-SYS_GTYPE"
			t "-SYS_CTYPE"

;******************************************************************************
			g BASE_DIR_DATA
;******************************************************************************

