﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AllocateBank		= $0611
:BASE_DIR_DATA		= $6400
:BackTempDrive		= $05ad
:BootDrive		= $064d
:BootMode		= $0650
:BootPart		= $064e
:BootSDir		= $0651
:BootType		= $064f
:DoneWithWM		= $08b1
:END_CODE_WM		= $2942
:FindBootDrive		= $04ba
:FindFreeBank		= $05e8
:FindLinkDrive		= $0551
:Flag_CacheModeOn	= $0661
:Flag_MyComputer	= $0660
:Flag_ViewALTitle	= $065f
:FontG3			= $0670
:FreeBank		= $062f
:G3_SCRN_STACK		= $065d
:G3_SYSTEM_BUF		= $065e
:GD_VEC2ENTRY		= $1533
:InitBalken		= $26fa
:InitForWM		= $089a
:LinkDrive		= $0657
:LinkMode		= $065a
:LinkPart		= $0658
:LinkSDir		= $065b
:LinkType		= $0659
:OpenBootDrive		= $0484
:OpenLinkDrive		= $051b
:OpenUserWinDrive	= $098a
:OpenWinDrive		= $0987
:PrintBalken		= $2710
:PrintCurBalken		= $272b
:SaveBootDrive		= $0400
:SaveTempDrive		= $0577
:SetPosBalken		= $270a
:TempBootDrive		= $0449
:TempDrive		= $0653
:TempLinkDrive		= $04e0
:TempPart		= $0654
:TempSDir		= $0655
:WINDOW_MAXIMIZED	= $234b
:WMODE_DRIVE		= $22d6
:WMODE_FMOVE		= $231c
:WMODE_PART		= $22dd
:WMODE_SDIR_S		= $22eb
:WMODE_SDIR_T		= $22e4
:WMODE_SLCT_H		= $2300
:WMODE_SLCT_L		= $22f9
:WMODE_VDEL		= $22f2
:WMODE_VICON		= $2307
:WMODE_VINFO		= $2315
:WMODE_VSIZE		= $230e
:WM_CALL_DRAW		= $0bec
:WM_CALL_DRAWROUT	= $0bd5
:WM_CALL_REDRAW		= $0c04
:WM_CALL_SSLCT		= $0c9d
:WM_CLEAR_SCREEN	= $0cb7
:WM_CLOSE_WINDOW	= $0914
:WM_CONVERT_CARDS	= $2041

:WM_CONVERT_PIXEL	= $0d60
:WM_COPY_WIN_DATA	= $1d12
:WM_COUNT_ICON_X	= $2326
:WM_COUNT_ICON_XY	= $2328
:WM_COUNT_ICON_Y	= $2327
:WM_ChkMouse		= $09d3
:WM_DATA_COLUMN		= $218d
:WM_DATA_CURENTRY	= $2189
:WM_DATA_GETFILE	= $21a7
:WM_DATA_GRID_X		= $218b
:WM_DATA_GRID_Y		= $218c
:WM_DATA_INFO		= $2191
:WM_DATA_MAXENTRY	= $2187
:WM_DATA_MOVEBAR	= $219b
:WM_DATA_OPTIONS	= $21a2
:WM_DATA_PRNFILE	= $21a5
:WM_DATA_RIGHTCLK	= $219c
:WM_DATA_ROW		= $218e
:WM_DATA_SIZE		= $2180
:WM_DATA_TITLE		= $218f
:WM_DATA_WINEXIT	= $219e
:WM_DATA_WININIT	= $2193
:WM_DATA_WINMOVE	= $2199
:WM_DATA_WINMSLCT	= $21a0
:WM_DATA_WINPRNT	= $2195
:WM_DATA_WINSLCT	= $2197
:WM_DATA_WINSSLCT	= $21a3
:WM_DATA_X0		= $2183
:WM_DATA_X1		= $2185
:WM_DATA_Y0		= $2181
:WM_DATA_Y1		= $2182
:WM_DRAW_ALL_WIN	= $0ba0
:WM_DRAW_FRAME		= $0e43
:WM_DRAW_INFO		= $0d9f
:WM_DRAW_MOVER		= $0e8d
:WM_DRAW_NO_TOP		= $0bb5
:WM_DRAW_SLCT_WIN	= $0cd7
:WM_DRAW_TITLE		= $0d6f
:WM_DRAW_TOP_WIN	= $0ba3
:WM_DRAW_USER_WIN	= $0dcf
:WM_FIND_WINDOW		= $0ac7
:WM_FUNC_POS		= $18cb
:WM_FUNC_SORT		= $18a2
:WM_GET_GRID_X		= $1ea1
:WM_GET_GRID_Y		= $1ecf
:WM_GET_ICON_X		= $1e63
:WM_GET_ICON_XY		= $1ed7
:WM_GET_ICON_Y		= $1ea9
:WM_GET_SLCT_AREA	= $2080
:WM_GET_SLCT_SIZE	= $1df8
:WM_GET_WIN_SIZE	= $1dfb
:WM_IS_WIN_FREE		= $0942
:WM_LINE_OUTPUT		= $100a
:WM_LOAD_AREA		= $2431
:WM_LOAD_BACKSCR	= $26ac
:WM_LOAD_SCREEN		= $23d9
:WM_LOAD_WIN_DATA	= $1d1d
:WM_MOVE_ENTRY_I	= $19d6
:WM_MOVE_ENTRY_T	= $1a36
:WM_MOVE_MODE		= $2325
:WM_MOVE_POS		= $1c01

:WM_NO_MARGIN		= $1f6b
:WM_NO_MOUSE_WIN	= $1f80
:WM_OPEN_WINDOW		= $08be
:WM_SAVE_AREA		= $242e
:WM_SAVE_BACKSCR	= $265e
:WM_SAVE_SCREEN		= $23d6
:WM_SAVE_WIN_DATA	= $1d3a
:WM_SET_ENTRY		= $1f9f
:WM_SET_MARGIN		= $1f3d
:WM_STD_OUTPUT		= $0ef5
:WM_TEST_ENTRY		= $10d7
:WM_TEST_MOVE		= $0aae
:WM_UPDATE		= $0b66
:WM_UPDATE_ALL		= $0b7e
:WM_USER_WINDOW		= $08cf
:WM_WCODE		= $2353
:WM_WIN2TOP		= $0964
:WM_WIN_BLOCKED		= $0af3
:WM_WIN_DATA_BUF	= $2180
:WM_WIN_MARGIN		= $1f3a
:WindowOpenCount	= $2352
:WindowStack		= $233b
:ramBaseWin0		= $0662
:ramBaseWin1		= $0663
:ramBaseWin2		= $0664
:ramBaseWin3		= $0665
:ramBaseWin4		= $0666
:ramBaseWin5		= $0667
:ramBaseWin6		= $0668
:ramCacheMode0		= $0669
:ramCacheMode1		= $066a
:ramCacheMode2		= $066b
:ramCacheMode3		= $066c
:ramCacheMode4		= $066d
:ramCacheMode5		= $066e
:ramCacheMode6		= $066f
