﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** MP3/GD3 Kernel-Variablen.
;(c) 1999 M. Kanet
;Stand: 25.08.1999
:BackScrPattern		= $9fcf
:C_Balken		= $9fea
:C_DBoxBack		= $9ff0
:C_DBoxDIcon		= $9ff1
:C_DBoxTitel		= $9fef
:C_FBoxBack		= $9ff3
:C_FBoxDIcon		= $9ff4
:C_FBoxFiles		= $9ff5
:C_FBoxTitel		= $9ff2
:C_FarbTab		= $9fea
:C_FarbTabEnd		= $a000
:C_GEOS_BACK		= $9ffd
:C_GEOS_FRAME		= $9ffe
:C_GEOS_MOUSE		= $9fff
:C_InputField		= $9ffb
:C_InputFieldOff	= $9ffc
:C_Mouse		= $9fee
:C_PullDMenu		= $9ffa
:C_Register		= $9feb
:C_RegisterBack		= $9fed
:C_RegisterOff		= $9fec
:C_WinBack		= $9ff7
:C_WinIcon		= $9ff9
:C_WinShadow		= $9ff8
:C_WinTitel		= $9ff6
:DB_GFileClass		= $9fd8
:DB_GFileType		= $9fd7
:DB_GetFileEntry	= $9fda
:DB_StdBoxSize		= $9fdb
:DeskTopName		= $c3cf
:Deutsch		= $0110
:DirectColor		= $c0e2
:DoneWithDskDvJob	= $9f4f
:DskDrvBaseH		= $9f82
:DskDrvBaseL		= $9f7e
:Englisch		= $0220
:Flag_ColorDBox		= $9fd1
:Flag_CrsrRepeat	= $9fce
:Flag_DBoxType		= $9fd5
:Flag_ExtRAMinUse	= $9fcb
:Flag_GetFiles		= $9fd6
:Flag_IconDown		= $9fd4
:Flag_IconMinX		= $9fd2
:Flag_IconMinY		= $9fd3
:Flag_LoadPrnt		= $9fae
:Flag_MenuStatus	= $9fe2
:Flag_Optimize		= $9fac
:Flag_ScrSaver		= $9fcd
:Flag_ScrSvCnt		= $9fcc
:Flag_SetColor		= $9fd0
:Flag_SetMLine		= $9fe1
:Flag_SplCurDok		= $9fc7
:Flag_SplMaxDok		= $9fc8
:Flag_SpoolADDR		= $9fc3
:Flag_SpoolCount	= $9fc6
:Flag_SpoolMaxB		= $9fc2
:Flag_SpoolMinB		= $9fc1
:Flag_Spooler		= $9fc0
:Flag_TaskAktiv		= $9fc9
:Flag_TaskBank		= $9fca
:GEOS_InitSystem	= $c0ee
:GEOS_RAM_TYP		= $9fa8
:GetBackScreen		= $c0e8
:HelpSystemActive	= $9f68
:HelpSystemBank		= $9f69
:HelpSystemDrive	= $9f6a
:HelpSystemFile		= $9f6c
:HelpSystemPage		= $9f7d
:HelpSystemPart		= $9f6b
:MP3_64K_DATA		= $9faa
:MP3_64K_DISK		= $9fab
:MP3_64K_SYSTEM		= $9fa9
:OS_VAR_MP		= $9f7e
:OS_VARS		= $8000
:PrntFileNameRAM	= $9faf
:PutKeyInBuffer		= $c0f1
:RamBankFirst		= $9fa6
:RamBankInUse		= $9f96
:RealDrvMode		= $9f92
:RealDrvType		= $9f8e
:RecColorBox		= $c0e5
:ResetScreen		= $c0eb
:SCPU_OptOff		= $c0fa
:SCPU_OptOn		= $c0f7
:SCPU_Pause		= $c0f4
:SCPU_SetOpt		= $c0fd
:doubleSideFlg		= $9f86
:drivePartData		= $9f8a
:i_ColorBox		= $c0df
:i_UserColor		= $c0dc
:millenium		= $9fad

;*** Modi für Dialogbox.
:DRIVE			= $07
:DUMMY			= $08
:DBUSRFILES		= $09
:DBSETCOL		= $0a
:DBSETDRVICON		= %01000000
:DBSELECTPART		= %10000000

;*** Einsprung in Laufwerkstreiber.
:GetBlock_dskBuf	= $903c
:PutBlock_dskBuf	= $903f
:DiskDrvType		= $904e
:DiskDrvVersion		= $904f
:OpenRootDir		= $9050
:OpenSubDir		= $9053
:GetBAMBlock		= $9056
:PutBAMBlock		= $9059
:GetPDirEntry		= $905c
:ReadPDirEntry		= $905f
:OpenPartition		= $9062
:SwapPartition		= $9065
:GetPTypeData		= $9068
:SendCommand		= $906b
:DiskDrvTypeCode	= $906e

;*** Startadressen Installationsroutinen.
:SIZE_DDRV_INIT		= $1000
:SIZE_DDRV_DATA		= $0d80
:BASE_DDRV_INIT		= APP_RAM
:BASE_DDRV_DATA		= BASE_DDRV_INIT + SIZE_DDRV_INIT

;*** Sprungtabelle für Installationsroutine.
:DDrv_TestMode		= BASE_DDRV_INIT +0
:DDrv_Install		= BASE_DDRV_INIT +3
:DDrv_DeInstall		= BASE_DDRV_INIT +6

;*** Definition der Laufwerkstypen.
:Drv1541		= $01
:Drv1571		= $02
:Drv1581		= $03
:DrvNative		= $04
:Drv81DOS		= $05
:DrvFDDOS		= $15
:DrvPCDOS		= $05
:DrvShadow1541		= $41
:DrvShadow1581		= $43
:DrvRAM1541		= $81
:DrvRAM1571		= $82
:DrvRAM1581		= $83
:DrvRAMNM		= $84
:DrvFD			= $10
:DrvFD2			= $13
:DrvFD4			= $13
:DrvHD			= $20
:DrvRAMLink		= $30
:DrvFD41		= $11
:DrvFD71		= $12
:DrvFD81		= $13
:DrvFDNM		= $14
:DrvHD41		= $21
:DrvHD71		= $22
:DrvHD81		= $23
:DrvHDNM		= $24
:DrvRL41		= $31
:DrvRL71		= $32
:DrvRL81		= $33
:DrvRLNM		= $34
:DrvCMD			= %00110000

;*** Laufwerksmodi für RealDrvMode.
:SET_MODE_PARTITION	= %10000000
:SET_MODE_SUBDIR	= %01000000

;*** Definition der RAM-Typen.
:RAM_SCPU		= $10				;SuperCPU/RAMCard ab ROM V1.4!
:RAM_BBG		= $20				;GeoRAM/BBGRAM.
:RAM_REU		= $40				;Commodore C=REU.
:RAM_RL			= $80				;RAMLink.

;*** Einsprünge im C64/C128-Kernal.
:IOINIT			= $fda3
:CINT			= $ff81				;Reset: Timer, IO, PAL/NTSC, Bildschirm.
:SETMSG			= $ff90				;Dateiparameter definieren.
:SECOND			= $ff93				;Sekundär-Adresse nach LISTEN senden.
:TKSA			= $ff96				;Sekundär-Adresse nach TALK senden.
:ACPTR			= $ffa5				;Byte-Eingabe vom IEC-Bus.
:CIOUT			= $ffa8				;Byte-Ausgabe auf IEC-Bus.
:UNTALK			= $ffab				;UNTALK-Signal auf IEC-Bus senden.
:UNLSN			= $ffae				;UNLISTEN-Signal auf IEC-Bus senden.
:LISTEN			= $ffb1				;LISTEN-Signal auf IEC-Bus senden.
:TALK			= $ffb4				;TALK-Signal auf IEC-Bus senden.
:SETLFS			= $ffba				;Dateiparameter setzen.
:SETNAM			= $ffbd				;Dateiname setzen.
:OPENCHN		= $ffc0				;Datei öffnen.
:CLOSE			= $ffc3				;Datei schließen.
:CHKIN			= $ffc6				;Eingabefile setzen.
:CKOUT			= $ffc9				;Ausgabefile setzen.
:CLRCHN			= $ffcc				;Standard-I/O setzen.
:BSOUT			= $ffd2				;Zeichen ausgeben.
:LOAD			= $ffd5				;Datei laden.
:GETIN			= $ffe4				;Tastatur-Eingabe.
:CLALL			= $ffe7				;Alle Kanäle schließen.

;*** Einsprünge im RAMLink-Kernal.
:EN_SET_REC		= $e0a9
:RL_HW_EN		= $e0b1
:SET_REC_IMG		= $fe03
:EXEC_REC_REU		= $fe06
:EXEC_REC_SEC		= $fe09
:RL_HW_DIS		= $fe0c
:RL_HW_DIS2		= $fe0f
:EXEC_REU_DIS		= $fe1e
:EXEC_SEC_DIS		= $fe21

;*** Fehlermeldungen.
:NO_ERROR		= $00
:NO_BLOCKS		= $01
:INV_TRACK		= $02
:INSUFF_SPACE		= $03
:FULL_DIRECTORY		= $04
:FILE_NOT_FOUND		= $05
:BAD_BAM		= $06
:UNOPENED_VLIR		= $07
:INV_RECORD		= $08
:OUT_OF_RECORDS		= $09
;STRUCT_MISMAT		= $0a
:BFR_OVERFLOW		= $0b
:CANCEL_ERR		= $0c
:DEV_NOT_FOUND		= $0d
;INCOMPATIBLE		= $0e
:HDR_NOT_THERE		= $20
:NO_SYNC		= $21
:DBLK_NOT_THERE		= $22
:DAT_CHKSUM_ERR		= $23
:WR_VER_ERR		= $25
:WR_PR_ON		= $26
:HDR_CHKSUM_ERR		= $27
:DSK_ID_MISMAT		= $29
:BYTE_DEC_ERR		= $2e
:NO_PARTITION		= $30
:PART_FORMAT_ERR	= $31
:ILLEGAL_PARTITION	= $32
:NO_PART_FD_ERR		= $33
:ILLEGAL_DEVICE		= $40
:NO_FREE_RAM		= $60
:DOS_MISMATCH		= $73

;*** GEOS-Dateytyp definieren.
:GATEWAY_DIR		= $10
:GATEWAY_DOC		= $11
:GEOSHELL_COM		= $15
:GEOFAX_PRINTER		= $16
