﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Zeiger auf Menü-Initialisierung setzen.
:OPEN_MENU_SETINT	LoadW	appMain,OPEN_MENU_INIT
			rts

;*** Erweiterung für ":DoPreviousMenu", damit auch Farben des
;    letzten Menüs richtig gesetzt werden.
:OPEN_PREV_MENU		lda	Rectangle +1
			sta	:2 +1
			lda	Rectangle +2
			sta	:3 +1

			lda	#< :1
			sta	Rectangle +1
			lda	#> :1
			sta	Rectangle +2

			jmp	DoPreviousMenu

::1			lda	C_PullDMenu
			jsr	DirectColor
::2			lda	#$ff
			sta	Rectangle +1
::3			ldx	#$ff
			stx	Rectangle +2
			jmp	CallRoutine

;*** Hintergrundfarbe für menü setzen.
:OPEN_MENU_SETCOL	ldy	#$05
::1			lda	(r0L),y
			sta	r2,y
			dey
			bpl	:1

			lda	C_PullDMenu
			jmp	DirectColor

;*** Vektoren zurücksetzen.
:CLOSE_MENU		lda	#$00
			sta	mouseFaultVec +0
			sta	mouseFaultVec +1
			sta	RecoverVector +0
			sta	RecoverVector +1

			lda	mouseOn
			and	#%10111111
			sta	mouseOn

			clc
			jsr	StartMouseMode
			jsr	WM_NO_MOUSE_WIN
			jmp	InitForWM

;*** GEOS-Menü öffnen.
:OPEN_MENU_GEOS		jsr	DoneWithWM		;Fenster-Abfrage abschalten.
			jsr	WM_NO_MARGIN		;Fenster-grenzen löschen.
			jsr	WM_SAVE_BACKSCR		;Aktuellen Bildschirm speichern.

			LoadW	r0,MENU_DATA_GEOS	;Zeiger auf Menü-Daten setzen.

;*** Farbe setzen und PullDown-Menü öffnen.
:OPEN_MENU		jsr	OPEN_MENU_SETCOL	;Farbe für Menü setzen.

			lda	#$01
			jsr	DoMenu			;Menü aktivieren.

;*** Menü-Vektoren setzen.
:OPEN_MENU_INIT		LoadW	appMain,DrawClock
			LoadW	RecoverVector,:1
			LoadW	mouseFaultVec,:2
			rts

;--- Ersatz für RecoverRectangle.
::1			LoadW	r10,G3_BACKSCR_BUF
			LoadW	r11,G3_BACKCOL_BUF
			lda	G3_SYSTEM_BUF
			jmp	WM_LOAD_AREA

;--- Mausabfrage.
::2			lda	faultData		;Hat Mauszeiger aktuelles Menü
			and	#%00001000		;verlassen ?
			bne	:3			;Ja, ein Menü zurück.
			ldx	#%10000000
			ldy	#%11000000
::3			txa				;Hat Mauszeiger obere/linke
			and	faultData		;Grenze verlassen ?
			bne	:4			;Ja, ein Menü zurück.
			tya

::4			lda	menuNumber		;Hauptmenü aktiv ?
			beq	:5			;Ja, übergehen.
			jsr	OPEN_PREV_MENU		;Ein Menü zurück.
			jmp	OPEN_MENU_INIT

::5			jsr	RecoverAllMenus		;Menüs löschen.
			jmp	CLOSE_MENU		;Vektoren zurücksetzen.

;*** Menü "GEOS" verlassen.
:EXIT_MENU_GEOS		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla

			cmp	#$00			; => Programme
			beq	:1
			cmp	#$01			; => Dokumente
			beq	:1
			cmp	#$03			; => Beenden
			beq	:2
::1			rts

;--- Programm beenden.
::2			ldy	G3_SYSTEM_BUF
			jsr	FreeBank
			ldy	G3_SCRN_STACK
			jsr	FreeBank

::3			lda	WindowStack
			beq	:4
			bmi	:4
			jsr	WM_CLOSE_WINDOW
			jmp	:3

::4			jmp	EnterDeskTop

;*** Menü "Einstellungen" aktivieren.
:OPEN_MENU_SETUP	LoadW	r0,MENU_DATA_SETUP
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Einstellungen" verlassen.
:EXIT_MENU_SETUP	pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla

			cmp	#$00			; => Setup starten.
			beq	:1
			cmp	#$01			; => AppLinks speichern.
			beq	:2
			rts

;--- Setup starten.
::1			jsr	OpenBootDrive
			LoadW	r6,SetupName
			LoadB	r0L,%00000000
			jmp	GetFile

;--- AppLinks speichern.
::2			jmp	LNK_SAVE_DATA

:SetupName		b "GD.SETUP",NULL

;*** Untermenüs aus Kontextmenü öffnen.
:OPEN_MENU_VOPT		ldx	#11
			b $2c
:OPEN_MENU_DISK		ldx	#12
			jsr	OPEN_MENU_GETDAT
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** PopUp-Menü öffnen.
:PM_OPEN_MENU		jsr	OPEN_MENU_GETDAT

			jsr	DoneWithWM
			jsr	WM_NO_MARGIN
			jsr	WM_SAVE_BACKSCR

			jmp	OPEN_MENU

;*** PopUp-Menü beenden.
:PopUpExit_000		ldx	#0
			b $2c
:PopUpExit_001		ldx	#1
			b $2c
:PopUpExit_002		ldx	#2
			b $2c
:PopUpExit_003		ldx	#3
			b $2c
:PopUpExit_004		ldx	#4
			b $2c
:PopUpExit_005		ldx	#5
			b $2c
:PopUpExit_006		ldx	#6
			b $2c
:PopUpExit_007		ldx	#7
			b $2c
:PopUpExit_008		ldx	#8
			b $2c
:PopUpExit_009		ldx	#9
			b $2c
:PopUpExit_010		ldx	#10
			b $2c
:PopUpExit_011		ldx	#11
			b $2c
:PopUpExit_012		ldx	#12
			pha
			txa
			pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla
			asl
			tax
			lda	PopUpRout_Vec +0,x
			sta	r0L
			lda	PopUpRout_Vec +1,x
			sta	r0H
			pla
			asl
			tay
			iny
			lda	(r0L),y
			tax
			dey
			lda	(r0L),y
			jmp	CallRoutine

;*** Informationen für aktuelles Menü einlesen.
;    r0   = Zeiger auf Menüdaten.
;    AKKU = Menü-Breite.
:OPEN_MENU_GETDAT	lda	PopUpData_Width,x
			sta	r5H
			txa
			asl
			tax
			lda	PopUpData_Vec +0,x
			sta	r0L
			lda	PopUpData_Vec +1,x
			sta	r0H

			ldy	#$06
			lda	(r0L),y
			and	#%00001111

			tay
			lda	#$02
::1			clc
			adc	#14
			dey
			bne	:1

			ora	#%00000111
			sta	r5L

			lda	#MAX_AREA_WIN_Y -1
			sec
			sbc	r5L
			cmp	mouseYPos
			bcc	:4
			lda	mouseYPos
::4			and	#%11111000
			sta	r2L
			clc
			adc	r5L
			sta	r2H

			sec
			lda	#< MAX_AREA_WIN_X -1
			sbc	r5H
			sta	r3L
			lda	#> MAX_AREA_WIN_X -1
			sbc	#$00
			sta	r3H

			CmpW	r3,mouseXPos
			bcc	:5
			MoveW	mouseXPos,r3

::5			clc
			lda	r3L
			and	#%11111000
			sta	r3L
			adc	r5H
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

			ldy	#$05
::6			lda	r2,y
			sta	(r0L),y
			dey
			bpl	:6
			rts

;*** PopUp-Fenster für DeskTop-Eigenschaften.
:PM_PROPERTIES		jsr	AL_FIND_ICON
			txa
			bne	:10

			MoveB	r13H,AL_CNT_FILE
			MoveW	r14 ,AL_VEC_FILE
			MoveW	r15 ,AL_VEC_ICON

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			cmp	#$00
			beq	:11
			cmp	#$80
			beq	:12
			cmp	#$fd
			beq	:15
			cmp	#$fe
			beq	:13
			cmp	#$ff
			beq	:14
			rts

;--- Rechter Mausklick auf DeskTop.
::10			ldx	#0
			b $2c

;--- Rechter Mausklick auf AppLink/Datei.
::11			ldx	#1
			b $2c

;--- Rechter Mausklick auf AppLink/Arbeitsplatz.
::12			ldx	#2
			b $2c

;--- Rechter Mausklick auf AppLink/Drucker.
::13			ldx	#3
			b $2c

;--- Rechter Mausklick auf AppLink/Laufwerk.
::14			ldx	#4
			b $2c

;--- Rechter Mausklick auf AppLink/Verzeichnis.
::15			ldx	#10
			jmp	PM_OPEN_MENU

;*** PopUp-Fenster für Datei-Eigenschaften.
:PM_FILE		jsr	WM_TEST_ENTRY
			bcc	:1

;--- Rechter Mausklick auf Datei-Icon.
			stx	r0L
			sty	r0H
			LoadW	r1,32
			ldx	#r0L
			ldy	#r1L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r0
			MoveW	r0,vecDirEntry

			ldx	#6
			jmp	PM_OPEN_MENU

;--- Rechter Mausklick auf Fenster-Hintergrund.
::1			ldx	WM_WCODE

			ldy	#" "
			lda	WMODE_VDEL,x
			beq	:2
			ldy	#"*"
::2			sty	PopUpText_105 +3

			ldy	#" "
			lda	WMODE_VSIZE,x
			bpl	:2a
			ldy	#"*"
::2a			sty	PopUpText_114 +3

			ldy	#" "
			lda	WMODE_VICON,x
			bpl	:2b
			ldy	#"*"
::2b			sty	PopUpText_115 +3

			ldy	#" "
			lda	WMODE_VINFO,x
			bpl	:2c
			ldy	#"*"
::2c			sty	PopUpText_116 +3

			ldy	#" "
			lda	WMODE_FMOVE,x
			bpl	:2d
			ldy	#"*"
::2d			sty	PopUpText_117 +3

			ldy	WMODE_DRIVE,x
			lda	RealDrvMode -8,y
			and	#SET_MODE_SUBDIR
			beq	:3

			ldx	#9
			b $2c
::3			ldx	#7
			jmp	PM_OPEN_MENU

;*** Rechter Mausklick auf Arbeitsplatz.
:PM_MYCOMP		jsr	WM_TEST_ENTRY
			bcs	:1
::0			rts

::1			stx	MyComputerEntry +0
			sty	MyComputerEntry +1
			cpx	#$04
			bcs	:2

			lda	driveType,x
			beq	:0

			ldx	#8
			b $2c
::2			ldx	#5
			jmp	PM_OPEN_MENU

:PF_OPEN_DRV_A		ldx	#$08
			b $2c
:PF_OPEN_DRV_B		ldx	#$09
			b $2c
:PF_OPEN_DRV_C		ldx	#$0a
			b $2c
:PF_OPEN_DRV_D		ldx	#$0b
			lda	driveType -8,x
			beq	:2
			txa
			jsr	SetDevice
			txa
			bne	:2

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:1
			jsr	OpenRootDir
			txa
			bne	:2

::1			lda	curDrive
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	WP_OPEN_DRIVE_NV
::2			rts

:PF_VIEW_ALTITLE	ldx	#" "
			lda	Flag_ViewALTitle
			eor	#$ff
			sta	Flag_ViewALTitle
			beq	:1
			ldx	#"*"
::1			stx	PopUpText_108 +3
			jsr	MNU_DRAW_DESKTOP
			jmp	WM_DRAW_ALL_WIN

;*** PopUp-Fenster für DeskTop-Eigenschaften.
:PF_OPEN_SDIR		MoveW	AL_VEC_FILE,r14
			jmp	AL_OPEN_SDIR

:PF_OPEN_FILE		MoveW	vecDirEntry,r0
			ldy	#$02
			lda	(r0L),y
			and	#%00001111
			cmp	#$06
			beq	:1
			jmp	StartFile_r0

::1			iny
			lda	(r0L),y
			sta	r1L
			iny
			lda	(r0L),y
			sta	r1H
			jsr	OpenSubDir
			jmp	WM_CALL_DRAW

:PF_DEL_FILE		MoveW	vecDirEntry,r0
			ldy	#$05
::1			lda	(r0L),y
			beq	:2
			cmp	#$a0
			beq	:2
			sta	dataFileName -5,y
			iny
			cpy	#$15
			bcc	:1
::2			lda	#$00
			sta	dataFileName -5,y
			LoadW	r0,dataFileName
			jsr	DeleteFile
			jmp	WM_CALL_DRAW

;*** PopUp-Fenster für Laufwerks-Eigenschaften.
:PF_OPEN_ROOT		ldx	WM_WCODE
			lda	#$01
			sta	WMODE_SDIR_T,x
			sta	WMODE_SDIR_S,x
			jsr	OpenRootDir
			jmp	WM_CALL_DRAW

:PF_OPEN_PARENT		jsr	OpenWinDrive

			lda	curDirHead+34		;Hauptverzeichnis ?
			beq	:1
			sta	r1L
			ldx	WM_WCODE
			sta	WMODE_SDIR_T,x
			lda	curDirHead+35
			sta	r1H
			sta	WMODE_SDIR_S,x
			jsr	OpenSubDir
			jmp	WM_CALL_DRAW
::1			rts

:PF_NEW_VIEW		jsr	OpenWinDrive

			ldx	WM_WCODE
			lda	WMODE_DRIVE,x
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	WP_OPEN_DRIVE_NV

:PF_OPEN_PART		lda	WM_WCODE
			jsr	WM_WIN2TOP
			jsr	OpenWinDrive

			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox

			ldx	curDrive
			lda	drivePartData -8,x
			ldx	WM_WCODE
			sta	WMODE_PART,x
			lda	curDirHead +32
			sta	WMODE_SDIR_T,x
			lda	curDirHead +33
			sta	WMODE_SDIR_S,x

			jsr	WM_DRAW_NO_TOP
			jmp	WM_CALL_DRAW

:PF_VIEW_DELFILE	ldy	WM_WCODE
			lda	WMODE_VDEL,y
			eor	#$ff
			sta	WMODE_VDEL,y
			jmp	WM_CALL_DRAW

:PF_VIEW_SIZE		ldy	WM_WCODE
			lda	WMODE_VSIZE,y
			eor	#$ff
			sta	WMODE_VSIZE,y
			jmp	WM_CALL_REDRAW

:PF_VIEW_ICONS		ldy	WM_WCODE
			lda	#$00
			sta	WMODE_VINFO,y
			lda	WMODE_VICON,y
			eor	#$ff
			sta	WMODE_VICON,y
			bne	:1

			jsr	WM_LOAD_WIN_DATA
			lda	#$00
			sta	WM_DATA_GRID_Y
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

::1			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$00
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

:PF_VIEW_DETAILS	ldy	WM_WCODE
			lda	#$ff
			sta	WMODE_VICON,y
			lda	WMODE_VINFO,y
			eor	#$ff
			sta	WMODE_VINFO,y
			bne	:1

			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$00
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

::1			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$01
			sta	WM_DATA_COLUMN
			lda	#$ee
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

:PF_VIEW_SLOWMOVE	ldy	WM_WCODE
			lda	WMODE_FMOVE,y
			eor	#$ff
			sta	WMODE_FMOVE,y
			rts

;*** PopUp-Fenster für Laufwerks-Eigenschaften.
:PF_OPEN_DRIVE		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	WP_OPEN_DRIVE

:PF_OPEN_NEWVIEW	lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	WP_OPEN_DRIVE_NV

:PF_SWAP_PART		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldy	MyComputerEntry +1
			bne	:1
			lda	MyComputerEntry +0
			cmp	#$04
			bcs	:1
			adc	#$08
			jsr	SetDevice
			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox
			jsr	WM_DRAW_NO_TOP
			jmp	WM_CALL_DRAW
::1			rts

:PF_CREATE_AL		jsr	AL_SET_INIT
			ldx	#r3L
			jsr	GetPtrCurDkNm
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE

			lda	curDirHead +34
			ora	curDirHead +35
			bne	:1
			jsr	AL_SET_TYPDV
			LoadW	r4,Icon_Drive +1
			ldx	#<Color_Drive
			ldy	#>Color_Drive
			jmp	:2

::1			jsr	AL_SET_TYPSD
			LoadW	r4,Icon_Map +1
			ldx	#<Color_SDir
			ldy	#>Color_SDir

::2			stx	:21a +1
			sty	:21a +2

			LoadB	r3L,1
			jsr	DrawSprite
			jsr	AL_SET_ICON

			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS

			ldx	#$00
			ldy	#LINK_DATA_COLOR
::21a			lda	Color_SDir,x
			sta	(r0L),y
			inx
			iny
			cpx	#$09
			bcc	:21a

			jsr	AL_SET_WMODE

			lda	curDirHead +32
			ldx	curDirHead +33
			jsr	AL_SET_SDIR
			jmp	AL_SET_END

:MENU_DATA_GEOS		b MENU_GEOS_Y0
			b MENU_GEOS_Y1
			w MENU_GEOS_X0
			w MENU_GEOS_X1

			b MAX_ENTRY_GEOS!VERTICAL

			w :1
			b MENU_ACTION
			w EXIT_MENU_GEOS

			w :2
			b MENU_ACTION
			w EXIT_MENU_GEOS

			w :3
			b DYN_SUB_MENU
			w OPEN_MENU_SETUP

			w :4
			b MENU_ACTION
			w EXIT_MENU_GEOS

::1			b PLAINTEXT,BOLDON
			b "Programme",NULL
::2			b "Dokumente",NULL
::3			b "Einstellungen",NULL
::4			b "Beenden",NULL

:MENU_DATA_SETUP	b MENU_SETUP_Y0
			b MENU_SETUP_Y1
			w MENU_SETUP_X0
			w MENU_SETUP_X1

			b MAX_ENTRY_SETUP!VERTICAL

			w :1
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w :2
			b MENU_ACTION
			w EXIT_MENU_SETUP

::1			b PLAINTEXT,BOLDON
			b "Systemsteuerung",NULL
::2			b "AppLinks speichern",NULL

;*** Texte für Menüs.
:PopUpText_001		b PLAINTEXT,BOLDON									,"Überlappend",NULL
:PopUpText_002		b PLAINTEXT,BOLDON									,"Nebeneinander",NULL
:PopUpText_003		b PLAINTEXT,BOLDON									,"Neue Ansicht",NULL
:PopUpText_100		b PLAINTEXT,BOLDON									,"Öffnen",NULL
:PopUpText_101		b PLAINTEXT,BOLDON									,"Partition wechseln",NULL
:PopUpText_102		b PLAINTEXT,BOLDON									,"Installieren",NULL
:PopUpText_103		b PLAINTEXT,BOLDON									,"Neuer Drucker",NULL
:PopUpText_104		b PLAINTEXT,BOLDON									,"Löschen",NULL
:PopUpText_105		b PLAINTEXT,BOLDON									,"( ) Gelöschte Dateien",NULL
:PopUpText_106		b PLAINTEXT,BOLDON									,"Hauptverzeichnis",NULL
:PopUpText_107		b PLAINTEXT,BOLDON									,"Verzeichnis zurück",NULL
:PopUpText_108		b PLAINTEXT,BOLDON									,"( ) Titel anzeigen",NULL
:PopUpText_109		b PLAINTEXT,BOLDON									,"Arbeitsplatz öffnen",NULL
:PopUpText_110		b PLAINTEXT,BOLDON									,"  >> Laufwerk A:",NULL
:PopUpText_111		b PLAINTEXT,BOLDON									,"  >> Laufwerk B:",NULL
:PopUpText_112		b PLAINTEXT,BOLDON									,"  >> Laufwerk C:",NULL
:PopUpText_113		b PLAINTEXT,BOLDON									,"  >> Laufwerk D:",NULL
:PopUpText_114		b PLAINTEXT,BOLDON									,"( ) Größe in KByte",NULL
:PopUpText_115		b PLAINTEXT,BOLDON									,"( ) Textmodus",NULL
:PopUpText_116		b PLAINTEXT,BOLDON									,"( ) Details zeigen",NULL
:PopUpText_117		b PLAINTEXT,BOLDON									,"( ) Anzeige bremsen",NULL
:PopUpText_200		b PLAINTEXT,BOLDON									,"AppLink löschen",NULL
:PopUpText_201		b PLAINTEXT,BOLDON									,"AppLink erstellen",NULL
:PopUpText_300		b PLAINTEXT,BOLDON									,">> Anzeige",NULL
:PopUpText_301		b PLAINTEXT,BOLDON									,">> Diskette",NULL
:PopUpText_302		b PLAINTEXT,BOLDON									,">> Datei",NULL
:PopUpText_400		b PLAINTEXT,BOLDON									,"Löschen",NULL
:PopUpText_401		b PLAINTEXT,BOLDON									,"Formatieren",NULL
:PopUpText_402		b PLAINTEXT,BOLDON									,"Aufräumen",NULL
:PopUpText_403		b PLAINTEXT,BOLDON									,"Eigenschaften",NULL

:PopUpData_Vec		w PopUpData_000
			w PopUpData_001
			w PopUpData_002
			w PopUpData_003
			w PopUpData_004
			w PopUpData_005
			w PopUpData_006
			w PopUpData_007
			w PopUpData_008
			w PopUpData_009
			w PopUpData_010
			w PopUpData_011
			w PopUpData_012

:PopUpRout_Vec		w PopUpRout_000
			w PopUpRout_001
			w PopUpRout_002
			w PopUpRout_003
			w PopUpRout_004
			w PopUpRout_005
			w PopUpRout_006
			w PopUpRout_007
			w PopUpRout_008
			w PopUpRout_009
			w PopUpRout_010
			w PopUpRout_011
			w PopUpRout_012

:PopUpData_Width	b $6f
			b $5f
			b $7f
			b $5f
			b $6f
			b $5f
			b $5f
			b $7f
			b $6f
			b $7f
			b $5f
			b $7f
			b $5f

:PopUpData_000		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_001
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_002
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_108
			b MENU_ACTION
			w PopUpExit_000

:PopUpRout_000		w WM_FUNC_SORT
			w WM_FUNC_POS
			w PF_VIEW_ALTITLE

:PopUpData_001		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_001

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_001

:PopUpRout_001		w AL_OPEN_ENTRY
			w AL_DEL_ENTRY

:PopUpData_002		b $00,$00
			w $0000,$0000

			b 5!VERTICAL

			w PopUpText_109
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_110
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_111
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_112
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_113
			b MENU_ACTION
			w PopUpExit_002

:PopUpRout_002		w AL_OPEN_ENTRY
			w PF_OPEN_DRV_A
			w PF_OPEN_DRV_B
			w PF_OPEN_DRV_C
			w PF_OPEN_DRV_D

:PopUpData_003		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_102
			b MENU_ACTION
			w PopUpExit_003

			w PopUpText_103
			b MENU_ACTION
			w PopUpExit_003

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_003

:PopUpRout_003		w AL_SET_PRINTER
			w AL_SWAP_PRINTER
			w AL_DEL_ENTRY

:PopUpData_004		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_004

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_004

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_004

:PopUpRout_004		w AL_OPEN_ENTRY
			w AL_SWAP_PART
			w AL_DEL_ENTRY

:PopUpData_005		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_103
			b MENU_ACTION
			w PopUpExit_005

			w PrntFileName
			b MENU_ACTION
			w PopUpExit_005

:PopUpRout_005		w ChangePrinter
			w $0000

:PopUpData_006		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_006

			w PopUpText_104
			b MENU_ACTION
			w PopUpExit_006

:PopUpRout_006		w PF_OPEN_FILE
			w PF_DEL_FILE

:PopUpData_007		b $00,$00
			w $0000,$0000

			b 5!VERTICAL

			w PopUpText_003
			b MENU_ACTION
			w PopUpExit_007

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_007

			w PopUpText_300
			b DYN_SUB_MENU
			w OPEN_MENU_VOPT

			w PopUpText_301
			b DYN_SUB_MENU
			w OPEN_MENU_DISK

			w PopUpText_201
			b MENU_ACTION
			w PopUpExit_007

:PopUpRout_007		w PF_NEW_VIEW
			w PF_OPEN_PART
			w $0000
			w $0000
			w PF_CREATE_AL

:PopUpData_008		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_008

			w PopUpText_003
			b MENU_ACTION
			w PopUpExit_008

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_008

:PopUpRout_008		w PF_OPEN_DRIVE
			w PF_OPEN_NEWVIEW
			w PF_SWAP_PART

:PopUpData_009		b $00,$00
			w $0000,$0000

			b 7!VERTICAL

			w PopUpText_003
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_106
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_107
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_300
			b DYN_SUB_MENU
			w OPEN_MENU_VOPT

			w PopUpText_301
			b DYN_SUB_MENU
			w OPEN_MENU_DISK

			w PopUpText_201
			b MENU_ACTION
			w PopUpExit_009

:PopUpRout_009		w PF_NEW_VIEW
			w PF_OPEN_PART
			w PF_OPEN_ROOT
			w PF_OPEN_PARENT
			w $0000
			w $0000
			w PF_CREATE_AL

:PopUpData_010		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_010

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_010

:PopUpRout_010		w PF_OPEN_SDIR
			w AL_DEL_ENTRY

:PopUpData_011		b $00,$00
			w $0000,$0000

			b 5!VERTICAL

			w PopUpText_105
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_114
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_115
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_116
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_117
			b MENU_ACTION
			w PopUpExit_011

:PopUpRout_011		w PF_VIEW_DELFILE
			w PF_VIEW_SIZE
			w PF_VIEW_ICONS
			w PF_VIEW_DETAILS
			w PF_VIEW_SLOWMOVE

:PopUpData_012		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PopUpText_400
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_401
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_402
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_403
			b MENU_ACTION
			w PopUpExit_012

:PopUpRout_012		w $0000
			w $0000
			w $0000
			w $0000
