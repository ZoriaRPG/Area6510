﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "o.WM"
			t "TopSym"
			t "TopSym/G3"
			t "TopMac/G3"
			t "SymbTab/WM"

			f DATA
			o $0400

.BASE_DIR_DATA = $6400

if 0=1
*************
Routine:		InitForWM
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Initialisiert die Mausabfrage für ":mouseVector".
*************
Routine:		DoneWithWM
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Deaktiviert die Mausabfrage über ":otherPressVec".
*************
Routine:		WM_IS_WIN_FREE
Parameter:		-
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y
Funktion:		Sucht nach einer freien Fenster-Nr.
*************
Routine:		WM_COPY_WIN_DATA
Parameter:		r0	Zeiger auf Datentabelle.
Rückgabe:		-
Verändert:		A,Y
Funktion:		Kopiert Datentabelle in Zwischenspeicher.
*************
Routine:		WM_LOAD_WIN_DATA
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Kopiert Fensterdaten in Zwischenspeicher.
*************
Routine:		WM_SAVE_WIN_DATA
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Kopiert Daten aus Zwischenspeicher in Tabelle mit Fensterdaten.
*************
Routine:		WM_WIN2TOP
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Setzt Fenster-Nr. (AKKU) an erste Stelle im Stack.
*************
Routine:		WM_DEF_STD_WSIZE
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Definiert Fenstergröße in Abhängigkeit von der aktuellen
			Fenster-Nr. Die einzelnen Fenster werden dabei "überlappend"
			angeordnet!
*************
Routine:		WM_GET_SLCT_SIZE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Fenstergröße für aktuelles Fenster.
*************
Routine:		WM_GET_WIN_SIZE
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Fenstergröße für gewähltes Fenster.
*************
Routine:		WM_SET_MAX_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert max. Fenstergröße.
*************
Routine:		WM_SET_STD_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Standard-Fenstergröße.
*************
Routine:		WM_CALL_DRAW
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet das angegebene Fenster mit Inhalt.
*************
Routine:		WM_CALL_REDRAW
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet das angegebene Fenster neu.
*************
Routine:		WM_CALLRIGHTCLK
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Klick mit rechter Maustaste auswerten.
*************
Routine:		WM_CALL_MOVE
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Fensterinhalt verschieben.
*************
Routine:		WM_CALL_EXEC
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Mausklick in Fenster auswerten.
*************
Routine:		WM_CALL_EXIT
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Fenster zum schließen vorbereiten.
*************
Routine:		WM_OPEN_WINDOW
Parameter:		r0	Zeiger auf Datentabelle.
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y,r0-r15
Funktion:		Öffnet und zeichnet ein neues Fenster mit Inhalt.
*************
Routine:		WM_USER_WINDOW
Parameter:		AKKU	Fenster-Nr.
			r0	Zeiger auf Datentabelle.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Schließt ein geöffneten Fensters.
*************
Routine:		WM_CLOSE_WINDOW
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y,r0-r15
Funktion:		Öffnet und zeichnet ein Fenster mit Inhalt innerhalb eines
			bereits geöffneten Fensters (z.B. Arbeitsplatz=>Laufwerk öffnen)
*************
endif

if 0=1
*************
Routine:		WM_CHK_MOUSE
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Wertet Mausklick auf DeskTop/Fenster/Icon aus.
*************
Routine:		WM_FIND_WINDOW
Parameter:		-
Rückgabe:		XREG	$00=Kein Fehler.
			YREG	Fenster-Nr.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Wertet Mausklick auf DeskTop/Fenster/Icon aus.
*************
Routine:		WM_WIN_BLOCKED
Parameter:		-
Rückgabe:		XREG	$00=Fenster nicht verdeckt.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Prüft ob aktuelles Fenster verdeckt ist.
*************
Routine:		WM_UPDATE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Aktualisiert und prüft das oberste Fenster.
*************
Routine:		WM_UPDATE_ALL
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Aktualisiert alle Fenster.
*************
Routine:		WM_DRAW_ALL_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet alle Fenster neu.
*************
Routine:		WM_DRAW_TOP_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet oberstes Fenster neu.
*************
Routine:		WM_DRAW_NO_TOP
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet bis auf oberstes alle Fenster neu.
*************
Routine:		WM_CLEAR_SCREEN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Löscht den aktuellen Bildschirm.
*************
Routine:		WM_DRAW_SLCT_WIN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet leeres Fenster mit System-Icons.
*************
Routine:		WM_DRAW_TITLE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Ausgabe der Titelzeile für aktuelles Fenster.
*************
Routine:		WM_DRAW_INFO
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Ausgabe der Infozeile für aktuelles Fenster.
*************
Routine:		WM_DRAW_USER_WIN
Parameter:		r2-r4	Fenstergröße.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet leeres Fenster ohne System-Icons.
*************
Routine:		WM_DRAW_FRAME
Parameter:		r2-r4	Fenstergröße.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet Gummi-Band um aktuelles Fenster.
*************
Routine:		WM_DRAW_MOVER
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet Scrollbalken.
*************
Routine:		WM_FIND_JOBS
Parameter:		r14	Zeiger auf Jobtabelle
			r15H	Anzahl Jobs
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Sucht Job für ausgewähltes System-Icon.
*************
Routine:		WM_DEF_AREA...
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Bereich für Icon definieren.
*************
Routine:		WM_FUNC...
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Funktion für Icon ausführen.
*************
Routine:		WM_GET_ICON_X
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_XAnzahl Icons in X-Richtung.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Zeile.
*************
Routine:		WM_GET_ICON_Y
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_YAnzahl Icons in Y-Richtung.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Spalte.
*************
Routine:		WM_GET_ICON_XY
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_XY									Anzahl Icons in Fenster.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Fenster.
*************
Routine:		WM_GET_GRID_X
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		AKKU	Breite für Spalte.
Verändert:		A,X,Y,r0-r15
Funktion:		Berechnet Breite einer Spalte.
*************
Routine:		WM_GET_GRID_Y
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		AKKU	Höhe für Zeile.
Verändert:		A,X,Y,r0-r15
Funktion:		Berechnet Höhe einer Zeile.
*************
endif

if 0=1
*************
Routine:		WM_TEST_WIN_POS
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_WIN_DATA_BUF
Verändert:		A,X,Y,r0
Funktion:		Prüft ob erster Eintrag in linker oberer Ecke innerhalb des
			gültigen Bereichs liegt.
*************
Routine:		WM_TEST_CUR_POS
Parameter:		r0	Zeiger auf aktuellen Eintrag.
Rückgabe:		r0
Verändert:		A,X,r0
Funktion:		Prüft ob aktueller Eintrag in linker oberer Ecke innerhalb des
			gültigen Bereichs liegt (Sub. von "WM_TEST_WIN_POS")
*************
Routine:		WM_WIN_MARGIN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
			Datei-Icons innerhalb des Fensterrahmens.
*************
Routine:		WM_SET_MARGIN
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
			Datei-Icons innerhalb des Fensterrahmens.
*************
Routine:		WM_NO_MARGIN
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Löscht Grenzen für Textausgabe.
*************
Routine:		WM_NO_MOUSE_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Löscht Grenzen für Mausbewegung.
*************
Routine:		WM_TEST_ENTRY
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Prüft ob ein Datei-Icon angeklickt wurde.
*************
Routine:		WM_SET_ENTRY
Parameter:		r0	Nr. des gesuchten Eintrages.
Rückgabe:		r1L,r1H	X(C) und Y(P)-Position für Icon.
			XREG	$00 = Icon ist sichtbar.
Verändert:		A,X,Y,r0-r4
Funktion:		Berechnet Position für Icon in Fenster.
*************
Routine:		WM_SAVE_SCREEN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r4
Funktion:		Speichert Bildschirm-Inhalt in ScreenBuffer.
*************
Routine:		WM_LOAD_SCREEN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r4
Funktion:		Lädt Bildschirm-Inhalt aus ScreenBuffer.
*************
Routine:		WM_CONVERT_CARDS
Parameter:		r1L,r1H,r2L,r2HWandelt Bitmap nach Pixel.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Berechnet Pixelgröße von Icon.
*************
Routine:		WM_GET_SLCT_AREA
Parameter:		-
Rückgabe:		r2L - r4Gewählte Rahmengröße
			r5L	SelectMode: $00=Ganz/$FF=Teilweise
Verändert:		A,X,Y,r2-r5L
Funktion:		Auswahlfenster erstellen.
*************
endif

			t "-GD_SysRout"

.Flag_ViewALTitle	b $00
.Flag_MyComputer	b $00
.Flag_CacheModeOn	b $ff

.ramBaseWin0		b $00
.ramBaseWin1		b $00
.ramBaseWin2		b $00
.ramBaseWin3		b $00
.ramBaseWin4		b $00
.ramBaseWin5		b $00
.ramBaseWin6		b $00

.ramCacheMode0		b $c0
.ramCacheMode1		b $c0
.ramCacheMode2		b $c0
.ramCacheMode3		b $c0
.ramCacheMode4		b $c0
.ramCacheMode5		b $c0
.ramCacheMode6		b $c0

;*** Spezieller Zeichensatz für GEOS_MegaPatch (7x8)
.FontG3			v 7,"fnt.GeoDesk"

;******************************************************************************
;Ab hier folgt Code für Fenstermanager
;******************************************************************************

;*** FensterManager initialisieren.
;    Der FM klinkt sich hierbei direkt in die Mausabfrage ein und
;    kehrt danach erst zur eigentlichen Mausroutine zurück.
.InitForWM		MoveW	otherPressVec,mouseOldVec
			LoadW	otherPressVec,WM_ChkMouse
			rts

;*** FensterManager abschalten.
.DoneWithWM		MoveW	mouseOldVec,otherPressVec
			rts

;*** Neues Fenster öffnen.
.WM_OPEN_WINDOW		jsr	WM_IS_WIN_FREE		;Freie Fenster-Nr. suchen.
			cpx	#NO_ERROR		;Fenster gefunden ?
			beq	:1			; => Ja, weiter...
			rts

::1			ldy	WindowOpenCount		;Fenster in Stackspeicher eintragen.
			sta	WindowStack,y
			inc	WindowOpenCount

;--- Freie Fenster-Nr. gefunden.
.WM_USER_WINDOW		pha				;Fensterdaten in
			sta	WM_WCODE		;Zwischenspeicher kopieren.
			jsr	WM_COPY_WIN_DATA
			pla
			tax
			lda	#$00			;Flag löschen: "Maximiert".
			sta	WINDOW_MAXIMIZED,x
			txa
			jsr	WM_WIN2TOP		;Fenster nach oben holen.

			lda	WM_DATA_SIZE		;Feste Fenstergröße ?
			bne	:1			; => Ja, weiter...

			lda	WM_DATA_Y0
			ora	WM_DATA_Y1		;Fenstergröße definiert ?
			bne	:1			; => Ja, weiter...
			jsr	WM_DEF_STD_WSIZE	;Standard-Größe festlegen.

::1			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			ldx	WM_WCODE
			lda	#$00
			sta	WMODE_VDEL,x
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x
			sta	WMODE_VICON,x
			sta	WMODE_VSIZE,x
			sta	WMODE_VINFO,x

			jsr	WM_CALL_DRAW		;Fenster ausgeben.

			lda	WM_WCODE
			ldx	#NO_ERROR
			rts

;*** Fenster schließen.
.WM_CLOSE_WINDOW	pha				;Vorbereiten: Fenster schließen.
			jsr	WM_CALL_EXIT
			pla
			sta	:tmp_WinNr		;Fenster-Nr. merken.

			ldx	#$00			;Fenster-Nr. in Stack suchen und
			ldy	#$00			;löschen. Stack komprimieren.
::1			lda	WindowStack ,x
			cmp	:tmp_WinNr
			beq	:2
			sta	WindowStack ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1
			cpy	#MAX_WINDOWS
			beq	:3

			dec	WindowOpenCount		;Anzahl offene Fenster -1.

			lda	#$ff
			sta	WindowStack ,y
			jmp	WM_DRAW_ALL_WIN		;Alle Fenster neu zeichnen.
::3			rts

::tmp_WinNr		b $00

;*** Freie Fenster-Nr. suchen.
.WM_IS_WIN_FREE		ldy	WindowOpenCount
			cpy	#MAX_WINDOWS
			bcs	:3

			lda	#$00
::1			ldx	#$00
::2			cmp	WindowStack,x
			bne	:4
			clc
			adc	#$01
			cmp	#MAX_WINDOWS
			bcc	:1
::3			ldx	#NO_MORE_WINDOWS
			rts

::4			inx
			cpx	#MAX_WINDOWS
			bcc	:2
			ldx	#NO_ERROR
			rts

;*** Window nach oben holen.
.WM_WIN2TOP		sta	:tmp_WinNr
			sta	WM_WCODE

			ldx	#MAX_WINDOWS -1
			ldy	#MAX_WINDOWS -1
::1			lda	WindowStack ,x
			cmp	:tmp_WinNr
			beq	:2
			sta	WindowStack ,y
			dey
			bmi	:3
::2			dex
			bpl	:1
			lda	:tmp_WinNr
			sta	WindowStack
::3			rts

::tmp_WinNr		b $00

.OpenWinDrive		lda	WM_WCODE
.OpenUserWinDrive	sta	:curDrive
			tax
			ldy	WMODE_DRIVE,x
			lda	driveType -8,y
			bne	:2
			ldx	#DEV_NOT_FOUND
::1			rts

::2			tya
			jsr	SetDevice
			txa
			bne	:1

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_PARTITION!SET_MODE_SUBDIR
			bne	:3
			jmp	OpenDisk

::3			pha
			and	#SET_MODE_PARTITION
			beq	:4

			ldx	:curDrive
			lda	WMODE_PART,x
			sta	r3H
			jsr	OpenPartition

::4			pla
			and	#SET_MODE_SUBDIR
			beq	:1

			ldx	:curDrive
			lda	WMODE_SDIR_T,x
			sta	r1L
			lda	WMODE_SDIR_S,x
			sta	r1H
			jmp	OpenSubDir

::curDrive		b $00

;*** Mausklick auswerten.
.WM_ChkMouse		lda	mouseData		;Mausbutton gedrückt ?
			bmi	:1			; => Nein, Ende...

			jsr	WM_FIND_WINDOW		;Fenster suchen.
			txa				;Wurde Fenster gefunden ?
			beq	:2			; => Ja, weiter...

			lda	mouseOldVec +0		;Mausabfrage extern fortsetzen.
			ldx	mouseOldVec +1
			jmp	CallRoutine
::1			rts

::2			lda	WindowStack		;Nr. des obersten Fensters einlesen
			sta	VWM_CUR_TOP_WIN		;und zwisichenspeichern.

;--- Fenster-Daten einlesen.
			lda	WindowStack,y		;Neue Fenster-Nr. einlesen und
			sta	WM_WCODE		;Fensterdaten kopieren.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_WCODE		;Desktop ?
			beq	:5			; => Ja, keine Fenster-Icons.

;--- Klick in Fenster ?
			php
			sei
			jsr	WM_GET_SLCT_SIZE
			AddVBW	8,r3
			SubVW	8,r4
			AddVB	8,r2L
			SubVB	8,r2H
			jsr	IsMseInRegion
			plp
			tax				;Mausklick innerhalb Dateifenster ?
			bne	:5			; => Ja, weiter...

;--- Klick auf Fenster-Icons ?
			jsr	WM_WIN_BLOCKED		;Ist Ziel-Fenster durch andere
			txa				;Fenster verdeckt ?
			pha
			lda	WM_WCODE
			jsr	WM_WIN2TOP		;Fenster umsortieren.
			pla				;Ist Fenster verdeckt ?
			beq	:3			; => Nein, weiter...

			jsr	WM_DRAW_TOP_WIN		;Ziel-Fenster neu zeichnen.

::3			LoadW	r14,WM_TAB_WINFUNC1
			LoadB	r15H,12
			jsr	WM_FIND_JOBS		;System-Icons auswerten.
			txa				;Wurde Fenster-Icon gewählt ?
			beq	:4			; => Ja, Ende...

			LoadW	r14,WM_TAB_WINFUNC2
			LoadB	r15H,3
			jsr	WM_FIND_JOBS		;Scroll-Icons auswerten.
			txa
			bne	:8
::4			rts

;--- Rechter Mausklick ?
::5			php
			sei
			ldx	CPU_DATA
			ldy	#$35
			sty	CPU_DATA
			lda	$dc01
			stx	CPU_DATA
			plp

			cmp	#207			;Rechter Mausklick ?
			beq	:7c			; => Nein, weiter...
			cmp	#253			;Rechter Mausklick ?
			beq	:7b			; => Nein, weiter...
			cmp	#254			;Rechter Mausklick ?
			bne	:6			; => Nein, weiter...
			jmp	WM_CALL_RIGHTCLK	;Mausklick auswerten.

;--- Klick auf DeskTop ?
::6			lda	WM_WCODE		;Klick auf DeskTop?
			bne	:7			; => Nein, weiter...
			jmp	WM_CALL_EXEC		;DeskTop-Klick auswerten.

;--- Wechsel zu anderem Fenster.
::7			jsr	WM_TEST_ENTRY		;Datei-Icon ausgewählt ?
			bcc	:7a			; => Nein, weiter...
			jsr	:8
			jmp	WM_CALL_EXEC		;Datei-Klick auswerten.

::7a			jsr	WM_TEST_MOVE
			bcc	:8
::7b			jsr	:8
			jmp	WM_CALL_MSLCT		;Datei-Klick auswerten.

::7c			jsr	:8
			jmp	WM_CALL_SSLCT

::8			lda	WM_WCODE		;Fenster umsortieren.
			jsr	WM_WIN2TOP
			jmp	WM_DRAW_TOP_WIN

;*** AppLink erzeugen.
.WM_TEST_MOVE		lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			sec
			rts

::1			clc
			rts

;*** Aktuelles Fenster suchen.
.WM_FIND_WINDOW		php				;IRQ sperren.
			sei

			lda	#$00			;Fenster-Nr. löschen.
			sta	:tmp_WinNr

::1			ldx	#WINDOW_NOT_FOUND	;Fehlermeldung vorbereiten.

			ldy	:tmp_WinNr
			cpy	#MAX_WINDOWS		;Alle Fenster durchsucht ?
			beq	:3			; => Ja, Ende...
			lda	WindowStack,y		;Fenster-Nr. einlesen.
			bmi	:3			; => Ende erreicht...

			jsr	WM_GET_WIN_SIZE		;Fenstergröße einlesen.
			jsr	IsMseInRegion		;Mausposition abfragen.
			tax				;Ist Maus in Bereich ?
			bne	:2			; => Ja, Fenster gefunden.

			inc	:tmp_WinNr		;Zeiger auf nächstes Fenster und
			jmp	:1			;weitersuchen.

::2			ldx	#NO_ERROR		;Flag für "Kein Fehler" setzen und
			ldy	:tmp_WinNr		;Fenster-Nr. einlesen.
::3			plp
			rts

::tmp_WinNr		b $00

;*** Prüfen ob Fenster verdeckt.
.WM_WIN_BLOCKED		lda	WM_WCODE		;Aktuelle Fenster-Nr. in
			sta	:tmp_WinNr		;Zwischenspeicher einlesen.
			jsr	:1			;Ist Fenster verdeckt ?

			txa				;Ergebnis zwischenspeichern.
			pha

			lda	:tmp_WinNr		;Fensterdaten für aktuelles
			sta	WM_WCODE		;Fenster wieder einlesen.
			jsr	WM_LOAD_WIN_DATA

			pla
			tax
			rts

;--- Ist Fenster verdeckt ?
::1			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			ldx	#$05			;Größe des aktuellen
::2			lda	WM_DATA_Y0,x		;Fensters einlesen.
			sta	r2L       ,x
			dex
			bpl	:2

::3			inx
			stx	:tmp_Vec2Stack		;Zeiger auf Fensterstack speichern.

			lda	WindowStack,x
			bmi	:4			; => Nicht definiert, weiter...
			beq	:4			; => Desktop-Fenster, weiter...
			cmp	:tmp_WinNr		;Aktuelles Fenster ?
			beq	:5			; => Ja, übergehen...

			sta	WM_WCODE		;Fensterdaten einlesen.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_Y0		;Fenstergröße testen.
			cmp	r2H
			bcs	:4
			lda	WM_DATA_Y1
			cmp	r2L
			bcc	:4
			CmpW	WM_DATA_X0,r4
			bcs	:4
			CmpW	WM_DATA_X1,r3
			bcc	:4

			ldx	#WINDOW_BLOCKED		;Fenster blockiert, Ende...
			rts

::4			ldx	:tmp_Vec2Stack
			cpx	#MAX_WINDOWS -1		;Alle Fenster überprüft ?
			bcc	:3			; => Nein, weiter...

::5			ldx	#NO_ERROR
			rts

::tmp_Vec2Stack		b $00
::tmp_WinNr		b $00

;*** Aktuelles Fenster aktualisieren.
.WM_UPDATE		jsr	WM_DRAW_NO_TOP
			lda	WindowStack
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_GET_ICON_XY
			jsr	WM_TEST_WIN_POS
			jsr	WM_CALL_REDRAW
			ldx	#NO_ERROR
			rts

;*** Alle Fenster (außer Desktop) aktualisieren.
.WM_UPDATE_ALL		lda	#$00			;Hintergrundbild und Verknüpfungen
			sta	WM_WCODE		;aus Speicher holen und neuzeichnen.
			jsr	WM_LOAD_SCREEN

			lda	#MAX_WINDOWS -1		;Alle Fenster neu zeichnen.
::1			pha
			tax
			lda	WindowStack,x
			beq	:2
			bmi	:2
			sta	WM_WCODE
			jsr	WM_CALL_DRAW
::2			pla
			sec
			sbc	#$01
			bpl	:1
			ldx	#NO_ERROR
			rts

;*** Alle Fenster neu zeichnen.
.WM_DRAW_ALL_WIN	jsr	WM_DRAW_NO_TOP

;*** Aktuelles Fenster neu zeichnen.
.WM_DRAW_TOP_WIN	lda	WindowStack
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_LOAD_SCREEN
			jsr	WM_DRAW_MOVER
			ldx	#NO_ERROR
			rts

;*** Zeichen alle Fenster bis auf oberstes Fenster neu.
.WM_DRAW_NO_TOP		lda	WM_WCODE
			pha

			lda	#MAX_WINDOWS -1
::1			pha
			tax
			lda	WindowStack,x
			bmi	:2
			sta	WM_WCODE
			jsr	WM_LOAD_SCREEN
::2			pla
			sec
			sbc	#$01
			bne	:1

			pla
			sta	WM_WCODE
			jmp	WM_LOAD_WIN_DATA

;*** Fenster neu zeichnen.
.WM_CALL_DRAWROUT	lda	WM_DATA_WINPRNT +0
			ldx	WM_DATA_WINPRNT +1
			ldy	WM_WCODE
			cmp	#$ff
			bne	:1
			cpx	#$ff
			bne	:1
			jmp	WM_STD_OUTPUT
::1			jmp	CallRoutine

;*** Fenster neu zeichnen.
.WM_CALL_DRAW		jsr	WM_LOAD_WIN_DATA
			jsr	WM_DRAW_SLCT_WIN
			lda	WM_DATA_WININIT +0
			ldx	WM_DATA_WININIT +1
			ldy	WM_WCODE
			jsr	CallRoutine
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW_W

;*** Fenster-Inhalt aktualisieren.
.WM_CALL_REDRAW		jsr	WM_DRAW_SLCT_WIN
:WM_CALL_REDRAW_W	jsr	WM_WIN_MARGIN
			jsr	WM_CALL_DRAWROUT
			jsr	WM_SAVE_WIN_DATA
			jsr	WM_DRAW_TITLE		;Titelzeile ausgeben.
			jsr	WM_DRAW_INFO		;Infozeile ausgeben.
			jsr	WM_DRAW_MOVER
			jmp	WM_SAVE_SCREEN

;*** Rechten Mausklick in Fenster auswerten.
:WM_CALL_RIGHTCLK	lda	mouseData		;Mausbutton gedrückt ?
			bpl	WM_CALL_RIGHTCLK	; => Nein, Ende...
			ClrB	pressFlag

			lda	#$3f			;Pause für Maustreiber mit
::1			pha				;Doppelklick auf rechter Taste.
			jsr	UpdateMouse
			pla
			sec
			sbc	#$01
			bpl	:1

::2			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:2			; => Nein, Ende...
			ClrB	pressFlag

			lda	WM_DATA_RIGHTCLK +0
			ldx	WM_DATA_RIGHTCLK +1
			jmp	CallRoutine

;*** Mausklick in Fenster auswerten.
;    Übergabe:		AKKU = Fenster-Nr.
:WM_CALL_EXEC		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINSLCT +0
			ldx	WM_DATA_WINSLCT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;*** Fensterinhalt verschieben.
:WM_CALL_MOVE		lda	WM_DATA_WINMOVE +0
			ldx	WM_DATA_WINMOVE +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	WM_MOVE_ENTRY_I

::51			cmp	#$ee
			bne	:52
			cpx	#$ee
			bne	:52
			jmp	WM_MOVE_ENTRY_T

::52			ldy	WM_WCODE
			jmp	CallRoutine

;*** Mausklick in Fenster auswerten.
;    Übergabe:		AKKU = Fenster-Nr.
:WM_CALL_EXIT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINEXIT +0
			ldx	WM_DATA_WINEXIT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;*** Mausklick in Fenster auswerten.
;    Übergabe:		AKKU = Fenster-Nr.
:WM_CALL_MSLCT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINMSLCT +0
			ldx	WM_DATA_WINMSLCT +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	WM_SELECT_FILES

::51			ldy	WM_WCODE
			jmp	CallRoutine

;*** Fenster neu zeichnen.
.WM_CALL_SSLCT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINSSLCT +0
			ldx	WM_DATA_WINSSLCT +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	GD_SLCT_FILE

::51			ldy	WM_WCODE
			jmp	CallRoutine

;*** Alle Fenster löschen.
.WM_CLEAR_SCREEN	LoadB	dispBufferOn,ST_WR_FORE

			lda	sysRAMFlg
			and	#%00001000		;Hintergrundbild aktiv ?
			beq	:1			; => Nein, weiter...
			jmp	GetBackScreen

::1			lda	BackScrPattern
			jsr	SetPattern
			jsr	WM_SET_MAX_WIN
			lda	C_GEOS_BACK
			jsr	DirectColor
			jmp	Rectangle

;*** Leeres Fenster zeichnen.
.WM_DRAW_SLCT_WIN	jsr	WM_GET_SLCT_SIZE
			jsr	WM_DRAW_USER_WIN

			jsr	WM_NO_MARGIN

;--- System-Icons zeichnen.
			LoadW	r14,WM_SYS1ICON_TAB
			LoadB	r15H,5
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Resize-Icons zeichnen.
			lda	WM_DATA_SIZE
			bne	:1

			LoadW	r14,WM_SYS2ICON_TAB
			LoadB	r15H,4
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Move-UP/DOWN-Icons zeichnen.
::1			lda	WM_DATA_MOVEBAR
			beq	:2

			LoadW	r14,WM_SYS3ICON_TAB
			LoadB	r15H,2
			MoveB	C_WinTitel,r13H
			jmp	WM_DRAW_ICON_TAB
::2			rts

;*** Fenster-Icons darstellen.
;    Übergabe:		r14  = Zeiger auf Icon-Tabelle.
;			r15H = Anzahl Icons.
;			r13H = Farbe
:WM_DRAW_ICON_TAB	ldy	#$03
			lda	(r14L),y
			tax
			dey
			lda	(r14L),y
			jsr	CallRoutine

			lda	r13H
			jsr	DirectColor
			jsr	WM_CONVERT_PIXEL

			ldy	#$00
			lda	(r14L),y
			sta	r0L
			iny
			lda	(r14L),y
			sta	r0H
			LoadB	r2L,Icon_MoveW
			LoadB	r2H,Icon_MoveH
			jsr	BitmapUp

			AddVBW	4,r14

			dec	r15H
			bne	WM_DRAW_ICON_TAB
			rts

;*** Rechteck-Koordinaten in ":BitmapUp"-Format konvertieren.
.WM_CONVERT_PIXEL	lda	r3H			;X-Koordinate in CARDs
			lsr				;umrechnen.
			lda	r3L
			ror
			lsr
			lsr
			sta	r1L

			lda	r2L			;Y-Koordinate übernehmen.
			sta	r1H
			rts

;*** Titelzeile ausgeben.
.WM_DRAW_TITLE		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2L			;X/Y-Koordinaten berechnen.
			clc
			adc	#$06
			sta	r1H

			lda	r3L
			clc
			adc	#$14
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$2c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_TITLE +0	;Titelzeile ausgeben.
			ldx	WM_DATA_TITLE +1
			jmp	CallRoutine

;*** Titelzeile ausgeben.
.WM_DRAW_INFO		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;X/Y-Koordinaten berechnen.
			sec
			sbc	#$02
			sta	r1H

			lda	r3L
			clc
			adc	#$0c
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$0c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_INFO +0		;Titelzeile ausgeben.
			ldx	WM_DATA_INFO +1
			jmp	CallRoutine

;*** Freies Fenster zeichnen.
;    Übergabe:		r2-r4 = Größe des Fensters.
.WM_DRAW_USER_WIN	PushB	r2L
			PushW	r3

			lda	C_WinBack
			jsr	DirectColor
			lda	#$00
			jsr	SetPattern
			jsr	Rectangle

			lda	r2L
			pha
			lda	r2H
			pha

;--- Kopfzeile zeichnen.
			lda	r2L
			clc
			adc	#$07
			sta	r2H
			lda	C_WinTitel
			jsr	DirectColor

;--- Fußzeile zeichnen.
			pla
			sta	r2H
			sec
			sbc	#$07
			sta	r2L

			lda	C_WinTitel
			jsr	DirectColor

			pla
			sta	r2L

			MoveB	r2H,r11L
			lda	#%11111111
			jsr	HorizontalLine

;--- Linken/recten Rand zeichnen.
			lda	r4H
			pha
			lda	r4L
			pha

			MoveW	r3,r4

			MoveB	r2L,r3L
			MoveB	r2H,r3H
			lda	#%11111111
			jsr	VerticalLine

			pla
			sta	r4L
			pla
			sta	r4H
			lda	#%11111111
			jsr	VerticalLine

			PopW	r3
			PopB	r2L
			rts

;*** Rahmen invertieren.
.WM_DRAW_FRAME		PushB	r2L
			PushB	r2H
			ldx	r2L
			inx
			stx	r2H
			jsr	InvertRectangle
			PopB	r2H
			tax
			dex
			stx	r2L
			jsr	InvertRectangle
			PopB	r2L

			PushW	r3
			PushW	r4
			MoveW	r3,r4
			jsr	InvertRectangle
			PopW	r4
			MoveW	r4,r3
			jsr	InvertRectangle
			PopW	r3
			rts

;*** Scroll-Balken zeichnen.
.WM_DRAW_MOVER		lda	WM_DATA_MOVEBAR
			bne	:1
			rts

::1			jsr	WM_GET_ICON_XY
			jsr	WM_GET_SLCT_SIZE

			ldx	#r4L
			ldy	#$03
			jsr	DShiftRight
			lda	r4L
			sta	:tmp_MovData +0
			lda	r2L
			clc
			adc	#$08
			sta	:tmp_MovData +1
			lda	r2H
			sec
			sbc	r2L
			sec
			sbc	#4*8
			sta	:tmp_MovData +2
			inc	:tmp_MovData +2

			lda	WM_DATA_MAXENTRY +0
			sta	:tmp_MovData +3
			lda	WM_DATA_MAXENTRY +1
			sta	:tmp_MovData +4

			lda	WM_DATA_CURENTRY +0
			sta	:tmp_MovData +7
			lda	WM_DATA_CURENTRY +1
			sta	:tmp_MovData +8

			lda	WM_COUNT_ICON_XY
			sta	:tmp_MovData +5
			lda	#$00
			sta	:tmp_MovData +6

			LoadW	r0,:tmp_MovData
			jsr	InitBalken
			jmp	PrintBalken

::tmp_MovData		b $00,$00,$00
			w $0000,$0000,$0000

.WM_STD_OUTPUT		jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_GETFILE +0
			ldx	WM_DATA_GETFILE +1
			jsr	CallRoutine
			jsr	WM_SAVE_WIN_DATA

			LoadW	r0,FontG3
			jsr	LoadCharSet
			LoadB	currentMode,$00

;.WM_NEW_OUTPUT
			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine
			txa
			beq	:4

			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			rts

:InitFPosData		jsr	WM_LOAD_WIN_DATA
			jsr	WM_WIN_MARGIN

			jsr	WM_GET_GRID_X
			sta	CurGridX
			jsr	WM_GET_GRID_Y
			sta	CurGridY

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	MinXPos
			sta	CurXPos

			lda	rightMargin +1
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowTop
			clc
			adc	#$08
			sta	MinYPos
			sta	CurYPos

			lda	windowBottom
			sta	MaxYPos

			lda	WM_DATA_CURENTRY +0
			sta	CurEntry +0
			lda	WM_DATA_CURENTRY +1
			sta	CurEntry +1

			lda	#$00
			sta	CountX
			sta	CountY
			rts

.WM_LINE_OUTPUT		lda	r0L
			sta	CurEntry +0
			lda	r0H
			sta	CurEntry +1

			lda	r1L
			sta	CurXPos
			lda	r1H
			sta	CurYPos

			jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_GETFILE +0
			ldx	WM_DATA_GETFILE +1
			jsr	CallRoutine

			jsr	WM_SAVE_WIN_DATA
			jsr	WM_WIN_MARGIN

			LoadW	r0,FontG3
			jsr	LoadCharSet
			LoadB	currentMode,$00

			jsr	WM_GET_GRID_X
			sta	CurGridX
			jsr	WM_GET_GRID_Y
			sta	CurGridY

			lda	rightMargin +1
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowBottom
			sta	MaxYPos

			lda	#$00
			sta	CountX

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3
			rts

::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine
			txa
			beq	:4

			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:6

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:6
::5			jmp	:1

::6			rts

;*** Icon-Bereich ermitteln.
.WM_TEST_ENTRY		php
			sei

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			plp
			clc
			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			jsr	GD_SET_ENTRY2
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS
			jsr	IsMseInRegion
			tay
			pla
			tax
			tya
			beq	:4a

			ldx	CurEntry +0
			ldy	CurEntry +1
			plp
			sec
			rts

::4a			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			plp
			clc
			rts

:GD_SET_ENTRY2		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L
			LoadB	r2L,$03
			LoadB	r2H,$15

			ldx	#$ff
			rts

::11			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$ff
			rts

::12			lda	r2L
			sec
			sbc	r1L
			sta	r2L

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$7f
			rts

:GD_SLCT_FILE		php
			sei

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			plp
			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			jsr	GD_SET_ENTRY
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS
			jsr	IsMseInRegion
			tay
			pla
			tax
			tya
			beq	:4a

			plp
			MoveW	CurEntry,r0
			jsr	GD_INVERT_FILE
			jsr	WM_SAVE_SCREEN
			jmp	WM_SAVE_WIN_DATA

::4a			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			plp
			rts

:WM_SELECT_FILES	jsr	WM_LOAD_WIN_DATA

			jsr	WM_GET_SLCT_AREA

			MoveB	r2L,SlctY0
			MoveB	r2H,SlctY1
			MoveW	r3 ,SlctX0
			MoveW	r4 ,SlctX1
			MoveB	r5L,SlctMode

			jsr	OpenWinDrive

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			jmp	:6

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			jsr	GD_SET_ENTRY
			txa
			beq	:4

			pha
			jsr	GD_TEST_ENTRY
			pla
			bcc	:3a

			pha
			MoveW	CurEntry,r0
			jsr	GD_INVERT_FILE
			pla

::3a			inc	CurEntry +0
			bne	:3b
			inc	CurEntry +1

::3b			tax

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			jsr	WM_SAVE_SCREEN
			jmp	WM_SAVE_WIN_DATA

;*** Variablen für Dateiauswahl/Dateianzeige.
:CurXPos		b $00
:CurYPos		b $00
:MinXPos		b $00
:MaxXPos		b $00
:MinYPos		b $00
:MaxYPos		b $00
:CurEntry		w $0000
:CurGridX		b $00
:CurGridY		b $00
:CountX			b $00
:CountY			b $00
:SlctY0			b $00
:SlctY1			b $00
:SlctX0			w $0000
:SlctX1			w $0000
:SlctMode		b $00

:GD_SET_ENTRY		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L
			LoadB	r2L,$03
			LoadB	r2H,$15

			ldx	#$ff
			rts

::11			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$ff
			rts

::12			lda	r2L
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$7f
			rts

:GD_TEST_ENTRY		jsr	WM_CONVERT_CARDS

::101			CmpB	r2L,SlctY1
			bcs	:201
			CmpB	r2H,SlctY0
			bcc	:201

			bit	SlctMode
			bmi	:103

			CmpB	r2L,SlctY0
			bcc	:201
			CmpB	r2H,SlctY1
			bcs	:201

::103			CmpW	r3 ,SlctX1
			bcs	:201
			CmpW	r4 ,SlctX0
			bcc	:201

			bit	SlctMode
			bmi	:105

			CmpW	r3 ,SlctX0
			bcc	:201
			CmpW	r4 ,SlctX1
			bcs	:201

;--- Datei gewählt.
::105			sec
			rts

;--- Nicht gewählt.
::201			clc
			rts

:GD_INVERT_FILE		CmpW	r4,rightMargin
			bcc	:52a
			MoveW	rightMargin,r4

::52a			CmpB	r2H,windowBottom
			bcc	:52b
			MoveB	windowBottom,r2H

::52b			jsr	InvertRectangle

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			bpl	:3b
			MoveW	r0,r15
			jsr	GD_VEC2ENTRY

			LoadW	r0 ,r4
			MoveW	r15,r1
			LoadW	r2 ,2
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	FetchRAM

			ldx	WM_WCODE
			ldy	WMODE_SLCT_L,x
			lda	WMODE_SLCT_H,x
			tax

			lda	r4L
			ora	r4H
			beq	:66
			ldx	#$ff
			ldy	#$ff
::66			iny
			bne	:67
			inx
::67			sty	r4L
			stx	r4H

			jsr	StashRAM
			jmp	:3

;--- Kein RAM-Cache.
::3b			LoadW	r5L,$20
			ldx	#r0L
			ldy	#r5L
			jsr	BMult
			AddVW	BASE_DIR_DATA,r0

			ldy	#$00
			lda	(r0L),y
			sta	r4L
			iny
			lda	(r0L),y
			sta	r4H

			ldx	WM_WCODE
			ldy	WMODE_SLCT_L,x
			lda	WMODE_SLCT_H,x
			tax

			lda	r4L
			ora	r4H
			beq	:77
			ldx	#$ff
			ldy	#$ff
::77			iny
			bne	:78
			inx
::78			sty	r4L
			stx	r4H

			ldy	#$00
			lda	r4L
			sta	(r0L),y
			iny
			lda	r4H
			sta	(r0L),y

::3			lda	r4L
			ora	r4H
			bne	:9a

			ldx	WM_WCODE
			lda	WMODE_SLCT_L,x
			bne	:9b
			dec	WMODE_SLCT_H,x
::9b			dec	WMODE_SLCT_L,x
			rts

::9a			ldx	WM_WCODE
			inc	WMODE_SLCT_L,x
			bne	:3a
			inc	WMODE_SLCT_H,x
::3a			rts

.GD_VEC2ENTRY		ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			bne	:y0
			clc
			rts

::y0			ldx	#$20
			cmp	#$80
			beq	:z0
			ldx	#$40
			cmp	#$40
			beq	:z0
			ldx	#$60
::z0			stx	r14L
			ldx	#r15L
			ldy	#r14L
			jsr	BMult
			AddVW	$0300,r15
			sec
			rts

;*** Aktuellen Job ermitteln.
:WM_FIND_JOBS		php
			sei

			lda	#$00
			sta	r15L
::1			lda	r15L
			cmp	r15H
			beq	:4
			asl
			asl
			tay
			ldx	#$00
::2			lda	(r14L),y
			sta	r0L   ,x
			iny
			inx
			cpx	#$04
			bcc	:2

			lda	r0L
			ldx	r0H
			jsr	CallRoutine
			jsr	IsMseInRegion
			tax
			beq	:3

			plp

			lda	r1L
			ldx	r1H
			jmp	CallRoutine

::3			inc	r15L
			jmp	:1

::4			ldx	#JOB_NOT_FOUND
::5			plp
			rts

;*** Bereiche für Fenster-Icons berechnen.
:WM_DEF_AREA_CL		lda	#$00
			b $2c
:WM_DEF_AREA_DN		lda	#$03
			b $2c
:WM_DEF_AREA_STD	lda	#$06
			b $2c
:WM_DEF_AREA_MN		lda	#$09
			b $2c
:WM_DEF_AREA_MX		lda	#$0c
			b $2c
:WM_DEF_AREA_UL		lda	#$0f
			b $2c
:WM_DEF_AREA_UR		lda	#$12
			b $2c
:WM_DEF_AREA_DL		lda	#$15
			b $2c
:WM_DEF_AREA_DR		lda	#$18
			b $2c
:WM_DEF_AREA_WUP	lda	#$1b
			b $2c
:WM_DEF_AREA_WDN	lda	#$1e
:WM_DEF_AREA		pha
			jsr	WM_GET_SLCT_SIZE
			pla
			tay

			lda	WM_DEFICON_TAB +0,y
			bmi	:2

			lda	r3L
			clc
			adc	WM_DEFICON_TAB +1,y
			sta	r3L
			bcc	:1
			inc	r3H
::1			jmp	:3

::2			lda	r4L
			sec
			sbc	WM_DEFICON_TAB +1,y
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H

::3			lda	r3L
			clc
			adc	#$07
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

::4			lda	WM_DEFICON_TAB +0,y
			and	#%01000000
			bne	:5

			lda	r2L
			clc
			adc	WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

::5			lda	r2H
			sec
			sbc	WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

;*** Bereich für Klick auf Titelzeile berechnen.
:WM_DEF_AREA_MV		jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$07
			sta	r2H

			lda	r3L
			clc
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H

			lda	r4L
			sec
			sbc	#$28
			sta	r4L
			lda	r4H
			sbc	#$00
			sta	r4H
			rts

;*** Bereich für Klick auf Titelzeile berechnen.
:WM_DEF_AREA_BAR	jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$08
			sta	r2L

			lda	r2H
			sec
			sbc	#$18
			sta	r2H

			lda	r4L
			sec
			sbc	#$07
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H
			rts

;*** Funktion: Gewähltes Fenster schließen.
:WM_FUNC_CLOSE		lda	WM_WCODE
			jsr	WM_CLOSE_WINDOW
			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster maximieren.
:WM_FUNC_MAX		ldx	WM_WCODE
			lda	WINDOW_MAXIMIZED,x
			bne	:1
			dec	WINDOW_MAXIMIZED,x
			jsr	WM_UPDATE
::1			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster zurücksetzen.
:WM_FUNC_MIN		ldx	WM_WCODE
			lda	WINDOW_MAXIMIZED,x
			beq	:1
			inc	WINDOW_MAXIMIZED,x
			jsr	WM_UPDATE
::1			ldx	#NO_ERROR
			rts

;*** Funktion: Standardgröße für Fenster setzen.
:WM_FUNC_STD		ldx	WM_WCODE
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_DEF_STD_WSIZE
			jsr	WM_SAVE_WIN_DATA
			jsr	WM_UPDATE
			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster nach unten verschieben.
:WM_FUNC_DOWN		lda	WindowOpenCount
			cmp	#$03
			bcc	:4

			ldx	#$00
			ldy	#$00
::1			lda	WindowStack ,x
			beq	:3
			cmp	WM_WCODE
			beq	:2
			sta	WindowStack ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1

::3			lda	WM_WCODE
			sta	WindowStack ,y
			jsr	WM_DRAW_ALL_WIN
::4			ldx	#NO_ERROR
			rts

;*** Fenstergröße ändern ?
:WM_FUNC_SIZE_UL	lda	#<WM_FJOB_SIZE_UL
			ldx	#>WM_FJOB_SIZE_UL
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_UR	lda	#<WM_FJOB_SIZE_UR
			ldx	#>WM_FJOB_SIZE_UR
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DL	lda	#<WM_FJOB_SIZE_DL
			ldx	#>WM_FJOB_SIZE_DL
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DR	lda	#<WM_FJOB_SIZE_DR
			ldx	#>WM_FJOB_SIZE_DR

;*** Fenstergröße ändern.
:WM_FUNC_RESIZE		jsr	WM_EDIT_WIN
			ldx	WM_WCODE
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_SET_WIN_SIZE
			jmp	WM_UPDATE

;*** Fenster nach links/oben vergrößern.
:WM_FJOB_SIZE_UL	jsr	WM_FJOB_TEST_Y0
			jsr	WM_FJOB_TEST_X0

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2L
			rts

;*** Fenster nach rechts/oben vergrößern.
:WM_FJOB_SIZE_UR	jsr	WM_FJOB_TEST_Y0
			jsr	WM_FJOB_TEST_X1

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2L
			rts

;*** Fenster nach links/unten vergrößern.
:WM_FJOB_SIZE_DL	jsr	WM_FJOB_TEST_Y1
			jsr	WM_FJOB_TEST_X0

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2H
			rts

;*** Fenster nach rechts/unten vergrößern.
:WM_FJOB_SIZE_DR	jsr	WM_FJOB_TEST_Y1
			jsr	WM_FJOB_TEST_X1

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2H
			rts

;*** Verkleinern nach oben möglich ?
:WM_FJOB_TEST_Y0	lda	r2H
			sec
			sbc	mouseYPos
			cmp	#MIN_SIZE_WIN_Y
			bcs	:1
			MoveB	r2L,mouseYPos
::1			rts

;*** Verkleinern nach unten möglich ?
:WM_FJOB_TEST_Y1	lda	mouseYPos
			sec
			sbc	r2L
			cmp	#MIN_SIZE_WIN_Y
			bcs	:1
			MoveB	r2H,mouseYPos
::1			rts

;*** Verkleinern nach rechts möglich ?
:WM_FJOB_TEST_X0	lda	r4L
			sec
			sbc	mouseXPos +0
			tax
			lda	r4H
			sbc	mouseXPos +1
			bne	:1
			cpx	#MIN_SIZE_WIN_X
			bcs	:1
			MoveW	r3 ,mouseXPos
::1			rts

;*** Verkleinern nach links möglich ?
:WM_FJOB_TEST_X1	lda	mouseXPos +0
			sec
			sbc	r3L
			tax
			lda	mouseXPos +1
			sbc	r3H
			bne	:1
			cpx	#MIN_SIZE_WIN_X
			bcs	:1
			MoveW	r4 ,mouseXPos
::1			rts

;*** Fenster verschieben.
:WM_FUNC_SIZE_MV	jsr	WM_GET_SLCT_SIZE

			MoveB	r2L,mouseYPos
			MoveW	r3 ,mouseXPos

			lda	r4L
			sec
			sbc	r3L
			sta	r13L
			lda	r4H
			sbc	r3H
			sta	r13H

			lda	r2H
			sec
			sbc	r2L
			sta	r14L

			lda	#<WM_FJOB_TEST_MOV
			ldx	#>WM_FJOB_TEST_MOV
			jsr	WM_EDIT_WIN

			jsr	WM_SET_CARD_XY

			PushB	r2L
			PushW	r3
			PushW	r13
			PushB	r14L

			lda	r2L
			lsr
			lsr
			lsr
			pha
			lda	r3H
			lsr
			lda	r3L
			ror
			lsr
			lsr
			pha
			jsr	WM_DRAW_NO_TOP
			pla
			sta	DB_DELTA_X
			pla
			sta	DB_DELTA_Y

			jsr	WM_LOAD_SCREEN

			PopB	r14L
			PopW	r13
			PopW	r3
			PopB	r2L

			MoveW	r3  ,r4
			AddW	r13 ,r4
			MoveB	r2L ,r2H
			AddB	r14L,r2H
			jsr	WM_SET_WIN_SIZE
			jsr	WM_DRAW_MOVER
			jmp	WM_SAVE_SCREEN

;*** Verschiebung möglich ?
:WM_FJOB_TEST_MOV	jsr	WM_FJOB_TEST_MX
			jsr	WM_FJOB_TEST_MY

			lda	mouseXPos +0
			sta	r3L
			clc
			adc	r13L
			sta	r4L
			lda	mouseXPos +1
			sta	r3H
			adc	r13H
			sta	r4H

			lda	mouseYPos
			sta	r2L
			clc
			adc	r14L
			sta	r2H
			rts

;*** Verschiebung in Y-Richtung möglich ?
:WM_FJOB_TEST_MY	lda	mouseYPos
			clc
			adc	r14L
			cmp	#MAX_AREA_WIN_Y
			bcc	:1
			MoveB	r2L,mouseYPos
::1			rts

;*** Verschiebung in X-Richtung möglich ?
:WM_FJOB_TEST_MX	lda	mouseXPos +0
			clc
			adc	r13L
			tax
			lda	mouseXPos +1
			adc	r13H
			cmp	#> MAX_AREA_WIN_X
			bne	:1
			cpx	#< MAX_AREA_WIN_X
			bcc	:1
			MoveW	r3,mouseXPos
::1			rts

;*** Gewähltes Fenster vergrößern/verschieben.
;    Übergabe:		AKKU/XREG = Zeiger auf Test-Routine.
:WM_EDIT_WIN		sta	:3 +1
			stx	:3 +2

			php
			cli

			lda	WindowStack
			jsr	WM_GET_WIN_SIZE

			LoadB	mouseBottom,$b7

::1			jsr	WM_DRAW_FRAME

::2			lda	mouseData
			bmi	:4
			lda	inputData
			bmi	:2

			jsr	WM_DRAW_FRAME
::3			jsr	$ffff
			jmp	:1

::4			jsr	WM_DRAW_FRAME

			LoadB	pressFlag,$00
			LoadB	mouseBottom,$c7
			plp
			rts

;*** Funktion: Fenster sortieren.
.WM_FUNC_SORT		lda	#$01
			sta	r1L
::1			lda	r1L
			sta	WM_WCODE
			tax
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_DEF_STD_WSIZE
			jsr	WM_SAVE_WIN_DATA
			lda	WM_WCODE
			jsr	WM_WIN2TOP

			inc	r1L
			lda	r1L
			cmp	#MAX_WINDOWS
			bcc	:1

			jmp	WM_UPDATE_ALL

;*** Funktion: Fenster anordnen.
.WM_FUNC_POS		lda	#MIN_AREA_WIN_Y
			sta	r2L
			lda	#MAX_AREA_WIN_Y -1
			sta	r2H

			ldx	WindowOpenCount
			dex
			cpx	#$03 +1
			bcc	:2
			bne	:1
			ldx	#$02
			b $2c
::1			ldx	#$03
			lda	#$57
			sta	r2H

::2			stx	r1H
			dex
			txa
			asl
			tax
			lda	:WIDTH +0,x
			sta	r5L
			lda	:WIDTH +1,x
			sta	r5H

			lda	#$00
			sta	r1L

::3			LoadW	r3,4*8

::4			ldx	r1L
			lda	WindowStack,x
			beq	:5
			bmi	:5
			sta	WM_WCODE
			tax
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			lda	r2L
			sta	WM_DATA_Y0
			lda	r2H
			sta	WM_DATA_Y1
			lda	r3L
			sta	WM_DATA_X0 +0
			lda	r3H
			sta	WM_DATA_X0 +1
			clc
			lda	r3L
			adc	r5L
			sta	r3L
			sta	WM_DATA_X1 +0
			lda	r3H
			adc	r5H
			sta	r3H
			sta	WM_DATA_X1 +1
			jsr	WM_SAVE_WIN_DATA

			inc	r3L
			bne	:5
			inc	r3H
::5			inc	r1L
			lda	r1L
			cmp	r1H
			bne	:6

			lda	#$58
			sta	r2L
			lda	#MAX_AREA_WIN_Y -9
			sta	r2H
			jmp	:3

::6			cmp	#MAX_WINDOWS
			bcc	:4

			jmp	WM_UPDATE_ALL

::WIDTH			w 36*8-1
			w 18*8-1
			w 12*8-1

;*** Fensterinhalt nach oben/unten verschieben.
:WM_FUNC_MOVER		lda	#$7f
			b $2c
:WM_FUNC_MOVE_UP	lda	#$00
			b $2c
:WM_FUNC_MOVE_DN	lda	#$ff
			sta	WM_MOVE_MODE

::1			jsr	WM_LOAD_WIN_DATA
			PushW	WM_DATA_CURENTRY

			jsr	WM_CALL_MOVE

			PopW	r0
			txa
			bne	:1a

			CmpW	WM_DATA_CURENTRY,r0
			beq	:2

			jsr	WM_WIN_MARGIN

			MoveB	windowTop   ,r2L
			MoveB	windowBottom,r2H
			MoveW	leftMargin  ,r3
			MoveW	rightMargin ,r4

			lda	#$00
			jsr	SetPattern
			jsr	Rectangle

			jsr	WM_CALL_DRAWROUT

::1a			jsr	WM_DRAW_MOVER

			ldy	WM_WCODE
			lda	WMODE_FMOVE,y
			bpl	:1b
			jsr	SCPU_Pause
::1b			lda	mouseData		;Dauerfunktion ?
			bpl	:1

::2			jsr	WM_SAVE_SCREEN

			ldx	#NO_ERROR
			rts

;*** Standard-Routine zum verschieben von Daten.
;    Wird aufgerufen mit Tabellen-Eintrag $FFFF.
.WM_MOVE_ENTRY_I	jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H

			lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_POS

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	r0L
			ora	r0H
			beq	:2

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:1

			lda	#$00
			sta	r0L
			sta	r0H
::1			jmp	WM_SET_NEW_POS
::2			ldx	#$00
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	r0L
			adc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			adc	#$00
			sta	r0H
			jsr	WM_TEST_CUR_POS

;*** ScrollBalken neu positionieren.
:WM_SET_NEW_POS		lda	r0L
			sta	WM_DATA_CURENTRY +0
			ldx	r0H
			stx	WM_DATA_CURENTRY +1
			jsr	SetPosBalken
			jsr	WM_SAVE_WIN_DATA
			ldx	#$00
			rts

;*** Standard-Routine zum verschieben von Text-Daten.
;    Wird aufgerufen mit Tabellen-Eintrag $EEEE.
.WM_MOVE_ENTRY_T	lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_ENTRY_I

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	WM_DATA_CURENTRY +0
			ora	WM_DATA_CURENTRY +1
			bne	:11
			ldx	#$00
			rts

::11			SubVW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	:WM_MOV_LAST_LINE
			ldx	#$ff
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	WM_DATA_CURENTRY +0
			adc	WM_COUNT_ICON_Y
			tax
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			cmp	WM_DATA_MAXENTRY +1
			bne	:21
			cpx	WM_DATA_MAXENTRY +0
::21			bcc	:22
			ldx	#$00
			rts

::22			AddVBW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	:WM_MOV_NEXT_LINE
			ldx	#$ff
			rts

;*** Verschiebt Fensterinhalt um eine Zeile nach unten und
;    gibt Inhalt der nächsten Zeile aus.
::WM_MOV_NEXT_LINE
			lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			clc
			adc	#$08
			lsr
			lsr
			lsr
			sta	r1L
			LoadB	r1H,0
			LoadW	r0 ,320
			ldx	#r1L
			ldy	#r0L
			jsr	DMult

			AddW	leftMargin,r1
			AddVW	SCREEN_BASE,r1

			lda	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r1H
			adc	#> 320
			sta	r0H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::31			dec	WM_COUNT_ICON_Y
			beq	:32
			jsr	MoveData

			lda	r0L
			sta	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			adc	#> 320
			sta	r0H
			jmp	:31

::32			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			jsr	WM_GET_ICON_Y
			lda	WM_COUNT_ICON_Y
			asl
			asl
			asl
			clc
			adc	windowTop
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			clc
			adc	WM_COUNT_ICON_Y
			tay
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			tax

			tya
			bne	:33
			dex
::33			dey
			sty	r0L
			stx	r0H
			jmp	WM_LINE_OUTPUT

;*** Verschiebt Fensterinhalt um eine Zeile nach oben und
;    gibt Inhalt der letzten Zeile aus.
::WM_MOV_LAST_LINE
			lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			lsr
			lsr
			lsr
			clc
			adc	WM_COUNT_ICON_Y
			sta	r0L
			dec	r0L
			LoadB	r0H,0
			LoadW	r1 ,320
			ldx	#r0L
			ldy	#r1L
			jsr	DMult

			AddW	leftMargin,r0
			AddVW	SCREEN_BASE,r0

			lda	r0L
			clc
			adc	#< 320
			sta	r1L
			lda	r0H
			adc	#> 320
			sta	r1H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::41			dec	WM_COUNT_ICON_Y
			beq	:42
			jsr	MoveData

			lda	r0L
			sta	r1L
			sec
			sbc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			sbc	#> 320
			sta	r0H
			jmp	:41

::42			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			lda	windowTop
			clc
			adc	#$08
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jmp	WM_LINE_OUTPUT

;*** Balken verschieben.
.WM_MOVE_POS		CmpW	SB_MaxEntry,SB_MaxEScr
			bcc	:11
			jsr	IsMseOnPos		;Position der Maus ermitteln.
			cmp	#$01			;Oberhalb des Anzeigebalkens ?
			beq	:12			;Ja, eine Seite zurück.
			cmp	#$02			;Auf dem Anzeigebalkens ?
			beq	:13			;Ja, Balken verschieben.
			cmp	#$03			;Unterhalb des Anzeigebalkens ?
			beq	:14			;Ja, eine Seite vorwärts.
::11			rts

::12			jmp	:MOVE_LAST_PAGE
::13			jmp	:MOVE_NEW_POS
::14			jmp	:MOVE_NEXT_PAGE

;*** Balken verschieben.
::MOVE_NEW_POS		jsr	StopMouseMove		;Mausbewegung einschränken.

::MOVE_NEXT		jsr	UpdateMouse		;Mausdaten aktualisieren.

			ldx	mouseData		;Maustaste noch gedrückt ?
			bmi	:21			; => Nein, neue Position anzeigen.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			jsr	UpdateMouse		;Mausdaten aktualisieren.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			cmp	#$06			;Maus nach unten ?
			beq	:MOVE_DOWN		; => Ja, auswerten.
			cmp	#$02			;Maus nach oben ?
			beq	:MOVE_UP		; => Ja, auswerten.
			bne	:MOVE_NEXT		; => Nein, Schleife...

;--- Balken neu positionieren.
::21			jsr	:MOVE_TO_FILE		;Position in Dateiliste berechnen.
			ClrB	pressFlag		;Maustastenklick löschen.
			jsr	WM_NO_MOUSE_WIN
			ldx	#$00
			rts

;--- Balken nach oben.
::MOVE_UP		lda	SB_Top			;Am oberen Rand ?
			beq	:MOVE_NEXT		; =: Ja, Abbruch...
			dec	mouseTop
			dec	mouseBottom
			dec	SB_Top
			dec	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;--- Balken nach unten.
::MOVE_DOWN		lda	SB_Top
			clc
			adc	SB_Length
			cmp	SB_MaxYlen
			bcs	:MOVE_NEXT
			inc	mouseTop
			inc	mouseBottom
			inc	SB_Top
			inc	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;*** Mausposition in Listenposition umrechnen.
::MOVE_TO_FILE		lda	SB_Top			;Aktuelle Mausposition in
			sta	r0L			;Position in Tabelle umrechnen.
			lda	#$00
			sta	r0H

			sec
			lda	SB_MaxEntry +0
			sbc	SB_MaxEScr  +0
			sta	r11L
			lda	SB_MaxEntry +1
			sbc	SB_MaxEScr  +1
			sta	r11H

			ldx	#r0L
			ldy	#r11L
			jsr	DMult

			lda	SB_End
			sec
			sbc	SB_Top
			sta	r10L
			inc	r10L
			lda	SB_MaxYlen
			sec
			sbc	r10L
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r0L
			ldy	#r11L
			jsr	Ddiv
			jmp	WM_SET_NEW_POS

;*** Eine Seite vorwärts.
::MOVE_NEXT_PAGE	clc
			lda	WM_COUNT_ICON_XY
			adc	r0L
			sta	r0L
			lda	#$00
			adc	r0H
			sta	r0H
			jsr	WM_TEST_CUR_POS
			jmp	:31

;*** Eine Seite zurück.
::MOVE_LAST_PAGE	lda	mouseData		;Mausbutton gedrückt ?
			bpl	:MOVE_LAST_PAGE		; => Nein, Ende...
			ClrB	pressFlag

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:31
			lda	#$00
			sta	r0L
			sta	r0H
::31			jsr	WM_SET_NEW_POS

::32			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:32			; => Nein, Ende...
			ClrB	pressFlag
			ldx	#$00
			rts

;*** Fensterdaten in Zwischenspeicher einlesen.
.WM_COPY_WIN_DATA	ldy	#WINDOW_DATA_SIZE -1
::1			lda	(r0L),y
			sta	WM_WIN_DATA_BUF,y
			dey
			bpl	:1
			rts

;*** Fensterdaten laden.
.WM_LOAD_WIN_DATA	PushW	r0

			ldx	WM_WCODE
			lda	WM_WIN_DATA_VECL,x
			sta	r0L
			lda	WM_WIN_DATA_VECH,x
			sta	r0H

			jsr	WM_COPY_WIN_DATA
			PopW	r0
			rts

;*** Fensterdaten speichern.
.WM_SAVE_WIN_DATA	PushW	r0

			ldx	WM_WCODE
			lda	WM_WIN_DATA_VECL,x
			sta	r0L
			lda	WM_WIN_DATA_VECH,x
			sta	r0H

			ldy	#WINDOW_DATA_SIZE -1
::1			lda	WM_WIN_DATA_BUF,y
			sta	(r0L),y
			dey
			bpl	:1

			PopW	r0
			rts

;*** Standardgröße für aktuelles Fenster berechnen.
:WM_DEF_STD_WSIZE	lda	#WIN_STD_POS_Y
			sta	WM_DATA_Y0
			lda	#WIN_STD_POS_Y +WIN_STD_SIZE_Y -1
			sta	WM_DATA_Y1

			lda	#< WIN_STD_POS_X
			sta	WM_DATA_X0 +0
			lda	#> WIN_STD_POS_X
			sta	WM_DATA_X0 +1

			lda	#< WIN_STD_POS_X +WIN_STD_SIZE_X -1
			sta	WM_DATA_X1 +0
			lda	#> WIN_STD_POS_X +WIN_STD_SIZE_X -1
			sta	WM_DATA_X1 +1

			ldx	WM_WCODE
			ldy	#$00
::1			cpx	#$01
			beq	:3
			cpy	#$05
			bne	:2

			ldy	#$00
			SubVB	8*5,WM_DATA_Y0
			SubVB	8*5,WM_DATA_Y1
			AddVW	8  ,WM_DATA_X0
			AddVW	8  ,WM_DATA_X1

::2			AddVB	8  ,WM_DATA_Y0
			AddVB	8  ,WM_DATA_Y1
			AddVW	8  ,WM_DATA_X0
			AddVW	8  ,WM_DATA_X1
			iny
			dex
			bne	:1
::3			rts

;*** Größe für aktuelles Fenster einlesen.
;    Übergabe:		":WM_WCODE" = Fenster-Nr.
.WM_GET_SLCT_SIZE	lda	WM_WCODE

;*** Größe für aktuelles Fenster einlesen.
;    Übergabe:		AKKU = Fenster-Nr.
.WM_GET_WIN_SIZE	tax
			beq	WM_SET_MAX_WIN
			lda	WM_WCODE
			pha
			stx	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			ldx	WM_WCODE
			pla
			sta	WM_WCODE

			ldy	WINDOW_MAXIMIZED,x
			bne	:2

			ldx	#$05
::1			lda	WM_DATA_Y0,x
			sta	r2L       ,x
			dex
			bpl	:1
			bmi	:3

::2			jsr	WM_SET_MAX_WIN
::3			jmp	WM_LOAD_WIN_DATA

;*** Fenstergröße setzen.
:WM_SET_MAX_WIN		ldy	#$00 +5
			b $2c
:WM_SET_STD_WIN		ldy	#$06 +5
			ldx	#$00 +5
::1			lda	WM_WIN_SIZE_TAB,y
			sta	r2L            ,x
			dey
			dex
			bpl	:1
			rts

;*** Größe für aktuelles Fenster festlegen.
;    Übergabe:		":WM_WCODE" = Fenster-Nr.
:WM_SET_WIN_SIZE	jsr	WM_SET_CARD_XY
			jsr	WM_LOAD_WIN_DATA

			ldx	#$05
::1			lda	r2L       ,x
			sta	WM_DATA_Y0,x
			dex
			bpl	:1

			jmp	WM_SAVE_WIN_DATA

;*** Koordinaten auf CARDs umrechnen.
:WM_SET_CARD_XY		lda	r2L
			and	#%11111000
			sta	r2L

			lda	r2H
			ora	#%00000111
			sta	r2H

			lda	r3L
			and	#%11111000
			sta	r3L

			lda	r4L
			ora	#%00000111
			sta	r4L
			rts

;*** Anzahl Datei-Icons in X-Richtung berechnen.
.WM_GET_ICON_X		ldx	WM_DATA_COLUMN
			bne	:3

;--- Anzahl Spalten nicht definiert,
;    Anzahl Spalten aus Fensterbreite und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_X
			sta	:CUR_GRID_X

;--- Fensterbreite berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r4L
			sec
			sbc	r3L
			sta	r0L
			lda	r4H
			sbc	r3H
			sta	r0H

			ldx	#r0L
			ldy	#$03
			jsr	DShiftRight

			ldx	r0L
			inx
			txa
			sec
			sbc	#$04

;--- Max. Anzahl Icons pro Zeile berechnen.
			ldx	#$00
::2			cmp	#$02
			bcc	:3
			tay
			bmi	:3
			sec
			sbc	:CUR_GRID_X
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_X
			rts

::CUR_GRID_X		b $00

;*** Breite für Eintrag ermitteln.
.WM_GET_GRID_X		lda	WM_DATA_GRID_X
			bne	:1
			lda	#WM_GRID_ICON_XC
::1			rts

;*** Anzahl Datei-Icons in Y-Richtung berechnen.
.WM_GET_ICON_Y		ldx	WM_DATA_ROW
			bne	:3

;--- Anzahl Zeilen nicht definiert,
;    Anzahl Zeilen aus Fensterhöhe und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_Y
			sta	:CUR_GRID_Y

;--- Fensterhöhe berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;Fenstergröße - Rahmen - 7
			sec
			sbc	r2L
			sec
			sbc	#$17

;--- Max. Anzahl Zeilen im Fenster berechnen.
			ldx	#$00
::2			sec
			sbc	:CUR_GRID_Y
			bcc	:3
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_Y
			rts

::CUR_GRID_Y		b $00

;*** Höhe eines Eintrages berechnen.
.WM_GET_GRID_Y		lda	WM_DATA_GRID_Y
			bne	:1
			lda	#WM_GRID_ICON_Y
::1			rts

;*** Anzahl Icons im Fenster berechnen.
.WM_GET_ICON_XY		jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	#$00
			ldy	WM_COUNT_ICON_Y
			beq	:2
::1			clc
			adc	WM_COUNT_ICON_X
			dey
			bne	:1

::2			sta	WM_COUNT_ICON_XY
			rts

;*** Fensterposition testen.
:WM_TEST_WIN_POS	lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jsr	WM_TEST_CUR_POS
			lda	r0L
			sta	WM_DATA_CURENTRY +0
			lda	r0H
			sta	WM_DATA_CURENTRY +1
			jmp	WM_SAVE_WIN_DATA

;*** Aktuelle Fensterposition testen.
:WM_TEST_CUR_POS	clc
			lda	r0L
			adc	WM_COUNT_ICON_XY
			tax
			lda	r0H
			adc	#$00

			cmp	WM_DATA_MAXENTRY +1
			bne	:1
			cpx	WM_DATA_MAXENTRY +0
::1			beq	:4
			bcc	:4

			sec
			lda	WM_DATA_MAXENTRY +0
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sbc	#$00
			sta	r0H
			bcc	:3

;			lda	r0L
;			clc
;			adc	WM_COUNT_ICON_X
;			tax
;			lda	r0H
;			adc	#$00
;
;			cmp	WM_DATA_MAXENTRY +1
;			bne	:2
;			cpx	WM_DATA_MAXENTRY +0
;::2			bcs	:4
;			stx	r0L
;			sta	r0H
			rts

::3			lda	#$00
			sta	r0L
			sta	r0H
::4			rts

;*** Fenstergrenzen für Textausgabe setzen.
;    Übergabe:		AKKU = Fensternummer.
.WM_WIN_MARGIN		lda	WM_WCODE
.WM_SET_MARGIN		pha
			jsr	WM_GET_WIN_SIZE

			lda	r3L
			clc
			adc	#$08
			sta	leftMargin +0
			lda	r3H
			adc	#$00
			sta	leftMargin +1

			lda	r4L
			sec
			sbc	#$08
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	r2L
			clc
			adc	#$08
			sta	windowTop

			lda	r2H
			sec
			sbc	#$08
			sta	windowBottom
			pla
			rts

;*** Fenstergrenzen löschen.
.WM_NO_MARGIN		lda	#$00
			sta	windowTop
			sta	leftMargin  +0
			sta	leftMargin  +1
			lda	#SCR_HIGHT_40_80   -1
			sta	windowBottom
			lda	#< SCR_WIDTH_40 -1
			sta	rightMargin +0
			lda	#> SCR_WIDTH_40 -1
			sta	rightMargin +1
			rts

;*** Maus-Fenstergrenzen löschen.
.WM_NO_MOUSE_WIN	LoadB	mouseTop,$00
			LoadB	mouseBottom,$c7
			LoadW	mouseLeft,$0000
			LoadW	mouseRight,$013f
			rts

;*** Position für Icon-Eintrag berechnen.
;    Übergabe:		r0   = Nummer des gesuchten Eintrages.
;    Rücggabe:		xReg = $00, Eintrag gefunden (r1L/r1H).
;			xReg = $FF, Eintrag nicht sichtbar.
.WM_SET_ENTRY		PushW	r0
			jsr	WM_GET_ICON_XY
			jsr	WM_GET_GRID_X
			sta	:CurGrid_X
			jsr	WM_GET_GRID_Y
			sta	:CurGrid_Y
			jsr	WM_GET_SLCT_SIZE
			PopW	r0

			lda	WM_DATA_CURENTRY +0
			sta	r4L
			lda	WM_DATA_CURENTRY +1
			sta	r4H

			CmpW	r0,r4
			bcs	:2
::1			ldx	#$ff
			rts

::2			clc
			lda	r3L
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H
			ldx	#r3L
			ldy	#$03
			jsr	DShiftRight
			lda	r3L
			sta	:MinIcon_X

			lda	r2L
			clc
			adc	#$10
			sta	r1H
			lda	WM_COUNT_ICON_Y
			sta	:MaxIcon_Y

::3			lda	:MinIcon_X
			sta	r1L
			lda	WM_COUNT_ICON_X
			sta	:MaxIcon_X

::4			CmpW	r4,r0
			bne	:5
			ldx	#$00
			rts

::5			inc	r4L
			bne	:6
			inc	r4H

::6			dec	:MaxIcon_X
			bne	:7
			dec	:MaxIcon_Y
			beq	:1

			clc
			lda	r1H
			adc	:CurGrid_Y
			sta	r1H
			jmp	:3

::7			clc
			lda	r1L
			adc	:CurGrid_X
			sta	r1L
			jmp	:4

::MinIcon_X		b $00
::MaxIcon_X		b $00
::MaxIcon_Y		b $00
::CurGrid_X		b $00
::CurGrid_Y		b $00

;*** Datei-Icon invertieren.
;    Übergabe:		r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
.WM_CONVERT_CARDS	MoveB	r1L,r3L
			LoadB	r3H,0
			ldx	#r3L
			ldy	#$03
			jsr	DShiftLeft

			MoveB	r2L,r4L
			LoadB	r4H,0
			ldx	#r4L
			ldy	#$03
			jsr	DShiftLeft

			lda	r3L
			clc
			adc	r4L
			sta	r4L
			lda	r3H
			adc	r4H
			sta	r4H

			lda	r4L
			bne	:1
			dec	r4H
::1			dec	r4L

			lda	r1H
			sta	r2L
			clc
			adc	r2H
			sta	r2H
			dec	r2H
			rts

;*** Position für Icon-Eintrag berechnen.
;    Rückgabe:		r2L-r4	Gewählter Rahmenbereich.
;			r5L	Markierungsmodus:
;				$00 = Links nach rechts.
;				$ff = Rechts nach Links.
.WM_GET_SLCT_AREA	jsr	:SET_MOUSE_FRAME

			lda	mouseXPos +0
			sta	:ClickX   +0
			lda	mouseXPos +1
			sta	:ClickX   +1
			lda	mouseYPos
			sta	:ClickY

			AddVBW	8,mouseXPos
			AddVB	8,mouseYPos

::1			MoveW	mouseXPos,:MouseX
			MoveB	mouseYPos,:MouseY
			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME

::2			lda	mouseData
			bmi	:3
			lda	inputData
			bmi	:2

			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME
			jmp	:1

::3			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME

			ldx	#$00
			CmpW	:ClickX,:MouseX
			bcc	:4
			dex
::4			stx	r5L
			jmp	WM_NO_MOUSE_WIN

;--- Rahmenposition setzen.
::SET_FRAME		CmpW	:MouseX,:ClickX
			bcs	:11
			MoveW	:MouseX,r3
			MoveW	:ClickX,r4
			jmp	:12

::11			MoveW	:ClickX,r3
			MoveW	:MouseX,r4

::12			CmpB	:MouseY,:ClickY
			bcs	:13
			MoveB	:MouseY,r2L
			MoveB	:ClickY,r2H
			jmp	:14

::13			MoveB	:ClickY,r2L
			MoveB	:MouseY,r2H
::14			rts

;--- Mausgrenzen setzen.
::SET_MOUSE_FRAME	lda	WM_WCODE
			jsr	WM_GET_WIN_SIZE

			lda	r3L
			clc
			adc	#$08
			sta	mouseLeft +0
			lda	r3H
			adc	#$00
			sta	mouseLeft +1

			lda	r4L
			sec
			sbc	#$08
			sta	mouseRight +0
			lda	r4H
			sbc	#$00
			sta	mouseRight +1

			lda	r2L
			clc
			adc	#$08
			sta	mouseTop

			lda	r2H
			sec
			sbc	#$08
			sta	mouseBottom
			rts

;--- Variablen.
::ClickX		w $0000
::ClickY		b $00
::MouseX		w $0000
::MouseY		b $00

;*** Variablen.
.WM_WIN_DATA_BUF	s WINDOW_DATA_SIZE
.WM_DATA_SIZE		= WM_WIN_DATA_BUF +0
.WM_DATA_Y0		= WM_WIN_DATA_BUF +1
.WM_DATA_Y1		= WM_WIN_DATA_BUF +2
.WM_DATA_X0		= WM_WIN_DATA_BUF +3
.WM_DATA_X1		= WM_WIN_DATA_BUF +5
.WM_DATA_MAXENTRY	= WM_WIN_DATA_BUF +7
.WM_DATA_CURENTRY	= WM_WIN_DATA_BUF +9
.WM_DATA_GRID_X		= WM_WIN_DATA_BUF +11
.WM_DATA_GRID_Y		= WM_WIN_DATA_BUF +12
.WM_DATA_COLUMN		= WM_WIN_DATA_BUF +13
.WM_DATA_ROW		= WM_WIN_DATA_BUF +14
.WM_DATA_TITLE		= WM_WIN_DATA_BUF +15
.WM_DATA_INFO		= WM_WIN_DATA_BUF +17
.WM_DATA_WININIT	= WM_WIN_DATA_BUF +19
.WM_DATA_WINPRNT	= WM_WIN_DATA_BUF +21
.WM_DATA_WINSLCT	= WM_WIN_DATA_BUF +23
.WM_DATA_WINMOVE	= WM_WIN_DATA_BUF +25
.WM_DATA_MOVEBAR	= WM_WIN_DATA_BUF +27
.WM_DATA_RIGHTCLK	= WM_WIN_DATA_BUF +28
.WM_DATA_WINEXIT	= WM_WIN_DATA_BUF +30
.WM_DATA_WINMSLCT	= WM_WIN_DATA_BUF +32
.WM_DATA_OPTIONS	= WM_WIN_DATA_BUF +34
.WM_DATA_WINSSLCT	= WM_WIN_DATA_BUF +35
.WM_DATA_PRNFILE	= WM_WIN_DATA_BUF +37
.WM_DATA_GETFILE	= WM_WIN_DATA_BUF +39

:WM_WIN_DATA_VECL	b <WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *6

:WM_WIN_DATA_VECH	b >WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *6

:WM_WIN_DATA		s MAX_WINDOWS  *WINDOW_DATA_SIZE

.WMODE_DRIVE		s MAX_WINDOWS
.WMODE_PART		s MAX_WINDOWS
.WMODE_SDIR_T		s MAX_WINDOWS
.WMODE_SDIR_S		s MAX_WINDOWS
.WMODE_VDEL		s MAX_WINDOWS
.WMODE_SLCT_L		s MAX_WINDOWS
.WMODE_SLCT_H		s MAX_WINDOWS
.WMODE_VICON		s MAX_WINDOWS
.WMODE_VSIZE		s MAX_WINDOWS
.WMODE_VINFO		s MAX_WINDOWS
.WMODE_FMOVE		s MAX_WINDOWS

:mouseOldVec		w $0000
.WM_MOVE_MODE		b $00
.WM_COUNT_ICON_X	b $00
.WM_COUNT_ICON_Y	b $00
.WM_COUNT_ICON_XY	b $00

:WM_WIN_SIZE_TAB	b $00  ,MAX_AREA_WIN_Y -1
			w $0000,MAX_AREA_WIN_X -1
			b WIN_STD_POS_Y,WIN_STD_POS_Y + WIN_STD_SIZE_Y -1
			w WIN_STD_POS_X,WIN_STD_POS_X + WIN_STD_SIZE_X -1
			b $00  ,SCR_HIGHT_40_80 -1
			w $0000,SCR_WIDTH_40 -1

.WindowStack		b $00,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
.WINDOW_MAXIMIZED	s MAX_WINDOWS

.WindowOpenCount	b $01
.WM_WCODE		b $00
:VWM_CUR_TOP_WIN	b $00

;*** Fensterfunktionen.
:WM_TAB_WINFUNC1	w WM_DEF_AREA_CL
			w WM_FUNC_CLOSE

			w WM_DEF_AREA_DN
			w WM_FUNC_DOWN

			w WM_DEF_AREA_STD
			w WM_FUNC_STD

			w WM_DEF_AREA_MN
			w WM_FUNC_MIN

			w WM_DEF_AREA_MX
			w WM_FUNC_MAX

			w WM_DEF_AREA_UL
			w WM_FUNC_SIZE_UL

			w WM_DEF_AREA_UR
			w WM_FUNC_SIZE_UR

			w WM_DEF_AREA_DL
			w WM_FUNC_SIZE_DL

			w WM_DEF_AREA_DR
			w WM_FUNC_SIZE_DR

			w WM_DEF_AREA_MV
			w WM_FUNC_SIZE_MV

:WM_TAB_WINFUNC2	w WM_DEF_AREA_WUP
			w WM_FUNC_MOVE_UP

			w WM_DEF_AREA_WDN
			w WM_FUNC_MOVE_DN

			w WM_DEF_AREA_BAR
			w WM_FUNC_MOVER

;*** Tabelle für Icon-Bereiche.
;    b $00!$00 = linke  obere  Ecke
;      $80!$00 = rechte obere  Ecke
;      $00!$40 = linke  untere Ecke
;      $80!$40 = rechte untere Ecke
;    b DeltaX
;    b DeltaY
:WM_DEFICON_TAB		b $00!$00,$08,$00		;Close
			b $80!$00,$27,$00		;Sortieren.
			b $80!$00,$1f,$00		;Standard
			b $80!$00,$17,$00		;Minimize
			b $80!$00,$0f,$00		;Maximize
			b $00!$00,$00,$00		;Resize UL
			b $80!$00,$07,$00		;Resize UR
			b $00!$40,$00,$07		;Resize DL
			b $80!$40,$07,$07		;Resize DR
			b $80!$40,$07,$17		;Resize DL
			b $80!$40,$07,$0f		;Resize DL

;*** Angaben zur Ausgabe der Fenster-Icons.
:WM_SYS1ICON_TAB	w Icon_CL
			w WM_DEF_AREA_CL

			w Icon_DN
			w WM_DEF_AREA_DN

			w Icon_ST
			w WM_DEF_AREA_STD

			w Icon_MN
			w WM_DEF_AREA_MN

			w Icon_MX
			w WM_DEF_AREA_MX

:WM_SYS2ICON_TAB	w Icon_UL
			w WM_DEF_AREA_UL

			w Icon_UR
			w WM_DEF_AREA_UR

			w Icon_DL
			w WM_DEF_AREA_DL

			w Icon_DR
			w WM_DEF_AREA_DR

:WM_SYS3ICON_TAB	w Icon_PU
			w WM_DEF_AREA_WUP

			w Icon_PD
			w WM_DEF_AREA_WDN

;*** Bildschirm für Dialogbox wiederherstellen/zwischenspeichern.
.WM_SAVE_SCREEN		ldy	#$90			;Job-Code für ":StashRAM"
			b $2c
.WM_LOAD_SCREEN		ldy	#$91			;Job-Code für ":FetchRAM"
			sty	DataJobCode		;Aktuellen Job-Code speichern.

			php
			sei

			lda	dispBufferOn		;Bildschirmflag retten und
			pha				;Grafik nur in Vordergrund.
			lda	#ST_WR_FORE
			sta	dispBufferOn

			PushW	r10
			PushW	r11

			lda	WM_WCODE
			asl
			tay
			lda	WIN_DATA_GRFX +0,y
			sta	r10L
			lda	WIN_DATA_GRFX +1,y
			sta	r10H
			lda	WIN_DATA_COLS +0,y
			sta	r11L
			lda	WIN_DATA_COLS +1,y
			sta	r11H

			jsr	WM_GET_SLCT_SIZE

			lda	G3_SCRN_STACK
			jsr	WM_JOB_SVLD_SCRN

			PopW	r11
			PopW	r10

			pla
			sta	dispBufferOn		;Bildschirmflag zurücksetzen.
			plp

			lda	#$ff
			sta	DB_DELTA_Y
			sta	DB_DELTA_X
			rts

;*** Bildschirm für Dialogbox wiederherstellen/zwischenspeichern.
.WM_SAVE_AREA		ldy	#$90			;Job-Code für ":StashRAM"
			b $2c
.WM_LOAD_AREA		ldy	#$91			;Job-Code für ":FetchRAM"
			sty	DataJobCode		;Aktuellen Job-Code speichern.
			tax
			php
			sei

			lda	dispBufferOn		;Bildschirmflag retten und
			pha				;Grafik nur in Vordergrund.
			lda	#ST_WR_FORE
			sta	dispBufferOn

			txa
			jsr	WM_JOB_SVLD_SCRN

			pla
			sta	dispBufferOn		;Bildschirmflag zurücksetzen.
			plp

			lda	#$ff
			sta	DB_DELTA_Y
			sta	DB_DELTA_X
			rts

:WM_JOB_SVLD_SCRN	sta	DataBank

			lda	r3H			;Linken Rand der Box in Cards
			lsr				;berechnen.
			lda	r3L
			and	#%11111000
			sta	r3L
			ror
			lsr
			lsr
			sta	RX_Margin_Left
			ldx	DB_DELTA_X
			cpx	#$ff
			beq	:1
			txa
::1			sta	DB_Margin_Left

			lda	r2L			;Oberen Rand der Box in Cards
			lsr				;berechnen.
			lsr
			lsr
			sta	RX_Margin_Top
			ldx	DB_DELTA_Y
			cpx	#$ff
			beq	:2
			txa
::2			sta	DB_Margin_Top		;Startwert für Job-Routine.

			lda	r2H			;Höhe der Box in Cards
			lsr				;berechnen.
			lsr
			lsr
			sec
			sbc	RX_Margin_Top
			clc
			adc	#$01
			sta	CountLines

			lda	r4L			;Anzahl Grafik-Bytes pro Zeile
			ora	#%00000111
			sta	r4L
			sec				;berechnen.
			sbc	r3L
			sta	r0L
			lda	r4H
			sbc	r3H
			sta	r0H

			inc	r0L
			bne	:3
			inc	r0H
::3			lda	r0L			;Anzahl Bytes in Zwischenspeicher.
			sta	GrfxDataBytes +0
			lda	r0H
			sta	GrfxDataBytes +1

			ldx	#r0L			;Bytes in Cards umrechnen.
			ldy	#$03
			jsr	DShiftRight

			lda	r0L			;Anzahl Cards in Zwischenspeicher.
			sta	ColsDataBytes

::4			jsr	DefCurLineCols		;Zeiger auf Farbdaten berechnen.
			ldy	DataJobCode		;Job-Code einlesen und
			jsr	DoRAMOp			;Daten speichern/einlesen.
			jsr	DefCurLineGrfx		;Zeiger auf Grafikdaten berechnen.
			ldy	DataJobCode		;Job-Code einlesen und
			jsr	DoRAMOp			;Daten speichern/einlesen.

			inc	DB_Margin_Top		;Zeiger auf nächste Zeile.
			inc	RX_Margin_Top		;Zeiger auf nächste Zeile.
			dec	CountLines		;Zähler korrigieren.
			bne	:4			;Box weiter bearbeiten.

::5			rts

;*** Daten für aktuelle Zeile definieren.
:DefCurLineGrfx		ldx	DB_Margin_Top		;Zeiger auf Grafikzeile
			lda	SCREEN_LINE_L,x		;berechnen.
			clc
			adc	#<SCREEN_BASE
			sta	r0L
			lda	SCREEN_LINE_H,x
			adc	#>SCREEN_BASE
			sta	r0H

			ldx	RX_Margin_Top		;Zeiger auf Grafikzeile
			lda	SCREEN_LINE_L,x		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r10L
			sta	r1L
			lda	SCREEN_LINE_H,x
			adc	r10H
			sta	r1H

			ldx	DB_Margin_Left		;Zeiger auf Grafikzeile
			lda	SCREEN_COLUMN_L,x	;berechnen.
			clc
			adc	r0L
			sta	r0L
			lda	SCREEN_COLUMN_H,x
			adc	r0H
			sta	r0H

			ldx	RX_Margin_Left		;Zeiger auf Grafikzeile
			lda	SCREEN_COLUMN_L,x	;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r1L
			sta	r1L
			lda	SCREEN_COLUMN_H,x
			adc	r1H
			sta	r1H

			lda	GrfxDataBytes +0	;Anzahl Grafikbytes festlegen.
			sta	r2L
			lda	GrfxDataBytes +1
			sta	r2H

			lda	DataBank		;MegaPatch-Bank in REU festlegen.
			sta	r3L

::3			rts

;*** Daten für aktuelle Zeile definieren.
:DefCurLineCols		ldx	DB_Margin_Top		;Zeiger auf Grafikzeile
			lda	COLOR_LINE_L,x		;berechnen.
			clc
			adc	#<COLOR_MATRIX
			sta	r0L
			lda	COLOR_LINE_H,x
			adc	#>COLOR_MATRIX
			sta	r0H

			ldx	RX_Margin_Top		;Zeiger auf Grafikzeile
			lda	COLOR_LINE_L,x		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r11L
			sta	r1L
			lda	COLOR_LINE_H,x
			adc	r11H
			sta	r1H

			lda	DB_Margin_Left		;Zeiger auf Grafikzeile
			clc				;berechnen.
			adc	r0L
			sta	r0L
			lda	#$00
			adc	r0H
			sta	r0H

			lda	RX_Margin_Left		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r1L
			sta	r1L
			lda	#$00
			adc	r1H
			sta	r1H

			lda	ColsDataBytes		;Anzahl Farbbytes festlegen.
			sta	r2L
			lda	#$00
			sta	r2H

			lda	DataBank		;MegaPatch-Bank in REU festlegen.
			sta	r3L
::3			rts

;*** Variablen.
:DataJobCode		b $00
:DB_Margin_Left		b $00
:DB_Margin_Top		b $00
:RX_Margin_Left		b $00
:RX_Margin_Top		b $00
:CountLines		b $00
:ColsDataBytes		b $00
:GrfxDataBytes		w $0000
:DB_DELTA_Y		b $ff
:DB_DELTA_X		b $ff
:DataBank		b $00

:WIN_DATA_GRFX		w $0000
			w 8192 *1
			w 8192 *2
			w 8192 *3
			w 8192 *4
			w 8192 *5
			w 8192 *6
:WIN_DATA_COLS		w 8192 *7 +1024*0
			w 8192 *7 +1024*1
			w 8192 *7 +1024*2
			w 8192 *7 +1024*3
			w 8192 *7 +1024*4
			w 8192 *7 +1024*5
			w 8192 *7 +1024*6

;*** Startadressen der Grafikzeilen.
:SCREEN_LINE_L		b <  0*8*40,<  1*8*40,<  2*8*40,<  3*8*40
			b <  4*8*40,<  5*8*40,<  6*8*40,<  7*8*40
			b <  8*8*40,<  9*8*40,< 10*8*40,< 11*8*40
			b < 12*8*40,< 13*8*40,< 14*8*40,< 15*8*40
			b < 16*8*40,< 17*8*40,< 18*8*40,< 19*8*40
			b < 20*8*40,< 21*8*40,< 22*8*40,< 23*8*40
			b < 24*8*40

:SCREEN_LINE_H		b >  0*8*40,>  1*8*40,>  2*8*40,>  3*8*40
			b >  4*8*40,>  5*8*40,>  6*8*40,>  7*8*40
			b >  8*8*40,>  9*8*40,> 10*8*40,> 11*8*40
			b > 12*8*40,> 13*8*40,> 14*8*40,> 15*8*40
			b > 16*8*40,> 17*8*40,> 18*8*40,> 19*8*40
			b > 20*8*40,> 21*8*40,> 22*8*40,> 23*8*40
			b > 24*8*40

;*** Startadressen der Grafikspalten.
:SCREEN_COLUMN_L	b < 8 * 0 ,< 8 * 1 ,< 8 * 2 ,< 8 * 3
			b < 8 * 4 ,< 8 * 5 ,< 8 * 6 ,< 8 * 7
			b < 8 * 8 ,< 8 * 9 ,< 8 * 10,< 8 * 11
			b < 8 * 12,< 8 * 13,< 8 * 14,< 8 * 15
			b < 8 * 16,< 8 * 17,< 8 * 18,< 8 * 19
			b < 8 * 20,< 8 * 21,< 8 * 22,< 8 * 23
			b < 8 * 24,< 8 * 25,< 8 * 26,< 8 * 27
			b < 8 * 28,< 8 * 29,< 8 * 30,< 8 * 31
			b < 8 * 32,< 8 * 33,< 8 * 34,< 8 * 35
			b < 8 * 36,< 8 * 37,< 8 * 38,< 8 * 39

:SCREEN_COLUMN_H	b > 8 * 0 ,> 8 * 1 ,> 8 * 2 ,> 8 * 3
			b > 8 * 4 ,> 8 * 5 ,> 8 * 6 ,> 8 * 7
			b > 8 * 8 ,> 8 * 9 ,> 8 * 10,> 8 * 11
			b > 8 * 12,> 8 * 13,> 8 * 14,> 8 * 15
			b > 8 * 16,> 8 * 17,> 8 * 18,> 8 * 19
			b > 8 * 20,> 8 * 21,> 8 * 22,> 8 * 23
			b > 8 * 24,> 8 * 25,> 8 * 26,> 8 * 27
			b > 8 * 28,> 8 * 29,> 8 * 30,> 8 * 31
			b > 8 * 32,> 8 * 33,> 8 * 34,> 8 * 35
			b > 8 * 36,> 8 * 37,> 8 * 38,> 8 * 39

;*** Startadressen der Farbzeilen.
:COLOR_LINE_L		b <  0*40,<  1*40,<  2*40,<  3*40
			b <  4*40,<  5*40,<  6*40,<  7*40
			b <  8*40,<  9*40,< 10*40,< 11*40
			b < 12*40,< 13*40,< 14*40,< 15*40
			b < 16*40,< 17*40,< 18*40,< 19*40
			b < 20*40,< 21*40,< 22*40,< 23*40
			b < 24*40

:COLOR_LINE_H		b >  0*40,>  1*40,>  2*40,>  3*40
			b >  4*40,>  5*40,>  6*40,>  7*40
			b >  8*40,>  9*40,> 10*40,> 11*40
			b > 12*40,> 13*40,> 14*40,> 15*40
			b > 16*40,> 17*40,> 18*40,> 19*40
			b > 20*40,> 21*40,> 22*40,> 23*40
			b > 24*40

;*** Aktuellen Bildschirm in Hintergrundspeicher kopieren.
;    Wird verwendet für Menüs usw.
.WM_SAVE_BACKSCR	ldx	#$00
::51			lda	r0L,x
			pha
			inx
			cpx	#$07
			bcc	:51

			lda	G3_SYSTEM_BUF
			sta	r3L

			LoadW	r0,SCREEN_BASE
			LoadW	r1,G3_BACKSCR_BUF
			LoadW	r2,8000
			jsr	StashRAM

			LoadW	r0,COLOR_MATRIX
			LoadW	r1,G3_BACKCOL_BUF
			LoadW	r2,1000
			jsr	StashRAM

			ldx	#$06
::52			pla
			sta	r0L,x
			dex
			bpl	:52
			rts

;*** Aktuellen Bildschirm aus Hintergrundspeicher kopieren.
;    Wird verwendet für Menüs usw.
.WM_LOAD_BACKSCR	ldx	#$00
::51			lda	r0L,x
			pha
			inx
			cpx	#$07
			bcc	:51

			lda	G3_SYSTEM_BUF
			sta	r3L

			LoadW	r0,SCREEN_BASE
			LoadW	r1,G3_BACKSCR_BUF
			LoadW	r2,8000
			jsr	FetchRAM

			LoadW	r0,COLOR_MATRIX
			LoadW	r1,G3_BACKCOL_BUF
			LoadW	r2,1000
			jsr	FetchRAM

			ldx	#$06
::52			pla
			sta	r0L,x
			dex
			bpl	:52
			rts

;******************************************************************************
; Funktion		: Auswahltabelle
; Datum			: 02.07.97
; Aufruf		: JSR  InitBalken
; Übergabe		: r0 = Zeiger auf Datentabelle.
;			  b    Zeiger auf xPos									(in CARDS!)
;			  b    Zeiger auf yPos									(in PIXEL!)
;			  b    max. Länge des Balken								(in PIXEL!)
;			  w    max. Anzahl Einträge in Tabelle.
;			  w    max. Einträge auf einer Seite.
;			  w    Tabellenzeiger = Nr. der ersten Datei auf der Seite!
;
;'InitBalken'		Muß als erstes aufgerufen werden um die Daten (r0-r2) für
;			den Anzeigebalken zu definieren und den Balken auf dem
;			Bildschirm auszugeben.
;'SetPosBalken'		Setzt den Füllbalken auf neue Position. Dazu muß im AKKU die
;			neue Position des Tabellenzeigers übergeben werden.
;'PrintBalken'		Zeichnet den Anzeige- und Füllbalken erneut. Dazu muß aber
;			vorher mindestens 1x 'InitBalken' aufgerufen worden sein!
;'ReadSB_Data'		Übergibt folgende Werte an die aufrufende Routine:
;			r0L = SB_XPosByte X-Position Balken in CARDS.
;			r0H = SB_YPosByte Y-Position in Pixel.
;			r1L = SB_MaxYlen									Byte Länge des Balkens.
;			r1H = SB_MaxEntry									Byte Anzahl Einträge in Tabelle.
;			r2L = SB_MaxEScr									Byte Anzahl Einträge auf Seite.
;			r2H = SB_PosEntry									Byte Aktuelle Position in Tabelle.
;			r3  = SB_PosTopWord Startadresse im Grafikspeicher.
;			r4L = SB_TopByte Oberkante Füllbalken.
;			r4H = SB_EndByte Unterkante Füllbalken.
;			r5L = SB_LengthByte Länge Füllbalken.
;'IsMseOnPos'		Mausklick auf Anzeigebalken auswerten. Ergebnis im AKKU:
;			$01 = Mausklick Oberhalb Füllbalken.
;			$02 = Mausklick auf Füllbalken.
;			$03 = Mausklick Unterhalb Füllbalken.
;'StopMouseMove'	Schränkt Mausbewegung ein.
;'SetRelMouse'		Setzt neue Mausposition. Wird beim Verschieben des
;			Füllbalkens benötigt. Vorher muß ein 'JSR SetPosBalken'
;			erfolgen!
;******************************************************************************

;*** Balken initialiseren.
.InitBalken		ldy	#$08			;Paraeter speichern.
::1			lda	(r0L),y
			sta	SB_XPos,y
			dey
			bpl	:1

			jsr	Anzeige_Ypos		;Position Anzeigebalken berechnen.
			jmp	Balken_Ymax		;Länge des Füllbalkens anzeigen.

;*** Neue Balkenposition defnieren und anzeigen.
.SetPosBalken		sta	SB_PosEntry +0		;Neue Position Füllbalken setzen.
			stx	SB_PosEntry +1

;*** Balken ausgeben.
.PrintBalken		jsr	Balken_Ypos		;Y-Position Füllbalken berechnen.
			lda	SB_MaxYlen
			sec
			sbc	SB_Top
			bcc	:1
			cmp	SB_Length
			bcs	PrintCurBalken

::1			lda	SB_MaxYlen
			sec
			sbc	SB_Length
			sta	SB_Top

.PrintCurBalken		MoveW	SB_PosTop,r0		;Grafikposition berechnen.

			ClrB	r1L			;Zähler für Balkenlänge löschen.
			lda	SB_YPos			;Zeiger innerhalb Grafik-CARD be-
			and	#%00000111		;rechnen (Wert von $00-$07).
			tay

::1			lda	#%01010101
			sta	r1H
			lda	r1L
			lsr
			bcc	:1a
			asl	r1H

::1a			lda	SB_Length		;Balkenlänge = $00 ?
			beq	:4			;Ja, kein Füllbalken anzeigen.

			ldx	r1L
			cpx	SB_Top			;Anfang Füllbalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.
			bcc	:4			;Kleiner, dann Hintergrund ausgeben.
			cpx	SB_End			;Ende Füllbalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.
			bcs	:4			;Größer, dann Hintergrund ausgeben.
			inx
			cpx	SB_MaxYlen		;Ende Anzeigebalken erreicht ?
			beq	:3			;Ja, Quer-Linie ausgeben.

::2			lda	r1H
			and	#%10000001
			ora	#%01100110		;Wert für Füllbalken.
			bne	:5

::3			lda	r1H
			ora	#%01111110
			bne	:5

::4			lda	r1H
::5			sta	(r0L),y			;Byte in Grafikspeicher schreiben.
			inc	r1L
			CmpB	r1L,SB_MaxYlen		;Gesamte Balkenlänge ausgegeben ?
			beq	:6			;Ja, Abbruch...

			iny
			cpy	#8			;8 Byte in einem CARD gespeichert ?
			bne	:1			;Nein, weiter...

			AddVW	320,r0			;Zeiger auf nächstes CARD berechnen.
			ldy	#$00
			beq	:1			;Schleife...
::6			rts				;Ende.

;*** Position des Anzeigebalken berechnen.
:Anzeige_Ypos		MoveB	SB_XPos,r0L		;Zeiger auf X-CARD berechnen.
			LoadB	r0H,NULL
			ldx	#r0L
			ldy	#$03
			jsr	DShiftLeft
			AddVW	SCREEN_BASE,r0		;Zeiger auf Grafikspeicher.

			lda	SB_YPos			;Zeiger auf Y-Position
			lsr				;berechnen.
			lsr
			lsr
			tay
			beq	:2
::1			AddVW	40*8,r0
			dey
			bne	:1
::2			MoveW	r0,SB_PosTop		;Grafikspeicher-Adresse merken.
			rts

;*** Länge des Balken berechnen.
:Balken_Ymax		CmpW	SB_MaxEScr,SB_MaxEntry
			bcc	:0			;Balken möglich ?

			lda	#$00			;Nein, weiter...
			beq	:1

::0			MoveB	SB_MaxYlen,r0		;Länge Balken berechnen.
			LoadB	r0H,NULL
			MoveW	SB_MaxEScr,r1
			jsr	Mult_r0r1

			MoveW	SB_MaxEntry,r1
			jsr	Div_r0r1

			CmpBI	r0L,8			;Balken kleiner 8 Pixel ?
			bcs	:1			;Nein, weiter...
			lda	#$08			;Mindestgröße für Balken.
::1			sta	SB_Length
			rts

;*** Position des Balken berechnen.
:Balken_Ypos		ldx	#NULL
			ldy	SB_Length
			CmpW	SB_MaxEScr,SB_MaxEntry
			bcs	:1

			MoveW	SB_PosEntry,r0
			lda	SB_MaxYlen
			suba	SB_Length
			sta	r1L
			LoadB	r1H,NULL
			jsr	Mult_r0r1

			MoveW	SB_MaxEntry,r1
			SubW	SB_MaxEScr ,r1
			jsr	Div_r0r1

			lda	r0L
			tax
			adda	SB_Length
			tay
::1			stx	SB_Top
			dey
			sty	SB_End
			rts

:Mult_r0r1		ldx	#r0L			;Multiplikation durchführen.
			ldy	#r1L
			jmp	DMult

:Div_r0r1		ldx	#r0L
			ldy	#r1L
			jmp	Ddiv

;*** Balken initialiseren.
:ReadSB_Data		ldx	#$0d
::1			lda	SB_XPos,x
			sta	r0L,x
			dex
			bpl	:1
			rts

;*** Mausklick überprüfen.
:IsMseOnPos		lda	mouseYPos
			suba	SB_YPos
			cmp	SB_Top
			bcc	:3
::1			cmp	SB_End
			bcc	:2
			lda	#$03
			b $2c
::2			lda	#$02
			b $2c
::3			lda	#$01
			rts

;*** Mausbewegung kontrollieren.
:StopMouseMove		lda	mouseXPos +0
			sta	mouseLeft +0
			sta	mouseRight+0
			lda	mouseXPos +1
			sta	mouseLeft +1
			sta	mouseRight+1
			lda	mouseYPos
			jmp	SetNewRelMse

:SetRelMouse		lda	#$ff
			adda	SB_Top

:SetNewRelMse		sta	mouseTop
			sta	mouseBottom
			suba	SB_Top
			sta	SetRelMouse+1
			rts

;*** Variablen.
:SB_XPos		b $00				;r0L
:SB_YPos		b $00				;r0H
:SB_MaxYlen		b $00				;r1L
:SB_MaxEntry		w $00				;r1H
:SB_MaxEScr		w $00				;r2L
:SB_PosEntry		w $00				;r2H

:SB_PosTop		w $0000				;r3
:SB_Top			b $00				;r4L
:SB_End			b $00				;r4H
:SB_Length		b $00				;r5L

;*** Icons.
:Icon_MoveW		= 1
:Icon_MoveH		= 8

:Icon_UL
<MISSING_IMAGE_DATA>

:Icon_UR
<MISSING_IMAGE_DATA>

:Icon_DL
<MISSING_IMAGE_DATA>

:Icon_DR
<MISSING_IMAGE_DATA>

:Icon_CL
<MISSING_IMAGE_DATA>

:Icon_PU
<MISSING_IMAGE_DATA>

:Icon_PD
<MISSING_IMAGE_DATA>

:Icon_ST
<MISSING_IMAGE_DATA>

:Icon_DN
<MISSING_IMAGE_DATA>

:Icon_MX
<MISSING_IMAGE_DATA>

:Icon_MN
<MISSING_IMAGE_DATA>

.END_CODE_WM
