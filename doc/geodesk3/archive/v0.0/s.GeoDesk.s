﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "WindowManager"
			t "TopSym"
			t "TopSym/G3"
			t "TopMac/G3"
			t "SymbTab/WM"
			t "s.WM.ext"
			t "SymbTab/LNK"
			t "s.LNK.ext"
			t "s.DLG.ext"

			f APPLICATION
			o $0400
			p MainInit

:MAX_ENTRY_GEOS		= 4
:MENU_GEOS_Y0		= ((MIN_AREA_BAR_Y-1) - MAX_ENTRY_GEOS*14 -2) & $f8
:MENU_GEOS_Y1		=  (MIN_AREA_BAR_Y-1)
:MENU_GEOS_X0		= MIN_AREA_BAR_X
:MENU_GEOS_X1		= $005f
:MENU_GEOS_W		= (MENU_GEOS_X1 - MENU_GEOS_X0 +1)
:MENU_GEOS_H		= (MENU_GEOS_Y1 - MENU_GEOS_Y0 +1)

:MAX_ENTRY_SETUP	= 2
:MENU_SETUP_Y0		= ((MIN_AREA_BAR_Y-1) - MAX_ENTRY_SETUP*14 -2) & $f8 -14 -2
:MENU_SETUP_Y1		=  (MIN_AREA_BAR_Y-1)                                -14 -2
:MENU_SETUP_X0		= MENU_GEOS_X1  + 1
:MENU_SETUP_X1		= MENU_SETUP_X0 + $006f
:MENU_SETUP_W		= (MENU_SETUP_X1 - MENU_SETUP_X0 +1)
:MENU_SETUP_H		= (MENU_SETUP_Y1 - MENU_SETUP_Y0 +1)

			d "o.WM"
			d "o.DLG"
			d "o.LNK"

			t "-AppLinkFunc"
			t "-PopUpMenu"

:SortDetails		b $00,$01,$02,$03,$04,$05

:vecDirEntry		w $0000
:MyComputerEntry	w $0000

;*** Dialogbox: Datei wählen.
:Dlg_SlctPart		b $81
			b DBGETFILES!DBSELECTPART ,$00,$00
			b CANCEL                  ,$00,$00
			b OPEN                    ,$00,$00
			b NULL

:MainInit		jsr	SaveBootDrive

;--- Speicherbänke reservieren.
			jsr	FindFreeBank
			cpx	#NO_ERROR
			bne	:1
			sty	G3_SCRN_STACK
			jsr	AllocateBank

			jsr	FindFreeBank
			cpx	#NO_ERROR
			bne	:1
			sty	G3_SYSTEM_BUF
			jsr	AllocateBank

			jsr	LNK_LOAD_DATA

			LoadW	r0,FontG3
			jsr	LoadCharSet

			LoadW	r0,WinData_DeskTop
			jsr	WM_COPY_WIN_DATA
			jsr	MNU_DRAW_DESKTOP
			jmp	WM_SAVE_WIN_DATA

;--- kein Speicher frei.
::1			jsr	GetBackScreen

			LoadW	r0,Dlg_NoFree2RAM
			jsr	DoDlgBox
			jmp	EnterDeskTop

;*** Taskleiste zeichnen.
:InitTaskBar		lda	#$02
			jsr	SetPattern
			jsr	i_Rectangle
			b	MIN_AREA_BAR_Y,MAX_AREA_BAR_Y
			w	MIN_AREA_BAR_X,MAX_AREA_BAR_X

			lda	C_WinBack
			jsr	DirectColor

			lda	#$00
			jsr	SetPattern
			jsr	i_FrameRectangle
			b	MIN_AREA_BAR_Y    ,MAX_AREA_BAR_Y
			w	MAX_AREA_BAR_X-$2f,MAX_AREA_BAR_X
			b	%11111111
			jsr	i_Rectangle
			b	MIN_AREA_BAR_Y+$01,MAX_AREA_BAR_Y
			w	MAX_AREA_BAR_X-$2e,MAX_AREA_BAR_X-$01
			lda	#$07
			jsr	DirectColor

:ReStartTaskBar		LoadW	appMain,DrawClock

			LoadW	r0,IconTab1
			jmp	DoIcons

;*** Aktuelle Uhrzeit ausgeben.
:DrawClock		LoadW	r0,FontG3
			jsr	LoadCharSet

			LoadB	dispBufferOn,ST_WR_FORE

			jsr	WM_NO_MARGIN

			LoadW	r11,MAX_AREA_BAR_X-$2f +4
			LoadB	r1H,MIN_AREA_BAR_Y +7
			lda	day
			jsr	PrnNumInfo
			lda	#"."
			jsr	SmallPutChar
			lda	month
			jsr	PrnNumInfo
			lda	#"."
			jsr	SmallPutChar
			lda	millenium
			jsr	PrnNumInfo
			lda	year
			jsr	PrnNumInfo
			lda	#" "
			jsr	SmallPutChar

			LoadW	r11,MAX_AREA_BAR_X-$2f +4
			LoadB	r1H,MIN_AREA_BAR_Y +14
			lda	hour
			jsr	PrnNumInfo
			lda	#":"
			jsr	SmallPutChar
			lda	minutes
			jsr	PrnNumInfo
			lda	#"."
			jsr	SmallPutChar
			lda	seconds
			jsr	PrnNumInfo
			lda	#" "
			jmp	SmallPutChar

:PrnNumInfo		jsr	HEXtoASCII
			pha
			txa
			jsr	SmallPutChar
			pla
			jmp	SmallPutChar

;*** DEZIMAL nach ASCII wandeln.
:HEXtoASCII		ldx	#$30
::1			cmp	#10
			bcc	:2
			inx
			sbc	#10
			bcs	:1
::2			adc	#$30
			rts

:GD_PRINT_ENTRY		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L

			lda	r0L
			ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	CopyFName

			ldy	#$18
			lda	(r15L),y
			cmp	#24
			bcc	:1y
			lda	#23
::1y			tax
			lda	V402y0 +0,x
			ora	C_WinBack
			sta	r3L

			LoadB	r2L,$03
			LoadB	r2H,$15
;			LoadB	r3L,$01
			LoadB	r3H,$04
			LoadW	r4 ,TEST
			jsr	GD_FICON_NAME

			lda	#$ff
			jmp	:51

::11			lda	r1H
			clc
			adc	#$06
			sta	r1H

			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			lda	r0L
			ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	CopyFName

			lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			LoadW	r0,TEST
			jsr	PutString
			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$ff
			jmp	:51

::12			lda	r2L
			pha

			lda	r0L
			ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	CopyFName

			lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			PushW	r11
			LoadW	r0,TEST
			jsr	PutString
			PopW	r11

			jsr	DrawDetails

			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			pla
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$7f
::51			pha

			ldy	#$00
			lda	(r15L),y
			bne	:52
			iny
			lda	(r15L),y
			beq	:53
::52			jsr	WM_CONVERT_CARDS
			CmpW	r4,rightMargin
			bcc	:52a
			MoveW	rightMargin,r4

::52a			CmpB	r2H,windowBottom
			bcc	:52b
			MoveB	windowBottom,r2H

::52b			jsr	InvertRectangle

::53			pla
			tax
			rts

;*** Desktop zeichnen.
:MNU_DRAW_DESKTOP	jsr	WM_CLEAR_SCREEN

			jsr	AL_DRAW_FILES
			jsr	InitTaskBar
			jsr	InitForWM
			lda	#$00
			sta	WM_WCODE
			jmp	WM_SAVE_SCREEN

;*** Icon verschieben.
:DRAG_N_DROP_ICON	php
			sei
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA

			lda	#$00
			sta	$d028
			lda	#$07
			sta	$d029

			stx	CPU_DATA
			plp

			LoadB	r3L,1
			jsr	DrawSprite
			jsr	EnablSprite

			LoadB	r3L,2
			jsr	EnablSprite

			lda	#$ff
			ldy	#$3e
::0			sta	spr2pic,y
			dey
			bpl	:0

			LoadB	mouseTop   ,$00
			LoadB	mouseBottom,MIN_AREA_BAR_Y -$08 -$18
			LoadW	mouseLeft  ,$0010
			LoadW	mouseRight ,SCR_WIDTH_40   -$10 -$18

			MoveW	appMain,:appMain_Buf
			LoadW	appMain,:1

			pla
			sta	:returnAdr_Buf +1
			pla
			sta	:returnAdr_Buf +0
			rts

::appMain_Buf		w $0000
::returnAdr_Buf		w $0000

::1			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r5L
			LoadB	r3L,1
			jsr	PosSprite
			LoadB	r3L,2
			jsr	PosSprite

			lda	mouseData
			bmi	:2

			lda	:appMain_Buf +0
			ldx	:appMain_Buf +1
			jmp	CallRoutine

;--- Icon wurde abgelegt:
::2			LoadB	r3L,1
			jsr	DisablSprite
			LoadB	r3L,2
			jsr	DisablSprite

			jsr	WM_NO_MOUSE_WIN

			MoveW	:appMain_Buf,appMain

			lda	:returnAdr_Buf +0
			pha
			lda	:returnAdr_Buf +1
			pha

			jsr	WM_FIND_WINDOW
			cpx	#NO_ERROR
			bne	:3
			lda	WindowStack,y		;Neues Fenster einlesen und
::3			rts

;*** Datei-Icon mit Dateiname ausgeben.
;    Übergabe:		r0  = Zeiger auf Icon.
;			r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
;			r3L = Farbe
;			r3H = DeltaY
;			r4  = Zeiger auf Dateiname.
;			      (Ende mit $00- oder $A0-Byte, max. 16 Zeichen)
:GD_FICON_NAME		PushB	r1L
			PushB	r1H
			PushB	r2L
			PushB	r2H
			PushB	r3L

			PushB	r3H
			PushW	r4			;Zeiger auf Dateiname retten.
			jsr	GD_DRAW_FICON		;Datei-Icon darstellen.
			PopW	r0			;Zeiger auf datename zurücksetzen.
			PopB	r3H
			txa				;Wurde Icon teilweise ausgegeben ?
			bne	:4			; => Nein, Ende...

;--- Textbreite für zentrierte Ausgabe berechnen.
			ldy	#$00
			sty	r4L			;Textbreite löschen.
			sty	r4H
::1			sty	:Vec2FName
			lda	(r0L),y			;Zeichen einlesen. Ende erreicht ?
			beq	:3			; => Ja, weiter...
			cmp	#$a0			;Ende erreicht ?
			beq	:3			; => Ja, weiter...
			ldx	currentMode
			jsr	GetRealSize		;Zeichenbreite ermitteln und
			tya				;zur Gesamtbreite addieren.
			clc
			adc	r4L
			sta	r4L
			bcc	:2
			inc	r4H
::2			ldy	:Vec2FName
			iny				;Zeiger auf nächstes Zeichen.
			cpy	#16			;Dateiname ausgegeben ?
			bne	:1			; => Nein, weiter...

::3			lsr	r4H			;Textbreite halbieren.
			ror	r4L

			lda	#$00			;Startposition für Textausgabe
			sta	r11H			;berechnen.
			lda	r1L
			asl
			asl
			asl
			sta	r11L
			rol	r11H
			AddVW	12,r11			;Zeiger auf Mitte des Date -Icons.

			lda	r11L			;Textposition lach links
			sec				;versetzen => Zentrierte Ausgabe.
			sbc	r4L
			sta	r11L
			lda	r11H
			sbc	r4H
			sta	r11H

			lda	r1H
			clc
			adc	#25
			clc
			adc	r3H
			sta	r1H
			jsr	PutString
			ldx	#$00

::4			PopB	r3L
			PopB	r2H
			PopB	r2L
			PopB	r1H
			PopB	r1L
			rts

::Vec2FName		b $00

;*** Datei-Icon ausgeben (63-Byte-Format).
;    Übergabe:		r0  = Zeiger auf Icon.
;			r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
;			r3L = Farbe
:GD_DRAW_FICON		lda	r1L			;Ist Icon innerhalb des
			asl				;sichtbaren Bereichs ?
			asl
			asl
			sta	r4L
			lda	#$00
			rol
			sta	r4H
			cmp	rightMargin +1
			bne	:1
			lda	r4L
			cmp	rightMargin +0
::1			beq	:2			; => Ja, weiter...
			bcs	:3			; => Nein, Ende...

::2			lda	r1H			;Ist Icon innerhalb des
			cmp	windowBottom		;sichtbaren Bereichs ?
			bcc	:4			; => Ja, weiter...
			ldx	#$7f
			rts
::3			ldx	#$ff			;Icon nicht dargestellt, Ende...
			rts

;--- Position im Grafikspeicher berechnen.
::4			ldx	r1H			;Zeiger auf erstes Byte in Zeile
			jsr	GetScanLine		;des Grafikspeichers berechnen.

			lda	r4L			;Zeiger auf erstes Byte für
			clc				;Icon berechnen.
			adc	r5L
			sta	r5L
			lda	r4H
			adc	r5H
			sta	r5H

::5			lda	rightMargin +1		;Breite des Icons reduzieren,
			lsr				;falls rechter Rand überschritten
			lda	rightMargin +0		;wird.
			ror
			lsr
			lsr
			tax
			inx
			stx	:6 +1
			stx	:7 +1

			lda	r1L
			clc
			adc	r2L
::6			cmp	#40
			beq	:8
			bcc	:8
::7			lda	#40			;Max. mögliche Breite berechnen.
			sec
			sbc	r1L
			sta	r2L
::8			lda	r2L			;Breite = $00 ?
			beq	:3			; => Ja, Ende...

			ldx	windowBottom		;Höhe des icons reduzieren,
			inx				;falls unterer Rand überschritten
			stx	:9 +1			;wird.
			stx	:a +1

			lda	r1H
			clc
			adc	r2H
::9			cmp	#200
			beq	:b
			bcc	:b
::a			lda	#200			;Max. mögliche Höhe berechnen.
			sec
			sbc	r1H
			sta	r2H
::b			lda	r2H			;Neue Icon-Höhe einlesen und
			sta	r4H			;zwischenspeichern. Höhe = $00 ?
			beq	:3			; => Ja, Ende...

;--- Zeiger auf Icon-Daten initialisieren.
			lda	r0L			;$BF-Code übergehen und
			clc				;Zeiger kopieren.
			adc	#$01
			sta	:aa +1
			lda	r0H
			adc	#$00
			sta	:aa +2

;--- "WriteCard"-Flag einlesen.
			ldx	r2L			;Flag für die zu beschreibenden
			lda	:ab,x			;Cards einlesen.
			sta	r4L

;--- Icon zeichnen.
			ldx	#$00

;--- Zeiger auf Card #2 und #3 berechnen.
::c			lda	r5L
			clc
			adc	#$08
			sta	r6L
			lda	r5H
			adc	#$00
			sta	r6H

			lda	r6L
			clc
			adc	#$08
			sta	r7L
			lda	r6H
			adc	#$00
			sta	r7H

			ldy	#$00
::d			jsr	:aa			;Card #1 immer kopieren.
			sta	(r5L),y

			bit	r4L			;Card #2 kopieren ?
			bvc	:e			; => Nein, weiter...
			jsr	:aa
			sta	(r6L),y

			bit	r4L			;Card #2 kopieren ?
			bpl	:f			; => Nein, weiter...
			jsr	:aa
			sta	(r7L),y
			bne	:g

::e			inx				;Icon-Daten überlesen.
::f			inx

::g			dec	r4H			;Alle Zeilen kopiert ?
			beq	:h			; => Ja, Ende...
			iny				;Card-Zeile kopiert ?
			cpy	#$08
			bcc	:d			; => Nein, weiter...

			AddVW	40*8,r5			;Zeiger auf nächste Zeile
			jmp	:c			;Nächste Zeile ausgeben.

;--- Farbe zeichnen.
::h			lda	r1L			;Größe des Farbrechtecks
			sta	r5L			;berechnen.
			lda	r1H
			lsr
			lsr
			lsr
			sta	r5H
			lda	r2L
			sta	r6L
			lda	r2H
			lsr
			lsr
			lsr
			sta	r6H
			lda	r2H			;Höhe für Farbrechteck korrigieren,
			and	#%00000111		;falls Höhe nicht durch 8 ohne
			beq	:j			;Rest teilbar ist
			inc	r6H
::j			lda	r3L
			sta	r7L
			bne	:i
			jsr	DrawColors
			ldx	#$00			;xReg = $00, Icon gezeichnet.
			rts

::i			jsr	RecColorBox
			ldx	#$00			;xReg = $00, Icon gezeichnet.
			rts

;*** Byte aus Icon-Daten einlesen.
::aa			lda	$ffff,x
			inx
			rts

::ab			b $00,$00,$40,$c0

:DrawColors		lda	r8L
			sta	:2 +1
			lda	r8H
			sta	:2 +2

			ldx	r5H
			lda	COLOR_LINE_L,x
			sta	r8L
			lda	COLOR_LINE_H,x
			sta	r8H

			ldx	#$00
			stx	r7H

::1			ldy	r7H
			ldx	:9,y
			ldy	r1L

			lda	r6L
			sta	r7L

::2			lda	$ffff,x
			sta	(r8L),y
			iny
			inx
			dec	r7L
			bne	:2

			AddVBW	40,r8

			inc	r7H
			dec	r6H
			bne	:1
			rts

::9			b	$00,$03,$06

;*** Startadressen der Grafikzeilen.
:COLOR_LINE_L		b < ( 0*40 + COLOR_MATRIX)
			b < ( 1*40 + COLOR_MATRIX)
			b < ( 2*40 + COLOR_MATRIX)
			b < ( 3*40 + COLOR_MATRIX)
			b < ( 4*40 + COLOR_MATRIX)
			b < ( 5*40 + COLOR_MATRIX)
			b < ( 6*40 + COLOR_MATRIX)
			b < ( 7*40 + COLOR_MATRIX)
			b < ( 8*40 + COLOR_MATRIX)
			b < ( 9*40 + COLOR_MATRIX)
			b < (10*40 + COLOR_MATRIX)
			b < (11*40 + COLOR_MATRIX)
			b < (12*40 + COLOR_MATRIX)
			b < (13*40 + COLOR_MATRIX)
			b < (14*40 + COLOR_MATRIX)
			b < (15*40 + COLOR_MATRIX)
			b < (16*40 + COLOR_MATRIX)
			b < (17*40 + COLOR_MATRIX)
			b < (18*40 + COLOR_MATRIX)
			b < (19*40 + COLOR_MATRIX)
			b < (20*40 + COLOR_MATRIX)
			b < (21*40 + COLOR_MATRIX)
			b < (22*40 + COLOR_MATRIX)
			b < (23*40 + COLOR_MATRIX)
			b < (24*40 + COLOR_MATRIX)

:COLOR_LINE_H		b > ( 0*40 + COLOR_MATRIX)
			b > ( 1*40 + COLOR_MATRIX)
			b > ( 2*40 + COLOR_MATRIX)
			b > ( 3*40 + COLOR_MATRIX)
			b > ( 4*40 + COLOR_MATRIX)
			b > ( 5*40 + COLOR_MATRIX)
			b > ( 6*40 + COLOR_MATRIX)
			b > ( 7*40 + COLOR_MATRIX)
			b > ( 8*40 + COLOR_MATRIX)
			b > ( 9*40 + COLOR_MATRIX)
			b > (10*40 + COLOR_MATRIX)
			b > (11*40 + COLOR_MATRIX)
			b > (12*40 + COLOR_MATRIX)
			b > (13*40 + COLOR_MATRIX)
			b > (14*40 + COLOR_MATRIX)
			b > (15*40 + COLOR_MATRIX)
			b > (16*40 + COLOR_MATRIX)
			b > (17*40 + COLOR_MATRIX)
			b > (18*40 + COLOR_MATRIX)
			b > (19*40 + COLOR_MATRIX)
			b > (20*40 + COLOR_MATRIX)
			b > (21*40 + COLOR_MATRIX)
			b > (22*40 + COLOR_MATRIX)
			b > (23*40 + COLOR_MATRIX)
			b > (24*40 + COLOR_MATRIX)

;*** Arbeitsplatz öffnen.
:OpenMyComputer		lda	Flag_MyComputer		;Ist "MyComputer" bereits geöffnet?
			beq	:1			; => Nein, weiter...
			jsr	WM_WIN2TOP		;Fenster "MyComputer" an erster
			jmp	WM_DRAW_ALL_WIN		;Stelle anordnen.

::1			jsr	WM_IS_WIN_FREE		;Freies Fenster suchen.
			cpx	#NO_ERROR		;Ist noch ein Fenster frei?
			bne	:2			; => Ende, kein Fenster mehr frei.

			sta	Flag_MyComputer		;Fenster-Nr. für "MyComputer"
			LoadW	r0,WinData_MyComp	;merken und öffnen.
			jmp	WM_OPEN_WINDOW

::2			rts

;*** Arbeitsplatz schliesen.
;    (Aufruf über FensterManager).
:CloseMyComputer	lda	#$00
			sta	Flag_MyComputer
			rts

;*** Icons für Arbeitsplatz anzeigen.
:DrawMyComputer		lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:0c
			ldx	#$00
			rts

::0c			sta	r1L

			ldx	r0L
			cpx	#$04
			bcs	:1q

			lda	driveType,x
			beq	:1q

			ldx	#$00
::0a			lda	r0L,x
			pha
			inx
			cpx	#10
			bcc	:0a

			lda	r0L
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk

::1m			ldx	#$09
::0b			pla
			sta	r0L,x
			dex
			bpl	:0b

::1q			lda	r0H
			pha
			lda	r0L
			pha
			asl
			tax
			lda	WP_Colors   +0,x
			sta	r8L
			lda	WP_Colors   +1,x
			sta	r8H
			txa
			asl
			tax
			lda	WP_IconData +0,x
			sta	r0L
			lda	WP_IconData +1,x
			sta	r0H
			lda	WP_IconData +2,x
			sta	r5L
			lda	WP_IconData +3,x
			sta	r5H

			ldy	#$00
			sty	dataFileName
			pla
			pha
			cmp	#$04
			bcs	:1a
			tax
			lda	driveType,x
			beq	:1d

::1a			lda	(r5L),y
			beq	:1b
			cmp	#$a0
			beq	:1b
			sta	dataFileName,y
			iny
			cpy	#$10
			bcc	:1a
::1b			lda	#$00
			sta	dataFileName,y
			iny
			cpy	#$11
			bcc	:1b

::1d			PushB	r1L
			PushB	r1H
			LoadB	r2L,$03
			LoadB	r2H,$15
			LoadB	r3L,$00
			LoadB	r3H,4
			LoadW	r4,dataFileName
			jsr	GD_FICON_NAME

			pla
			clc
			adc	#$07
			sta	r1H
			pla
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			pla
			pha
			cmp	#$04
			bcs	:1c

			lda	#" "
			jsr	SmallPutChar
			pla
			pha
			clc
			adc	#$41
			jsr	SmallPutChar
			lda	#":"
			jsr	SmallPutChar

::1c			pla
			sta	r0L
			pla
			sta	r0H
			ldx	#$ff
::2			rts

;*** Mausklick auf Arbeitsplatz.
:MseClkMyComputer	jsr	WM_TEST_ENTRY
			bcc	:22

			stx	:ENTRY +0
			sty	:ENTRY +1

			cpx	#$04
			bcs	:30

			lda	driveType,x
			bne	:20
			rts

;--- Laufwerk verschieben.
::20			jsr	WM_TEST_MOVE
			bcs	:21
			jmp	WP_OPEN_DRIVE

::21			lda	:ENTRY
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk

			LoadW	r4,Icon_Drive +1
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:22
			tay
			bne	:22

			jsr	AL_SET_INIT
			ldx	#r3L
			jsr	GetPtrCurDkNm
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jsr	AL_SET_ICON
			jsr	AL_SET_TYPDV
			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS

			ldx	#$00
			ldy	#LINK_DATA_COLOR
::21a			lda	Color_Drive,x
			sta	(r0L),y
			inx
			iny
			cpx	#$09
			bcc	:21a

			lda	curDirHead +32
			ldx	curDirHead +33
			jsr	AL_SET_SDIR
			jmp	AL_SET_END
::22			rts

;--- Drucker verschieben.
::30			jsr	WM_TEST_MOVE
			bcs	:31

			jsr	ChangePrinter
			jmp	WM_CALL_REDRAW

::31			LoadW	r4,Icon_Printer +1
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:22
			tay
			bne	:22

			jsr	AL_SET_INIT
			jsr	AL_SET_ICON
			jsr	AL_SET_XYPOS

			ldx	#$00
			ldy	#LINK_DATA_COLOR
::31a			lda	Color_Prnt,x
			sta	(r0L),y
			inx
			iny
			cpx	#$09
			bcc	:31a

			jsr	ChangePrinter

			jsr	AL_SET_INIT
			LoadW	r3,PrntFileName
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jsr	AL_SET_TYPPR
			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART

			lda	curDirHead +32
			ldx	curDirHead +33
			jsr	AL_SET_SDIR
			jmp	AL_SET_END

::ENTRY			w $0000

;*** Mausklick auf Dateifenster.
:MseClkFileWin		jsr	WM_TEST_ENTRY
			bcs	:2
::1			rts

;--- Mauslick auf Datei.
::2			stx	r0L
			sty	r0H
			LoadW	r1,32
			ldx	#r0L
			ldy	#r1L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r0
			MoveW	r0,:FNameVec

			jsr	WM_TEST_MOVE
			bcs	:11

;--- Datei öffnen.
			ldy	#$02
			lda	(r0L),y
			and	#%00001111
			cmp	#$06
			bne	:10
			iny
			lda	(r0L),y
			sta	r1L
			ldx	WM_WCODE
			sta	WMODE_SDIR_T,x
			iny
			lda	(r0L),y
			sta	r1H
			sta	WMODE_SDIR_S,x
			jsr	OpenSubDir
			jmp	WM_CALL_DRAW
::10			jmp	StartFile_r0

;--- Datei verschieben.
::11			ldy	#$02
			lda	(r0L),y
			bne	:12
			lda	#< Icon_Deleted +1
			ldx	#> Icon_Deleted +1
			bne	:14

::12			lda	(r0L),y
			and	#%00001111
			cmp	#$06
			bne	:12a
			lda	#< Icon_Map +1
			ldx	#> Icon_Map +1
			bne	:14

::12a			lda	r0L
			sta	:FNameVec +0
			clc
			adc	#$02
			sta	r9L
			lda	r0H
			sta	:FNameVec +1
			adc	#$00
			sta	r9H

			jsr	OpenWinDrive
			jsr	GetFHdrInfo

			lda	#< fileHeader +5
			ldx	#> fileHeader +5
::14			sta	r4L
			stx	r4H
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:16
			tay
			bne	:16

			jsr	AL_SET_INIT
			MoveW	:FNameVec,r3
			AddVBW	5,r3
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jsr	AL_SET_ICON

			MoveW	:FNameVec,r3
			jsr	AL_SET_TYPSD
			ldy	#$02
			lda	(r3L),y
			and	#%00001111
			cmp	#$06
			beq	:15
			jsr	AL_SET_TYPFL

::15			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS

			ldx	#$00
			ldy	#LINK_DATA_COLOR
::21a			lda	#$01
			sta	(r0L),y
			inx
			iny
			cpx	#$09
			bcc	:21a

			MoveW	:FNameVec,r3
			ldy	#$04
			lda	(r3L),y
			tax
			dey
			lda	(r3L),y
			jsr	AL_SET_SDIR
			jmp	AL_SET_END
::16			rts

::FNameVec		w $0000

;*** Mausklick auf Link-Eintrag ?
:MseClkAppLink		jsr	AL_FIND_ICON
			txa
			beq	:2
::1			rts

::2			jsr	WM_TEST_MOVE
			bcc	:3
			jmp	AL_MOVE_ICON
::3			jmp	AL_OPEN_ENTRY

;******************************************************************************
;*** Neuen Drucker wählen.
;******************************************************************************
;*** Neuen Druckertreiber laden.
:ChangePrinter		LoadB	r7L,PRINTER
			LoadW	r10,$0000
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			bne	:1			; => Nein, weiter...

			LoadW	r0,dataFileName		;Druckertreiber laden.
			LoadW	r6,PrntFileName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jmp	GetFile			;Druckertreiber einlesen.
::1			rts

;******************************************************************************
;*** Datei auswählen.
;******************************************************************************
;    Übergabe:		r7L  = Datei-Typ.
;			r10  = Datei-Klasse.
;    Rückgabe:		In ":dataFileName" steht der Dateiname.
;			xReg = $00, Datei wurde ausgewählt.
:OpenFile		MoveB	r7L,:OpenFile_Type
			MoveW	r10,:OpenFile_Class

::1			ldx	curDrive
			lda	driveType -8,x
			bne	:3

			ldx	#8
::2			lda	driveType -8,x
			bne	:3
			inx
			cpx	#12
			bcc	:2
			ldx	#$ff
			rts

::3			txa
			jsr	SetDevice

::4			MoveB	:OpenFile_Type ,r7L
			MoveW	:OpenFile_Class,r10
			LoadW	r5 ,dataFileName
			LoadB	r7H,255
			LoadW	r0,Dlg_SlctFile
			jsr	DoDlgBox		;Datei auswählen.

			lda	sysDBData		;Laufwerk wechseln ?
			bpl	:5			; => Nein, weiter...

			and	#%00001111
			jsr	SetDevice		;Neues Laufwerk aktivieren.
			txa				;Laufwerksfehler ?
			beq	:4			; => Nein, weiter...
			bne	:1			; => Ja, gültiges Laufwerk suchen.

::5			cmp	#DISK			;Partition wechseln ?
			beq	:4			; => Ja, weiter...
			ldx	#$ff
			cmp	#CANCEL			;Abbruch gewählt ?
			beq	:6			; => Ja, Abbruch...
			inx
::6			rts

::OpenFile_Type		b $00
::OpenFile_Class	w $0000

;******************************************************************************
;*** Anwendung starten auf die ":r0" zeigt.
;******************************************************************************
:StartFile_r0		ldy	#$18
			lda	(r0L),y
			cmp	#APPLICATION
			beq	:0
			cmp	#AUTO_EXEC
			bne	:3

::0			ldy	#$05
::1			lda	(r0L),y
			beq	:2
			cmp	#$a0
			beq	:2
			sta	:FileName -5,y
			iny
			cpy	#$15
			bcc	:1
::2			lda	#$00
			sta	:FileName -5,y
			iny
			cpy	#$16
			bcc	:2

			LoadB	r0L,$00
			LoadW	r6,:FileName
			jmp	GetFile
::3			rts

::FileName		s 17

:SaveWinDrive		ldx	WM_WCODE
:SaveUserWinDrive	lda	curDrive
			sta	WMODE_DRIVE   ,x
			tay
			lda	drivePartData- 8,y
			sta	WMODE_PART    ,x
			lda	curDirHead   +32
			sta	WMODE_SDIR_T   ,x
			lda	curDirHead   +33
			sta	WMODE_SDIR_S   ,x
			rts

:TEST			s 17

:DrawDetails		AddVBW	$40,r11

			lda	#$00
::1			pha
			tax
			ldy	SortDetails,x
			dey
			bne	:2
			jsr	Detail_Size
::2			dey
			bne	:3
			jsr	Detail_Date
::3			dey
			bne	:4
			jsr	Detail_Time
::4			dey
			bne	:5
			jsr	Detail_GType
::5			dey
			bne	:6
			jsr	Detail_CType

::6			AddVBW	6,r11
			pla
			clc
			adc	#$01
			cmp	#$06
			bcc	:1
			rts

:Detail_Num		lda	r11H
			pha
			lda	r11L
			pha

			tya
			pha

			txa
			jsr	:201
			pha
			txa
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar

			pla
			jsr	SmallPutChar

			pla
			clc
			adc	#11
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

::201			ldx	#$30
::202			cmp	#10
			bcc	:203
			sbc	#10
			inx
			bne	:202
::203			adc	#$30
			rts

:Detail_Size		ldy	#$1e
			lda	(r15L),y
			sta	r0L
			iny
			lda	(r15L),y
			sta	r0H

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:1
			lda	r0L
			pha
			ldx	#r0L
			ldy	#$02
			jsr	DShiftRight
			pla
			and	#%00000011
			beq	:1

			inc	r0L
			bne	:1
			inc	r0H

::1			lda	#$20 ! SET_RIGHTJUST ! SET_SUPRESS
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:2
			lda	#"K"
			jsr	SmallPutChar

::2			rts

:Detail_Date		ldy	#$1b
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$1a
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$19
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

:Detail_Time		ldy	#$1c
			lda	(r15L),y
			tax
			ldy	#":"
			jsr	Detail_Num

			ldy	#$1d
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

:Detail_GType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$18
			lda	(r15L),y
			cmp	#24
			bcc	:1
			lda	#23
::1			asl
			tax
			lda	V402q0 +0,x
			sta	r0L
			lda	V402q0 +1,x
			sta	r0H
			jsr	PutString

			pla
			clc
			adc	#$50
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

:Detail_CType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$02
			lda	(r15L),y
			pha
			and	#%00000111
			asl
			asl
			tay
			lda	V402t0+2,y
			pha
			lda	V402t0+1,y
			pha
			lda	V402t0+0,y
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla

			pha
			bmi	:3
			lda	#"*"
			jsr	SmallPutChar
::3			pla
			and	#%01000000
			beq	:4
			lda	#"<"
			jsr	SmallPutChar

::4			pla
			clc
			adc	#$18
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** Texte für Verzeichnis.
:V402q0			w V402r0 ,V402r1 ,V402r2 ,V402r3 ,V402r4
			w V402r5 ,V402r6 ,V402r7 ,V402r8 ,V402r9
			w V402r10,V402r11,V402r12,V402r13,V402r14
			w V402r15,V402r99,V402r17,V402r99,V402r99
			w V402r99,V402r21,V402r22,V402r99

:V402q1			w V402s0 ,V402s1 ,V402s2 ,V402s3

:V402r0			b "Nicht GEOS",NULL
:V402r1			b "BASIC",NULL
:V402r2			b "Assembler",NULL
:V402r3			b "Datenfile",NULL
:V402r4			b "System-Datei",NULL
:V402r5			b "DeskAccessory",NULL
:V402r6			b "Anwendung",NULL
:V402r7			b "Dokument",NULL
:V402r8			b "Zeichensatz",NULL
:V402r9			b "Druckertreiber",NULL
:V402r10		b "Eingabetreiber",NULL
:V402r11		b "Laufwerkstreiber",NULL
:V402r12		b "Startprogramm",NULL
:V402r13		b "Temporär",NULL
:V402r14		b "Selbstausführend",NULL
:V402r15		b "Eingabetreiber 128",NULL
:V402r17		b "gateWay-Dokument",NULL
:V402r21		b "geoShell-Kommando",NULL
:V402r22		b "geoFAX Druckertreiber",NULL
:V402r99		b "GEOS ???",NULL

:V402r105		b "< 1581 - Partition >",NULL
:V402r106		b "< Unterverzeichnis >",NULL

:V402s0			b "GEOS 40 Zeichen",NULL
:V402s1			b "GEOS 40 & 80 Zeichen",NULL
:V402s2			b "GEOS 64",NULL
:V402s3			b "GEOS 128, 80 Zeichen",NULL

:V402t0			b "DEL SEQ PRG USR REL CBM DIR ??? "
:V402t1			b "Sequentiell",NULL
:V402t2			b "GEOS-VLIR",NULL

:V402y0			b $00
:V402y1			b $00
:V402y2			b $30
:V402y3			b $30
:V402y4			b $20
:V402y5			b $60
:V402y6			b $60
:V402y7			b $30
:V402y8			b $70
:V402y9			b $50
:V402y10		b $50
:V402y11		b $20
:V402y12		b $20
:V402y13		b $00
:V402y14		b $60
:V402y15		b $50
:V402y17		b $60
:V402y21		b $60
:V402y22		b $50
:V402y99		b $70

:GET_ALL_FILES		lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			bne	:0001
			jsr	OpenWinDrive
			jmp	:3

::0001			ldx	WM_WCODE
			lda	WMODE_DRIVE   ,x
			cmp	:curDrive
			bne	:2a

			tay
			lda	RealDrvMode   -8,y
			bpl	:1
			lda	WMODE_PART    ,x
			cmp	:drivePartData
			bne	:2a

::1			lda	RealDrvMode   -8,y
			and	#%01000000
			beq	:2

			lda	WMODE_SDIR_T   ,x
			cmp	:curDirHead_32
			bne	:2a
			lda	WMODE_SDIR_S   ,x
			cmp	:curDirHead_33
			bne	:2a
::2			jmp	:SAVE_FILES_DRV

::2a			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%11000000
			beq	:3

			LoadW	r0,curDirHead
			LoadW	r1,$0000
			LoadW	r2,$0100
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	VerifyRAM
			and	#%00100000
			bne	:3

			ldx	curDrive
			lda	RealDrvType -8,x
			and	#%00000011
			cmp	#$02
			bcc	:0000

			LoadW	r0,dir2Head
			inc	r1H
			jsr	VerifyRAM
			and	#%00100000
			bne	:3

			ldx	curDrive
			lda	RealDrvType -8,x
			and	#%00000011
			cmp	#$03
			bcc	:0000

			LoadW	r0,dir3Head
			inc	r1H
			jsr	VerifyRAM
			and	#%00100000
			bne	:3
::0000			rts

::curDrive		b $00
::drivePartData		b $00
::curDirHead_32		b $00
::curDirHead_33		b $00

::3			jsr	OpenWinDrive

			ldx	WM_WCODE
			lda	ramBaseWin0,x
			beq	:0
			jsr	:201

::0			lda	#$00
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1

			jsr	i_FillRam
			w	(OS_VARS - BASE_DIR_DATA)
			w	BASE_DIR_DATA
			b	$00

			LoadW	r10,BASE_DIR_DATA +2
			LoadW	r11,$0300

			jsr	Get1stDirEntry
			txa
			bne	:50a
			cpy	#$ff
			bne	:51
::50a			jmp	:54

::51			ldy	#$00
			lda	(r5L),y
			bne	:51a
			iny
			lda	(r5L),y
			beq	:51c

			ldx	WM_WCODE
			lda	WMODE_VDEL,x
			bne	:51a
::51c			jmp	:53

::51a			ldy	#$1d
::52			lda	(r5L),y
			sta	(r10L),y
			dey
			bpl	:52

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%10000000
			beq	:52a

			lda	r10L
			sec
			sbc	#$02
			sta	r0L
			lda	r10H
			sbc	#$00
			sta	r0H
			MoveW	r11,r1
			LoadW	r2,32
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	StashRAM
			AddVBW	32,r11

::52a			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%01000000
			beq	:52b

			ldy	#$00
			lda	(r5L),y
			bne	:42c
			LoadW	r0,Icon_Deleted
			jmp	:42b

::42c			and	#%00001111
			cmp	#$06
			bne	:42d
			LoadW	r0,Icon_Map
			jmp	:42b

::42d			ldy	#$13
			lda	(r5L),y
			sta	r1L
			iny
			lda	(r5L),y
			sta	r1H
			ora	r1L
			bne	:42a
			LoadW	r0,Icon_CBM
			jmp	:42b

::42a			LoadW	r4,fileHeader
			jsr	GetBlock
			LoadW	r0,fileHeader+4

::42b			MoveW	r11,r1
			LoadW	r2,64
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	StashRAM
			AddVBW	64,r11

::52b			AddVBW	32,r10

			inc	WM_DATA_MAXENTRY +0
			bne	:53
			inc	WM_DATA_MAXENTRY +1

::53			jsr	GetNxtDirEntry
			txa
			bne	:54
			cpy	#$ff
			beq	:54
			jmp	:51

::54			jsr	WM_SAVE_WIN_DATA
			rts

::SAVE_FILES_DRV	ldx	WM_WCODE
			lda	WMODE_DRIVE   ,x
			sta	:curDrive

			tay
			lda	RealDrvMode   -8,y
			bpl	:1a
			lda	WMODE_PART    ,x
			sta	:drivePartData

::1a			lda	RealDrvMode   -8,y
			and	#%01000000
			beq	:0a

			lda	WMODE_SDIR_T   ,x
			sta	:curDirHead_32
			lda	WMODE_SDIR_S   ,x
			sta	:curDirHead_33
::0a			rts

::201			LoadW	r0,curDirHead
			LoadW	r1,$0000
			LoadW	r2,$0100
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	StashRAM

			LoadW	r0,dir2Head
			inc	r1H
			jsr	StashRAM

			LoadW	r0,dir3Head
			inc	r1H
			jmp	StashRAM

:GET_NEXT_FICON		sta	r15L
			stx	r15H
			sta	r13L
			stx	r13H

			PushB	r1L
			PushB	r1H

			jsr	GD_VEC2ENTRY
			bcc	:0

			LoadW	r0,:dataBuf
			MoveW	r15,r1
			MoveW	r14,r2
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	FetchRAM

			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			cmp	#$c0
			bne	:z1
			LoadW	r0 ,:dataBuf +32
			LoadW	r15,:dataBuf
			PopB	r1H
			PopB	r1L
			rts

::z1			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%10000000
			beq	:0
			LoadW	r15,:dataBuf
			jmp	:1

::0			MoveW	r13,r15
			LoadW	r14,32
			ldx	#r15L
			ldy	#r14L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r15

::1			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%01000000
			beq	:z2
			LoadW	r0,:dataBuf
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::z2			ldy	#$02
			lda	(r15L),y
			bne	:2
			LoadW	r0,Icon_Deleted
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::2			and	#%00001111
			cmp	#$06
			bne	:2a
			LoadW	r0,Icon_Map
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::2a			ldy	#$15
			lda	(r15L),y
			beq	:91
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader
			jsr	GetBlock
			LoadW	r0,fileHeader +4
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::91			LoadW	r0,Icon_CBM
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::dataBuf		s 96

:CopyFName		ldy	#$05
::3			lda	(r15L),y
			cmp	#$a0
			beq	:4
			sta	TEST-5,y
			iny
			cpy	#$15
			bcc	:3
::4			lda	#$00
			sta	TEST-5,y
			iny
			cpy	#$16
			bcc	:4
			rts

:InitWIN		lda	#$00
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			ldx	WM_WCODE
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			bit	Flag_CacheModeOn
			bpl	:2

			ldx	WM_WCODE
			lda	ramBaseWin0,x
			bne	:2

			jsr	FindFreeBank
			cpx	#NO_ERROR
			beq	:1

			ldx	WM_WCODE
			lda	#$00
			sta	ramCacheMode0,x
			rts

::1			tya
			ldx	WM_WCODE
			sta	ramBaseWin0  ,x
			jsr	AllocateBank
::2			rts

:ExitWIN		bit	Flag_CacheModeOn
			bpl	:1
			ldx	WM_WCODE
			ldy	ramBaseWin0,x
			beq	:1
			jmp	FreeBank
::1			rts

:WP_OPEN_DRIVE_NV	cpy	#$00
			bne	:40
			cpx	#$04
			bcs	:40
			txa
			pha
			jsr	WM_IS_WIN_FREE
			sta	:WM_NV_WINDOW
			pla
			cpx	#NO_ERROR
			bne	:40
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk

			ldx	:WM_NV_WINDOW
			jsr	SaveUserWinDrive

			LoadW	r0,WinData_Files
			jmp	WM_OPEN_WINDOW
::40			rts

::WM_NV_WINDOW		b $00

:WP_OPEN_DRIVE		cpy	#$00
			bne	:40
			cpx	#$04
			bcs	:40
			txa
			clc
			adc	#$08
			jsr	SetDevice

			ldx	curDrive
			lda	RealDrvMode -8,x
			asl
			bpl	:1
			jsr	OpenRootDir
			jmp	:2

::1			jsr	OpenDisk

::2			lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			jsr	SaveWinDrive

			jsr	WM_LOAD_WIN_DATA

			LoadW	r0,WinData_Files

			ldy	#$01
::39			lda	WM_WIN_DATA_BUF,y
			sta	(r0L),y
			iny
			cpy	#$07
			bcc	:39

			lda	Flag_MyComputer
			ldx	#$00
			stx	Flag_MyComputer
			pha
			jsr	WM_WIN2TOP
;			jsr	WM_DRAW_NO_TOP
			LoadW	r0,WinData_Files
			pla
			jsr	WM_USER_WINDOW

			ldy	#$01
			lda	#$00
::38			sta	WinData_Files,y
			iny
			cpy	#$07
			bcc	:38

::40			rts

::SelectDrive		b $00
::SelectedWindow	b $00

:PrntCurDkName		lda	curDrive
			tax
			clc
			adc	#$39
			sta	:DiskName +0

			lda	drivePartData -8,x
			ldy	#$30
			ldx	#$30
::1			cmp	#100
			bcc	:2
			sbc	#100
			iny
			bne	:1
::2			cmp	#10
			bcc	:3
			sbc	#10
			inx
			bne	:2
::3			adc	#$30
			sty	:DiskName +2
			stx	:DiskName +3
			sta	:DiskName +4

			ldx	#r0L
			jsr	GetPtrCurDkNm

			ldy	#$00
::4			lda	(r0L),y
			beq	:5
			cmp	#$a0
			beq	:5
			sta	:DiskName +6,y
			iny
			cpy	#$10
			bcc	:4
::5			lda	#$00
			sta	:DiskName +6,y
			iny
			cpy	#$11
			bcc	:5

			LoadW	r0,:DiskName
			jmp	PutString

::DiskName		b $00,"/"
			b $00,$00,$00
			b ":"
			s 17

:PrntCurDkInfo		PushB	r1H
			jsr	OpenWinDrive
			jsr	CalcBlksFree

			ldx	#r3L
			ldy	#$02
			jsr	DShiftRight
			ldx	#r4L
			ldy	#$02
			jsr	DShiftRight

			PopB	r1H
			PushB	r3H
			PushB	r3L
			PushW	r4

			MoveW	r4,r0
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text0
			jsr	PutString

			PopW	r4

			pla
			sec
			sbc	r4L
			sta	r0L
			pla
			sbc	r4H
			sta	r0H
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text1
			jsr	PutString

			lda	WM_DATA_MAXENTRY +0
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sta	r0H
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text2
			jmp	PutString

::tmp_Text0		b "Kb frei, ",0
::tmp_Text1		b "Kb belegt, ",0
::tmp_Text2		b " Datei(en)",0

:WinData_DeskTop	b $ff				;$00 = Standardfenster
							;$ff = Feste Größe.
			b $00,$b7			;Fenstergröße wenn erstes Byte = $FF.
			w $0000,$013f

			w $0000				;Anzahl Einträge.
			w $0000				;Zeiger auf ersten Eintrag.

			b $00				;Breite eines Eintrages.
							;$00 = Standard-Icon-Breite.
			b $00				;Höhe eines Eintrages.
							;$00 = Standard-Icon-Höhe.

			b $00				;Anzahl Spalten.
							;$00 = Wird berechnet.
			b $00				;Anzahl Zeilen.
							;$00 = Wird berechnet.

			w $0000				;Zeiger auf Text für Titelzeile.
			w $0000				;Zeiger auf Text für Infozeile.
			w $0000				;Routine zum einlesen von Daten.
							;(z.B. Dateien einlesen)
			w $0000				;Routine zur Ausgabe der Daten.
			w MseClkAppLink			;Routine wenn Fenster angeklickt.
			w $0000				;Routine zum verschieben der Daten.

			b $00				;Scrollbalken.
							;$00 = Kein Scrollbalken.

			w PM_PROPERTIES
			w $0000
			w $0000
			b $00

			w $0000				;Routine zur Ausgabe eines Eintrages.
			w $0000				;Routine zur Ausgabe eines Eintrages.
			w $0000				;Routine zur Ausgabe eines Eintrages.

:WinData_MyComp		b $00				;$00 = Standardfenster
							;$ff = Feste Größe.
			b $00,$00			;Fenstergröße wenn erstes Byte = $FF.
			w $0000,$0000

			w $0005				;Anzahl Einträge.
			w $0000				;Zeiger auf ersten Eintrag.

			b $00				;Breite eines Eintrages.
							;$00 = Standard-Icon-Breite.
			b $00				;Höhe eines Eintrages.
							;$00 = Standard-Icon-Höhe.

			b $00				;Anzahl Spalten.
							;$00 = Wird berechnet.
			b $00				;Anzahl Zeilen.
							;$00 = Wird berechnet.

			w :101				;Zeiger auf Text für Titelzeile.
			w $0000				;Zeiger auf Text für Infozeile.
			w $0000				;Routine zum einlesen von Daten.
							;(z.B. Dateien einlesen)
			w $ffff				;Routine zur Ausgabe der Daten.
			w MseClkMyComputer		;Routine wenn Fenster angeklickt.
			w $ffff				;Routine zum verschieben der Daten.
							;$FFFF = Systemroutine (für Icons).

			b $ff				;Scrollbalken.
							;$00 = Kein Scrollbalken.
			w PM_MYCOMP			;Routine für rechten Mausklick.
			w CloseMyComputer		;Routine zum Fensterschließen.
			w $0000
			b $00

			w $0000				;Routine zur Ausgabe eines Eintrages.
			w DrawMyComputer		;Routine zur Ausgabe eines Eintrages.
			w $0000				;Routine zur Ausgabe eines Eintrages.

::101			LoadW	r0,:102
			jmp	PutString

::102			b "ARBEITSPLATZ",0

:WinData_Files		b $00				;$00 = Standardfenster
							;$ff = Feste Größe.
			b $00,$00			;Fenstergröße wenn erstes Byte = $FF.
			w $0000,$0000

			w $0000				;Anzahl Einträge.
			w $0000				;Zeiger auf ersten Eintrag.

			b $00				;Breite eines Eintrages.
							;$00 = Standard-Icon-Breite.
			b $00				;Höhe eines Eintrages.
							;$00 = Standard-Icon-Höhe.

			b $00				;Anzahl Spalten.
							;$00 = Wird berechnet.
			b $00				;Anzahl Zeilen.
							;$00 = Wird berechnet.

			w PrntCurDkName			;Zeiger auf Text für Titelzeile.
			w PrntCurDkInfo			;Zeiger auf Text für Infozeile.
			w InitWIN			;Routine zum einlesen von Daten.
							;(z.B. Dateien einlesen)
			w $ffff				;Routine zur Ausgabe der Daten.
			w MseClkFileWin			;Routine wenn Fenster angeklickt.
			w $ffff				;Routine zum verschieben der Daten.
							;$FFFF = Systemroutine (für Icons).

			b $ff				;Scrollbalken.
							;$00 = Kein Scrollbalken.
			w PM_FILE			;Routine für rechten Mausklick.
			w ExitWIN			;Routine zum Fensterschließen.
			w $ffff
			b $00

			w $ffff				;Routine zur Ausgabe eines Eintrages.
			w GD_PRINT_ENTRY		;Routine zur Ausgabe eines Eintrages.
			w GET_ALL_FILES			;Einzelne datei ausgeben.

;*** Daten für Icons im OpenMyComputer.
:WP_IconData		w Icon_Drive  ,DrACurDkNm
			w Icon_Drive  ,DrBCurDkNm
			w Icon_Drive  ,DrCCurDkNm
			w Icon_Drive  ,DrDCurDkNm
			w Icon_Printer,PrntFileName

:WP_Colors		w Color_Drive
			w Color_Drive
			w Color_Drive
			w Color_Drive
			w Color_Prnt

:Color_Drive		b $05,$05,$05,$0f,$0f,$0f,$09,$09,$09
:Color_Prnt		b $15,$15,$05,$bf,$bf,$b5,$b9,$b9,$b9
:Color_SDir		b $75,$75,$75,$75,$75,$75,$79,$79,$79

;*** Icon-Tabelle.
:IconTab1		b $01
			w $0000
			b $00

			w Icon_GEOS
			b MIN_AREA_BAR_X / 8
			b MIN_AREA_BAR_Y
			b Icon_GEOSx,Icon_GEOSy
			w OPEN_MENU_GEOS

;*** Icons.
:Icon_GEOS
<MISSING_IMAGE_DATA>
:Icon_GEOSx		= .x
:Icon_GEOSy		= .y
