﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Startlaufwerk speichern.
.SaveBootDrive		lda	curDrive
			sta	BootDrive
			sta	LinkDrive
			jsr	SetDevice

			ldx	curDrive
			lda	RealDrvType   -8,x
			sta	BootType
			sta	LinkType

			lda	RealDrvMode   -8,x
			sta	BootMode
			sta	LinkMode
			and	#%10000000
			beq	:1
			lda	drivePartData -8,x
::1			sta	BootPart
			sta	LinkPart

			lda	RealDrvMode   -8,x
			and	#%01000000
			tax
			beq	:2
			lda	curDirHead +32
			ldx	curDirHead +33
::2			sta	BootSDir +0
			stx	BootSDir +1
			sta	LinkSDir +0
			stx	LinkSDir +1
			rts

;*** Startlauafwerk einblenden.
.TempBootDrive		jsr	FindBootDrive
			cpx	#NO_ERROR
			bne	:3
			jsr	SetDevice
			txa
			bne	:3
			jsr	OpenDisk
			txa
			bne	:3

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#%10000000
			beq	:1
			lda	drivePartData -8,x
::1			sta	TempPart

			lda	RealDrvMode -8,x
			and	#%01000000
			tax
			beq	:2
			lda	curDirHead +32
			ldx	curDirHead +33
::2			sta	TempSDir +0
			stx	TempSDir +1
			jmp	OpenBootDrive
::3			rts

;*** Startlaufwerk öffnen.
.OpenBootDrive		jsr	FindBootDrive
			cpx	#NO_ERROR
			bne	:3
			jsr	SetDevice
			txa
			bne	:3

			bit	BootMode
			bpl	:1
			lda	BootPart
			sta	r3H
			jsr	OpenPartition
			jmp	:2

::1			jsr	OpenDisk
::2			txa
			bne	:3

			bit	BootMode
			bvc	:3
			lda	BootSDir +0
			sta	r1L
			lda	BootSDir +1
			sta	r1H
			jmp	OpenSubDir
::3			rts

;*** Startlaufwerk suchen.
.FindBootDrive		ldx	curDrive
			lda	RealDrvType -8,x
			cmp	BootType
			beq	:3

			ldx	#$08
::1			lda	driveType -8,x
			beq	:2
			lda	RealDrvType -8,x
			cmp	BootType
			beq	:3
::2			inx
			cpx	#$0c
			bcc	:1

			ldx	#DEV_NOT_FOUND
			rts

::3			txa
			ldx	#NO_ERROR
			rts

;*** Startlauafwerk einblenden.
.TempLinkDrive		jsr	FindLinkDrive
			cpx	#NO_ERROR
			bne	:3
			jsr	SetDevice
			txa
			bne	:3
			jsr	OpenDisk
			txa
			bne	:3

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#%10000000
			beq	:1
			lda	drivePartData -8,x
::1			sta	TempPart

			lda	RealDrvMode -8,x
			and	#%01000000
			tax
			beq	:2
			lda	curDirHead +32
			ldx	curDirHead +33
::2			sta	TempSDir +0
			stx	TempSDir +1
			jmp	OpenLinkDrive
::3			rts

;*** Startlaufwerk öffnen.
.OpenLinkDrive		jsr	FindLinkDrive
			cpx	#NO_ERROR
			bne	:3
			jsr	SetDevice
			txa
			bne	:3

			bit	LinkMode
			bpl	:1
			lda	LinkPart
			sta	r3H
			jsr	OpenPartition
			jmp	:2

::1			jsr	OpenDisk
::2			txa
			bne	:3

			bit	LinkMode
			bvc	:3
			lda	LinkSDir +0
			sta	r1L
			lda	LinkSDir +1
			sta	r1H
			jmp	OpenSubDir
::3			rts

;*** Linklaufwerk suchen.
.FindLinkDrive		ldx	curDrive
			lda	RealDrvType -8,x
			cmp	LinkType
			beq	:3

			ldx	#$08
::1			lda	driveType -8,x
			beq	:2
			lda	RealDrvType -8,x
			cmp	LinkType
			beq	:3
::2			inx
			cpx	#$0c
			bcc	:1

			ldx	#DEV_NOT_FOUND
			rts

::3			txa
			ldx	#NO_ERROR
			rts

;*** Aktuelles Laufwerk speichern.
.SaveTempDrive		lda	curDrive
			jsr	SetDevice
			txa
			bne	:3
			jsr	OpenDisk
			txa
			bne	:3

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#%10000000
			beq	:1
			lda	drivePartData -8,x
::1			sta	TempPart

			lda	RealDrvMode -8,x
			and	#%01000000
			tax
			beq	:2
			lda	curDirHead +32
			ldx	curDirHead +33
::2			sta	TempSDir +0
			stx	TempSDir +1
			ldx	#NO_ERROR
::3			rts

;*** Zurück zum Quell-Laufwerk.
.BackTempDrive		lda	TempDrive
			jsr	SetDevice
			txa
			bne	:3

			ldx	curDrive
			lda	RealDrvMode -8,x
			bpl	:1

			lda	TempPart
			sta	r3H
			jsr	OpenPartition
			jmp	:2

::1			jsr	OpenDisk
::2			txa
			bne	:3

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#%01000000
			tax
			bne	:3

			lda	TempSDir +0
			sta	r1L
			lda	TempSDir +1
			sta	r1H
			jmp	OpenSubDir
::3			rts

;*** Freie 64K-Speicherbank suchen.
.FindFreeBank		ldy	#$00
::1			jsr	GetBankByte
			beq	:2
			iny
			cpy	ramExpSize
			bne	:1
			ldy	#$00
::2			rts

;*** Tabellenwert für Speicherbank finden.
:GetBankByte		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
::1			cpx	#$00
			beq	:2
			asl
			asl
			dex
			bne	:1
::2			and	#%11000000
			rts

;*** Speicherbank reservieren.
.AllocateBank		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
			ora	:DOUBLE_BIT,x
			pha
			tya
			lsr
			lsr
			tax
			pla
			sta	RamBankInUse,x
			rts

::DOUBLE_BIT		b %11000000
			b %00110000
			b %00001100
			b %00000011

;*** Speicherbank freigeben.
.FreeBank		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
			and	:DOUBLE_BIT,x
			pha
			tya
			lsr
			lsr
			tax
			pla
			sta	RamBankInUse,x
			rts

::DOUBLE_BIT		b %00111111
			b %11001111
			b %11110011
			b %11111100

.BootDrive		b $00
.BootPart		b $00
.BootType		b $00
.BootMode		b $00
.BootSDir		b $00,$00

.TempDrive		b $00
.TempPart		b $00
.TempSDir		b $00,$00

.LinkDrive		b $08
.LinkPart		b $05
.LinkType		b $33
.LinkMode		b $80
.LinkSDir		b $00,$00

.G3_SCRN_STACK		b $00
.G3_SYSTEM_BUF		b $00

