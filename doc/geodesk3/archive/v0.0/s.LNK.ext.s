﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AL_SET_DEVICE		= $2c10
:END_CODE_LNK		= $3db6
:Icon_00		= $35f6
:Icon_01		= $3636
:Icon_CBM		= $3d76
:Icon_Deleted		= $3d36
:Icon_Drive		= $3c36
:Icon_Map		= $3cb6
:Icon_MapGD		= $3c76
:Icon_Printer		= $3cf6
:Icons			= $35f6
:IconsEnd		= $3c36
:LNK_LOAD_DATA		= $2a71
:LNK_SAVE_DATA		= $2e79
:LinkData		= $30e1
:LinkDataEnd		= $35f6
