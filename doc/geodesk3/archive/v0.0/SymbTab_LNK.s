﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:LINK_DATA_FILE		= 0
:LINK_DATA_NAME		= 17
:LINK_DATA_TYPE		= 34
:LINK_DATA_XPOS		= 35
:LINK_DATA_YPOS		= 36
:LINK_DATA_COLOR	= 37
:LINK_DATA_DRIVE	= 46
:LINK_DATA_DVTYP	= 47
:LINK_DATA_DPART	= 48
:LINK_DATA_DSDIR	= 49
:LINK_DATA_WMODE	= 51
:LINK_DATA_BUFSIZE	= 52
