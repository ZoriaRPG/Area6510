﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "o.LNK"
			t "TopSym"
			t "TopSym/G3"
			t "TopMac/G3"
			t "SymbTab/WM"
			t "s.WM.ext"
			t "SymbTab/LNK"
			t "s.DLG.ext"

			f DATA
			o END_CODE_DLG

.LNK_LOAD_DATA		jsr	TempLinkDrive
			txa
			bne	:1

			LoadW	r6 ,ND_Name
			LoadB	r7L,SYSTEM
			LoadB	r7H,1
			LoadW	r10,ND_Class
			jsr	FindFTypes
			txa
			beq	:2
::1			jmp	EnterDeskTop

::2			lda	r7H
			bne	:1

			LoadW	r6,ND_Name
			jsr	FindFile
			txa
			bne	:1

			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	GetBlock

			ldx	#$00
			stx	ND_Record
			inx
			stx	LinkData
			LoadW	r14,LinkData +LINK_DATA_BUFSIZE +1
			LoadW	r15,Icon_01

::3			jsr	OpenLinkDrive
			txa
			bne	:1

			lda	ND_Record
			cmp	#25
			bcs	:5
			asl
			tax
			lda	ND_VLIR +2,x
			beq	:4
			sta	r1L
			lda	ND_VLIR +3,x
			sta	r1H
			LoadW	r4,ND_Data
			jsr	GetBlock

			lda	ND_Data +2
			beq	:4

			jsr	CopyNotesData
			jsr	LoadIconData

			inc	LinkData
			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15

::4			inc	ND_Record
			jmp	:3

::5			jmp	BackTempDrive

:CopyNotesData		ldx	#$02
			ldy	#LINK_DATA_FILE
			jsr	:101
			ldy	#LINK_DATA_NAME
			jsr	:101

			ldy	#LINK_DATA_TYPE
			jsr	:201

			inx
			ldy	#LINK_DATA_XPOS
			jsr	:201
			ldy	#LINK_DATA_YPOS
			jsr	:201

			inx
			ldy	#LINK_DATA_COLOR
::1			jsr	:201
			iny
			cpy	#LINK_DATA_COLOR +9
			bcc	:1

			inx
			lda	ND_Data,x
			sec
			sbc	#$39
			ldy	#LINK_DATA_DRIVE
			sta	(r14L),y
			inx

			ldy	#LINK_DATA_DVTYP
			jsr	:201
			ldy	#LINK_DATA_DPART
			jsr	:201

			inx
			ldy	#LINK_DATA_DSDIR +0
			jsr	:201
			ldy	#LINK_DATA_DSDIR +1
			jsr	:201
			jmp	LoadIconData

::101			lda	#$10
			sta	r0L
::101a			lda	ND_Data,x
			cmp	#CR
			beq	:102
			sta	(r14L),y
			inx
			iny
			dec	r0L
			bne	:101a
::102			lda	#$00
::103			sta	(r14L),y
			iny
			dec	r0L
			bpl	:103
			inx
			rts

::201			lda	ND_Data,x
			sec
			sbc	#$30
			cmp	#10
			bcc	:201a
			sbc	#$07
::201a			asl
			asl
			asl
			asl
			sta	r0L
			inx
			lda	ND_Data,x
			sec
			sbc	#$30
			cmp	#10
			bcc	:201b
			sbc	#$07
::201b			ora	r0L
			sta	(r14L),y
			inx
			rts

:LoadIconData		ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			beq	:1
			cmp	#$ff
			beq	:2a
			cmp	#$fe
			beq	:3a
			cmp	#$fd
			beq	:4a

::4a			ldy	#$3f
::4			lda	Icon_Map,y
			sta	(r15L),y
			dey
			bpl	:4
			rts

::3a			ldy	#$3f
::3			lda	Icon_Printer,y
			sta	(r15L),y
			dey
			bpl	:3
			rts

::2a			ldy	#$3f
::2			lda	Icon_Drive,y
			sta	(r15L),y
			dey
			bpl	:2
			rts

::1			jsr	AL_SET_DEVICE

			MoveW	r14,r6
			jsr	FindFile
			LoadW	r9,dirEntryBuf
			jsr	GetFHdrInfo

			ldy	#$3f
::5			lda	fileHeader +4,y
			sta	(r15L),y
			dey
			bpl	:5
			rts

.AL_SET_DEVICE		ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			tax

			ldy	#LINK_DATA_DVTYP
			lda	(r14L),y
			ldy	driveType   -8,x
			beq	:1
			cmp	RealDrvType -8,x
			beq	:4

::1			ldx	#$08
::2			lda	driveType   -8,x
			beq	:3
			cmp	RealDrvType -8,x
			beq	:4
::3			inx
			cpx	#$0c
			bcc	:2
::3a			LoadW	r0,Dlg_DrvTypNFnd
			jsr	DoDlgBox
			ldx	#$ff
			rts

::4			txa
			jsr	SetDevice
			txa
			bne	:3a

			ldy	#LINK_DATA_DPART
			lda	(r14L),y
			sta	r3H
			jsr	OpenPartition
			txa
			bne	:3a
			rts

:ND_Record		b $00
:ND_Name		s 17
:ND_Class		b "Notes       V1.0",NULL
:ND_Data		s 256
:ND_VLIR		s 256

.LNK_SAVE_DATA		jsr	TempLinkDrive
			txa
			bne	:1

			LoadW	r6 ,ND_Name
			LoadB	r7L,SYSTEM
			LoadB	r7H,1
			LoadW	r10,ND_Class
			jsr	FindFTypes
			txa
			beq	:2
::1			jmp	EnterDeskTop

::2			lda	r7H
			bne	:1

			LoadW	r6,ND_Name
			jsr	FindFile
			txa
			bne	:1

			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	GetBlock
			txa
			bne	:1

			ldx	#$00
			stx	ND_Record
			inx
			stx	r13H
			LoadW	r14,LinkData +LINK_DATA_BUFSIZE +1
			LoadW	r15,Icon_01

::3			lda	ND_Record
			cmp	#25
			bcc	:3a
			jmp	:5

::3a			asl
			tax
			lda	ND_VLIR +2,x
			bne	:4a

			lda	#$01
			sta	r3L
			sta	r3H
			jsr	SetNextFree
			txa
			beq	:ab
::aa			jmp	Panic

::ab			lda	ND_Record
			asl
			tax
			lda	r3L
			sta	r1L
			sta	ND_VLIR +2,x
			lda	r3H
			sta	r1H
			sta	ND_VLIR +3,x

			lda	#$00
			sta	ND_Data +2
			sta	ND_Data +0
			lda	#$02
			sta	ND_Data +1

::4a			jsr	SaveNotesData

			lda	ND_Data +2
			bne	:4c
			lda	ND_Record
			asl
			tax
			lda	ND_VLIR +2,x
			sta	r6L
			lda	#$00
			sta	ND_VLIR +2,x
			lda	ND_VLIR +3,x
			sta	r6H
			lda	#$ff
			sta	ND_VLIR +3,x
			jsr	FreeBlock
			jmp	:4d

::4c			lda	ND_Record
			asl
			tax
			lda	ND_VLIR +2,x
			sta	r1L
			lda	ND_VLIR +3,x
			sta	r1H
			LoadW	r4,ND_Data
			jsr	PutBlock

::4d			jsr	PutDirHead
			txa
			beq	:4
			jmp	:aa

::4			inc	r13H
			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15
			inc	ND_Record
			jmp	:3

::5			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	PutBlock
			txa
			bne	:ac
			jmp	BackTempDrive
::ac			jmp	Panic

:SaveNotesData		lda	r13H
			cmp	LinkData
			bcc	:0
			lda	#$00
			sta	ND_Data +2
::z			rts

::0			ldx	#$02
			stx	r13L

			ldy	#LINK_DATA_FILE
::1			lda	(r14L),y
			beq	:2
			sta	ND_Data,x
			iny
			inx
			bne	:1
::2			lda	#CR
			sta	ND_Data,x
			inx

			ldy	#LINK_DATA_NAME
::3			lda	(r14L),y
			beq	:4
			sta	ND_Data,x
			iny
			inx
			bne	:3
::4			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_XPOS
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_YPOS
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_COLOR
::5			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L
			iny
			cpy	#LINK_DATA_COLOR +9
			bcc	:5
			lda	#CR
			sta	ND_Data,x
			inx

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			clc
			adc	#$39
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DVTYP
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DPART
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DSDIR +0
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DSDIR +1
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			lda	#NULL
			sta	ND_Data,x
			sta	ND_Data +0
			stx	ND_Data +1
			rts

::11			pha
			lsr
			lsr
			lsr
			lsr
			jsr	:12
			tax
			pla
			and	#%00001111
::12			cmp	#10
			bcc	:13
			clc
			adc	#$07
::13			adc	#$30
			rts

;*** Verküpfungen für DeskTop
.LinkData		b $01

			b "Arbeitsplatz",0,0,0,0,0	;Name
			b "Arbeitsplatz",0,0,0,0,0	;Angezeigter Name
			b $80				;Typ: $00=Appl./$FF=Verzeichnis.
							;     $80=Arbeitsplatz/$FE=Drucker
			b $02,$01			;Pos
			b 1,1,1,1,1,1,1,1,1		;Farbe
			b $00				;Laufwerk
			b $00				;Laufwerkstyp
			b $00				;Partition
			b $00,$00			;SubDir
			b $00				;Selection

			s 24*LINK_DATA_BUFSIZE
.LinkDataEnd

.Icons
.Icon_00		j
<MISSING_IMAGE_DATA>

.Icon_01		s 24*64
.IconsEnd

.Icon_Drive
			j
<MISSING_IMAGE_DATA>

.Icon_MapGD
			j
<MISSING_IMAGE_DATA>

.Icon_Map
			j
<MISSING_IMAGE_DATA>
if 0=1

<MISSING_IMAGE_DATA>
endif

.Icon_Printer
			j
<MISSING_IMAGE_DATA>

.Icon_Deleted
			j
<MISSING_IMAGE_DATA>

.Icon_CBM
			j
<MISSING_IMAGE_DATA>

.END_CODE_LNK
