﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:G3_BACKSCR_BUF		= $0000
:G3_BACKCOL_BUF		= G3_BACKSCR_BUF + 8000

:WINDOW_DATA_SIZE	= 41

:MAX_WINDOWS		= 7
:NO_MORE_WINDOWS	= $80
:NO_WIN_SELECT		= $81
:NO_LNK_SELECT		= $82
:WINDOW_CLOSED		= $83
:WINDOW_NOT_FOUND	= $84
:JOB_NOT_FOUND		= $85
:WINDOW_BLOCKED		= $86

:SCR_WIDTH_40		= $0140
:SCR_HIGHT_40_80	= $c8
:TASKBAR_HIGHT		= $10

:MIN_AREA_WIN_Y		= $00
:MIN_AREA_WIN_X		= $0000
:MAX_AREA_WIN_X		= SCR_WIDTH_40
:MAX_AREA_WIN_XC	= MAX_AREA_WIN_X /8
:MAX_AREA_WIN_Y		= SCR_HIGHT_40_80 - TASKBAR_HIGHT
:MIN_SIZE_WIN_X		= $0050
:MIN_SIZE_WIN_Y		= $0030

:MIN_AREA_BAR_Y		= SCR_HIGHT_40_80 - TASKBAR_HIGHT
:MAX_AREA_BAR_Y		= SCR_HIGHT_40_80 - 1
:MIN_AREA_BAR_X		= $0000
:MAX_AREA_BAR_X		= SCR_WIDTH_40 - 1

:WIN_STD_POS_X		= $0020
:WIN_STD_POS_Y		= $10
:WIN_STD_SIZE_X		= $00d8
:WIN_STD_SIZE_Y		= $78

:WM_GRID_ICON_XC	= 8
:WM_GRID_ICON_X		= WM_GRID_ICON_XC *8
:WM_GRID_ICON_Y		= 4*8
