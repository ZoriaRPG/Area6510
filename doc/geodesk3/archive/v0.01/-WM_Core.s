﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:		InitForWM
;Parameter:		-
;Rückgabe:		-
;Verändert:		A
;Funktion:		Initialisiert die Mausabfrage für ":mouseVector".
;			Der FM klinkt sich hierbei direkt in die Mausabfrage ein und
;			kehrt danach erst zur eigentlichen Mausroutine zurück.
;******************************************************************************
.InitForWM		MoveW	otherPressVec,mouseOldVec
			LoadW	otherPressVec,WM_ChkMouse
			rts

;******************************************************************************
;Routine:		DoneWithWM
;Parameter:		-
;Rückgabe:		-
;Verändert:		A
;Funktion:		Deaktiviert die Mausabfrage über ":otherPressVec".
;******************************************************************************
.DoneWithWM		MoveW	mouseOldVec,otherPressVec
			rts

;******************************************************************************
;Routine:		WM_IS_WIN_FREE
;Parameter:		-
;Rückgabe:		AKKU	Fenster-Nr.
;			XREG	$00=Kein Fehler.
;Verändert:		A,X,Y
;Funktion:		Sucht nach einer freien Fenster-Nr.
;******************************************************************************
.WM_IS_WIN_FREE		ldy	WindowOpenCount
			cpy	#MAX_WINDOWS
			bcs	:3

			lda	#$00
::1			ldx	#$00
::2			cmp	WindowStack,x
			bne	:4
			clc
			adc	#$01
			cmp	#MAX_WINDOWS
			bcc	:1
::3			ldx	#NO_MORE_WINDOWS
			rts

::4			inx
			cpx	#MAX_WINDOWS
			bcc	:2
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_LOAD_WIN_DATA
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y
;Funktion:		Kopiert Fensterdaten in Zwischenspeicher.
;******************************************************************************
.WM_LOAD_WIN_DATA	PushW	r0

			ldx	WM_WCODE
			lda	WM_WIN_DATA_VECL,x
			sta	r0L
			lda	WM_WIN_DATA_VECH,x
			sta	r0H

			jsr	WM_COPY_WIN_DATA
			PopW	r0
			rts

;******************************************************************************
;Routine:		WM_COPY_WIN_DATA
;Parameter:		r0	Zeiger auf Datentabelle.
;Rückgabe:		-
;Verändert:		A,Y
;Funktion:		Kopiert Fensterdaten in Zwischenspeicher.
;******************************************************************************
.WM_COPY_WIN_DATA	ldy	#WINDOW_DATA_SIZE -1
::1			lda	(r0L),y
			sta	WM_WIN_DATA_BUF,y
			dey
			bpl	:1
			rts

;******************************************************************************
;Routine:		WM_SAVE_WIN_DATA
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y
;Funktion:		Kopiert Daten aus Zwischenspeicher in Fensterdaten-Tabelle.
;******************************************************************************
.WM_SAVE_WIN_DATA	PushW	r0

			ldx	WM_WCODE
			lda	WM_WIN_DATA_VECL,x
			sta	r0L
			lda	WM_WIN_DATA_VECH,x
			sta	r0H

			ldy	#WINDOW_DATA_SIZE -1
::1			lda	WM_WIN_DATA_BUF,y
			sta	(r0L),y
			dey
			bpl	:1

			PopW	r0
			rts

;******************************************************************************
;Routine:		WM_WIN2TOP
;Parameter:		AKKU	Fenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y
;Funktion:		Setzt Fenster-Nr. (AKKU) an erste Stelle im Stack.
;******************************************************************************
.WM_WIN2TOP		sta	:tmp_WinNr
			sta	WM_WCODE

			ldx	#MAX_WINDOWS -1
			ldy	#MAX_WINDOWS -1
::1			lda	WindowStack ,x
			cmp	:tmp_WinNr
			beq	:2
			sta	WindowStack ,y
			dey
			bmi	:3
::2			dex
			bpl	:1
			lda	:tmp_WinNr
			sta	WindowStack
::3			rts

::tmp_WinNr		b $00

;******************************************************************************
;Routine:		WM_DEF_STD_WSIZE
;Parameter:		WM_WCODEFenster-Nr.
;			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y
;Funktion:		Standardgröße für aktuelles Fenster setzen.
;******************************************************************************
:WM_DEF_STD_WSIZE	lda	#WIN_STD_POS_Y
			sta	WM_DATA_Y0
			lda	#WIN_STD_POS_Y +WIN_STD_SIZE_Y -1
			sta	WM_DATA_Y1

			lda	#< WIN_STD_POS_X
			sta	WM_DATA_X0 +0
			lda	#> WIN_STD_POS_X
			sta	WM_DATA_X0 +1

			lda	#< WIN_STD_POS_X +WIN_STD_SIZE_X -1
			sta	WM_DATA_X1 +0
			lda	#> WIN_STD_POS_X +WIN_STD_SIZE_X -1
			sta	WM_DATA_X1 +1

			ldx	WM_WCODE
			ldy	#$00
::1			cpx	#$01
			beq	:3
			cpy	#$05
			bne	:2

			ldy	#$00
			SubVB	8*5,WM_DATA_Y0
			SubVB	8*5,WM_DATA_Y1
			AddVW	8  ,WM_DATA_X0
			AddVW	8  ,WM_DATA_X1

::2			AddVB	8  ,WM_DATA_Y0
			AddVB	8  ,WM_DATA_Y1
			AddVW	8  ,WM_DATA_X0
			AddVW	8  ,WM_DATA_X1
			iny
			dex
			bne	:1
::3			rts

;******************************************************************************
;Routine:		WM_GET_SLCT_SIZE
;Parameter:		WM_WCODE = Fenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Größe für aktuelles Fenster einlesen.
;******************************************************************************
.WM_GET_SLCT_SIZE	lda	WM_WCODE

;******************************************************************************
;Routine:		WM_GET_WIN_SIZE
;Parameter:		AKKU = Fenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Größe für bestimmtes Fenster einlesen.
;******************************************************************************
.WM_GET_WIN_SIZE	tax
			beq	WM_SET_MAX_WIN
			lda	WM_WCODE
			pha
			stx	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			ldx	WM_WCODE
			pla
			sta	WM_WCODE

			ldy	WINDOW_MAXIMIZED,x
			bne	:2

			ldx	#$05
::1			lda	WM_DATA_Y0,x
			sta	r2L       ,x
			dex
			bpl	:1
			bmi	:3

::2			jsr	WM_SET_MAX_WIN
::3			jmp	WM_LOAD_WIN_DATA

;******************************************************************************
;Routine:		WM_SET_MAX_WIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Definiert max. Fenstergröße.
;******************************************************************************
:WM_SET_MAX_WIN		ldy	#$00 +5
			b $2c

;******************************************************************************
;Routine:		WM_SET_STD_WIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Definiert Standard-Fenstergröße.
;******************************************************************************
:WM_SET_STD_WIN		ldy	#$06 +5
			ldx	#$00 +5
::1			lda	WM_WIN_SIZE_TAB,y
			sta	r2L            ,x
			dey
			dex
			bpl	:1
			rts

;******************************************************************************
;Routine:		WM_CALL_DRAW
;Parameter:		WM_WCODE = Fenster-Nr.
;			WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet das angegebene Fenster mit Inhalt.
;******************************************************************************
.WM_CALL_DRAW		jsr	WM_LOAD_WIN_DATA
			jsr	WM_DRAW_SLCT_WIN
			lda	WM_DATA_WININIT +0
			ldx	WM_DATA_WININIT +1
			ldy	WM_WCODE
			jsr	CallRoutine
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW_W

;******************************************************************************
;Routine:		WM_CALL_REDRAW
;Parameter:		WM_WCODE = Fenster-Nr.
;			WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet das angegebene Fenster neu.
;******************************************************************************
.WM_CALL_REDRAW		jsr	WM_DRAW_SLCT_WIN
:WM_CALL_REDRAW_W	jsr	WM_WIN_MARGIN
			jsr	WM_CALL_DRAWROUT
			jsr	WM_SAVE_WIN_DATA
			jsr	WM_DRAW_TITLE		;Titelzeile ausgeben.
			jsr	WM_DRAW_INFO		;Infozeile ausgeben.
			jsr	WM_DRAW_MOVER
			jmp	WM_SAVE_SCREEN

;******************************************************************************
;Routine:		WM_CALL_RIGHTCLK
;Parameter:		WM_WCODEFenster-Nr.
;			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Klick mit rechter Maustaste auswerten.
;******************************************************************************
:WM_CALL_RIGHTCLK	lda	mouseData		;Mausbutton gedrückt ?
			bpl	WM_CALL_RIGHTCLK	; => Nein, Ende...
			ClrB	pressFlag

			lda	#$3f			;Pause für Maustreiber mit
::1			pha				;Doppelklick auf rechter Taste.
			jsr	UpdateMouse
			pla
			sec
			sbc	#$01
			bpl	:1

::2			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:2			; => Nein, Ende...
			ClrB	pressFlag

			lda	WM_DATA_RIGHTCLK +0
			ldx	WM_DATA_RIGHTCLK +1
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_CALL_MOVE
;Parameter:		WM_WCODE = Fenster-Nr.
;			WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fensterinhalt verschieben.
;******************************************************************************
:WM_CALL_MOVE		lda	WM_DATA_WINMOVE +0
			ldx	WM_DATA_WINMOVE +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	WM_MOVE_ENTRY_I

::51			cmp	#$ee
			bne	:52
			cpx	#$ee
			bne	:52
			jmp	WM_MOVE_ENTRY_T

::52			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_CALL_EXEC
;Parameter:		WM_WCODEFenster-Nr.
;			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Mausklick in Fenster auswerten.
;******************************************************************************
:WM_CALL_EXEC		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINSLCT +0
			ldx	WM_DATA_WINSLCT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_CALL_EXIT
;Parameter:		WM_WCODEFenster-Nr.
;			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster zum schließen vorbereiten.
;******************************************************************************
:WM_CALL_EXIT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINEXIT +0
			ldx	WM_DATA_WINEXIT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_OPEN_WINDOW
;Parameter:		r0	Zeiger auf Datentabelle.
;Rückgabe:		AKKU	Fenster-Nr.
;			XREG	$00=Kein Fehler.
;Verändert:		A,X,Y,r0-r15
;Funktion:		Öffnet und zeichnet ein neues Fenster mit Inhalt.
;******************************************************************************
.WM_OPEN_WINDOW		jsr	WM_IS_WIN_FREE		;Freie Fenster-Nr. suchen.
			cpx	#NO_ERROR		;Fenster gefunden ?
			beq	:1			; => Ja, weiter...
			rts

::1			ldy	WindowOpenCount		;Fenster in Stackspeicher eintragen.
			sta	WindowStack,y
			inc	WindowOpenCount

;******************************************************************************
;Routine:		WM_USER_WINDOW
;Parameter:		AKKU	Fenster-Nr.
;			r0	Zeiger auf Datentabelle.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Öffnet und zeichnet ein bestimmtes Fenster mit Inhalt.
;******************************************************************************
.WM_USER_WINDOW		pha				;Fensterdaten in
			sta	WM_WCODE		;Zwischenspeicher kopieren.
			jsr	WM_COPY_WIN_DATA
			pla
			tax
			lda	#$00			;Flag löschen: "Maximiert".
			sta	WINDOW_MAXIMIZED,x
			txa
			jsr	WM_WIN2TOP		;Fenster nach oben holen.

			lda	WM_DATA_SIZE		;Feste Fenstergröße ?
			bne	:1			; => Ja, weiter...

			lda	WM_DATA_Y0
			ora	WM_DATA_Y1		;Fenstergröße definiert ?
			bne	:1			; => Ja, weiter...
			jsr	WM_DEF_STD_WSIZE	;Standard-Größe festlegen.

::1			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			ldx	WM_WCODE
			lda	#$00
			sta	WMODE_VDEL,x
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x
			sta	WMODE_VICON,x
			sta	WMODE_VSIZE,x
			sta	WMODE_VINFO,x

			jsr	WM_CALL_DRAW		;Fenster ausgeben.

			lda	WM_WCODE
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_CLOSE_WINDOW
;Parameter:		AKKU	Fenster-Nr.
;Rückgabe:		AKKU	Fenster-Nr.
;			XREG	$00=Kein Fehler.
;Verändert:		A,X,Y,r0-r15
;Funktion:		Öffnet und zeichnet ein Fenster mit Inhalt innerhalb eines
;			bereits geöffneten Fensters.
;			(z.B. Arbeitsplatz=>Laufwerk öffnen)
;******************************************************************************
.WM_CLOSE_WINDOW	pha				;Vorbereiten: Fenster schließen.
			jsr	WM_CALL_EXIT
			pla
			sta	:tmp_WinNr		;Fenster-Nr. merken.

			ldx	#$00			;Fenster-Nr. in Stack suchen und
			ldy	#$00			;löschen. Stack komprimieren.
::1			lda	WindowStack ,x
			cmp	:tmp_WinNr
			beq	:2
			sta	WindowStack ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1
			cpy	#MAX_WINDOWS
			beq	:3

			dec	WindowOpenCount		;Anzahl offene Fenster -1.

			lda	#$ff
			sta	WindowStack ,y
			jmp	WM_DRAW_ALL_WIN		;Alle Fenster neu zeichnen.
::3			rts

::tmp_WinNr		b $00

;******************************************************************************
;Routine:		WM_ChkMouse
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Wertet Mausklick auf DeskTop/Fenster/Icon aus.
;******************************************************************************
.WM_ChkMouse		lda	mouseData		;Mausbutton gedrückt ?
			bmi	:1			; => Nein, Ende...

			jsr	WM_FIND_WINDOW		;Fenster suchen.
			txa				;Wurde Fenster gefunden ?
			beq	:2			; => Ja, weiter...

			lda	mouseOldVec +0		;Mausabfrage extern fortsetzen.
			ldx	mouseOldVec +1
			jmp	CallRoutine
::1			rts

::2			lda	WindowStack		;Nr. des obersten Fensters einlesen
			sta	VWM_CUR_TOP_WIN		;und zwisichenspeichern.

;--- Fenster-Daten einlesen.
			lda	WindowStack,y		;Neue Fenster-Nr. einlesen und
			sta	WM_WCODE		;Fensterdaten kopieren.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_WCODE		;Desktop ?
			beq	:5			; => Ja, keine Fenster-Icons.

;--- Klick in Fenster ?
			php
			sei
			jsr	WM_GET_SLCT_SIZE
			AddVBW	8,r3
			SubVW	8,r4
			AddVB	8,r2L
			SubVB	8,r2H
			jsr	IsMseInRegion
			plp
			tax				;Mausklick innerhalb Dateifenster ?
			bne	:5			; => Ja, weiter...

;--- Klick auf Fenster-Icons ?
			jsr	WM_WIN_BLOCKED		;Ist Ziel-Fenster durch andere
			txa				;Fenster verdeckt ?
			pha
			lda	WM_WCODE
			jsr	WM_WIN2TOP		;Fenster umsortieren.
			pla				;Ist Fenster verdeckt ?
			beq	:3			; => Nein, weiter...

			jsr	WM_DRAW_TOP_WIN		;Ziel-Fenster neu zeichnen.

::3			LoadW	r14,:WM_TAB_WINFUNC1
			LoadB	r15H,12
			jsr	WM_FIND_JOBS		;System-Icons auswerten.
			txa				;Wurde Fenster-Icon gewählt ?
			beq	:4			; => Ja, Ende...

			LoadW	r14,:WM_TAB_WINFUNC2
			LoadB	r15H,3
			jsr	WM_FIND_JOBS		;Scroll-Icons auswerten.
			txa
			bne	:8
::4			rts

;--- Rechter Mausklick ?
::5			php
			sei
			ldx	CPU_DATA
			ldy	#$35
			sty	CPU_DATA
			lda	$dc01
			stx	CPU_DATA
			plp

			cmp	#207			;Rechter Mausklick ?
			beq	:7c			; => Nein, weiter...
			cmp	#253			;Rechter Mausklick ?
			beq	:7b			; => Nein, weiter...
			cmp	#254			;Rechter Mausklick ?
			bne	:6			; => Nein, weiter...
			jmp	WM_CALL_RIGHTCLK	;Mausklick auswerten.

;--- Klick auf DeskTop ?
::6			lda	WM_WCODE		;Klick auf DeskTop?
			bne	:7			; => Nein, weiter...
			jmp	WM_CALL_EXEC		;DeskTop-Klick auswerten.

;--- Wechsel zu anderem Fenster.
::7			jsr	WM_TEST_ENTRY		;Datei-Icon ausgewählt ?
			bcc	:7a			; => Nein, weiter...
			jsr	:8
			jmp	WM_CALL_EXEC		;Datei-Klick auswerten.

::7a			jsr	WM_TEST_MOVE
			bcc	:8
::7b			jsr	:8
			jmp	WM_CALL_MSLCT		;Datei-Klick auswerten.

::7c			jsr	:8
			jmp	WM_CALL_SSLCT

::8			lda	WM_WCODE		;Fenster umsortieren.
			jsr	WM_WIN2TOP
			jmp	WM_DRAW_TOP_WIN
;*** Fensterfunktionen.
::WM_TAB_WINFUNC1	w WM_DEF_AREA_CL
			w WM_FUNC_CLOSE

			w WM_DEF_AREA_DN
			w WM_FUNC_DOWN

			w WM_DEF_AREA_STD
			w WM_FUNC_STD

			w WM_DEF_AREA_MN
			w WM_FUNC_MIN

			w WM_DEF_AREA_MX
			w WM_FUNC_MAX

			w WM_DEF_AREA_UL
			w WM_FUNC_SIZE_UL

			w WM_DEF_AREA_UR
			w WM_FUNC_SIZE_UR

			w WM_DEF_AREA_DL
			w WM_FUNC_SIZE_DL

			w WM_DEF_AREA_DR
			w WM_FUNC_SIZE_DR

			w WM_DEF_AREA_MV
			w WM_FUNC_SIZE_MV

::WM_TAB_WINFUNC2	w WM_DEF_AREA_WUP
			w WM_FUNC_MOVE_UP

			w WM_DEF_AREA_WDN
			w WM_FUNC_MOVE_DN

			w WM_DEF_AREA_BAR
			w WM_FUNC_MOVER

;******************************************************************************
;Routine:		WM_FIND_WINDOW
;Parameter:		-
;Rückgabe:		XREG	$00=Kein Fehler.
;			YREG	Fenster-Nr.
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Aktuelles Fenster suchen.
;******************************************************************************
.WM_FIND_WINDOW		php				;IRQ sperren.
			sei

			lda	#$00			;Fenster-Nr. löschen.
			sta	:tmp_WinNr

::1			ldx	#WINDOW_NOT_FOUND	;Fehlermeldung vorbereiten.

			ldy	:tmp_WinNr
			cpy	#MAX_WINDOWS		;Alle Fenster durchsucht ?
			beq	:3			; => Ja, Ende...
			lda	WindowStack,y		;Fenster-Nr. einlesen.
			bmi	:3			; => Ende erreicht...

			jsr	WM_GET_WIN_SIZE		;Fenstergröße einlesen.
			jsr	IsMseInRegion		;Mausposition abfragen.
			tax				;Ist Maus in Bereich ?
			bne	:2			; => Ja, Fenster gefunden.

			inc	:tmp_WinNr		;Zeiger auf nächstes Fenster und
			jmp	:1			;weitersuchen.

::2			ldx	#NO_ERROR		;Flag für "Kein Fehler" setzen und
			ldy	:tmp_WinNr		;Fenster-Nr. einlesen.
::3			plp
			rts

::tmp_WinNr		b $00

;******************************************************************************
;Routine:		WM_WIN_BLOCKED
;Parameter:		-
;Rückgabe:		XREG	$00=Fenster nicht verdeckt.
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Prüft ob aktuelles Fenster verdeckt ist.
;******************************************************************************
.WM_WIN_BLOCKED		lda	WM_WCODE		;Aktuelle Fenster-Nr. in
			sta	:tmp_WinNr		;Zwischenspeicher einlesen.
			jsr	:1			;Ist Fenster verdeckt ?

			txa				;Ergebnis zwischenspeichern.
			pha

			lda	:tmp_WinNr		;Fensterdaten für aktuelles
			sta	WM_WCODE		;Fenster wieder einlesen.
			jsr	WM_LOAD_WIN_DATA

			pla
			tax
			rts

;--- Ist Fenster verdeckt ?
::1			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			ldx	#$05			;Größe des aktuellen
::2			lda	WM_DATA_Y0,x		;Fensters einlesen.
			sta	r2L       ,x
			dex
			bpl	:2

::3			inx
			stx	:tmp_Vec2Stack		;Zeiger auf Fensterstack speichern.

			lda	WindowStack,x
			bmi	:4			; => Nicht definiert, weiter...
			beq	:4			; => Desktop-Fenster, weiter...
			cmp	:tmp_WinNr		;Aktuelles Fenster ?
			beq	:5			; => Ja, übergehen...

			sta	WM_WCODE		;Fensterdaten einlesen.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_Y0		;Fenstergröße testen.
			cmp	r2H
			bcs	:4
			lda	WM_DATA_Y1
			cmp	r2L
			bcc	:4
			CmpW	WM_DATA_X0,r4
			bcs	:4
			CmpW	WM_DATA_X1,r3
			bcc	:4

			ldx	#WINDOW_BLOCKED		;Fenster blockiert, Ende...
			rts

::4			ldx	:tmp_Vec2Stack
			cpx	#MAX_WINDOWS -1		;Alle Fenster überprüft ?
			bcc	:3			; => Nein, weiter...

::5			ldx	#NO_ERROR
			rts

::tmp_Vec2Stack		b $00
::tmp_WinNr		b $00

;******************************************************************************
;Routine:		WM_UPDATE
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Aktualisiert und prüft das oberste Fenster.
;******************************************************************************
.WM_UPDATE		jsr	WM_DRAW_NO_TOP
			lda	WindowStack
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_GET_ICON_XY
			jsr	WM_TEST_WIN_POS
			jsr	WM_CALL_REDRAW
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_UPDATE_ALL
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Aktualisiert alle Fenster (außer Desktop).
;******************************************************************************
.WM_UPDATE_ALL		lda	#$00			;Hintergrundbild und Verknüpfungen
			sta	WM_WCODE		;aus Speicher holen und neuzeichnen.
			jsr	WM_LOAD_SCREEN

			lda	#MAX_WINDOWS -1		;Alle Fenster neu zeichnen.
::1			pha
			tax
			lda	WindowStack,x
			beq	:2
			bmi	:2
			sta	WM_WCODE
			jsr	WM_CALL_DRAW
::2			pla
			sec
			sbc	#$01
			bpl	:1
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_DRAW_ALL_WIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet alle Fenster neu.
;******************************************************************************
.WM_DRAW_ALL_WIN	jsr	WM_DRAW_NO_TOP

;******************************************************************************
;Routine:		WM_DRAW_TOP_WIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet oberstes Fenster neu.
;******************************************************************************
.WM_DRAW_TOP_WIN	lda	WindowStack
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_LOAD_SCREEN
			jsr	WM_DRAW_MOVER
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_DRAW_NO_TOP
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet bis auf oberstes alle Fenster neu.
;******************************************************************************
.WM_DRAW_NO_TOP		lda	WM_WCODE
			pha

			lda	#MAX_WINDOWS -1
::1			pha
			tax
			lda	WindowStack,x
			bmi	:2
			sta	WM_WCODE
			jsr	WM_LOAD_SCREEN
::2			pla
			sec
			sbc	#$01
			bne	:1

			pla
			sta	WM_WCODE
			jmp	WM_LOAD_WIN_DATA

;******************************************************************************
;Routine:		WM_CLEAR_SCREEN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Löscht alle Fenster vom Bildschirm.
;******************************************************************************
.WM_CLEAR_SCREEN	LoadB	dispBufferOn,ST_WR_FORE

			lda	sysRAMFlg
			and	#%00001000		;Hintergrundbild aktiv ?
			beq	WM_RESET_SCREEN		; => Nein, weiter...
			jmp	GetBackScreen

;******************************************************************************
;Routine:		WM_RESET_SCREEN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Setzt den Bildschirm auf GEOS-V2-Standard zurück.
;******************************************************************************
.WM_RESET_SCREEN	lda	BackScrPattern
			jsr	SetPattern
			jsr	WM_SET_MAX_WIN
			lda	C_GEOS_BACK
			jsr	DirectColor
			jmp	Rectangle

;******************************************************************************
;Routine:		WM_DRAW_SLCT_WIN
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet leeres Fenster mit System-Icons.
;******************************************************************************
.WM_DRAW_SLCT_WIN	jsr	WM_GET_SLCT_SIZE
			jsr	WM_DRAW_USER_WIN

			jsr	WM_NO_MARGIN

;--- System-Icons zeichnen.
			LoadW	r14,:WM_SYS1ICON_TAB
			LoadB	r15H,5
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Resize-Icons zeichnen.
			lda	WM_DATA_SIZE
			bne	:1

			LoadW	r14,:WM_SYS2ICON_TAB
			LoadB	r15H,4
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Move-UP/DOWN-Icons zeichnen.
::1			lda	WM_DATA_MOVEBAR
			beq	:2

			LoadW	r14,:WM_SYS3ICON_TAB
			LoadB	r15H,2
			MoveB	C_WinTitel,r13H
			jmp	WM_DRAW_ICON_TAB
::2			rts

;*** Angaben zur Ausgabe der Fenster-Icons.
::WM_SYS1ICON_TAB	w Icon_CL
			w WM_DEF_AREA_CL

			w Icon_DN
			w WM_DEF_AREA_DN

			w Icon_ST
			w WM_DEF_AREA_STD

			w Icon_MN
			w WM_DEF_AREA_MN

			w Icon_MX
			w WM_DEF_AREA_MX

::WM_SYS2ICON_TAB	w Icon_UL
			w WM_DEF_AREA_UL

			w Icon_UR
			w WM_DEF_AREA_UR

			w Icon_DL
			w WM_DEF_AREA_DL

			w Icon_DR
			w WM_DEF_AREA_DR

::WM_SYS3ICON_TAB	w Icon_PU
			w WM_DEF_AREA_WUP

			w Icon_PD
			w WM_DEF_AREA_WDN

;******************************************************************************
;Routine:		WM_DRAW_ICON_TAB
;Parameter:		r14  = Zeiger auf Icon-Tabelle.
;			r15H = Anzahl Icons.
;			r13H = Farbe
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster-Icons darstellen.
;			Unterprogramm für WM_DRAW_SLCT_WIN.
;******************************************************************************
:WM_DRAW_ICON_TAB	ldy	#$03
			lda	(r14L),y
			tax
			dey
			lda	(r14L),y
			jsr	CallRoutine

			lda	r13H
			jsr	DirectColor
			jsr	WM_CONVERT_PIXEL

			ldy	#$00
			lda	(r14L),y
			sta	r0L
			iny
			lda	(r14L),y
			sta	r0H
			LoadB	r2L,Icon_MoveW
			LoadB	r2H,Icon_MoveH
			jsr	BitmapUp

			AddVBW	4,r14

			dec	r15H
			bne	WM_DRAW_ICON_TAB
			rts

;******************************************************************************
;Routine:		WM_DRAW_TITLE
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Ausgabe der Titelzeile für aktuelles Fenster.
;******************************************************************************
.WM_DRAW_TITLE		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2L			;X/Y-Koordinaten berechnen.
			clc
			adc	#$06
			sta	r1H

			lda	r3L
			clc
			adc	#$14
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$2c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_TITLE +0	;Titelzeile ausgeben.
			ldx	WM_DATA_TITLE +1
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_DRAW_INFO
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Ausgabe der Infozeile für aktuelles Fenster.
;******************************************************************************
.WM_DRAW_INFO		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;X/Y-Koordinaten berechnen.
			sec
			sbc	#$02
			sta	r1H

			lda	r3L
			clc
			adc	#$0c
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$0c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_INFO +0		;Titelzeile ausgeben.
			ldx	WM_DATA_INFO +1
			jmp	CallRoutine

;******************************************************************************
;Routine:		WM_DRAW_USER_WIN
;Parameter:		r2-r4	Fenstergröße.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Freies Fenster ohne System-Icons zeichnen.
;******************************************************************************
.WM_DRAW_USER_WIN	PushB	r2L
			PushW	r3

			lda	C_WinBack
			jsr	DirectColor
			lda	#$00
			jsr	SetPattern
			jsr	Rectangle

			lda	r2L
			pha
			lda	r2H
			pha

;--- Kopfzeile zeichnen.
			lda	r2L
			clc
			adc	#$07
			sta	r2H
			lda	C_WinTitel
			jsr	DirectColor

;--- Fußzeile zeichnen.
			pla
			sta	r2H
			sec
			sbc	#$07
			sta	r2L

			lda	C_WinTitel
			jsr	DirectColor

			pla
			sta	r2L

			MoveB	r2H,r11L
			lda	#%11111111
			jsr	HorizontalLine

;--- Linken/recten Rand zeichnen.
			lda	r4H
			pha
			lda	r4L
			pha

			MoveW	r3,r4

			MoveB	r2L,r3L
			MoveB	r2H,r3H
			lda	#%11111111
			jsr	VerticalLine

			pla
			sta	r4L
			pla
			sta	r4H
			lda	#%11111111
			jsr	VerticalLine

			PopW	r3
			PopB	r2L
			rts

;******************************************************************************
;Routine:		WM_DRAW_FRAME
;Parameter:		r2-r4	Fenstergröße.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet Gummi-Band um aktuelles Fenster.
;******************************************************************************
.WM_DRAW_FRAME		PushB	r2L
			PushB	r2H
			ldx	r2L
			inx
			stx	r2H
			jsr	InvertRectangle
			PopB	r2H
			tax
			dex
			stx	r2L
			jsr	InvertRectangle
			PopB	r2L

			PushW	r3
			PushW	r4
			MoveW	r3,r4
			jsr	InvertRectangle
			PopW	r4
			MoveW	r4,r3
			jsr	InvertRectangle
			PopW	r3
			rts

;******************************************************************************
;Routine:		WM_DRAW_MOVER
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Zeichnet Scrollbalken.
;******************************************************************************
.WM_DRAW_MOVER		lda	WM_DATA_MOVEBAR
			bne	:1
			rts

::1			jsr	WM_GET_ICON_XY
			jsr	WM_GET_SLCT_SIZE

			ldx	#r4L
			ldy	#$03
			jsr	DShiftRight
			lda	r4L
			sta	:tmp_MovData +0
			lda	r2L
			clc
			adc	#$08
			sta	:tmp_MovData +1
			lda	r2H
			sec
			sbc	r2L
			sec
			sbc	#4*8
			sta	:tmp_MovData +2
			inc	:tmp_MovData +2

			lda	WM_DATA_MAXENTRY +0
			sta	:tmp_MovData +3
			lda	WM_DATA_MAXENTRY +1
			sta	:tmp_MovData +4

			lda	WM_DATA_CURENTRY +0
			sta	:tmp_MovData +7
			lda	WM_DATA_CURENTRY +1
			sta	:tmp_MovData +8

			lda	WM_COUNT_ICON_XY
			sta	:tmp_MovData +5
			lda	#$00
			sta	:tmp_MovData +6

			LoadW	r0,:tmp_MovData
			jsr	InitBalken
			jmp	PrintBalken

::tmp_MovData		b $00,$00,$00
			w $0000,$0000,$0000

;******************************************************************************
;Routine:		WM_FIND_JOBS
;Parameter:		r14	Zeiger auf Jobtabelle
;			r15H	Anzahl Jobs
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Sucht Job für ausgewähltes System-Icon.
;******************************************************************************
:WM_FIND_JOBS		php
			sei

			lda	#$00
			sta	r15L
::1			lda	r15L
			cmp	r15H
			beq	:4
			asl
			asl
			tay
			ldx	#$00
::2			lda	(r14L),y
			sta	r0L   ,x
			iny
			inx
			cpx	#$04
			bcc	:2

			lda	r0L
			ldx	r0H
			jsr	CallRoutine
			jsr	IsMseInRegion
			tax
			beq	:3

			plp

			lda	r1L
			ldx	r1H
			jmp	CallRoutine

::3			inc	r15L
			jmp	:1

::4			ldx	#JOB_NOT_FOUND
::5			plp
			rts

;******************************************************************************
;Routine:		WM_GET_ICON_X
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		WM_COUNT_ICON_XAnzahl Icons in X-Richtung.
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Berechnet Anzahl Icons pro Zeile.
;******************************************************************************
.WM_GET_ICON_X		ldx	WM_DATA_COLUMN
			bne	:3

;--- Anzahl Spalten nicht definiert,
;    Anzahl Spalten aus Fensterbreite und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_X
			sta	:CUR_GRID_X

;--- Fensterbreite berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r4L
			sec
			sbc	r3L
			sta	r0L
			lda	r4H
			sbc	r3H
			sta	r0H

			ldx	#r0L
			ldy	#$03
			jsr	DShiftRight

			ldx	r0L
			inx
			txa
			sec
			sbc	#$04

;--- Max. Anzahl Icons pro Zeile berechnen.
			ldx	#$00
::2			cmp	#$02
			bcc	:3
			tay
			bmi	:3
			sec
			sbc	:CUR_GRID_X
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_X
			rts

::CUR_GRID_X		b $00

;******************************************************************************
;Routine:		WM_GET_ICON_Y
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		WM_COUNT_ICON_YAnzahl Icons in Y-Richtung.
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Berechnet Anzahl Icons pro Spalte.
;******************************************************************************
.WM_GET_ICON_Y		ldx	WM_DATA_ROW
			bne	:3

;--- Anzahl Zeilen nicht definiert,
;    Anzahl Zeilen aus Fensterhöhe und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_Y
			sta	:CUR_GRID_Y

;--- Fensterhöhe berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;Fenstergröße - Rahmen - 7
			sec
			sbc	r2L
			sec
			sbc	#$17

;--- Max. Anzahl Zeilen im Fenster berechnen.
			ldx	#$00
::2			sec
			sbc	:CUR_GRID_Y
			bcc	:3
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_Y
			rts

::CUR_GRID_Y		b $00

;******************************************************************************
;Routine:		WM_GET_ICON_XY
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		WM_COUNT_ICON_XY									Anzahl Icons in Fenster.
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Berechnet Anzahl Icons pro Fenster.
;******************************************************************************
.WM_GET_ICON_XY		jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	#$00
			ldy	WM_COUNT_ICON_Y
			beq	:2
::1			clc
			adc	WM_COUNT_ICON_X
			dey
			bne	:1

::2			sta	WM_COUNT_ICON_XY
			rts

;******************************************************************************
;Routine:		WM_GET_GRID_X
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		AKKU	Breite für Spalte.
;Verändert:		A,X,Y,r0-r15
;Funktion:		Berechnet Breite einer Spalte.
;******************************************************************************
.WM_GET_GRID_X		lda	WM_DATA_GRID_X
			bne	:1
			lda	#WM_GRID_ICON_XC
::1			rts

;******************************************************************************
;Routine:		WM_GET_GRID_Y
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		AKKU	Höhe für Zeile.
;Verändert:		A,X,Y,r0-r15
;Funktion:		Berechnet Höhe einer Zeile.
;******************************************************************************
.WM_GET_GRID_Y		lda	WM_DATA_GRID_Y
			bne	:1
			lda	#WM_GRID_ICON_Y
::1			rts

;******************************************************************************
;Routine:		WM_TEST_WIN_POS
;Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:		WM_WIN_DATA_BUF
;Verändert:		A,X,Y,r0
;Funktion:		Prüft ob erster Eintrag in linker oberer Ecke innerhalb des
;			gültigen Bereichs liegt.
;******************************************************************************
:WM_TEST_WIN_POS	lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jsr	WM_TEST_CUR_POS
			lda	r0L
			sta	WM_DATA_CURENTRY +0
			lda	r0H
			sta	WM_DATA_CURENTRY +1
			jmp	WM_SAVE_WIN_DATA

;******************************************************************************
;Routine:		WM_TEST_CUR_POS
;Parameter:		r0	Zeiger auf aktuellen Eintrag.
;Rückgabe:		r0
;Verändert:		A,X,r0
;Funktion:		Prüft ob aktueller Eintrag in linker oberer Ecke innerhalb
;			des gültigen Bereichs liegt (Sub. von "WM_TEST_WIN_POS")
;******************************************************************************
:WM_TEST_CUR_POS	clc
			lda	r0L
			adc	WM_COUNT_ICON_XY
			tax
			lda	r0H
			adc	#$00

			cmp	WM_DATA_MAXENTRY +1
			bne	:1
			cpx	WM_DATA_MAXENTRY +0
::1			beq	:4
			bcc	:4

			sec
			lda	WM_DATA_MAXENTRY +0
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sbc	#$00
			sta	r0H
			bcc	:3

;			lda	r0L
;			clc
;			adc	WM_COUNT_ICON_X
;			tax
;			lda	r0H
;			adc	#$00
;
;			cmp	WM_DATA_MAXENTRY +1
;			bne	:2
;			cpx	WM_DATA_MAXENTRY +0
;::2			bcs	:4
;			stx	r0L
;			sta	r0H
			rts

::3			lda	#$00
			sta	r0L
			sta	r0H
::4			rts

;******************************************************************************
;Routine:		WM_WIN_MARGIN
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
;			Datei-Icons innerhalb des Fensterrahmens.
;******************************************************************************
.WM_WIN_MARGIN		lda	WM_WCODE

;******************************************************************************
;Routine:		WM_SET_MARGIN
;Parameter:		AKKU	Fenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
;			Datei-Icons innerhalb des Fensterrahmens.
;******************************************************************************
.WM_SET_MARGIN		pha
			jsr	WM_GET_WIN_SIZE

			lda	r3L
			clc
			adc	#$08
			sta	leftMargin +0
			lda	r3H
			adc	#$00
			sta	leftMargin +1

			lda	r4L
			sec
			sbc	#$08
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	r2L
			clc
			adc	#$08
			sta	windowTop

			lda	r2H
			sec
			sbc	#$08
			sta	windowBottom
			pla
			rts

;******************************************************************************
;Routine:		WM_NO_MARGIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A
;Funktion:		Löscht Grenzen für Textausgabe.
;******************************************************************************
.WM_NO_MARGIN		lda	#$00
			sta	windowTop
			sta	leftMargin  +0
			sta	leftMargin  +1
			lda	#SCR_HIGHT_40_80   -1
			sta	windowBottom
			lda	#< SCR_WIDTH_40 -1
			sta	rightMargin +0
			lda	#> SCR_WIDTH_40 -1
			sta	rightMargin +1
			rts

;******************************************************************************
;Routine:		WM_NO_MOUSE_WIN
;Parameter:		-
;Rückgabe:		-
;Verändert:		A
;Funktion:		Löscht Grenzen für Mausbewegung.
;******************************************************************************
.WM_NO_MOUSE_WIN	LoadB	mouseTop,$00
			LoadB	mouseBottom,$c7
			LoadW	mouseLeft,$0000
			LoadW	mouseRight,$013f
			rts

;******************************************************************************
;Routine:		WM_TEST_ENTRY
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0,r2-r4
;Funktion:		Prüft ob ein Datei-Icon angeklickt wurde.
;******************************************************************************
.WM_TEST_ENTRY		php
			sei

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			plp
			clc
			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			jsr	GD_SET_ENTRY2
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS
			jsr	IsMseInRegion
			tay
			pla
			tax
			tya
			beq	:4a

			ldx	CurEntry +0
			ldy	CurEntry +1
			plp
			sec
			rts

::4a			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			plp
			clc
			rts

;******************************************************************************
;Routine:		WM_SET_ENTRY
;Parameter:		r0	Nr. des gesuchten Eintrages.
;Rückgabe:		r1L,r1H	X(C) und Y(P)-Position für Icon.
;			XREG	$00 = Icon ist sichtbar.
;			XREG	$FF = Icon ist nicht sichtbar.
;Verändert:		A,X,Y,r0-r4
;Funktion:		Berechnet Position für Icon in Fenster.
;******************************************************************************
.WM_SET_ENTRY		PushW	r0
			jsr	WM_GET_ICON_XY
			jsr	WM_GET_GRID_X
			sta	:CurGrid_X
			jsr	WM_GET_GRID_Y
			sta	:CurGrid_Y
			jsr	WM_GET_SLCT_SIZE
			PopW	r0

			lda	WM_DATA_CURENTRY +0
			sta	r4L
			lda	WM_DATA_CURENTRY +1
			sta	r4H

			CmpW	r0,r4
			bcs	:2
::1			ldx	#$ff
			rts

::2			clc
			lda	r3L
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H
			ldx	#r3L
			ldy	#$03
			jsr	DShiftRight
			lda	r3L
			sta	:MinIcon_X

			lda	r2L
			clc
			adc	#$10
			sta	r1H
			lda	WM_COUNT_ICON_Y
			sta	:MaxIcon_Y

::3			lda	:MinIcon_X
			sta	r1L
			lda	WM_COUNT_ICON_X
			sta	:MaxIcon_X

::4			CmpW	r4,r0
			bne	:5
			ldx	#$00
			rts

::5			inc	r4L
			bne	:6
			inc	r4H

::6			dec	:MaxIcon_X
			bne	:7
			dec	:MaxIcon_Y
			beq	:1

			clc
			lda	r1H
			adc	:CurGrid_Y
			sta	r1H
			jmp	:3

::7			clc
			lda	r1L
			adc	:CurGrid_X
			sta	r1L
			jmp	:4

::MinIcon_X		b $00
::MaxIcon_X		b $00
::MaxIcon_Y		b $00
::CurGrid_X		b $00
::CurGrid_Y		b $00

;******************************************************************************
;Routine:		WM_CONVERT_CARDS
;Parameter:		r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
;Rückgabe:		-
;Verändert:		A,X,Y,r2-r4
;Funktion:		Berechnet Pixelgröße von Icon.
;******************************************************************************
.WM_CONVERT_CARDS	MoveB	r1L,r3L
			LoadB	r3H,0
			ldx	#r3L
			ldy	#$03
			jsr	DShiftLeft

			MoveB	r2L,r4L
			LoadB	r4H,0
			ldx	#r4L
			ldy	#$03
			jsr	DShiftLeft

			lda	r3L
			clc
			adc	r4L
			sta	r4L
			lda	r3H
			adc	r4H
			sta	r4H

			lda	r4L
			bne	:1
			dec	r4H
::1			dec	r4L

			lda	r1H
			sta	r2L
			clc
			adc	r2H
			sta	r2H
			dec	r2H
			rts

;******************************************************************************
;Routine:		WM_CONVERT_PIXEL
;Parameter:		r3  = X-Koordinate (WORD, Pixel)
;			r2L = Y-Koordinate (BYTE, Pixel)
;Rückgabe:		r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;Verändert:		A,r1L,r1H
;Funktion:		Wandelt X/Y von Pixel nach CARDS/Pixel um.
;******************************************************************************
.WM_CONVERT_PIXEL	lda	r3H			;X-Koordinate in CARDs
			lsr				;umrechnen.
			lda	r3L
			ror
			lsr
			lsr
			sta	r1L

			lda	r2L			;Y-Koordinate übernehmen.
			sta	r1H
			rts

;******************************************************************************
;Routine:		WM_GET_SLCT_AREA
;Parameter:		-
;Rückgabe:		r2L-r4	Gewählter Rahmenbereich.
;			r5L	Markierungsmodus:
;				$00 = Links nach rechts.
;				$ff = Rechts nach Links.
;Verändert:		A,X,Y,r2-r5L
;Funktion:		Auswahlfenster erstellen.
;******************************************************************************
.WM_GET_SLCT_AREA	jsr	:SET_MOUSE_FRAME

			lda	mouseXPos +0
			sta	:ClickX   +0
			lda	mouseXPos +1
			sta	:ClickX   +1
			lda	mouseYPos
			sta	:ClickY

			AddVBW	8,mouseXPos
			AddVB	8,mouseYPos

::1			MoveW	mouseXPos,:MouseX
			MoveB	mouseYPos,:MouseY
			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME

::2			lda	mouseData
			bmi	:3
			lda	inputData
			bmi	:2

			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME
			jmp	:1

::3			jsr	:SET_FRAME
			jsr	WM_DRAW_FRAME

			ldx	#$00
			CmpW	:ClickX,:MouseX
			bcc	:4
			dex
::4			stx	r5L
			jmp	WM_NO_MOUSE_WIN

;--- Rahmenposition setzen.
::SET_FRAME		CmpW	:MouseX,:ClickX
			bcs	:11
			MoveW	:MouseX,r3
			MoveW	:ClickX,r4
			jmp	:12

::11			MoveW	:ClickX,r3
			MoveW	:MouseX,r4

::12			CmpB	:MouseY,:ClickY
			bcs	:13
			MoveB	:MouseY,r2L
			MoveB	:ClickY,r2H
			jmp	:14

::13			MoveB	:ClickY,r2L
			MoveB	:MouseY,r2H
::14			rts

;--- Mausgrenzen setzen.
::SET_MOUSE_FRAME	lda	WM_WCODE
			jsr	WM_GET_WIN_SIZE

			lda	r3L
			clc
			adc	#$08
			sta	mouseLeft +0
			lda	r3H
			adc	#$00
			sta	mouseLeft +1

			lda	r4L
			sec
			sbc	#$08
			sta	mouseRight +0
			lda	r4H
			sbc	#$00
			sta	mouseRight +1

			lda	r2L
			clc
			adc	#$08
			sta	mouseTop

			lda	r2H
			sec
			sbc	#$08
			sta	mouseBottom
			rts

;--- Variablen.
::ClickX		w $0000
::ClickY		b $00
::MouseX		w $0000
::MouseY		b $00
