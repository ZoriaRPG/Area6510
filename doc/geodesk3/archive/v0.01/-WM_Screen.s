﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:		WM_SAVE_SCREEN
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r4
;Funktion:		Speichert Bildschirm-Inhalt in ScreenBuffer.
;******************************************************************************
.WM_SAVE_SCREEN		ldy	#$90			;Job-Code für ":StashRAM"
			b $2c

;******************************************************************************
;Routine:		WM_LOAD_SCREEN
;Parameter:		WM_WCODEFenster-Nr.
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r4
;Funktion:		Lädt Bildschirm-Inhalt aus ScreenBuffer.
;******************************************************************************
.WM_LOAD_SCREEN		ldy	#$91			;Job-Code für ":FetchRAM"
			sty	DataJobCode		;Aktuellen Job-Code speichern.

			php
			sei

			lda	dispBufferOn		;Bildschirmflag retten und
			pha				;Grafik nur in Vordergrund.
			lda	#ST_WR_FORE
			sta	dispBufferOn

			PushW	r10
			PushW	r11

			lda	WM_WCODE
			asl
			tay
			lda	:WIN_DATA_GRFX +0,y
			sta	r10L
			lda	:WIN_DATA_GRFX +1,y
			sta	r10H
			lda	:WIN_DATA_COLS +0,y
			sta	r11L
			lda	:WIN_DATA_COLS +1,y
			sta	r11H

			jsr	WM_GET_SLCT_SIZE

			lda	G3_SCRN_STACK
			jsr	WM_JOB_SVLD_SCRN

			PopW	r11
			PopW	r10

			pla
			sta	dispBufferOn		;Bildschirmflag zurücksetzen.
			plp

			lda	#$ff
			sta	DB_DELTA_Y
			sta	DB_DELTA_X
			rts

::WIN_DATA_GRFX		w $0000
			w 8192 *1
			w 8192 *2
			w 8192 *3
			w 8192 *4
			w 8192 *5
			w 8192 *6
::WIN_DATA_COLS		w 8192 *7 +1024*0
			w 8192 *7 +1024*1
			w 8192 *7 +1024*2
			w 8192 *7 +1024*3
			w 8192 *7 +1024*4
			w 8192 *7 +1024*5
			w 8192 *7 +1024*6

