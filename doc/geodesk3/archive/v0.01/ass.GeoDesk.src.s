﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			o $4000
			c "ass.SysFile V1.0"
			n "ass.GeoDesk"
			a "Markus Kanet"
			h "Steuerdatei für AutoAssembler-Modus des MegaAssembler V3."
			f $04

;b $f0,name,0		Datei für MegaAssembler/Linker angeben
;b $f1				Benutzerdefinierte Routine ausführen.
;				Am Ende in :a0 einen Zeiger auf das
;				nächste Befehlsbyte übergeben und die
;				Routinee mit 'RTS' beenden.
;b $f2,DEVICE		Quelltext-Laufwerk wechseln (8-11).
;b $f4				Zum Linker wechseln.
;b $f5				Zum MegaAssembler wechseln.
;b $ff				AutoAssembler beenden.

:MainInit		b $f0,"s.WM",$00
			b $f0,"s.DLG",$00
			b $f0,"s.LNK",$00
			b $f0,"s.GeoDesk",$00
			b $ff

;Erlaubte Dateigroesse: 16384 Bytes
;Datenspeicher von $4000-$4fff
			g $4fff
