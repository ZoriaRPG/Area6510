﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:		InitForWM
;Parameter:		-
;Rückgabe:		-
;Verändert:		A
;Funktion:		ScrollBalken verschieben.
;******************************************************************************
.WM_MOVE_POS		CmpW	SB_MaxEntry,SB_MaxEScr
			bcc	:11
			jsr	IsMseOnPos		;Position der Maus ermitteln.
			cmp	#$01			;Oberhalb des Anzeigebalkens ?
			beq	:12			;Ja, eine Seite zurück.
			cmp	#$02			;Auf dem Anzeigebalkens ?
			beq	:13			;Ja, Balken verschieben.
			cmp	#$03			;Unterhalb des Anzeigebalkens ?
			beq	:14			;Ja, eine Seite vorwärts.
::11			rts

::12			jmp	:MOVE_LAST_PAGE
::13			jmp	:MOVE_NEW_POS
::14			jmp	:MOVE_NEXT_PAGE

;*** Balken verschieben.
::MOVE_NEW_POS		jsr	StopMouseMove		;Mausbewegung einschränken.

::MOVE_NEXT		jsr	UpdateMouse		;Mausdaten aktualisieren.

			ldx	mouseData		;Maustaste noch gedrückt ?
			bmi	:21			; => Nein, neue Position anzeigen.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			jsr	UpdateMouse		;Mausdaten aktualisieren.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			cmp	#$06			;Maus nach unten ?
			beq	:MOVE_DOWN		; => Ja, auswerten.
			cmp	#$02			;Maus nach oben ?
			beq	:MOVE_UP		; => Ja, auswerten.
			bne	:MOVE_NEXT		; => Nein, Schleife...

;--- Balken neu positionieren.
::21			jsr	:MOVE_TO_FILE		;Position in Dateiliste berechnen.
			ClrB	pressFlag		;Maustastenklick löschen.
			jsr	WM_NO_MOUSE_WIN
			ldx	#$00
			rts

;--- Balken nach oben.
::MOVE_UP		lda	SB_Top			;Am oberen Rand ?
			beq	:MOVE_NEXT		; =: Ja, Abbruch...
			dec	mouseTop
			dec	mouseBottom
			dec	SB_Top
			dec	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;--- Balken nach unten.
::MOVE_DOWN		lda	SB_Top
			clc
			adc	SB_Length
			cmp	SB_MaxYlen
			bcs	:MOVE_NEXT
			inc	mouseTop
			inc	mouseBottom
			inc	SB_Top
			inc	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;*** Mausposition in Listenposition umrechnen.
::MOVE_TO_FILE		lda	SB_Top			;Aktuelle Mausposition in
			sta	r0L			;Position in Tabelle umrechnen.
			lda	#$00
			sta	r0H

			sec
			lda	SB_MaxEntry +0
			sbc	SB_MaxEScr  +0
			sta	r11L
			lda	SB_MaxEntry +1
			sbc	SB_MaxEScr  +1
			sta	r11H

			ldx	#r0L
			ldy	#r11L
			jsr	DMult

			lda	SB_End
			sec
			sbc	SB_Top
			sta	r10L
			inc	r10L
			lda	SB_MaxYlen
			sec
			sbc	r10L
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r0L
			ldy	#r11L
			jsr	Ddiv
			jmp	WM_SET_NEW_POS

;*** Eine Seite vorwärts.
::MOVE_NEXT_PAGE	clc
			lda	WM_COUNT_ICON_XY
			adc	r0L
			sta	r0L
			lda	#$00
			adc	r0H
			sta	r0H
			jsr	WM_TEST_CUR_POS
			jmp	:31

;*** Eine Seite zurück.
::MOVE_LAST_PAGE	lda	mouseData		;Mausbutton gedrückt ?
			bpl	:MOVE_LAST_PAGE		; => Nein, Ende...
			ClrB	pressFlag

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:31
			lda	#$00
			sta	r0L
			sta	r0H
::31			jsr	WM_SET_NEW_POS

::32			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:32			; => Nein, Ende...
			ClrB	pressFlag
			ldx	#$00
			rts
