﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Laufwerk öffnen.
;Bei Nicht-CMD-Laufwerken :OpenDisk ausführen.
;Bei CMD-Laufwerken Partition und ggf. Verzeichnis öffnen.
.OpenWinDrive		lda	WM_WCODE
.OpenUserWinDrive	sta	:curDrive
			tax
			ldy	WMODE_DRIVE,x
			lda	driveType -8,y
			bne	:2
			ldx	#DEV_NOT_FOUND
::1			rts

::2			tya
			jsr	SetDevice
			txa
			bne	:1

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_PARTITION!SET_MODE_SUBDIR
			bne	:3
			jmp	OpenDisk

::3			pha
			and	#SET_MODE_PARTITION
			beq	:4

			ldx	:curDrive
			lda	WMODE_PART,x
			sta	r3H
			jsr	OpenPartition

::4			pla
			and	#SET_MODE_SUBDIR
			beq	:1

			ldx	:curDrive
			lda	WMODE_SDIR_T,x
			sta	r1L
			lda	WMODE_SDIR_S,x
			sta	r1H
			jmp	OpenSubDir

::curDrive		b $00
