﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Bildschirm für Dialogbox wiederherstellen/zwischenspeichern.
.WM_SAVE_AREA		ldy	#$90			;Job-Code für ":StashRAM"
			b $2c
.WM_LOAD_AREA		ldy	#$91			;Job-Code für ":FetchRAM"
			sty	DataJobCode		;Aktuellen Job-Code speichern.
			tax
			php
			sei

			lda	dispBufferOn		;Bildschirmflag retten und
			pha				;Grafik nur in Vordergrund.
			lda	#ST_WR_FORE
			sta	dispBufferOn

			txa
			jsr	WM_JOB_SVLD_SCRN

			pla
			sta	dispBufferOn		;Bildschirmflag zurücksetzen.
			plp

			lda	#$ff
			sta	DB_DELTA_Y
			sta	DB_DELTA_X
			rts

:WM_JOB_SVLD_SCRN	sta	DataBank

			lda	r3H			;Linken Rand der Box in Cards
			lsr				;berechnen.
			lda	r3L
			and	#%11111000
			sta	r3L
			ror
			lsr
			lsr
			sta	RX_Margin_Left
			ldx	DB_DELTA_X
			cpx	#$ff
			beq	:1
			txa
::1			sta	DB_Margin_Left

			lda	r2L			;Oberen Rand der Box in Cards
			lsr				;berechnen.
			lsr
			lsr
			sta	RX_Margin_Top
			ldx	DB_DELTA_Y
			cpx	#$ff
			beq	:2
			txa
::2			sta	DB_Margin_Top		;Startwert für Job-Routine.

			lda	r2H			;Höhe der Box in Cards
			lsr				;berechnen.
			lsr
			lsr
			sec
			sbc	RX_Margin_Top
			clc
			adc	#$01
			sta	CountLines

			lda	r4L			;Anzahl Grafik-Bytes pro Zeile
			ora	#%00000111
			sta	r4L
			sec				;berechnen.
			sbc	r3L
			sta	r0L
			lda	r4H
			sbc	r3H
			sta	r0H

			inc	r0L
			bne	:3
			inc	r0H
::3			lda	r0L			;Anzahl Bytes in Zwischenspeicher.
			sta	GrfxDataBytes +0
			lda	r0H
			sta	GrfxDataBytes +1

			ldx	#r0L			;Bytes in Cards umrechnen.
			ldy	#$03
			jsr	DShiftRight

			lda	r0L			;Anzahl Cards in Zwischenspeicher.
			sta	ColsDataBytes

::4			jsr	DefCurLineCols		;Zeiger auf Farbdaten berechnen.
			ldy	DataJobCode		;Job-Code einlesen und
			jsr	DoRAMOp			;Daten speichern/einlesen.
			jsr	DefCurLineGrfx		;Zeiger auf Grafikdaten berechnen.
			ldy	DataJobCode		;Job-Code einlesen und
			jsr	DoRAMOp			;Daten speichern/einlesen.

			inc	DB_Margin_Top		;Zeiger auf nächste Zeile.
			inc	RX_Margin_Top		;Zeiger auf nächste Zeile.
			dec	CountLines		;Zähler korrigieren.
			bne	:4			;Box weiter bearbeiten.

::5			rts

;*** Daten für aktuelle Zeile definieren.
:DefCurLineGrfx		ldx	DB_Margin_Top		;Zeiger auf Grafikzeile
			lda	:SCRN_LINE_L,x		;berechnen.
			clc
			adc	#<SCREEN_BASE
			sta	r0L
			lda	:SCRN_LINE_H,x
			adc	#>SCREEN_BASE
			sta	r0H

			ldx	RX_Margin_Top		;Zeiger auf Grafikzeile
			lda	:SCRN_LINE_L,x		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r10L
			sta	r1L
			lda	:SCRN_LINE_H,x
			adc	r10H
			sta	r1H

			ldx	DB_Margin_Left		;Zeiger auf Grafikzeile
			lda	:SCRN_COLUMN_L,x	;berechnen.
			clc
			adc	r0L
			sta	r0L
			lda	:SCRN_COLUMN_H,x
			adc	r0H
			sta	r0H

			ldx	RX_Margin_Left		;Zeiger auf Grafikzeile
			lda	:SCRN_COLUMN_L,x	;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r1L
			sta	r1L
			lda	:SCRN_COLUMN_H,x
			adc	r1H
			sta	r1H

			lda	GrfxDataBytes +0	;Anzahl Grafikbytes festlegen.
			sta	r2L
			lda	GrfxDataBytes +1
			sta	r2H

			lda	DataBank		;MegaPatch-Bank in REU festlegen.
			sta	r3L

::3			rts

;*** Startadressen der Grafikzeilen.
::SCRN_LINE_L		b <  0*8*40,<  1*8*40,<  2*8*40,<  3*8*40
			b <  4*8*40,<  5*8*40,<  6*8*40,<  7*8*40
			b <  8*8*40,<  9*8*40,< 10*8*40,< 11*8*40
			b < 12*8*40,< 13*8*40,< 14*8*40,< 15*8*40
			b < 16*8*40,< 17*8*40,< 18*8*40,< 19*8*40
			b < 20*8*40,< 21*8*40,< 22*8*40,< 23*8*40
			b < 24*8*40

::SCRN_LINE_H		b >  0*8*40,>  1*8*40,>  2*8*40,>  3*8*40
			b >  4*8*40,>  5*8*40,>  6*8*40,>  7*8*40
			b >  8*8*40,>  9*8*40,> 10*8*40,> 11*8*40
			b > 12*8*40,> 13*8*40,> 14*8*40,> 15*8*40
			b > 16*8*40,> 17*8*40,> 18*8*40,> 19*8*40
			b > 20*8*40,> 21*8*40,> 22*8*40,> 23*8*40
			b > 24*8*40

;*** Startadressen der Grafikspalten.
::SCRN_COLUMN_L		b < 8 * 0 ,< 8 * 1 ,< 8 * 2 ,< 8 * 3
			b < 8 * 4 ,< 8 * 5 ,< 8 * 6 ,< 8 * 7
			b < 8 * 8 ,< 8 * 9 ,< 8 * 10,< 8 * 11
			b < 8 * 12,< 8 * 13,< 8 * 14,< 8 * 15
			b < 8 * 16,< 8 * 17,< 8 * 18,< 8 * 19
			b < 8 * 20,< 8 * 21,< 8 * 22,< 8 * 23
			b < 8 * 24,< 8 * 25,< 8 * 26,< 8 * 27
			b < 8 * 28,< 8 * 29,< 8 * 30,< 8 * 31
			b < 8 * 32,< 8 * 33,< 8 * 34,< 8 * 35
			b < 8 * 36,< 8 * 37,< 8 * 38,< 8 * 39

::SCRN_COLUMN_H		b > 8 * 0 ,> 8 * 1 ,> 8 * 2 ,> 8 * 3
			b > 8 * 4 ,> 8 * 5 ,> 8 * 6 ,> 8 * 7
			b > 8 * 8 ,> 8 * 9 ,> 8 * 10,> 8 * 11
			b > 8 * 12,> 8 * 13,> 8 * 14,> 8 * 15
			b > 8 * 16,> 8 * 17,> 8 * 18,> 8 * 19
			b > 8 * 20,> 8 * 21,> 8 * 22,> 8 * 23
			b > 8 * 24,> 8 * 25,> 8 * 26,> 8 * 27
			b > 8 * 28,> 8 * 29,> 8 * 30,> 8 * 31
			b > 8 * 32,> 8 * 33,> 8 * 34,> 8 * 35
			b > 8 * 36,> 8 * 37,> 8 * 38,> 8 * 39

;*** Daten für aktuelle Zeile definieren.
:DefCurLineCols		ldx	DB_Margin_Top		;Zeiger auf Grafikzeile
			lda	:COLOR_LINE_L,x		;berechnen.
			clc
			adc	#<COLOR_MATRIX
			sta	r0L
			lda	:COLOR_LINE_H,x
			adc	#>COLOR_MATRIX
			sta	r0H

			ldx	RX_Margin_Top		;Zeiger auf Grafikzeile
			lda	:COLOR_LINE_L,x		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r11L
			sta	r1L
			lda	:COLOR_LINE_H,x
			adc	r11H
			sta	r1H

			lda	DB_Margin_Left		;Zeiger auf Grafikzeile
			clc				;berechnen.
			adc	r0L
			sta	r0L
			lda	#$00
			adc	r0H
			sta	r0H

			lda	RX_Margin_Left		;Zeiger auf Zwischenspeicher
			clc				;berechnen.
			adc	r1L
			sta	r1L
			lda	#$00
			adc	r1H
			sta	r1H

			lda	ColsDataBytes		;Anzahl Farbbytes festlegen.
			sta	r2L
			lda	#$00
			sta	r2H

			lda	DataBank		;MegaPatch-Bank in REU festlegen.
			sta	r3L
::3			rts

;*** Startadressen der Farbzeilen.
::COLOR_LINE_L		b <  0*40,<  1*40,<  2*40,<  3*40
			b <  4*40,<  5*40,<  6*40,<  7*40
			b <  8*40,<  9*40,< 10*40,< 11*40
			b < 12*40,< 13*40,< 14*40,< 15*40
			b < 16*40,< 17*40,< 18*40,< 19*40
			b < 20*40,< 21*40,< 22*40,< 23*40
			b < 24*40

::COLOR_LINE_H		b >  0*40,>  1*40,>  2*40,>  3*40
			b >  4*40,>  5*40,>  6*40,>  7*40
			b >  8*40,>  9*40,> 10*40,> 11*40
			b > 12*40,> 13*40,> 14*40,> 15*40
			b > 16*40,> 17*40,> 18*40,> 19*40
			b > 20*40,> 21*40,> 22*40,> 23*40
			b > 24*40
