﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

*************
Routine:		InitForWM
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Initialisiert die Mausabfrage für ":mouseVector".
*************
Routine:		DoneWithWM
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Deaktiviert die Mausabfrage über ":otherPressVec".
*************
Routine:		WM_IS_WIN_FREE
Parameter:		-
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y
Funktion:		Sucht nach einer freien Fenster-Nr.
*************
Routine:		WM_COPY_WIN_DATA
Parameter:		r0	Zeiger auf Datentabelle.
Rückgabe:		-
Verändert:		A,Y
Funktion:		Kopiert Datentabelle in Zwischenspeicher.
*************
Routine:		WM_LOAD_WIN_DATA
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Kopiert Fensterdaten in Zwischenspeicher.
*************
Routine:		WM_SAVE_WIN_DATA
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Kopiert Daten aus Zwischenspeicher in Tabelle mit Fensterdaten.
*************
Routine:		WM_WIN2TOP
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Setzt Fenster-Nr. (AKKU) an erste Stelle im Stack.
*************
Routine:		WM_DEF_STD_WSIZE
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y
Funktion:		Definiert Fenstergröße in Abhängigkeit von der aktuellen
			Fenster-Nr. Die einzelnen Fenster werden dabei "überlappend"
			angeordnet!
*************
Routine:		WM_GET_SLCT_SIZE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Fenstergröße für aktuelles Fenster.
*************
Routine:		WM_GET_WIN_SIZE
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Fenstergröße für gewähltes Fenster.
*************
Routine:		WM_SET_MAX_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert max. Fenstergröße.
*************
Routine:		WM_SET_STD_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Definiert Standard-Fenstergröße.
*************
Routine:		WM_CALL_DRAW
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet das angegebene Fenster mit Inhalt.
*************
Routine:		WM_CALL_REDRAW
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet das angegebene Fenster neu.
*************
Routine:		WM_CALLRIGHTCLK
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Klick mit rechter Maustaste auswerten.
*************
Routine:		WM_CALL_MOVE
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Fensterinhalt verschieben.
*************
Routine:		WM_CALL_EXEC
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Mausklick in Fenster auswerten.
*************
Routine:		WM_CALL_EXIT
Parameter:		WM_WCODEFenster-Nr.
			WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Fenster zum schließen vorbereiten.
*************
Routine:		WM_OPEN_WINDOW
Parameter:		r0	Zeiger auf Datentabelle.
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y,r0-r15
Funktion:		Öffnet und zeichnet ein neues Fenster mit Inhalt.
*************
Routine:		WM_USER_WINDOW
Parameter:		AKKU	Fenster-Nr.
			r0	Zeiger auf Datentabelle.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Schließt ein geöffneten Fensters.
*************
Routine:		WM_CLOSE_WINDOW
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		AKKU	Fenster-Nr.
			XREG	$00=Kein Fehler.
Verändert:		A,X,Y,r0-r15
Funktion:		Öffnet und zeichnet ein Fenster mit Inhalt innerhalb eines
			bereits geöffneten Fensters (z.B. Arbeitsplatz=>Laufwerk öffnen)
*************

*************
Routine:		WM_CHK_MOUSE
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Wertet Mausklick auf DeskTop/Fenster/Icon aus.
*************
Routine:		WM_FIND_WINDOW
Parameter:		-
Rückgabe:		XREG	$00=Kein Fehler.
			YREG	Fenster-Nr.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Wertet Mausklick auf DeskTop/Fenster/Icon aus.
*************
Routine:		WM_WIN_BLOCKED
Parameter:		-
Rückgabe:		XREG	$00=Fenster nicht verdeckt.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Prüft ob aktuelles Fenster verdeckt ist.
*************
Routine:		WM_UPDATE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Aktualisiert und prüft das oberste Fenster.
*************
Routine:		WM_UPDATE_ALL
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Aktualisiert alle Fenster.
*************
Routine:		WM_DRAW_ALL_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet alle Fenster neu.
*************
Routine:		WM_DRAW_TOP_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet oberstes Fenster neu.
*************
Routine:		WM_DRAW_NO_TOP
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet bis auf oberstes alle Fenster neu.
*************
Routine:		WM_CLEAR_SCREEN
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Löscht den aktuellen Bildschirm.
*************
Routine:		WM_DRAW_SLCT_WIN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet leeres Fenster mit System-Icons.
*************
Routine:		WM_DRAW_TITLE
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Ausgabe der Titelzeile für aktuelles Fenster.
*************
Routine:		WM_DRAW_INFO
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Ausgabe der Infozeile für aktuelles Fenster.
*************
Routine:		WM_DRAW_USER_WIN
Parameter:		r2-r4	Fenstergröße.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet leeres Fenster ohne System-Icons.
*************
Routine:		WM_DRAW_FRAME
Parameter:		r2-r4	Fenstergröße.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet Gummi-Band um aktuelles Fenster.
*************
Routine:		WM_DRAW_MOVER
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Zeichnet Scrollbalken.
*************
Routine:		WM_FIND_JOBS
Parameter:		r14	Zeiger auf Jobtabelle
			r15H	Anzahl Jobs
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Sucht Job für ausgewähltes System-Icon.
*************
Routine:		WM_DEF_AREA...
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Bereich für Icon definieren.
*************
Routine:		WM_FUNC...
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0-r15
Funktion:		Funktion für Icon ausführen.
*************
Routine:		WM_GET_ICON_X
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_XAnzahl Icons in X-Richtung.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Zeile.
*************
Routine:		WM_GET_ICON_Y
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_YAnzahl Icons in Y-Richtung.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Spalte.
*************
Routine:		WM_GET_ICON_XY
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_COUNT_ICON_XY									Anzahl Icons in Fenster.
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Berechnet Anzahl Icons pro Fenster.
*************
Routine:		WM_GET_GRID_X
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		AKKU	Breite für Spalte.
Verändert:		A,X,Y,r0-r15
Funktion:		Berechnet Breite einer Spalte.
*************
Routine:		WM_GET_GRID_Y
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		AKKU	Höhe für Zeile.
Verändert:		A,X,Y,r0-r15
Funktion:		Berechnet Höhe einer Zeile.
*************

*************
Routine:		WM_TEST_WIN_POS
Parameter:		WM_WIN_DATA_BUFDaten für aktuelles Fenster.
Rückgabe:		WM_WIN_DATA_BUF
Verändert:		A,X,Y,r0
Funktion:		Prüft ob erster Eintrag in linker oberer Ecke innerhalb des
			gültigen Bereichs liegt.
*************
Routine:		WM_TEST_CUR_POS
Parameter:		r0	Zeiger auf aktuellen Eintrag.
Rückgabe:		r0
Verändert:		A,X,r0
Funktion:		Prüft ob aktueller Eintrag in linker oberer Ecke innerhalb des
			gültigen Bereichs liegt (Sub. von "WM_TEST_WIN_POS")
*************
Routine:		WM_WIN_MARGIN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
			Datei-Icons innerhalb des Fensterrahmens.
*************
Routine:		WM_SET_MARGIN
Parameter:		AKKU	Fenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Setzt Grenzen für Textausgabe auf Bereich für
			Datei-Icons innerhalb des Fensterrahmens.
*************
Routine:		WM_NO_MARGIN
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Löscht Grenzen für Textausgabe.
*************
Routine:		WM_NO_MOUSE_WIN
Parameter:		-
Rückgabe:		-
Verändert:		A
Funktion:		Löscht Grenzen für Mausbewegung.
*************
Routine:		WM_TEST_ENTRY
Parameter:		-
Rückgabe:		-
Verändert:		A,X,Y,r0,r2-r4
Funktion:		Prüft ob ein Datei-Icon angeklickt wurde.
*************
Routine:		WM_SET_ENTRY
Parameter:		r0	Nr. des gesuchten Eintrages.
Rückgabe:		r1L,r1H	X(C) und Y(P)-Position für Icon.
			XREG	$00 = Icon ist sichtbar.
Verändert:		A,X,Y,r0-r4
Funktion:		Berechnet Position für Icon in Fenster.
*************
Routine:		WM_SAVE_SCREEN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r4
Funktion:		Speichert Bildschirm-Inhalt in ScreenBuffer.
*************
Routine:		WM_LOAD_SCREEN
Parameter:		WM_WCODEFenster-Nr.
Rückgabe:		-
Verändert:		A,X,Y,r0-r4
Funktion:		Lädt Bildschirm-Inhalt aus ScreenBuffer.
*************
Routine:		WM_CONVERT_CARDS
Parameter:		r1L,r1H,r2L,r2HWandelt Bitmap nach Pixel.
Rückgabe:		-
Verändert:		A,X,Y,r2-r4
Funktion:		Berechnet Pixelgröße von Icon.
*************
Routine:		WM_GET_SLCT_AREA
Parameter:		-
Rückgabe:		r2L - r4Gewählte Rahmengröße
			r5L	SelectMode: $00=Ganz/$FF=Teilweise
Verändert:		A,X,Y,r2-r5L
Funktion:		Auswahlfenster erstellen.
*************
