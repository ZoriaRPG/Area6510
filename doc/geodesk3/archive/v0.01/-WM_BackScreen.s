﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Aktuellen Bildschirm in Hintergrundspeicher kopieren.
;    Wird verwendet für Menüs usw.
.WM_SAVE_BACKSCR	ldx	#$00
::51			lda	r0L,x
			pha
			inx
			cpx	#$07
			bcc	:51

			lda	G3_SYSTEM_BUF
			sta	r3L

			LoadW	r0,SCREEN_BASE
			LoadW	r1,G3_BACKSCR_BUF
			LoadW	r2,8000
			jsr	StashRAM

			LoadW	r0,COLOR_MATRIX
			LoadW	r1,G3_BACKCOL_BUF
			LoadW	r2,1000
			jsr	StashRAM

			ldx	#$06
::52			pla
			sta	r0L,x
			dex
			bpl	:52
			rts

;*** Aktuellen Bildschirm aus Hintergrundspeicher kopieren.
;    Wird verwendet für Menüs usw.
.WM_LOAD_BACKSCR	ldx	#$00
::51			lda	r0L,x
			pha
			inx
			cpx	#$07
			bcc	:51

			lda	G3_SYSTEM_BUF
			sta	r3L

			LoadW	r0,SCREEN_BASE
			LoadW	r1,G3_BACKSCR_BUF
			LoadW	r2,8000
			jsr	FetchRAM

			LoadW	r0,COLOR_MATRIX
			LoadW	r1,G3_BACKCOL_BUF
			LoadW	r2,1000
			jsr	FetchRAM

			ldx	#$06
::52			pla
			sta	r0L,x
			dex
			bpl	:52
			rts
