﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "o.WM"
			t "TopSym"
			t "TopSym/G3"
			t "TopMac/G3"
			t "SymbTab/WM"

			f DATA
			o $0400

.BASE_DIR_DATA = $6400

.Flag_ViewALTitle	b $00
.Flag_MyComputer	b $00
.Flag_CacheModeOn	b $ff

.ramBaseWin0		b $00
.ramBaseWin1		b $00
.ramBaseWin2		b $00
.ramBaseWin3		b $00
.ramBaseWin4		b $00
.ramBaseWin5		b $00
.ramBaseWin6		b $00

.ramCacheMode0		b $c0
.ramCacheMode1		b $c0
.ramCacheMode2		b $c0
.ramCacheMode3		b $c0
.ramCacheMode4		b $c0
.ramCacheMode5		b $c0
.ramCacheMode6		b $c0

;*** Variablen.
.WM_WIN_DATA_BUF	s WINDOW_DATA_SIZE
.WM_DATA_SIZE		= WM_WIN_DATA_BUF +0
.WM_DATA_Y0		= WM_WIN_DATA_BUF +1
.WM_DATA_Y1		= WM_WIN_DATA_BUF +2
.WM_DATA_X0		= WM_WIN_DATA_BUF +3
.WM_DATA_X1		= WM_WIN_DATA_BUF +5
.WM_DATA_MAXENTRY	= WM_WIN_DATA_BUF +7
.WM_DATA_CURENTRY	= WM_WIN_DATA_BUF +9
.WM_DATA_GRID_X		= WM_WIN_DATA_BUF +11
.WM_DATA_GRID_Y		= WM_WIN_DATA_BUF +12
.WM_DATA_COLUMN		= WM_WIN_DATA_BUF +13
.WM_DATA_ROW		= WM_WIN_DATA_BUF +14
.WM_DATA_TITLE		= WM_WIN_DATA_BUF +15
.WM_DATA_INFO		= WM_WIN_DATA_BUF +17
.WM_DATA_WININIT	= WM_WIN_DATA_BUF +19
.WM_DATA_WINPRNT	= WM_WIN_DATA_BUF +21
.WM_DATA_WINSLCT	= WM_WIN_DATA_BUF +23
.WM_DATA_WINMOVE	= WM_WIN_DATA_BUF +25
.WM_DATA_MOVEBAR	= WM_WIN_DATA_BUF +27
.WM_DATA_RIGHTCLK	= WM_WIN_DATA_BUF +28
.WM_DATA_WINEXIT	= WM_WIN_DATA_BUF +30
.WM_DATA_WINMSLCT	= WM_WIN_DATA_BUF +32
.WM_DATA_OPTIONS	= WM_WIN_DATA_BUF +34
.WM_DATA_WINSSLCT	= WM_WIN_DATA_BUF +35
.WM_DATA_PRNFILE	= WM_WIN_DATA_BUF +37
.WM_DATA_GETFILE	= WM_WIN_DATA_BUF +39

:WM_WIN_DATA_VECL	b <WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *6

:WM_WIN_DATA_VECH	b >WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *6

:WM_WIN_DATA		s MAX_WINDOWS  *WINDOW_DATA_SIZE

.WMODE_DRIVE		s MAX_WINDOWS
.WMODE_PART		s MAX_WINDOWS
.WMODE_SDIR_T		s MAX_WINDOWS
.WMODE_SDIR_S		s MAX_WINDOWS
.WMODE_VDEL		s MAX_WINDOWS
.WMODE_SLCT_L		s MAX_WINDOWS
.WMODE_SLCT_H		s MAX_WINDOWS
.WMODE_VICON		s MAX_WINDOWS
.WMODE_VSIZE		s MAX_WINDOWS
.WMODE_VINFO		s MAX_WINDOWS
.WMODE_FMOVE		s MAX_WINDOWS

:mouseOldVec		w $0000
.WM_MOVE_MODE		b $00
.WM_COUNT_ICON_X	b $00
.WM_COUNT_ICON_Y	b $00
.WM_COUNT_ICON_XY	b $00

:WM_WIN_SIZE_TAB	b $00  ,MAX_AREA_WIN_Y -1
			w $0000,MAX_AREA_WIN_X -1
			b WIN_STD_POS_Y,WIN_STD_POS_Y + WIN_STD_SIZE_Y -1
			w WIN_STD_POS_X,WIN_STD_POS_X + WIN_STD_SIZE_X -1
			b $00  ,SCR_HIGHT_40_80 -1
			w $0000,SCR_WIDTH_40 -1

.WindowStack		b $00,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
.WINDOW_MAXIMIZED	s MAX_WINDOWS

.WindowOpenCount	b $01
.WM_WCODE		b $00
:VWM_CUR_TOP_WIN	b $00

;*** Variablen.
:DataJobCode		b $00
:DB_Margin_Left		b $00
:DB_Margin_Top		b $00
:RX_Margin_Left		b $00
:RX_Margin_Top		b $00
:CountLines		b $00
:ColsDataBytes		b $00
:GrfxDataBytes		w $0000
:DB_DELTA_Y		b $ff
:DB_DELTA_X		b $ff
:DataBank		b $00

;*** Variablen für Dateiauswahl/Dateianzeige.
:CurXPos		b $00
:CurYPos		b $00
:MinXPos		b $00
:MaxXPos		b $00
:MinYPos		b $00
:MaxYPos		b $00
:CurEntry		w $0000
:CurGridX		b $00
:CurGridY		b $00
:CountX			b $00
:CountY			b $00
:SlctY0			b $00
:SlctY1			b $00
:SlctX0			w $0000
:SlctX1			w $0000
:SlctMode		b $00

;*** Spezieller Zeichensatz für GEOS_MegaPatch (7x8)
.FontG3			v 7,"fnt.GeoDesk"

;*** Zusatzroutinen einlesen.
			t "-GD_SysRout"
			t "-WM_OpenDrv"

;******************************************************************************
;Ab hier folgt Code für Fenstermanager
;******************************************************************************
			t "-WM_Core"
			t "-WM_IconActions"
			t "-WM_Screen"
			t "-WM_BackScreen"
			t "-WM_DlgScrnBuf"
			t "-WM_ScrollBar"
			t "-WM_ScrollBarMv"
			t "-WM_Icons"			;Icon-Daten.

;*** AppLink erzeugen.
.WM_TEST_MOVE		lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			sec
			rts

::1			clc
			rts

;*** Fenster neu zeichnen.
.WM_CALL_DRAWROUT	lda	WM_DATA_WINPRNT +0
			ldx	WM_DATA_WINPRNT +1
			ldy	WM_WCODE
			cmp	#$ff
			bne	:1
			cpx	#$ff
			bne	:1
			jmp	WM_STD_OUTPUT
::1			jmp	CallRoutine

;*** Mausklick in Fenster auswerten.
;    Übergabe:		AKKU = Fenster-Nr.
:WM_CALL_MSLCT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINMSLCT +0
			ldx	WM_DATA_WINMSLCT +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	WM_SELECT_FILES

::51			ldy	WM_WCODE
			jmp	CallRoutine

;*** Fenster neu zeichnen.
.WM_CALL_SSLCT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINSSLCT +0
			ldx	WM_DATA_WINSSLCT +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	GD_SLCT_FILE

::51			ldy	WM_WCODE
			jmp	CallRoutine

.WM_STD_OUTPUT		jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_GETFILE +0
			ldx	WM_DATA_GETFILE +1
			jsr	CallRoutine
			jsr	WM_SAVE_WIN_DATA

			LoadW	r0,FontG3
			jsr	LoadCharSet
			LoadB	currentMode,$00

;.WM_NEW_OUTPUT
			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine
			txa
			beq	:4

			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			rts

:InitFPosData		jsr	WM_LOAD_WIN_DATA
			jsr	WM_WIN_MARGIN

			jsr	WM_GET_GRID_X
			sta	CurGridX
			jsr	WM_GET_GRID_Y
			sta	CurGridY

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	MinXPos
			sta	CurXPos

			lda	rightMargin +1
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowTop
			clc
			adc	#$08
			sta	MinYPos
			sta	CurYPos

			lda	windowBottom
			sta	MaxYPos

			lda	WM_DATA_CURENTRY +0
			sta	CurEntry +0
			lda	WM_DATA_CURENTRY +1
			sta	CurEntry +1

			lda	#$00
			sta	CountX
			sta	CountY
			rts

.WM_LINE_OUTPUT		lda	r0L
			sta	CurEntry +0
			lda	r0H
			sta	CurEntry +1

			lda	r1L
			sta	CurXPos
			lda	r1H
			sta	CurYPos

			jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_GETFILE +0
			ldx	WM_DATA_GETFILE +1
			jsr	CallRoutine

			jsr	WM_SAVE_WIN_DATA
			jsr	WM_WIN_MARGIN

			LoadW	r0,FontG3
			jsr	LoadCharSet
			LoadB	currentMode,$00

			jsr	WM_GET_GRID_X
			sta	CurGridX
			jsr	WM_GET_GRID_Y
			sta	CurGridY

			lda	rightMargin +1
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowBottom
			sta	MaxYPos

			lda	#$00
			sta	CountX

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3
			rts

::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine
			txa
			beq	:4

			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:6

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:6
::5			jmp	:1

::6			rts

:GD_SET_ENTRY2		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L
			LoadB	r2L,$03
			LoadB	r2H,$15

			ldx	#$ff
			rts

::11			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$ff
			rts

::12			lda	r2L
			sec
			sbc	r1L
			sta	r2L

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$7f
			rts

:GD_SLCT_FILE		php
			sei

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			plp
			rts

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			jsr	GD_SET_ENTRY
			txa
			beq	:4

			pha
			jsr	WM_CONVERT_CARDS
			jsr	IsMseInRegion
			tay
			pla
			tax
			tya
			beq	:4a

			plp
			MoveW	CurEntry,r0
			jsr	GD_INVERT_FILE
			jsr	WM_SAVE_SCREEN
			jmp	WM_SAVE_WIN_DATA

::4a			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			plp
			rts

:WM_SELECT_FILES	jsr	WM_LOAD_WIN_DATA

			jsr	WM_GET_SLCT_AREA

			MoveB	r2L,SlctY0
			MoveB	r2H,SlctY1
			MoveW	r3 ,SlctX0
			MoveW	r4 ,SlctX1
			MoveB	r5L,SlctMode

			jsr	OpenWinDrive

			jsr	InitFPosData

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3

::1a			lda	#$00
			sta	CountX

			lda	MinXPos
			sta	CurXPos

			lda	CurYPos
			clc
			adc	CurGridY
			cmp	MaxYPos
			bcc	:2
::2a			jmp	:6

::2			sta	CurYPos
			inc	CountY

			ldx	WM_DATA_ROW
			beq	:2b
			cmp	WM_DATA_ROW
			bcs	:2a

::2b			lda	CurXPos
::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			jsr	GD_SET_ENTRY
			txa
			beq	:4

			pha
			jsr	GD_TEST_ENTRY
			pla
			bcc	:3a

			pha
			MoveW	CurEntry,r0
			jsr	GD_INVERT_FILE
			pla

::3a			inc	CurEntry +0
			bne	:3b
			inc	CurEntry +1

::3b			tax

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:5a

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:5a
::5			jmp	:1
::5a			jmp	:1a

::6			jsr	WM_SAVE_SCREEN
			jmp	WM_SAVE_WIN_DATA

:GD_SET_ENTRY		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L
			LoadB	r2L,$03
			LoadB	r2H,$15

			ldx	#$ff
			rts

::11			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$ff
			rts

::12			lda	r2L
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			ldx	#$7f
			rts

:GD_TEST_ENTRY		jsr	WM_CONVERT_CARDS

::101			CmpB	r2L,SlctY1
			bcs	:201
			CmpB	r2H,SlctY0
			bcc	:201

			bit	SlctMode
			bmi	:103

			CmpB	r2L,SlctY0
			bcc	:201
			CmpB	r2H,SlctY1
			bcs	:201

::103			CmpW	r3 ,SlctX1
			bcs	:201
			CmpW	r4 ,SlctX0
			bcc	:201

			bit	SlctMode
			bmi	:105

			CmpW	r3 ,SlctX0
			bcc	:201
			CmpW	r4 ,SlctX1
			bcs	:201

;--- Datei gewählt.
::105			sec
			rts

;--- Nicht gewählt.
::201			clc
			rts

:GD_INVERT_FILE		CmpW	r4,rightMargin
			bcc	:52a
			MoveW	rightMargin,r4

::52a			CmpB	r2H,windowBottom
			bcc	:52b
			MoveB	windowBottom,r2H

::52b			jsr	InvertRectangle

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			bpl	:3b
			MoveW	r0,r15
			jsr	GD_VEC2ENTRY

			LoadW	r0 ,r4
			MoveW	r15,r1
			LoadW	r2 ,2
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	FetchRAM

			ldx	WM_WCODE
			ldy	WMODE_SLCT_L,x
			lda	WMODE_SLCT_H,x
			tax

			lda	r4L
			ora	r4H
			beq	:66
			ldx	#$ff
			ldy	#$ff
::66			iny
			bne	:67
			inx
::67			sty	r4L
			stx	r4H

			jsr	StashRAM
			jmp	:3

;--- Kein RAM-Cache.
::3b			LoadW	r5L,$20
			ldx	#r0L
			ldy	#r5L
			jsr	BMult
			AddVW	BASE_DIR_DATA,r0

			ldy	#$00
			lda	(r0L),y
			sta	r4L
			iny
			lda	(r0L),y
			sta	r4H

			ldx	WM_WCODE
			ldy	WMODE_SLCT_L,x
			lda	WMODE_SLCT_H,x
			tax

			lda	r4L
			ora	r4H
			beq	:77
			ldx	#$ff
			ldy	#$ff
::77			iny
			bne	:78
			inx
::78			sty	r4L
			stx	r4H

			ldy	#$00
			lda	r4L
			sta	(r0L),y
			iny
			lda	r4H
			sta	(r0L),y

::3			lda	r4L
			ora	r4H
			bne	:9a

			ldx	WM_WCODE
			lda	WMODE_SLCT_L,x
			bne	:9b
			dec	WMODE_SLCT_H,x
::9b			dec	WMODE_SLCT_L,x
			rts

::9a			ldx	WM_WCODE
			inc	WMODE_SLCT_L,x
			bne	:3a
			inc	WMODE_SLCT_H,x
::3a			rts

.GD_VEC2ENTRY		ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			bne	:y0
			clc
			rts

::y0			ldx	#$20
			cmp	#$80
			beq	:z0
			ldx	#$40
			cmp	#$40
			beq	:z0
			ldx	#$60
::z0			stx	r14L
			ldx	#r15L
			ldy	#r14L
			jsr	BMult
			AddVW	$0300,r15
			sec
			rts

;*** Fenster nach links/oben vergrößern.
:WM_FJOB_SIZE_UL	jsr	WM_FJOB_TEST_Y0
			jsr	WM_FJOB_TEST_X0

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2L
			rts

;*** Fenster nach rechts/oben vergrößern.
:WM_FJOB_SIZE_UR	jsr	WM_FJOB_TEST_Y0
			jsr	WM_FJOB_TEST_X1

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2L
			rts

;*** Fenster nach links/unten vergrößern.
:WM_FJOB_SIZE_DL	jsr	WM_FJOB_TEST_Y1
			jsr	WM_FJOB_TEST_X0

			MoveW	mouseXPos,r3
			MoveB	mouseYPos,r2H
			rts

;*** Fenster nach rechts/unten vergrößern.
:WM_FJOB_SIZE_DR	jsr	WM_FJOB_TEST_Y1
			jsr	WM_FJOB_TEST_X1

			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r2H
			rts

;*** Verkleinern nach oben möglich ?
:WM_FJOB_TEST_Y0	lda	r2H
			sec
			sbc	mouseYPos
			cmp	#MIN_SIZE_WIN_Y
			bcs	:1
			MoveB	r2L,mouseYPos
::1			rts

;*** Verkleinern nach unten möglich ?
:WM_FJOB_TEST_Y1	lda	mouseYPos
			sec
			sbc	r2L
			cmp	#MIN_SIZE_WIN_Y
			bcs	:1
			MoveB	r2H,mouseYPos
::1			rts

;*** Verkleinern nach rechts möglich ?
:WM_FJOB_TEST_X0	lda	r4L
			sec
			sbc	mouseXPos +0
			tax
			lda	r4H
			sbc	mouseXPos +1
			bne	:1
			cpx	#MIN_SIZE_WIN_X
			bcs	:1
			MoveW	r3 ,mouseXPos
::1			rts

;*** Verkleinern nach links möglich ?
:WM_FJOB_TEST_X1	lda	mouseXPos +0
			sec
			sbc	r3L
			tax
			lda	mouseXPos +1
			sbc	r3H
			bne	:1
			cpx	#MIN_SIZE_WIN_X
			bcs	:1
			MoveW	r4 ,mouseXPos
::1			rts

;*** Standard-Routine zum verschieben von Daten.
;    Wird aufgerufen mit Tabellen-Eintrag $FFFF.
.WM_MOVE_ENTRY_I	jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H

			lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_POS

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	r0L
			ora	r0H
			beq	:2

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:1

			lda	#$00
			sta	r0L
			sta	r0H
::1			jmp	WM_SET_NEW_POS
::2			ldx	#$00
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	r0L
			adc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			adc	#$00
			sta	r0H
			jsr	WM_TEST_CUR_POS

;*** ScrollBalken neu positionieren.
:WM_SET_NEW_POS		lda	r0L
			sta	WM_DATA_CURENTRY +0
			ldx	r0H
			stx	WM_DATA_CURENTRY +1
			jsr	SetPosBalken
			jsr	WM_SAVE_WIN_DATA
			ldx	#$00
			rts

;*** Standard-Routine zum verschieben von Text-Daten.
;    Wird aufgerufen mit Tabellen-Eintrag $EEEE.
.WM_MOVE_ENTRY_T	lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_ENTRY_I

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	WM_DATA_CURENTRY +0
			ora	WM_DATA_CURENTRY +1
			bne	:11
			ldx	#$00
			rts

::11			SubVW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	:WM_MOV_LAST_LINE
			ldx	#$ff
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	WM_DATA_CURENTRY +0
			adc	WM_COUNT_ICON_Y
			tax
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			cmp	WM_DATA_MAXENTRY +1
			bne	:21
			cpx	WM_DATA_MAXENTRY +0
::21			bcc	:22
			ldx	#$00
			rts

::22			AddVBW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	:WM_MOV_NEXT_LINE
			ldx	#$ff
			rts

;*** Verschiebt Fensterinhalt um eine Zeile nach unten und
;    gibt Inhalt der nächsten Zeile aus.
::WM_MOV_NEXT_LINE
			lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			clc
			adc	#$08
			lsr
			lsr
			lsr
			sta	r1L
			LoadB	r1H,0
			LoadW	r0 ,320
			ldx	#r1L
			ldy	#r0L
			jsr	DMult

			AddW	leftMargin,r1
			AddVW	SCREEN_BASE,r1

			lda	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r1H
			adc	#> 320
			sta	r0H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::31			dec	WM_COUNT_ICON_Y
			beq	:32
			jsr	MoveData

			lda	r0L
			sta	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			adc	#> 320
			sta	r0H
			jmp	:31

::32			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			jsr	WM_GET_ICON_Y
			lda	WM_COUNT_ICON_Y
			asl
			asl
			asl
			clc
			adc	windowTop
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			clc
			adc	WM_COUNT_ICON_Y
			tay
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			tax

			tya
			bne	:33
			dex
::33			dey
			sty	r0L
			stx	r0H
			jmp	WM_LINE_OUTPUT

;*** Verschiebt Fensterinhalt um eine Zeile nach oben und
;    gibt Inhalt der letzten Zeile aus.
::WM_MOV_LAST_LINE
			lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			lsr
			lsr
			lsr
			clc
			adc	WM_COUNT_ICON_Y
			sta	r0L
			dec	r0L
			LoadB	r0H,0
			LoadW	r1 ,320
			ldx	#r0L
			ldy	#r1L
			jsr	DMult

			AddW	leftMargin,r0
			AddVW	SCREEN_BASE,r0

			lda	r0L
			clc
			adc	#< 320
			sta	r1L
			lda	r0H
			adc	#> 320
			sta	r1H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::41			dec	WM_COUNT_ICON_Y
			beq	:42
			jsr	MoveData

			lda	r0L
			sta	r1L
			sec
			sbc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			sbc	#> 320
			sta	r0H
			jmp	:41

::42			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			lda	windowTop
			clc
			adc	#$08
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jmp	WM_LINE_OUTPUT

.END_CODE_WM
