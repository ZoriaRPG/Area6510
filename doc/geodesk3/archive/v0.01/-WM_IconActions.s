﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:		WM_DEF_AREA...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Bereich für Icon definieren.
;******************************************************************************
;*** Bereiche für Fenster-Icons berechnen.
:WM_DEF_AREA_CL		lda	#$00
			b $2c
:WM_DEF_AREA_DN		lda	#$03
			b $2c
:WM_DEF_AREA_STD	lda	#$06
			b $2c
:WM_DEF_AREA_MN		lda	#$09
			b $2c
:WM_DEF_AREA_MX		lda	#$0c
			b $2c
:WM_DEF_AREA_UL		lda	#$0f
			b $2c
:WM_DEF_AREA_UR		lda	#$12
			b $2c
:WM_DEF_AREA_DL		lda	#$15
			b $2c
:WM_DEF_AREA_DR		lda	#$18
			b $2c
:WM_DEF_AREA_WUP	lda	#$1b
			b $2c
:WM_DEF_AREA_WDN	lda	#$1e
:WM_DEF_AREA		pha
			jsr	WM_GET_SLCT_SIZE
			pla
			tay

			lda	:WM_DEFICON_TAB +0,y
			bmi	:2

			lda	r3L
			clc
			adc	:WM_DEFICON_TAB +1,y
			sta	r3L
			bcc	:1
			inc	r3H
::1			jmp	:3

::2			lda	r4L
			sec
			sbc	:WM_DEFICON_TAB +1,y
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H

::3			lda	r3L
			clc
			adc	#$07
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

::4			lda	:WM_DEFICON_TAB +0,y
			and	#%01000000
			bne	:5

			lda	r2L
			clc
			adc	:WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

::5			lda	r2H
			sec
			sbc	:WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

;*** Tabelle für Icon-Bereiche.
;    b $00!$00 = linke  obere  Ecke
;      $80!$00 = rechte obere  Ecke
;      $00!$40 = linke  untere Ecke
;      $80!$40 = rechte untere Ecke
;    b DeltaX
;    b DeltaY
::WM_DEFICON_TAB	b $00!$00,$08,$00		;Close
			b $80!$00,$27,$00		;Sortieren.
			b $80!$00,$1f,$00		;Standard
			b $80!$00,$17,$00		;Minimize
			b $80!$00,$0f,$00		;Maximize
			b $00!$00,$00,$00		;Resize UL
			b $80!$00,$07,$00		;Resize UR
			b $00!$40,$00,$07		;Resize DL
			b $80!$40,$07,$07		;Resize DR
			b $80!$40,$07,$17		;Resize DL
			b $80!$40,$07,$0f		;Resize DL

;******************************************************************************
;Routine:		WM_DEF_AREA...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Bereich für Klick auf Titelzeile berechnen.
;******************************************************************************
:WM_DEF_AREA_MV		jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$07
			sta	r2H

			lda	r3L
			clc
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H

			lda	r4L
			sec
			sbc	#$28
			sta	r4L
			lda	r4H
			sbc	#$00
			sta	r4H
			rts

;******************************************************************************
;Routine:		WM_DEF_AREA...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Bereich für Klick auf Scrollbalken berechnen.
;******************************************************************************
:WM_DEF_AREA_BAR	jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$08
			sta	r2L

			lda	r2H
			sec
			sbc	#$18
			sta	r2H

			lda	r4L
			sec
			sbc	#$07
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H
			rts

;******************************************************************************
;Routine:		WM_FUNC...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Funktion für Icon ausführen.
;******************************************************************************
;*** Funktion: Gewähltes Fenster schließen.
:WM_FUNC_CLOSE		lda	WM_WCODE
			jsr	WM_CLOSE_WINDOW
			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster maximieren.
:WM_FUNC_MAX		ldx	WM_WCODE
			lda	WINDOW_MAXIMIZED,x
			bne	:1
			dec	WINDOW_MAXIMIZED,x
			jsr	WM_UPDATE
::1			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster zurücksetzen.
:WM_FUNC_MIN		ldx	WM_WCODE
			lda	WINDOW_MAXIMIZED,x
			beq	:1
			inc	WINDOW_MAXIMIZED,x
			jsr	WM_UPDATE
::1			ldx	#NO_ERROR
			rts

;*** Funktion: Standardgröße für Fenster setzen.
:WM_FUNC_STD		ldx	WM_WCODE
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_DEF_STD_WSIZE
			jsr	WM_SAVE_WIN_DATA
			jsr	WM_UPDATE
			ldx	#NO_ERROR
			rts

;*** Funktion: Fenster nach unten verschieben.
:WM_FUNC_DOWN		lda	WindowOpenCount
			cmp	#$03
			bcc	:4

			ldx	#$00
			ldy	#$00
::1			lda	WindowStack ,x
			beq	:3
			cmp	WM_WCODE
			beq	:2
			sta	WindowStack ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1

::3			lda	WM_WCODE
			sta	WindowStack ,y
			jsr	WM_DRAW_ALL_WIN
::4			ldx	#NO_ERROR
			rts

;*** Fenstergröße ändern ?
:WM_FUNC_SIZE_UL	lda	#<WM_FJOB_SIZE_UL
			ldx	#>WM_FJOB_SIZE_UL
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_UR	lda	#<WM_FJOB_SIZE_UR
			ldx	#>WM_FJOB_SIZE_UR
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DL	lda	#<WM_FJOB_SIZE_DL
			ldx	#>WM_FJOB_SIZE_DL
			jmp	WM_FUNC_RESIZE

:WM_FUNC_SIZE_DR	lda	#<WM_FJOB_SIZE_DR
			ldx	#>WM_FJOB_SIZE_DR

;*** Fenstergröße ändern.
:WM_FUNC_RESIZE		jsr	WM_EDIT_WIN
			ldx	WM_WCODE
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_SET_WIN_SIZE
			jmp	WM_UPDATE

;******************************************************************************
;Routine:		WM_FUNC...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fensterinhalt nach oben/unten verschieben.
;******************************************************************************
:WM_FUNC_MOVER		lda	#$7f
			b $2c
:WM_FUNC_MOVE_UP	lda	#$00
			b $2c
:WM_FUNC_MOVE_DN	lda	#$ff
			sta	WM_MOVE_MODE

::1			jsr	WM_LOAD_WIN_DATA
			PushW	WM_DATA_CURENTRY

			jsr	WM_CALL_MOVE

			PopW	r0
			txa
			bne	:1a

			CmpW	WM_DATA_CURENTRY,r0
			beq	:2

			jsr	WM_WIN_MARGIN

			MoveB	windowTop   ,r2L
			MoveB	windowBottom,r2H
			MoveW	leftMargin  ,r3
			MoveW	rightMargin ,r4

			lda	#$00
			jsr	SetPattern
			jsr	Rectangle

			jsr	WM_CALL_DRAWROUT

::1a			jsr	WM_DRAW_MOVER

			ldy	WM_WCODE
			lda	WMODE_FMOVE,y
			bpl	:1b
			jsr	SCPU_Pause
::1b			lda	mouseData		;Dauerfunktion ?
			bpl	:1

::2			jsr	WM_SAVE_SCREEN

			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:		WM_FUNC...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster verschieben.
;******************************************************************************
:WM_FUNC_SIZE_MV	jsr	WM_GET_SLCT_SIZE

			MoveB	r2L,mouseYPos
			MoveW	r3 ,mouseXPos

			lda	r4L
			sec
			sbc	r3L
			sta	r13L
			lda	r4H
			sbc	r3H
			sta	r13H

			lda	r2H
			sec
			sbc	r2L
			sta	r14L

			lda	#<WM_FJOB_TEST_MOV
			ldx	#>WM_FJOB_TEST_MOV
			jsr	WM_EDIT_WIN

			jsr	WM_SET_CARD_XY

			PushB	r2L
			PushW	r3
			PushW	r13
			PushB	r14L

			lda	r2L
			lsr
			lsr
			lsr
			pha
			lda	r3H
			lsr
			lda	r3L
			ror
			lsr
			lsr
			pha
			jsr	WM_DRAW_NO_TOP
			pla
			sta	DB_DELTA_X
			pla
			sta	DB_DELTA_Y

			jsr	WM_LOAD_SCREEN

			PopB	r14L
			PopW	r13
			PopW	r3
			PopB	r2L

			MoveW	r3  ,r4
			AddW	r13 ,r4
			MoveB	r2L ,r2H
			AddB	r14L,r2H
			jsr	WM_SET_WIN_SIZE
			jsr	WM_DRAW_MOVER
			jmp	WM_SAVE_SCREEN

;*** Gewähltes Fenster vergrößern/verschieben.
;    Übergabe:		AKKU/XREG = Zeiger auf Test-Routine.
:WM_EDIT_WIN		sta	:3 +1
			stx	:3 +2

			php
			cli

			lda	WindowStack
			jsr	WM_GET_WIN_SIZE

			LoadB	mouseBottom,$b7

::1			jsr	WM_DRAW_FRAME

::2			lda	mouseData
			bmi	:4
			lda	inputData
			bmi	:2

			jsr	WM_DRAW_FRAME
::3			jsr	$ffff
			jmp	:1

::4			jsr	WM_DRAW_FRAME

			LoadB	pressFlag,$00
			LoadB	mouseBottom,$c7
			plp
			rts

;*** Größe für aktuelles Fenster festlegen.
;    Übergabe:		":WM_WCODE" = Fenster-Nr.
:WM_SET_WIN_SIZE	jsr	WM_SET_CARD_XY
			jsr	WM_LOAD_WIN_DATA

			ldx	#$05
::1			lda	r2L       ,x
			sta	WM_DATA_Y0,x
			dex
			bpl	:1

			jmp	WM_SAVE_WIN_DATA

;*** Koordinaten auf CARDs umrechnen.
:WM_SET_CARD_XY		lda	r2L
			and	#%11111000
			sta	r2L

			lda	r2H
			ora	#%00000111
			sta	r2H

			lda	r3L
			and	#%11111000
			sta	r3L

			lda	r4L
			ora	#%00000111
			sta	r4L
			rts

;******************************************************************************
;Routine:		WM_FJOB...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster verschieben möglich?
;******************************************************************************
:WM_FJOB_TEST_MOV	jsr	WM_FJOB_TEST_MX
			jsr	WM_FJOB_TEST_MY

			lda	mouseXPos +0
			sta	r3L
			clc
			adc	r13L
			sta	r4L
			lda	mouseXPos +1
			sta	r3H
			adc	r13H
			sta	r4H

			lda	mouseYPos
			sta	r2L
			clc
			adc	r14L
			sta	r2H
			rts

;*** Verschiebung in Y-Richtung möglich ?
:WM_FJOB_TEST_MY	lda	mouseYPos
			clc
			adc	r14L
			cmp	#MAX_AREA_WIN_Y
			bcc	:1
			MoveB	r2L,mouseYPos
::1			rts

;*** Verschiebung in X-Richtung möglich ?
:WM_FJOB_TEST_MX	lda	mouseXPos +0
			clc
			adc	r13L
			tax
			lda	mouseXPos +1
			adc	r13H
			cmp	#> MAX_AREA_WIN_X
			bne	:1
			cpx	#< MAX_AREA_WIN_X
			bcc	:1
			MoveW	r3,mouseXPos
::1			rts

;******************************************************************************
;Routine:		WM_FUNC...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster überlappend darstellen.
;******************************************************************************
.WM_FUNC_SORT		lda	#$01
			sta	r1L
::1			lda	r1L
			sta	WM_WCODE
			tax
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			jsr	WM_DEF_STD_WSIZE
			jsr	WM_SAVE_WIN_DATA
			lda	WM_WCODE
			jsr	WM_WIN2TOP

			inc	r1L
			lda	r1L
			cmp	#MAX_WINDOWS
			bcc	:1

			jmp	WM_UPDATE_ALL

;******************************************************************************
;Routine:		WM_FUNC...
;Parameter:		-
;Rückgabe:		-
;Verändert:		A,X,Y,r0-r15
;Funktion:		Fenster nebeneinander anordnen.
;******************************************************************************
.WM_FUNC_POS		lda	#MIN_AREA_WIN_Y
			sta	r2L
			lda	#MAX_AREA_WIN_Y -1
			sta	r2H

			ldx	WindowOpenCount
			dex
			cpx	#$03 +1
			bcc	:2
			bne	:1
			ldx	#$02
			b $2c
::1			ldx	#$03
			lda	#$57
			sta	r2H

::2			stx	r1H
			dex
			txa
			asl
			tax
			lda	:WIDTH +0,x
			sta	r5L
			lda	:WIDTH +1,x
			sta	r5H

			lda	#$00
			sta	r1L

::3			LoadW	r3,4*8

::4			ldx	r1L
			lda	WindowStack,x
			beq	:5
			bmi	:5
			sta	WM_WCODE
			tax
			lda	#$00
			sta	WINDOW_MAXIMIZED,x
			jsr	WM_LOAD_WIN_DATA
			lda	r2L
			sta	WM_DATA_Y0
			lda	r2H
			sta	WM_DATA_Y1
			lda	r3L
			sta	WM_DATA_X0 +0
			lda	r3H
			sta	WM_DATA_X0 +1
			clc
			lda	r3L
			adc	r5L
			sta	r3L
			sta	WM_DATA_X1 +0
			lda	r3H
			adc	r5H
			sta	r3H
			sta	WM_DATA_X1 +1
			jsr	WM_SAVE_WIN_DATA

			inc	r3L
			bne	:5
			inc	r3H
::5			inc	r1L
			lda	r1L
			cmp	r1H
			bne	:6

			lda	#$58
			sta	r2L
			lda	#MAX_AREA_WIN_Y -9
			sta	r2H
			jmp	:3

::6			cmp	#MAX_WINDOWS
			bcc	:4

			jmp	WM_UPDATE_ALL

::WIDTH			w 36*8-1
			w 18*8-1
			w 12*8-1
