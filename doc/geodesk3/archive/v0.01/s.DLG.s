﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

			n "o.DLG"
			t "TopSym"
			t "TopSym/G3"
			t "TopMac/G3"
			t "s.WM.ext"

			f DATA
			o END_CODE_WM

;*** Titelzeile in Dialogbox löschen.
.Dlg_DrawTitel		lda	#$00
			jsr	SetPattern
			jsr	i_Rectangle
			b	$30,$3f
			w	$0040,$00ff
			lda	C_DBoxTitel
			jsr	DirectColor
			jmp	UseSystemFont

;*** Dialogboxen.
.Dlg_NoFree2RAM		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w D00a
			b DBTXTSTR   ,$0c,$20
			w D01a
			b DBTXTSTR   ,$0c,$2a
			w D01b
			b DBTXTSTR   ,$0c,$3a
			w D01c
			b OK         ,$01,$50
			b NULL

:D00a			b PLAINTEXT,BOLDON
			b "Systemfehler",NULL
:D01a			b "GeoDesk benötigt mindestens",NULL
:D01b			b "zwei freie Speicherbänke!",NULL
:D01c			b "Das Programm wird beendet.",NULL

.Dlg_SlctFile		b $81
			b DBGETFILES!DBSETDRVICON ,$00,$00
			b CANCEL                  ,$00,$00
			b DISK                    ,$00,$00
			b OK,0,0
			b NULL

;*** Dialogboxen.
.Dlg_DrvTypNFnd		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w D00b
			b DBTXTSTR   ,$0c,$20
			w D02a
			b DBTXTSTR   ,$0c,$2a
			w D02b
			b DBTXTSTR   ,$0c,$3a
			w D02c
			b OK         ,$01,$50
			b NULL

:D00b			b PLAINTEXT,BOLDON
			b "Konfigurationsfehler",NULL
:D02a			b "GeoDesk konnte das Laufwerk",NULL
:D02b			b "im System nicht finden!",NULL
:D02c			b "Funktion wird abgebrochen.",NULL

.END_CODE_DLG
