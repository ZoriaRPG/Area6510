﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Neue Laufwerks-Ansicht in "MyComputer" öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
;Im Unterschied zu :OpenDriveNewWin
;wird hier bei Native immer das Haupt-
;Verzeichnis geöffnet.
;Hier muss auch kein leeres Fenster
;gesucht werden da "MyComputer" dazu
;wiederverwendet wird.
:OpenDriveCurWin	cpy	#$00
			bne	:40
			cpx	#$04
			bcs	:40
			txa
			clc
			adc	#$08
			jsr	SetDevice

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:1
			jsr	OpenRootDir
			jmp	:2

::1			jsr	OpenDisk

::2			lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			jsr	SaveWinDrive

			jsr	WM_LOAD_WIN_DATA

			LoadW	r0,WinData_Files

			ldy	#$01			;Fenster-Größe von "MyComputer"
::39			lda	WM_WIN_DATA_BUF,y	;für neues Laufwerksfenster
			sta	(r0L),y			;kopieren.
			iny
			cpy	#$07
			bcc	:39

			lda	Flag_MyComputer		;Fenster-Nr. für "MyComputer".
			ldx	#$00			;"MyComputer" schließen.
			stx	Flag_MyComputer

			pha				;Fenster-Nr. speichern und das
			jsr	WM_WIN2TOP		;Ziel-Fenster nach oben holen.
;			jsr	WM_DRAW_NO_TOP
			LoadW	r0,WinData_Files
			pla
			jsr	WM_USER_WINDOW		;Neuen Fenster-Inhalt darstellen.

			ldy	#$01			;Standard-Fenstergröße löschen.
			lda	#$00
::38			sta	WinData_Files,y
			iny
			cpy	#$07
			bcc	:38

::40			rts

::SelectDrive		b $00
::SelectedWindow	b $00

;*** Neue Laufwerks-Ansicht öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
:OpenDriveNewWin	cpy	#$00			;Zähler prüfen:
			bne	:40			;Nur Einträge 0-3 = Laufwerk A-D
			cpx	#$04			;sind erlaubt.
			bcs	:40
			txa
			pha
			jsr	WM_IS_WIN_FREE		;Leeres Fenster suchen.
			sta	:WM_NV_WINDOW
			pla
			cpx	#NO_ERROR		;Fehler gefunden?
			bne	:40			; => Nein, Abbruch...
			clc
			adc	#$08
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk

			ldx	:WM_NV_WINDOW		;Laufwerksdaten für
			jsr	SaveUserWinDrive	;neues Fenster speichern.

			LoadW	r0,WinData_Files	;Neues Fenster öffnen.
			jmp	WM_OPEN_WINDOW
::40			rts

::WM_NV_WINDOW		b $00

:SaveWinDrive		ldx	WM_WCODE
:SaveUserWinDrive	lda	curDrive
			sta	WMODE_DRIVE   ,x
			tay
			lda	drivePartData- 8,y
			sta	WMODE_PART    ,x
			lda	curDirHead   +32
			sta	WMODE_SDIR_T  ,x
			lda	curDirHead   +33
			sta	WMODE_SDIR_S  ,x
			rts

:InitWIN		lda	#$00
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			ldx	WM_WCODE
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			bit	Flag_CacheModeOn
			bpl	:2

			ldx	curDrive
			lda	driveType -8,x
			bmi	:2

			ldx	WM_WCODE
			lda	ramBaseWin0,x
			bne	:2

			jsr	FindFreeBank
			cpx	#NO_ERROR
			beq	:1

			ldx	WM_WCODE
			lda	#$00
			sta	ramCacheMode0,x
			rts

::1			tya
			ldx	WM_WCODE
			sta	ramBaseWin0  ,x
			jsr	AllocateBank
::2			rts

:ExitWIN		bit	Flag_CacheModeOn
			bpl	:1
			ldx	WM_WCODE
			ldy	ramBaseWin0,x
			beq	:1
			jmp	FreeBank
::1			rts

:PrntCurDkName		lda	curDrive
			tax
			clc
			adc	#$39
			sta	:DiskName +0

			lda	drivePartData -8,x
			ldy	#$30
			ldx	#$30
::1			cmp	#100
			bcc	:2
			sbc	#100
			iny
			bne	:1
::2			cmp	#10
			bcc	:3
			sbc	#10
			inx
			bne	:2
::3			adc	#$30
			sty	:DiskName +2
			stx	:DiskName +3
			sta	:DiskName +4

			ldx	#r0L
			jsr	GetPtrCurDkNm

			ldy	#$00
::4			lda	(r0L),y
			beq	:5
			cmp	#$a0
			beq	:5
			sta	:DiskName +6,y
			iny
			cpy	#$10
			bcc	:4
::5			lda	#$00
			sta	:DiskName +6,y
			iny
			cpy	#$11
			bcc	:5

			LoadW	r0,:DiskName
			jmp	PutString

::DiskName		b $00,"/"
			b $00,$00,$00
			b ":"
			s 17

:PrntCurDkInfo		PushB	r1H
			jsr	OpenWinDrive
			jsr	CalcBlksFree

			ldx	#r3L
			ldy	#$02
			jsr	DShiftRight
			ldx	#r4L
			ldy	#$02
			jsr	DShiftRight

			PopB	r1H
			PushB	r3H
			PushB	r3L
			PushW	r4

			MoveW	r4,r0
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text0
			jsr	PutString

			PopW	r4

			pla
			sec
			sbc	r4L
			sta	r0L
			pla
			sbc	r4H
			sta	r0H
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text1
			jsr	PutString

			lda	WM_DATA_MAXENTRY +0
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sta	r0H
			lda	#%11000000
			jsr	PutDecimal
			LoadW	r0,:tmp_Text2
			jmp	PutString

::tmp_Text0		b "Kb frei, ",0
::tmp_Text1		b "Kb belegt, ",0
::tmp_Text2		b " Datei(en)",0
