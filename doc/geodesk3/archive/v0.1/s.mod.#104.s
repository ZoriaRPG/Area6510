﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* AppLink-Daten laden.
;* AppLink-Daten speichern.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopMac.MP3"
			t "TopSym.GD"
			t "s.mod.#101.ext"
			t "-SYS_APPLINK"
endif

			n "mod.#104.obj"
			f DATA
			o VLIR_BASE
			p MainInit
			c "GeoDesk64   V0.1"
			a "Markus Kanet"

:VlirJumpTable		jmp	xLNK_LOAD_DATA
			jmp	xLNK_SAVE_DATA

;*** Konfiguration aus Datei laden.
:xLNK_LOAD_DATA		jsr	TempLinkDrive		;AppLink-Laufwerk aktivieren.
			txa				;Laufwerksfehler?
			bne	:1			; => Ja, Abbruch...

			jsr	LNK_FIND_CONFIG		;AppLink-Datei suchen.
			txa				;Datei gefunden?
			beq	:2			; => Ja, weiter...
::1			rts				; => Abbruch...

::2			lda	dirEntryBuf +1		;VLIR-Header einlesen.
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	GetBlock
			txa				;VLIR-Sektor gelesen?
			bne	:1			; => Nein, Abbruch...

			ldx	#$00			;Zeiger auf ersten AppLink.
			stx	ND_Record
			inx
			stx	LinkData
			LoadW	r14,LinkData +LINK_DATA_BUFSIZE +1
			LoadW	r15,Icon_01

::3			jsr	OpenLinkDrive		;AppLink-Laufwerk öffnen.
			txa
			bne	:1

			lda	ND_Record
			cmp	#25
			bcs	:5
			asl
			tax
			lda	ND_VLIR +2,x
			beq	:4
			sta	r1L
			lda	ND_VLIR +3,x
			sta	r1H
			LoadW	r4,ND_Data
			jsr	GetBlock

			lda	ND_Data +2
			beq	:4

			jsr	CopyNotesData
			jsr	LoadIconData

			inc	LinkData
			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15

::4			inc	ND_Record
			jmp	:3

::5			jmp	BackTempDrive

:LNK_FIND_CONFIG	LoadW	r6,GD_APPLINK
			jmp	FindFile

;*** Leere Konfigurationsdatei erstellen.
:LNK_NEW_CONFIG		jsr	OpenDisk
			txa
			beq	:2
::1			rts

::2			LoadB	r10L,0
			LoadW	r9,HdrB000
			jsr	SaveFile
			txa
			bne	:1

			jsr	LNK_FIND_CONFIG
			txa
			bne	:1

			LoadW	r9,dirEntryBuf
			jsr	GetFHdrInfo
			txa
			bne	:1

			lda	HdrB160
			sta	fileHeader +160

			lda	dirEntryBuf+19
			sta	r1L
			lda	dirEntryBuf+20
			sta	r1H
			LoadW	r4,fileHeader
			jmp	PutBlock

;*** Info-Block für SETUP-Dateien.
:HdrB000		w GD_APPLINK
:HdrB002		b $03,$15
			b $bf
			b %11111111,%11111111,%11111111
			b %10000000,%00000000,%00000001
			b %10000000,%00000000,%00000001
			b %10011111,%11111111,%11110001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10011111,%11111111,%11111001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10010000,%00000000,%00011001
			b %10011100,%00000000,%00011001
			b %10010100,%00000000,%00011001
			b %10010010,%00000000,%00011001
			b %10001111,%11111111,%11111001
			b %10000000,%00000000,%00000001
			b %11111111,%11111111,%11111111

:HdrB068		b $83
:HdrB069		b SYSTEM
:HdrB070		b VLIR
:HdrB071		w $0000,$ffff,$0000
:HdrB077		b "Notes       "		;Klasse
:HdrB089		b "V1.0"			;Version
:HdrB093		b NULL
:HdrB094		b $00,$00			;Reserviert
:HdrB096		b $00				;Bildschirmflag
:HdrB097		b "GeoDesk64"			;Autor
:HdrB106		s 11				;Reserviert
:HdrB117		b "note pad    "  		;Anwendung/Klasse
:HdrB129		b "V2.0"  			;Anwendung/Version
:HdrB133		b NULL
:HdrB134		s 26				;Reserviert.

:HdrB160		b "Konfigurationsdatei",CR
			b "für GeoDesk AppLinks",NULL
:HdrEnd			s (HdrB000+256)-HdrEnd

:ND_Record		b $00
:ND_Name		s 17
:ND_Class		= HdrB077 ;b "Notes       V1.0",NULL
:ND_Data		s 256
:ND_VLIR		s 256

:CopyNotesData		ldx	#$02
			ldy	#LINK_DATA_FILE
			jsr	:101
			ldy	#LINK_DATA_NAME
			jsr	:101

			ldy	#LINK_DATA_TYPE
			jsr	:201

			inx
			ldy	#LINK_DATA_XPOS
			jsr	:201
			ldy	#LINK_DATA_YPOS
			jsr	:201

			inx
			ldy	#LINK_DATA_COLOR
::1			jsr	:201
			iny
			cpy	#LINK_DATA_COLOR +9
			bcc	:1

			inx
			lda	ND_Data,x
			sec
			sbc	#$39
			ldy	#LINK_DATA_DRIVE
			sta	(r14L),y
			inx

			ldy	#LINK_DATA_DVTYP
			jsr	:201
			ldy	#LINK_DATA_DPART
			jsr	:201

			inx
			ldy	#LINK_DATA_DSDIR +0
			jsr	:201
			ldy	#LINK_DATA_DSDIR +1
			jsr	:201

			inx
			ldy	#LINK_DATA_ENTRY +0
			jsr	:201
			ldy	#LINK_DATA_ENTRY +1
			jsr	:201
			ldy	#LINK_DATA_ENTRY +2
			jsr	:201
			jmp	LoadIconData

::101			lda	#$10
			sta	r0L
::101a			lda	ND_Data,x
			cmp	#CR
			beq	:102
			sta	(r14L),y
			inx
			iny
			dec	r0L
			bne	:101a
::102			lda	#$00
::103			sta	(r14L),y
			iny
			dec	r0L
			bpl	:103
			inx
			rts

::201			lda	ND_Data,x
			sec
			sbc	#$30
			cmp	#10
			bcc	:201a
			sbc	#$07
::201a			asl
			asl
			asl
			asl
			sta	r0L
			inx
			lda	ND_Data,x
			sec
			sbc	#$30
			cmp	#10
			bcc	:201b
			sbc	#$07
::201b			ora	r0L
			sta	(r14L),y
			inx
			rts

:LoadIconData		ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			beq	:1
			cmp	#$ff
			beq	:2a
			cmp	#$fe
			beq	:3a
			cmp	#$fd
			beq	:4a

::4a			ldy	#$3f
::4			lda	Icon_Map,y
			sta	(r15L),y
			dey
			bpl	:4
			rts

::3a			ldy	#$3f
::3			lda	Icon_Printer,y
			sta	(r15L),y
			dey
			bpl	:3
			rts

::2a			ldy	#$3f
::2			lda	Icon_Drive,y
			sta	(r15L),y
			dey
			bpl	:2
			rts

::1			jsr	AL_SET_DEVICE

			MoveW	r14,r6
			jsr	FindFile
			LoadW	r9,dirEntryBuf
			jsr	GetFHdrInfo

			ldy	#$3f
::5			lda	fileHeader +4,y
			sta	(r15L),y
			dey
			bpl	:5
			rts

:xLNK_SAVE_DATA		jsr	TempLinkDrive
			txa
			bne	:1

			jsr	LNK_FIND_CONFIG
			txa
			beq	:2
			jsr	LNK_NEW_CONFIG
			txa
			beq	:2
::1			jmp	EnterDeskTop

::2			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	GetBlock
			txa
			bne	:1

			ldx	#$00
			stx	ND_Record
			inx
			stx	r13H
			LoadW	r14,LinkData +LINK_DATA_BUFSIZE +1
			LoadW	r15,Icon_01

::3			lda	ND_Record
			cmp	#25
			bcc	:3a
			jmp	:5

::3a			asl
			tax
			lda	ND_VLIR +2,x
			bne	:4a

			lda	#$01
			sta	r3L
			sta	r3H
			jsr	SetNextFree
			txa
			beq	:ab
::aa			jmp	Panic

::ab			lda	ND_Record
			asl
			tax
			lda	r3L
			sta	r1L
			sta	ND_VLIR +2,x
			lda	r3H
			sta	r1H
			sta	ND_VLIR +3,x

			lda	#$00
			sta	ND_Data +2
			sta	ND_Data +0
			lda	#$02
			sta	ND_Data +1

::4a			jsr	SaveNotesData

			lda	ND_Data +2
			bne	:4c
			lda	ND_Record
			asl
			tax
			lda	ND_VLIR +2,x
			sta	r6L
			lda	#$00
			sta	ND_VLIR +2,x
			lda	ND_VLIR +3,x
			sta	r6H
			lda	#$ff
			sta	ND_VLIR +3,x
			jsr	FreeBlock
			jmp	:4d

::4c			lda	ND_Record
			asl
			tax
			lda	ND_VLIR +2,x
			sta	r1L
			lda	ND_VLIR +3,x
			sta	r1H
			LoadW	r4,ND_Data
			jsr	PutBlock

::4d			jsr	PutDirHead
			txa
			beq	:4
			jmp	:aa

::4			inc	r13H
			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15
			inc	ND_Record
			jmp	:3

::5			lda	dirEntryBuf +1
			sta	r1L
			lda	dirEntryBuf +2
			sta	r1H
			LoadW	r4,ND_VLIR
			jsr	PutBlock
			txa
			bne	:ac
			jmp	BackTempDrive
::ac			jmp	Panic

:SaveNotesData		lda	r13H
			cmp	LinkData
			bcc	:0
			lda	#$00
			sta	ND_Data +2
::z			rts

::0			ldx	#$02
			stx	r13L

			ldy	#LINK_DATA_FILE
::1			lda	(r14L),y
			beq	:2
			sta	ND_Data,x
			iny
			inx
			bne	:1
::2			lda	#CR
			sta	ND_Data,x
			inx

			ldy	#LINK_DATA_NAME
::3			lda	(r14L),y
			beq	:4
			sta	ND_Data,x
			iny
			inx
			bne	:3
::4			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_XPOS
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_YPOS
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_COLOR
::5			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L
			iny
			cpy	#LINK_DATA_COLOR +9
			bcc	:5
			lda	#CR
			sta	ND_Data,x
			inx

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			clc
			adc	#$39
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DVTYP
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

			ldy	#LINK_DATA_DPART
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

;--- Verzeichnis/Spur.
			ldy	#LINK_DATA_DSDIR +0
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

;--- Verzeichnis/Sektor.
			ldy	#LINK_DATA_DSDIR +1
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
			stx	r13L

;--- Verzeichnis-Eintrag/Spur.
			ldy	#LINK_DATA_ENTRY +0
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

;--- Verzeichnis-Eintrag/Sektor.
			ldy	#LINK_DATA_ENTRY +1
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			stx	r13L

;--- Verzeichnis-Eintrag/Zeiger.
			ldy	#LINK_DATA_ENTRY +2
			lda	(r14L),y
			jsr	:11
			pha
			txa
			ldx	r13L
			sta	ND_Data,x
			inx
			pla
			sta	ND_Data,x
			inx
			lda	#CR
			sta	ND_Data,x
			inx
;			stx	r13L

			lda	#NULL
			sta	ND_Data,x
			sta	ND_Data +0
			stx	ND_Data +1
			rts

::11			pha
			lsr
			lsr
			lsr
			lsr
			jsr	:12
			tax
			pla
			and	#%00001111
::12			cmp	#10
			bcc	:13
			clc
			adc	#$07
::13			adc	#$30
			rts
