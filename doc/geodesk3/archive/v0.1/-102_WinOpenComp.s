﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Arbeitsplatz öffnen.
:OpenMyComputer		lda	Flag_MyComputer		;Ist "MyComputer" bereits geöffnet?
			beq	:1			; => Nein, weiter...
			jsr	WM_WIN2TOP		;Fenster "MyComputer" an erster
			jmp	WM_DRAW_ALL_WIN		;Stelle anordnen.

::1			jsr	WM_IS_WIN_FREE		;Freies Fenster suchen.
			cpx	#NO_ERROR		;Ist noch ein Fenster frei?
			bne	:2			; => Ende, kein Fenster mehr frei.

			sta	Flag_MyComputer		;Fenster-Nr. für "MyComputer"
							;speichern.

			jsr	WM_CLR_WINDRVDAT	;Keine Laufwerksdaten speichern.

			LoadW	r0,WinData_MyComp
			jmp	WM_OPEN_WINDOW

::2			rts

;*** Arbeitsplatz aktualisieren.
:UpdateMyComputer	lda	Flag_MyComputer		;Ist "MyComputer" bereits geöffnet?
			beq	:1			; => Nein, weiter...
			jsr	WM_WIN2TOP		;"MyComputer" nach oben und
			jsr	WM_UPDATE		;Fensterinhalt aktualisieren.
::1			rts

;*** Arbeitsplatz schliesen.
;    (Aufruf über FensterManager).
:CloseMyComputer	lda	#$00
			sta	Flag_MyComputer
			rts

;*** Icons für Arbeitsplatz anzeigen.
:DrawMyComputer		lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:0c
			ldx	#$00
			rts

::0c			sta	r1L

			ldx	r0L
			cpx	#$04
			bcs	:1q

			lda	driveType,x
			beq	:1q

			ldx	#$00
::0a			lda	r0L,x
			pha
			inx
			cpx	#10
			bcc	:0a

			lda	r0L
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk
			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:1m
			jsr	OpenRootDir

::1m			ldx	#$09
::0b			pla
			sta	r0L,x
			dex
			bpl	:0b

::1q			lda	r0H
			pha
			lda	r0L
			pha
			asl
			tax
			lda	WP_Colors   +0,x
			sta	r8L
			lda	WP_Colors   +1,x
			sta	r8H
			txa
			asl
			tax
			lda	WP_IconData +0,x
			sta	r0L
			lda	WP_IconData +1,x
			sta	r0H
			lda	WP_IconData +2,x
			sta	r5L
			lda	WP_IconData +3,x
			sta	r5H

			ldy	#$00
			sty	dataFileName
			pla
			pha
			cmp	#$04
			bcs	:1a
			tax
			lda	driveType,x
			beq	:1d

::1a			lda	(r5L),y
			beq	:1b
			cmp	#$a0
			beq	:1b
			sta	dataFileName,y
			iny
			cpy	#$10
			bcc	:1a
::1b			lda	#$00
			sta	dataFileName,y
			iny
			cpy	#$11
			bcc	:1b

::1d			PushB	r1L
			PushB	r1H
			LoadB	r2L,$03
			LoadB	r2H,$15
			LoadB	r3L,$00
			LoadB	r3H,4
			LoadW	r4,dataFileName
			jsr	GD_FICON_NAME

			pla
			clc
			adc	#$07
			sta	r1H
			pla
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			pla
			pha
			cmp	#$04
			bcs	:1c

			lda	#" "
			jsr	SmallPutChar
			pla
			pha
			clc
			adc	#$41
			jsr	SmallPutChar
			lda	#":"
			jsr	SmallPutChar

::1c			pla
			sta	r0L
			pla
			sta	r0H
			ldx	#$ff
::2			rts

;*** Daten für Icons im "MyComputer".
:WP_IconData		w Icon_Drive  ,DrACurDkNm
			w Icon_Drive  ,DrBCurDkNm
			w Icon_Drive  ,DrCCurDkNm
			w Icon_Drive  ,DrDCurDkNm
			w Icon_Printer,PrntFileName
			w Icon_Input  ,inputDevName

:WP_Colors		w Color_Drive
			w Color_Drive
			w Color_Drive
			w Color_Drive
			w Color_Prnt
			w Color_Inpt

:Color_Drive		b $05,$05,$05,$0f,$0f,$0f,$09,$09,$09
:Color_Prnt		b $15,$15,$05,$bf,$bf,$b5,$b9,$b9,$b9
:Color_Inpt		b $05,$05,$05,$05,$01,$05,$b9,$09,$b9
:Color_SDir		b $75,$75,$75,$75,$75,$75,$79,$79,$79
:Color_Std		b $01,$01,$01,$01,$01,$01,$01,$01,$01
