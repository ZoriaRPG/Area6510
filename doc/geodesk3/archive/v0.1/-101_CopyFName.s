﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateiname aus Verzeichniseintrag kopieren.
;    Übergabe: XReg = ZeroPage-Register/Zeiger Verzeichnis-Eintrag.
;              YReg = ZeroPage-Register/Zeiger auf 17Byte-Puffer.
.SysCopyFName		stx	:read +1
			sty	:write1 +1
			sty	:write2 +1

;--- Dateiname aus Verzeichnis-Eintrag kopieren.
			ldy	#$00
::loop			iny				;Zeiger auf Name im Verzeichnis-
			iny				;Eintrag setzen.
			iny
			iny
			iny
::read			lda	(r0L),y			;Zeichen aus Dateiname einlesen.
			dey
			dey
			dey
			dey
			dey
			tax				;Ende-Markierung erreicht?
			beq	:end			; => Ja, Ende...
			cmp	#$a0			;Ende-Markierung erreicht?
			beq	:end			; => Ja, Ende...
::write1		sta	(r0L),y			;Zeichen in Puffer kopieren.
			iny				;Zeiger auf Puffer korrigieren.
			cpy	#$10			;Puffer voll?
			bcc	:loop			; => Nein, weiter...

;--- Ende, Puffer mit $00-Bytes auffüllen.
::end			lda	#$00			;Dateiname auf 17 Zeichen mit
::write2		sta	(r0L),y			;$00-Bytes auffüllen.
			iny
			cpy	#$10
			bcc	:write2
			rts
