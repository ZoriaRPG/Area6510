﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Freie 64K-Speicherbank suchen.
:FindFreeBank		ldy	ramExpSize
			beq	:2
::1			dey
			jsr	GetBankByte
			beq	:3
			tya
			bne	:1
::2			ldx	#NO_FREE_RAM
			b $2c
::3			ldx	#NO_ERROR
			rts

;*** Tabellenwert für Speicherbank finden.
:GetBankByte		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
::1			cpx	#$00
			beq	:2
			asl
			asl
			dex
			bne	:1
::2			and	#%11000000
			rts

;*** Speicherbank reservieren.
:AllocateBank		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
			ora	:DOUBLE_BIT,x
			pha
			tya
			lsr
			lsr
			tax
			pla
			sta	RamBankInUse,x
			rts

::DOUBLE_BIT		b %11000000
			b %00110000
			b %00001100
			b %00000011

;*** Speicherbank freigeben.
:FreeBank		tya
			lsr
			lsr
			tax
			lda	RamBankInUse,x
			pha
			tya
			and	#%00000011
			tax
			pla
			and	:DOUBLE_BIT,x
			pha
			tya
			lsr
			lsr
			tax
			pla
			sta	RamBankInUse,x
			rts

::DOUBLE_BIT		b %00111111
			b %11001111
			b %11110011
			b %11111100
