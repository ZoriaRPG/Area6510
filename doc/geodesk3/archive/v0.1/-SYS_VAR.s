﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

.SYSVAR_START

.GD_SCRN_STACK		b $00
.GD_SYSTEM_BUF		b $00
.GD_RAM_GDESK		b $00

.GD_CLASS		b "GeoDesk64   V0.1",NULL
.GD_APPLINK		b "GeoDesk.lnk",NULL

:GD_DACC_ADDR		s GD_VLIR_COUNT * 4

.curVLIRModule		b $00
.curSystemName		s 17

.BootDrive		b $00
.BootPart		b $00
.BootType		b $00
.BootMode		b $00
.BootSDir		b $00,$00
.BootRBase		b $00

.TempDrive		b $00
.TempMode		b $00
.TempPart		b $00
.TempSDir		b $00,$00

.LinkDrive		b $08
.LinkPart		b $05
.LinkType		b $33
.LinkMode		b $80
.LinkSDir		b $00,$00
.LinkRBase		b $00

.SYSVAR_END
.SYSVAR_SIZE = SYSVAR_END - SYSVAR_START
