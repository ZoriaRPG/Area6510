﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS/Einstellungen - Setup starten.
:MENU_SETUP_EDIT	jsr	UpdateCore
			jmp	MNU_OPEN_EDITOR

;*** GEOS/Einstellungen - Drucker wechseln.
:MENU_SETUP_PRNT	lda	#$08
			jsr	SetDevice
			jsr	SLCT_PRINTER
			txa
			bne	:exit
			jmp	UpdateMyComputer
::exit			rts

;*** GEOS/Einstellungen - Eingabegerät wechseln.
:MENU_SETUP_INPT	lda	#$08
			jsr	SetDevice
			jsr	SLCT_INPUT
			txa
			bne	:exit
			jmp	UpdateMyComputer
::exit			rts

;*** GEOS/Einstellungen - AppLinks speichern.
;Wird direkt über EXIT_MENU_SETUP aufgerufen.
;MENU_SETUP_SALNK	jmp	LNK_SAVE_DATA

;*** GEOS/Einstellungen - Laufwerks-Cache.
:MENU_SETUP_CACHE	jsr	WM_CLOSE_ALL_WIN	;Alle Fenster schließen.

			ldx	Flag_CacheModeOn
			bne	:2
			dex
			stx	Flag_CacheModeOn
			ldy	WM_WCODE
			lda	#%10000000
			sta	ramCacheMode0,y
			bne	:4

::2			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			cmp	#%11000000
			beq	:3
			lda	#%11000000
			sta	ramCacheMode0,y
			bne	:4

::3			inx
			stx	Flag_CacheModeOn
			ldy	WM_WCODE
			lda	#%00000000
			sta	ramCacheMode0,y
::4			rts

;*** GEOS/Einstellungen - Laufwerks-Cache/Debug-Modus.
:MENU_SETUP_DEBUG	jsr	WM_CLOSE_ALL_WIN	;Alle Fenster schließen.
			ldx	#$00
			lda	colorModeDebug
			eor	#$ff
			sta	colorModeDebug
			cmp	#$00
			beq	:1
			ldx	#$20
::1			stx	colorModeIcons
			rts

;*** PopUp/Arbeitsplatz - Laufwerk öffnen.
:PF_OPEN_DRV_A		ldx	#$08
			b $2c
:PF_OPEN_DRV_B		ldx	#$09
			b $2c
:PF_OPEN_DRV_C		ldx	#$0a
			b $2c
:PF_OPEN_DRV_D		ldx	#$0b
			lda	driveType -8,x
			beq	:2
			txa
			jsr	SetDevice
			txa
			bne	:2

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:1
			jsr	OpenRootDir
			txa
			bne	:2

::1			lda	curDrive
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveNewWin
::2			rts

;*** PopUp Desktop - Titel anzeigen.
:PF_VIEW_ALTITLE	lda	Flag_ViewALTitle
			eor	#$ff
			sta	Flag_ViewALTitle
			jsr	MNU_UPDATE
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN

;*** PopUp Desktop - Hintergrund anzeigen.
:PF_BACK_SCREEN		lda	sysRAMFlg
			eor	#%00001000
			sta	sysRAMFlg
			ldx	#$00
			and	#%00001000
			beq	:1
			dex
::1			stx	Flag_BackScreen
			jsr	MNU_UPDATE
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN

;*** PopUp DeskTop - AppLinks sperren.
:PF_LOCK_APPLINK	lda	appLinkLocked
			eor	#$ff
			sta	appLinkLocked
			rts

;*** PopUp DeskTop/AppLink - Verzeichnis öffnen.
:PF_OPEN_SDIR		MoveW	AL_VEC_FILE,r14
			jmp	AL_OPEN_SDIR

;*** PopUp Datei/Verzeichnis - Öffnen.
:PF_OPEN_FILE		MoveW	vecDirEntry,r0		;Zeiger auf Verzeichnis-Eintrag.
			jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;*** PopUp Datei/Verzeichnis - Löschen.
:PF_DEL_FILE		MoveW	vecDirEntry,r0
			LoadW	r1,dataFileName
			ldx	#r0L 			;Dateiname aus Verzeichnis-Eintrag
			ldy	#r1L			;in Puffer kopieren.
			jsr	SysCopyFName

			LoadW	r0,dataFileName
			jsr	DeleteFile
			jmp	WM_CALL_DRAW

;*** PopUp Fenster/Native - ROOT öffnen.
:PF_OPEN_ROOT		ldx	WM_WCODE
			lda	#$01
			sta	WMODE_SDIR_T,x
			sta	WMODE_SDIR_S,x
			jsr	OpenRootDir
			jmp	WM_CALL_DRAW

;*** PopUp Fenster/Native - Verzeichnis zurück.
:PF_OPEN_PARENT		jsr	OpenWinDrive

			lda	curDirHead+34		;Hauptverzeichnis ?
			beq	:1
			sta	r1L
			ldx	WM_WCODE
			sta	WMODE_SDIR_T,x
			lda	curDirHead+35
			sta	r1H
			sta	WMODE_SDIR_S,x
			jsr	OpenSubDir
			jmp	WM_CALL_DRAW
::1			rts

;*** PopUp Fenster - Neue Ansicht.
:PF_NEW_VIEW		jsr	OpenWinDrive

			ldx	WM_WCODE
			lda	WMODE_DRIVE,x
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveNewWin

;*** PopUp Fenster - Neu laden.
:PF_RELOAD_DISK		jsr	OpenWinDrive
			jmp	WM_CALL_DRAW

;*** PopUp Fenster - Partition wechseln.
:PF_OPEN_PART		lda	WM_WCODE
			jsr	WM_WIN2TOP
			jsr	OpenWinDrive

			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox

			ldx	curDrive
			lda	drivePartData -8,x
			ldx	WM_WCODE
			sta	WMODE_PART,x
			lda	curDirHead +32
			sta	WMODE_SDIR_T,x
			lda	curDirHead +33
			sta	WMODE_SDIR_S,x

			jsr	WM_DRAW_NO_TOP
			jmp	WM_CALL_DRAW

;*** PopUp Fenster/Anzeige - Gelöschte Dateien anzeigen.
:PF_VIEW_DELFILE	ldy	WM_WCODE
			lda	WMODE_VDEL,y
			eor	#$ff
			sta	WMODE_VDEL,y
			jmp	WM_CALL_DRAW

;*** PopUp Fenster/Anzeige - KByte/Blocks.
:PF_VIEW_SIZE		ldy	WM_WCODE
			lda	WMODE_VSIZE,y
			eor	#$ff
			sta	WMODE_VSIZE,y
			jmp	WM_CALL_REDRAW

;*** PopUp Fenster/Anzeige - Icons anzeigen.
:PF_VIEW_ICONS		ldy	WM_WCODE
			lda	#$00
			sta	WMODE_VINFO,y
			lda	WMODE_VICON,y
			eor	#$ff
			sta	WMODE_VICON,y
			bne	:1

			jsr	WM_LOAD_WIN_DATA
			lda	#$00
			sta	WM_DATA_GRID_Y
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

::1			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$00
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

;*** PopUp Fenster/Anzeige - Textmodus.
:PF_VIEW_DETAILS	ldy	WM_WCODE
			lda	#$ff
			sta	WMODE_VICON,y
			lda	WMODE_VINFO,y
			eor	#$ff
			sta	WMODE_VINFO,y
			bne	:1

			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$00
			sta	WM_DATA_COLUMN
			lda	#$ff
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

::1			jsr	WM_LOAD_WIN_DATA
			lda	#$08
			sta	WM_DATA_GRID_Y
			lda	#$01
			sta	WM_DATA_COLUMN
			lda	#$ee
			sta	WM_DATA_WINMOVE +0
			sta	WM_DATA_WINMOVE +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_REDRAW

;*** PopUp Fenster/Anzeige - SlowMode.
:PF_VIEW_SLOWMOVE	ldy	WM_WCODE
			lda	WMODE_FMOVE,y
			eor	#$ff
			sta	WMODE_FMOVE,y
			rts

;*** PopUp Fenster/Anzeige - Icons/Farbe anzeigen.
:PF_VIEW_SETCOLOR	lda	colorMode
			eor	#$ff
			sta	colorMode
			jsr	MNU_UPDATE
			jmp	WM_CALL_REDRAW

;*** PopUp Fenster - Dateifilter.
:PF_FILTER_ALL		lda	#$00
			b $2c
:PF_FILTER_BASIC	lda	#$80
			b $2c
:PF_FILTER_APPS		lda	#$80 ! APPLICATION
			b $2c
:PF_FILTER_EXEC		lda	#$80 ! AUTO_EXEC
			b $2c
:PF_FILTER_DOCS		lda	#$80 ! APPL_DATA
			b $2c
:PF_FILTER_DA		lda	#$80 ! DESK_ACC
			b $2c
:PF_FILTER_FONT		lda	#$80 ! FONT
			b $2c
:PF_FILTER_PRNT		lda	#$80 ! PRINTER
			b $2c
:PF_FILTER_INPT		lda	#$80 ! INPUT_DEVICE
			ldx	WM_WCODE
			sta	WMODE_FILTER,x
			jmp	WM_CALL_DRAW

;*** PopUp Laufwerk - Öffnen.
:PF_OPEN_DRIVE		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveCurWin

;*** PopUp Laufwerk/Fenster - Neues Fenster.
:PF_OPEN_NEWVIEW	lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveNewWin

;*** PopUp Laufwerk/Fenster - Partition wechseln.
:PF_SWAP_PART		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldy	MyComputerEntry +1
			bne	:1
			lda	MyComputerEntry +0
			cmp	#$04
			bcs	:1
			adc	#$08
			jsr	SetDevice
			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox
			jsr	WM_DRAW_NO_TOP
			jmp	WM_CALL_DRAW
::1			rts

;*** PopUp Laufwerk/Fenster - AppLink erstellen.
:PF_CREATE_AL		jsr	AL_SET_INIT
			ldx	#r3L
			jsr	GetPtrCurDkNm
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE

			lda	curDirHead +34
			ora	curDirHead +35
			bne	:1
			jsr	AL_SET_TYPDV
			jsr	AL_SET_COL_DRV
			ldx	#<Icon_Drive +1
			ldy	#>Icon_Drive +1
			jmp	:2

::1			jsr	AL_SET_TYPSD
			jsr	AL_SET_COL_SDIR
			ldx	#<Icon_Map +1
			ldy	#>Icon_Map +1
::2			stx	r4L
			sty	r4H

			LoadB	r3L,1
			jsr	DrawSprite
			jsr	AL_SET_ICON

			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS

			jsr	AL_SET_WMODE

			lda	curDirHead +32		;Zeiger auf aktuellen
			ldx	curDirHead +33		;Verzeichnis-Header.
			jsr	AL_SET_SDIR

			lda	curDirHead +36		;Zeiger auf aktuellen Verzeichnis-
			ldx	curDirHead +37		;Eintrag zur Prüfung ob das
			ldy	curDirHead +38		;Verzeichnis noch existiert.
			jsr	AL_SET_SDIRE

			jmp	AL_SET_END
