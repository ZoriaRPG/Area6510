﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* GeoDesk von Disk starten.
;* GeoDesk aus DACC starten.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopMac.MP3"
			t "TopSym.GD"
			t "-SYS_APPLINK"
endif

			n "mod.#101.obj"
			f APPLICATION
			o APP_RAM
			p MainInit
			c "GeoDesk64   V0.1"
			a "Markus Kanet"

;*** Einsprungtabelle für Modul-Funktionen.
:VlirJumpTable		jmp	MNU_INIT
			jmp	MNU_REBOOT

;*** Systemvariablen.
			t "-SYS_VAR"

;*** Programmvariablen.
			t "-101_VarDataGD"
			t "-101_VarDataWM"

;*** Systemroutinen.
			t "-101_WM"
			t "-101_SwitchDrive"
			t "-101_DlgTitle"
			t "-101_DrawClock"
			t "-101_UpdateVLIR"
			t "-101_LoadVLIR"
			t "-101_AppLinkDev"
			t "-101_AppLinkData"
			t "-101_ResetFontGD"
			t "-101_CopyFName"

;*** System-Icons.
			t "-101_SystemIcons"

;*** Spezieller Zeichensatz (7x8)
.FontG3			v 7,"fnt.GeoDesk"

;*** Beginn Speicher für VLIR-Module.
.VLIR_BASE
