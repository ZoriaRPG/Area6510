﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Icon verschieben.
;    Übergabe: r4 = Zeiger auf Icon-Daten.
;    Rückgabe: XReg = $00/Kein Fehler.
;              AKKU = Fenster-Nr. für Icon-Ablage.
:DRAG_N_DROP_ICON	php
			sei
			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA

			lda	#$00
			sta	$d028
			lda	#$07
			sta	$d029

			stx	CPU_DATA
			plp

			lda	#$ff			;Hintergrund-Sprite#2 löschen.
			ldy	#$3e 			;Dieses Sprite deckt dann den
::0			sta	spr2pic,y		;Bildschirm-Hintergrund ab.
			dey
			bpl	:0

			LoadB	r3L,1			;Icon-Daten in Sprite#1 kopieren.
			jsr	DrawSprite
			jsr	EnablSprite

			LoadB	r3L,2			;Hintergrund-Sprite#2 einschalten.
			jsr	EnablSprite

			LoadB	mouseTop   ,$00
			LoadB	mouseBottom,MIN_AREA_BAR_Y -$08 -$18
			LoadW	mouseLeft  ,$0010
			LoadW	mouseRight ,SCR_WIDTH_40   -$10 -$18

			MoveW	appMain,:appMain_Buf
			LoadW	appMain,:1

			pla
			sta	:returnAdr_Buf +1
			pla
			sta	:returnAdr_Buf +0
			rts

::appMain_Buf		w $0000
::returnAdr_Buf		w $0000

::1			MoveW	mouseXPos,r4
			MoveB	mouseYPos,r5L
			LoadB	r3L,1
			jsr	PosSprite
			LoadB	r3L,2
			jsr	PosSprite

			lda	mouseData
			bmi	:2

			lda	:appMain_Buf +0
			ldx	:appMain_Buf +1
			jmp	CallRoutine

;--- Icon wurde abgelegt:
::2			LoadB	r3L,1
			jsr	DisablSprite
			LoadB	r3L,2
			jsr	DisablSprite

			jsr	WM_NO_MOUSE_WIN

			MoveW	:appMain_Buf,appMain

			lda	:returnAdr_Buf +0
			pha
			lda	:returnAdr_Buf +1
			pha

			jsr	WM_FIND_WINDOW
			cpx	#NO_ERROR
			bne	:3
			lda	WindowStack,y		;Neues Fenster einlesen und
::3			rts
