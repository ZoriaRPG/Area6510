﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Zeiger auf Menü-Initialisierung setzen.
:OPEN_MENU_SETINT	LoadW	appMain,OPEN_MENU_INIT
			rts

;*** Erweiterung für ":DoPreviousMenu", damit auch Farben des
;    letzten Menüs richtig gesetzt werden.
:OPEN_PREV_MENU		lda	Rectangle +1
			sta	:2 +1
			lda	Rectangle +2
			sta	:3 +1

			lda	#< :1
			sta	Rectangle +1
			lda	#> :1
			sta	Rectangle +2

			jmp	DoPreviousMenu

::1			lda	C_PullDMenu
			jsr	DirectColor
::2			lda	#$ff
			sta	Rectangle +1
::3			ldx	#$ff
			stx	Rectangle +2
			jmp	CallRoutine

;*** Hintergrundfarbe für menü setzen.
:OPEN_MENU_SETCOL	ldy	#$05
::1			lda	(r0L),y
			sta	r2,y
			dey
			bpl	:1

			lda	C_PullDMenu
			jmp	DirectColor

;*** Vektoren zurücksetzen.
:CLOSE_MENU		lda	#$00
			sta	mouseFaultVec +0
			sta	mouseFaultVec +1
			sta	RecoverVector +0
			sta	RecoverVector +1

			lda	mouseOn
			and	#%10111111
			sta	mouseOn

			clc
			jsr	StartMouseMode
			jsr	WM_NO_MOUSE_WIN
			jmp	InitForWM

;*** GEOS-Menü öffnen.
:OPEN_MENU_GEOS		jsr	MNU_UPDATE
			jsr	DoneWithWM		;Fenster-Abfrage abschalten.
			jsr	WM_NO_MARGIN		;Fenster-grenzen löschen.
			jsr	WM_SAVE_BACKSCR		;Aktuellen Bildschirm speichern.

			LoadW	r0,MENU_DATA_GEOS	;Zeiger auf Menü-Daten setzen.

;*** Farbe setzen und PullDown-Menü öffnen.
:OPEN_MENU		jsr	OPEN_MENU_SETCOL	;Farbe für Menü setzen.

			lda	#$01
			jsr	DoMenu			;Menü aktivieren.

;*** Menü-Vektoren setzen.
:OPEN_MENU_INIT		LoadW	appMain,DrawClock
			LoadW	RecoverVector,:1
			LoadW	mouseFaultVec,:2
			rts

;--- Ersatz für RecoverRectangle.
::1			LoadW	r10,GD_BACKSCR_BUF
			LoadW	r11,GD_BACKCOL_BUF
			lda	GD_SYSTEM_BUF
			jmp	WM_LOAD_AREA

;--- Mausabfrage.
::2			lda	faultData		;Hat Mauszeiger aktuelles Menü
			and	#%00001000		;verlassen ?
			bne	:3			;Ja, ein Menü zurück.
			ldx	#%10000000
			ldy	#%11000000
::3			txa				;Hat Mauszeiger obere/linke
			and	faultData		;Grenze verlassen ?
			bne	:4			;Ja, ein Menü zurück.
			tya

::4			lda	menuNumber		;Hauptmenü aktiv ?
			beq	:5			;Ja, übergehen.
			jsr	OPEN_PREV_MENU		;Ein Menü zurück.
			jmp	OPEN_MENU_INIT

::5			jsr	RecoverAllMenus		;Menüs löschen.
			jmp	CLOSE_MENU		;Vektoren zurücksetzen.

;*** Menü "Programme" aktivieren.
:OPEN_MENU_APPL		LoadW	r0,MENU_DATA_APPL
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Programme" verlassen.
:EXIT_MENU_APPL		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

			cmp	#$00			; => Cache aktivieren.
			beq	:1
			cmp	#$01			; => Cache-Debug aktivieren.
			beq	:3
			rts

::1			jmp	MNU_OPEN_APPL
::3			jmp	MNU_OPEN_AUTO

;*** Menü "Dokumente" aktivieren.
:OPEN_MENU_DOCS		LoadW	r0,MENU_DATA_DOCS
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Dokumente" verlassen.
:EXIT_MENU_DOCS		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

			cmp	#$00			; => Alle Dokumente.
			beq	:1
			cmp	#$01			; => geoWrite Dokumente.
			beq	:2
			cmp	#$02			; => geoPaint Dokumente.
			beq	:3
			rts

::1			jmp	MNU_OPEN_DOCS
::2			jmp	MNU_OPEN_WRITE
::3			jmp	MNU_OPEN_PAINT

;*** Menü "Beenden" aktivieren.
:OPEN_MENU_EXIT		LoadW	r0,MENU_DATA_EXIT
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Beenden" verlassen.
:EXIT_MENU_EXIT		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

			cmp	#$00			; => Nach GEOS beenden.
			beq	:1
			cmp	#$01			; => Nach BASIC beenden.
			beq	:2
			cmp	#$02			; => BASIC-Programm starten.
			beq	:3
			rts

::1			jmp	MNU_OPEN_EXITG
::2			jmp	MNU_OPEN_EXIT64
::3			jmp	MNU_OPEN_EXITB

;*** Menü "Einstellungen" aktivieren.
:OPEN_MENU_SETUP	LoadW	r0,MENU_DATA_SETUP
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Einstellungen" verlassen.
:EXIT_MENU_SETUP	pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla

			cmp	#$00			; => Setup starten.
			beq	:1
			cmp	#$01			; => Drucker wechseln.
			beq	:2
			cmp	#$02			; => Eingabe wechseln.
			beq	:3
			cmp	#$03			; => Eingabe wechseln.
			beq	:4
			cmp	#$04			; => AppLinks speichern.
			beq	:5
			cmp	#$05			; => Konfiguration speichern.
			beq	:6
::0			rts

::1			jmp	MENU_SETUP_EDIT
::2			jmp	MENU_SETUP_PRNT
::3			jmp	MENU_SETUP_INPT
::4			jmp	MNU_OPEN_BACKSCR
::5			jmp	LNK_SAVE_DATA
::6			jmp	MNU_SAVE_CONFIG

;*** Menü "Disk-Cache" aktivieren.
:OPEN_MENU_CACHE	LoadW	r0,MENU_DATA_CACHE
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** Menü "Disk-Cache" verlassen.
:EXIT_MENU_CACHE	pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla

			cmp	#$00			; => Cache aktivieren.
			beq	:1
			cmp	#$01			; => Cache-Debug aktivieren.
			beq	:3
			rts

::1			jmp	MENU_SETUP_CACHE
::3			jmp	MENU_SETUP_DEBUG

;*** Untermenüs aus Kontextmenü öffnen.
:OPEN_MENU_VOPT		ldx	#11
			b $2c
:OPEN_MENU_DISK		ldx	#12
			b $2c
:OPEN_MENU_FILTER	ldx	#14
			jsr	OPEN_MENU_GETDAT
			jsr	OPEN_MENU_SETCOL
			jmp	OPEN_MENU_SETINT

;*** PopUp-Menü öffnen.
:PM_OPEN_MENU		jsr	OPEN_MENU_GETDAT

			jsr	DoneWithWM
			jsr	WM_NO_MARGIN
			jsr	WM_SAVE_BACKSCR

			jmp	OPEN_MENU

;*** PopUp-Menü beenden.
:PopUpExit_000		ldx	#0
			b $2c
:PopUpExit_001		ldx	#1
			b $2c
:PopUpExit_002		ldx	#2
			b $2c
:PopUpExit_003		ldx	#3
			b $2c
:PopUpExit_004		ldx	#4
			b $2c
:PopUpExit_005		ldx	#5
			b $2c
:PopUpExit_006		ldx	#6
			b $2c
:PopUpExit_007		ldx	#7
			b $2c
:PopUpExit_008		ldx	#8
			b $2c
:PopUpExit_009		ldx	#9
			b $2c
:PopUpExit_010		ldx	#10
			b $2c
:PopUpExit_011		ldx	#11
			b $2c
:PopUpExit_012		ldx	#12
			b $2c
:PopUpExit_013		ldx	#13
			b $2c
:PopUpExit_014		ldx	#14
			pha
			txa
			pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla
			asl
			tax
			lda	PopUpRout_Vec +0,x
			sta	r0L
			lda	PopUpRout_Vec +1,x
			sta	r0H
			pla
			asl
			tay
			iny
			lda	(r0L),y
			tax
			dey
			lda	(r0L),y
			jmp	CallRoutine

;*** Informationen für aktuelles Menü einlesen.
;    r0   = Zeiger auf Menüdaten.
;    AKKU = Menü-Breite.
:OPEN_MENU_GETDAT	jsr	MNU_UPDATE

			lda	PopUpData_Width,x
			sta	r5H
			txa
			asl
			tax
			lda	PopUpData_Vec +0,x
			sta	r0L
			lda	PopUpData_Vec +1,x
			sta	r0H

			ldy	#$06
			lda	(r0L),y
			and	#%00001111

			tay
			lda	#$02
::1			clc
			adc	#14
			dey
			bne	:1

			ora	#%00000111
			sta	r5L

			lda	#MAX_AREA_WIN_Y -1
			sec
			sbc	r5L
			cmp	mouseYPos
			bcc	:4
			lda	mouseYPos
::4			and	#%11111000
			sta	r2L
			clc
			adc	r5L
			sta	r2H

			sec
			lda	#< MAX_AREA_WIN_X -1
			sbc	r5H
			sta	r3L
			lda	#> MAX_AREA_WIN_X -1
			sbc	#$00
			sta	r3H

			CmpW	r3,mouseXPos
			bcc	:5
			MoveW	mouseXPos,r3

::5			clc
			lda	r3L
			and	#%11111000
			sta	r3L
			adc	r5H
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

			ldy	#$05
::6			lda	r2,y
			sta	(r0L),y
			dey
			bpl	:6
			rts

;*** Menu-Einträge aktualisieren.
;    XReg darf nicht verändert werden!
:MNU_UPDATE		ldy	#" "
			lda	Flag_ViewALTitle
			beq	:1
			ldy	#"*"
::1			sty	PopUpText_108 +3

			ldy	#" "
			lda	colorMode
			bne	:2
			ldy	#"*"
::2			sty	PopUpText_118 +3

			ldy	#" "
			lda	sysRAMFlg
			and	#%00001000
			beq	:3
			ldy	#"*"
::3			sty	PopUpText_054 +3

			ldy	#" "
			lda	colorModeDebug
			beq	:4
			ldy	#"*"
::4			sty	GMenuText_21 +3

			lda	Flag_CacheModeOn
			beq	:5
			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000		;RAM-Cache aktiv?
::5			ldy	#" "
			cmp	#$00
			beq	:6
			ldy	#"+"
			cmp	#%10000000
			beq	:6
			ldy	#"*"
::6			sty	GMenuText_20 +3

			ldy	WM_WCODE
			lda	WMODE_FILTER,y
			ldy	#" "
			cmp	#$00
			beq	:7
			ldy	#"*"
::7			sty	PopUpText_120 +3

			ldy	#" "
			lda	appLinkLocked
			beq	:8
			ldy	#"*"
::8			sty	PopUpText_055 +3

			rts

;*** PopUp-Fenster für DeskTop-Eigenschaften.
:PM_PROPERTIES		jsr	AL_FIND_ICON
			txa
			bne	:10

			MoveB	r13H,AL_CNT_FILE
			MoveW	r14 ,AL_VEC_FILE
			MoveW	r15 ,AL_VEC_ICON

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			cmp	#$00
			beq	:11
			cmp	#$80
			beq	:12
			cmp	#$fd
			beq	:15
			cmp	#$fe
			beq	:13
			cmp	#$ff
			beq	:14
			rts

;--- Rechter Mausklick auf DeskTop.
::10			ldx	#0
			b $2c

;--- Rechter Mausklick auf AppLink/Datei.
::11			ldx	#1
			b $2c

;--- Rechter Mausklick auf AppLink/Arbeitsplatz.
::12			ldx	#2
			b $2c

;--- Rechter Mausklick auf AppLink/Drucker.
::13			ldx	#3
			b $2c

;--- Rechter Mausklick auf AppLink/Laufwerk.
::14			ldx	#4
			b $2c

;--- Rechter Mausklick auf AppLink/Verzeichnis.
::15			ldx	#10
			jmp	PM_OPEN_MENU

;*** PopUp-Fenster für Datei-Eigenschaften.
:PM_FILE		jsr	WM_TEST_ENTRY
			bcc	:1

;--- Rechter Mausklick auf Datei-Icon.
			stx	r0L
			sty	r0H
			LoadW	r1,32
			ldx	#r0L
			ldy	#r1L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r0
			MoveW	r0,vecDirEntry

			ldx	#6
			jmp	PM_OPEN_MENU

;--- Rechter Mausklick auf Fenster-Hintergrund.
::1			ldx	WM_WCODE

			ldy	#" "
			lda	WMODE_VDEL,x
			beq	:2
			ldy	#"*"
::2			sty	PopUpText_105 +3

			ldy	#" "
			lda	WMODE_VSIZE,x
			bpl	:2a
			ldy	#"*"
::2a			sty	PopUpText_114 +3

			ldy	#" "
			lda	WMODE_VICON,x
			bpl	:2b
			ldy	#"*"
::2b			sty	PopUpText_115 +3

			ldy	#" "
			lda	WMODE_VINFO,x
			bpl	:2c
			ldy	#"*"
::2c			sty	PopUpText_116 +3

			ldy	#" "
			lda	WMODE_FMOVE,x
			bpl	:2d
			ldy	#"*"
::2d			sty	PopUpText_117 +3

			ldy	WMODE_DRIVE,x
			lda	RealDrvMode -8,y
			and	#SET_MODE_SUBDIR
			beq	:3

			ldx	#9
			b $2c
::3			ldx	#7
			jmp	PM_OPEN_MENU

;*** Rechter Mausklick auf Arbeitsplatz.
:PM_MYCOMP		jsr	WM_TEST_ENTRY
			bcs	:1
::0			rts

::1			stx	MyComputerEntry +0
			sty	MyComputerEntry +1

			cpx	#$04			;Rechtsklick auf Drucker?
			beq	:2			; => Ja, weiter...
			cpx	#$05			;Rechtsklick auf Eingabegerät?
			beq	:3			; => Ja, weiter...
			bcs	:0			; => Rechtsklick ungültig.

			lda	driveType,x		;Existiert Laufwerk?
			beq	:0			; => Rechtsklick ungültig.

			ldx	#8			;PopUp-Menü für Laufwerk.
			b $2c
::2			ldx	#5			;PopUp-Menü für Drucker.
			b $2c
::3			ldx	#13			;PopUp-Menü für Eingabegerät.
			jmp	PM_OPEN_MENU

