﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* GeoDesk initialisieren.
;* GeoDesk starten.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopMac.MP3"
			t "TopSym.GD"
			t "-SYS_APPLINK"
			t "s.mod.#101.ext"
endif

			n "mod.#102.obj"
			f DATA
			o VLIR_BASE
			p MainInit
			c "GeoDesk64   V0.1"
			a "Markus Kanet"

:VlirJumpTable		jmp	MainInit
			jmp	MainReBoot

:vecDirEntry		w $0000
:MyComputerEntry	w $0000

;*** Dialogbox: Datei wählen.
:Dlg_SlctPart		b $81
			b DBGETFILES!DBSELECTPART ,$00,$00
			b CANCEL                  ,$00,$00
			b OPEN                    ,$00,$00
			b NULL

;*** Systemroutinen.
			t "-102_MainDesktop"
			t "-102_TaskBar"
			t "-102_StartFile"
			t "-102_AppLink"
			t "-102_PopUpGEOS"
			t "-102_PopUpMenu"
			t "-102_PopUpFunc"
			t "-102_PopUpData"
			t "-102_DoFileEntry"
			t "-102_DrawIcon"
			t "-102_DragNDrop"
			t "-102_WinOpenComp"
			t "-102_WinOpenDrv"
			t "-102_WinMseCheck"
			t "-102_WinDataTab"

;*** Speicherverwaltung.
			t "-SYS_RAM"

;******************************************************************************
			g BASE_DIR_DATA
;******************************************************************************

