﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Erweiterte Systemvariablen.
.GD_VAR_START
.reloadFiles		b $00				;$FF = Verzeichnis von Disk laden.
							;Übergeht Inhalte im Cache.
.colorMode		b $ff				;$FF = S/W-Modus.
.colorModeDebug		b $00				;$FF = Debug-Modus, Cache=Farbig.
.colorModeIcons		b $00				;$20 = Farbe für Icons aus Cache.
							;$00 = S/W-Datei-Icons.
.appLinkLocked		b $00				;$00 = Drag'n'Drop für AppLinks.
							;$FF = AppLinks gesperrt.
.Flag_ViewALTitle	b $00				;$00 = Keine Titel anzeigen.
							;$FF = Titel anzeigen
.Flag_CacheModeOn	b $ff				;$00 = DiskCache nicht aktiv.
							;$FF = DiskCache aktiv.
.Flag_BackScreen	b $ff				;$00 = Kein Hintergrundbild.
							;$FF = Hintergrundbild verwenden.

.GD_VAR_END
