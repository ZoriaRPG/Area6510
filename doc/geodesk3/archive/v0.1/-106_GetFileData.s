﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateien für Laufwerksfenster einlesen.
;--- Enthält aktuelles Fenster bereits Daten?
:xGET_ALL_FILES		jsr	OpenWinDrive		;Laufwerk öffnen.
			txa
			bne	:jmp_load_disk

			ldx	curDrive
			lda	driveType -8,x		;RAM-Laufwerk?
			bmi	:jmp_load_disk		; => Ja, Dateien von Disk lesen.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			bne	:0001			; => Dateien im Cache vorhanden...
::jmp_load_disk		jmp	:load_from_disk		; => Keine Dateien => Disk öffnen.

;--- Prüfen ob Dateien aus Cache eingelesen werden können.
;Wird :reloadFiles auf $FF gesetzt werden die Daten immer
;von Diskk eingelesen. Evtl. Sinnvoll nach einem Reboot in den
;Desktop wenn andere Programme Daten auf Disk geändert haben.
;Aktuell wird die BAM geprüft, das funktioniert auf NativeMode
;aber nur bedingt (es werden nicht alle BAM-Sektoren getestet).
::0001			;lda	reloadFiles		;Dateien von Disk lesen?
			;bne	:jmp_load_disk		; => Ja, weiter...

			bit	Flag_CacheModeOn	;Cache-Modus aktiv?
			bpl	:jmp_load_disk		; => Nein, weiter...
			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%11000000		;RAM-Cache für Laufwerk aktiv?
			beq	:jmp_load_disk		; => Nein, Disk öffnen.

::verify1541		jsr	SetBAMCache1Sek		;Zeiger auf ersten BAM Sektor.
			bne	:verify1571
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:jmp_load_disk		; => Cache veraltet, Disk öffnen.

::verify1571		jsr	SetBAMCache2Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verify1581		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:jmp_load_disk		; => Cache veraltet, Disk öffnen.

::verify1581		jsr	SetBAMCache3Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verifyNative		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:jmp_load_disk		; => Cache veraltet, Disk öffnen.

::verifyNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y
			bne	:load_from_cache

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:jmp_load_disk		; => Ja, Disk öffnen.

			jsr	SetBAMCache4Sek		;BAM-CRC erstellen.
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:jmp_load_disk		; => Cache veraltet, Disk öffnen.

;--- Verzeichnis nicht geändert,
;    Dateien aus Cache laden.
::load_from_cache	bit	colorModeDebug		;Debug-Modus aktiv?
			bpl	:no_debug_cache		; => Nein, weiter...
			lda	#$20			;Farbe für Icons aus Cache setzen.
			sta	colorModeIcons

::no_debug_cache	jsr	i_FillRam		;Verzeichnis-Speicher löschen.
			w	(OS_VARS - BASE_DIR_DATA)
			w	BASE_DIR_DATA
			b	$00

			LoadW	r0,BASE_DIR_DATA	;Zeiger auf Verzeichnis-Daten
			LoadW	r1,$0300		;im Cache setzen.
			LoadW	r2,32

			ldx	WM_WCODE		;DACC-Speicherbank für aktuelles
			lda	ramBaseWin0,x		;Fenster setzen.
			sta	r3L

			lda	WM_DATA_MAXENTRY +0
			sta	r12L
			lda	WM_DATA_MAXENTRY +1
			sta	r12H

;--- Datei-Einträge aus Cache einlesen.
::loop			lda	r12L			;Weitere Dateien vorhanden?
			ora	r12H
			beq	:end			; => Nein, Ende...
			jsr	FetchRAM

			AddVBW	32,r0			;Zeiger RAM-Verzeichnis-Eintrag.
							;32Byte = Datei-Info.
			AddVBW	96,r1			;Zeiger DACC-Verzeichnis-Eintrag.
							;96Byte = Datei- und Icon-Info.

			lda	r12L			;Dateizähler runterzählen.
			bne	:next
			dec	r12H
::next			dec	r12L
			jmp	:loop			;Nächste Datei einlesen.

;--- Dateien aus Cache gelesen, Ende.
::end			jmp	WM_SAVE_WIN_DATA	;Ende, Fensterdaten sichern.

;--- Verzeichnis geändert, Dateien von
;    Disk einlesen.
::load_from_disk	bit	colorModeDebug		;Debug-Modus aktiv?
			bpl	:no_debug_disk		; => Nein, weiter...
			lda	#$00			;Farbe für Icons von Disk setzen.
			sta	colorModeIcons

::no_debug_disk		bit	Flag_CacheModeOn	;Cache-Modus aktiv?
			bpl	:0			; => Nein, weiter...

			ldx	curDrive
			lda	driveType -8,x		;RAM-Laufwerk?
			bmi	:0			; => Ja, Kein Cache...

			ldx	WM_WCODE
			lda	ramBaseWin0,x		;Cache-Speicherbank einlesen.
			beq	:0			; => Kein Cache aktiv, weiter...
			jsr	:updateBAMcache		;BAM-CRC berechnen.

::0			lda	#$00			;Dateizähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1

			jsr	i_FillRam		;Verzeichnis-Speicher löschen.
			w	(OS_VARS - BASE_DIR_DATA)
			w	BASE_DIR_DATA
			b	$00

			LoadW	r10,BASE_DIR_DATA +2
			LoadW	r11,$0300
			LoadB	r12L,240

			jsr	Get1stDirEntry		;Ersten Verzeichnis-Eintrag lesen.
			txa				;Fehler?
			bne	:50a			; => Ja, Abbruch...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			bne	:51			; => Ja, Ende...
::50a			jmp	:54

::51			ldy	#$00
			lda	(r5L),y			;Dateityp einlesen.
			bne	:51a			; => Echte Datei, weiter...
			iny
			lda	(r5L),y			;Track gesetzt?
			beq	:51c			; => Nein, kein Datei-Eintrag.

			ldx	WM_WCODE
			lda	WMODE_VDEL,x		;Gelöschte Dateien anzeigen?
			bne	:51d			; => Ja, weiter...
::51c			jmp	:53			; => Nein, nächster Eintrag.

::51a			and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;CMD-Verzeichnis?
			beq	:51b			; => Ja, weiter...

::51d			ldx	WM_WCODE
			lda	WMODE_FILTER,x		;Filter aktiv?
			bpl	:51b			; => Nein, weiter...
			ldy	#$16
			lda	(r5L),y			;GEOS-Dateityp einlesen.
			ora	#%10000000		;Bit#7 für Vergleich setzen.
			cmp	WMODE_FILTER,x		;Datei für Filter gültig?
			bne	:51c			; => Nein, nächster Eintrag.

::51b			dec	r12L			;Zähler für Dateieinträge.

			ldy	#$1d
::52			lda	(r5L),y			;Verzeichnis-Eintrag kopieren.w
			sta	(r10L),y
			dey
			bpl	:52

			bit	Flag_CacheModeOn	;Cache-Modus aktiv?
			bpl	:52a			; => Nein, weiter...

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%10000000		;Verzeichnis-Eintrag in Cache?
			beq	:52a			; => Nein, weiter...

			lda	r10L			;Verzeichnis-Eintrag im
			sec				;Cache speichern.
			sbc	#$02
			sta	r0L
			lda	r10H
			sbc	#$00
			sta	r0H
			MoveW	r11,r1
			LoadW	r2,32
			jsr	SetDiskRamBase
			jsr	StashRAM
			AddVBW	32,r11

::52a			bit	Flag_CacheModeOn	;Cache-Modus aktiv?
			bpl	:52b			; => Nein, weiter...
			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%01000000		;Icon-Eintrag in Cache?
			beq	:52b			; => Nein, weiter...

			ldy	#$00
			lda	(r5L),y			;Datei "gelöscht"?
			bne	:42c			; => Nein, weiter...
			LoadW	r0,Icon_Deleted		;Icon "Gelöscht" in Cache...
			jmp	:42b

::42c			and	#%00001111
			cmp	#$06			;Dateityp = "Verzeichnis"?
			bne	:42d			; => Nein, weiter...
			LoadW	r0,Icon_Map		;Icon "Verzeichnis" in Cache...
			jmp	:42b

::42d			ldy	#$13			;Zeiger auf FileHeader einlesen.
			lda	(r5L),y
			beq	:42e			; => Kein, FileHeader...
			sta	r1L
			iny
			lda	(r5L),y
			sta	r1H
			ora	r1L			;FileHeader gesetzt?
			bne	:42a			; => Ja, weiter...
::42e			LoadW	r0,Icon_CBM		;Icon "CBM" in Cache...
			jmp	:42b

::42a			LoadW	r4,fileHeader
			jsr	GetBlock		;GEOS-FileHeader einlesen.
			txa				;Fehler?
			bne	:54			; => Ja, Abbruch...
			LoadW	r0,fileHeader+4		;Zeiger auf Icon-Daten.

::42b			MoveW	r11,r1			;Datei-Icon in Cache speichern.
			LoadW	r2,64
			jsr	SetDiskRamBase
			jsr	StashRAM
			AddVBW	64,r11			;Zähler anpassen.

::52b			AddVBW	32,r10			;Zeiger auf nächsten Datei-Speicher.

			inc	WM_DATA_MAXENTRY +0
			bne	:53
			inc	WM_DATA_MAXENTRY +1

::53			jsr	GetNxtDirEntry		;Nächsten Eintrag lesen.
			txa				;Fehler?
			bne	:54			; => Ja, Abbruch...

			lda	r12L			;Fenster "voll"?
			beq	:54			; => Ja, Ende...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:54			; => Ja, Ende...
			jmp	:51			; => Nächsten Eintrag einlesen...

::54			jmp	WM_SAVE_WIN_DATA	; => Ende...

;--- Cache-Modus: Aktuelle BAM sichern.
::updateBAMcache
::upd1541		jsr	SetBAMCache1Sek		;Ersten BAM-Sektor sichern?
			bne	:upd1571		; => Nein, weiter...
			jsr	StashRAM

::upd1571		jsr	SetBAMCache2Sek		;Zweiten BAM-Sektor sichern?
			bne	:upd1581		; => Nein, weiter...
			jsr	StashRAM

::upd1581		jsr	SetBAMCache3Sek		;Dritten BAM-Sektor sichern?
			bne	:updNative		; => Nein, weiter...
			jsr	StashRAM

::updNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y	;Native-BAM sichern?
			bne	:exit1			; => Nein, Ende...

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:exit1			; => Ja, Abbruch...

			jsr	SetBAMCache4Sek		;Native-BAM sichern?.
			jmp	StashRAM
::exit1			rts

;*** CRC für Native-Mode BAM erstellen.
:CreateNM_CRC		jsr	i_FillRam		;CRC-Puffer löschen.
			w	256
			w	diskBlkBuf
			b	$00

			jsr	EnterTurbo		;I/O und GEOS-Turbo aktivieren.
			jsr	InitForIO

			ldx	#$02			;Zeiger auf ersten BAM-Sektor.
::1			txa
			pha
			jsr	GetBAMBlock		;BAM-Sektor einlesen.
			pla
			cpx	#NO_ERROR		;Fehler?
			bne	:exit1			; => Ja, Abbruch...

			ldy	#<dir2Head		;Zeiger auf BAM-Sektor.
			sty	r0L
			ldy	#>dir2Head
			sty	r0H
			stx	r1L
			inx
			stx	r1H
			pha
			jsr	CRC			;CRC für BAM-Sektor berechnen.w
			pla
			tax
			asl
			tay
			lda	r2L			;CRC in Pugffer kopieren.
			sta	diskBlkBuf +0,y
			lda	r2H
			sta	diskBlkBuf +1,y

			cpx	#$02			;Erster BAM-Sektor?
			bne	:2			; => Nein, weiter...
			lda	dir2Head +8		;Anzahl Tracks einlesen und
			sta	BAMTrackMaxNM		;zwischenspeichern.

::2			inx
			lda	BAMTrackCntNM-2,x
			cmp	BAMTrackMaxNM		;Max. Anzahl Tracks erreicht?
			bcs	:3			; => Ja, Ende...
			cpx	#33			;Max. Anzahl BAM-Sektoren erreicht?
			bcc	:1			; => Nein, weiter...

;--- BAM-CRC erstellt, Ende.
::3			ldx	#NO_ERROR		;Ende...
::exit1			jmp	DoneWithIO

;*** Zeiger auf Cache-Bereich für BAM-Sektoren.
;--- 1541/1571/1581/Native.
:SetBAMCache1Sek	ldx	curDrive
			lda	driveType -8,x
			and	#%00000111		;Laufwerksmodus isolieren.
			sta	driveTypeCopy

			tay
;			ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo1,y
			bne	SetBAMCacheExit
			lda	#<curDirHead		;Zeiger auf ersten BAM-Sektor.
			ldx	#>curDirHead
			ldy	#$00
:SetBAMCacheInit	sta	r0L
			stx	r0H
			ldx	#$00			;XReg ist bereits $00.
			stx	r1L
			sty	r1H
			stx	r2L
			inx
			stx	r2H
			jsr	SetDiskRamBase
			ldx	#$00
:SetBAMCacheExit	rts

;--- 1571/1581.
:SetBAMCache2Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo2,y
			bne	SetBAMCacheExit
			lda	#<dir2Head		;1571, 1581 oder Native.
			ldx	#>dir2Head		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$01
			bne	SetBAMCacheInit

;--- 1581.
:SetBAMCache3Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo3,y
			bne	SetBAMCacheExit

			cpy	#DrvNative		;NativeMode ?
			bne	:1			; => Nein, Abbruch...
			lda	#<DISK_BASE		; => Native, Variablen testen.
			ldx	#>DISK_BASE
			bne	:2

::1			lda	#<dir3Head		; => 1581, dir3Head testen.
			ldx	#>dir3Head
::2			ldy	#$02
			bne	SetBAMCacheInit

;--- Native.
:SetBAMCache4Sek	lda	#<diskBlkBuf		;1571, 1581 oder Native.
			ldx	#>diskBlkBuf		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$01
			bne	SetBAMCacheInit

:driveTypeCopy		b $00

;*** Tabelle mit BAM-Sektor-info.
;$00 = BAM-Sektor vorhanden.
;$FF = BAM-Sektor nicht vorhanden.
;Type: NoDRV,1541,1571,1581,
;      Native,NoDRV,NoDRV,NoDRV
:BAMCopySekInfo1	b $ff,$00,$00,$00,$00,$ff,$ff,$ff
:BAMCopySekInfo2	b $ff,$ff,$00,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo3	b $ff,$ff,$ff,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo4	b $ff,$ff,$ff,$ff,$00,$ff,$ff,$ff

;*** Datentabelle mit Max. Track je BAM-Sektor.
:BAMTrackCntNM		b $01,$08,$10,$18,$20,$28,$30,$38
			b $40,$48,$50,$58,$60,$68,$70,$78
			b $80,$88,$90,$98,$a0,$a8,$b0,$b8
			b $c0,$c8,$d0,$d8,$e0,$e8,$f0,$f8

;*** Puffer für Max. Tracks/NativeMode.
:BAMTrackMaxNM		b $00

;*** Zeiger auf 64K-Speicherbank setzen.
:SetDiskRamBase		ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			rts
