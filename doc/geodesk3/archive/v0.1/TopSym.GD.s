﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Anzahl VLIR-Module ohne BOOT.
;Die Anzahl der Module wird dann in
;den DACC-Speicher geladen.
:GD_VLIR_COUNT		= 7

;*** VLIR-Modulnummern.
;VLIR_BOOT		= 0
:VLIR_WM		= 0
:VLIR_DESKTOP		= 1
:VLIR_PARTITION		= 2
:VLIR_APPLINK		= 3
:VLIR_FILE_OPEN		= 4
:VLIR_LOAD_FILES	= 5
:VLIR_SAVE_CONFIG	= 6

;*** Startadresse Boot-Loader.
:VLIR_BOOT_START	= $7000

;*** Startadresse Verzeichnis-Daten.
:BASE_DIR_DATA		= $6400
