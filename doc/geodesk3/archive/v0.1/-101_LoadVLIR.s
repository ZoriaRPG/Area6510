﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** VLIR-Module nachladen.
;Hierbei wird das Modul geladen und mittels JMP-Befehl die
;entsprechende Routine angesprungen.
;Diese Routinen sollten nur mittels JMP-Befehl angesprungen werden da
;kein Rücksprung zur vorherigen Programm-Adresse erfolgt!
.MNU_INIT		lda	#$00			;Oberfläche initialisieren.
			b $2c
.MNU_REBOOT		lda	#$03			;Rückkehr aus Unterprogramm.
			ldx	#VLIR_DESKTOP
			bne	MNU_OPEN_VLIR

.MNU_SWAP_PART		lda	#$00			;Partitionen wechseln.
			ldx	#VLIR_PARTITION
			bne	MNU_OPEN_VLIR

.MNU_OPEN_FILE		lda	#$00
			b $2c
.MNU_OPEN_EDITOR	lda	#$03
			b $2c
.MNU_OPEN_APPL		lda	#$12
			b $2c
.MNU_OPEN_AUTO		lda	#$15
			b $2c
.MNU_OPEN_DOCS		lda	#$18
			b $2c
.MNU_OPEN_WRITE		lda	#$1b
			b $2c
.MNU_OPEN_PAINT		lda	#$1e
			b $2c
.MNU_OPEN_BACKSCR	lda	#$21
			b $2c
.MNU_OPEN_EXITG		lda	#$24
			b $2c
.MNU_OPEN_EXIT64	lda	#$27
			b $2c
.MNU_OPEN_EXITB		lda	#$2a
			ldx	#VLIR_FILE_OPEN
			bne	MNU_OPEN_VLIR

.MNU_SAVE_CONFIG	lda	#$00			;Konfiguration speichern.
			ldx	#VLIR_SAVE_CONFIG
;			bne	MNU_OPEN_VLIR

.MNU_OPEN_VLIR		clc
			adc	#<VLIR_BASE
			sta	:JmpAdr +1
			lda	#$00
			adc	#>VLIR_BASE
			sta	:JmpAdr +2

			stx	:VlirMod +1

			ldx	:VlirMod +1 		;Hauptmenü nachladen?
			dex
			beq	:VlirMod		; => Ja, weiter...

			lda	#$00			;Systemvektoren zurücksetzen.
			sta	appMain +0		;Andere Programm-Module können
			sta	appMain +1		;daher Maus- und Tastaturabfragen
			sta	RecoverVector +0	;aus dem Hauptmodul nicht nutzen!
			sta	RecoverVector +1
			sta	mouseFaultVec +0
			sta	mouseFaultVec +1
			sta	otherPressVec +0
			sta	otherPressVec +1

;--- Programm-Modul nachladen.
;    Im AKKU befindet sich hier die
;    Nummer des Programm-Moduls.
::VlirMod		lda	#$ff
			jsr	LoadVLIRModule		;Neues Modul laden.

::JmpAdr		jmp	$ffff			;Unterprogrammm starten.

;*** VLIR-Module nachladen.
;Hierbei wird das Modul geladen und mittels JSR-Befehl die
;entsprechende Routine angesprungen. Anschließend wird das
;vorherige VLIR-Modul wieder eingelesen und es erfolgt die
;Rückkehr zur vorherigen Programm-Adresse.
.LNK_LOAD_DATA		lda	#$00			;AppLink-Daten laden.
			b $2c
.LNK_SAVE_DATA		lda	#$03			;AppLink-Daten speichern.
			ldx	#VLIR_APPLINK
			bne	SWAP_MODUL

.SLCT_PRINTER		lda	#$06
			b $2c
.SLCT_INPUT		lda	#$09
			b $2c
.OPEN_PRNT_ERR		lda	#$0c
			b $2c
.OPEN_PRNT_OK		lda	#$0f
			ldx	#VLIR_FILE_OPEN
			bne	SWAP_MODUL

.GET_ALL_FILES		lda	#$00
			ldx	#VLIR_LOAD_FILES
			;bne	SWAP_MODUL

:SWAP_MODUL		cpx	curVLIRModule
			bne	:0
			clc
			adc	#<VLIR_BASE
			sta	:CallAdr +1
			lda	#$00
			adc	#>VLIR_BASE
			sta	:CallAdr +2
::CallAdr		jmp	$ffff

::0			sta	:JmpAdr
			stx	:VlirMod

			PushW	appMain
			PushW	RecoverVector
			PushW	mouseFaultVec
			PushW	otherPressVec

			ldx	#$1f
::save1			lda	r0L,x
			pha
			dex
			bpl	:save1

			jsr	UPDATE_CURMOD		;Aktuelles Programm-Modul speichern.

			ldx	#$00
::load1			pla
			sta	r0L,x
			inx
			cpx	#$20
			bcc	:load1

			lda	curVLIRModule		;Aktuelles Modul merken.
			sta	:curVlirMod

			lda	:JmpAdr
			ldx	:VlirMod
			jsr	MNU_OPEN_VLIR		;Neues Modul nachladen/starten.
;			stx	:XReg +1		;XReg kann Fehlerstatus enthalten!

			lda	:curVlirMod		;Zurück zum vorherigen Modul.
			jsr	LoadVLIRModule

			PopW	otherPressVec
			PopW	mouseFaultVec
			PopW	RecoverVector
			PopW	appMain

::XReg			;ldx	#$00			;Fehlerstatus zurücksetzen.
			rts

::JmpAdr		b $00
::VlirMod		b $00
::curVlirMod		b $00

;*** Programm-Modul aus erweitertem
;    Speicher nachladen.
:LoadVLIRModule		sta	curVLIRModule		;Modul-Nummer speichern.

			txa				;Evtl. Fehlerregister sichern.
			pha

			ldx	#$1f
::save			lda	r0L,x
			pha
			dex
			bpl	:save

			lda	curVLIRModule		;Modul-Nummer speichern.
			jsr	SetVlirVecRAM		;Zeiger auf Modul im Speicher.
			jsr	FetchRAM		;Modul einlesen.

			ldx	#$00
::load			pla
			sta	r0L,x
			inx
			cpx	#$20
			bcc	:load

			pla
			tax				;Fehlerregister zurücksetzen.

			rts

;*** Zeiger auf Speicherbereich setzen.
;    Übergabe: AKKU = Modul.
;                     $00 = Variablen.
:SetVlirVecRAM		asl
			asl
			tax
			lda	#<APP_RAM
			ldy	#>APP_RAM
			cpx	#$00
			beq	:1
			lda	#<VLIR_BASE
			ldy	#>VLIR_BASE
::1			sta	r0L
			sty	r0H
			lda	GD_DACC_ADDR +0,x
			sta	r1L
			lda	GD_DACC_ADDR +1,x
			sta	r1H
			lda	GD_DACC_ADDR +2,x
			sta	r2L
			lda	GD_DACC_ADDR +3,x
			sta	r2H
			lda	GD_RAM_GDESK
			sta	r3L
			rts
