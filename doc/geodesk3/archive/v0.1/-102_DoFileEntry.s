﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:SortDetails		b $00,$01,$02,$03,$04,$05

:GD_PRINT_ENTRY		ldx	WM_WCODE
			lda	WMODE_VICON,x
			bne	:11

			lda	r1L
			clc
			adc	#$03
			cmp	r2L
			bcc	:1
			ldx	#$00
			rts

::1			sta	r1L

			;lda	r0L
			;ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	ReadFName

			ldy	#$18
			lda	(r15L),y
			cmp	#24
			bcc	:1y
			lda	#23
::1y			tax
			bit	colorMode
			bmi	:1yy
			lda	V402y0 +0,x		;Icon-Farb einlesen.
			bne	:1yyy
::1yy			lda	colorModeIcons
::1yyy			ora	C_WinBack
			sta	r3L

			LoadB	r2L,$03
			LoadB	r2H,$15
;			LoadB	r3L,$01
			LoadB	r3H,$04
			LoadW	r4 ,FNameBuf
			jsr	GD_FICON_NAME

			lda	#$ff
			jmp	:51

::11			lda	r1H
			clc
			adc	#$06
			sta	r1H

			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:12

			lda	r1L
			clc
			adc	r3L
			cmp	r2L
			bcc	:13
			beq	:13
			ldx	#$00
			rts

::13			;lda	r0L
			;ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	ReadFName

			lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			LoadW	r0,FNameBuf
			jsr	PutString
			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$ff
			jmp	:51

::12			lda	r2L
			pha

			;lda	r0L
			;ldx	r0H
			jsr	GET_NEXT_FICON
			jsr	ReadFName

			lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			PushW	r11
			LoadW	r0,FNameBuf
			jsr	PutString
			PopW	r11

			jsr	DrawDetails

			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			pla
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$7f
::51			pha

			ldy	#$00
			lda	(r15L),y
			bne	:52
			iny
			lda	(r15L),y
			beq	:53
::52			jsr	WM_CONVERT_CARDS
			CmpW	r4,rightMargin
			bcc	:52a
			MoveW	rightMargin,r4

::52a			CmpB	r2H,windowBottom
			bcc	:52b
			MoveB	windowBottom,r2H

::52b			jsr	InvertRectangle

::53			pla
			tax
			rts

;*** Zeiger auf Eintrag in :r0.
:GET_NEXT_FICON		lda	r0L
			sta	r13L
			sta	r15L
			lda	r0H
			sta	r13H
			sta	r15H

			PushB	r1L
			PushB	r1H

			jsr	GD_VEC2ENTRY		;Zeiger auf Cache setzen.
			bcc	:0			; => Kein Cache aktiv, weiter...

			LoadW	r0,:dataBuf
			MoveW	r15,r1
			MoveW	r14,r2
			ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			jsr	FetchRAM

			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			cmp	#$c0
			bne	:z1
			LoadW	r0 ,:dataBuf +32
			LoadW	r15,:dataBuf
			PopB	r1H
			PopB	r1L
			rts

::z1			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%10000000
			beq	:0
			LoadW	r15,:dataBuf
			jmp	:1

::0			MoveW	r13,r15
			LoadW	r14,32
			ldx	#r15L
			ldy	#r14L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r15

::1			bit	Flag_CacheModeOn
			bpl	:z2
			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%01000000
			beq	:z2
			LoadW	r0,:dataBuf
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::z2			ldy	#$02
			lda	(r15L),y
			bne	:2
			LoadW	r0,Icon_Deleted
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::2			and	#%00001111
			cmp	#$06
			bne	:2a
			LoadW	r0,Icon_Map
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::2a			ldy	#$15
			lda	(r15L),y
			beq	:91
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader
			jsr	GetBlock
			LoadW	r0,fileHeader +4
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::91			LoadW	r0,Icon_CBM
			PopB	r1H
			PopB	r1L
			ldx	#$00
			rts

::dataBuf		s 96

;*** Dateiname kopieren.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
;    Rückgabe: FNameBuf = Dateiname.
:ReadFName		LoadW	r14,FNameBuf
			ldx	#r15L
			ldy	#r14L
			jmp	SysCopyFName

:FNameBuf		s 17

:DrawDetails		AddVBW	$40,r11

			lda	#$00
::1			pha
			tax
			ldy	SortDetails,x
			dey
			bne	:2
			jsr	Detail_Size
::2			dey
			bne	:3
			jsr	Detail_Date
::3			dey
			bne	:4
			jsr	Detail_Time
::4			dey
			bne	:5
			jsr	Detail_GType
::5			dey
			bne	:6
			jsr	Detail_CType

::6			AddVBW	6,r11
			pla
			clc
			adc	#$01
			cmp	#$06
			bcc	:1
			rts

:Detail_Num		lda	r11H
			pha
			lda	r11L
			pha

			tya
			pha

			txa
			jsr	:201
			pha
			txa
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar

			pla
			jsr	SmallPutChar

			pla
			clc
			adc	#11
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

::201			ldx	#$30
::202			cmp	#10
			bcc	:203
			sbc	#10
			inx
			bne	:202
::203			adc	#$30
			rts

:Detail_Size		ldy	#$1e
			lda	(r15L),y
			sta	r0L
			iny
			lda	(r15L),y
			sta	r0H

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:1
			lda	r0L
			pha
			ldx	#r0L
			ldy	#$02
			jsr	DShiftRight
			pla
			and	#%00000011
			beq	:1

			inc	r0L
			bne	:1
			inc	r0H

::1			lda	#$20 ! SET_RIGHTJUST ! SET_SUPRESS
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:2
			lda	#"K"
			jsr	SmallPutChar

::2			rts

:Detail_Date		ldy	#$1b
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$1a
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$19
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

:Detail_Time		ldy	#$1c
			lda	(r15L),y
			tax
			ldy	#":"
			jsr	Detail_Num

			ldy	#$1d
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

:Detail_GType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$18
			lda	(r15L),y
			cmp	#24
			bcc	:1
			lda	#23
::1			asl
			tax
			lda	V402q0 +0,x
			sta	r0L
			lda	V402q0 +1,x
			sta	r0H
			jsr	PutString

			pla
			clc
			adc	#$50
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

:Detail_CType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$02
			lda	(r15L),y
			pha
			and	#%00000111
			asl
			asl
			tay
			lda	V402t0+2,y
			pha
			lda	V402t0+1,y
			pha
			lda	V402t0+0,y
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla

			pha
			bmi	:3
			lda	#"*"
			jsr	SmallPutChar
::3			pla
			and	#%01000000
			beq	:4
			lda	#"<"
			jsr	SmallPutChar

::4			pla
			clc
			adc	#$18
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** Texte für Verzeichnis.
:V402q0			w V402r0 ,V402r1 ,V402r2 ,V402r3 ,V402r4
			w V402r5 ,V402r6 ,V402r7 ,V402r8 ,V402r9
			w V402r10,V402r11,V402r12,V402r13,V402r14
			w V402r15,V402r99,V402r17,V402r99,V402r99
			w V402r99,V402r21,V402r22,V402r99

:V402q1			w V402s0 ,V402s1 ,V402s2 ,V402s3

:V402r0			b "Nicht GEOS",NULL
:V402r1			b "BASIC",NULL
:V402r2			b "Assembler",NULL
:V402r3			b "Datenfile",NULL
:V402r4			b "System-Datei",NULL
:V402r5			b "DeskAccessory",NULL
:V402r6			b "Anwendung",NULL
:V402r7			b "Dokument",NULL
:V402r8			b "Zeichensatz",NULL
:V402r9			b "Druckertreiber",NULL
:V402r10		b "Eingabetreiber",NULL
:V402r11		b "Laufwerkstreiber",NULL
:V402r12		b "Startprogramm",NULL
:V402r13		b "Temporär",NULL
:V402r14		b "Selbstausführend",NULL
:V402r15		b "Eingabetreiber 128",NULL
:V402r17		b "gateWay-Dokument",NULL
:V402r21		b "geoShell-Kommando",NULL
:V402r22		b "geoFAX Druckertreiber",NULL
:V402r99		b "GEOS ???",NULL

:V402r105		b "< 1581 - Partition >",NULL
:V402r106		b "< Unterverzeichnis >",NULL

:V402s0			b "GEOS 40 Zeichen",NULL
:V402s1			b "GEOS 40 & 80 Zeichen",NULL
:V402s2			b "GEOS 64",NULL
:V402s3			b "GEOS 128, 80 Zeichen",NULL

:V402t0			b "DEL SEQ PRG USR REL CBM DIR ??? "
:V402t1			b "Sequentiell",NULL
:V402t2			b "GEOS-VLIR",NULL

:V402y0			b $00
:V402y1			b $00
:V402y2			b $30
:V402y3			b $30
:V402y4			b $20
:V402y5			b $60
:V402y6			b $60
:V402y7			b $30
:V402y8			b $70
:V402y9			b $50
:V402y10		b $50
:V402y11		b $20
:V402y12		b $20
:V402y13		b $00
:V402y14		b $60
:V402y15		b $50
:V402y17		b $60
:V402y21		b $60
:V402y22		b $50
:V402y99		b $70
