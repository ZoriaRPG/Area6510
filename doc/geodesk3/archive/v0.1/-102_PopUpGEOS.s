﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS-Hauptmenü.
:MAX_ENTRY_GEOS		= 4
:MENU_GEOS_Y0		= ((MIN_AREA_BAR_Y-1) - MAX_ENTRY_GEOS*14 -2) & $f8
:MENU_GEOS_Y1		=  (MIN_AREA_BAR_Y-1)
:MENU_GEOS_X0		= MIN_AREA_BAR_X
:MENU_GEOS_X1		= $005f
:MENU_GEOS_W		= (MENU_GEOS_X1 - MENU_GEOS_X0 +1)
:MENU_GEOS_H		= (MENU_GEOS_Y1 - MENU_GEOS_Y0 +1)

:MENU_DATA_GEOS		b MENU_GEOS_Y0
			b MENU_GEOS_Y1
			w MENU_GEOS_X0
			w MENU_GEOS_X1

			b MAX_ENTRY_GEOS!VERTICAL

			w GMenuText_00
			b DYN_SUB_MENU
			w OPEN_MENU_APPL

			w GMenuText_01
			b DYN_SUB_MENU
			w OPEN_MENU_DOCS

			w GMenuText_02
			b DYN_SUB_MENU
			w OPEN_MENU_SETUP

			w GMenuText_03
			b DYN_SUB_MENU
			w OPEN_MENU_EXIT

:GMenuText_00		b PLAINTEXT,BOLDON
			b "Programme >>",NULL
:GMenuText_01		b PLAINTEXT,BOLDON
			b "Dokumente >>",NULL
:GMenuText_02		b PLAINTEXT,BOLDON
			b "Einstellungen >>",NULL
:GMenuText_03		b PLAINTEXT,BOLDON
			b "Beenden >>",NULL

;*** GEOS/Programme
:MAX_ENTRY_APPL		= 2
:MENU_APPL_Y0		= ((MENU_GEOS_Y0 + 24 - MAX_ENTRY_APPL*14 -2) & $f8)  -0 -0
:MENU_APPL_Y1		=  (MENU_APPL_Y0 + MAX_ENTRY_APPL*16 )                -0 -1
:MENU_APPL_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_APPL_X1		= MENU_APPL_X0 + $006f
:MENU_APPL_W		= (MENU_APPL_X1 - MENU_APPL_X0 +1)
:MENU_APPL_H		= (MENU_APPL_Y1 - MENU_APPL_Y0 +1)

:MENU_DATA_APPL		b MENU_APPL_Y0
			b MENU_APPL_Y1
			w MENU_APPL_X0
			w MENU_APPL_X1

			b MAX_ENTRY_APPL!VERTICAL

			w GMenuText_30
			b MENU_ACTION
			w EXIT_MENU_APPL

			w GMenuText_31
			b MENU_ACTION
			w EXIT_MENU_APPL

:GMenuText_30		b PLAINTEXT,BOLDON
			b "Anwendungen",NULL
:GMenuText_31		b PLAINTEXT,BOLDON
			b "AutoStart",NULL

;*** GEOS/Dokumente
:MAX_ENTRY_DOCS		= 3
:MENU_DOCS_Y0		= ((MENU_GEOS_Y0 + 40 - MAX_ENTRY_DOCS*14 -2) & $f8)  -0 -0
:MENU_DOCS_Y1		=  (MENU_DOCS_Y0 + MAX_ENTRY_DOCS*16 )                -0 -1
:MENU_DOCS_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_DOCS_X1		= MENU_DOCS_X0 + $006f
:MENU_DOCS_W		= (MENU_DOCS_X1 - MENU_DOCS_X0 +1)
:MENU_DOCS_H		= (MENU_DOCS_Y1 - MENU_DOCS_Y0 +1)

:MENU_DATA_DOCS		b MENU_DOCS_Y0
			b MENU_DOCS_Y1
			w MENU_DOCS_X0
			w MENU_DOCS_X1

			b MAX_ENTRY_DOCS!VERTICAL

			w GMenuText_40
			b MENU_ACTION
			w EXIT_MENU_DOCS

			w GMenuText_41
			b MENU_ACTION
			w EXIT_MENU_DOCS

			w GMenuText_42
			b MENU_ACTION
			w EXIT_MENU_DOCS

:GMenuText_40		b PLAINTEXT,BOLDON
			b "Dokumente",NULL
:GMenuText_41		b PLAINTEXT,BOLDON
			b "GeoWrite",NULL
:GMenuText_42		b PLAINTEXT,BOLDON
			b "GeoPaint",NULL

;*** GEOS/Beenden
:MAX_ENTRY_EXIT		= 3
:MENU_EXIT_Y0		= (((MIN_AREA_BAR_Y-1) - MAX_ENTRY_EXIT*14 -2) & $f8) -8 -0
:MENU_EXIT_Y1		=  (MIN_AREA_BAR_Y-1)                                 -8 -0
:MENU_EXIT_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_EXIT_X1		= MENU_EXIT_X0 + $006f
:MENU_EXIT_W		= (MENU_EXIT_X1 - MENU_EXIT_X0 +1)
:MENU_EXIT_H		= (MENU_EXIT_Y1 - MENU_EXIT_Y0 +1)

:MENU_DATA_EXIT		b MENU_EXIT_Y0
			b MENU_EXIT_Y1
			w MENU_EXIT_X0
			w MENU_EXIT_X1

			b MAX_ENTRY_EXIT!VERTICAL

			w GMenuText_50
			b MENU_ACTION
			w EXIT_MENU_EXIT

			w GMenuText_51
			b MENU_ACTION
			w EXIT_MENU_EXIT

			w GMenuText_52
			b MENU_ACTION
			w EXIT_MENU_EXIT

:GMenuText_50		b PLAINTEXT,BOLDON
			b "Zurück zu GEOS",NULL
:GMenuText_51		b PLAINTEXT,BOLDON
			b "Basic starten",NULL
:GMenuText_52		b PLAINTEXT,BOLDON
			b "Programm starten",NULL

;*** GEOS/Einstellungen
:MAX_ENTRY_SETUP	= 7
:MENU_SETUP_Y0		= (((MIN_AREA_BAR_Y-1) - MAX_ENTRY_SETUP*14 -2) & $f8) -8 -0
:MENU_SETUP_Y1		=  (MIN_AREA_BAR_Y-1)                                  -8 -0
:MENU_SETUP_X0		= MENU_GEOS_X1  + 1 - (MENU_GEOS_W/2)
:MENU_SETUP_X1		= MENU_SETUP_X0 + $008f
:MENU_SETUP_W		= (MENU_SETUP_X1 - MENU_SETUP_X0 +1)
:MENU_SETUP_H		= (MENU_SETUP_Y1 - MENU_SETUP_Y0 +1)

:MENU_DATA_SETUP	b MENU_SETUP_Y0
			b MENU_SETUP_Y1
			w MENU_SETUP_X0
			w MENU_SETUP_X1

			b MAX_ENTRY_SETUP!VERTICAL

			w GMenuText_10
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_11
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_12
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_16
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_13
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_14
			b MENU_ACTION
			w EXIT_MENU_SETUP

			w GMenuText_15
			b DYN_SUB_MENU
			w OPEN_MENU_CACHE

:GMenuText_10		b PLAINTEXT,BOLDON
			b "GEOS Editor",NULL
:GMenuText_11		b PLAINTEXT,BOLDON
			b "Drucker wechseln",NULL
:GMenuText_12		b PLAINTEXT,BOLDON
			b "Eingabegerät wechseln",NULL
:GMenuText_16		b PLAINTEXT,BOLDON
			b "Hintergrundbild wechseln",NULL
:GMenuText_13		b PLAINTEXT,BOLDON
			b "AppLinks speichern",NULL
:GMenuText_14		b PLAINTEXT,BOLDON
			b "Einstellungen speichern",NULL
:GMenuText_15		b PLAINTEXT,BOLDON
			b ">> Disk-Cache",NULL

;*** GEOS/Einstellungen/Cache
:MAX_ENTRY_CACHE	= 2
:MENU_CACHE_Y0		= (((MIN_AREA_BAR_Y-1) - MAX_ENTRY_CACHE*14 -2) & $f8) -16 -0
:MENU_CACHE_Y1		=  (MIN_AREA_BAR_Y-1)                                  -16 -0
:MENU_CACHE_X0		= MENU_SETUP_X1  + 1 - (MENU_SETUP_W/2)
:MENU_CACHE_X1		= MENU_CACHE_X0 + $006f
:MENU_CACHE_W		= (MENU_CACHE_X1 - MENU_CACHE_X0 +1)
:MENU_CACHE_H		= (MENU_CACHE_Y1 - MENU_CACHE_Y0 +1)

:MENU_DATA_CACHE	b MENU_CACHE_Y0
			b MENU_CACHE_Y1
			w MENU_CACHE_X0
			w MENU_CACHE_X1

			b MAX_ENTRY_CACHE!VERTICAL

			w GMenuText_20
			b MENU_ACTION
			w EXIT_MENU_CACHE

			w GMenuText_21
			b MENU_ACTION
			w EXIT_MENU_CACHE

:GMenuText_20		b PLAINTEXT,BOLDON
			b "( ) Cache-Modus",NULL
:GMenuText_21		b PLAINTEXT,BOLDON
			b "( ) Cache-Debug",NULL
