﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Verküpfungen für DeskTop
.LinkData		b $01

			b "Arbeitsplatz",0,0,0,0,0	;Name
			b "Arbeitsplatz",0,0,0,0,0	;Angezeigter Name
			b $80				;Typ: $00=Appl./$FF=Verzeichnis.
							;     $80=Arbeitsplatz/$FE=Drucker
			b $02,$01			;Pos
			b 1,1,1,1,1,1,1,1,1		;Farbe
			b $00				;Laufwerk
			b $00				;Laufwerkstyp
			b $00				;Partition
			b $00,$00			;SubDir
			b $00				;Selection

			s 24*LINK_DATA_BUFSIZE
.LinkDataEnd

;*** Desktop-Icons.
.Icons
;--- Arbeitsplatz-Icon.
.Icon_00		j
<MISSING_IMAGE_DATA>

;--- Speicher für AppLink-Icons.
.Icon_01		s 24*64
.IconsEnd
