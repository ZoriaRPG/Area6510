﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Mausklick auf Arbeitsplatz.
:MseClkMyComputer	jsr	WM_TEST_ENTRY
			bcc	:22

			stx	:ENTRY +0
			sty	:ENTRY +1

			cpx	#$04
			bcc	:20
			beq	:30
			cpx	#$05
			bne	:10
			jmp	AL_OPEN_INPUT
::10			rts

;--- Laufwerk verschieben.
::20			lda	driveType,x
			beq	:10

			jsr	WM_TEST_MOVE
			bcs	:21
			jmp	OpenDriveCurWin

::21			lda	:ENTRY
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk

			LoadW	r4,Icon_Drive +1
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:22
			tay
			bne	:22

			jsr	AL_SET_INIT
			ldx	#r3L
			jsr	GetPtrCurDkNm
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jsr	AL_SET_ICON
			jsr	AL_SET_TYPDV
			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS
			jsr	AL_SET_COL_DRV

			lda	curDirHead +32
			ldx	curDirHead +33
			jsr	AL_SET_SDIR
			jmp	AL_SET_END
::22			rts

;--- Drucker verschieben.
::30			jsr	WM_TEST_MOVE
			bcs	:31

			jsr	SLCT_PRINTER
			txa
			bne	:32
			jmp	UpdateMyComputer

::31			LoadW	r4,Icon_Printer +1
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:22
			tay
			bne	:22

			jsr	AL_SET_INIT
			jsr	AL_SET_ICON
			jsr	AL_SET_XYPOS
			jsr	AL_SET_COL_PRNT

;--- Drucker wählen.
;Das ist notwendig damit dem AppLink
;das Laufwerk mit dem Druckertreiber
;zugeordnet werden kann.
			jsr	SLCT_PRINTER
			txa
			bne	:32

			jsr	AL_SET_INIT
			LoadW	r3,PrntFilename
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jsr	AL_SET_TYPPR
			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART

			lda	curDirHead +32
			ldx	curDirHead +33
			jsr	AL_SET_SDIR
			jmp	AL_SET_END

::32			rts

::ENTRY			w $0000

;*** Mausklick auf Dateifenster.
:MseClkFileWin		jsr	WM_TEST_ENTRY
			bcs	:2
::1			rts

;--- Mauslick auf Datei.
::2			stx	r0L
			sty	r0H
			LoadW	r1,32
			ldx	#r0L
			ldy	#r1L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r0
			MoveW	r0,:FNameVec

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:11			; => Ja, weiter...

;--- Datei öffnen.
::OpenFile		jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;--- Datei/Verzeichnis verschieben.
::11			ldy	#$02
			lda	(r0L),y			;Dateityp "Gelöscht"?
			and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;NativeMode Verzeichnis?
			bne	:MoveFile		; => Nein, weiter...
			jmp	:MoveSDir		;Verzeichnis verschieben.

;--- Datei verschieben.
::MoveFile		tax				;Dateityp "Gelöscht"?
			bne	:12			; => Nein, weiter...
			lda	#<Icon_Deleted +1	;Zeiger auf "Gelöscht"-Icon.
			ldx	#>Icon_Deleted +1
			bne	:14

::12			ldy	#$18
			lda	(r0L),y			;GEOS-Dateityp einlesen.
			bne	:12b			; => GEOS-Datei, weiter...
			lda	#< Icon_CBM +1		;Zeiger auf "CBM"-Icon.
			ldx	#> Icon_CBM +1
			bne	:14

::12b			lda	r0L
			sta	:FNameVec +0
			clc
			adc	#$02
			sta	r9L
			lda	r0H
			sta	:FNameVec +1
			adc	#$00
			sta	r9H

			jsr	OpenWinDrive		;Laufwerk aktivieren.
			jsr	GetFHdrInfo		;Dateiheader einlesen.

			lda	#<fileHeader +5
			ldx	#>fileHeader +5
::14			sta	r4L
			stx	r4H
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:MoveExit
			tay
			bne	:MoveExit

			jsr	:MoveStage1

			jsr	:MoveFNameVec
			jsr	AL_SET_TYPFL
			jsr	AL_SET_COL_STD

			jsr	:MoveStage2
			jmp	AL_SET_END
::MoveExit		rts

;--- Verzeichnis verschieben.
::MoveSDir		lda	#< Icon_Map +1		;Zeiger auf "Verzeichnis"-Icon.
			ldx	#> Icon_Map +1
			sta	r4L
			stx	r4H
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:MoveExit
			tay
			bne	:MoveExit

			jsr	:MoveStage1

			jsr	:MoveFNameVec
			jsr	AL_SET_TYPSD
			jsr	AL_SET_COL_SDIR

			jsr	:MoveStage2

			PushW	r0
			LoadW	r4,diskBlkBuf
			jsr	GetBlock
			PopW	r0
			txa
			beq	:15b
			lda	#$00
			tax
			tay
			beq	:15c

::15b			lda	diskBlkBuf +36		;Zeiger auf aktuellen Verzeichnis-
			ldx	diskBlkBuf +37		;Eintrag zur Prüfung ob das
			ldy	diskBlkBuf +38		;Verzeichnis noch existiert.
::15c			jsr	AL_SET_SDIRE

			jmp	AL_SET_END

;--- Shared1: Datei/Verzeichnis verschieben.
::MoveStage1		jsr	AL_SET_INIT
			jsr	:MoveFNameVec
			AddVBW	5,r3
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE
			jmp	AL_SET_ICON

;--- Shared2: Datei/Verzeichnis verschieben.
::MoveStage2		jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP
			jsr	AL_SET_PART
			jsr	AL_SET_XYPOS

			jsr	:MoveFNameVec

			ldy	#$04
			lda	(r3L),y
			sta	r1H
			tax
			dey
			lda	(r3L),y
			sta	r1L
			jmp	AL_SET_SDIR

::MoveFNameVec		MoveW	:FNameVec,r3
			rts

::FNameVec		w $0000

;*** Mausklick auf Link-Eintrag ?
:MseClkAppLink		jsr	AL_FIND_ICON
			txa
			beq	:2
::1			rts

::2			bit	appLinkLocked
			bmi	:3
			jsr	WM_TEST_MOVE
			bcc	:3
			jmp	AL_MOVE_ICON

::3			lda	mouseData
			bpl	:3
			ClrB	pressFlag

			jmp	AL_OPEN_ENTRY
