﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** AppLink-Laufwerk öffnen.
.AL_SET_DEVICE		lda	#$00
			sta	Flag_ALOpenDisk

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			tax

			ldy	#LINK_DATA_DVTYP
			lda	(r14L),y
			ldy	driveType   -8,x
			beq	:1
			cmp	RealDrvType -8,x
			beq	:OpenAppLPart

::1			ldx	#$08
::2			lda	driveType   -8,x
			beq	:3
			cmp	RealDrvType -8,x
			beq	:OpenAppLPart
::3			inx
			cpx	#$0c
			bcc	:2

;--- AppLink-Laufwerk nicht gefunden.
::error			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			sec
			sbc	#$08
			clc
			adc	#"A"
			sta	ErrDrive +10
			LoadW	r0,Dlg_ErrAppLink
			jsr	DoDlgBox
			ldx	#$ff
			rts

;--- CMD-Partitionen.
::OpenAppLPart		txa
			jsr	SetDevice
			txa
			bne	:error

			ldx	curDrive		;CMD-Laufwerk mit Partitionen?
			lda	RealDrvMode -8,x
			and	#SET_MODE_PARTITION
			beq	:OpenAppLSubD		; => Nein, weiter...

			ldy	#LINK_DATA_DPART
			lda	(r14L),y		;Partiton definiert?
			beq	:OpenAppLSubD		; => Nein, weiter...
			sta	r3H
			jsr	OpenPartition		;Partition öffnen.
			txa				;Diskettenfehler?
			bne	:error			; => Ja, Abbruch...
			dex
			stx	Flag_ALOpenDisk

;--- NativeMode-Unterverzeichnisse.
::OpenAppLSubD		ldx	curDrive		;CMD-Laufwerk mit Verzeichnissen?
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:OpenStdDisk		; => Nein, Ende...

			ldy	#LINK_DATA_DSDIR
			lda	(r14L),y		;Verzeichnis definiert?
			beq	:OpenStdDisk		; => Nein, Ende.
			sta	r1L
			iny
			lda	(r14L),y
			sta	r1H
			jsr	OpenSubDir		;Unterverzeichnis öffnen.
			txa				;Diskettenfehler?
			bne	:error			; => Ja, Abbruch...
			dex
			stx	Flag_ALOpenDisk

::OpenStdDisk		ldx	#$00
			bit	Flag_ALOpenDisk		;Diskette bereits geöffnet?
			bmi	:end			; => Ja, weiter...

			jsr	OpenDisk		;GEOS/OpenDisk.
			txa				;Diskettenfehler?
			bne	:error			; => Ja, Abbruch...
::end			rts

;*** Variablen.
:Flag_ALOpenDisk	b $00				;$00 = OpenDisk aufrufen.
							;$FF = OpenDisk nicht nötig.

;*** Dialogboxen.
:Dlg_ErrAppLink		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w ErrDrive
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk kann das AppLink-",NULL
::3			b PLAINTEXT
			b "nicht öffnen!",NULL

:ErrDrive		b BOLDON
			b "Laufwerk X:",NULL
