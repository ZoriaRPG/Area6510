﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Texte für Menüs.
:PopUpText_050		b PLAINTEXT,BOLDON									,"Überlappend",NULL
:PopUpText_051		b PLAINTEXT,BOLDON									,"Nebeneinander",NULL
:PopUpText_052		b PLAINTEXT,BOLDON									,"Neu laden",NULL
:PopUpText_053		b PLAINTEXT,BOLDON									,"Neue Ansicht",NULL
:PopUpText_054		b PLAINTEXT,BOLDON									,"( ) Hintergrundbild",NULL
:PopUpText_055		b PLAINTEXT,BOLDON									,"( ) AppLink sperren",NULL
:PopUpText_056		b PLAINTEXT,BOLDON									,"Hintergrund wechseln",NULL
:PopUpText_100		b PLAINTEXT,BOLDON									,"Öffnen",NULL
:PopUpText_101		b PLAINTEXT,BOLDON									,"Partition wechseln",NULL
:PopUpText_102		b PLAINTEXT,BOLDON									,"Installieren",NULL
:PopUpText_103		b PLAINTEXT,BOLDON									,"Drucker",NULL
:PopUpText_104		b PLAINTEXT,BOLDON									,"Löschen",NULL
:PopUpText_105		b PLAINTEXT,BOLDON									,"( ) Gelöschte Dateien",NULL
:PopUpText_106		b PLAINTEXT,BOLDON									,"Hauptverzeichnis",NULL
:PopUpText_107		b PLAINTEXT,BOLDON									,"Verzeichnis zurück",NULL
:PopUpText_108		b PLAINTEXT,BOLDON									,"( ) Titel anzeigen",NULL
:PopUpText_109		b PLAINTEXT,BOLDON									,"Laufwerk öffnen",NULL
:PopUpText_110		b PLAINTEXT,BOLDON									,"  >> Laufwerk A:",NULL
:PopUpText_111		b PLAINTEXT,BOLDON									,"  >> Laufwerk B:",NULL
:PopUpText_112		b PLAINTEXT,BOLDON									,"  >> Laufwerk C:",NULL
:PopUpText_113		b PLAINTEXT,BOLDON									,"  >> Laufwerk D:",NULL
:PopUpText_114		b PLAINTEXT,BOLDON									,"( ) Größe in KByte",NULL
:PopUpText_115		b PLAINTEXT,BOLDON									,"( ) Textmodus",NULL
:PopUpText_116		b PLAINTEXT,BOLDON									,"( ) Details zeigen",NULL
:PopUpText_117		b PLAINTEXT,BOLDON									,"( ) Anzeige bremsen",NULL
:PopUpText_118		b PLAINTEXT,BOLDON									,"( ) Farbe anzeigen",NULL
:PopUpText_119		b PLAINTEXT,BOLDON									,"Eingabegerät",NULL
:PopUpText_120		b PLAINTEXT,BOLDON									,"( ) Dateifilter",NULL
:PopUpText_200		b PLAINTEXT,BOLDON									,"AppLink löschen",NULL
:PopUpText_201		b PLAINTEXT,BOLDON									,"AppLink erstellen",NULL
:PopUpText_300		b PLAINTEXT,BOLDON									,">> Anzeige",NULL
:PopUpText_301		b PLAINTEXT,BOLDON									,">> Diskette",NULL
:PopUpText_302		b PLAINTEXT,BOLDON									,">> Datei",NULL
:PopUpText_400		b PLAINTEXT,BOLDON									,"Löschen",NULL
:PopUpText_401		b PLAINTEXT,BOLDON									,"Formatieren",NULL
:PopUpText_402		b PLAINTEXT,BOLDON									,"Aufräumen",NULL
:PopUpText_403		b PLAINTEXT,BOLDON									,"Eigenschaften",NULL
:PopUpText_500		b PLAINTEXT,BOLDON									,"Alle Dateien",NULL
:PopUpText_501		b PLAINTEXT,BOLDON									,"Anwednungen",NULL
:PopUpText_502		b PLAINTEXT,BOLDON									,"Autostart",NULL
:PopUpText_503		b PLAINTEXT,BOLDON									,"Dokumente",NULL
:PopUpText_504		b PLAINTEXT,BOLDON									,"Hilfsmittel",NULL
:PopUpText_505		b PLAINTEXT,BOLDON									,"Zeichensatz",NULL
:PopUpText_506		b PLAINTEXT,BOLDON									,"Druckertreiber",NULL
:PopUpText_507		b PLAINTEXT,BOLDON									,"Eingabetreiber",NULL
:PopUpText_508		b PLAINTEXT,BOLDON									,"BASIC-Programme",NULL

:PopUpData_Vec		w PopUpData_000			;PopUp auf DeskTop.
			w PopUpData_001			;PopUp auf AppLink.
			w PopUpData_002			;PopUp auf Arbeitsplatz.
			w PopUpData_003			;PopUp auf AppLink/Drucker.
			w PopUpData_004			;PopUp auf AppLink/Laufwerk.
			w PopUpData_005			;PopUp auf Arbeitsplatz/Drucker.
			w PopUpData_006			;PopUp auf Datei in Fenster.
			w PopUpData_007			;PopUp auf Fenster/Lfwk.1541/71/81.
			w PopUpData_008			;PopUp auf Arbeitsplatz/Laufwerk.
			w PopUpData_009			;PopUp auf Fenster/Lfwk.Native.
			w PopUpData_010			;PopUp auf AppLink/Verzeichnis.
			w PopUpData_011			;Untermenü "Anzeige" auf Fens/Lfwk.
			w PopUpData_012			;Untermenü "Diskette" auf Fens/Lfwk.
			w PopUpData_013			;PopUp auf Arbeitsplatz/Eingabe.
			w PopUpData_014			;Untermenü "Anzeige/Dateifilter".

:PopUpRout_Vec		w PopUpRout_000
			w PopUpRout_001
			w PopUpRout_002
			w PopUpRout_003
			w PopUpRout_004
			w PopUpRout_005
			w PopUpRout_006
			w PopUpRout_007
			w PopUpRout_008
			w PopUpRout_009
			w PopUpRout_010
			w PopUpRout_011
			w PopUpRout_012
			w PopUpRout_013
			w PopUpRout_014

:PopUpData_Width	b $7f
			b $5f
			b $7f
			b $6f
			b $6f
			b $6f
			b $5f
			b $7f
			b $6f
			b $7f
			b $5f
			b $7f
			b $5f
			b $5f
			b $6f

;*** PopUp auf DeskTop.
:PopUpData_000		b $00,$00
			w $0000,$0000

			b 6!VERTICAL

			w PopUpText_050
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_051
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_056
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_054
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_108
			b MENU_ACTION
			w PopUpExit_000

			w PopUpText_055
			b MENU_ACTION
			w PopUpExit_000

:PopUpRout_000		w WM_FUNC_SORT
			w WM_FUNC_POS
			w MNU_OPEN_BACKSCR
			w PF_BACK_SCREEN
			w PF_VIEW_ALTITLE
			w PF_LOCK_APPLINK

;*** PopUp auf AppLink.
:PopUpData_001		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_001

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_001

:PopUpRout_001		w AL_OPEN_ENTRY
			w AL_DEL_ENTRY

;*** PopUp auf Arbeitsplatz.
:PopUpData_002		b $00,$00
			w $0000,$0000

			b 5!VERTICAL

			w PopUpText_109
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_110
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_111
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_112
			b MENU_ACTION
			w PopUpExit_002

			w PopUpText_113
			b MENU_ACTION
			w PopUpExit_002

:PopUpRout_002		w AL_OPEN_ENTRY
			w PF_OPEN_DRV_A
			w PF_OPEN_DRV_B
			w PF_OPEN_DRV_C
			w PF_OPEN_DRV_D

;*** PopUp auf AppLink/Drucker.
:PopUpData_003		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_102
			b MENU_ACTION
			w PopUpExit_003

			w GMenuText_11
			b MENU_ACTION
			w PopUpExit_003

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_003

:PopUpRout_003		w AL_OPEN_PRNT
			w AL_SWAP_PRINTER
			w AL_DEL_ENTRY

;*** PopUp auf AppLink/Laufwerk.
:PopUpData_004		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_004

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_004

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_004

:PopUpRout_004		w AL_OPEN_ENTRY
;			w AL_SWAP_PART
			w MNU_SWAP_PART
			w AL_DEL_ENTRY

;*** PopUp auf Arbeitsplatz/Drucker.
:PopUpData_005		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_103
			b MENU_ACTION
			w PopUpExit_005

			w PrntFileName
			b MENU_ACTION
			w PopUpExit_005

:PopUpRout_005		w AL_SWAP_PRINTER
			w AL_SWAP_PRINTER

;*** PopUp auf Arbeitsplatz/Eingabe.
:PopUpData_013		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_119
			b MENU_ACTION
			w PopUpExit_013

			w inputDevName
			b MENU_ACTION
			w PopUpExit_013

:PopUpRout_013		w AL_OPEN_INPUT
			w AL_OPEN_INPUT

;*** PopUp auf Datei in Fenster.
:PopUpData_006		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_006

			w PopUpText_104
			b MENU_ACTION
			w PopUpExit_006

:PopUpRout_006		w PF_OPEN_FILE
			w PF_DEL_FILE

;*** PopUp auf Fenster/Laufwerk 1541/71/81.
:PopUpData_007		b $00,$00
			w $0000,$0000

			b 7!VERTICAL

			w PopUpText_052
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_053
			b MENU_ACTION
			w PopUpExit_007

			w PopUpText_120
			b DYN_SUB_MENU
			w OPEN_MENU_FILTER

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_007

			w PopUpText_300
			b DYN_SUB_MENU
			w OPEN_MENU_VOPT

			w PopUpText_301
			b DYN_SUB_MENU
			w OPEN_MENU_DISK

			w PopUpText_201
			b MENU_ACTION
			w PopUpExit_007

:PopUpRout_007		w PF_RELOAD_DISK
			w PF_NEW_VIEW
;			w PF_OPEN_PART
			w $0000
			w MNU_SWAP_PART
			w $0000
			w $0000
			w PF_CREATE_AL

;*** PopUp auf Arbeitsplatz/Laufwerk.
:PopUpData_008		b $00,$00
			w $0000,$0000

			b 3!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_008

			w PopUpText_053
			b MENU_ACTION
			w PopUpExit_008

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_008

:PopUpRout_008		w PF_OPEN_DRIVE
			w PF_OPEN_NEWVIEW
;			w PF_SWAP_PART
			w MNU_SWAP_PART

;*** PopUp auf Fenster/Laufwerk Native.
:PopUpData_009		b $00,$00
			w $0000,$0000

			b 9!VERTICAL

			w PopUpText_052
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_053
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_120
			b DYN_SUB_MENU
			w OPEN_MENU_FILTER

			w PopUpText_101
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_106
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_107
			b MENU_ACTION
			w PopUpExit_009

			w PopUpText_300
			b DYN_SUB_MENU
			w OPEN_MENU_VOPT

			w PopUpText_301
			b DYN_SUB_MENU
			w OPEN_MENU_DISK

			w PopUpText_201
			b MENU_ACTION
			w PopUpExit_009

:PopUpRout_009		w PF_RELOAD_DISK
			w PF_NEW_VIEW
;			w PF_OPEN_PART
			w $0000
			w MNU_SWAP_PART
			w PF_OPEN_ROOT
			w PF_OPEN_PARENT
			w $0000
			w $0000
			w PF_CREATE_AL

;*** PopUp auf AppLink/Verzeichnis.
:PopUpData_010		b $00,$00
			w $0000,$0000

			b 2!VERTICAL

			w PopUpText_100
			b MENU_ACTION
			w PopUpExit_010

			w PopUpText_200
			b MENU_ACTION
			w PopUpExit_010

:PopUpRout_010		w PF_OPEN_SDIR
			w AL_DEL_ENTRY

;*** Untermenü "Anzeige" auf Fenster/Laufwerk.
:PopUpData_011		b $00,$00
			w $0000,$0000

			b 6!VERTICAL

			w PopUpText_105
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_114
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_115
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_116
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_117
			b MENU_ACTION
			w PopUpExit_011

			w PopUpText_118
			b MENU_ACTION
			w PopUpExit_011

:PopUpRout_011		w PF_VIEW_DELFILE
			w PF_VIEW_SIZE
			w PF_VIEW_ICONS
			w PF_VIEW_DETAILS
			w PF_VIEW_SLOWMOVE
			w PF_VIEW_SETCOLOR

;*** Untermenü "Diskette" auf Fenster/Laufwerk.
:PopUpData_012		b $00,$00
			w $0000,$0000

			b 4!VERTICAL

			w PopUpText_400
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_401
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_402
			b MENU_ACTION
			w PopUpExit_012

			w PopUpText_403
			b MENU_ACTION
			w PopUpExit_012

:PopUpRout_012		w $0000
			w $0000
			w $0000
			w $0000

;*** Untermenü "Anzeige/Dateifilter" auf Fenster/Laufwerk.
:PopUpData_014		b $00,$00
			w $0000,$0000

			b 9!VERTICAL

			w PopUpText_500
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_501
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_502
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_503
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_504
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_505
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_506
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_507
			b MENU_ACTION
			w PopUpExit_014

			w PopUpText_508
			b MENU_ACTION
			w PopUpExit_014

:PopUpRout_014		w PF_FILTER_ALL
			w PF_FILTER_APPS
			w PF_FILTER_EXEC
			w PF_FILTER_DOCS
			w PF_FILTER_DA
			w PF_FILTER_FONT
			w PF_FILTER_PRNT
			w PF_FILTER_INPT
			w PF_FILTER_BASIC
