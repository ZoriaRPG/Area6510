﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Aufbau der AppLink-
;    Konfigurationsdatei:
:LINK_DATA_FILE		= 0				;AppLink-Name.
:LINK_DATA_NAME		= 17				;Dateiname.
:LINK_DATA_TYPE		= 34				;AppLink-Typ:
							; $00=Anwendung.
							; $80=Arbeitsplatz.
							; $FF=Laufwerk.
							; $FE=Drucker.
							; $FD=Verzeichnis.	
:LINK_DATA_XPOS		= 35				;Icon XPos (Cards).
:LINK_DATA_YPOS		= 36				;Icon YPos (Pixel).
:LINK_DATA_COLOR	= 37				;Farbdaten (3x3 Bytes).
:LINK_DATA_DRIVE	= 46				;Laufwerk: Adresse.
:LINK_DATA_DVTYP	= 47				;Laufwerk: RealDrvType.
:LINK_DATA_DPART	= 48				;Laufwerk: Partition.
:LINK_DATA_DSDIR	= 49				;Laufwerk: SubDir Tr/Se.
:LINK_DATA_ENTRY	= 51				;Verzeichnis-Eintrag.
:LINK_DATA_WMODE	= 54				;Fensteroptionen.
							; Bit#7 = 1 : Gelöschte Dateien anzeigen.
							; Bit#6 = 1 : Icons anzeigen.
							; Bit#5 = 1 : Größe in KByte anzeigen.
							; Bit#4 = 1 : Textmodus/Details anzeigen.

:LINK_DATA_BUFSIZE	= 55				;Größe AppLink-Datensatz.
:LINK_COUNT_MAX		= 25				;Max. Anzahl AppLinks.

