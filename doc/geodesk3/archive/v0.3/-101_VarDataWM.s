﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

.GD_BACKSCR_BUF		= $0000
.GD_BACKCOL_BUF		= GD_BACKSCR_BUF + 8000

.WINDOW_DATA_SIZE	= 41

.MAX_WINDOWS		= 7

.NO_MORE_WINDOWS	= $80
.NO_WIN_SELECT		= $81
.NO_LNK_SELECT		= $82
.WINDOW_CLOSED		= $83
.WINDOW_NOT_FOUND	= $84
.JOB_NOT_FOUND		= $85
.WINDOW_BLOCKED		= $86

.SCR_WIDTH_40		= $0140
.SCR_HIGHT_40_80	= $c8
.TASKBAR_HIGHT		= $10

.MIN_AREA_WIN_Y		= $00
.MIN_AREA_WIN_X		= $0000
.MAX_AREA_WIN_X		= SCR_WIDTH_40
.MAX_AREA_WIN_XC	= MAX_AREA_WIN_X /8
.MAX_AREA_WIN_Y		= SCR_HIGHT_40_80 - TASKBAR_HIGHT
.MIN_SIZE_WIN_X		= $0050
.MIN_SIZE_WIN_Y		= $0030

.MIN_AREA_BAR_Y		= SCR_HIGHT_40_80 - TASKBAR_HIGHT
.MAX_AREA_BAR_Y		= SCR_HIGHT_40_80 - 1
.MIN_AREA_BAR_X		= $0000
.MAX_AREA_BAR_X		= SCR_WIDTH_40 - 1

.WIN_STD_POS_X		= $0020
.WIN_STD_POS_Y		= $10
.WIN_STD_SIZE_X		= $00d8
.WIN_STD_SIZE_Y		= $78

.WM_GRID_ICON_XC	= 8
.WM_GRID_ICON_X		= WM_GRID_ICON_XC *8
.WM_GRID_ICON_Y		= 4*8

;*** Zeiger auf Original-Mausroutine.
:mouseOldVec		w $0000

;*** Fenster-Scroll-Modus.
.WM_MOVE_MODE		b $00

;*** Rechtsklick auf Titelzeile.
.WM_TITEL_STATUS	b $00				;$FF=Rechtsklick auf Titelzeile.

;*** Anzahl Icons.
.WM_COUNT_ICON_X	b $00
.WM_COUNT_ICON_Y	b $00
.WM_COUNT_ICON_XY	b $00

.WM_VAR_START

;*** Anzahl offener Fenster.
.WindowOpenCount	b $01

;*** MyComputer-Flag.
.Flag_MyComputer	b $00				;$00=Nicht geöffnet od. Fenster-Nr.

;*** Aktuelles Fenster.
.WM_WCODE		b $00

;*** Fenster-Stack.
.WindowStack		b $00,$ff,$ff,$ff,$ff,$ff,$ff,$ff
			b $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff

;*** Daten für aktuelles Fenster..
.WM_WIN_DATA_BUF	s WINDOW_DATA_SIZE

;--- Fenstertyp.
;$00 = Standardfenster.
;$ff = Feste Größe.
.WM_DATA_SIZE		= WM_WIN_DATA_BUF +0
;--- Fenstergröße.
;NULL wenn Typ=$00.
;yo,yu,xl,xr wenn Typ=$FF.
.WM_DATA_Y0		= WM_WIN_DATA_BUF +1
.WM_DATA_Y1		= WM_WIN_DATA_BUF +2
.WM_DATA_X0		= WM_WIN_DATA_BUF +3
.WM_DATA_X1		= WM_WIN_DATA_BUF +5
;--- Anzahl Einträge.
.WM_DATA_MAXENTRY	= WM_WIN_DATA_BUF +7
;--- Zeiger auf ersten Eintrag.
.WM_DATA_CURENTRY	= WM_WIN_DATA_BUF +9
;--- Breite eines Eintrages.
;$00 = Standard-Icon-Breite.
.WM_DATA_GRID_X		= WM_WIN_DATA_BUF +11
;--- Höhe eines Eintrages.
;$00 = Standard-Icon-Höhe.
.WM_DATA_GRID_Y		= WM_WIN_DATA_BUF +12
;--- Anzahl Spalten.
;$00 = Wird berechnet.
.WM_DATA_COLUMN		= WM_WIN_DATA_BUF +13
;--- Anzahl Zeilen.
;$00 = Wird berechnet.
.WM_DATA_ROW		= WM_WIN_DATA_BUF +14
;--- Zeiger auf Routine für Titelzeile.
.WM_DATA_TITLE		= WM_WIN_DATA_BUF +15
;--- Zeiger auf Routine für Infozeile.
.WM_DATA_INFO		= WM_WIN_DATA_BUF +17
;--- Neues Fenster initialisieren.
;(z.B. 64K-Bank für Datei-Cache suchen)
.WM_DATA_WININIT	= WM_WIN_DATA_BUF +19
;--- Routine zur Ausgabe der Daten.
.WM_DATA_WINPRNT	= WM_WIN_DATA_BUF +21
;--- Routine wenn Fenster angeklickt.
.WM_DATA_WINSLCT	= WM_WIN_DATA_BUF +23
;--- Routine zum verschieben der Daten.
;$FFFF = Systemroutine (für Icons).
.WM_DATA_WINMOVE	= WM_WIN_DATA_BUF +25
;--- Scrollbalken.
;$00 = Kein Scrollbalken.
.WM_DATA_MOVEBAR	= WM_WIN_DATA_BUF +27
;--- Routine für rechten Mausklick.
.WM_DATA_RIGHTCLK	= WM_WIN_DATA_BUF +28
;--- Routine zum Fenster schließen.
.WM_DATA_WINEXIT	= WM_WIN_DATA_BUF +30
;--- Routine für Mehrfachauswahl.
;$0000=Nicht möglich, $ffff=Standard.
.WM_DATA_WINMSLCT	= WM_WIN_DATA_BUF +32
;--- Ungenutzt?
.WM_DATA_OPTIONS	= WM_WIN_DATA_BUF +34
;--- Routine für Einzelauswahl.
;$0000=Nicht möglich, $ffff=Standard.
.WM_DATA_WINSSLCT	= WM_WIN_DATA_BUF +35
;--- Routine zur Ausgabe eines Eintrages.
.WM_DATA_PRNFILE	= WM_WIN_DATA_BUF +37
;--- Dateieinträge einlesen.
.WM_DATA_GETFILE	= WM_WIN_DATA_BUF +39

;*** Speicher für alle Fensterdaten.
:WM_WIN_DATA		s MAX_WINDOWS  *WINDOW_DATA_SIZE

;*** Angaben zum Laufwerk, Partition, Verzeichnis.
.WIN_DRIVE		s MAX_WINDOWS			;Laufwerk für aktuelles Fenster.
.WIN_PART		s MAX_WINDOWS			;Partition für aktuelles Fenster.
.WIN_SDIR_T		s MAX_WINDOWS			;Zeiger auf Verzeichnis-Header für
.WIN_SDIR_S		s MAX_WINDOWS			;aktuelles Fenster.
.WIN_REALTYPE		s MAX_WINDOWS			;RealDrvType für Laufwerk.
.WIN_DATAMODE		s MAX_WINDOWS			;$00=Std, $80=CMD-Part., $40=DImage.

;*** Angaben zum Start der aktuellen dateien im Verzeichnis.
.WIN_DIR_TR		s MAX_WINDOWS			;Erster Verzeichis-Eintrag: Track
.WIN_DIR_SE		s MAX_WINDOWS			;Erster Verzeichis-Eintrag: Sektor
.WIN_DIR_POS		s MAX_WINDOWS			;Erster Verzeichis-Eintrag: Pos.
.WIN_DIR_NR_L		s MAX_WINDOWS			;Erster Verzeichis-Eintrag: Nummer/L
.WIN_DIR_NR_H		s MAX_WINDOWS			;Erster Verzeichis-Eintrag: Nummer/H

;*** Variablen für Fenster.
.WMODE_SLCT_L		s MAX_WINDOWS			;Anzahl ausgewählter Einträge.
.WMODE_SLCT_H		s MAX_WINDOWS

.WMODE_MAXIMIZED	s MAX_WINDOWS			;$FF=Fenster maximiert.
.WMODE_VICON		s MAX_WINDOWS			;$00=Icon-Modus, $FF=Text-Modus.
.WMODE_VSIZE		s MAX_WINDOWS			;$FF=Größen in KBytes.
.WMODE_VINFO		s MAX_WINDOWS			;$FF=Text-Modus mit Details.
.WMODE_FILTER		s MAX_WINDOWS			;$00=Kein Dateifilter.
.WMODE_SORT		s MAX_WINDOWS

.WM_VAR_END
.WM_VAR_SIZE = WM_VAR_END - WM_VAR_START
