﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** PopUp-Menü öffnen.
:OPEN_MENU_POPUP	jsr	OPEN_MENU_GETDAT	;Zeiger auf PopUp-Menu-Daten.
			jmp	OPEN_MENU

;*** GEOS-Menü öffnen.
:OPEN_MENU_GEOS		LoadW	r0,MENU_DATA_GEOS	;Zeiger auf Menü-Daten setzen.

;*** Farbe setzen und PullDown-Menü öffnen.
;    Übergabe: r0 = Zeiger auf Menüdaten.
:OPEN_MENU		jsr	DoneWithWM		;Fenster-Abfrage abschalten.
			jsr	WM_NO_MARGIN		;Fenster-grenzen löschen.
			jsr	WM_SAVE_BACKSCR		;Aktuellen Bildschirm speichern.

			jsr	OPEN_MENU_SETCOL	;Farbe für Menü setzen.

			jsr	UpdateMenuData		;Menütexte aktualisieren.

			lda	#$01
			jsr	DoMenu			;Menü aktivieren.

;*** Menü-Vektoren setzen.
:OPEN_MENU_INIT		LoadW	appMain,DrawClock
			LoadW	RecoverVector,:1	;Eigene RecoverRectangle-Routine.
			LoadW	mouseFaultVec,:2	;Eigene Mausabfrage setzen.
			rts

;--- Ersatz für RecoverRectangle.
::1			LoadW	r10,GD_BACKSCR_BUF
			LoadW	r11,GD_BACKCOL_BUF
			lda	GD_SYSDATA_BUF
			jmp	WM_LOAD_AREA

;--- Mausabfrage.
::2			lda	faultData		;Hat Mauszeiger aktuelles Menü
			and	#%00001000		;verlassen ?
			bne	:3			;Ja, ein Menü zurück.
			ldx	#%10000000
			ldy	#%11000000
::3			txa				;Hat Mauszeiger obere/linke
			and	faultData		;Grenze verlassen ?
			bne	:4			;Ja, ein Menü zurück.
			tya

::4			lda	menuNumber		;Hauptmenü aktiv ?
			beq	:5			;Ja, übergehen.
			jsr	OPEN_PREV_MENU		;Ein Menü zurück.
			jmp	OPEN_MENU_INIT

::5			jsr	RecoverAllMenus		;Menüs löschen.
			;jmp	CLOSE_MENU		;Vektoren zurücksetzen.

;*** Vektoren zurücksetzen.
:CLOSE_MENU		lda	#$00
			sta	mouseFaultVec +0	;Mausabfrage löschen.
			sta	mouseFaultVec +1
			sta	RecoverVector +0	;RecoverRectangle löschen.
			sta	RecoverVector +1

			lda	mouseOn			;Menü-Flag löschen.
			and	#%10111111
			sta	mouseOn

			clc
			jsr	StartMouseMode		;Mausabfrage starten.
			jsr	WM_NO_MOUSE_WIN		;Fenstergrenzen zurücksetzen.
			jmp	InitForWM		;Fenstermanager wieder aktivieren.

;*** Zeiger auf Menü-Initialisierung setzen.
;    Übergabe: XAKKU/XREG = Zeiger auf Menüdaten.
:MENU_SETINT		sta	r0L
			stx	r0H
:MENU_SETINT_r0		LoadW	appMain,OPEN_MENU_INIT
			;jmp	OPEN_MENU_SETCOL

;*** Hintergrundfarbe für Menü setzen.
:OPEN_MENU_SETCOL	ldy	#$05
::1			lda	(r0L),y			;Fenstergröße einlesen.
			sta	r2,y
			dey
			bpl	:1

			lda	C_PullDMenu		;Farbe für Menü setzen.
			jmp	DirectColor

;*** Erweiterung für ":DoPreviousMenu", damit auch Farben des
;    letzten Menüs richtig gesetzt werden.
:OPEN_PREV_MENU		lda	Rectangle +1		;Rectangel-Routine von
			sta	:2 +1			;GEOS auf eigene Routine umbiegen.
			lda	Rectangle +2
			sta	:3 +1

			lda	#< :1
			sta	Rectangle +1
			lda	#> :1
			sta	Rectangle +2

			jmp	DoPreviousMenu		;Vorheriges Menü aufrufen.

::1			lda	C_PullDMenu		;Farbe für Menü setzen.
			jsr	DirectColor

::2			lda	#$ff			;Original-Rectangle-Routine im
			sta	Rectangle +1		;GEOS-Kernal wieder installieren.
::3			ldx	#$ff
			stx	Rectangle +2
			jmp	CallRoutine		;Rectangle aufrufen.

;*** Informationen für aktuelles Menü einlesen.
;    Übergabe: XREG = Menü-Nr.
:OPEN_MENU_GETDAT	lda	PDat_Width,x		;Menü-Breite einlesen.
			sta	r5H

			txa				;Zeiger auf Tabelle mit den
			asl				;Menüdaten einlesen.
			tax
			lda	PDat_Vec +0,x
			sta	r0L
			lda	PDat_Vec +1,x
			sta	r0H

;--- Größe für Menü berechnen.
;Hinweis: Das Menü wird dabei auf
;ganze CARDs ab-/aufgerundet um die
;Farb-CARDs am C64 nutzen zu können.
			ldy	#$06			;Anzahl Menü-Einträge einlesen.
			lda	(r0L),y
			and	#%00001111

			tay				;Höhe für Menü berechnen.
			lda	#$02
::1			clc
			adc	#14
			dey
			bne	:1

			ora	#%00000111		;Untere Kante auf volle CARDs
			sta	r5L			;aufrunden.

			lda	#MAX_AREA_WIN_Y -1
			sec
			sbc	r5L
			cmp	mouseYPos		;Y-Position für Menü ermitteln.
			bcc	:4			;Standard = Mausposition. Wenn
			lda	mouseYPos		;Menü ausserhalb Bildschirm, dann
							;Y-Position korrigieren.
::4			and	#%11111000		;Obere Kante auf ganze CARDs
			sta	r2L			;abrunden.
			clc
			adc	r5L
			sta	r2H

			sec				;X-Position für Menü ermitteln.
			lda	#< MAX_AREA_WIN_X -1
			sbc	r5H
			sta	r3L
			lda	#> MAX_AREA_WIN_X -1
			sbc	#$00
			sta	r3H

			CmpW	r3,mouseXPos		;Standard = Mausposition. Wenn
			bcc	:5			;Menü ausserhalb Bildschirm, dann
			MoveW	mouseXPos,r3		;X-Position korrigieren.

::5			clc				;Linke Kante auf volle CARDS
			lda	r3L			;abrunden, rechte Kante auf volle
			and	#%11111000		;CARDs aufrunden.
			sta	r3L
			adc	r5H
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

			ldy	#$05			;Größe für Menü in
::6			lda	r2,y			;Menüdaten kopieren.
			sta	(r0L),y
			dey
			bpl	:6
			rts

;*** Menu-Einträge aktualisieren.
:UpdateMenuData

;--- Menüdaten für GEOS-Menü anpassen.
			ldy	#" "			;GEOS/Einstellungen: DEL anzeigen.
			lda	Flag_ViewDelFile
			beq	:set_del_mode
			ldy	#"*"
::set_del_mode		sty	GTx_18 +1

			ldy	#" "			;GEOS/Einstellungen: Farbe zeigen.
			lda	colorMode
			bne	:set_col_mode
			ldy	#"*"
::set_col_mode		sty	GTx_19 +1

			ldy	#" "			;GEOS/Einstellungen: Icons laden.
			lda	Flag_PreLoadIcon
			beq	:set_preload
			ldy	#"*"
::set_preload		sty	GTx_20 +1

			ldy	#" "			;GEOS/Einstellungen: Cache/Debug.
			lda	colorModeDebug
			beq	:set_debug
			ldy	#"*"
::set_debug		sty	GTx_21 +1

			ldy	#" "			;GEOS/Einstellungen: Icon-Cache.
			lda	Flag_IconCache
			beq	:set_icon_cache
			ldy	#"*"
::set_icon_cache	sty	GTx_22 +1

;--- Menüdaten für PopUp-Menü anpassen.
			ldx	WM_WCODE		;Fenster-Nr. einlesen.

			ldy	#" "			;Applink: Titel anzeigen.
			lda	Flag_ViewALTitle
			beq	:set_applink_t
			ldy	#"*"
::set_applink_t		sty	PTx_108 +1

			ldy	#" "			;AppLink: Gesperrt.
			lda	appLinkLocked
			beq	:set_applink_l
			ldy	#"*"
::set_applink_l		sty	PTx_055 +1

			ldy	#" "			;Desktop: Hintergrundbild zeigen.
			lda	sysRAMFlg
			and	#%00001000
			beq	:set_backscr
			ldy	#"*"
::set_backscr		sty	PTx_054 +1

			ldy	#" "			;Anzeige: Größe in KByte/Blocks.
			lda	WMODE_VSIZE,x
			bpl	:set_dir_size
			ldy	#"*"
::set_dir_size		sty	PTx_114_H +2		;Extra Byte "BOLDON" überspringen!

			ldy	#" "			;Anzeige: Icons/Text.
			lda	WMODE_VICON,x
			bpl	:set_dir_icon
			ldy	#"*"
::set_dir_icon		sty	PTx_115 +1

			ldy	#" "			;Anzeige: Details anzeigen.
			lda	WMODE_VINFO,x
			bpl	:set_dir_details
			ldy	#"*"
::set_dir_details	sty	PTx_116 +1

			ldy	#" "			;Anzeige: Ausgabe bremsen.
			lda	Flag_SlowScroll
			bpl	:set_dir_slow
			ldy	#"*"
::set_dir_slow		sty	PTx_117 +1

			ldy	#" "			;Anzeige: Dateifilter.
			lda	WMODE_FILTER,x
			beq	:set_dir_filter
			ldy	#"*"
::set_dir_filter	sty	PTx_120 +1

			rts

;*** Menü "Programme" aktivieren.
:OPEN_MENU_APPL		lda	#<MENU_DATA_APPL
			ldx	#>MENU_DATA_APPL
			jmp	MENU_SETINT

;*** Menü "Programme" verlassen.
:EXIT_MENU_APPL		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

;--- Weniger als drei Menüeinträge.
			cmp	#$00			; => Cache aktivieren.
			beq	:1
			cmp	#$01			; => Cache-Debug aktivieren.
			beq	:3
			rts

::1			jmp	MNU_OPEN_APPL
::3			jmp	MNU_OPEN_AUTO

;*** Menü "Dokumente" aktivieren.
:OPEN_MENU_DOCS		lda	#<MENU_DATA_DOCS
			ldx	#>MENU_DATA_DOCS
			jmp	MENU_SETINT

;*** Menü "Dokumente" verlassen.
:EXIT_MENU_DOCS		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

;--- Mehr als drei Menüeinträge.
			cmp	#MAX_ENTRY_DOCS		;Menü-Eintrag gültig?
			bcs	:1			; => Nein, Ende...

			asl				;Zeiger auf Routine für
			tay				;Menüeintrag einlesen.
			lda	:vecRoutTab +0,y
			ldx	:vecRoutTab +1,y
			jmp	CallRoutine		;Menüroutine aufrufen.
::1			rts

::vecRoutTab		w MNU_OPEN_DOCS			;Alle Dokumente.
			w MNU_OPEN_WRITE		;geoWrite Dokumente.
			w MNU_OPEN_PAINT		;geoPaint Dokumente.

;*** Menü "Beenden" aktivieren.
:OPEN_MENU_EXIT		lda	#<MENU_DATA_EXIT
			ldx	#>MENU_DATA_EXIT
			jmp	MENU_SETINT

;*** Menü "Beenden" verlassen.
:EXIT_MENU_EXIT		pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			jsr	UpdateCore		;Variablen sichern.
			pla

;--- Mehr als drei Menüeinträge.
			cmp	#MAX_ENTRY_EXIT		;Menü-Eintrag gültig?
			bcs	:1			; => Nein, Ende...

			asl				;Zeiger auf Routine für
			tay				;Menüeintrag einlesen.
			lda	:vecRoutTab +0,y
			ldx	:vecRoutTab +1,y
			jmp	CallRoutine		;Menüroutine aufrufen.
::1			rts

::vecRoutTab		w MNU_OPEN_EXITG		;Nach GEOS beenden.
			w MNU_OPEN_EXIT64		;Nach BASIC beenden.
			w MNU_OPEN_EXITB		;BASIC-Programm starten.

;*** Menü "Einstellungen" aktivieren.
:OPEN_MENU_SETUP	lda	#<MENU_DATA_SETUP
			ldx	#>MENU_DATA_SETUP
			jmp	MENU_SETINT

;*** Menü "Einstellungen" verlassen.
:EXIT_MENU_SETUP	pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla

;--- Mehr als drei Menüeinträge.
			cmp	#MAX_ENTRY_SETUP	;Menü-Eintrag gültig?
			bcs	:1			; => Nein, Ende...

			asl				;Zeiger auf Routine für
			tay				;Menüeintrag einlesen.
			lda	:vecRoutTab +0,y
			ldx	:vecRoutTab +1,y
			jmp	CallRoutine		;Menüroutine aufrufen.
::1			rts

::vecRoutTab		w MENU_SETUP_EDIT		;GEOS.Editor starten.
			w MENU_SETUP_PRNT		;Drucker wechseln.
			w MENU_SETUP_INPT		;Eingabe wechseln.
			w MNU_OPEN_BACKSCR		;Hintergrundbild wechseln.
			w LNK_SAVE_DATA			;AppLinks speichern.
			w MNU_SAVE_CONFIG		;Konfiguration speichern.
			w MENU_SETUP_VDEL		;Gelöschte Dateien.
			w MENU_SETUP_COLOR		;Farbe anzeigen.
			w MENU_SETUP_CACHE		;64Kb Icon-Cache.
			w MENU_SETUP_PLOAD		;PreLoad Icons.
			w MENU_SETUP_DEBUG		;Cache/Debug.

;*** Untermenüs aus Kontextmenü öffnen.
:OPEN_MENU_VOPT		ldx	#11
			b $2c
:OPEN_MENU_DISK		ldx	#12
			b $2c
:OPEN_MENU_FILTER	ldx	#14
			b $2c
:OPEN_MENU_SORT		ldx	#15
			b $2c
:OPEN_MENU_SELECT	ldx	#16
			jsr	OPEN_MENU_GETDAT
			jmp	MENU_SETINT_r0

;*** PopUp-Menü beenden.
:PExit_000		ldx	#0
			b $2c
:PExit_001		ldx	#1
			b $2c
:PExit_002		ldx	#2
			b $2c
:PExit_003		ldx	#3
			b $2c
:PExit_004		ldx	#4
			b $2c
:PExit_005		ldx	#5
			b $2c
:PExit_006		ldx	#6
			b $2c
:PExit_007		ldx	#7
			b $2c
:PExit_008		ldx	#8
			b $2c
:PExit_009		ldx	#9
			b $2c
:PExit_010		ldx	#10
			b $2c
:PExit_011		ldx	#11
			b $2c
:PExit_012		ldx	#12
			b $2c
:PExit_013		ldx	#13
			b $2c
:PExit_014		ldx	#14
			b $2c
:PExit_015		ldx	#15
			b $2c
:PExit_016		ldx	#16
			b $2c
:PExit_017		ldx	#17
			b $2c
:PExit_018		ldx	#18
			b $2c
:PExit_019		ldx	#19
			pha
			txa
			pha
			jsr	RecoverAllMenus		;Menüs löschen.
			jsr	CLOSE_MENU		;Vektoren zurücksetzen.
			pla
			asl
			tax
			lda	PSys_Vec +0,x
			sta	r0L
			lda	PSys_Vec +1,x
			sta	r0H
			pla
			asl
			tay
			iny
			lda	(r0L),y
			tax
			dey
			lda	(r0L),y
			jmp	CallRoutine

;*** PopUp-Fenster für DeskTop-Eigenschaften.
:PM_PROPERTIES		jsr	AL_FIND_ICON
			txa
			bne	:10

			MoveB	r13H,AL_CNT_FILE
			MoveW	r14 ,AL_VEC_FILE
			MoveW	r15 ,AL_VEC_ICON

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			cmp	#AL_TYPE_FILE
			beq	:11
			cmp	#AL_TYPE_MYCOMP
			beq	:12
			cmp	#AL_TYPE_SUBDIR
			beq	:15
			cmp	#AL_TYPE_PRNT
			beq	:13
			cmp	#AL_TYPE_DRIVE
			beq	:14
			rts

;--- Rechter Mausklick auf AppLink/Laufwerk.
::14			ldy	#LINK_DATA_DRIVE	;Laufwerksadresse einlesen.
			lda	(r14L),y
			tax
			lda	RealDrvMode -8,x	;Laufwerk CMD/SD2IEC?
			and	#SET_MODE_PARTITION!SET_MODE_SD2IEC
			beq	:16			; => Nein, keine Partitionsauswahl.

;--- Rechter Mausklick auf AppLink/Laufwerk/CMD/SD.
			ldx	#4
			b $2c

;--- Rechter Mausklick auf AppLink/Laufwerk/Std.
::16			ldx	#18
			b $2c

;--- Rechter Mausklick auf DeskTop.
::10			ldx	#0
			b $2c

;--- Rechter Mausklick auf AppLink/Datei.
::11			ldx	#1
			b $2c

;--- Rechter Mausklick auf AppLink/Arbeitsplatz.
::12			ldx	#2
			b $2c

;--- Rechter Mausklick auf AppLink/Drucker.
::13			ldx	#3
			b $2c

;--- Rechter Mausklick auf AppLink/Verzeichnis.
::15			ldx	#10
			jmp	OPEN_MENU_POPUP

;*** PopUp-Fenster für Datei-Eigenschaften.
:PM_FILE		ldx	WM_WCODE
			lda	WM_TITEL_STATUS		;Rechtsklick/Titel angeklickt?
			beq	:slct_window		; => Nein, weiter...

;--- Rechter Mausklkick auf Titelzeile.
;			ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl aktiv?
			bmi	:exit			; => Ja, Ende...
			bne	:slct_titel_nat

;--- Dateimodus.
;			ldx	WM_WCODE
			ldy	WIN_DRIVE,x		;Laufwerksadresse einlesen.

			lda	RealDrvMode -8,y	;SD2IEC ?
			and	#SET_MODE_SD2IEC
			bne	:slct_titel_part	; => Nein, weiter...

			lda	RealDrvMode -8,y	;Native/CMD ?
			and	#SET_MODE_PARTITION!SET_MODE_SUBDIR
			beq	:slct_window		; => Nein, weiter...

			cmp	#SET_MODE_PARTITION
			beq	:slct_titel_part	; => CMD 41/71/81.
			cmp	#SET_MODE_SUBDIR
			beq	:slct_titel_nat		; => Nicht-CMD/Native.

;--- CMD-/NativeMode-Laufwerke.
::slct_titel_cmd	ldx	#17			;Partition/Verzeichnis wechseln.
			b $2c
::slct_titel_nat	ldx	#9			;Verzeichnis wechseln.
			jmp	OPEN_MENU_POPUP

;--- CMD-41/71/81 oder SD2IEC-Laufwerk.
::slct_titel_part	jmp	PF_SWAP_DSKIMG		;Partition wechseln.

;--- Rechter Maisklick auf Titel nicht möglich.
::exit			rts

;--- Mausklick auf Fensterinhalt.
::slct_window		lda	WIN_DATAMODE,x		;Partitionsauswahl aktiv?
			bne	:popup_window		; => Ja, weiter...

			jsr	WM_TEST_ENTRY		;Icon angeklickt?
			bcc	:popup_window		; => Nein, weiter...

;--- Rechter Mausklick auf Datei-Icon.
::popup_file		stx	r0L			;Nummer Dateieintrag.
			sty	r0H

			ldx	#r0L			;Zeiger auf Dateieintrag
			ldy	#r1L			;berechnen.
			jsr	SET_POS_RAM

			ldy	#$02
			lda	(r0L),y			;Dateityp-Byte einlesen.
			cmp	#$ff			;"Weitere Dateien"?
			beq	:exit			; => Ja, Ende...

			MoveW	r0,vecDirEntry

			ldx	#6
			jmp	OPEN_MENU_POPUP		;PopUp-Menü öffnen.

;--- Rechter Mausklick auf Fenster-Hintergrund.
::popup_window		ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Partitionsauswahl aktiv?
			bne	:slct_win_part		; => Ja, weiter...

::slct_win_drive	ldx	#7			;PopUp-Menü Laufwerk.
			b $2c
::slct_win_part		ldx	#11			;PopUp-Menü Partitionsauswahl.
			jmp	OPEN_MENU_POPUP

;*** Rechter Mausklick auf Arbeitsplatz.
:PM_MYCOMP		jsr	WM_TEST_ENTRY		;Eintrag angeklickt?
			bcs	:slct_entry		; => Ja, weiter...
::exit			rts

::slct_entry		stx	MyComputerEntry +0
			sty	MyComputerEntry +1

			cpx	#$04			;Rechtsklick auf Drucker?
			beq	:slct_prnt		; => Ja, weiter...
			cpx	#$05			;Rechtsklick auf Eingabegerät?
			beq	:slct_inpt		; => Ja, weiter...
			bcs	:exit			; => Rechtsklick ungültig.

			lda	driveType,x		;Existiert Laufwerk?
			beq	:exit			; => Rechtsklick ungültig.
			lda	RealDrvMode,x		;Laufwerksmodus einlesen.
			and	#SET_MODE_PARTITION!SET_MODE_SD2IEC
			bne	:slct_drive_part	; => Nein, weiter...

::slct_drive		ldx	#19			;PopUp-Menü für Laufwerk.
			b $2c
::slct_drive_part	ldx	#8			;PopUp-Menü für Laufwerk.
			b $2c
::slct_prnt		ldx	#5			;PopUp-Menü für Drucker.
			b $2c
::slct_inpt		ldx	#13			;PopUp-Menü für Eingabegerät.
			jmp	OPEN_MENU_POPUP

