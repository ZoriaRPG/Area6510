﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Liste mit Partitions-Typen.
:DImgTypeList		b "??647181NP??????"		;SD2IEC.
:PartTypeList		b "??417181NA??????"		;CMD.

;*** DiskImage-Typ.
:DiskImgTyp		b $00				;$01-$04 für DiskImage-Typ.

;*** Partitions-Verzeichnis abrufen.
:FComPartList		b "$=P:*=??",NULL		;Nur Partitionen.
:FComDImgList		b "$:*.D??=P",NULL		;Nur DiskImages.
:FComSDirList		b "$:*=B",NULL			;Nur Verzeichnisse.

;*** Anzahl Dateien und Verzeichnisse.
:cntEntries		b $00,$00			;Dateien/Verzeichnisse getrennt.
:ListEntries		b $00				;Anzahl Gesamteinträge.

;*** Verzeichnis-Typ.
:ReadDirMode		b $00				;$00=Dateien, $FF=Verzeichnisse.

;*** Auf SD2IEC/Verzeichnis-Modus testen.
;Damit sollen nur die Anzahl der Blocks ohne
;den Verzeichnis-Inhalt einzulesen.
:FComCkSDMode		b "$:",$ff,NULL
:Blocks			w $0000				;Anzahl Blocks letzter Eintrag.

;*** Befehle zum DiskImage-Wechsel.
:FComCDRoot		w $0004				;Befehl: Zu "ROOT" wechseln.
			b "CD//"
:FComExitDImg		w $0003				;Befehl: Eine Ebene zurück.
			b "CD",$5f

;*** SD2IEC-DiskImage/Verzeichnis-Befehl.
:FComCDirDev		b $00
:FComCDir		w $0000				;Befehl: Verzeichnis/Image wechseln.
			b "CD:"
			s 17

;*** Zwischenspeicher für Partitionsdaten.
:partEntryBuf		s 30

;*** Größe für 1541/71/81-Partitionen.
:partSizeData		w 684				;Anzahl Blocks: 1541.
			w 1368				;Anzahl Blocks: 1571.
			w 3200				;Anzahl Blocks: 1581.
