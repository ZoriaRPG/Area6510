﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** BASIC/SD-ROOT-Verzeichnis öffnen.
:dirRootSD		jsr	CkSD2IECMode		;SD2IEC-Mous testen.
			txa	 													;Verzeichnis oder DiskImage?
			beq	:1			; => Verzeichnis-Modus aktiv.

			lda	#<FComExitDImg		;Aktives DiskImage verlassen.
			ldx	#>FComExitDImg
			jsr	SendCom

::1			lda	#<FComCDRoot		;Root aktivieren.
			ldx	#>FComCDRoot
			jsr	SendCom
			jmp	getDiskData		;Neues Verzeichnis einlesen.

;*** BASIC/Ein SD-Verzeichnis zurück.
:dirOpenSD		lda	#<FComExitDImg		;Ein SD2IEC-Verzeichnis zurück.
			ldx	#>FComExitDImg
			jsr	SendCom
			jmp	getDiskData		;Neues Verzeichnis einlesen.

;*** Verzeichnis oder DiskImage öffnen.
:dirOpenEntry		ldy	#$05
			ldx	#$03
::1			lda	(a0L),y			;Verzeichnisname in "CD"-Befehl
			beq	:2			;übertragen...
			cmp	#$a0
			beq	:2
			sta	FComCDir+2,x
			inx
			iny
			cpy	#$05+16
			bne	:1
::2			lda	#$00			;Befehl abschließen.
			sta	FComCDir+2,x
			stx	FComCDir+0		;Länge Befehl setzen.

			lda	#<FComCDir		;Verzeichnis/Image wechseln.
			ldx	#>FComCDir
			jsr	SendCom

			ldy	#$02
			lda	(a0L),y
			cmp	#$06			;Verzeichnis-Wechsel?
			beq	:exit			; => Nein weiter...

			jmp	OpenDisk		;Disk öffnen.

::exit			ldx	#$ff			;Flag setzen: "Dateien einlesen".
			rts
