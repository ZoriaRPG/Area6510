﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Icons für Fenster-Manager.
.Icon_Drive
			j
<MISSING_IMAGE_DATA>

.Icon_MapGD
			j
<MISSING_IMAGE_DATA>

.Icon_Map
			j
<MISSING_IMAGE_DATA>
if FALSE
<MISSING_IMAGE_DATA>

<MISSING_IMAGE_DATA>
endif

.Icon_Printer
			j
<MISSING_IMAGE_DATA>

.Icon_Input
			j
<MISSING_IMAGE_DATA>

.Icon_Deleted
			j
<MISSING_IMAGE_DATA>

.Icon_CBM
			j
<MISSING_IMAGE_DATA>

if FALSE
.Icon_Drv41
			j
<MISSING_IMAGE_DATA>

.Icon_Drv71
			j
<MISSING_IMAGE_DATA>

.Icon_Drv81
			j
<MISSING_IMAGE_DATA>

.Icon_DrvNM
			j
<MISSING_IMAGE_DATA>
endif

.Icon_41_71
			j
<MISSING_IMAGE_DATA>

.Icon_81_NM
			j
<MISSING_IMAGE_DATA>

.Icon_MoreFiles
			j                <MISSING_IMAGE_DATA>
