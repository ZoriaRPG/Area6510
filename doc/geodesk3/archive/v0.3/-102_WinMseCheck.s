﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Mausklick auf Arbeitsplatz.
:MseClkMyComputer	jsr	WM_TEST_ENTRY		;Icon ausgewählt?
			bcc	:exit_mycomp		; => Nein, weiter.

			stx	:winEntry +0		;Nr. des Eintrags speichern.
			;sty	:winEntry +1

			cpy	#$00			;Aktuell max. 255 Einträge.
			bne	:exit_mycomp		; => Größer 256, Abbruch...

			cpx	#$04			;Laufwerk/Drucker ausgewählt?
			bcc	:slct_drive		; => Laufwerk.
			beq	:slct_prnt		; => Drucker.
			cpx	#$05			;Eingabe ausgewäht?
			bne	:exit_mycomp		; => Nein, Abbruch...
			jmp	AL_OPEN_INPUT		;Eingabetreiber wechseln.
::exit_mycomp		rts

;--- Laufwerk verschieben.
::slct_drive		lda	driveType,x		;Laufwerk vorhanden?
			beq	:exit_mycomp		; => Nein, Abbruch...

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_drive		; => Ja, weiter...
			jmp	OpenDriveCurWin		;Laufwerksfenster öffnen.

::dnd_drive		lda	:winEntry		;Laufwerk aktivieren.
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk		;Diskette öffnen.

			LoadW	r4,Icon_Drive +1
			jsr	DRAG_N_DROP_ICON	;Drag`n`Drop ausführen.
			cpx	#NO_ERROR		;Icon abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.
			tay				;Auf DeskTop abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.

			jmp	AL_DnD_Drive		;AppLink für Laufwerk erstellen.

;--- Drucker verschieben.
::slct_prnt		jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_prnt		; => Ja, weiter...

			jsr	SLCT_PRINTER		;Drucker auswählen.
			txa				;Fehler?
			bne	:exit_mycomp		; => Ja, Abruch...
			jmp	UpdateMyComputer	;Arbeitsplatz aktualisieren.

::dnd_prnt		LoadW	r4,Icon_Printer +1
			jsr	DRAG_N_DROP_ICON	;Drag`n`Drop ausführen.
			cpx	#NO_ERROR		;Icon abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.
			tay				;Auf DeskTop abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.

;--- Hinweis:
;Es wird hier ein neuer Drucker
;ausgewählt der dann als AppLink auf
;dem DeskTop abgelegt wird.
;Notwendig da der Pfad zum Treiber im
;AppLink gespeichert wird.
			jsr	SLCT_PRINTER		;Drucker auswählen.
			txa				;Fehler?
			bne	:exit_mycomp		; => Ja, Abruch...

			jmp	AL_DnD_Printer		;AppLink für Drucker erstellen.

::winEntry		w $0000

;*** Mausklick auf Dateifenster.
:MseClkFileWin		jsr	WM_TEST_ENTRY		;Icon ausgewählt?
			bcs	:slct_file		; => Ja, weiter...
::exit			rts

;--- Mauslick auf Datei.
::slct_file		stx	r0L			;Datei-Nr. speichern.
			sty	r0H

			ldx	#r0L			;Zeiger auf Verzeichnis-Eintrag
			ldy	#r1L			;berechnen.
			jsr	SET_POS_RAM

			ldx	WM_WCODE
			lda	WIN_DATAMODE,x		;Fenstermodus einlesen.
			bmi	:open_partition		; => Partitionen wechseln.
			bne	:open_diskimage		; => SD2IEC/DiskImages wechseln.

			ldy	#$02
			lda	(r0L),y
			cmp	#$ff			;"Weitere Dateien"?
			beq	:more_files		; => Ja, weiter...

			MoveW	r0,:dirEntryVec		;Zeiger zwischenspeichern.

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_entry		; => Ja, weiter...

;--- Datei öffnen.
::open_file		jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;--- Weitere Dateien öffnen.
::more_files		jmp	LoadMoreFiles		;"Weitere Dateien" angeklickt.

;--- SD2IEC: DiskImage oder Verzeichnis öffnen.
::open_diskimage	MoveW	r0,a0
			jsr	OPEN_SD_ENTRY		;SD2IEC-Eintrag öffnen.
			txa				;Status prüfen:
			bne	:reload_window		; => Verzeichnis gewechselt.
			beq	:reset_window		; => DiskImage geöffnet.

;--- CMD: Partition öffnen.
::open_partition	ldy	#$03
			lda	(r0L),y			;Partitions-Nr. einlesen und
			sta	r3H			;speichern.
			jsr	OpenPartition		;Partition wechseln.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			jsr	SaveWinDrive		;Laufwerksdaten aktualisieren.

::reset_window		ldx	WM_WCODE
			lda	#$00			;Partitionsmodus löschen.
			sta	WIN_DATAMODE,x

			txa				;Partition ausgewählt.
			pha				;Falls "MyComputer" geöffnen:
			jsr	UpdateMyComputer	;"MyComputer" aktualisieren.
			pla
			jsr	WM_WIN2TOP		;Laufwerks-Fenster wieder
			jsr	WM_LOAD_WIN_DATA	;aktivieren.

::reload_window		lda	#$00			;Anzahl Einträge löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	SET_LOAD_DISK		;Dateien von Disk neu einlesen.
			jmp	WM_CALL_DRAW		;Fenster neu laden.

;--- Datei/Verzeichnis verschieben.
::dnd_entry		ldy	#$02
			lda	(r0L),y			;Dateityp "Gelöscht"?
			and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;NativeMode Verzeichnis?
			bne	:dnd_file		; => Nein, weiter...
			jmp	:dnd_subdir		;Verzeichnis verschieben.

;--- Datei aus Fenster verschieben.
::dnd_file		tax				;Dateityp "Gelöscht"?
			bne	:chk_ftype		; => Nein, weiter...
			lda	#<Icon_Deleted +1	;Zeiger auf "Gelöscht"-Icon.
			ldx	#>Icon_Deleted +1
			bne	:do_dnd

::chk_ftype		jsr	CheckFType		;Dateityp auswerten.
			cpx	#NO_ERROR		;Starten möglich?
			bne	:exit_fwin		; => Nein, Ende...

			cmp	#$00			;BASIC oder GEOS-Datei?
			bne	:1			; => GEOS-Datei, weiter...
			lda	#<Icon_CBM +1		;Zeiger auf "CBM"-Icon.
			ldx	#>Icon_CBM +1
			bne	:do_dnd

::1			cmp	#PRINTER		;Druckertreiber?
			bne	:2			; => Nein, weiter...
			lda	#<Icon_Printer +1
			ldx	#>Icon_Printer +1
			bne	:do_dnd

::2			lda	r0L			;Zeiger auf Verzeichnis-Eintrag
			clc				;berechnen, da GetFHdrInfo einen
			adc	#$02			;30-Byte-Eintrag erwartet.
			sta	r9L
			lda	r0H
			adc	#$00
			sta	r9H

			jsr	OpenWinDrive		;Laufwerk aktivieren.
			jsr	GetFHdrInfo		;Dateiheader einlesen.

			lda	#<fileHeader +5
			ldx	#>fileHeader +5
::do_dnd		sta	r4L			;Zeiger auf Icon für DnD
			stx	r4H			;setzen.
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:exit_fwin
			tay				;Ziel-Fenster = DeskTop?
			bne	:exit_fwin		; => Nein, Ende.

			MoveW	:dirEntryVec,r0		;Zeiger auf Verzeichnis_Eintrag.

			ldy	#$18
			lda	(r0L),y			;GEOS-Dateityp auswerten.
			cmp	#PRINTER		;Drucker?
			beq	:dnd_prnt		; => Ja, Drucker-AppLink erstellen.

			jmp	AL_DnD_Files		;AppLink für Datei erstellen.

;--- Dateiklick abbrechen.
::exit_fwin		rts

;--- Drucker aus Fenster verschieben.
::dnd_prnt		jmp	AL_DnD_FilePrnt		;AppLink für Drucker erstellen.

;--- Verzeichnis aus Fenster verschieben.
::dnd_subdir		lda	#< Icon_Map +1		;Zeiger auf "Verzeichnis"-Icon.
			ldx	#> Icon_Map +1
			sta	r4L
			stx	r4H
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:exit_fwin
			tay				;Ziel-Fenster = DeskTop?
			bne	:exit_fwin		; => Nein, Ende.

			MoveW	:dirEntryVec,r0		;Zeiger auf Verzeichnis_Eintrag.

			ldy	#$03
			lda	(r0L),y			;Track/Sektor für Verzeichnis-
			sta	r1L			;Header einlesen.
			iny
			lda	(r0L),y
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock		;Verzeichnis-Header einlesen.
			txa				;Fehler?
			bne	:exit_fwin		; => Ja, Abbruch...

			jmp	AL_DnD_SubDir		;AppLink für Verzeichnis erstellen.

::dirEntryVec		w $0000

;*** Weitere Dateien einlesen.
:LoadMoreFiles		lda	mouseData		;Warten bis Maustaste nicht
			bpl	LoadMoreFiles		;mehr gedrückt.
			ClrB	pressFlag

;			lda	#$00			;Verzeichnis auf Anfang.
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			lda	WM_DATA_MAXENTRY +0
			sec				;Anzahl Dateieinträge -1 für
			sbc	#$01 			;"Weitere Dateien" abziehen.
			ldy	WM_DATA_MAXENTRY +1
			bcs	:1
			dey

::1			ldx	WM_WCODE		;Position im Verzeichnis
			clc				;berechnen.
			adc	WIN_DIR_NR_L,x
			sta	WIN_DIR_NR_L,x
			tya
			adc	WIN_DIR_NR_H,x
			sta	WIN_DIR_NR_H,x

			jsr	SET_LOAD_DISK		;Dateien von Disk einlesen.
			jsr	WM_CALL_GETFILES	;Verzeichnis einlesen.
			jmp	WM_CALL_REDRAW		;Fenster neu zeichnen.

;*** Mausklick auf Link-Eintrag ?
:MseClkAppLink		jsr	AL_FIND_ICON		;AppLink finden.
			txa				;AppLink gewählt?
			beq	:open_link		; => Ja, weiter...
			rts

::open_link		bit	appLinkLocked		;AppLinks gesperrt?
			bmi	:wait			; => Ja, weiter...

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcc	:wait			; => Nein, weiter...
			jmp	AL_MOVE_ICON

::wait			lda	mouseData		;Warten bis Maustaste nicht
			bpl	:wait			;mehr gedrückt.
			ClrB	pressFlag

			jmp	AL_OPEN_ENTRY		;AppLink öffnen.
