﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* Partition wechseln.
;* SD2IEC Hauptverzeichnis.
;* SD2IEC Elternverzeichnis.
;* SD2IEC Eintrag öffnen.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopMac.MP3"
			t "TopSym.GD"
			t "TopSym.ROM"
			t "s.mod.#101.ext"
endif

			n "mod.#103.obj"
			t "-SYS_CLASS.h"
			f DATA
			o VLIR_BASE
			p VLIR_BASE
			a "Markus Kanet"

:VlirJumpTable		jmp	getDiskData
			jmp	dirRootSD
			jmp	dirOpenSD
			jmp	dirOpenEntry

;--- Fenstermodus => Partitionen oder SD2IEC-DiskImages einlesen.
:readPartModeCMD	= TRUE				;CMD-Partitionen über Tabelle einlesen.
;readPartModeCMD	= FALSE				;CMD-Partitionen über Verzeichnis einlesen.
			t "-103_DiskCore"
			t "-103_DiskShared"
			t "-103_DiskSD2IEC"
			t "-103_DiskCMD"
			t "-103_DirSelect"
			t "-103_Data"
