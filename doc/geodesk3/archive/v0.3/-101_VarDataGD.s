﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Erweiterte Systemvariablen.
.GD_VAR_START
.Flag_ReloadFiles	b $00				;$80 = Dateien von Disk laden.
							;$40 = BAM testen/Cache oder Disk.
							;$3F = Nur Dateien sortieren.
							;$00 = Dateien aus Cache.
.colorMode		b $ff				;$FF = S/W-Modus.
.colorModeDebug		b $00				;$FF = Debug-Modus, Cache=Farbig.
.colorIconCache		b $20				;$20 = Farbe für Icons aus Cache.
.colorIconDisk		b $00				;$00 = S/W-Datei-Icons.
.appLinkLocked		b $00				;$00 = Drag'n'Drop für AppLinks.
							;$FF = AppLinks gesperrt.
.Flag_ViewALTitle	b $00				;$00 = Keine Titel anzeigen.
							;$FF = Titel anzeigen
.Flag_BackScreen	b $ff				;$00 = Kein Hintergrundbild.
							;$FF = Hintergrundbild verwenden.
.Flag_SlowScroll	b $00				;$00 = SlowScroll deaktiviert.
							;$FF = SlowScroll aktiv.
.Flag_ViewDelFile	b $00				;$00 = Gelöschte Dateien aus.
							;$FF = Gelöschte Dateien ein.
.Flag_IconCache		b $ff				;$00 = Kein Icon-Cache aktiv.
							;$FF = 64K-Icon-Cache aktiv.
.Flag_PreLoadIcon	b $00				;$00 = Icon nicht in Speicher laden.
							;$FF = Icon in Speicher laden.

.GD_VAR_END
