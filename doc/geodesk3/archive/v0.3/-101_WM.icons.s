﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Icons.
:Icon_MoveW		= 1				;Breite für Icons in CARDs.
:Icon_MoveH		= 8				;Höhe für Icons in Pixel.

:Icon_UL
<MISSING_IMAGE_DATA>

:Icon_UR
<MISSING_IMAGE_DATA>

:Icon_DL
<MISSING_IMAGE_DATA>

:Icon_DR
<MISSING_IMAGE_DATA>

:Icon_CL
<MISSING_IMAGE_DATA>

:Icon_PU
<MISSING_IMAGE_DATA>

:Icon_PD
<MISSING_IMAGE_DATA>

:Icon_ST
<MISSING_IMAGE_DATA>

:Icon_DN
<MISSING_IMAGE_DATA>

:Icon_MX
<MISSING_IMAGE_DATA>

:Icon_MN
<MISSING_IMAGE_DATA>

