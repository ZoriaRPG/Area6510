﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Speichern der Systemvariablen, Programm-Modul und
;    Verzeichnisdaten. Wird verwendet bevor man das Modul
;    wechselt oder eine Anwendung startet.
.UpdateCore		lda	GD_RAM_GDESK
			beq	:1

			jsr	UPDATE_APPVAR		;Variablen speichern.
			jsr	UPDATE_MAINMOD		;Haupt-Modul speichern.
			jsr	UPDATE_CURMOD		;Aktuelles Programm-Modul speichern.

			lda	WM_WCODE		;Fenster geöffnet?
			beq	:1			; => Nein, weiter...

			jsr	SET_CACHE_DATA		;Zeiger auf Dateien im Cache.
			jmp	StashRAM		;Verzeichnisdaten speichern.

::1			rts				;Kein "GeoDesk im RAM".
