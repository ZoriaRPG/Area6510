﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** AppLinks auf DeskTop ausgeben.
:AL_DRAW_FILES		LoadW	r0,FontG3		;GeoDesk-Font aktivieren.
			jsr	LoadCharSet

			jsr	AL_INIT_TAB		;AppLink-Register initialisieren.

::1			jsr	WM_NO_MARGIN		;Fenstergrenzen löschen.

			jsr	AL_DEF_AREA		;Position/Größe für Icon.

			lda	#$00			;Bildschirm-Bereich für
			jsr	SetPattern		;Icon löschen.
			jsr	Rectangle
			lda	#$07			;Farben für AppLink-Icon auf
			jsr	DirectColor		;Bildschirm löschen.

			bit	Flag_ViewALTitle	;AppLink-Titel anzeigen?
			bpl	:2			; => Nein, weiter...

			PushB	r2L			;Position/Größe für Icon speichern.
			PushB	r2H
			PushW	r3
			PushW	r4

			ldx	r2H			;Position/Größe für Titel
			inx				;berechnen.
			stx	r2L
			txa
			clc
			adc	#$07
			sta	r2H
			SubVW	16,r3
			AddVBW	16,r4
			MoveW	r3,leftMargin
			MoveW	r4,rightMargin

			lda	#$00			;Bildschirm-Bereich für
			jsr	SetPattern		;AppLink-Titel löschen.
			jsr	Rectangle
			lda	#$07			;Farben für AppLink-Titel auf
			jsr	DirectColor		;Bildschirm löschen.

			PopW	r4			;Position/Größe für Icon wieder
			PopW	r3			;herstellen.
			PopB	r2H
			PopB	r2L

::2			lda	r15L			;Zeiger auf Speicher für
			sta	r0L			;AppLink-Icon.
			lda	r15H
			sta	r0H

			jsr	WM_CONVERT_PIXEL	;Koordinate von Pixel nach CARDs.

			LoadB	r2L,3			;Breite Icon in CARDs.
			LoadB	r2H,21			;Höhe Icon in Pixel.
			LoadB	r3L,$00			;Farbe für Icon.
			LoadB	r3H,5			;Delta-Y für Titel.

			lda	r14L			;Zeiger auf Farbtabelle in
			clc				;AppLink-Daten berechnen.
			adc	#< LINK_DATA_COLOR
			sta	r8L
			lda	r14H
			adc	#> LINK_DATA_COLOR
			sta	r8H

			lda	r14L			;Zeiger auf Titel in
			clc				;AppLink-Daten berechnen.
			adc	#17
			sta	r4L
			lda	r14H
			adc	#$00
			sta	r4H

			PushB	r1L			;Position für Icon speichern.
			PushB	r1H

			bit	Flag_ViewALTitle	;AppLink-Titel anzeigen?
			bpl	:3			; => Nein, weiter...

			jsr	GD_FICON_NAME		;Icon und Titel ausgeben.
			jmp	:4

::3			jsr	GD_DRAW_FICON		;Nur Icon ausgeben.

::4			PopB	r1H			;Position für Icon zurücksetzen.
			PopB	r1L

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y		;AppLink-Typ einlesen.
			cmp	#AL_TYPE_DRIVE		;Laufwerk?
			bne	:5			; => Nein, weiter...

			ldy	#LINK_DATA_DRIVE	;Laufwerksbuchstabe auf
			lda	(r14L),y		;AppLink-Icon schreiben.
			sec
			sbc	#$08
			jsr	PrntGeosDrvName		;Laufwerk A: bis D: ausgeben.

::5			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15

			dec	AL_CNT_ENTRY		;Alle AppLinks ausgegeben?
			beq	:6			; => Ja, Ende...
			jmp	:1			; => Nein, weiter...
::6			rts

;*** AppLink-Icon unter Mauszeiger suchen.
;    Rückgabe: XREG = $00, Icon unter Mauszeiger.
;              r14  = Zeiger auf AppLink-Daten.
;              r15  = Zeiger auf AppLink-Icon.
:AL_FIND_ICON		php				;IRQ-Status speichern und
			sei				;Interrupt sperren.

			jsr	AL_INIT_TAB		;AppLink-Register initialisieren.
::1			jsr	AL_DEF_AREA		;Position/Größe für Icon.

			jsr	IsMseInRegion		;Mauszeiger auf Icon?
			tax
			bne	:2			; => Ja, gefunden. Ende.

			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15
			inc	r13H

			lda	r13H
			cmp	LinkData		;Alle AppLink-Icons geprüft?
			bne	:1			; => Nein, weiter...

			plp				;IRQ-Status zurücksetzen.
			ldx	#FILE_NOT_FOUND		;Kein AppLink unter Mauszeiger.
			rts

::2			plp				;IRQ-Status zurücksetzen.
			ldx	#NO_ERROR		;AppLink unter Mauszeiger gefunden.
			rts

;*** Zeiger auf AppLink-Daten initialisieren.
;    Rückgabe: r13H = Aktueller AppLink.
;              r14  = Zeiger auf AppLink-Daten.
;              r15  = Zeiger auf AppLink-Icon.
;              AL_CNT_ENTRY = Anzahl AppLinks.
:AL_INIT_TAB		LoadB	r13H,0			;Aktuelle AppLink-Nr.
			LoadW	r14,LinkData		;Zeiger auf AppLink-Daten.
			LoadW	r15,Icons		;Zeiger auf AppLink-Icon.

			ldy	#$00
			lda	(r14L),y		;Anzahl AppLinks einlesen.
			sta	AL_CNT_ENTRY

			AddVBW	1,r14			;AppLink-Zähler überspringen.
			rts

;*** Position/Größe für AppLink-Icon.
;    Rückgabe: r2L/r2H = Y oben/unten.
;              r3/r4   = X links/rechts.
:AL_DEF_AREA		ldy	#LINK_DATA_YPOS
			lda	(r14L),y		;Y-Position in CARDs einlesen.
			asl				;Nach Pixel wandeln.
			asl
			asl
			sta	r2L
			clc
			adc	#$17			;Untere Grenze für Icon
			sta	r2H			;berechnen.

			ldy	#LINK_DATA_XPOS
			lda	(r14L),y		;X-Position in CARDs einlesen.
			asl				;Nach Pixel wandeln.
			asl
			asl
			sta	r3L
			lda	#$00
			rol
			sta	r3H

			lda	r3L			;Rechte Grenze für Icon
			clc				;berechnen.
			adc	#$17
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H
			rts

;*** Zeiger auf AppLink-Daten berechnen.
;    Übergabe: LinkData = AppLink-Nr. 0-23.
;    Rückgabe: r0 = Zeiger auf AppLink-Daten.
;              r1 = Zeiger auf AppLink-Icon.
:AL_SET_INIT		lda	LinkData		;AppLink-Nr. einlesen.
			sta	r0L
			lda	#$00
			sta	r0H

			lda	#< LINK_DATA_BUFSIZE
			sta	r1L
			lda	#> LINK_DATA_BUFSIZE
			sta	r1H			;Größe AppLink-Eintrag.

			ldx	#r0L
			ldy	#r1L
			jsr	DMult			;Zeiger innerhalb AppLink-Tabelle.

			AddVW	LinkData+1,r0		;Zeiger auf AppLink-Daten. Erstes
							;Byte übrspringen = AppLink-Zähler.

			lda	LinkData		;AppLink-Nr. einlesen.
			sta	r1L
			lda	#$00
			sta	r1H

			ldx	#r1L
			ldy	#$06
			jsr	DShiftLeft		;Zeiger innerhalb AppLink-Icons.

			AddVW	Icons     ,r1		;Zeiger auf AppLink-Icons.
			rts

;*** AppLink-Dateiname/Titel speichern.
:AL_SET_FNAME		ldy	#LINK_DATA_FILE
			b $2c
:AL_SET_TITLE		ldy	#LINK_DATA_NAME
			sty	r4L			;Position in AppLink-Daten.

			ldy	#$00
			sty	r4H			;Zeichenzähler löschen.

::1			ldy	r4H			;Zeichen aus Name einlesen.
			lda	(r3L),y			;Ende erreicht?
			beq	:2			; => Ja, weiter...
			cmp	#$a0			;$A0 = Füllbyte erreicht?
			beq	:2			; => Ja, weiter...
			ldy	r4L
			sta	(r0L),y			;Zeichen in AppLink-Daten schreiben.

			inc	r4L			;Zeiger auf nächstes Zeichen.
			inc	r4H

			lda	r4H
			cmp	#16			;Max. 16 Zeichen eingelesen?
			bcc	:1			; => Nein, weiter...

::2			ldy	r4L			;Dateiname oder Titel mit
			ldx	r4H			;$00-Bytes auffüllen und mit
			lda	#$00			;einem $00-Byte beenden.
::3			sta	(r0L),y
			iny
			inx
			cpx	#16 +1
			bcc	:3
			rts

;*** Icon in AppLink-Daten kopieren.
:AL_SET_ICON		ldy	#$3f			;63 Bytes an Icon-Daten in
::1			lda	spr1pic -1,y		;AppLink-Daten kopieren.
			sta	(r1L),y
			dey
			bne	:1

			lda	#$bf			;Kennung für GEOS-Icon-Bitmap.
			sta	(r1L),y
			rts

;*** Farbe für Icon in AppLink-Daten schreiben.
:AL_SET_COL_DRV		lda	#<Color_Drive		;Zeiger auf Farb-Tabelle für
			ldx	#>Color_Drive		;AppLink = Laufwerk.
			bne	AL_SET_COLOR

:AL_SET_COL_SDIR	lda	#<Color_SDir		;Zeiger auf Farb-Tabelle für
			ldx	#>Color_SDir		;AppLink = Verzeichnis.
			bne	AL_SET_COLOR

:AL_SET_COL_PRNT	lda	#<Color_Prnt		;Zeiger auf Farb-Tabelle für
			ldx	#>Color_Prnt		;AppLink = Drucker.
			bne	AL_SET_COLOR

:AL_SET_COL_STD		lda	#<Color_Std		;Zeiger auf Farb-Tabelle für
			ldx	#>Color_Std		;AppLink = Datei.

:AL_SET_COLOR		sta	:1 +1			;Zeiger auf Farb-Tabelle speichern.
			stx	:1 +2

			ldx	#$00
			ldy	#LINK_DATA_COLOR
::1			lda	$ffff,x			;Farbangaben in AppLink-Daten
			sta	(r0L),y			;kopieren: 3x3 Bytes.
			inx
			iny
			cpx	#$09
			bcc	:1
			rts

;*** AppLink-Typ festlegen.
:AL_SET_TYPFL		lda	#AL_TYPE_FILE		;AppLink = Datei.
			b $2c
:AL_SET_TYPSD		lda	#AL_TYPE_SUBDIR		;AppLink = Verzeichnis.
			b $2c
:AL_SET_TYPPR		lda	#AL_TYPE_PRNT		;AppLink = Drucker.
			b $2c
:AL_SET_TYPDV		lda	#AL_TYPE_DRIVE		;AppLink = Laufwerk.
:AL_SET_TYPE		ldy	#LINK_DATA_TYPE
			sta	(r0L),y
			rts

;*** AppLink-Position auf DeskTop speichern.
;Dabei wird auch die X-/Y-Position
;geprüft, da AppLinks nur innerhalb
;eines kleineren Bildschirm-Bereichs
;abgelegt werden können, da noch Platz
;für den Titel links/rechts/unterhalb
;des Icons erforderlich ist.
:AL_SET_XYPOS		CmpBI	mouseYPos,MIN_AREA_BAR_Y -$08 -$18
			bcc	:1
			LoadB	mouseYPos,MIN_AREA_BAR_Y -$08 -$18

::1			CmpWI	mouseXPos,$0010
			bcs	:2
			LoadW	mouseXPos,$0010

::2			CmpWI	mouseXPos,SCR_WIDTH_40   -$10 -$18
			bcc	:3
			LoadW	mouseXPos,SCR_WIDTH_40   -$10 -$18

::3			lda	mouseXPos +1
			lsr
			lda	mouseXPos +0
			ror
			lsr
			lsr
			ldy	#LINK_DATA_XPOS
			sta	(r0L),y

			lda	mouseYPos
			lsr
			lsr
			lsr
			ldy	#LINK_DATA_YPOS
			sta	(r0L),y
			rts

;*** Laufwerksdaten in AppLink-Daten speichern.
:AL_SET_DRIVE		lda	curDrive		;Laufwerksadresse speichern.
			ldy	#LINK_DATA_DRIVE
			sta	(r0L),y
			rts

:AL_SET_RDTYP		ldx	curDrive		;RealDrvType speichern.
			lda	RealDrvType   -8,x
			ldy	#LINK_DATA_DVTYP
			sta	(r0L),y
			rts

:AL_SET_PART		ldx	curDrive		;CMD-Partition speichern.
			lda	drivePartData -8,x
			ldy	#LINK_DATA_DPART
			sta	(r0L),y
			rts

:AL_SET_SDIR		ldy	#LINK_DATA_DSDIR	;Native-Verzeichnis speichern.
			sta	(r0L),y
			iny
			txa
			sta	(r0L),y
			rts

:AL_SET_SDIRE		pha				;Verzeichniseintrag speichern.
			tya
			ldy	#LINK_DATA_ENTRY +2
			sta	(r0L),y			;Zeiger auf Eintrag in Sektor.
			dey
			txa
			sta	(r0L),y			;Verzeichnis-Eintrag/Sektor.
			dey
			pla
			sta	(r0L),y			;Verzeichnis-Eintrag/Spur.
			rts

;*** Fensteroptionen in AppLink-Daten speichern.
;    Hinweis: Die Optionen werden dabei
;             als BITs innerhalb eines
;             Options-Byte gespeichert.
;             Max. 8 Optionen möglich!
;    Bit %7 = Icon- oder Text-Modus.
;    Bit %6 = Blocks oder KBytes.
;    Bit %5 = Text- oder Detail-Modus.
;    Bit %4 = Frei.
;    Bit %3 = Frei.
;    Bit %2 = Frei.
;    Bit %1 = Frei.
;    Bit %0 = Frei.
:AL_SET_WMODE		lda	#$00			;Fensteroptionen in AppLink
			sta	:1 +1			;Daten speichern.

			ldx	WM_WCODE		;Fenster-Nr. einlesen.

			lda	WMODE_VICON,x		;Icon- oder Text-Modus.
			and	#%10000000
			ora	:1 +1
			sta	:1 +1

			lda	WMODE_VSIZE,x		;Blocks oder KBytes.
			and	#%01000000
			ora	:1 +1
			sta	:1 +1

			lda	WMODE_VINFO,x		;Text- oder Detail-Modus.
			and	#%00100000
			ora	:1 +1
			sta	:1 +1

			ldy	#LINK_DATA_WMODE
::1			lda	#$ff
			sta	(r0L),y			;Optionen speichern.
			rts

;*** AppLink auf DeskTop verschieben.
;    Übergabe: r14 = zeiger auf AppLink-Daten.
;              r15 = Zeiger auf AppLink-Icon.
:AL_MOVE_ICON		MoveW	r14,AL_VEC_FILE		;Zeiger auf AppLink-Daten.

			lda	r15L			;GEOS-Icon-Bitmap-Kennung $BF
			clc				;übergehen und Zeiger auf Icon-
			adc	#$01			;Daten korrigieren.
			sta	r4L
			lda	r15H
			adc	#$00
			sta	r4H

			jsr	DRAG_N_DROP_ICON	;Drag`n`Drop-Routine aufrufen.
			cpx	#NO_ERROR		;Icon abgelegt?
			bne	:1			; => Nein, Abbruch...
			tay				;AppLink für "MyComputer"?
			bne	:1			; => Ja, Ende...

			MoveW	AL_VEC_FILE,r0		;Zeiger auf neue Icon-Position in
			jsr	AL_SET_XYPOS		;AppLink-Daten speichern.

			jsr	MainDrawDesktop		;DeskTop+AppLinks neu zeichnen.
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

::1			rts

;*** AppLink öffnen.
:AL_OPEN_ENTRY		MoveW	r14,AL_VEC_FILE

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
;			cmp	#AL_TYPE_FILE
			beq	AL_OPEN_FILE		;AppLink/Datei öffnen.

			cmp	#AL_TYPE_MYCOMP
			beq	AL_OPEN_MYCOM		;AppLink/MyComputer öffnen.

			cmp	#AL_TYPE_PRNT
			beq	AL_OPEN_PRNT		;AppLink/Drucker installieren.

			cmp	#AL_TYPE_SUBDIR
			beq	:1			;AppLink/Verzeichnis öffnen.

			cmp	#AL_TYPE_DRIVE
			beq	:2			;AppLink/Laufwerk öffnen.

			rts				;AppLink-Typ unbekannt, Ende.

::1			jmp	AL_OPEN_SDIR
::2			jmp	AL_OPEN_DRIVE

;*** AppLink öffnen: Arbeitsplatz.
:AL_OPEN_MYCOM		jmp	OpenMyComputer		;"MyComputer" öffnen.

;*** AppLink öffnen: Datei.
:AL_OPEN_FILE		jsr	AL_SET_DEVICE		;AppLink-Laufwerk aktivieren.
			txa				;Fehler?
			bne	:1			; => Ja, Fehler ausgeben, Abbruch.

			MoveW	r14,r6
			jsr	FindFile		;Datei suchen.
			txa				;Fehler?
			bne	:1			; => Ja, Fehler ausgeben, Abbruch.

			LoadW	r0,dirEntryBuf -2	;Zeiger auf Verzeichnis-Eintrag.
			jmp	StartFile_r0		;Datei öffnen.

::1			jmp	AL_OPEN_ERROR		;AppLink-Fehler ausgeben.

;*** AppLink öffnen: Drucker.
:AL_OPEN_PRNT		MoveW	AL_VEC_FILE,r14		;Zeiger auf AppLink-Eintrag.

			ldy	#LINK_DATA_DRIVE	;Laufwerk für AppLink/Drucker
			lda	(r14L),y		;einlesen.

			jsr	Sys_SvTempDrive		;Laufwerk temporär speichern.
			txa				;Fehler?
			bne	:err			; => Ja, Fehler ausgeben, Abbruch.

			jsr	AL_SET_DEVICE		;AppLink-Laufwerk aktivieren.
			txa				;Fehler?
			bne	:err			; => Ja, Fehler ausgeben, Abbruch.

			MoveW	AL_VEC_FILE,r0		;Druckername in GEOS-System
			LoadW	r6,PrntFileName		;übernehmen.
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Druckertreiber einlesen.
			txa
			pha
			jsr	BackTempDrive		;Original-Laufwerk zurücksetzen.
			pla				;Fehler?
			bne	:err			; => Ja, Fehler ausgeben, Abbruch.

			jsr	OPEN_PRNT_OK		;Hinweis "Drucker installiert".
			jmp	UpdateMyComputer	;"MyComputer"-Fenster aktualisieren.

::err			jmp	OPEN_PRNT_ERR		;AppLink-Fehler ausgeben.

;*** Eingabegerät wechseln.
:AL_OPEN_INPUT		jsr	SLCT_INPUT		;Eingabetreiber wechseln.
			txa				;Abruch?
			bne	:1			; => Ja, Ende...
			jmp	UpdateMyComputer	;"MyComputer"-Fenster aktualisieren.
::1			rts

;*** Fehler beim öffnen des AppLink.
:AL_OPEN_ERROR		ldy	#LINK_DATA_DRIVE
			lda	(r14L),y		;Laufwerk für AppLink einlesen und
			sec				;in Fehlermeldung übertragen.
			sbc	#$08
			clc
			adc	#"A"
			sta	ErrDrive +10
			LoadW	r0,Dlg_ErrAppLink
			jmp	DoDlgBox

;*** AppLink öffnen: Verzeichnis.
:AL_OPEN_SDIR		jsr	AL_SET_DEVICE		;AppLink-Laufwerk aktivieren.
			txa				;Fehler?
			beq	:drv_ok			; => Nein, weiter...
::drv_error		rts

::drv_ok		ldx	curDrive
			lda	RealDrvMode -8,x	;CMD-Laufwerk?
			bpl	:1			; =>  Nein, weiter...

			ldy	#LINK_DATA_DPART	;CMD-Partition einlesen.
			lda	(r14L),y
			beq	:1
			sta	r3H
			jsr	OpenPartition		;Partition aktivieren.

::1			ldy	#LINK_DATA_ENTRY
			lda	(r14L),y		;Verzeichnis-Eintrag gesetzt?
			beq	:drv_error		; => Nein, Fehler ausgeben.

			sta	r1L			;Track/Sektor für Verzeichnis-
			iny				;Eintrag setzen und Sektor
			lda	(r14L),y		;einlesen.
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock
			txa				;Fehler?
			bne	:drv_error		; => Ja, Fehler ausgeben.

			ldy	#LINK_DATA_ENTRY +2
			lda	(r14L),y
			tax
			lda	diskBlkBuf,x
			and	#%00001111
			cmp	#$06			;Ist Eintrag noch ein Verzeichnis?
			bne	:drv_error		; => Nein, Fehler ausgeben.

			ldy	#LINK_DATA_DSDIR
			lda	(r14L),y		;Ist Track/Sektor noch Zeiger auf
			cmp	diskBlkBuf+1,x		;gespeicherten Verzeichnis-Header?
			bne	:drv_error		; => Nein, Fehler ausgeben.
			sta	r1L
			iny
			lda	(r14L),y
			cmp	diskBlkBuf+2,x
			bne	:drv_error
			sta	r1H
			jsr	OpenSubDir		;Unterverzeichnis öffnen.

			jmp	AL_OPEN_NEW_DSK		;Nneues Fenster für AppLink öffnen.

;*** AppLink öffnen: Partition.
:AL_OPEN_DSKIMG		jsr	AL_SET_DEVICE		;AppLink-Laufwerk aktivieren.
			txa				;Fehler?
			bne	:exit			; => Ja, Fehler ausgeben, Abbruch.

			ldx	WM_WCODE
			lda	#$ff
			sta	WIN_DATAMODE,x		;Partitionswechsel-Modus setzen.
			jmp	AL_OPEN_NEW_PART	;Partition wechseln.
::exit			rts

;*** AppLink öffnen: Laufwerk.
:AL_OPEN_DRIVE		jsr	AL_SET_DEVICE		;AppLink-Laufwerk aktivieren.
			txa				;Fehler?
			beq	:drv_ok			; => Nein, weiter...
			rts

::drv_ok		ldx	curDrive
			lda	RealDrvMode -8,x	;CMD-Laufwerk?
			bpl	AL_OPEN_NEW_WIN		; => Nein, weiter...

			ldy	#LINK_DATA_DPART	;CMD-Partition einlesen.
			lda	(r14L),y		;Partitition gesetzt?
			beq	AL_OPEN_NEW_WIN		; => Nein, weiter...
			sta	r3H
			jsr	OpenPartition		;Partition aktivieren.

:AL_OPEN_NEW_DSK	lda	#$00			;Partitions-Modus löschen.
			b $2c
:AL_OPEN_NEW_PART	lda	#$ff			;Partitions-Modus setzen.

:AL_OPEN_NEW_WIN	pha				;Partitions-Modus speichern.
			jsr	WM_IS_WIN_FREE		;Freies Fenster suchen.

			tax				;Fenster-Nr. als Zeiger setzen.
			pla
			sta	WIN_DATAMODE,x		;Partitionsmodus für Fenster.

			jsr	AL_INIT_WMODES		;Fensteroptionen einlesen.

			ldy	#LINK_DATA_DRIVE	;AppLink-Laufwerk einlesen.
			lda	(r14L),y
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveUsrWin		;Neues Fenster öffnen.

;*** Fenster-optionen initialisieren.
;    Übergabe: XREG = Fenster-Nr.
;              r14  = Zeiger auf AppLink-Daten.
;    Hinweis: Die Optionen werden dabei
;             als BITs innerhalb eines
;             Options-Byte gespeichert.
;             Max. 8 Optionen möglich!
;    Bit %7 = Icon- oder Text-Modus.
;    Bit %6 = Blocks oder KBytes.
;    Bit %5 = Text- oder Detail-Modus.
;    Bit %4 = Frei.
;    Bit %3 = Frei.
;    Bit %2 = Frei.
;    Bit %1 = Frei.
;    Bit %0 = Frei.
:AL_INIT_WMODES		ldy	#LINK_DATA_WMODE	;Fensteroptionen einlesen.
			lda	(r14L),y

			asl
			pha
			ldy	#$00
			bcc	:1
			dey
::1			tya
			sta	WMODE_VICON,x		;Icon- oder Text-Modus.
			pla

			asl
			pha
			ldy	#$00
			bcc	:2
			dey
::2			tya
			sta	WMODE_VSIZE,x		;Blocks oder KBytes.
			pla

			asl
			ldy	#$00
			bcc	:3
			dey
::3			tya
			sta	WMODE_VINFO,x		;Text- oder Detail-Modus.
			rts

;*** AppLink: Eintrag löschen.
:AL_DEL_ENTRY		ldx	AL_CNT_FILE		;AppLink-Nr. einlesen.
			beq	:4			; => $00 = MyComputer, Abbruch.
			inx
			cpx	LinkData		;Letzter Eintrag?
			beq	:1			; => Ja, weiter...

			lda	AL_VEC_FILE +0		;AppLink-Daten aus Tabelle löschen.
			sta	r1L
			clc
			adc	#< LINK_DATA_BUFSIZE
			sta	r0L
			lda	AL_VEC_FILE +1
			sta	r1H
			adc	#> LINK_DATA_BUFSIZE
			sta	r0H

			lda	#<LinkDataEnd
			sec
			sbc	r1L
			sta	r2L
			lda	#>LinkDataEnd
			sbc	r1H
			sta	r2H

			jsr	MoveData

			lda	AL_VEC_ICON +0		;AppLink-Icon aus Tabelle löschen.
			sta	r1L
			clc
			adc	#$40
			sta	r0L
			lda	AL_VEC_ICON +1
			sta	r1H
			adc	#$00
			sta	r0H

			lda	#<IconsEnd
			sec
			sbc	r1L
			sta	r2L
			lda	#>IconsEnd
			sbc	r1H
			sta	r2H

			jsr	MoveData

;--- Letzten AppLink-Eintrag löschen.
::1			ldy	#LINK_DATA_BUFSIZE -1
			lda	#$00
::2			sta	LinkDataEnd -LINK_DATA_BUFSIZE,y
			dey
			bpl	:2

			ldy	#LINK_ICON_BUFSIZE -1
::3			sta	IconsEnd -LINK_ICON_BUFSIZE,y
			dey
			bpl	:3

			dec	LinkData		;Anzahl AppLinkss -1.

			jsr	MainDrawDesktop		;DeskTop+AppLinks neu zeichnen.
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.
::4			rts

;*** AppLink: Umbennen.
:AL_RENAME_ENTRY	jsr	LNK_RENAME		;AppLink umbennen.

			jsr	MainDrawDesktop		;DeskTop+AppLinks neu zeichnen.
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** AppLink: Drucker wechseln.
:AL_SWAP_PRINTER	MoveW	AL_VEC_FILE,r14		;Zeiger auf AppLink-Eintrag.

			jsr	AL_SET_DEVICE		;AppLink-Laufwerk öffnen.

			jsr	SLCT_PRINTER		;Drucker auswählen.
			txa				;Abbruch?
			bne	:1			; => Ja, Ende
			jmp	UpdateMyComputer	;"MyComputer"-Fenster aktualisieren.
::1			rts

;*** Variablen.
:AL_CNT_ENTRY		b $00
:AL_CNT_FILE		b $00
:AL_VEC_FILE		w $0000
:AL_VEC_ICON		w $0000

;*** Dialogboxen.
:Dlg_ErrAppLink		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w ErrDrive
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk kann den AppLink von",NULL
::3			b PLAINTEXT
			b "nicht finden oder öffnen!",NULL

:ErrDrive		b BOLDON
			b "Laufwerk X:",NULL

