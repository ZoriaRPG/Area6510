﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Datei-Icon mit Dateiname ausgeben.
;    Übergabe:		r0  = Zeiger auf Icon.
;			r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
;			r3L = Farbe
;			r3H = DeltaY
;			r4  = Zeiger auf Dateiname.
;			      (Ende mit $00- oder $A0-Byte, max. 16 Zeichen)
;    Hinweis: Die Register r1L bis r3L werden nicht verändert.
:GD_FICON_NAME		PushB	r1L			;Icon-Daten sichern.
			PushB	r1H
			PushB	r2L
			PushB	r2H
			PushB	r3L

			PushB	r3H
			PushW	r4			;Zeiger auf Dateiname retten.
			jsr	GD_DRAW_FICON		;Datei-Icon darstellen.
			PopW	r0			;Zeiger auf datename zurücksetzen.
			PopB	r3H
			txa				;Wurde Icon teilweise ausgegeben ?
			bne	:4			; => Nein, Ende...

;--- Textbreite für zentrierte Ausgabe berechnen.
			ldy	#$00
			sty	r4L			;Textbreite löschen.
			sty	r4H
::1			sty	:2 +1			;Zeiger sichern.
			lda	(r0L),y			;Zeichen einlesen. Ende erreicht ?
			beq	:3			; => Ja, weiter...
			cmp	#$a0			;Ende erreicht ?
			beq	:3			; => Ja, weiter...
			ldx	currentMode
			jsr	GetRealSize		;Zeichenbreite ermitteln und
			tya				;zur Gesamtbreite addieren.
			clc
			adc	r4L
			sta	r4L
			bcc	:2
			inc	r4H
::2			ldy	#$ff
			iny				;Zeiger auf nächstes Zeichen.
			cpy	#16			;Dateiname ausgegeben ?
			bne	:1			; => Nein, weiter...

::3			lsr	r4H			;Textbreite halbieren.
			ror	r4L

			lda	#$00			;Startposition für Textausgabe
			sta	r11H			;berechnen.
			lda	r1L
			asl
			asl
			asl
			sta	r11L
			rol	r11H
			AddVW	12,r11			;Zeiger auf Mitte des Datei-Icons.

			lda	r11L			;Textposition nach links
			sec				;versetzen => Zentrierte Ausgabe.
			sbc	r4L
			sta	r11L
			lda	r11H
			sbc	r4H
			sta	r11H

			lda	r1H			;Y-Koordinate für
			clc				;Textausgabe berechnen.
			adc	#25			;3 Cards + 1 Pixelzeile.
			clc
			adc	r3H
			sta	r1H

			jsr	PutString		;Dateiname ausgeben.
			ldx	#NO_ERROR

::4			PopB	r3L			;Icon-Daten wieder
			PopB	r2H			;zurücksetzen.
			PopB	r2L
			PopB	r1H
			PopB	r1L
			rts

;*** Datei-Icon ausgeben (63-Byte-Format).
;    Übergabe:		r0  = Zeiger auf Icon.
;			r1L = X-Koordinate (Cards)
;			r1H = Y-Koordinate (Pixel)
;			r2L = Breite (Cards)
;			r2H = Höhe (Pixel)
;			r3L = Farbe
;                 r8  = Zeiger auf Farbtabelle (3x3 Cards)
:GD_DRAW_FICON		lda	r1L			;Ist Icon innerhalb des
			asl				;sichtbaren Bereichs ?
			asl
			asl
			sta	r4L
			lda	#$00
			rol
			sta	r4H
			cmp	rightMargin +1
			bne	:1
			lda	r4L
			cmp	rightMargin +0
::1			beq	:2			; => Ja, weiter...
			bcs	:3			; => Nein, Ende...

::2			lda	r1H			;Ist Icon innerhalb des
			cmp	windowBottom		;sichtbaren Bereichs ?
			bcc	:4			; => Ja, weiter...
			ldx	#$7f
			rts
::3			ldx	#$ff			;Icon nicht dargestellt, Ende...
			rts

;--- Position im Grafikspeicher berechnen.
::4			ldx	r1H			;Zeiger auf erstes Byte in Zeile
			jsr	GetScanLine		;des Grafikspeichers berechnen.

			lda	r4L			;Zeiger auf erstes Byte für
			clc				;Icon berechnen.
			adc	r5L
			sta	r5L
			lda	r4H
			adc	r5H
			sta	r5H

::5			lda	rightMargin +1		;Breite des Icons reduzieren,
			lsr				;falls rechter Rand überschritten
			lda	rightMargin +0		;wird.
			ror
			lsr
			lsr
			tax
			inx
			stx	:6 +1
			stx	:7 +1

			lda	r1L
			clc
			adc	r2L
::6			cmp	#40
			beq	:8
			bcc	:8
::7			lda	#40			;Max. mögliche Breite berechnen.
			sec
			sbc	r1L
			sta	r2L
::8			lda	r2L			;Breite = $00 ?
			beq	:3			; => Ja, Ende...

			ldx	windowBottom		;Höhe des icons reduzieren,
			inx				;falls unterer Rand überschritten
			stx	:9 +1			;wird.
			stx	:a +1

			lda	r1H
			clc
			adc	r2H
::9			cmp	#200
			beq	:b
			bcc	:b
::a			lda	#200			;Max. mögliche Höhe berechnen.
			sec
			sbc	r1H
			sta	r2H
::b			lda	r2H			;Neue Icon-Höhe einlesen und
			sta	r4H			;zwischenspeichern. Höhe = $00 ?
			beq	:3			; => Ja, Ende...

;--- Zeiger auf Icon-Daten initialisieren.
			lda	r0L			;$BF-Code übergehen und
			clc				;Zeiger kopieren.
			adc	#$01
			sta	:read_icon_data +1
			lda	r0H
			adc	#$00
			sta	:read_icon_data +2

;--- "WriteCard"-Flag einlesen.
			ldx	r2L			;Flag für die zu beschreibenden
			lda	:card_info,x		;Cards einlesen.
			sta	r4L

;--- Icon zeichnen.
			ldx	#$00

;--- Zeiger auf Card #2 und #3 berechnen.
::c			lda	r5L
			clc
			adc	#$08
			sta	r6L
			lda	r5H
			adc	#$00
			sta	r6H

			lda	r6L
			clc
			adc	#$08
			sta	r7L
			lda	r6H
			adc	#$00
			sta	r7H

			ldy	#$00
::d			jsr	:read_icon_data		;Card #1 immer kopieren.
			sta	(r5L),y

			bit	r4L			;Card #2 kopieren ?
			bvc	:e			; => Nein, weiter...
			jsr	:read_icon_data
			sta	(r6L),y

			bit	r4L			;Card #2 kopieren ?
			bpl	:f			; => Nein, weiter...
			jsr	:read_icon_data
			sta	(r7L),y
			bne	:g

::e			inx				;Icon-Daten überlesen.
::f			inx

::g			dec	r4H			;Alle Zeilen kopiert ?
			beq	:h			; => Ja, Ende...
			iny				;Card-Zeile kopiert ?
			cpy	#$08
			bcc	:d			; => Nein, weiter...

			AddVW	40*8,r5			;Zeiger auf nächste Zeile
			jmp	:c			;Nächste Zeile ausgeben.

;--- Farbe zeichnen.
::h			lda	r1L			;Größe des Farbrechtecks
			sta	r5L			;berechnen.
			lda	r1H
			lsr
			lsr
			lsr
			sta	r5H
			lda	r2L
			sta	r6L
			lda	r2H
			lsr
			lsr
			lsr
			sta	r6H
			lda	r2H			;Höhe für Farbrechteck korrigieren,
			and	#%00000111		;falls Höhe nicht durch 8 ohne
			beq	:j			;Rest teilbar ist.
			inc	r6H

::j			lda	r3L			;Icon-Farbe = Standard?
			sta	r7L
			bne	:i			; => Nein, weiter.

;--- Farben aus Farbtabelle ausgeben.
			jsr	DrawColors
			ldx	#$00			;xReg = $00, Icon gezeichnet.
			rts

;--- Standard-Farben ausgeben.
::i			jsr	RecColorBox
			ldx	#$00			;xReg = $00, Icon gezeichnet.
			rts

;--- Byte aus Icon-Daten einlesen.
::read_icon_data	lda	$ffff,x
			inx
			rts

;--- Tabelle mit Daten zur CARD-Ausgabe.
::card_info		b $00,$00,$40,$c0

;*** Farben für Icon zeichnen.
;    Übergabe: r8  = Zeiger auf Farbtabelle (3x3 Cards)
:DrawColors		lda	r8L			;Zeiger auf Farb-Tabelle in
			sta	:copy_columns +1	;Kopier-Routine übertragen.
			lda	r8H
			sta	:copy_columns +2

			ldx	r5H			;Zeiger auf aktuelle Farb-Zeile
			lda	COLOR_LINE_L,x		;ermitteln.
			sta	r8L
			lda	COLOR_LINE_H,x
			sta	r8H

			ldx	#$00			;CARD-Zeile auf Anfang.
			stx	r7H

::copy_rows		ldy	r7H			;CARD-Zeile einlesen.
			ldx	:offset_table,y		;Zeiger auf CARD einlesen.
			ldy	r1L

			lda	r6L
			sta	r7L

::copy_columns		lda	$ffff,x
			sta	(r8L),y
			iny
			inx
			dec	r7L
			bne	:copy_columns

			AddVBW	40,r8

			inc	r7H
			dec	r6H
			bne	:copy_rows
			rts

::offset_table		b	$00,$03,$06

;*** Startadressen der Grafikzeilen.
:COLOR_LINE_L		b < ( 0*40 + COLOR_MATRIX)
			b < ( 1*40 + COLOR_MATRIX)
			b < ( 2*40 + COLOR_MATRIX)
			b < ( 3*40 + COLOR_MATRIX)
			b < ( 4*40 + COLOR_MATRIX)
			b < ( 5*40 + COLOR_MATRIX)
			b < ( 6*40 + COLOR_MATRIX)
			b < ( 7*40 + COLOR_MATRIX)
			b < ( 8*40 + COLOR_MATRIX)
			b < ( 9*40 + COLOR_MATRIX)
			b < (10*40 + COLOR_MATRIX)
			b < (11*40 + COLOR_MATRIX)
			b < (12*40 + COLOR_MATRIX)
			b < (13*40 + COLOR_MATRIX)
			b < (14*40 + COLOR_MATRIX)
			b < (15*40 + COLOR_MATRIX)
			b < (16*40 + COLOR_MATRIX)
			b < (17*40 + COLOR_MATRIX)
			b < (18*40 + COLOR_MATRIX)
			b < (19*40 + COLOR_MATRIX)
			b < (20*40 + COLOR_MATRIX)
			b < (21*40 + COLOR_MATRIX)
			b < (22*40 + COLOR_MATRIX)
			b < (23*40 + COLOR_MATRIX)
			b < (24*40 + COLOR_MATRIX)

:COLOR_LINE_H		b > ( 0*40 + COLOR_MATRIX)
			b > ( 1*40 + COLOR_MATRIX)
			b > ( 2*40 + COLOR_MATRIX)
			b > ( 3*40 + COLOR_MATRIX)
			b > ( 4*40 + COLOR_MATRIX)
			b > ( 5*40 + COLOR_MATRIX)
			b > ( 6*40 + COLOR_MATRIX)
			b > ( 7*40 + COLOR_MATRIX)
			b > ( 8*40 + COLOR_MATRIX)
			b > ( 9*40 + COLOR_MATRIX)
			b > (10*40 + COLOR_MATRIX)
			b > (11*40 + COLOR_MATRIX)
			b > (12*40 + COLOR_MATRIX)
			b > (13*40 + COLOR_MATRIX)
			b > (14*40 + COLOR_MATRIX)
			b > (15*40 + COLOR_MATRIX)
			b > (16*40 + COLOR_MATRIX)
			b > (17*40 + COLOR_MATRIX)
			b > (18*40 + COLOR_MATRIX)
			b > (19*40 + COLOR_MATRIX)
			b > (20*40 + COLOR_MATRIX)
			b > (21*40 + COLOR_MATRIX)
			b > (22*40 + COLOR_MATRIX)
			b > (23*40 + COLOR_MATRIX)
			b > (24*40 + COLOR_MATRIX)
