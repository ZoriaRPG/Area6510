﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Anzahl VLIR-Module ohne BOOT.
;Die Anzahl der Module wird dann in
;den DACC-Speicher geladen.
:GD_VLIR_COUNT		= 7

;*** VLIR-Modulnummern.
;VLIR_BOOT		= 0
:VLIR_WM		= 0
:VLIR_DESKTOP		= 1
:VLIR_PARTITION		= 2
:VLIR_APPLINK		= 3
:VLIR_FILE_OPEN		= 4
:VLIR_LOAD_FILES	= 5
:VLIR_SAVE_CONFIG	= 6

;*** Startadresse Boot-Loader.
:VLIR_BOOT_START	= $7000

;*** Max. Anzahl Dateien im RAM.
:MAX_DIR_ENTRIES	= 160				;Max.160 wegen Icon-Cache.
;MAX_DIR_ENTRIES	= 7				;Für PagingMode-Test.

;*** Startadresse Verzeichnis-Daten.
;Zusätzlich 32Bytes für "Weitere Dateien" abziehen.
:BASE_DIR_DATA		= ( OS_VARS - MAX_DIR_ENTRIES * 32 -32 )

;*** AppLink-Typen.
:AL_TYPE_FILE		= $00
:AL_TYPE_DRIVE		= $ff
:AL_TYPE_PRNT		= $fe
:AL_TYPE_SUBDIR		= $fd
:AL_TYPE_MYCOMP		= $80

;*** AppLink: Zeiger auf Datentabelle.
:AL_ID_FILE		= $00
:AL_ID_DRIVE		= $01
:AL_ID_PRNT		= $02
:AL_ID_SUBDIR		= $03

;*** AppLink: $FF = Fensteroptionen speichern.
:AL_WMODE_FILE		= $00
:AL_WMODE_DRIVE		= $ff
:AL_WMODE_PRNT		= $00
:AL_WMODE_SUBDIR	= $ff

;*** Farbe für GeoDesk-Uhr.
:GD_COLOR_CLOCK		= $07

;*** Anzahl Befehle in Spungtabelle ":MAIN".
:GD_JMPTBL_COUNT	= $02

;*** Einsprungadresse für EnterDeskTop.
:GD_ENTER_DT		= APP_RAM +3
