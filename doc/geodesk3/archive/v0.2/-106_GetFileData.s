﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateien für Laufwerksfenster einlesen.
;--- Enthält aktuelles Fenster bereits Daten?
:xGET_ALL_FILES		lda	reloadFiles
			beq	:exit

			cmp	#$7f
			beq	:test_sort_dir

			jsr	i_FillRam		;Verzeichnis-Speicher löschen.
			w	(OS_VARS - BASE_DIR_DATA)
			w	BASE_DIR_DATA
			b	$00

			jsr	OpenWinDrive		;Laufwerk öffnen.
			txa				;Fehler?
			beq	:get_files		; => Nein, weiter...

;--- Laufwerksfehler, Abbruch.
			lda	#$00			;Dateizähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	reloadFiles
			jmp	WM_SAVE_WIN_DATA	; => Ende...

;--- Dateien eilesen.
::get_files		ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%10000000		;Cache aktiv?
			beq	:no_cache		; => Nein, von Disk laden.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			beq	:no_cache		; => Keine Dateien vorhanden...

			jsr	GetFilesCache		;Dateien im Cache gültig?
			txa
			beq	:test_sort_dir		; => Ja, Ende...

::no_cache		jsr	GetFilesDisk		;Dateien von Disk laden.

			jsr	UpdateCache		;Cache aktualisieren.

			ldx	WM_WCODE
			lda	#$00
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			lda	WMODE_SORT,x		;Fenster sortieren?
			beq	:exit			; => Nein, Ende...

			and	#%01111111		;Flag löschen: Fenster sortiert.
			sta	WMODE_SORT,x
			bpl	:sort_dir

::test_sort_dir		ldx	WM_WCODE
			lda	WMODE_SORT,x		;Fenster sortieren?
			beq	:exit			; => Nein, Ende...
			bmi	:exit			; => Bereits sortiert, Ende...

::sort_dir		jsr	xSORT_ALL_FILES		;Falls "Neue Ansicht" oder wenn kein
							;Cache aktiv, dann ist eventuell
							;Sortiermodus für Fenster gesetzt.
::exit			ldx	#$00
			stx	reloadFiles
			rts

;*** Dateien von Disk laden.
:GetFilesDisk		lda	#$00			;Dateizähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1

			jsr	Get1stDirEntry		;Ersten Verzeichnis-Eintrag lesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...
			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:end_read_data		; => Ja, Ende...

::test_next_entry	jsr	TestDirEntry		;Verzeichnis-Eintrag testen und
							;ggf. in Speicher kopieren.

::next_entry		jsr	GetNxtDirEntry		;Nächsten Eintrag lesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			cpy	#$ff			;Verzeichnis-Ende erreicht?
			beq	:end_read_data		; => Ja, Ende...

			lda	WM_DATA_MAXENTRY +1
			cmp	#>MAX_DIR_ENTRIES
			bne	:1
			lda	WM_DATA_MAXENTRY +0
			cmp	#<MAX_DIR_ENTRIES
			beq	:end_read_data		;Speicher voll, Ende...

;--- Hinweis:
;Hier besteht die Möglichkeit einen
;zusäztlichen Eintrag im Verzeichnis zu
;speichern, z.B. "Weitere Dateien..."
::1			jmp	:test_next_entry	; => Weiter mit nächsten Eintrag.

;--- Komplettes Verzeichnis eingelesen.
::end_read_data		lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			beq	:exit			; => Keine Dateien vorhanden...

			ldx	WM_WCODE
			lda	WMODE_VICON,x		;Anzeige-Modus einlesen.
			bne	:10			; => Keine Icons anzeigen.

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%01000000		;Icon-Cache aktiv?
			beq	:10			; => Nein, weiter...

			bit	Flag_PreLoadIcon	;Icons vorab laden?
			bpl	:10			; => Nein, weiter...

			jsr	LoadIconToCache		;Icons in Cache kopieren.

;--- Hinweis:
;":LoadIconToCache" setzt das Flag
;"Icon im Cache" im Verzeichnis-Eintrag!
::10			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%10000000		;Verzeichnis-Cache aktiv?
			beq	:exit			; => Nein, Ende...

			jsr	CopyDIRtoCache		;Verzeichnis in Cache speichern.

::exit			jmp	WM_SAVE_WIN_DATA	; => Ende...

;*** Verzeichnis-Eintrag testen und in Speicher übernehmen.
;    Übergabe: r5 = Zeiger auf 30-Byte-Verzeichnis-Eintrag.
;              diskBlkBuf = Verzeichnis-Sektor.
;              WM_DATA_MAXENTRY = Verzeichnis-Eintrag-Nr.
:TestDirEntry		lda	WM_DATA_MAXENTRY +0
			sta	r15L
			lda	WM_DATA_MAXENTRY +1
			sta	r15H

			ldx	#r15L			;Zeiger auf Verzeichnis-Eintrag
			ldy	#r9L			;im RAM berechnen.
			jsr	WM_SET_RAM_POS

			ldy	#$00
			lda	(r5L),y			;Dateityp einlesen.
			bne	:test_sdir		; => Echte Datei, weiter...
			iny
			lda	(r5L),y			;Dagtenspur gesetzt?
			beq	:exit			; => Nein, kein Datei-Eintrag.

::test_del_file		ldx	WM_WCODE
			lda	WMODE_VDEL,x		;Gelöschte Dateien anzeigen?
			beq	:exit			; => Nein, Ende...

::test_sdir		and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;CMD-Verzeichnis?
			beq	:copy_entry		; => Ja, weiter...

::test_filter		ldx	WM_WCODE
			lda	WMODE_FILTER,x		;Filter aktiv?
			bpl	:copy_entry		; => Nein, weiter...

			ldy	#$16
			lda	(r5L),y			;GEOS-Dateityp einlesen.
			ora	#%10000000		;Bit#7 für Vergleich setzen.
			cmp	WMODE_FILTER,x		;Datei für Filter gültig?
			bne	:exit			; => Nein, nächster Eintrag.

::copy_entry		lda	#$00
			tay
			sta	(r15L),y		;Flag für "Dateiauswahl" löschen.
			iny
			lda	#$ff
			sta	(r15L),y		;Flag für "Icon im Cache" löschen.

::loop1			dey
			lda	(r5L),y			;Verzeichnis-Eintrag kopieren.
			iny
			iny
			sta	(r15L),y
			cpy	#32 -1			;Verzeichnis-Eintrag kopiert?
			bcc	:loop1			; => Nein, weiter...

			inc	WM_DATA_MAXENTRY +0
			bne	:exit
			inc	WM_DATA_MAXENTRY +1

::exit			rts

;*** Verzeichnis im RAM in Cache kopieren.
:CopyDIRtoCache		lda	#$00			;Datei-Zähler löschen.
			sta	r11L
			sta	r11H

			sta	r14L			;Zeiger auf ersten Eintrag
			sta	r14H			;im Cache setzen.
			jsr	WM_SET_CACHE_POS

			LoadW	r0,BASE_DIR_DATA	;Anfang Verzeichnis im RAM.
			MoveW	r14,r1			;Anfang Verzeichnis im Cache.
			LoadW	r2,32			;Größe Verzeichnis-Eintrag.
			MoveB	r12H,r3L		;Speicherbank (:WM_SET_CACHE_POS).

::save_entry		jsr	StashRAM		;Eintrag in Cache kopieren.

			jsr	SetVecNxEntry		;Zeiger auf nächsten Eintrag setzen.
			bne	:save_entry		;Ende erreicht? => Nein, weiter...
			rts

;*** Zeiger auf nächsten Eintrag setzen.
;    Übergabe: r0  = Zeiger auf 32-Byte-Berzeichnis-Eintrag.
;              r1  = Zeiger auf Verzeichnis-Eintrag im Cache.
;              r12L= Eintragsgröße 32/64/96 Byte.
;              r11 = Datei-Zähler.
;              MAX_DIR_ENTRY = Max. Anzahl an Dateien.
;    Rückgabe: r0  = Zeiger nächsten 32-Byte-Berzeichnis-Eintrag.
;              r1  = Zeiger nächsten Verzeichnis-Eintrag im Cache.
;              Z-Flag=1, Ende erreicht.
:SetVecNxEntry		lda	r0L			;Zeiger auf nächsten
			clc				;Verzeichnis-Eintrag im RAM.
			adc	#$20
			sta	r0L
			bcc	:1
			inc	r0H

::1			lda	r1L			;Zeiger auf nächsten
			clc				;Verzeichnis-Eintrag im RAM.
			adc	r12L			;32/64/96 Bytes (:WM_SET_CACHE_POS).
			sta	r1L
			bcc	:2
			inc	r1H

::2			inc	r11L			;Datei-Zähler erhöhen.
			bne	:3
			inc	r11H

::3			lda	r11H			;Alle Einträge kopiert?
			cmp	WM_DATA_MAXENTRY +1
			bne	:exit
			lda	r11L
			cmp	WM_DATA_MAXENTRY +0
::exit			rts				;Ende.

;*** Datei-Icons in Cache kopieren.
:LoadIconToCache	lda	#$00			;Datei-Zähler löschen.
			sta	r11L
			sta	r11H

			sta	r14L			;Zeiger auf ersten Eintrag
			sta	r14H			;im Cache setzen.
			jsr	WM_SET_CACHE_POS

			LoadW	r15,BASE_DIR_DATA	;Zeiger auf Verzeichnis im RAM.

::test_entry		jsr	GetVecIcon_r0

			lda	r0L
			ora	r0H
			beq	:next_entry

::save_entry		MoveW	r13,r1
			LoadW	r2,64			;Größe Icon-Eintrag.
			MoveB	r12H,r3L		;Speicherbank (:WM_SET_CACHE_POS).
			jsr	StashRAM		;Icon in Cache kopieren.

::next_entry		lda	r15L			;Zeiger auf nächsten
			clc				;Verzeichnis-Eintrag im RAM.
			adc	#$20
			sta	r15L
			bcc	:1
			inc	r15H

::1			lda	r13L			;Zeiger auf nächsten
			clc				;Verzeichnis-Eintrag im RAM.
			adc	r12L			;32/64/96 Bytes (:WM_SET_CACHE_POS).
			sta	r13L
			bcc	:2
			inc	r13H

::2			inc	r11L			;Datei-Zähler erhöhen.
			bne	:3
			inc	r11H

::3			lda	r11H			;Alle Einträge kopiert?
			cmp	WM_DATA_MAXENTRY +1
			bne	:4
			lda	r11L
			cmp	WM_DATA_MAXENTRY +0
			beq	:exit
::4			jmp	:test_entry		; => Nein, weiter...

::exit			rts				;Ende.

;*** Zeiger auf Datei-Icon einlesen.
;Hier werden nur GEOS-Infoblock-Icons eingelesen!
;    Übergabe: r15 = 32-Byte Verzeichnis-Eintrag.
;    Rückgabe: r0  = $0000 = Kein Icon.
;                  > $0000 = Zeiger auf GEOS-Icon.
:GetVecIcon_r0		lda	#$00			;Zeiger auf Icon löschen.
			sta	r0L
			sta	r0H

			ldy	#$02
			lda	(r15L),y		;Datei "gelöscht"?
			beq	:exit			; => Ja, Ende...

			and	#%00001111
			cmp	#$06			;Dateityp = "Verzeichnis"?
			beq	:exit			; => Ja, Ende...

			ldy	#$18
			lda	(r15L),y		;GEOS-Datei?
			beq	:exit			; => Nein, Ende...

			ldy	#$15
			lda	(r15L),y		;GEOS-Datei mit FileHeader?
			beq	:exit			; => Nein, Ende...
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader		;Zeiger auf Zwischenspeicher.

			ldy	#$01
			lda	#$00
			sta	(r15L),y		;Flag "Icon im Cache" setzen.

			jsr	GetBlock		;GEOS-FileHeader einlesen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			lda	#<fileHeader+4		;Zeiger auf Icon-Daten.
			ldx	#>fileHeader+4
			bne	:set_icon_vec

::error			lda	#<Icon_Deleted		;Fehler File-Header:
			ldx	#>Icon_Deleted		;Ersatz-icon "Gelöscht" verwenden.

::set_icon_vec		sta	r0L
			stx	r0H
::exit			rts

;*** Prüfen ob Dateien aus Cache eingelesen werden können.
;Wird :reloadFiles auf $FF gesetzt werden die Daten immer
;von Disk eingelesen. Evtl. Sinnvoll nach einem Reboot in den
;Desktop wenn andere Programme Daten auf Disk geändert haben.
;Aktuell wird die BAM geprüft, das funktioniert auf NativeMode
;aber nur bedingt (es werden nicht alle BAM-Sektoren getestet).
:GetFilesCache		;lda	reloadFiles		;Dateien von Disk lesen?
			;bne	:cache_fault		; => Ja, weiter...

::verify1541		jsr	SetBAMCache1Sek		;Zeiger auf ersten BAM Sektor.
			bne	:verify1571
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verify1571		jsr	SetBAMCache2Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verify1581		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verify1581		jsr	SetBAMCache3Sek		;Zeiger auf zweiten BAM Sektor.
			bne	:verifyNative		; => 1541, Ende...
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehler?
			bne	:cache_fault		; => Cache veraltet, Disk öffnen.

::verifyNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y
			bne	:load_from_cache

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:cache_fault		; => Ja, Disk öffnen.

			jsr	SetBAMCache4Sek		;BAM-CRC erstellen.
			jsr	VerifyRAM		;Mit Kopie im Speicher vergleichen.
			and	#%00100000		;Cache-Fehl
			beq	:load_from_cache	; => Nein, Cache gültig...

;--- Verzeichnis geändert,
;    Dateien von Disk laden.
::cache_fault		ldx	#$ff
			rts

;--- Verzeichnis nicht geändert,
;    Dateien aus Cache laden.
::load_from_cache	lda	#$00			;Datei-Zähler löschen.
			sta	r11L
			sta	r11H

			sta	r14L			;Zeiger auf ersten Eintrag
			sta	r14H			;im Cache setzen.
			jsr	WM_SET_CACHE_POS

			LoadW	r0,BASE_DIR_DATA	;Zeiger auf Verzeichnis im RAM.
			MoveW	r14,r1			;Anfang Verzeichnis im Cache.
			LoadW	r2,32			;Größe Verzeichnis-Eintrag.
			MoveB	r12H,r3L		;Speicherbank (:WM_SET_CACHE_POS).

::load_entry		jsr	FetchRAM		;Eintrag aus Cache kopieren.

			jsr	SetVecNxEntry		;Zeiger auf nächsten Eintrag setzen.
			bne	:load_entry		;Ende erreicht? => Nein, weiter...

			ldx	#$00
			rts				;Ende.

;*** Cache-Modus: Aktuelle BAM sichern.
:UpdateCache
::upd1541		jsr	SetBAMCache1Sek		;Ersten BAM-Sektor sichern?
			bne	:upd1571		; => Nein, weiter...
			jsr	StashRAM

::upd1571		jsr	SetBAMCache2Sek		;Zweiten BAM-Sektor sichern?
			bne	:upd1581		; => Nein, weiter...
			jsr	StashRAM

::upd1581		jsr	SetBAMCache3Sek		;Dritten BAM-Sektor sichern?
			bne	:updNative		; => Nein, weiter...
			jsr	StashRAM

::updNative		ldy	driveTypeCopy		;Laufwerksmodus einlesen.
			ldx	BAMCopySekInfo4,y	;Native-BAM sichern?
			bne	:exit			; => Nein, Ende...

			jsr	CreateNM_CRC		;BAM-CRC prüfen.
			txa				;Disk-Fehler?
			bne	:exit			; => Ja, Abbruch...

			jsr	SetBAMCache4Sek		;Native-BAM sichern?.
			jmp	StashRAM
::exit			rts

;*** CRC für Native-Mode BAM erstellen.
:CreateNM_CRC		jsr	i_FillRam		;CRC-Puffer löschen.
			w	256
			w	diskBlkBuf
			b	$00

			jsr	EnterTurbo		;I/O und GEOS-Turbo aktivieren.
			jsr	InitForIO

			ldx	#$02			;Zeiger auf ersten BAM-Sektor.
::1			txa
			pha
			jsr	GetBAMBlock		;BAM-Sektor einlesen.
			pla
			cpx	#NO_ERROR		;Fehler?
			bne	:exit1			; => Ja, Abbruch...

			ldy	#<dir2Head		;Zeiger auf BAM-Sektor.
			sty	r0L
			ldy	#>dir2Head
			sty	r0H
			stx	r1L
			inx
			stx	r1H
			pha
			jsr	CRC			;CRC für BAM-Sektor berechnen.w
			pla
			tax
			asl
			tay
			lda	r2L			;CRC in Pugffer kopieren.
			sta	diskBlkBuf +0,y
			lda	r2H
			sta	diskBlkBuf +1,y

			cpx	#$02			;Erster BAM-Sektor?
			bne	:2			; => Nein, weiter...
			lda	dir2Head +8		;Anzahl Tracks einlesen und
			sta	BAMTrackMaxNM		;zwischenspeichern.

::2			inx
			lda	BAMTrackCntNM-2,x
			cmp	BAMTrackMaxNM		;Max. Anzahl Tracks erreicht?
			bcs	:3			; => Ja, Ende...
			cpx	#33			;Max. Anzahl BAM-Sektoren erreicht?
			bcc	:1			; => Nein, weiter...

;--- BAM-CRC erstellt, Ende.
::3			ldx	#NO_ERROR		;Ende...
::exit1			jmp	DoneWithIO

;*** Zeiger auf Cache-Bereich für BAM-Sektoren.
;--- 1541/1571/1581/Native.
:SetBAMCache1Sek	ldx	curDrive
			lda	driveType -8,x
			and	#%00000111		;Laufwerksmodus isolieren.
			sta	driveTypeCopy

			tay
;			ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo1,y
			bne	SetBAMCacheExit
			lda	#<curDirHead		;Zeiger auf ersten BAM-Sektor.
			ldx	#>curDirHead
			ldy	#$00
:SetBAMCacheInit	sta	r0L
			stx	r0H
			ldx	#$00			;XReg ist bereits $00.
			stx	r1L
			sty	r1H
			stx	r2L
			inx
			stx	r2H
			jsr	SetDiskRamBase
			ldx	#$00
:SetBAMCacheExit	rts

;--- 1571/1581.
:SetBAMCache2Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo2,y
			bne	SetBAMCacheExit
			lda	#<dir2Head		;1571, 1581 oder Native.
			ldx	#>dir2Head		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$01
			bne	SetBAMCacheInit

;--- 1581.
:SetBAMCache3Sek	ldy	driveTypeCopy		;Laufwerkstyp einlesen.
			ldx	BAMCopySekInfo3,y
			bne	SetBAMCacheExit

			cpy	#DrvNative		;NativeMode ?
			bne	:1			; => Nein, Abbruch...
			lda	#<DISK_BASE		; => Native, Variablen testen.
			ldx	#>DISK_BASE
			bne	:2

::1			lda	#<dir3Head		; => 1581, dir3Head testen.
			ldx	#>dir3Head
::2			ldy	#$02
			bne	SetBAMCacheInit

;--- Native.
:SetBAMCache4Sek	lda	#<diskBlkBuf		;1571, 1581 oder Native.
			ldx	#>diskBlkBuf		;Zeiger auf 2ten BAM-Sektor.
			ldy	#$01
			bne	SetBAMCacheInit

;*** Zeiger auf 64K-Speicherbank setzen.
:SetDiskRamBase		ldx	WM_WCODE
			lda	ramBaseWin0,x
			sta	r3L
			rts

;*** Temporäe Kopie von driveType.
:driveTypeCopy		b $00

;*** Tabelle mit BAM-Sektor-info.
;$00 = BAM-Sektor vorhanden.
;$FF = BAM-Sektor nicht vorhanden.
;Type: NoDRV,1541,1571,1581,
;      Native,NoDRV,NoDRV,NoDRV
:BAMCopySekInfo1	b $ff,$00,$00,$00,$00,$ff,$ff,$ff
:BAMCopySekInfo2	b $ff,$ff,$00,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo3	b $ff,$ff,$ff,$00,$ff,$ff,$ff,$ff
:BAMCopySekInfo4	b $ff,$ff,$ff,$ff,$00,$ff,$ff,$ff

;*** Datentabelle mit Max. Track je BAM-Sektor.
:BAMTrackCntNM		b $01,$08,$10,$18,$20,$28,$30,$38
			b $40,$48,$50,$58,$60,$68,$70,$78
			b $80,$88,$90,$98,$a0,$a8,$b0,$b8
			b $c0,$c8,$d0,$d8,$e0,$e8,$f0,$f8

;*** Puffer für Max. Tracks/NativeMode.
:BAMTrackMaxNM		b $00
