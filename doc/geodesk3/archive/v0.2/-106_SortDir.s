﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Dateien im Speicher sortieren.
;    r15 = Anzahl Dateien -1.
;    r14 = Zeiger auf letzten Eintrag im Cache.
;    r13L= 64K Speicherbank für Cache.
;    r12 = Größe Cache-Eintrag 32/64/96Bytes (WORD).
;    r11 = Zeiger auf letzten Verzeichnis-Eintrag im Speicher.
;    r10 = Zeiger auf ersten Verzeichnis-Eintrag im Speicher.
;    r9  = Zeiger auf ersten Eintrag im Cache.
;    r8  = Temporärer Zeiger auf letzten Verzeichnis-Eintrag im Speicher.
;    r7  = Temporärer Zeiger auf letzten Eintrag im Cache.
;    a0  = Vektor auf Sortier-Routine.
:xSORT_ALL_FILES	ldx	WM_WCODE
			ldy	WMODE_SORT,x
			beq	:no_sort

			lda	WM_DATA_MAXENTRY+0
			ldx	WM_DATA_MAXENTRY+1
			bne	:do_sort
			cmp	#$02
			bcs	:do_sort

::no_sort		ldx	#$ff			;Weniger als 2 Dateien, Ende...
			rts

::do_sort		sec				;Zeiger auf letzten Eintrag
			sbc	#$01			;berechnen.
			sta	r15L
			bcs	:1
			dex
::1			stx	r15H

			tya				;Vektor auf Sortier-Routine.
			asl				;In ":a0" ablegen da die restlichen
			tax				;":rX" Adressen evtl. durch ":BMult"
			lda	vecSortMode+0,x		;in ":WM_SET_CACHE_POS" verändert
			sta	a0L			;werden können.
			lda	vecSortMode+1,x
			sta	a0H

			MoveW	r15,r11
			ldx	#r11L			;Zeiger auf letzten Verzeichnis-
			ldy	#r0L			;Eintrag im Speicher berechnen.
			jsr	WM_SET_RAM_POS

			ClrW	r10
			ldx	#r10L			;Zeiger auf ersten Verzeichnis-
			ldy	#r0L			;Eintrag im Speicher berechnen.
			jsr	WM_SET_RAM_POS

			ClrW	r14			;Zeiger auf Verzeichnis-Eintrag
			jsr	WM_SET_CACHE_POS	;im Cache berechnen.
			bcc	:no_cache		; => Kein Cache.
			MoveW	r14,r9

			MoveW	r15,r14			;Zeiger auf Verzeichnis-Eintrag
			jsr	WM_SET_CACHE_POS	;im Cache berechnen.

			lda	r12H			;64K-Speicherbank für Cache.
			ldx	#$00			;Größe Cache-Eintrag als WORD.
			stx	r12H
			b $2c
::no_cache		lda	#$00			;r13L = 0, Kein Cache.
			sta	r13L

::do_compare		MoveW	r11,r8			;Temporärer Zähler auf letzten
							;Verzeichnis-Eintrag für Vergleich.

			lda	r13L			;Cache vorhanden?
			beq	:next_entry		; => Nein, weiter...
			MoveW	r14,r7			;Temporärer Zähler auf letzten
							;Cache-Eintrag für Vergleich.

::next_entry		CmpW	r8,r10			;Aktueller Zähler = temp. Zähler?
			beq	:do_next		; => Ja, weiter...

			lda	a0L			;Einträge vergleichen.
			ldx	a0H
			jsr	CallRoutine

			SubVW	32,r8			;Temporären Zähler Speicher.
			lda	r13L			;Cache vorhanden?
			beq	:2			; => Nein, weiter...
			SubW	r12,r7			;Temporären Zähler Cache.
::2			jmp	:next_entry		;Weiter mit nächstem Vergleich.

::do_next		AddVBW	32,r10			;Zähler auf nächsten Eintrag.

			lda	r13L			;Cache vorhanden?
			beq	:3			; => Nein, weiter...
			AddW	r12,r9			;Zähler auf nächsten Eintrag/Cache.

::3			CmpW	r10,r11			;Ende erreicht?
			bne	:do_compare		; => Nein, weiter...

			ldx	WM_WCODE		;Flag setzen: Verzeichnis sortiert.
			lda	WMODE_SORT,x
			ora	#%10000000
			sta	WMODE_SORT,x

			ldx	#$00			;Dateien sortiert, Ende.
			rts

;*** Einträge vertauschen.
;    r10 = Zeiger auf aktuellen Verzeichnis-Eintrag im Speicher.
;    r8  = Temporärer Zeiger auf letzten Verzeichnis-Eintrag im Speicher.
;    r9  = Zeiger auf aktuellen Eintrag im Cache.
;    r7  = Temporärer Zeiger auf letzten Eintrag im Cache.
;    r13L= 64K Speicherbank für Cache.
;    r12 = Größe Cache-Eintrag 32/64/96Bytes (WORD).
:SwapEntry		ldy	#$1f			;Einträge im Speicher tauschen.
::101			lda	(r10L),y
			tax
			lda	(r8L),y
			sta	(r10L),y
			txa
			sta	(r8L),y
			dey
			bpl	:101

			lda	r13L			;Cache vorhanden?
			beq	:no_cache		; => Nein, weiter...

			LoadW	r0,swapDataBuf		;Zeiger auf Zwischenspeicher.
			MoveW	r9,r1			;Zeiger auf Verzeichnis-Eintrag.
			MoveW	r12,r2			;Größe Eintrag (32/64/96 Bytes).
			MoveB	r13L,r3L		;Zeiger auf Speicherbank.
			jsr	FetchRAM		;Verzeichnis-Eintrag einlesen.

			MoveW	r7,r1			;Zeiger auf Vergleichs-Eintrag und
			jsr	SwapRAM			;mit Verzeichnis-Eintrag tauschen.

			MoveW	r9,r1			;Vergleichs-Eintrag zurück
			jsr	StashRAM		;in Cache speichern.

::no_cache		rts

;*** Einträge vergleichen.
;    r10 = Zeiger auf aktuellen Verzeichnis-Eintrag im Speicher.
;    r8  = Temporärer Zeiger auf letzten Verzeichnis-Eintrag im Speicher.

;*** Modus: Name.
:SortName		ldy	#$05
			lda	(r10L),y		;Zuerst nach Buchstabe a=A
			jsr	:convert_upper		;vergleichen.
			sta	:101 +1
			lda	(r8L),y
			jsr	:convert_upper
::101			cmp	#$ff
			bcc	:106
			beq	:102
			bcs	:109

::102			lda	(r8L),y			;Hier unterscheiden zwischen
			cmp	(r10L),y		;Groß- und Kleinbuchstaben.
			beq	:108
			bcc	:103
			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::103			rts

::104			ldy	#$05			;Zeichen vergleichen.
::105			lda	(r8L),y
			cmp	(r10L),y
			bcs	:107
::106			jmp	SwapEntry		;Eintrag tauschen/sortieren.

::107			bne	:109
::108			iny				;Weitervergleichen bis
			cpy	#$15			;alle 11 Zeichen geprüft.
			bne	:105
::109			rts

::convert_upper		cmp	#$61			;Kleinbuchstaben in
			bcc	:13			;Großbuchstaben wandeln.
			cmp	#$7e			;Sortieren nach Buchstabe a=A,...
			bcs	:13			;Kein Unterschied Groß/Klein.
::12			sub	$20
::13			rts

;*** Modus: Größe.
:SortSize		ldy	#$1f
			lda	(r8L),y
			cmp	(r10L),y
			bcs	:102
::101			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::102			bne	:103
			dey
			lda	(r8L),y
			cmp	(r10L),y
			bcc	:101
			bne	:103
			jmp	SortName		;Größe gleich, nach Name sortieren.
::103			rts

;*** Modus: Datum/Aufwärts.
:SortDateUp		jsr	ConvertDate		;yy/mm/dd nach yyyy/mm/dd wandeln.

			ldx	#$00
::101			lda	dateFile_r8,x
			cmp	dateFile_r10,x
			bcs	:103
::102			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::103			bne	:104
			inx
			cpx	#$06
			bcc	:101
			jmp	SortName		;Datum gleich, nach Name sortieren.
::104			rts

;*** Modus: Datum/Abwärts.
:SortDateDown		jsr	ConvertDate		;yy/mm/dd nach yyyy/mm/dd wandeln.

			ldx	#$00
::101			lda	dateFile_r8,x
			cmp	dateFile_r10,x
			bcs	:103
::102			rts
::103			bne	:104
			inx
			cpx	#$06
			bcc	:101
			jmp	SortName		;Datum gleich, nach Name sortieren.
::104			jmp	SwapEntry		;Eintrag tauschen/sortieren.

;*** Datum von yy/mm/dd nach yyyy/mm/dd konvertieren.
:ConvertDate		ldy	#$19
			ldx	#$01
::1			lda	(r10L),y
			sta	dateFile_r10,x
			lda	(r8L),y
			sta	dateFile_r8,x
			iny
			inx
			cpx	#$06
			bcc	:1

			lda	dateFile_r10 +1
			jsr	:century
			stx	dateFile_r10 +0
			lda	dateFile_r8 +1
			jsr	:century
			stx	dateFile_r8 +0
			rts

;--- Jahrhundert ermitteln.
::century		ldx	#19
			cmp	#80			;Jahr >= 80 => 1980.
			bcs	:99
			ldx	#20			;Jahr <  80 => 2000 - 2079.
::99			rts

;*** Modus: Typ.
:SortTyp		ldy	#$02
			lda	(r10L),y
			and	#%00001111
			sta	:101 +1
			lda	(r8L),y
			and	#%00001111
::101			cmp	#$ff
			beq	:103
			bcs	:102
			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::103			jmp	SortName		;Typ gleich, nach Name sortieren.
::102			rts

;*** Modus: GEOS-Dateityp.
if FALSE
;--- V1: Nur nach GEOS-Dateityp sortieren.
:SortGEOS		ldy	#$18
			lda	(r8L),y
			cmp	(r10L),y
			bcs	:101
			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::101			rts
endif

;--- V2: Nach GEOS/Priorität sortieren.
;Anwendungen zuerst, danach Dokumente.
;Systemdateien am Ende.
:SortGEOS		ldy	#$02
			lda	(r10L),y		;CBM-Dateityp einlesen.
			and	#%00001111
			cmp	#$06			;Typ = Verzeichnis?
			bne	:11			; => Nein, weiter...
			lda	#$ff			;Verzeichnisse an Ende sortieren.
			bne	:12
::11			ldy	#$18
			lda	(r10L),y		;GEOS-Dateityp einlesen und
			jsr	:get_priority		;in GEOS-Priorität konvertieren.
::12			sta	:30 +1

			ldy	#$02
			lda	(r8L),y			;CBM-Dateityp einlesen.
			and	#%00001111
			cmp	#$06			;Typ = Verzeichnis?
			bne	:21			; => Nein, weiter...
			lda	#$ff			;Verzeichnisse an Ende sortieren.
			bne	:30
::21			ldy	#$18
			lda	(r8L),y			;GEOS-Dateityp einlesen und
			jsr	:get_priority		;in GEOS-Priorität konvertieren.

::30			cmp	#$ff
			beq	:31
			bcs	:exit
			jmp	SwapEntry		;Eintrag tauschen/sortieren.
::31			jmp	SortName		;GEOS gleich, nach Name sortieren.

;--- GEOS-Datei nach Priorität sortieren.
::get_priority		cmp	#$10
			bcs	:exit
			tax
			lda	GEOS_Priority,x
::exit			rts

;*** Tabelle mit Zeigern auf die Sortier-Routinen.
:vecSortMode		w $0000				;Kein sortieren.
			w SortName
			w SortSize
			w SortDateUp
			w SortDateDown
			w SortTyp
			w SortGEOS

;*** Zwischenspeicher für Cache/sortieren.
:swapDataBuf		s 32+64

;*** Zwischenspeicher für Datum.
:dateFile_r10		s 07
:dateFile_r8		s 07

;*** Konvertierungstabelle GEOS-Dateityp.
:GEOS_Priority		b $03 ;$00 = nicht GEOS.
			b $04 ;$01 = BASIC-Programm.
			b $05 ;$02 = Assembler-Programm.
			b $07 ;$03 = Datenfile.
			b $0e ;$04 = Systemdatei.
			b $02 ;$05 = Hilfsprogramm.
			b $00 ;$06 = Anwendung.
			b $06 ;$07 = Dokument.
			b $08 ;$08 = Zeichensatz.
			b $09 ;$09 = Druckertreiber.
			b $0a ;$0a = Eingabetreiber.
			b $0c ;$0b = Laufwerkstreiber.
			b $0d ;$0c = Startprogramm.
			b $0f ;$0d = Temporär.
			b $01 ;$0e = Selbstausführend.
			b $0b ;$0f = Eingabetreiber 128.
			b $10 ;$10 = Unbekannt.
