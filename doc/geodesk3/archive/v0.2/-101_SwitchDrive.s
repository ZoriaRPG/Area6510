﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Laufwerk und Diskette öffnen.
;    Übergabe: AKKU = Laufwerk
;    Rückgabe: XREG = $00/Fehler
.SetDev_OpenDsk		jsr	SetDevice		;Laufwerk aktivieren.
			txa				;Laufwerksfehler?
			bne	:1			; => Ja, Abbruch.
			jmp	OpenDisk		;Diskette öffnen.
::1			rts

;*** Partition ermitteln.
;    Übergabe: curDrive = Laufwerk.
;    Rückgabe: $00 oder Partitions-Nr.
.Sys_GetDrv_Part	ldx	curDrive		;Aktuelles Laufwerk einlesen.
			lda	RealDrvMode -8,x	;Laufwerks-Modus einlesen.
			and	#%10000000		;Partitioniertes Laufwerk?
			beq	:1			; => Nein, Ende...
			lda	drivePartData-8,x	;Aktive Partition einlesen.
::1			rts

;*** Verzeichnis ermitteln.
;    Übergabe: curDrive = Laufwerk.
;    Rückgabe: AKKU/XREG = Verzeichnis.
.Sys_GetDrv_SDir	ldx	curDrive		;Aktuelles Laufwerk einlesen.
			lda	RealDrvMode -8,x	;Laufwerks-Modus einlesen.
			and	#%01000000		;NativeMode-Laufwerk?
			tax
			beq	:1			; => Nein, Ende...
			lda	curDirHead +32		;Aktives Verzeichnis einlesen.
			ldx	curDirHead +33
::1			rts

;*** Aktuelles Laufwerk speichern.
.SaveTempDrive		lda	curDrive		;Aktuelles Laufwerk speichern.
;			jmp	Sys_SvTempDrive

;*** Partition setzen.
;    Übergabe: AKKU = Laufwerk.
;    Rückgabe: XREG = $00/Fehler
.Sys_SvTempDrive	jsr	SetDev_OpenDsk		;Laufwerk/Diskette öffnen.
			txa				;Laufwerkfehler?
			bne	:error			; => Ja, Abbruch...

			ldx	curDrive		;Laufwerksmodus speichern.
			lda	RealDrvMode -8,x
			sta	TempMode

			jsr	Sys_GetDrv_Part		;Partition einlesen und
			sta	TempPart		;zwischenspeichern.

			jsr	Sys_GetDrv_SDir		;Verzeichnis einlesen und
			sta	TempSDir +0		;zwischenspeichern.
			stx	TempSDir +1
			ldx	#NO_ERROR
::error			rts

;*** Zurück zum Quell-Laufwerk.
.BackTempDrive		lda	TempDrive
			jsr	SetDevice		;Laufwerk öffnen.
			txa				;Laufwerkfehler?
			bne	:exit			; => Ja, Abbruch...

			bit	TempMode		;Laufwerks-Modus einlesen.
			bmi	:open_part		; => Partitioniertes Laufwerk.
			bvs	:open_sdir		; => NativeMode-Laufwerk.
			jmp	OpenDisk		; => Standard-Laufwerk.

::open_part		lda	TempPart
			sta	r3H
			jsr	OpenPartition		;Partition öffnen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			bit	TempMode		;NativeMode-Laufwerk?
			bvs	:open_sdir		; => Ja, weiter...
::exit			rts

::open_sdir		lda	TempSDir +0
			sta	r1L
			lda	TempSDir +1
			sta	r1H
			jmp	OpenSubDir		;Verzeichnis öffnen.

;*** Startlaufwerk öffnen.
.OpenBootDrive		jsr	FindBootDrive		;Boot-Laufwerk suchen.
			cpx	#NO_ERROR		;Laufwerk gefunden?
			bne	:exit			; => Nein, Abbruch...

			jsr	SetDevice		;Laufwerk öffnen.
			txa				;Laufwerkfehler?
			bne	:exit			; => Ja, Abbruch...

			bit	BootMode		;Laufwerks-Modus einlesen.
			bmi	:open_part		; => Partitioniertes Laufwerk.
			bvs	:open_sdir		; => NativeMode-Laufwerk.
			jmp	OpenDisk		; => Standard-Laufwerk.

::open_part		lda	BootPart
			sta	r3H
			jsr	OpenPartition		;Partition öffnen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			bit	BootMode		;NativeMode-Laufwerk?
			bvs	:open_sdir		; => Ja, weiter...
::exit			rts

::open_sdir		lda	BootSDir +0
			sta	r1L
			lda	BootSDir +1
			sta	r1H
			jmp	OpenSubDir		;Verzeichnis öffnen.

;*** Laufwerk mit AppLink-Konfiguration öffnen.
.OpenLinkDrive		jsr	FindLinkDrive		;AppLink-Laufwerk suchen.
			cpx	#NO_ERROR		;Laufwerk gefunden?
			bne	:exit			; => Nein, Abbruch...

			jsr	SetDevice		;Laufwerk öffnen.
			txa				;Laufwerkfehler?
			bne	:exit			; => Ja, Abbruch...

			bit	LinkMode		;Laufwerks-Modus einlesen.
			bmi	:open_part		; => Partitioniertes Laufwerk.
			bvs	:open_sdir		; => NativeMode-Laufwerk.
			jmp	OpenDisk		; => Standard-Laufwerk.

::open_part		lda	LinkPart
			sta	r3H
			jsr	OpenPartition		;Partition öffnen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			bit	LinkMode		;NativeMode-Laufwerk?
			bvs	:open_sdir		; => Ja, weiter...
::exit			rts

::open_sdir		lda	LinkSDir +0
			sta	r1L
			lda	LinkSDir +1
			sta	r1H
			jmp	OpenSubDir		;Verzeichnis öffnen.

;*** Startlauafwerk einblenden.
.TempBootDrive		jsr	FindBootDrive		;Boot-Laufwerk suchen.
			cpx	#NO_ERROR		;Laufwerk gefunden?
			bne	:error			; => Nein, Abbruch...

			jsr	Sys_SvTempDrive		;Aktuelles Laufwerk speichern.
			txa				;Laufwerkfehler?
			bne	:error			; => Ja, Abbruch...

			ldx	curDrive		;Zusätzlich ramBase sichern.
			lda	ramBase -8,x
			sta	BootRBase
			sta	LinkRBase

			jmp	OpenBootDrive		;Boot-Laufwerk aktivieren.

::error			rts

;*** Aktuelles Laufwerk speichern und
;    Laufwerk mit AppLink-Konfiguration
;    öffnen.
.TempLinkDrive		jsr	FindLinkDrive		;AppLink-Laufwerk suchen.
			cpx	#NO_ERROR		;Laufwerk gefunden?
			bne	:error			; => Nein, Abbruch...

			jsr	Sys_SvTempDrive		;Aktuelles Laufwerk speichern.
			txa				;Laufwerkfehler?
			bne	:error			; => Ja, Abbruch...

			jmp	OpenLinkDrive		;AppLink-Laufwerk öffnen.

::error			rts

;*** Startlaufwerk suchen.
.FindBootDrive		ldx	curDrive
			jsr	:test_drive
			beq	:found

			ldx	#$08
::loop			jsr	:test_drive
			beq	:found
			inx
			cpx	#$0c
			bcc	:loop

			ldx	#DEV_NOT_FOUND
			rts

::found			txa
			ldx	#NO_ERROR
			rts

::test_drive		lda	RealDrvType -8,x
			cmp	BootType
			bne	:failed
			lda	driveType -8,x
			bpl	:ok
			lda	ramBase -8,x
			cmp	BootRBase
			bne	:failed
::ok			lda	#$00
			rts
::failed		lda	#$ff
			rts

;*** AppLink-Laufwerk suchen.
.FindLinkDrive		ldx	curDrive
			jsr	:test_drive
			beq	:found

			ldx	#$08
::loop			jsr	:test_drive
			beq	:found
			inx
			cpx	#$0c
			bcc	:loop

			ldx	#DEV_NOT_FOUND
			rts

::found			txa
			ldx	#NO_ERROR
			rts

::test_drive		lda	RealDrvType -8,x
			cmp	LinkType
			bne	:failed
			lda	driveType -8,x
			bpl	:ok
			lda	ramBase -8,x
			cmp	LinkRBase
			bne	:failed
::ok			lda	#$00
			rts
::failed		lda	#$ff
			rts
