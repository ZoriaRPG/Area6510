﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Taskleiste zeichnen.
:InitTaskBar		lda	#$02
			jsr	SetPattern
			jsr	i_Rectangle
			b	MIN_AREA_BAR_Y,MAX_AREA_BAR_Y
			w	MIN_AREA_BAR_X,MAX_AREA_BAR_X

			lda	C_WinBack
			jsr	DirectColor

			lda	#$00
			jsr	SetPattern
			jsr	i_FrameRectangle
			b	MIN_AREA_BAR_Y    ,MAX_AREA_BAR_Y
			w	MAX_AREA_BAR_X-$2f,MAX_AREA_BAR_X
			b	%11111111
			jsr	i_Rectangle
			b	MIN_AREA_BAR_Y+$01,MAX_AREA_BAR_Y
			w	MAX_AREA_BAR_X-$2e,MAX_AREA_BAR_X-$01
			lda	#$07
			jsr	DirectColor

:ReStartTaskBar		LoadW	appMain,DrawClock

			LoadW	r0,IconTab1
			jmp	DoIcons

;*** Icon-Tabelle für GEOS-Menü.
:IconTab1		b $01
			w $0000
			b $00

			w Icon_GEOS
			b MIN_AREA_BAR_X / 8
			b MIN_AREA_BAR_Y
			b Icon_GEOSx,Icon_GEOSy
			w OPEN_MENU_GEOS

:Icon_GEOS
<MISSING_IMAGE_DATA>
:Icon_GEOSx		= .x
:Icon_GEOSy		= .y

