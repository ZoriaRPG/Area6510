﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** GEOS/Einstellungen - Setup starten.
:MENU_SETUP_EDIT	jsr	UpdateCore
			jmp	MNU_OPEN_EDITOR

;*** GEOS/Einstellungen - Drucker wechseln.
:MENU_SETUP_PRNT	lda	#$08
			jsr	SetDevice
			jsr	SLCT_PRINTER
			txa
			bne	:exit
			jmp	UpdateMyComputer
::exit			rts

;*** GEOS/Einstellungen - Eingabegerät wechseln.
:MENU_SETUP_INPT	lda	#$08
			jsr	SetDevice
			jsr	SLCT_INPUT
			txa
			bne	:exit
			jmp	UpdateMyComputer
::exit			rts

;*** GEOS/Einstellungen - AppLinks speichern.
;Wird direkt über EXIT_MENU_SETUP aufgerufen.
;MENU_SETUP_SALNK	jmp	LNK_SAVE_DATA

;*** GEOS/Einstellungen - Cache/Debug-Modus.
:MENU_SETUP_PLOAD	lda	Flag_PreLoadIcon
			eor	#$ff
			sta	Flag_PreLoadIcon
			rts

;*** GEOS/Einstellungen - Cache/Debug-Modus.
:MENU_SETUP_DEBUG	jsr	WM_CLOSE_ALL_WIN	;Alle Fenster schließen.
			lda	colorModeDebug
			eor	#$ff
			sta	colorModeDebug
			rts

;*** PopUp/Arbeitsplatz - Laufwerk öffnen.
:PF_OPEN_DRV_A		ldx	#$08
			b $2c
:PF_OPEN_DRV_B		ldx	#$09
			b $2c
:PF_OPEN_DRV_C		ldx	#$0a
			b $2c
:PF_OPEN_DRV_D		ldx	#$0b
			lda	driveType -8,x
			beq	:2
			txa
			jsr	SetDevice
			txa
			bne	:2

			ldx	curDrive
			lda	RealDrvMode -8,x
			and	#SET_MODE_SUBDIR
			beq	:1
			jsr	OpenRootDir
			txa
			bne	:2

::1			lda	curDrive
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveNewWin
::2			rts

;*** PopUp Desktop - Titel anzeigen.
:PF_VIEW_ALTITLE	lda	Flag_ViewALTitle
			eor	#$ff
			sta	Flag_ViewALTitle
			jsr	UpdateMenuData
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** PopUp Desktop - Hintergrund anzeigen.
:PF_BACK_SCREEN		lda	sysRAMFlg
			eor	#%00001000
			sta	sysRAMFlg
			ldx	#$00
			and	#%00001000
			beq	:1
			dex
::1			stx	Flag_BackScreen
			jsr	UpdateMenuData
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN		;Fenster aus ScreenBuffer laden.

;*** PopUp DeskTop - AppLinks sperren.
:PF_LOCK_APPLINK	lda	appLinkLocked
			eor	#$ff
			sta	appLinkLocked
			rts

;*** PopUp DeskTop/AppLink - Verzeichnis öffnen.
:PF_OPEN_SDIR		MoveW	AL_VEC_FILE,r14
			jmp	AL_OPEN_SDIR

;*** PopUp Datei/Verzeichnis - Öffnen.
:PF_OPEN_FILE		MoveW	vecDirEntry,r0		;Zeiger auf Verzeichnis-Eintrag.
			jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;*** PopUp Datei/Verzeichnis - Löschen.
:PF_DEL_FILE		MoveW	vecDirEntry,r0
			LoadW	r1,dataFileName
			ldx	#r0L 			;Dateiname aus Verzeichnis-Eintrag
			ldy	#r1L			;in Puffer kopieren.
			jsr	SysCopyFName

			LoadW	r0,dataFileName
			jsr	DeleteFile
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp Fenster/Native - ROOT öffnen.
:PF_OPEN_ROOT		ldx	WM_WCODE
			lda	#$01
			sta	WMODE_SDIR_T,x
			sta	WMODE_SDIR_S,x
			jsr	OpenRootDir
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp Fenster/Native - Verzeichnis zurück.
:PF_OPEN_PARENT		jsr	OpenWinDrive

			lda	curDirHead+34		;Hauptverzeichnis ?
			beq	:1
			sta	r1L
			ldx	WM_WCODE
			sta	WMODE_SDIR_T,x
			lda	curDirHead+35
			sta	r1H
			sta	WMODE_SDIR_S,x
			jsr	OpenSubDir
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.
::1			rts

;*** PopUp Fenster - Neue Ansicht.
:PF_NEW_VIEW		jsr	OpenWinDrive

			jsr	WM_IS_WIN_FREE
			cpx	#NO_ERROR
			bne	:exit

			tay
			ldx	WM_WCODE
			lda	WMODE_VDEL,x
			sta	WMODE_VDEL,y
			lda	WMODE_VICON,x
			sta	WMODE_VICON,y
			lda	WMODE_VSIZE,x
			sta	WMODE_VSIZE,y
			lda	WMODE_VINFO,x
			sta	WMODE_VINFO,y
			lda	WMODE_FILTER,x
			sta	WMODE_FILTER,y
			lda	WMODE_SORT,x
			sta	WMODE_SORT,y

			lda	WMODE_DRIVE,x
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveUsrWin
::exit			rts

;*** PopUp Fenster - Neu laden.
:PF_RELOAD_DISK		jsr	OpenWinDrive
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp Fenster - Partition wechseln.
:PF_OPEN_PART		lda	WM_WCODE
			jsr	WM_WIN2TOP
			jsr	OpenWinDrive

			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox

			ldx	curDrive
			lda	drivePartData -8,x
			ldx	WM_WCODE
			sta	WMODE_PART,x
			lda	curDirHead +32
			sta	WMODE_SDIR_T,x
			lda	curDirHead +33
			sta	WMODE_SDIR_S,x

			jsr	WM_DRAW_NO_TOP		;Fenster aus ScreenBuffer laden.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp Fenster/Anzeige - Gelöschte Dateien anzeigen.
:PF_VIEW_DELFILE	ldy	WM_WCODE
			lda	WMODE_VDEL,y
			eor	#$ff
			sta	WMODE_VDEL,y
			jsr	WM_FLAG_RELOAD		;Flag setzen: "Dateien neu laden".
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** PopUp Fenster/Anzeige - KByte/Blocks.
:PF_VIEW_SIZE		ldy	WM_WCODE
			lda	WMODE_VSIZE,y
			eor	#$ff
			sta	WMODE_VSIZE,y
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp Fenster/Anzeige - Icons anzeigen.
:PF_VIEW_ICONS		ldx	WM_WCODE
			lda	#$00
			sta	WMODE_VINFO,x
			lda	WMODE_VICON,x
			eor	#$ff
			sta	WMODE_VICON,x
			jsr	INIT_WIN_GRID
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp Fenster/Anzeige - Textmodus.
:PF_VIEW_DETAILS	ldx	WM_WCODE
			lda	#$ff
			sta	WMODE_VICON,x
			lda	WMODE_VINFO,x
			eor	#$ff
			sta	WMODE_VINFO,x
			jsr	INIT_WIN_GRID
			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp Fenster/Anzeige - SlowMode.
:PF_VIEW_SLOWMOVE	lda	Flag_SlowScroll
			eor	#$ff
			sta	Flag_SlowScroll
			rts

;*** PopUp Fenster/Anzeige - Icons/Farbe anzeigen.
:PF_VIEW_SETCOLOR	lda	colorMode
			eor	#$ff
			sta	colorMode
			jsr	UpdateMenuData
			jmp	WM_REDRAW_ALL		;Alle Fenster neu zeichnen.

;*** PopUp Fenster - Dateifilter.
:PF_FILTER_ALL		lda	#$00
			b $2c
:PF_FILTER_BASIC	lda	#$80
			b $2c
:PF_FILTER_APPS		lda	#$80 ! APPLICATION
			b $2c
:PF_FILTER_EXEC		lda	#$80 ! AUTO_EXEC
			b $2c
:PF_FILTER_DOCS		lda	#$80 ! APPL_DATA
			b $2c
:PF_FILTER_DA		lda	#$80 ! DESK_ACC
			b $2c
:PF_FILTER_FONT		lda	#$80 ! FONT
			b $2c
:PF_FILTER_PRNT		lda	#$80 ! PRINTER
			b $2c
:PF_FILTER_INPT		lda	#$80 ! INPUT_DEVICE

			ldx	WM_WCODE
			sta	WMODE_FILTER,x		;Neuen Filter-Modus setzen.

			lda	#$00			;Dateien neu einlesen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	WM_FLAG_RELOAD		;Flag setzen: "Dateien neu laden".
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** Dateien sortieren.
:PF_SORT_NAME		lda	#$01
			b $2c
:PF_SORT_SIZE		lda	#$02
			b $2c
:PF_SORT_DATE_OLD	lda	#$03
			b $2c
:PF_SORT_DATE_NEW	lda	#$04
			b $2c
:PF_SORT_TYPE		lda	#$05
			b $2c
:PF_SORT_GEOS		lda	#$06
			ldx	WM_WCODE
			sta	WMODE_SORT,x		;Neuen Sortiermodus setzen.

			lda	#$00			;Zeiger auf Verzeichnis-Anfang.
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			lda	#$7f			;Flag für ":GetFiles" setzen:
			sta	reloadFiles		;"Nur Dateien sortieren".

			;jsr	WM_MOVE_WIN_UP		;Fenster nach oben sortieren.
							;Wird jetzt durch Mausabfrage
							;bereits erledigt.

			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.

;*** Alle Dateien auswählen.
:PF_SELECT_ALL		lda	#$ff
			b $2c

;*** Auswahl aufheben.
:PF_SELECT_NONE		lda	#$00
			sta	r10L			;Markierungsmodus merken.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			bne	:do_select		; => Dateien vorhanden, weiter.
			rts				;Keine Dateien, Ende.

::do_select		lda	#$00
			sta	r15L			;Zeiger auf erste Datei.
			sta	r15H

			sta	r14L			;Zeiger auf erste Datei im Cache.
			sta	r14H

			sta	r11L			;Zeiger auf Datei im Speicher.
			sta	r11H
			ldx	#r11L
			ldy	#r0L
			jsr	WM_SET_RAM_POS		;Zeiger auf Eintrag im Speicher.

			jsr	WM_SET_CACHE_POS	;Zeiger auf Eintrag im Cache.
			bcs	:1			; => Cache aktiv, weiter...

			lda	#$00			;Speicherbank für Cache löschen.
			sta	r3L
			beq	:next_file

::1			LoadW	r0,diskBlkBuf		;Daten für DoRAMOp setzen.
			MoveW	r14,r1
			MoveB	r12L,r2L
			LoadB	r2H,$00
			MoveB	r12H,r3L

::next_file		ldy	#$00
			lda	r10L			;Markierungsmodus in Speicher
			sta	(r11L),y		;schreiben.

			lda	r3L			;Cache aktiv?
			beq	:no_cache		; => Nein, weiter...

			jsr	FetchRAM		;Eintrag aus Cache laden.

			lda	r10L			;Markierungsmodus in Speicher
			sta	diskBlkBuf		;schreiben.

			jsr	StashRAM		;Eintrag in Cache speichern.

			AddW	r2,r1			;Zeiger auf nächsten Eintrag/Cache.

::no_cache		AddVBW	32,r11

			inc	r15L			;Zähler auf nächste Datei.
			bne	:3
			inc	r15H

::3			CmpW	r15,WM_DATA_MAXENTRY
			bne	:next_file		;Nächste Datei markieren.

			jmp	WM_CALL_REDRAW		;Fensterinhalt neu zeichnen.

;*** PopUp Laufwerk - Öffnen.
:PF_OPEN_DRIVE		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveCurWin

;*** PopUp Laufwerk/Fenster - Neues Fenster.
:PF_OPEN_NEWVIEW	lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldx	MyComputerEntry +0
			ldy	MyComputerEntry +1
			jmp	OpenDriveNewWin

;*** PopUp Laufwerk/Fenster - Partition wechseln.
:PF_SWAP_PART		lda	Flag_MyComputer
			jsr	WM_WIN2TOP
			ldy	MyComputerEntry +1
			bne	:1
			lda	MyComputerEntry +0
			cmp	#$04
			bcs	:1
			adc	#$08
			jsr	SetDevice
			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox
			jsr	WM_DRAW_NO_TOP		;Fenster aus ScreenBuffer laden.
			jmp	WM_CALL_DRAW		;Verzeichnis neu laden.
::1			rts

;*** PopUp Laufwerk/Fenster - AppLink erstellen.
:PF_CREATE_AL		lda	curDirHead +34
			ora	curDirHead +35
			bne	:1
			lda	#$01
			ldx	#<Icon_Drive +1
			ldy	#>Icon_Drive +1
			bne	:2

::1			lda	#$03
			ldx	#<Icon_Map +1
			ldy	#>Icon_Map +1

::2			pha

			stx	r4L
			sty	r4H

			LoadB	r3L,1
			jsr	DrawSprite

			pla
			jmp	AL_DnD_DrvSubD

