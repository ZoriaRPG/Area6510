﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:AL_CNT_ENTRY		b $00				;Evtl. durch r13L ersetzen.
:AL_CNT_FILE		b $00
:AL_VEC_FILE		w $0000
:AL_VEC_ICON		w $0000

;*** Link-Datei ausgeben.
:AL_DRAW_FILES		LoadW	r0,FontG3
			jsr	LoadCharSet
			jsr	AL_INIT_TAB
::1			jsr	WM_NO_MARGIN
			jsr	AL_DEF_AREA

			lda	#$00
			jsr	SetPattern
			jsr	Rectangle
			lda	#$07
			jsr	DirectColor

			bit	Flag_ViewALTitle
			bpl	:6

			PushB	r2L
			PushB	r2H
			PushW	r3
			PushW	r4

			ldx	r2H
			inx
			stx	r2L
			txa
			clc
			adc	#$07
			sta	r2H
			SubVW	16,r3
			AddVBW	16,r4
			MoveW	r3,leftMargin
			MoveW	r4,rightMargin

			lda	#$00
			jsr	SetPattern
			jsr	Rectangle
			lda	#$07
			jsr	DirectColor

			PopW	r4
			PopW	r3
			PopB	r2H
			PopB	r2L

::6			lda	r15L
			sta	r0L
			lda	r15H
			sta	r0H

			jsr	WM_CONVERT_PIXEL

			LoadB	r2L,3
			LoadB	r2H,21
			LoadB	r3L,$00
			LoadB	r3H,5

			lda	r14L
			clc
			adc	#< LINK_DATA_COLOR
			sta	r8L
			lda	r14H
			adc	#> LINK_DATA_COLOR
			sta	r8H

			lda	r14L
			clc
			adc	#17
			sta	r4L
			lda	r14H
			adc	#$00
			sta	r4H

			PushB	r1L
			PushB	r1H
			bit	Flag_ViewALTitle
			bpl	:4
			jsr	GD_FICON_NAME
			jmp	:5

::4			jsr	GD_DRAW_FICON

::5			pla
			sta	r1H
			pla
			sta	r11L

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			cmp	#$ff
			bne	:3

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			sec
			sbc	#$08
			jsr	PrntGeosDrvName		;Laufwerk A: bis D: ausgeben.

::3			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15

			dec	AL_CNT_ENTRY
			beq	:2
			jmp	:1
::2			rts

:AL_FIND_ICON		php
			sei

			jsr	AL_INIT_TAB
::1			jsr	AL_DEF_AREA

			jsr	IsMseInRegion
			tax
			bne	:2

			AddVBW	LINK_DATA_BUFSIZE,r14
			AddVBW	64,r15
			inc	r13H

			lda	r13H
			cmp	LinkData
			bne	:1

			plp
			ldx	#FILE_NOT_FOUND
			rts

::2			plp
			ldx	#NO_ERROR
			rts

:AL_INIT_TAB		LoadB	r13H,0
			LoadW	r14,LinkData
			LoadW	r15,Icons

			ldy	#$00
			lda	(r14L),y
			sta	AL_CNT_ENTRY

			AddVBW	1,r14
			rts

:AL_DEF_AREA		ldy	#LINK_DATA_YPOS
			lda	(r14L),y
			asl
			asl
			asl
			sta	r2L
			clc
			adc	#$17
			sta	r2H

			ldy	#LINK_DATA_XPOS
			lda	(r14L),y
			asl
			asl
			asl
			sta	r3L
			lda	#$00
			rol
			sta	r3H

			lda	r3L
			clc
			adc	#$17
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H
			rts

:AL_SET_INIT		lda	LinkData
			sta	r0L
			lda	#$00
			sta	r0H
			lda	#< LINK_DATA_BUFSIZE
			sta	r1L
			lda	#> LINK_DATA_BUFSIZE
			sta	r1H
			ldx	#r0L
			ldy	#r1L
			jsr	DMult

			lda	LinkData
			sta	r1L
			lda	#$00
			sta	r1H
			ldx	#r1L
			ldy	#$06
			jsr	DShiftLeft

			AddVW	LinkData+1,r0
			AddVW	Icons     ,r1
			rts

:AL_SET_FNAME		ldy	#LINK_DATA_FILE
			b $2c
:AL_SET_TITLE		ldy	#LINK_DATA_NAME
			sty	r4L
			ldy	#$00
			sty	r4H

::1			ldy	r4H
			lda	(r3L),y
			beq	:2
			cmp	#$a0
			beq	:2
			ldy	r4L
			sta	(r0L),y

			inc	r4L
			inc	r4H

			lda	r4H
			cmp	#$10
			bcc	:1

::2			ldy	r4L
			ldx	r4H
			lda	#$00
::3			sta	(r0L),y
			iny
			inx
			cpx	#$11
			bcc	:3
			rts

:AL_SET_ICON		ldy	#$3f
::1			lda	spr1pic -1,y
			sta	(r1L),y
			dey
			bne	:1

			lda	#$bf
			sta	(r1L),y
			rts

:AL_SET_COL_DRV		lda	#<Color_Drive
			ldx	#>Color_Drive
			bne	AL_SET_COLOR
:AL_SET_COL_SDIR	lda	#<Color_SDir
			ldx	#>Color_SDir
			bne	AL_SET_COLOR
:AL_SET_COL_PRNT	lda	#<Color_Prnt
			ldx	#>Color_Prnt
			bne	AL_SET_COLOR
:AL_SET_COL_STD		lda	#<Color_Std
			ldx	#>Color_Std
:AL_SET_COLOR		sta	:1 +1
			stx	:1 +2
			ldx	#$00
			ldy	#LINK_DATA_COLOR
::1			lda	$ffff,x
			sta	(r0L),y
			inx
			iny
			cpx	#$09
			bcc	:1
			rts

:AL_SET_TYPFL		lda	#$00
			b $2c
:AL_SET_TYPSD		lda	#$fd
			b $2c
:AL_SET_TYPPR		lda	#$fe
			b $2c
:AL_SET_TYPDV		lda	#$ff
:AL_SET_TYPE		ldy	#LINK_DATA_TYPE
			sta	(r0L),y
			rts

:AL_SET_XYPOS		CmpBI	mouseYPos,MIN_AREA_BAR_Y -$08 -$18
			bcc	:1
			LoadB	mouseYPos,MIN_AREA_BAR_Y -$08 -$18

::1			CmpWI	mouseXPos,$0010
			bcs	:2
			LoadW	mouseXPos,$0010

::2			CmpWI	mouseXPos,SCR_WIDTH_40   -$10 -$18
			bcc	:3
			LoadW	mouseXPos,SCR_WIDTH_40   -$10 -$18

::3			lda	mouseXPos +1
			lsr
			lda	mouseXPos +0
			ror
			lsr
			lsr
			ldy	#LINK_DATA_XPOS
			sta	(r0L),y

			lda	mouseYPos
			lsr
			lsr
			lsr
			ldy	#LINK_DATA_YPOS
			sta	(r0L),y
			rts

:AL_SET_DRIVE		lda	curDrive
			ldy	#LINK_DATA_DRIVE
			sta	(r0L),y
			rts

:AL_SET_RDTYP		ldx	curDrive
			lda	RealDrvType   -8,x
			ldy	#LINK_DATA_DVTYP
			sta	(r0L),y
			rts

:AL_SET_PART		ldx	curDrive
			lda	drivePartData -8,x
			ldy	#LINK_DATA_DPART
			sta	(r0L),y
			rts

:AL_SET_SDIR		ldy	#LINK_DATA_DSDIR
			sta	(r0L),y
			iny
			txa
			sta	(r0L),y
			rts

:AL_SET_SDIRE		pha
			tya
			ldy	#LINK_DATA_ENTRY +2
			sta	(r0L),y			;Zeiger auf Eintrag.
			dey
			txa
			sta	(r0L),y			;Verzeichnis-Eintrag/Sektor.
			dey
			pla
			sta	(r0L),y			;Verzeichnis-Eintrag/Spur.
			rts

:AL_SET_WMODE		lda	#$00
			sta	:1 +1
			ldx	WM_WCODE
			lda	WMODE_VDEL,x
			and	#%10000000
			ora	:1 +1
			sta	:1 +1
			lda	WMODE_VICON,x
			and	#%01000000
			ora	:1 +1
			sta	:1 +1
			lda	WMODE_VSIZE,x
			and	#%00100000
			ora	:1 +1
			sta	:1 +1
			lda	WMODE_VINFO,x
			and	#%00010000
			ora	:1 +1
			sta	:1 +1

			ldy	#LINK_DATA_WMODE
::1			lda	#$ff
			sta	(r0L),y
			rts

:AL_SET_END		inc	LinkData

			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN

;*** AppLink verschieben.
:AL_MOVE_ICON		MoveW	r14,AL_VEC_FILE

			lda	r15L
			clc
			adc	#$01
			sta	r4L
			lda	r15H
			adc	#$00
			sta	r4H

			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:1
			tay
			bne	:1

			MoveW	AL_VEC_FILE,r0
			jsr	AL_SET_XYPOS
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN
::1			rts

;*** AppLink öffnen.
:AL_OPEN_ENTRY		MoveW	r14,AL_VEC_FILE

			ldy	#LINK_DATA_TYPE
			lda	(r14L),y
			beq	AL_OPEN_FILE
			cmp	#$80
			beq	AL_OPEN_MYCOM
			cmp	#$fd
			beq	:1
			cmp	#$fe
			beq	:2
			cmp	#$ff
			beq	:3
			rts

::1			jmp	AL_OPEN_SDIR
::2			jmp	AL_OPEN_PRNT
::3			jmp	AL_OPEN_DRIVE

;*** AppLink öffnen: Datei.
:AL_OPEN_FILE		jsr	AL_SET_DEVICE
			txa
			bne	:1

			MoveW	r14,r6
			jsr	FindFile
			txa
			bne	:1

			LoadW	r0,dirEntryBuf -2
			jmp	StartFile_r0

::1			rts

;*** AppLink öffnen: Arbeitsplatz.
:AL_OPEN_MYCOM		jmp	OpenMyComputer

;*** AppLink öffnen: Drucker.
:AL_OPEN_PRNT		MoveW	AL_VEC_FILE,r14

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			jsr	Sys_SvTempDrive
			txa
			bne	:err

			jsr	AL_SET_DEVICE
			txa
			bne	:err

			MoveW	AL_VEC_FILE,r0
			LoadW	r6,PrntFileName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Druckertreiber einlesen.
			txa
			pha
			jsr	BackTempDrive
			pla
			bne	:err

			jsr	OPEN_PRNT_OK
			jmp	UpdateMyComputer

::err			jmp	OPEN_PRNT_ERR

;*** Eingabegerät wechseln.
:AL_OPEN_INPUT		jsr	SLCT_INPUT
			txa
			bne	:1
			jmp	UpdateMyComputer
::1			rts

;*** AppLink öffnen: Verzeichnis.
:AL_OPEN_SDIR		jsr	AL_SET_DEVICE
			txa
			bne	:2

			ldx	curDrive
			lda	RealDrvMode -8,x
			bpl	:1

			ldy	#LINK_DATA_DPART
			lda	(r14L),y
			sta	r3H
			jsr	OpenPartition

::1			ldy	#LINK_DATA_ENTRY
			lda	(r14L),y
			beq	:2
			sta	r1L
			iny
			lda	(r14L),y
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock
			txa
			bne	:2
			ldy	#LINK_DATA_ENTRY +2
			lda	(r14L),y
			tax
			lda	diskBlkBuf,x
			and	#%00001111
			cmp	#$06
			bne	:2

			ldy	#LINK_DATA_DSDIR
			lda	(r14L),y
			cmp	diskBlkBuf+1,x
			bne	:2
			sta	r1L
			iny
			lda	(r14L),y
			cmp	diskBlkBuf+2,x
			bne	:2
			sta	r1H
			jsr	OpenSubDir

			jsr	WM_IS_WIN_FREE
			tax
			jsr	AL_INIT_WMODES

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveUsrWin
::2			rts

;*** AppLink öffnen: Laufwerk.
:AL_OPEN_DRIVE		jsr	AL_SET_DEVICE
			txa
			bne	:2

			ldx	curDrive
			lda	RealDrvMode -8,x
			bpl	:1

			ldy	#LINK_DATA_DPART
			lda	(r14L),y
			sta	r3H
			jsr	OpenPartition

::1			jsr	WM_IS_WIN_FREE

			tax
			jsr	AL_INIT_WMODES

			ldy	#LINK_DATA_DRIVE
			lda	(r14L),y
			sec
			sbc	#$08
			tax
			ldy	#$00
			jmp	OpenDriveUsrWin
::2			rts

;*** Fenster-optionen initialisieren.
;    Übergabe: XREG = Fenster-Nr.
;              r14  = Zeiger auf AppLink-Daten.
:AL_INIT_WMODES		ldy	#LINK_DATA_WMODE
			lda	(r14L),y
			asl
			pha
			ldy	#$00
			bcc	:1a
			dey
::1a			tya
			sta	WMODE_VDEL,x
			pla

			asl
			pha
			ldy	#$00
			bcc	:1b
			dey
::1b			tya
			sta	WMODE_VICON,x
			pla

			asl
			pha
			ldy	#$00
			bcc	:1c
			dey
::1c			tya
			sta	WMODE_VSIZE,x
			pla

			asl
			ldy	#$00
			bcc	:1d
			dey
::1d			tya
			sta	WMODE_VINFO,x
			rts

;*** AppLink_ Eintrag löschen.
:AL_DEL_ENTRY		ldx	AL_CNT_FILE
			beq	:4
			inx
			cpx	LinkData
			beq	:1

			lda	AL_VEC_FILE +0
			sta	r1L
			clc
			adc	#< LINK_DATA_BUFSIZE
			sta	r0L
			lda	AL_VEC_FILE +1
			sta	r1H
			adc	#> LINK_DATA_BUFSIZE
			sta	r0H

			lda	#<LinkDataEnd
			sec
			sbc	r1L
			sta	r2L
			lda	#>LinkDataEnd
			sbc	r1H
			sta	r2H

			jsr	MoveData

			lda	AL_VEC_ICON +0
			sta	r1L
			clc
			adc	#$40
			sta	r0L
			lda	AL_VEC_ICON +1
			sta	r1H
			adc	#$00
			sta	r0H

			lda	#<IconsEnd
			sec
			sbc	r1L
			sta	r2L
			lda	#>IconsEnd
			sbc	r1H
			sta	r2H

			jsr	MoveData

::1			ldy	#41
			lda	#$00
::2			sta	LinkDataEnd-$80,y
			dey
			bpl	:2

			ldy	#63
::3			sta	IconsEnd-$40,y
			dey
			bpl	:3

			dec	LinkData

			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN
::4			rts

;*** AppLink umbennen.
:AL_RENAME_ENTRY	jsr	LNK_RENAME

			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN

;*** PopUp-Fenster für Laufwerks-Eigenschaften.
:AL_SWAP_PART		MoveW	AL_VEC_FILE,r14
			jsr	AL_SET_DEVICE
			txa
			bne	:1

			LoadW	r0,Dlg_SlctPart
			LoadW	r5,dataFileName
			jsr	DoDlgBox

			jsr	AL_SET_INIT
			ldx	#r3L
			jsr	GetPtrCurDkNm
			MoveW	AL_VEC_FILE,r0
			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE

			ldx	curDrive
			lda	drivePartData -8,x
			ldy	#LINK_DATA_DPART
			sta	(r0L),y
			jsr	MainDrawDesktop
			jmp	WM_DRAW_ALL_WIN
::1			rts

:AL_SWAP_PRINTER	MoveW	AL_VEC_FILE,r14
			jsr	AL_SET_DEVICE
			jsr	SLCT_PRINTER
			txa
			bne	:1
			jmp	UpdateMyComputer
::1			rts
