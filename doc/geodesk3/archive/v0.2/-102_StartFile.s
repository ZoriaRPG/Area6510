﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Anwendung starten auf die ":r0" zeigt.
;    Übergabe: r0 = Zeiger auf 32Byte Verzeichnis-Eintrag.
;        Oder:      Zeiger auf dirEntryBuf -2.
;
;:r0 als Quelle und :a0 als Ziel da die
;Register :r0-:r15 durch UpdateCore und
;VLIR-Routinen verändert werden können.
;
:StartFile_r0		MoveW	r0,a0			;Zeiger auf Eintrag nach :a0.
			jsr	UpdateCore		;Variablen sichern.
			jmp	MNU_OPEN_FILE		;VLIR-Modul "StartFile" aufrufen.

;*** Anwendung/Dokument/DA öffnen.
;    Übergabe: r0 = Zeiger auf 32Byte Verzeichnis-Eintrag.
;        Oder:      Zeiger auf dirEntryBuf -2.
:OpenFile_r0		ldy	#$02
			lda	(r0L),y
			and	#%00001111
			cmp	#$06
			beq	:opensdir

			jsr	CheckFType		;Dateityp auswerten.
			cpx	#NO_ERROR		;Starten möglich?
			beq	:openfile		; => Ja, weiter...
			rts

;--- Datei öffnen.
::openfile		cmp	#PRINTER		;Drucker?
			beq	:openprinter		; => Ja, weiter...
			jmp	StartFile_r0

;--- Verzeichnis öffnen.
::opensdir		iny				;Track/Sektor für
			lda	(r0L),y			;Verzeichnis-Header einlesen.
			sta	r1L
			ldx	WM_WCODE
			sta	WMODE_SDIR_T,x
			iny
			lda	(r0L),y
			sta	r1H
			sta	WMODE_SDIR_S,x
			jsr	OpenSubDir		;Verzeichnis öffnen.
			lda	#$00			;Zeiger auf ersten Eintrag.
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1
			jsr	WM_SAVE_WIN_DATA
			jmp	WM_CALL_DRAW		;Fenster neu laden.

;--- Drucker öffnen.
::openprinter		LoadW	r6,PrntFileName		;Zeiger auf Druckername.
			ldx	#r0L 			;Dateiname aus Verzeichnis-Eintrag
			ldy	#r6L			;in Puffer kopieren.
			jsr	SysCopyFName

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Druckertreiber einlesen.
			txa
			bne	:err

			jsr	OPEN_PRNT_OK

			ldx	Flag_MyComputer
			beq	:exit

			lda	WM_WCODE
			pha
			stx	WM_WCODE
			jsr	WM_CALL_DRAW
			pla
			sta	WM_WCODE
::exit			jmp	WM_CALL_DRAW

::err			jmp	OPEN_PRNT_ERR

;*** Auf gültigen Dateityp testen.
;    Übergabe: r0 = Zeiger auf 32Byte Verzeichnis-Eintrag.
;        Oder:      Zeiger auf dirEntryBuf -2.
;    Rückgabe: XReg = $00/Datei gültig oder #INCOMPATIBLE.
:CheckFType		ldx	#NO_ERROR
			ldy	#$18			;Dateityp auswerten.
			lda	(r0L),y
			beq	:ok			; => BASIC-Programm.
			cmp	#APPLICATION		;Anwendung?
			beq	:ok			; => Ja, weiter...
			cmp	#AUTO_EXEC		;AutoExec?
			beq	:ok			; => Ja, weiter...
			cmp	#APPL_DATA		;Dokument?
			beq	:ok			; => Ja, weiter...
			cmp	#DESK_ACC		;Hilfsmittel?
			beq	:ok			; => Ja, weiter...
			cmp	#PRINTER		;Drucker?
			beq	:ok			; => Ja, weiter...
			ldx	#INCOMPATIBLE
::ok			rts
