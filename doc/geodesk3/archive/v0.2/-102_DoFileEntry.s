﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Einzelnen Datei-Eintrag ausgeben.
;    Aufruf aus Fenster-Manager.
;    Übergabe: r0 = Aktueller Eintrag.
;              r1L/r1H = XPos/YPos.
;              r2L/r2H = MaxX/MaxY.
;              r3L/r3H = GridX/GridY
:GD_PRINT_ENTRY		MoveW	r0,r15			;Zeiger auf Verzeichnis-Eintrag
			ldx	#r15L			;im RAM berechnen.
			ldy	#r9L
			jsr	WM_SET_RAM_POS

			jsr	ReadFName		;Dateiname kopieren.

			ldx	WM_WCODE
			lda	WMODE_VICON,x		;Anzeige-Modus einlesen.
			bne	:text_mode		; => Keine Icons anzeigen.

;--- Icon-Ausgabe.
			lda	r1L			;Für Icon-Anzeige XPos +3 Cards.
			clc
			adc	#$03
			cmp	r2L			;Platz für weiteres Icon?
			bcc	:icon_mode		; => Ja, weiter...
			ldx	#$00			; => Kein Icon ausgegeben.
			rts

::icon_mode		sta	r1L

;--- Farbe für Datei-Icon ermitteln.
			ldy	#$18
			lda	(r15L),y		;GEOS-Dateityp einlesen.
			cmp	#24			;Wert gültig (0-23)?
			bcc	:1			; => Ja, weiter...
			lda	#23			;Unbekannter Dateityp.
::1			tax

;--- Icons in Farbe oder S/W?
			bit	colorMode		;Farb-Modus aktiv?
			bmi	:2			; => Nein, weiter...
			lda	V402y0 +0,x		;Icon-Farbe aus Tabelle einlesen.
			bne	:3

;--- icons in S/W, Debug aktiv?
::2			bit	colorModeDebug
			bpl	:icon_disk

			ldy	#$01
			lda	(r15L),y		;Icon im Cache?
			bne	:icon_disk		; => Nein, weiter...

::icon_cache		lda	colorIconCache
			jmp	:3

::icon_disk		lda	colorIconDisk
::3			ora	C_WinBack		;Mit Hintergrundfarbe verknüpfen.

			pha
			jsr	GetFileIcon_r0		;Datei-Icon einlesen.
			pla
			sta	r3L			;Farbwert speichern.

			LoadB	r2L,$03			;Breite Icon in Cards.
			LoadB	r2H,$15			;Höhe Icon in Pixel.
;			LoadB	r3L,$01			;Farbe für Icon (Bereits gesetzt).
			LoadB	r3H,$04			;DeltyY in Cards für Ausgabe Name.
			LoadW	r4 ,FNameBuf		;Zeiger auf Dateiname.
			jsr	GD_FICON_NAME

			lda	#$ff			;Weitere Einträge in Zeile möglich.
			jmp	:invert_entry		;Ggf. Eintrag invertieren.

;--- Text-Ausgabe.
::text_mode		lda	r1H
			clc
			adc	#$06
			sta	r1H

			ldx	WM_WCODE
			lda	WMODE_VINFO,x
			bne	:info_mode

			lda	r1L			;Platz in Zeile für einen
			clc				;weiteren Dateieintrag?
			adc	r3L
			cmp	r2L
			bcc	:print_entry		; => Ja, weiter...
			beq	:print_entry		; => Ja, weiter...
			ldx	#$00			; => Kein Eintrag ausgeggeben.
			rts

::print_entry		lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			LoadW	r0,FNameBuf
			jsr	PutString
			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			jsr	WM_GET_GRID_X
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$ff			;Weitere Einträge in Zeile möglich.
			jmp	:invert_entry		;Ggf. Eintrag invertieren.

;--- Text-Ausgabe/Details.
::info_mode		lda	r2L
			pha

			lda	r1L
			pha
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r11L
			ldy	#$03
			jsr	DShiftLeft

			PushW	r11
			LoadW	r0,FNameBuf
			jsr	PutString
			PopW	r11

			jsr	DrawDetails		;Datei-Eintrag ausgeben.

			pla
			sta	r1L

			lda	r1H
			sec
			sbc	#$06
			sta	r1H

			pla
			sec
			sbc	r1L
			sta	r2L
			jsr	WM_GET_GRID_Y
			sta	r2H

			lda	#$7f			;Zeilenende erreicht.

;--- Aktueller Eintrag ausgewählt?
;    Wenn ja, dann Eintrag invertieren.
::invert_entry		pha

			ldy	#$00
			lda	(r15L),y
			beq	:53

::52			jsr	WM_CONVERT_CARDS
			CmpW	r4,rightMargin
			bcc	:52a
			MoveW	rightMargin,r4

::52a			CmpB	r2H,windowBottom
			bcc	:52b
			MoveB	windowBottom,r2H

::52b			jsr	InvertRectangle

::53			pla
			tax
			rts

:SortDetails		b $00,$01,$02,$03,$04,$05

;*** Dateiname kopieren.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag.
;    Rückgabe: FNameBuf = Dateiname.
:ReadFName		LoadW	r9,FNameBuf
			ldx	#r15L
			ldy	#r9L
			jmp	SysCopyFName

:FNameBuf		s 17

;*** Zeiger auf Datei-Icon setzen.
;    Übergabe: r0  = Eintrag-Nr.
;    Rückgabe: r0  = Zeiger auf Datei-Icon.
:GetFileIcon_r0		PushB	r1L			;r1L/r1H enthält XPos/YPos.
			PushB	r1H			;Register r1L/r1H sichern.

			MoveW	r0,r14
			jsr	WM_SET_CACHE_POS	;Zeiger auf Cache setzen.
			bcc	:load_from_disk		; => Kein Cache aktiv, weiter...

			LoadW	r0,:dataBuf
			MoveW	r14,r1
			MoveB	r12L,r2L		;r14 = 32/64/96 Bytes. Wird durch
			LoadB	r2H,$00			;":WM_SET_CACHE_POS" definiert.
			MoveB	r12H,r3L		;Speicherbank.
			jsr	FetchRAM		;Cache-Eintrag einlesen.

			;bit	Flag_PreLoadIcon	;Alle Icons in Cache laden?
			;bmi	:test_cache		; => Ja, weiter...
			lda	:dataBuf +1		;Icon bereits im Cache?
			bne	:load_from_disk		; => Nein, Icon von Disk laden.

;--- Verzeichnis-Eintrag und Icon aus Cache.
::test_cache		ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			cmp	#%11000000		;Verzeichnis- und Icon-Cache aktiv?
			bne	:load_from_disk		; => Nein, weiter...

			lda	#<:dataBuf +32		;Zeiger auf Datei-Icon in Puffer.
			ldx	#>:dataBuf +32
			bne	:exit_set_entry

;--- Datei-Icon von Disk/Cache laden.
::load_from_disk	jsr	GetVecFileIcon		;Datei-Icon von Disk laden.

;--- Zeiger auf Datei-Icon setzen, Ende.
::exit_set_entry	sta	r0L			;Zeiger auf Datei-Icon
			stx	r0H			;speichern.

			PopB	r1H			;Register r1L/r1H zurücksetzen.
			PopB	r1L

			rts

;*** Puffer für Eintrag aus Cache.
::dataBuf		s 96

;*** Datei-Icon von Disk/aus Cache einlesen.
;    Übergabe: r12H = Cache/Speicherbank.
;              r13  = Zeiger auf Cache/Icon-Eintrag
;              r14  = Zeiger auf Cache/Verzeichnis-Eintrag.
;              r15  = Zeiger auf RAM/Verzeichnis-Eintrag.
;    Rückgabe: AKKU/XReg  = Zeiger auf Datei-Icon.
:GetVecFileIcon		ldy	#$02
			lda	(r15L),y		;Dateityp = Gelöscht?
			bne	:test_sdir		; => Nein, weiter...

::set_deleted		lda	#<Icon_Deleted		;Icon "Gelöscht".
			ldx	#>Icon_Deleted
			rts

::test_sdir		and	#%00001111
			cmp	#$06			;Dateityp = Verzeichnis?
			bne	:test_geos		; => Nein, weiter...
			lda	#<Icon_Map		;Icon "Verzeichnis".
			ldx	#>Icon_Map
			rts

::set_cbm		lda	#<Icon_CBM		;Icon "CBM".
			ldx	#>Icon_CBM
			rts

::test_geos		ldy	#$15			;Spur/Sektor Infoblock einlesen.
			lda	(r15L),y		;Infoblock definiert?
			beq	:set_cbm		; => Nein, keine GEOS-Datei.
			sta	r1L
			iny
			lda	(r15L),y
			sta	r1H
			LoadW	r4,fileHeader
			jsr	GetBlock		;Info-Block einlesen.
			txa				;Fehler?
			bne	:set_cbm		; => Ja, kein Infoblock => CBM.

			jsr	SaveIcon2Cache		;Icon in Cache speichern.

			lda	#<fileHeader +4		;Zeiger auf Icon in Infoblock.
			ldx	#>fileHeader +4
			rts

;*** Icon in fileHeader in Cache speichern.
;    Übergabe: r12H = Cache/Speicherbank.
;              r13  = Zeiger auf Cache/Icon-Eintrag
;              r14  = Zeiger auf Cache/Verzeichnis-Eintrag.
;              r15  = Zeiger auf Datei-Eintrag/Speicher.
:SaveIcon2Cache		bit	Flag_PreLoadIcon	;PreLoad-Option aktiv?
			bmi	:exit			; => Ja, Ende...
			ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%01000000		;Icon-Cache aktiv?
			bne	:icon_cache		; => Ja, weiter...
::exit			rts				;Ende, Icon bereits im Cache.

::icon_cache		LoadW	r0,fileHeader +4
			MoveW	r13,r1
			LoadW	r2,64
			MoveB	r12H,r3L
			jsr	StashRAM		;Datei-Icon in Cache speichern.

			ldy	#$01			;Kennung "Icon im Cache" in
			lda	#$00			;Verzeichnis-Eintrag setzen.
			sta	(r15L),y		;(Byte#1 = $00)

			MoveW	r15,r0			;Zeiger auf Verzeichnis-Eintrag.
			MoveW	r14,r1			;Verzeichnis-Eintrag im Cache.
			LoadW	r2,2			;Nur Byte #0/1 sichern.
			MoveB	r12H,r3L		;Speicherbank.
			jmp	StashRAM		;Verzeichnis-Eintrag sichern.

;*** Textausgabe/Details ausgeben.
:DrawDetails		AddVBW	$40,r11

			lda	#$00
::1			pha
			tax
			ldy	SortDetails,x
			dey
			bne	:2
			jsr	Detail_Size
::2			dey
			bne	:3
			jsr	Detail_Date
::3			dey
			bne	:4
			jsr	Detail_Time
::4			dey
			bne	:5
			jsr	Detail_GType
::5			dey
			bne	:6
			jsr	Detail_CType

::6			AddVBW	6,r11
			pla
			clc
			adc	#$01
			cmp	#$06
			bcc	:1
			rts

:Detail_Num		lda	r11H
			pha
			lda	r11L
			pha

			tya
			pha

			txa
			jsr	:201
			pha
			txa
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar

			pla
			jsr	SmallPutChar

			pla
			clc
			adc	#11
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

::201			ldx	#$30
::202			cmp	#10
			bcc	:203
			sbc	#10
			inx
			bne	:202
::203			adc	#$30
			rts

;*** Dateigröße ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag,
:Detail_Size		ldy	#$1e
			lda	(r15L),y
			sta	r0L
			iny
			lda	(r15L),y
			sta	r0H

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:1
			lda	r0L
			pha
			ldx	#r0L
			ldy	#$02
			jsr	DShiftRight
			pla
			and	#%00000011
			beq	:1

			inc	r0L
			bne	:1
			inc	r0H

::1			lda	#$20 ! SET_RIGHTJUST ! SET_SUPRESS
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x
			beq	:2
			lda	#"K"
			jsr	SmallPutChar

::2			rts

;*** Dateidatum ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag,
:Detail_Date		ldy	#$1b
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$1a
			lda	(r15L),y
			tax
			ldy	#"."
			jsr	Detail_Num

			ldy	#$19
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

;*** Dateizeit ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag,
:Detail_Time		ldy	#$1c
			lda	(r15L),y
			tax
			ldy	#":"
			jsr	Detail_Num

			ldy	#$1d
			lda	(r15L),y
			tax
			ldy	#" "
			jmp	Detail_Num

;*** GEOS-Dateityp ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag,
:Detail_GType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$02
			lda	(r15L),y		;CBM-Dateityp einlesen.
			and	#%00001111
			cmp	#$06			;Typ = Verzeichnis?
			bne	:0			; => Nein, weiter...
			lda	#24			;GEOS-Dateityp "Verzeichnis".
			bne	:1

::0			ldy	#$18
			lda	(r15L),y		;GEOS-Dateityp einlesen.
			cmp	#24
			bcc	:1
			lda	#23
::1			asl
			tax
			lda	V402q0 +0,x
			sta	r0L
			lda	V402q0 +1,x
			sta	r0H
			jsr	PutString

			pla
			clc
			adc	#$50
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** CBM-Dateityp ausgeben.
;    Übergabe: r15 = Zeiger auf Verzeichnis-Eintrag,
:Detail_CType		lda	r11H
			pha
			lda	r11L
			pha

			ldy	#$02
			lda	(r15L),y
			pha
			and	#%00000111
			asl
			asl
			tay
			lda	V402t0+2,y
			pha
			lda	V402t0+1,y
			pha
			lda	V402t0+0,y
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla
			jsr	SmallPutChar
			pla

			pha
			bmi	:3
			lda	#"*"
			jsr	SmallPutChar
::3			pla
			and	#%01000000
			beq	:4
			lda	#"<"
			jsr	SmallPutChar

::4			pla
			clc
			adc	#$18
			sta	r11L
			pla
			adc	#$00
			sta	r11H
			rts

;*** Texte für Verzeichnis.
:V402q0			w V402r0 ,V402r1 ,V402r2 ,V402r3 ,V402r4
			w V402r5 ,V402r6 ,V402r7 ,V402r8 ,V402r9
			w V402r10,V402r11,V402r12,V402r13,V402r14
			w V402r15,V402r99,V402r17,V402r99,V402r99
			w V402r99,V402r21,V402r22,V402r99,V402r255

:V402q1			w V402s0 ,V402s1 ,V402s2 ,V402s3

:V402r0			b "Nicht GEOS",NULL
:V402r1			b "BASIC",NULL
:V402r2			b "Assembler",NULL
:V402r3			b "Datenfile",NULL
:V402r4			b "System-Datei",NULL
:V402r5			b "DeskAccessory",NULL
:V402r6			b "Anwendung",NULL
:V402r7			b "Dokument",NULL
:V402r8			b "Zeichensatz",NULL
:V402r9			b "Druckertreiber",NULL
:V402r10		b "Eingabetreiber",NULL
:V402r11		b "Laufwerkstreiber",NULL
:V402r12		b "Startprogramm",NULL
:V402r13		b "Temporär",NULL
:V402r14		b "Selbstausführend",NULL
:V402r15		b "Eingabetreiber 128",NULL
:V402r17		b "gateWay-Dokument",NULL
:V402r21		b "geoShell-Kommando",NULL
:V402r22		b "geoFAX Druckertreiber",NULL
:V402r99		b "GEOS ???",NULL
:V402r255		b "Verzeichnis",NULL

:V402r105		b "< 1581 - Partition >",NULL
:V402r106		b "< Unterverzeichnis >",NULL

:V402s0			b "GEOS 40 Zeichen",NULL
:V402s1			b "GEOS 40 & 80 Zeichen",NULL
:V402s2			b "GEOS 64",NULL
:V402s3			b "GEOS 128, 80 Zeichen",NULL

:V402t0			b "DEL SEQ PRG USR REL CBM DIR ??? "
:V402t1			b "Sequentiell",NULL
:V402t2			b "GEOS-VLIR",NULL

;*** Farben für GEOS-Datei-Icons.
:V402y0			b $00
:V402y1			b $00
:V402y2			b $30
:V402y3			b $30
:V402y4			b $20
:V402y5			b $60
:V402y6			b $60
:V402y7			b $30
:V402y8			b $70
:V402y9			b $50
:V402y10		b $50
:V402y11		b $20
:V402y12		b $20
:V402y13		b $00
:V402y14		b $60
:V402y15		b $50
:V402y17		b $60
:V402y21		b $60
:V402y22		b $50
:V402y99		b $70
