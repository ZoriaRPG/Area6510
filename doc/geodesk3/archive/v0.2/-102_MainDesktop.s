﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

:MainInit		jsr	LNK_LOAD_DATA

			jsr	ResetFontGD

			LoadW	r0,WinData_DeskTop
			jsr	WM_COPY_WIN_DATA
			jsr	MainDrawDesktop
			jmp	WM_SAVE_WIN_DATA

:MainReBoot		;jsr	LNK_LOAD_DATA

			jsr	ResetFontGD

			lda	WM_WCODE
			pha

			jsr	MainDrawDesktop

			lda	#MAX_WINDOWS -1
::1			pha
			tax
			lda	WindowStack,x
			beq	:2
			bmi	:2
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA
			jsr	OpenWinDrive
			txa
			bne	:2
::3			jsr	WM_CALL_REDRAW
			jsr	WM_SAVE_WIN_DATA
::2			pla
			sec
			sbc	#$01
			bpl	:1

			pla
			sta	WM_WCODE
			jsr	WM_LOAD_WIN_DATA

			lda	#$ff
			sta	reloadFiles
			lda	WM_DATA_GETFILE +0
			ldx	WM_DATA_GETFILE +1
			jsr	CallRoutine
			inc	reloadFiles
			rts

;*** Desktop zeichnen.
:MainDrawDesktop	jsr	WM_CLEAR_SCREEN

			jsr	AL_DRAW_FILES
			jsr	InitTaskBar
			jsr	InitForWM

			lda	#$00
			sta	WM_WCODE
			jmp	WM_SAVE_SCREEN

;*** Alle Fenster neu zeichnen.
:MainDrawAllWin		jsr	WM_CLEAR_SCREEN

			jsr	AL_DRAW_FILES
			jsr	InitTaskBar
			jsr	InitForWM

			jsr	WM_DRAW_ALL_WIN
			jmp	WM_SAVE_SCREEN
