﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** AppLink für MyComputer/Laufwerk.
:AL_DnD_Drive		lda	#$01

;*** AppLink für Laufwerksfenster als Laufwerk/Verzeichnis.
:AL_DnD_DrvSubD		pha

			lda	curDirHead +32
			sta	r10L
			lda	curDirHead +33
			sta	r10H
			lda	#<curDirHead
			sta	r11L
			lda	#>curDirHead
			sta	r11H

			ldx	#r12L
			jsr	GetPtrCurDkNm

			pla
			ldx	r12L
			ldy	r12H
			jmp	AL_SET_LNKDATA_N

;*** AppLink für MyComputer/Drucker.
:AL_DnD_Printer		lda	#$02
			ldx	#<PrntFileName
			ldy	#>PrntFileName
			jmp	AL_SET_LNKDATA_S

;*** AppLink für Laufwerksfenster/Verzeichnis.
:AL_DnD_SubDir		lda	r1L
			sta	r10L
			lda	r1H
			sta	r10H
			lda	#<diskBlkBuf
			sta	r11L
			lda	#>diskBlkBuf
			sta	r11H

			lda	r0L
			clc
			adc	#$05
			tax
			lda	r0H
			adc	#$00
			tay
			lda	#$03
			jmp	AL_SET_LNKDATA_N

;*** AppLink für Laufwerksfenster/Dateien.
:AL_DnD_Files		lda	r0L
			clc
			adc	#$05
			tax
			lda	r0H
			adc	#$00
			tay
			lda	#$00
			jmp	AL_SET_LNKDATA_S

;*** AppLink für Laufwerksfenster/Drucker.
:AL_DnD_FilePrnt	LoadW	r12,FNameBuf		;AppLink für Drucker erstellen.
			ldx	#r0L
			ldy	#r12L
			jsr	SysCopyFName

			lda	#$02
			ldx	r12L
			ldy	r12H
			jmp	AL_SET_LNKDATA_S

;*** AppLink-Daten schreiben:
;    Drucker oder Dateien.
;    Übergabe: AKKU = $00/File, $01/Drive, $02/Prnt, $03/SubDir
;              X/Y  = Low/High-Zeiger auf Name.
:AL_SET_LNKDATA_S	pha
			lda	#$00
			sta	r10L
			sta	r10H
			sta	r11L
			sta	r11H
			pla

;*** AppLink-Daten schreiben:
;    Laufwerk oder Verzeichnis.
;    Übergabe: AKKU = $00/File, $01/Drive, $02/Prnt, $03/SubDir
;              X/Y  = Low/High-Zeiger auf Name.
;              r10 = Track/Sektor Verzeichnis-Header.
;              r11 = Zeiger Puffer mit dirHead.
:AL_SET_LNKDATA_N	sta	:type
			stx	:fname_vec +0
			sty	:fname_vec +1

			jsr	AL_SET_INIT

			MoveW	:fname_vec,r3

			jsr	AL_SET_FNAME
			jsr	AL_SET_TITLE

			jsr	AL_SET_ICON

			ldy	:type
			lda	:al_set_color_l,y
			ldx	:al_set_color_h,y
			jsr	AL_SET_COLOR

			jsr	AL_SET_XYPOS

			ldy	:type
			lda	:al_set_type,y
			jsr	AL_SET_TYPE

			jsr	AL_SET_DRIVE
			jsr	AL_SET_RDTYP

			ldx	curDrive
			lda	RealDrvMode -8,x
			pha
			and	#SET_MODE_PARTITION
			beq	:no_part
			jsr	AL_SET_PART
::no_part		pla
			and	#SET_MODE_SUBDIR
			beq	:no_subdir

			lda	r10L
			ldx	r10H
			jsr	AL_SET_SDIR

			ldy	#36
			lda	(r11L),y
			pha
			iny
			lda	(r11L),y
			tax
			iny
			lda	(r11L),y
			tay
			pla
			jsr	AL_SET_SDIRE

::no_subdir		ldy	:type
			lda	:al_set_wmode,y
			beq	:end
			jsr	AL_SET_WMODE

::end			jmp	AL_SET_END

;*** Variablen.
::type			b $00
::fname_vec		w $0000

;*** AppLink-Typen.
::al_set_type		b $00,$ff,$fe,$fd

;*** Fenster-Optionen speichern.
::al_set_wmode		b $00,$ff,$00,$ff

;*** Farben für AppLink-Icons.
::al_set_color_l	b <Color_Std
			b <Color_Drive
			b <Color_Prnt
			b <Color_SDir
::al_set_color_h	b >Color_Std
			b >Color_Drive
			b >Color_Prnt
			b >Color_SDir
