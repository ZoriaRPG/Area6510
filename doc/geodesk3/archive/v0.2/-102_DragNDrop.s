﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Icon verschieben.
;    Übergabe: r4 = Zeiger auf Icon-Daten.
;    Rückgabe: XReg = $00/Kein Fehler.
;              AKKU = Fenster-Nr. für Icon-Ablage.
:DRAG_N_DROP_ICON	jsr	InitForIO		;I/O-Bereich einblenden.

			lda	#$00			;Farbe Sprite#1 = Icon.
			sta	$d028
			lda	#$07			;Farbe Sprite#2 = Hintergrund.
			sta	$d029

			jsr	DoneWithIO		;I/O-Bereich ausblenden.

			lda	#$ff			;Hintergrund-Sprite#2 löschen.
			ldy	#$3e 			;Dieses Sprite deckt dann den
::loop			sta	spr2pic,y		;Bildschirm-Hintergrund ab.
			dey
			bpl	:loop

			LoadB	r3L,1			;Icon-Daten in Sprite#1 kopieren.
			jsr	DrawSprite
			jsr	EnablSprite

			LoadB	r3L,2			;Hintergrund-Sprite#2 einschalten.
			jsr	EnablSprite

			LoadB	mouseTop   ,$00
			LoadB	mouseBottom,MIN_AREA_BAR_Y -$08 -$18
			LoadW	mouseLeft  ,$0010
			LoadW	mouseRight ,SCR_WIDTH_40   -$10 -$18

			MoveW	appMain,:appMain_Buf
			LoadW	appMain,:chkMouse

			pla
			sta	:returnAdr_Buf +1
			pla
			sta	:returnAdr_Buf +0
			rts

::appMain_Buf		w $0000
::returnAdr_Buf		w $0000

;--- DnD-Icon mit Mauszeiger synchronisieren.
::chkMouse		MoveW	mouseXPos,r4
			MoveB	mouseYPos,r5L
			LoadB	r3L,1
			jsr	PosSprite
			LoadB	r3L,2
			jsr	PosSprite

			lda	mouseData		;Maustaste gedrückt?
			bmi	:end_dnd		; => Nein, weiter...

			lda	:appMain_Buf +0		;Original-MainLoop fortsetzen.
			ldx	:appMain_Buf +1
			jmp	CallRoutine

;--- Icon wurde abgelegt:
::end_dnd		LoadB	r3L,1			;DnD-Icon abschalten.
			jsr	DisablSprite
			LoadB	r3L,2
			jsr	DisablSprite

			jsr	WM_NO_MOUSE_WIN		;Grenzen für Mauszeiger löschen.

			lda	:appMain_Buf +0		;MainLoop zurücksetzen.
			sta	appMain +0
			lda	:appMain_Buf +1
			sta	appMain +1

			lda	:returnAdr_Buf +0	;Rücksprungadresse setzen.
			pha
			lda	:returnAdr_Buf +1
			pha

			jsr	WM_FIND_WINDOW		;Wurde Icon auf Fenster abgelegt?
			cpx	#NO_ERROR		;DeskTop = Fenster #0!
			bne	:end			; => Nein, Ende.
			lda	WindowStack,y		;Fenster-Nr. einlesen.
::end			rts
