﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Speichern der Systemvariablen, Programm-Modul und
;    Verzeichnisdaten. Wird verwendet bevor man das Modul
;    wechselt oder eine Anwendung startet.
.UpdateCore		jsr	UPDATE_APPVAR		;Variablen speichern.
			jsr	UPDATE_MAINMOD		;Haupt-Modul speichern.
			jsr	UPDATE_CURMOD		;Aktuelles Programm-Modul speichern.

			LoadW	r0,BASE_DIR_DATA	;Aktuelle Verzeichnisdaten sichern.
			LoadW	r1,$e400
			LoadW	r2,$1c00
			jmp	StashRAM

;*** Aktuelles Programm-Modul speichern.
;Sichert u.a. Variablen des Fenstermanagers.
:UPDATE_CURMOD		ldy	curVLIRModule
			b $2c

;*** Haupt-Modul speichern.
:UPDATE_MAINMOD		ldy	#$01
			b $2c

;*** Variablen speichern.
:UPDATE_APPVAR		ldy	#$00

			pha				;AKKU/XReg sichern.
			txa				;Beinhaltet ggf. Programm-Modul
			pha				;und Einsprungadresse.

			tya
			jsr	SetVlirVecRAM		;Sicher u.a. Variablen des
			jsr	StashRAM		;Fenstermanagers.

			pla
			tax
			pla

			rts
