﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   WM_RESET_FONT
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0
;Funktion:  Aktiviert GeoDesk-Zeichensatz.
;******************************************************************************
:WM_RESET_FONT		LoadW	r0,FontG3
			jsr	LoadCharSet
			LoadB	currentMode,$00
			rts

;******************************************************************************
;Routine:   WM_CALL_GETFILES
;Parameter: reloadFlag = $00 => Dateien aus Cache.
;                        $FF => Dateien von Disk einlesen.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Routine "Dateien laden" aufrufen.
;******************************************************************************
:WM_CALL_GETFILES	lda	WM_DATA_GETFILE+0
			ldx	WM_DATA_GETFILE+1
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_CALL_EXEC
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Mausklick in Fenster auswerten.
;******************************************************************************
:WM_CALL_EXEC		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINSLCT +0
			ldx	WM_DATA_WINSLCT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_CALL_EXIT
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster zum schließen vorbereiten.
;******************************************************************************
:WM_CALL_EXIT		jsr	WM_LOAD_WIN_DATA
			lda	WM_DATA_WINEXIT +0
			ldx	WM_DATA_WINEXIT +1
			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_CALL_RIGHTCLK
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Klick mit rechter Maustaste in Fenster auswerten.
;******************************************************************************
:WM_CALL_RIGHTCLK	lda	mouseData		;Mausbutton gedrückt ?
			bpl	WM_CALL_RIGHTCLK	; => Nein, Ende...
			ClrB	pressFlag

			lda	#$3f			;Pause für Maustreiber mit
::1			pha				;Doppelklick auf rechter Taste.
			jsr	UpdateMouse
			pla
			sec
			sbc	#$01
			bpl	:1

::2			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:2			; => Nein, Ende...
			ClrB	pressFlag

			lda	WM_DATA_RIGHTCLK +0
			ldx	WM_DATA_RIGHTCLK +1
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_SET_MAX_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Definiert max. Fenstergröße.
;******************************************************************************
:WM_SET_MAX_WIN		ldy	#$00 +5
			b $2c

;******************************************************************************
;Routine:   WM_SET_STD_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Definiert Standard-Fenstergröße.
;******************************************************************************
:WM_SET_STD_WIN		ldy	#$06 +5
			b $2c

;******************************************************************************
;Routine:   WM_SET_SCR_SIZE
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Definiert Bildschirm/Desktop-Größe.
;******************************************************************************
:WM_SET_SCR_SIZE	ldy	#$0c +5

			ldx	#$00 +5
::1			lda	WM_WIN_SIZE_TAB,y
			sta	r2L,x
			dey
			dex
			bpl	:1
			rts

;*** Fenstergrößen definieren.
:WM_WIN_SIZE_TAB	b $00				;Max. Fenstergröße.
			b MAX_AREA_WIN_Y -1
			w $0000
			w MAX_AREA_WIN_X -1

			b WIN_STD_POS_Y			;Standard-Fenstergröße.
			b WIN_STD_POS_Y + WIN_STD_SIZE_Y -1
			w WIN_STD_POS_X
			w WIN_STD_POS_X + WIN_STD_SIZE_X -1

			b $00				;Bildschirm/Desktop.
			b SCR_HIGHT_40_80 -1
			w $0000
			w SCR_WIDTH_40 -1

;******************************************************************************
;Routine:   WM_DRAW_SLCT_WIN
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet leeres Fenster mit System-Icons.
;******************************************************************************
:WM_DRAW_SLCT_WIN	jsr	WM_GET_SLCT_SIZE	;Fenstergröße einlesen.

			jsr	WM_DRAW_USER_WIN	;Leeres Fenster zeichnen.

			jsr	WM_NO_MARGIN		;Rand für Textausgabe zurücksetzen.

;--- System-Icons zeichnen.
			LoadW	r14,:sysIconTab1
			LoadB	r15H,5
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Resize-Icons zeichnen.
			lda	WM_DATA_SIZE		;Größenänderung zulassen?
			bne	:1			; => Nein, weiter...

			LoadW	r14,:sysIconTab2
			LoadB	r15H,4
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

;--- Move-UP/DOWN-Icons zeichnen.
::1			lda	WM_DATA_MOVEBAR		;Scrollbalken anzeigen?
			beq	:2			; => Nein, weiter...

			LoadW	r14,:sysIconTab3
			LoadB	r15H,2
			MoveB	C_WinTitel,r13H
			jsr	WM_DRAW_ICON_TAB

::2			rts

;*** Angaben zur Ausgabe der Fenster-Icons.
;    w Zeiger auf Icon-Daten.
;    w Routine zum setzen der Icon-Position.
::sysIconTab1		w Icon_CL
			w WM_DEF_AREA_CL

			w Icon_DN
			w WM_DEF_AREA_DN

			w Icon_ST
			w WM_DEF_AREA_STD

			w Icon_MN
			w WM_DEF_AREA_MN

			w Icon_MX
			w WM_DEF_AREA_MX

::sysIconTab2		w Icon_UL
			w WM_DEF_AREA_UL

			w Icon_UR
			w WM_DEF_AREA_UR

			w Icon_DL
			w WM_DEF_AREA_DL

			w Icon_DR
			w WM_DEF_AREA_DR

::sysIconTab3		w Icon_PU
			w WM_DEF_AREA_WUP

			w Icon_PD
			w WM_DEF_AREA_WDN

;******************************************************************************
;Routine:   WM_DRAW_USER_WIN
;Parameter: r2-r4 = Fenstergröße.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Leeres Fenster ohne System-Icons zeichnen.
;******************************************************************************
:WM_DRAW_USER_WIN	PushB	r2L
			PushW	r3

			lda	C_WinBack
			jsr	DirectColor
			lda	#$00
			jsr	SetPattern
			jsr	Rectangle

			lda	r2L
			pha
			lda	r2H
			pha

;--- Kopfzeile zeichnen.
			lda	r2L
			clc
			adc	#$07
			sta	r2H
			lda	C_WinTitel
			jsr	DirectColor

;--- Fußzeile zeichnen.
			pla
			sta	r2H
			sec
			sbc	#$07
			sta	r2L

			lda	C_WinTitel
			jsr	DirectColor

			pla
			sta	r2L

			MoveB	r2H,r11L
			lda	#%11111111
			jsr	HorizontalLine

;--- Linken/recten Rand zeichnen.
			lda	r4H
			pha
			lda	r4L
			pha

			MoveW	r3,r4

			MoveB	r2L,r3L
			MoveB	r2H,r3H
			lda	#%11111111
			jsr	VerticalLine

			pla
			sta	r4L
			pla
			sta	r4H
			lda	#%11111111
			jsr	VerticalLine

			PopW	r3
			PopB	r2L
			rts

;******************************************************************************
;Routine:   WM_DRAW_SLCT_WIN
;Parameter: r14  = Zeiger auf Icon-Tabelle.
;           r15H = Anzahl Icons.
;           r13H = Farbe
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster-Icons darstellen.
;******************************************************************************
:WM_DRAW_ICON_TAB	ldy	#$03
			lda	(r14L),y
			tax
			dey
			lda	(r14L),y
			jsr	CallRoutine

			lda	r13H
			jsr	DirectColor
			jsr	WM_CONVERT_PIXEL

			ldy	#$00
			lda	(r14L),y
			sta	r0L
			iny
			lda	(r14L),y
			sta	r0H
			LoadB	r2L,Icon_MoveW
			LoadB	r2H,Icon_MoveH
			jsr	BitmapUp

			AddVBW	4,r14

			dec	r15H
			bne	WM_DRAW_ICON_TAB
			rts

;******************************************************************************
;Routine:   WM_DRAW_TITLE
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgabe der Titelzeile für aktuelles Fenster.
;******************************************************************************
:WM_DRAW_TITLE		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2L			;X/Y-Koordinaten berechnen.
			clc
			adc	#$06
			sta	r1H

			lda	r3L
			clc
			adc	#$14
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$2c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_TITLE +0	;Titelzeile ausgeben.
			ldx	WM_DATA_TITLE +1
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_DRAW_INFO
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Ausgabe der Infozeile für aktuelles Fenster.
;******************************************************************************
:WM_DRAW_INFO		jsr	WM_NO_MARGIN
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;X/Y-Koordinaten berechnen.
			sec
			sbc	#$02
			sta	r1H

			lda	r3L
			clc
			adc	#$0c
			sta	r11L
			lda	r3H
			adc	#$00
			sta	r11H

			lda	r4L			;Ausgabe für rechten Rand
			sec				;begrenzen.
			sbc	#$0c
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	WM_DATA_INFO +0		;Titelzeile ausgeben.
			ldx	WM_DATA_INFO +1
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_DRAW_MOVER
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet Scrollbalken.
;******************************************************************************
:WM_DRAW_MOVER		lda	WM_DATA_MOVEBAR
			bne	:1
			rts

::1			jsr	WM_DEF_MOVER_DAT
			jsr	InitBalken
			jmp	PrintBalken

;******************************************************************************
;Routine:   WM_DEF_MOVER_DAT
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  r0 = Zeiger auf Datentabelle für Scrollbalken.
;Verändert: A,X,Y,r0-r15
;Funktion:  Definiert Größe/Lage Scrollbalken.
;******************************************************************************
:WM_DEF_MOVER_DAT	jsr	WM_GET_ICON_XY
			jsr	WM_GET_SLCT_SIZE

			ldx	#r4L
			ldy	#$03
			jsr	DShiftRight
			lda	r4L
			sta	:tmp_MovData +0
			lda	r2L
			clc
			adc	#$08
			sta	:tmp_MovData +1
			lda	r2H
			sec
			sbc	r2L
			sec
			sbc	#4*8
			sta	:tmp_MovData +2
			inc	:tmp_MovData +2

			lda	WM_DATA_MAXENTRY +0
			sta	:tmp_MovData +3
			lda	WM_DATA_MAXENTRY +1
			sta	:tmp_MovData +4

			lda	WM_DATA_CURENTRY +0
			sta	:tmp_MovData +7
			lda	WM_DATA_CURENTRY +1
			sta	:tmp_MovData +8

			lda	WM_COUNT_ICON_XY
			sta	:tmp_MovData +5
			lda	#$00
			sta	:tmp_MovData +6

			LoadW	r0,:tmp_MovData
			rts

::tmp_MovData		b $00,$00,$00
			w $0000,$0000,$0000

;******************************************************************************
;Routine:   WM_DRAW_FRAME
;Parameter: r2-r4 = Fenstergröße.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet Gummi-Band.
;           Wird zum verschieben des Fensters und
;           zur Mehrfach-Auswahl verwendet.
;******************************************************************************
:WM_DRAW_FRAME		PushB	r2L
			PushB	r2H
			ldx	r2L
			inx
			stx	r2H
			jsr	InvertRectangle
			PopB	r2H
			tax
			dex
			stx	r2L
			jsr	InvertRectangle
			PopB	r2L

			PushW	r3
			PushW	r4
			MoveW	r3,r4
			jsr	InvertRectangle
			PopW	r4
			MoveW	r4,r3
			jsr	InvertRectangle
			PopW	r3
			rts

;******************************************************************************
;Routine:   WM_SET_CARD_XY
;Parameter: r2L = Y-Koordinate/oben (Pixel)
;           r2H = Y-Koordinate/unten (Pixel)
;           r3  = X-Koordinate/links (Pixel)
;           r4  = X-Koordinate/rechts (Pixel)
;Rückgabe:  r2L = Y-Koordinate/oben (Pixel), abgerundet
;           r2H = Y-Koordinate/unten (Pixel), aufgerundet
;           r3  = X-Koordinate/links (Pixel), abgerundet
;           r4  = X-Koordinate/rechts (Pixel), aufgerundet
;Verändert: A,r2-r4
;Funktion:  Koordinaten auf CARD-Grenzen setzen.
;******************************************************************************
:WM_SET_CARD_XY		lda	r2L			;Y-Koordinate/oben auf Anfang
			and	#%11111000		;des CARDs setzen.
			sta	r2L

			lda	r2H			;Y-Koordinate/unten auf Ende
			ora	#%00000111		;des CARDs setzen.
			sta	r2H

			lda	r3L			;X-Koordinate/links auf Anfang
			and	#%11111000		;des CARDs setzen.
			sta	r3L

			lda	r4L			;X-Koordinate/rechts auf Ende
			ora	#%00000111		;des CARDs setzen.
			sta	r4L
			rts

;******************************************************************************
;Routine:   WM_GET_ICON_X
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  WM_COUNT_ICON_X = Anzahl Einträge in X-Richtung.
;Verändert: A,X,Y,r0,r2-r4
;Funktion:  Berechnet Anzahl Einträge pro Zeile.
;******************************************************************************
:WM_GET_ICON_X		ldx	WM_DATA_COLUMN
			bne	:3

;--- Anzahl Spalten nicht definiert,
;    Anzahl Spalten aus Fensterbreite und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_X
			sta	:CUR_GRID_X

;--- Fensterbreite berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r4L
			sec
			sbc	r3L
			sta	r0L
			lda	r4H
			sbc	r3H
			sta	r0H

			ldx	#r0L
			ldy	#$03
			jsr	DShiftRight

			ldx	r0L			;Max. Anzeigebereich ermitteln.
			inx				;Abzug: 1CARD linker Rand.
			txa				;       1CARD Abstand linker Rand.
			sec				;       1CARD rechter Rand.
			sbc	#$04

;--- Max. Anzahl Icons pro Zeile berechnen.
			ldx	#$00
::2			cmp	#$02
			bcc	:3
			tay
			bmi	:3
			sec
			sbc	:CUR_GRID_X
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_X
			rts

::CUR_GRID_X		b $00

;******************************************************************************
;Routine:   WM_GET_ICON_Y
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  WM_COUNT_ICON_Y = Anzahl Icons in Y-Richtung.
;Verändert: A,X,Y,r0,r2-r4
;Funktion:  Berechnet Anzahl Einträge pro Spalte.
;******************************************************************************
:WM_GET_ICON_Y		ldx	WM_DATA_ROW
			bne	:3

;--- Anzahl Zeilen nicht definiert,
;    Anzahl Zeilen aus Fensterhöhe und Icon-Breite berechnen.
::1			jsr	WM_GET_GRID_Y
			sta	:CUR_GRID_Y

;--- Fensterhöhe berechnen.
			jsr	WM_GET_SLCT_SIZE

			lda	r2H			;Max. Anzeigebereich ermitteln.
			sec				;Abzug: 8Pixel Titelzeile.
			sbc	r2L			;       8Pixel Abstand Titelzeile.
			sec				;       8Pixel Infozeile.
			sbc	#$17

;--- Max. Anzahl Zeilen im Fenster berechnen.
			ldx	#$00
::2			sec
			sbc	:CUR_GRID_Y
			bcc	:3
			inx
			bne	:2
::3			stx	WM_COUNT_ICON_Y
			rts

::CUR_GRID_Y		b $00

;******************************************************************************
;Routine:   WM_GET_ICON_XY
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  WM_COUNT_ICON_XY = Anzahl Einträge in Fenster.
;Verändert: A,X,Y,r0,r2-r4
;Funktion:  Berechnet Anzahl Einträge pro Fenster.
;******************************************************************************
:WM_GET_ICON_XY		jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	#$00
			ldy	WM_COUNT_ICON_Y
			beq	:2
::1			clc
			adc	WM_COUNT_ICON_X
			dey
			bne	:1

::2			sta	WM_COUNT_ICON_XY
			rts

;******************************************************************************
;Routine:   WM_TEST_WIN_POS
;Parameter: WM_WIN_DATA_BUFDaten für aktuelles Fenster.
;Rückgabe:  WM_WIN_DATA_BUF
;Verändert: A,X,Y,r0
;Funktion:  Prüft ob erster Eintrag in linker oberer Ecke innerhalb des
;           gültigen Bereichs liegt.
;******************************************************************************
:WM_TEST_WIN_POS	lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jsr	WM_TEST_CUR_POS
			lda	r0L
			sta	WM_DATA_CURENTRY +0
			lda	r0H
			sta	WM_DATA_CURENTRY +1
			jmp	WM_SAVE_WIN_DATA

;******************************************************************************
;Routine:   WM_TEST_CUR_POS
;Parameter: r0 = Zeiger auf aktuellen Eintrag.
;Rückgabe:  r0 = $0000 => Eintrag ungültig.
;Verändert: A,X,r0
;Funktion:  Prüft ob aktueller Eintrag in linker oberer Ecke innerhalb
;           des gültigen Bereichs liegt (Sub. von "WM_TEST_WIN_POS")
;******************************************************************************
:WM_TEST_CUR_POS	lda	r0L			;Icon-Position für nächste
			clc				;Seite berechen.
			adc	WM_COUNT_ICON_XY
			tax
			lda	r0H
			adc	#$00

			cmp	WM_DATA_MAXENTRY +1
			bne	:1
			cpx	WM_DATA_MAXENTRY +0
::1			beq	:4			;Eintrag innerhalb aktueller Seite?
			bcc	:4			; => Nein, Ende...

			sec
			lda	WM_DATA_MAXENTRY +0
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sbc	#$00			;Zeiger auf letzten Eintrag
			sta	r0H			;berechen.
			bcc	:3

;			lda	r0L
;			clc
;			adc	WM_COUNT_ICON_X
;			tax
;			lda	r0H
;			adc	#$00
;
;			cmp	WM_DATA_MAXENTRY +1
;			bne	:2
;			cpx	WM_DATA_MAXENTRY +0
;::2			bcs	:4
;			stx	r0L			;Zeiger auf Eintrag setzen.
;			sta	r0H
			rts

::3			lda	#$00			;Eintrag ungültig.
			sta	r0L
			sta	r0H
::4			rts

;******************************************************************************
;Routine:   WM_DEF_STD_WSIZE
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y
;Funktion:  Standardgröße für aktuelles Fenster setzen.
;Hinweis:   Bei Fenster 1-6 wird die Position um jeweils 8Pixel
;           nach rechts/unten versetzt.
;           Bei mehr als 6 Fenster bleibt die Position auf Standard.
;******************************************************************************
:WM_DEF_STD_WSIZE	ldx	#$05			;Standard-Fenstergröße setzen.
::0			lda	:stdWinSize,x
			sta	WM_DATA_Y0,x
			dex
			bpl	:0

			ldx	WM_WCODE		;Fenster-Position in Abhängigkeit
			cpx	#$06 +1			;der Fenster-Nr. verschieben.
			bcs	:3			; => Mehr als 6 Fenster, Abbruch.

			ldy	#$00
::1			cpx	#$01			;Fenster-Nr. erreicht?
			beq	:3			; => Ja, Ende...

if FALSE
;--- Hinweis:
;Aktuell sind nicht mehr als 6 Fenster
;möglich. Verschiebung nicht nötig.
			cpy	#$05			;Mehr als 6 Fenster?
			bne	:2			; => Nein, weiter...

			ldy	#$00			;Fenster-Position wieder auf
			SubVB	8*3,WM_DATA_Y0		;Y-Position zurücksetzen.
			SubVB	8*3,WM_DATA_Y1
			;AddVW	8  ,WM_DATA_X0		;X-Position nicht verändern.
			;AddVW	8  ,WM_DATA_X1
endif

::2			AddVB	8  ,WM_DATA_Y0		;Fenster um 1 CARD nach rechts/unten
			AddVB	8  ,WM_DATA_Y1		;verschieben.
			AddVW	8  ,WM_DATA_X0
			AddVW	8  ,WM_DATA_X1

			iny
			dex				;Fenster-Nr. erreicht?
			bne	:1			; => Nein, weiter...

::3			rts

::stdWinSize		b WIN_STD_POS_Y
			b WIN_STD_POS_Y +WIN_STD_SIZE_Y -1
			w WIN_STD_POS_X
			w WIN_STD_POS_X +WIN_STD_SIZE_X -1

;******************************************************************************
;Routine:   WM_SET_WIN_SIZE
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y
;Funktion:  Größe für aktuelles Fenster speichern.
;******************************************************************************
:WM_SET_WIN_SIZE	jsr	WM_SET_CARD_XY		;Fenstergröße auf CARDs runden.

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			ldx	#$05			;Fenstergröße in Tabelle speichern.
::1			lda	r2L,x
			sta	WM_DATA_Y0,x
			dex
			bpl	:1

			jmp	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

;******************************************************************************
;Routine:   WM_DEF_AREA...
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Bereiche für Fenster-Icons berechnen.
;******************************************************************************
:WM_DEF_AREA_CL		lda	#$00
			b $2c
:WM_DEF_AREA_DN		lda	#$03
			b $2c
:WM_DEF_AREA_STD	lda	#$06
			b $2c
:WM_DEF_AREA_MN		lda	#$09
			b $2c
:WM_DEF_AREA_MX		lda	#$0c
			b $2c
:WM_DEF_AREA_UL		lda	#$0f
			b $2c
:WM_DEF_AREA_UR		lda	#$12
			b $2c
:WM_DEF_AREA_DL		lda	#$15
			b $2c
:WM_DEF_AREA_DR		lda	#$18
			b $2c
:WM_DEF_AREA_WUP	lda	#$1b
			b $2c
:WM_DEF_AREA_WDN	lda	#$1e
:WM_DEF_AREA		pha
			jsr	WM_GET_SLCT_SIZE
			pla
			tay

			lda	:WM_DEFICON_TAB +0,y
			bmi	:2

			lda	r3L
			clc
			adc	:WM_DEFICON_TAB +1,y
			sta	r3L
			bcc	:1
			inc	r3H
::1			jmp	:3

::2			lda	r4L
			sec
			sbc	:WM_DEFICON_TAB +1,y
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H

::3			lda	r3L
			clc
			adc	#$07
			sta	r4L
			lda	r3H
			adc	#$00
			sta	r4H

::4			lda	:WM_DEFICON_TAB +0,y
			and	#%01000000
			bne	:5

			lda	r2L
			clc
			adc	:WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

::5			lda	r2H
			sec
			sbc	:WM_DEFICON_TAB +2,y
			sta	r2L
			clc
			adc	#$07
			sta	r2H
			rts

;*** Tabelle für Icon-Bereiche.
;    b $00!$00 = linke  obere  Ecke
;      $80!$00 = rechte obere  Ecke
;      $00!$40 = linke  untere Ecke
;      $80!$40 = rechte untere Ecke
;    b DeltaX
;    b DeltaY
::WM_DEFICON_TAB	b $00!$00,$08,$00		;Close
			b $80!$00,$27,$00		;Sortieren.
			b $80!$00,$1f,$00		;Standard
			b $80!$00,$17,$00		;Minimize
			b $80!$00,$0f,$00		;Maximize
			b $00!$00,$00,$00		;Resize UL
			b $80!$00,$07,$00		;Resize UR
			b $00!$40,$00,$07		;Resize DL
			b $80!$40,$07,$07		;Resize DR
			b $80!$40,$07,$17		;Resize DL
			b $80!$40,$07,$0f		;Resize DL

;******************************************************************************
;Routine:   WM_DEF_AREA_MV
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Bereich für Klick auf Titelzeile berechnen.
;******************************************************************************
:WM_DEF_AREA_MV		jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$07
			sta	r2H

			lda	r3L
			clc
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H

			lda	r4L
			sec
			sbc	#$28
			sta	r4L
			lda	r4H
			sbc	#$00
			sta	r4H
			rts

;******************************************************************************
;Routine:   WM_DEF_AREA_BAR
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Bereich für Klick auf Scrollbalken berechnen.
;******************************************************************************
:WM_DEF_AREA_BAR	jsr	WM_GET_SLCT_SIZE

			lda	r2L
			clc
			adc	#$08
			sta	r2L

			lda	r2H
			sec
			sbc	#$18
			sta	r2H

			lda	r4L
			sec
			sbc	#$07
			sta	r3L
			lda	r4H
			sbc	#$00
			sta	r3H
			rts

;******************************************************************************
;Routine:   WM_CALL_MOVE
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fensterinhalt verschieben.
;******************************************************************************
:WM_CALL_MOVE		lda	WM_DATA_WINMOVE +0
			ldx	WM_DATA_WINMOVE +1
			cmp	#$ff
			bne	:51
			cpx	#$ff
			bne	:51
			jmp	WM_MOVE_ENTRY_I

::51			cmp	#$ee
			bne	:52
			cpx	#$ee
			bne	:52
			jmp	WM_MOVE_ENTRY_T

::52			ldy	WM_WCODE
			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_MOVE_ENTRY_I
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Standard-Routine zum verschieben von Icon-Daten.
;           Wird aufgerufen mit Tabellen-Eintrag ":WM_DATA_MOVE" = $FFFF.
;******************************************************************************
:WM_MOVE_ENTRY_I	jsr	WM_GET_ICON_X
			jsr	WM_GET_ICON_Y

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H

			lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_POS

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	r0L
			ora	r0H
			beq	:2

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:1

			lda	#$00
			sta	r0L
			sta	r0H
::1			jmp	WM_SET_NEW_POS
::2			ldx	#$00
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	r0L
			adc	WM_COUNT_ICON_X
			sta	r0L
			lda	r0H
			adc	#$00
			sta	r0H
			jsr	WM_TEST_CUR_POS

;*** ScrollBalken neu positionieren.
:WM_SET_NEW_POS		lda	r0L
			sta	WM_DATA_CURENTRY +0
			ldx	r0H
			stx	WM_DATA_CURENTRY +1
			jsr	SetPosBalken
			jsr	WM_SAVE_WIN_DATA
			ldx	#$00
			rts

;******************************************************************************
;Routine:   WM_MOVE_ENTRY_T
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Standard-Routine zum verschieben von Text-Daten.
;           Wird aufgerufen mit Tabellen-Eintrag ":WM_DATA_MOVE" = $EEEE.
;******************************************************************************
:WM_MOVE_ENTRY_T	lda	WM_MOVE_MODE
			beq	:MOVE_DATA_UP
			bmi	:MOVE_DATA_DOWN
			jmp	WM_MOVE_ENTRY_I

;--- Nach oben verschieben.
::MOVE_DATA_UP		lda	WM_DATA_CURENTRY +0
			ora	WM_DATA_CURENTRY +1
			bne	:11
			ldx	#$00
			rts

::11			SubVW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	moveTextLastLine
			ldx	#$ff
			rts

;--- Nach unten verschieben.
::MOVE_DATA_DOWN	clc
			lda	WM_DATA_CURENTRY +0
			adc	WM_COUNT_ICON_Y
			tax
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			cmp	WM_DATA_MAXENTRY +1
			bne	:21
			cpx	WM_DATA_MAXENTRY +0
::21			bcc	:22
			ldx	#$00
			rts

::22			AddVBW	1,WM_DATA_CURENTRY
			jsr	WM_SAVE_WIN_DATA
			jsr	moveTextNextLine
			ldx	#$ff
			rts

;******************************************************************************
;Routine:   moveTextNextLine
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Verschiebt Fensterinhalt um eine Zeile nach oben und
;           gibt Inhalt der letzten Zeile neu aus.
;           Dabei wird direkt ein Teil des Grafik-Bildschirms über
;           ":MoveData" verschoben und nur die letzte Zeile neu ausgegeben.
;******************************************************************************
:moveTextNextLine	lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			clc
			adc	#$08
			lsr
			lsr
			lsr
			sta	r1L
			LoadB	r1H,0
			LoadW	r0 ,320
			ldx	#r1L
			ldy	#r0L
			jsr	DMult

			AddW	leftMargin,r1
			AddVW	SCREEN_BASE,r1

			lda	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r1H
			adc	#> 320
			sta	r0H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::31			dec	WM_COUNT_ICON_Y
			beq	:32
			jsr	MoveData

			lda	r0L
			sta	r1L
			clc
			adc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			adc	#> 320
			sta	r0H
			jmp	:31

::32			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			jsr	WM_GET_ICON_Y
			lda	WM_COUNT_ICON_Y
			asl
			asl
			asl
			clc
			adc	windowTop
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			clc
			adc	WM_COUNT_ICON_Y
			tay
			lda	WM_DATA_CURENTRY +1
			adc	#$00
			tax

			tya
			bne	:33
			dex
::33			dey
			sty	r0L
			stx	r0H
			jmp	WM_LINE_OUTPUT

;******************************************************************************
;Routine:   moveTextLastLine
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Verschiebt Fensterinhalt um eine Zeile nach unten und
;           gibt Inhalt der ersten Zeile neu aus.
;           Dabei wird direkt ein Teil des Grafik-Bildschirms über
;           ":MoveData" verschoben und nur die erste Zeile neu ausgegeben.
;******************************************************************************
:moveTextLastLine	lda	WM_WCODE
			jsr	WM_SET_MARGIN

			jsr	WM_GET_ICON_Y

			lda	windowTop
			lsr
			lsr
			lsr
			clc
			adc	WM_COUNT_ICON_Y
			sta	r0L
			dec	r0L
			LoadB	r0H,0
			LoadW	r1 ,320
			ldx	#r0L
			ldy	#r1L
			jsr	DMult

			AddW	leftMargin,r0
			AddVW	SCREEN_BASE,r0

			lda	r0L
			clc
			adc	#< 320
			sta	r1L
			lda	r0H
			adc	#> 320
			sta	r1H

			lda	rightMargin +0
			sec
			sbc	 leftMargin +0
			sta	r2L
			lda	rightMargin +1
			sbc	 leftMargin +1
			sta	r2H

			AddVBW	1,r2

::41			dec	WM_COUNT_ICON_Y
			beq	:42
			jsr	MoveData

			lda	r0L
			sta	r1L
			sec
			sbc	#< 320
			sta	r0L
			lda	r0H
			sta	r1H
			sbc	#> 320
			sta	r0H
			jmp	:41

::42			MoveW	r2,r0
			jsr	ClearRam

			lda	leftMargin +1
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	r1L

			lda	windowTop
			clc
			adc	#$08
			sta	r1H

			lda	WM_DATA_CURENTRY +0
			sta	r0L
			lda	WM_DATA_CURENTRY +1
			sta	r0H
			jmp	WM_LINE_OUTPUT

;******************************************************************************
;Routine:   WM_MOVE_POS
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Scrollbalken verschieben.
;******************************************************************************
:WM_MOVE_POS		CmpW	SB_MaxEntry,SB_MaxEScr
			bcc	:11

			jsr	IsMseOnPos		;Position der Maus ermitteln.
			cmp	#$02			;Auf dem Anzeigebalkens ?
			beq	:MOVE_NEW_POS		; => Ja, Balken verschieben.

			cmp	#$01			;Oberhalb des Anzeigebalkens ?
			beq	:12			; => Ja, eine Seite zurück.
			cmp	#$03			;Unterhalb des Anzeigebalkens ?
			beq	:14			; => Ja, eine Seite vorwärts.

::11			rts				;Oops... Ende.

::12			jmp	:MOVE_LAST_PAGE
::14			jmp	:MOVE_NEXT_PAGE

;--- Balken verschieben.
::MOVE_NEW_POS		jsr	StopMouseMove		;Mausbewegung einschränken.

::MOVE_NEXT		jsr	UpdateMouse		;Mausdaten aktualisieren.

			ldx	mouseData		;Maustaste noch gedrückt ?
			bmi	:21			; => Nein, neue Position anzeigen.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			jsr	UpdateMouse		;Mausdaten aktualisieren.

			lda	inputData		;Wurde Maus bewegt ?
			beq	:MOVE_NEXT		; => Nein, keine Bewegung, Schleife.

			cmp	#$06			;Maus nach unten ?
			beq	:MOVE_DOWN		; => Ja, auswerten.
			cmp	#$02			;Maus nach oben ?
			beq	:MOVE_UP		; => Ja, auswerten.
			bne	:MOVE_NEXT		; => Nein, Schleife...

;--- Balken neu positionieren.
::21			jsr	:MOVE_TO_FILE		;Position in Dateiliste berechnen.
			ClrB	pressFlag		;Maustastenklick löschen.
			jsr	WM_NO_MOUSE_WIN
			ldx	#$00
			rts

;--- Balken nach oben.
::MOVE_UP		lda	SB_Top			;Am oberen Rand ?
			beq	:MOVE_NEXT		; =: Ja, Abbruch...
			dec	mouseTop
			dec	mouseBottom
			dec	SB_Top
			dec	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;--- Balken nach unten.
::MOVE_DOWN		lda	SB_Top
			clc
			adc	SB_Length
			cmp	SB_MaxYlen
			bcs	:MOVE_NEXT
			inc	mouseTop
			inc	mouseBottom
			inc	SB_Top
			inc	SB_End
			jsr	PrintCurBalken		;Neue Balkenposition ausgeben.
			jmp	:MOVE_NEXT		;Schleife...

;--- Mausposition in Listenposition umrechnen.
::MOVE_TO_FILE		lda	SB_Top			;Aktuelle Mausposition in
			sta	r0L			;Position in Tabelle umrechnen.
			lda	#$00
			sta	r0H

			sec
			lda	SB_MaxEntry +0
			sbc	SB_MaxEScr  +0
			sta	r11L
			lda	SB_MaxEntry +1
			sbc	SB_MaxEScr  +1
			sta	r11H

			ldx	#r0L
			ldy	#r11L
			jsr	DMult

			lda	SB_End
			sec
			sbc	SB_Top
			sta	r10L
			inc	r10L
			lda	SB_MaxYlen
			sec
			sbc	r10L
			sta	r11L
			lda	#$00
			sta	r11H
			ldx	#r0L
			ldy	#r11L
			jsr	Ddiv
			jmp	WM_SET_NEW_POS

;--- Eine Seite vorwärts.
::MOVE_NEXT_PAGE	clc
			lda	WM_COUNT_ICON_XY
			adc	r0L
			sta	r0L
			lda	#$00
			adc	r0H
			sta	r0H
			jsr	WM_TEST_CUR_POS
			jmp	:31

;--- Eine Seite zurück.
::MOVE_LAST_PAGE	lda	mouseData		;Mausbutton gedrückt ?
			bpl	:MOVE_LAST_PAGE		; => Nein, Ende...
			ClrB	pressFlag

			sec
			lda	r0L
			sbc	WM_COUNT_ICON_XY
			sta	r0L
			lda	r0H
			sbc	#$00
			sta	r0H
			bcs	:31
			lda	#$00
			sta	r0L
			sta	r0H
::31			jsr	WM_SET_NEW_POS

::32			lda	mouseData		;Mausbutton gedrückt ?
			bpl	:32			; => Nein, Ende...
			ClrB	pressFlag
			ldx	#$00
			rts

;******************************************************************************
;Routine:   WM_LINE_OUTPUT
;Parameter: WM_WCODE = Fenster-Nr.
;           r0  = Zähler für aktuellen Eintrag.
;           r1L = X-Position für Ausgabe.
;           r1H = Y-Position für Ausgabe.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Standard-Routine zum verschieben von Text-Daten.
;           Wird aufgerufen mit Tabellen-Eintrag ":WM_DATA_MOVE" = $EEEE.
;******************************************************************************
:WM_LINE_OUTPUT		lda	r0L
			sta	CurEntry +0
			lda	r0H
			sta	CurEntry +1

			lda	r1L
			sta	CurXPos
			lda	r1H
			sta	CurYPos

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.
			jsr	WM_CALL_GETFILES	;Routine "Dateien laden" aufrufen.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	WM_WIN_MARGIN

			jsr	WM_RESET_FONT		;GeoDesk-Zeichensatz aktivieren.

			jsr	WM_GET_GRID_X
			sta	CurGridX
			jsr	WM_GET_GRID_Y
			sta	CurGridY

			lda	rightMargin +1
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowBottom
			sta	MaxYPos

			lda	#$00
			sta	CountX

::1			lda	CurXPos
			cmp	MaxXPos
			bcc	:3
			rts

::3			sta	r1L
			lda	CurYPos
			sta	r1H

			lda	MaxXPos
			sta	r2L
			lda	MaxYPos
			sta	r2H

			lda	CurGridX
			sta	r3L
			lda	CurGridY
			sta	r3H

			lda	CurEntry +0
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine
			txa
			beq	:4

			inc	CurEntry +0
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:6

			cpx	#$7f
			beq	:6

			lda	CurXPos
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX

			lda	WM_DATA_COLUMN
			beq	:5
			lda	CountX
			cmp	WM_DATA_COLUMN
			bcc	:6
::5			jmp	:1

::6			rts

;******************************************************************************
;Routine:   WM_CALL_DRAWROUT
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Daten für Fenster ausgeben.
;           $0000 = Keine Daten ausgeben.
;           $FFFF = Standard-Ausgabe, z.B. Dateien.
;******************************************************************************
:WM_CALL_DRAWROUT	lda	WM_DATA_WINPRNT +0
			ldx	WM_DATA_WINPRNT +1
			ldy	WM_WCODE
			cmp	#$ff
			bne	:1
			cpx	#$ff
			beq	WM_STD_OUTPUT
::1			jmp	CallRoutine

;******************************************************************************
;Routine:   WM_STD_OUTPUT
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Daten für Fenster ausgeben.
;******************************************************************************
:WM_STD_OUTPUT		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_CALL_GETFILES	;Routine "Dateien laden" aufrufen.

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	WM_RESET_FONT		;GeoDesk-Zeichensatz aktivieren.

			jsr	InitFPosData		;Daten für Ausgabe initialisieren.

			lda	WM_DATA_MAXENTRY +0
			ora	WM_DATA_MAXENTRY +1
			beq	:end_output		;Daten vorhanden? => Nein, Ende...

::next_column		lda	CurXPos			;Aktuelle X-Position innerhalb
			cmp	MaxXPos			;des gültigen Bereichs?
			bcc	:3			; => Ja, weiter...

::reset_column		jsr	restColumnData		;Spaltendaten zurücksetzen.
			bcc	:next_row		; => Nächste Zeile, weiter...
::end_output		rts				;Ende...

::next_row		sta	CurYPos			;Neue Y-Position speichern.
			inc	CountY			;Zähler für Zeilen korrigieren.

			ldx	WM_DATA_ROW		;Anzahl Zeilen begrenzt?
			beq	:2b			; => Nein, weiter...
			cmp	WM_DATA_ROW		;Max. Anzahl Zeilen erreicht?
			bcs	:end_output		; => Ja, Ende...

::2b			lda	CurXPos
::3			jsr	setEntryData		;Daten für aktuellen Eintrag setzen.

			lda	CurEntry +0		;Zeiger auf aktuellen Eintrag.
			sta	r0L
			lda	CurEntry +1
			sta	r0H

			lda	WM_DATA_PRNFILE +0
			ldx	WM_DATA_PRNFILE +1
			jsr	CallRoutine		;Nächsten Eintrag ausgeben.
			txa				;Wurde Eintrag ausgegeben?
			beq	:4			; => Nein, weiter...

			inc	CurEntry +0		;Zähler auf nächsten Eintrag.
			bne	:4
			inc	CurEntry +1

::4			CmpW	CurEntry,WM_DATA_MAXENTRY
			bcs	:end_output		; => Weitere Einträge ausgeben.

			cpx	#$7f			;War Eintrag im sichtbaren Bereich?
			beq	:5a			; => Nein, Eintrag in nächster
							;    Zeile darstellen.

			jsr	setNextColumn		;X-Position für nächsten Eintrag.

			lda	WM_DATA_COLUMN		;Max. Anzahl Spalten definiert?
			beq	:5			; => Nein, weiter...
			lda	CountX
			cmp	WM_DATA_COLUMN		;Max. Anzahl Spalten erreicht?
			bcc	:5a			; => Nein, weiter...

::5			jmp	:next_column		;Nächste Spalte.
::5a			jmp	:reset_column		;Nächste Zeile.

;*** Spaltenwerte zurücksetzen.
;    Rückgabe: C-Flag = 1, keine weitere Zeile möglich.
;    Wird auch von von Dateiauswahl verwendet.
:restColumnData		lda	#$00			;Zähler zurücksetzen.
			sta	CountX

			lda	MinXPos			;X-Position auf Anfang.
			sta	CurXPos

			lda	CurYPos			;Zeiger auf nächste Zeile.
			clc
			adc	CurGridY		;Neue Y-Position innerhalb
			cmp	MaxYPos			;des gültigen Bereichs?
			rts

;*** Datn für Eintrag setzen.
;    Übergabe: AKKU = Aktuelle X-Position.
;    Wird auch von von Dateiauswahl verwendet.
:setEntryData		sta	r1L			;Aktuelle X-Position setzen.
			lda	CurYPos
			sta	r1H			;Aktuelle Y-Position setzen.

			lda	MaxXPos			;Max. X-Position setzen.
			sta	r2L
			lda	MaxYPos			;Max. Y-Position setzen.
			sta	r2H

			lda	CurGridX		;X-Abstand setzen.
			sta	r3L
			lda	CurGridY		;Y-Abstand setzen.
			sta	r3H
			rts

;*** Zeiger auf nächste Spalte.
;    Wird auch von von Dateiauswahl verwendet.
:setNextColumn		lda	CurXPos			;X-Position für nächsten Eintrag.
			clc
			adc	CurGridX
			sta	CurXPos

			inc	CountX			;Zähler für Spalte korrigieren.
			rts

;******************************************************************************
;Routine:   InitFPosData
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Daten für Fenster ausgeben.
;******************************************************************************
:InitFPosData		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_WIN_MARGIN		;Grenzen für Textausgabe setzen.

			jsr	WM_GET_GRID_X		;Spaltenabstand berechnen.
			sta	CurGridX
			jsr	WM_GET_GRID_Y		;Zeilenabstand berechnen.
			sta	CurGridY

			lda	leftMargin +1		;Anfang für X-Position berechnen.
			lsr
			lda	leftMargin +0
			ror
			lsr
			lsr
			sta	MinXPos
			sta	CurXPos

			lda	rightMargin +1		;Max. X-Position berechnen.
			lsr
			lda	rightMargin +0
			ror
			lsr
			lsr
			sta	MaxXPos
			inc	MaxXPos

			lda	windowTop		;Anfang für Y-Position berechnen.
			clc
			adc	#$08
			sta	MinYPos
			sta	CurYPos

			lda	windowBottom		;Max. Y-Position berechnen.
			sta	MaxYPos

			lda	WM_DATA_CURENTRY +0
			sta	CurEntry +0
			lda	WM_DATA_CURENTRY +1
			sta	CurEntry +1		;Zeiger auf aktuellen Eintrag.

			lda	#$00			;Spalten-/Zeilenzähler löschen.
			sta	CountX
			sta	CountY
			rts
