﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* Datei öffnen.
;* Editor öffnen.
;* Drucker wechseln.
;* Eingabegerät wechseln.
;* Dialog: Fehler Drucker-Installation.
;* Dialog: Drucker-Installation OK.
;* Anwendung wählen.
;* AutoExec wählen.
;* Dokumente wählen.
;* geoWrite-Dokument wählen.
;* geoPaint-Dokument wählen.
;* Hintergrundbild wählen.
;* Nach GEOS beenden.
;* Nach BASIC beenden.
;* BASIC-Programm starten.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopMac.MP3"
			t "TopSym.GD"
			t "s.mod.#101.ext"
endif

			n "mod.#105.obj"
			t "-SYS_CLASS.h"
			f DATA
			o VLIR_BASE
			p MainInit
			a "Markus Kanet"

:VlirJumpTable		jmp	StartFile_a0
			jmp	OpenEditor
			jmp	ChangePrinter
			jmp	ChangeInput
			jmp	OpenPrntError
			jmp	OpenPrntOK
			jmp	SelectAppl
			jmp	SelectAuto
			jmp	SelectDocument
			jmp	SelectDocWrite
			jmp	SelectDocPaint
			jmp	SelectBackScrn
			jmp	ExitGEOS
			jmp	ExitBASIC
			jmp	ExitBAppl

;*** Speicherverwaltung.
			t "-SYS_RAM"

;*** Nach GEOS beenden.
:ExitGEOS		jsr	WM_CLOSE_ALL_WIN	;Alle Fenster schließen.

			jsr	SetADDR_EnterDT		;Original EnterDeskTop-Routine
			lda	#$00			;aus dem Speicher holen und
			sta	r1L			;wieder im System installieren.
			sta	r1H
			lda	GD_RAM_GDESK
			sta	r3L
			jsr	FetchRAM
			jsr	SetADDR_EnterDT
			jsr	StashRAM

			ldy	GD_SYSTEM_BUF		;Reservierten Speicher freiegeben.
			jsr	FreeBank
			ldy	GD_SCRN_STACK
			jsr	FreeBank
			ldy	GD_RAM_GDESK
			jsr	FreeBank

			jmp	EnterDeskTop		;Zurück zu GEOS.

;*** Nach BASIC beenden.
:NoBASIC		b NULL
:ExitBASIC		LoadW	r0,Dlg_ExitBasic
			jsr	DoDlgBox

			lda	sysDBData		;GEOS wirklich beenden?
			cmp	#YES			;"Ja" ?
			beq	:exit			; => Nein, Abbruch...
			jmp	MNU_REBOOT		;Zurück zum Desktop.

::exit			LoadW	r0,NoBASIC

;*** Nach BASIC verlassen und Befehl ausführen.
;    Übergabe: r0 = Zeiger auf Befehl.
:ExitBASIC_NoLoad	lda	#$00			;Kein Programm laden.
			sta	r5L
			sta	r5H

			sta	$0800			;Kein Programm starten.
			sta	$0801
			sta	$0802
			sta	$0803
			LoadW	r7,$0803

			jmp	ToBasic			;Nach BASIC beenden.

;*** Dialogboxen.
:Dlg_ExitBasic		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2c
			w :3
			b YES        ,$01,$50
			b CANCEL     ,$11,$50
			b NULL

::2			b PLAINTEXT
			b "GEOS wirklich beenden und",NULL
::3			b "den BASIC-Modus starten?",NULL

;*** BASIC-Programm starten.
:ExitBAppl		lda	#NOT_GEOS		;Dateityp "BASIC/Nicht GEOS".
			sta	r7L

			lda	#$00			;Keine GEOS-Klasse.
			sta	r10L
			sta	r10H

			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	:openfile
::exit			jmp	MNU_REBOOT		;Zurück zum Desktop.

::openfile		LoadW	r6,dataFileName
:ExitBApplRUN		jsr	FindFile		;Datei suchen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

;--- Ladeadresse prüfen.
			lda	dirEntryBuf+1		;Zeiger auf ersten Datenblock.
			sta	r1L
			lda	dirEntryBuf+2
			sta	r1H
			LoadW	r4,diskBlkBuf		;Zeiger auf Zwischenspeicher.
			jsr	GetBlock		;Ersten Datenblock einlesen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			lda	diskBlkBuf+2		;Laeadresse = $0801?
			cmp	#$01
			bne	:ask_load_abs		; => Nein, Absolut laden?
			lda	diskBlkBuf+3
			cmp	#$08
			beq	:load_std		; => Ja, weiter...

::ask_load_abs		ldx	curDrive		;Bei RAM-Laufwerk kein absolutes
			lda	RealDrvType -8,x	;laden mit ",dev,1" möglich.
			bpl	:0			; => Kein RAM-Laufwerk, weiter.

			LoadW	r0,Dlg_LoadAbsRAM	;Fragen ob von RAM normal geladen
			jsr	DoDlgBox		;werden soll...

			lda	sysDBData
			cmp	#YES			;"Ja" ?
			beq	:load_std		; => Normal laden.
			bne	:cancel			;Abbruch.

::0			LoadW	r0,Dlg_LoadAbs		;Fragen ob Absolut geladen
			jsr	DoDlgBox		;werden soll...

			lda	sysDBData
			cmp	#NO
			beq	:load_std		; => Nein, normal laden/starten.
			cmp	#YES
			beq	:load_abs		; => Ja, absolut laden.
::cancel		jmp	MNU_REBOOT		; => Abbruch, Zurück zum Desktop.

;--- Programm normal laden/starten.
::load_std		LoadW	r0,:RunBASIC		;"RUN"-Befehl.
			LoadW	r5,dirEntryBuf		;Zeiger auf Verzeichnis-Eintrag.
			LoadW	r7,$0801		;Ladeadresse.

			jmp	ToBasic			;Nach BASIC beenden.
::exit			jmp	OpenFileError

;--- Programm absolut laden/manuell starten.
::load_abs		LoadW	r0,dirEntryBuf -2	;Dateiname in LOAD-Befehl kopieren.
			LoadW	r1,:FileNameBuf
			ldx	#r0L
			ldy	#r1L
			jsr	SysCopyFName

			ldy	#$00
::2			lda	(r1L),y			;Ende Dateiname suchen.
			beq	:3
			iny
			cpy	#$10
			bne	:2

::3			lda	#$22			;",dev,1" an den Dateinamen
			sta	(r1L),y			;anhängen.
			iny
			lda	#$2c			;","
			sta	(r1L),y
			iny

			ldx	curDrive		;Laufwerk 8,9,10,11 in
			lda	:driveAdr1 -8,x		;Befehl eintragen.
			beq	:4
			sta	(r1L),y
			iny
::4			lda	:driveAdr2 -8,x
			sta	(r1L),y
			iny

			lda	#$2c			;","
			sta	(r1L),y
			iny
			lda	#"1"			;"1"
			sta	(r1L),y
			iny
			lda	#NULL			;Befehlsende.
			sta	(r1L),y

			LoadW	r0,:LoadBASIC		;"LOAD"-Befehl.
			jmp	ExitBASIC_NoLoad	;Nach BASIC beenden.

::RunBASIC		b "RUN",NULL
::LoadBASIC		b "LOAD",$22
::FileNameBuf		s 17
			b $22,",8,1",NULL
::driveAdr1		b NULL,NULL,"1","1"
::driveAdr2		b "8" ,"9" ,"0","1"

;*** Dialogboxen.
:Dlg_LoadAbs		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2c
			w :3
			b DBTXTSTR   ,$0c,$3c
			w :4
			b YES        ,$01,$50
			b NO         ,$08,$50
			b CANCEL     ,$11,$50
			b NULL

::2			b PLAINTEXT
			b "Das Programm verwendet nicht",NULL
::3			b "die Standard-Ladeadresse.",NULL
::4			b "Absolut mit `,x,1` laden?",NULL

;*** Dialogboxen.
:Dlg_LoadAbsRAM		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$1c
			w :2
			b DBTXTSTR   ,$0c,$28
			w :3
			b DBTXTSTR   ,$0c,$38
			w :4
			b DBTXTSTR   ,$0c,$44
			w :5
			b YES        ,$01,$50
			b CANCEL     ,$11,$50
			b NULL

::2			b PLAINTEXT
			b "Das Programm verwendet nicht",NULL
::3			b "die Standard-Ladeadresse.",NULL
::4			b "Absolutes Laden nicht unterstützt.",NULL
::5			b "Von RAM-Laufwerk laden/starten?",NULL

;*** Anwendung starten auf die ":a0" zeigt.
:StartFile_a0		LoadW	r1,AppFName
			ldx	#a0L 			;Dateiname aus Verzeichnis-Eintrag
			ldy	#r1L			;in Puffer kopieren.
			jsr	SysCopyFName

			ldy	#$18			;Dateityp auswerten.
			lda	(a0L),y
			beq	:start_basic		; => BASIC-Programm.
			cmp	#APPLICATION		;Anwendung?
			beq	:start_appl		; => Ja, weiter...
			cmp	#AUTO_EXEC		;AutoExec?
			beq	:start_appl		; => Ja, weiter...
			cmp	#APPL_DATA		;Dokument?
			beq	:start_doc		; => Ja, weiter...
			cmp	#DESK_ACC		;Hilfsmittel?
			beq	:start_appl		; => Ja, weiter...
			jmp	OpenFileError		; => Keine gültige Datei.

::start_appl		jmp	OpenAppl
::start_doc		jmp	OpenDoc
::start_auto		jmp	OpenAppl
::start_da		jmp	OpenDA
::start_basic		jmp	OpenBASIC

;*** Allgemeiner Dateifehler.
:OpenFileError		lda	#<Dlg_ErrOpen1
			ldx	#>Dlg_ErrOpen1
			bne	PrntErrorExit

;*** Nicht mit GEOS64 kompatibel.
:OpenG64Error		lda	#<Dlg_ErrOpen2
			ldx	#>Dlg_ErrOpen2

;*** Fehlermeldung ausgebenund zurück zum DeskTop.
:PrntErrorExit		sta	r0L
			stx	r0H
			jsr	DoDlgBox		;Fehler ausgeben.
			jmp	MNU_REBOOT		;Zurück zum Desktop.

;*** Datei laden vorbereiten.
:PrepGetFile		jsr	i_FillRam		;dlgBoxRamBuf löschen.
			w	417
			w	dlgBoxRamBuf
			b	$00

			jsr	UseSystemFont		;Standardzeichensatz.
			jsr	ResetScreen		;Bildschirm löschen.
			;jsr	WM_RESET_SCREEN

			ldx	#r15H			;ZeroPage löschen.
			lda	#$00
::loop			sta	$0000,x
			dex
			cpx	#r0L
			bcs	:loop
			rts

;*** Name der Anwendung die gestartet werden soll.
:AppFName		s 17
:DocFName		s 17
:AppClass		s 17
:DocDName		s 17
:ApplDrives		s $04

;*** Dialogboxen.
:Dlg_ErrOpen1		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk konnte die Datei"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "nicht öffnen!",NULL

;*** Dialogboxen.
:Dlg_ErrOpen2		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "Kann die Datei nicht öffnen!"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "Nicht mit GEOS64 kompatibel.",NULL

;*** Anwendung oder AutoExec starten.
:OpenAppl		jsr	ChkFlag_40_80		;40/80Z-Flag testen.
			txa				;Fehler?
			bne	:error_40_80		; => Ja, Abbruch...

			sei				;GEOS-Reset #0.
			cld
			ldx	#$ff
			txa

			jsr	GEOS_InitSystem		;GEOS-Reset #1.
			jsr	PrepGetFile

			lda	#>EnterDeskTop -1	;Bei Fehler zurück zum
			pha				;DeskTop.
			lda	#<EnterDeskTop -1
			pha

;			LoadB	r0L,$00			;Wird durch ":PrepGetFile" gelöscht.
			LoadW	r6,AppFName
			jmp	GetFile			;Datei laden/starten.
::error_40_80		jmp	OpenG64Error

;*** Dokument starten.
:OpenDoc		ldx	#r14L			;Zeiger auf Aktuelle Diskette.
			jsr	GetPtrCurDkNm

			ldy	#15
::0			lda	(r14L),y		;Diskname kopieren.
			sta	DocDName,y
			lda	AppFName,y		;Dateiname ist ein Dokument.
			sta	DocFName,y		;Name kopieren.
			dey
			bpl	:0

			jsr	LoadDokInfos		;Dokument-Daten einlesen.
			txa				;Fehler?
			beq	:1			; => Nein, weiter...
::error			jmp	OpenFileError		; => Abbruch.
::error_40_80		jmp	OpenG64Error

::1			jsr	IsApplOnDsk		;Anwendung suchen.
			cpx	#INCOMPATIBLE
			beq	:error_40_80
			txa				;Gefunden?
			bne	:error			; => Nein, Abbruch...

			ldy	#$0f			;Dokument- und Diskname
::2			lda	DocDName,y		;in Systemspeicher kopieren.
			sta	dataDiskName,y
			lda	DocFName,y
			sta	dataFileName,y
			dey
			bpl	:2

			sei				;GEOS-Reset #0.
			cld
			ldx	#$ff
			txa

			jsr	GEOS_InitSystem		;GEOS-Reset #1.
			jsr	PrepGetFile

			lda	#>EnterDeskTop -1	;Bei Fehler zurück zum
			pha				;DeskTop.
			lda	#<EnterDeskTop -1
			pha

			LoadW	r2,dataDiskName
			LoadW	r3,dataFileName
			LoadW	r6,AppFName
			LoadB	r0L,%10000000
			jmp	GetFile			;Anwendung+Dokument starten.

;*** Hilfsmittel laden.
:OpenDA			jsr	ChkFlag_40_80		;40/80Z-Flag testen.
			txa				;Fehler?
			bne	:error_40_80		; => Ja, Abbruch...

			jsr	PrepGetFile		;":dlgBoxRamBuf" löschen.

			LoadW	r6,AppFName
			LoadB	r0L,%00000000
;			LoadB	r10L,$00		;Wird durch ":PrepGetFile" gelöscht.
			jsr	GetFile			;DA laden.
			txa
			bne	:error
			jmp	MNU_REBOOT		;Zurück zum Desktop.
::error			jmp	OpenFileError
::error_40_80		jmp	OpenG64Error

;*** BASIC-Anwendung starten.
:OpenBASIC		LoadW	r0,Dlg_ExitBASIC
			jsr	DoDlgBox

			lda	sysDBData		;Abbruch ?
			cmp	#CANCEL
			beq	:exit			; => Nein, weiter...

			LoadW	r6,AppFName		;Zeiger auf Dateiname.
			jmp	ExitBApplRUN		;BASIC-Datei laden/starten.
::exit			jmp	MNU_REBOOT		;Zurück zum Desktop.

;*** Dialogboxen.
:Dlg_ExitBASIC		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w AppFName
			b DBTXTSTR   ,$0c,$3c
			w :3
			b YES        ,$01,$50
			b CANCEL     ,$11,$50
			b NULL

::2			b PLAINTEXT
			b "GEOS beenden und die BASIC-Datei"
			b BOLDON,NULL
::3			b PLAINTEXT
			b "laden und starten?",NULL

;*** Dokument-Infos einlesen.
:LoadDokInfos		LoadW	r6,DocFName		;Dateieintrag suchen.
			jsr	FindFile
			txa				;Diskettenfehler ?
			bne	:2			;Nein, weiter...

			LoadW	r9,dirEntryBuf		;Infoblock einlesen.
			jsr	GetFHdrInfo
			txa
			bne	:2

			ldy	#11			;"Class" der Application einlesen.
::1			lda	fileHeader+$75,y
			sta	AppClass,y
			dey
			bpl	:1

			ldx	#NO_ERROR		;Kein Fehler...
::2			rts				;Ende.

;*** Applikation suchen.
:IsApplOnDsk		lda	#%10000000		;Anwendung auf
			jsr	FindApplFile		;RAM-Laufwerken suchen.
			txa
			beq	:1
			lda	#%00000000		;Anwendung auf
			jsr	FindApplFile		;Disk-Laufwerken suchen.
::1			rts

;*** Anwendung auf den Laufwerken A: bis D: suchen.
:FindApplFile		sta	:2 +1			;Laufwerkstyp speichern.

			ldx	curDrive
			stx	r15L
::1			stx	r15H
			lda	driveType -8,x		;Laufwerk verfügbar?
			beq	:3			; => Nein, weiter...
			and	#%10000000
::2			cmp	#$ff			;Gesuchter Laufwerkstyp?
			bne	:3			; => Nein, weiter...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.

			jsr	OpenDisk		;Diskette öffnen.
			txa				;Disk-Fehler?
			bne	:3			; => Ja, weiter...

			LoadW	r6,AppFName		;Anwendung  suchen.
			LoadB	r7L,APPLICATION
			LoadB	r7H,1
			LoadW	r10,AppClass
			jsr	FindFTypes
			txa				;Fehler?
			bne	:3			; => Ja, Abbruch...
			lda	r7H			;Datei gefunden?
			bne	:3			; => Nein, weiter suchen...

			jsr	ChkFlag_40_80		;Gefundene Anwendung für GEOS64?
			txa
			beq	:5			; => Ja, Ende...

::3			ldx	r15H			;Zeiger auf nächstes Laufwerk.
			inx
			cpx	r15L			;Alle Laufwerke durchsucht?
			beq	:4			; => Ja, Ende...
			cpx	#12
			bcc	:1
			ldx	#$08
			bne	:1			;Auf nächsten Laufwerk weitersuchen.

::4			ldx	#$ff			;Nicht gefunden.

;--- Falls Anwendung nicht gefunden, Laufwerk zurücksetzen.
::5			txa				;Fehler?
			beq	:7			; => Nein, weiter...
			pha
			lda	r15L 			;Vorheriges Laufwerk wieder
			jsr	SetDevice		;aktivieren.
			pla
			tax
::7			rts

;*** C128/40/80Z-Modus testen.
:ChkFlag_40_80		LoadW	r6,AppFName		;Datei suchen.
			jsr	FindFile
			txa				;Fehler?
			bne	:4			; => Ja, Abbruch.

			LoadW	r9,dirEntryBuf		;Infoblock einlesen.
			jsr	GetFHdrInfo
			txa				;Info-Block gefunden ?
			bne	:4			; => Nein, BASIC-File, Abbruch...

			lda	fileHeader+$60		;40/80Z-Flag einlesen.
			ldx	c128Flag		;C64/C128?
			bne	:1			; => C128, Weiter...
;--- Ergänzung: 15.03.19/M.Kanet
;Unter GEOS gibt es kein Flag für "Nur GEOS128". Eine Anwendung die für den
;40+80Z-Modus entwickelt wurde kann auch für GEOS64 existieren. Es kann aber
;auch eine reine GEOS128-Anwendung sein.
;Unter GEOS64 werden daher GEOS64, 40ZOnly und 40/80Z akzeptiert.
			cmp	#$c0			;Nur 80Z?
if TRUE
;--- Keine Unterstützung für GEOS128.
;Daher nur auf "Nur GEOS128" testen.
			bne	:2			; => Nein, weiter...
::1			ldx	#INCOMPATIBLE		; => Nur GEOS128, Abbruch.
			b $2c
::2			ldx	#NO_ERROR		; => Anwendung OK.
::4			rts
endif

;--- Keine Unterstützung für GEOS128.
;Code aus GeoDOS64 vorerst deaktiviert.
if FALSE
			beq	:2			; => GEOS128-App auf GEOS64,Abbruch.
			bne	:3			;Evtl. GEOS64 App... weiter...

;--- Ergänzung: 15.03.19/M.Kanet
;Unter GEOS128 werden 40ZOnly, 80ZOnly und 40/80Z akzeptiert.
::1			cmp	#%00000000		;40/80Z-Flag einlesen.
			beq	:40
			cmp	#%01000000		;40/80Z ?
			beq	:80			;Ja -> 80Z-Modus setzen.
			cmp	#%10000000		;40/80Z ?
			beq	:2			;Ja -> 80Z-Modus setzen.
			cmp	#%11000000		;Nur 80Z ?
			beq	:80			;Ja -> 80Z-Modus setzen.
::2			ldx	#INCOMPATIBLE		; -> Nur GEOS64, Abbruch.
			bne	:4
::80			lda	#%10000000		;80Z-Modus setzen.
			b $2c
::40			lda	#%00000000		;40Z-Modus setzen.
			sta	appScrnMode
::3			ldx	#NO_ERROR
::4			rts

:appScrnMode		b $00
endif

;*** GEOS-Editor starten.
:OpenEditor		jsr	FindEditor		;GEOS.Editor suchen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch...

			LoadW	a0,dirEntryBuf -2	;Zeiger auf Verzeichnis-Eintrag.
			jmp	StartFile_a0		;Anwendung starten.

::error			lda	#<Dlg_ErrEditor1
			ldx	#>Dlg_ErrEditor1
			jmp	PrntErrorExit		;Fehler ausgeben => Desktop.

;*** GEOS.Editor auf Disk suchen.
:FindEditor		lda	#<FNameEdit64		;Name für "GEOS64.Editor".
			ldx	#>FNameEdit64
			sta	FNameEditPtr +0
			stx	FNameEditPtr +1

			lda	#%10000000		;GEOS.Editor auf
			jsr	FindEditFile		;RAM-Laufwerken suchen.
			txa
			beq	:1
			lda	#%00000000		;GEOS.Editor auf
			jsr	FindEditFile		;Disk-Laufwerken suchen.
::1			rts

;*** Datei auf den Laufwerken A: bis D: suchen.
:FindEditFile		sta	:2 +1

			ldx	curDrive
			stx	r15L
::1			stx	r15H
			lda	driveType -8,x		;Laufwerk verfügbar?
			beq	:3			; => Nein, weiter...
			and	#%10000000
::2			cmp	#$ff			;Gesuchter Laufwerkstyp?
			bne	:3			; => Nein, weiter...
			txa
			jsr	SetDevice		;Laufwerk aktivieren.

			jsr	OpenDisk		;Diskette öffnen.
			txa				;Disk-Fehler?
			bne	:3			; => Ja, weiter...

			MoveW	FNameEditPtr,r6
			jsr	FindFile		;Editor-Datei-Eintrag suchen.
			txa				;Gefunden?
			beq	:5			; => Ja, Ende...

::3			ldx	r15H			;Zeiger auf nächstes Laufwerk.
			inx
			cpx	r15L			;Alle Laufwerke durchsucht?
			beq	:4			; => Ja, Ende...
			cpx	#12
			bcc	:1
			ldx	#$08
			bne	:1			;Auf nächsten Laufwerk weitersuchen.

::4			ldx	#$ff			;Nicht gefunden.

;--- Falls Editor nicht gefunden, Laufwerk zurücksetzen.
::5			txa
			beq	:7
			pha
			lda	r15L 			;Vorheriges Laufwerk wieder
			jsr	SetDevice		;aktivieren.
			pla
			tax
::7			rts

:FNameEdit64		;b "GD.SETUP",NULL
			b "GEOS64.Editor",NULL
:FNameEditPtr		w $0000

;*** Dialogboxen.
:Dlg_ErrEditor1		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$10,$2e
			w :3
			b DBTXTSTR   ,$0c,$3c
			w :4
			b OK         ,$01,$50
			b NULL

::2			b PLAINTEXT
			b "GeoDesk konnte die Datei",NULL
::3			b BOLDON
			b "GEOS64.Editor",NULL
::4			b PLAINTEXT
			b "nicht finden!",NULL

;*** Neuen Druckertreiber laden.
:ChangePrinter		LoadB	r7L,PRINTER		;Dateityp festlegen.
			LoadW	r10,$0000		;Keine GEOS-Klasse.
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	InstallPrinter
			rts

:InstallPrinter		LoadW	r0,dataFileName		;Druckertreiber laden.
:LoadPrinter		LoadW	r6,PrntFileName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString		;Druckername kopieren.

			LoadW	r7 ,PRINTBASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Druckertreiber einlesen.
			txa
			bne	OpenPrntError		; => Nein, weiter...
			;jmp	OpenPrntOK

;*** Info: Drucker installiert.
:OpenPrntOK		LoadW	r0,:dlgbox		;Hinweis ausgeben:
			jsr	DoDlgBox		;Drucker wurde installiert.
			ldx	#NO_ERROR
			rts

::dlgbox		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$18,$30
			w PrntFileName
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "Der Drucker wurde installiert:"
			b BOLDON,NULL

;*** Fehler: Drucker konnte nicht geöffnet werden.
:OpenPrntError		lda	#$00			;Druckername löschen.
			sta	PrntFileName
			LoadW	r0,:dlgbox		;Hinweis ausgeben:
			jsr	DoDlgBox		;Drucker nicht installiert.
			ldx	#DEV_NOT_FOUND
			rts

::dlgbox		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$2a
			w :2
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "GeoDesk konnte den Drucker",NULL
::2			b "nicht installieren!",NULL

;*** Neuen Eingabetreiber laden.
:ChangeInput		LoadB	r7L,INPUT_DEVICE	;Dateityp festlegen.
			LoadW	r10,$0000		;Keine GEOS-Klasse.
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			bne	:1			; => Nein, weiter...

			LoadW	r0,dataFileName		;Name Eingabetreiber kopieren.
			LoadW	r6,inputDevName
			ldx	#r0L
			ldy	#r6L
			jsr	CopyString		;Name Eingabetreiber kopieren.

			LoadW	r7 ,MOUSE_BASE
			LoadB	r0L,%00000001
			jsr	GetFile			;Eingabetreiber einlesen.
			jmp	InitMouse		;Initialisieren.
::1			rts

;*** Datei auswählen.
;    Übergabe:		r7L  = Datei-Typ.
;			r10  = Datei-Klasse.
;    Rückgabe:		In ":dataFileName" steht der Dateiname.
;			xReg = $00, Datei wurde ausgewählt.
:OpenFile		MoveB	r7L,:OpenFile_Type
			MoveW	r10,:OpenFile_Class

::1			ldx	curDrive
			lda	driveType -8,x		;Aktuelles Laufwerk gültig?
			bne	:3			; => Ja, weiter...

			ldx	#8			;Gültiges Laufwerk suchen.
::2			lda	driveType -8,x
			bne	:3
			inx
			cpx	#12
			bcc	:2
			ldx	#$ff
			rts

::3			txa				;Laufwerk aktivieren.
			jsr	SetDevice

;--- Dateiauswahlbox.
::4			MoveB	:OpenFile_Type ,r7L
			MoveW	:OpenFile_Class,r10
			LoadW	r5 ,dataFileName
			LoadB	r7H,255
			LoadW	r0,:Dlg_SlctFile
			jsr	DoDlgBox		;Datei auswählen.

			lda	sysDBData		;Laufwerk wechseln ?
			bpl	:5			; => Nein, weiter...

			and	#%00001111
			jsr	SetDevice		;Neues Laufwerk aktivieren.
			txa				;Laufwerksfehler ?
			beq	:4			; => Nein, weiter...
			bne	:1			; => Ja, gültiges Laufwerk suchen.

::5			cmp	#DISK			;Partition wechseln ?
			beq	:4			; => Ja, weiter...
			ldx	#$ff
			cmp	#CANCEL			;Abbruch gewählt ?
			beq	:6			; => Ja, Abbruch...
			inx
::6			rts

::OpenFile_Type		b $00
::OpenFile_Class	w $0000

::Dlg_SlctFile		b $81
			b DBGETFILES!DBSETDRVICON ,$00,$00
			b CANCEL                  ,$00,$00
			b DISK                    ,$00,$00
			b OK,0,0
			b NULL

;*** Anwendung wählen.
:SelectAppl		lda	#APPLICATION		;GEOS-Anwednungen.
			b $2c
:SelectAuto		lda	#AUTO_EXEC		;GEOS-Autostart.
			sta	r7L

			lda	#$00			;GEOS-Klasse löschen.
			sta	r10L
			sta	r10H

;*** Anwendung/Dokument wählen.
:SelectAnyFile		jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	:openfile
::exit			jmp	MNU_REBOOT		;Zurück zum Desktop.

::openfile		LoadW	r6,dataFileName
			jsr	FindFile		;Datei suchen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			LoadW	a0,dirEntryBuf -2	;Datei öffnen.
			jmp	StartFile_a0

;*** Dokument wählen.
:SelectDocument		ldx	#0			;Alle Dokumente.
			b $2c
:SelectDocWrite		ldx	#2			;GeoWrite-Dokumente.
			b $2c
:SelectDocPaint		ldx	#4			;GeoPaint-Dokumente.
			lda	ApplClass +0,x		;GEOS-Klasse fürr Dokumente setzen.
			sta	r10L
			lda	ApplClass +1,x
			sta	r10H
			LoadB	r7L,APPL_DATA		;GEOS-Dokumente.
			jmp	SelectAnyFile		;Datei auswählen.

;*** Liste der Anwendungsklassen.
:ApplClass		w $0000
			w AppClassWrite
			w AppClassPaint
:AppClassWrite		b "Write Image ",NULL
:AppClassPaint		b "Paint Image ",NULL

;*** Hintergrundbild wechseln.
:SelectBackScrn		lda	#<AppClassPaint		;GEOS-Klasse für
			sta	r10L			;GeoPaint-Dokumente setzen.
			lda	#>AppClassPaint
			sta	r10H
			LoadB	r7L,APPL_DATA
			jsr	OpenFile		;Datei auswählen.
			txa				;Diskettenfehler ?
			beq	:openfile
::exit			jmp	MNU_REBOOT		;Zurück zum Desktop.

::openfile		LoadW	r6,dataFileName
			jsr	FindFile		;Datei suchen.
			txa				;Fehler?
			bne	:exit			; => Ja, Abbruch...

			php				;Hintergrundfarbe löschen.
			sei

			ldx	CPU_DATA
			lda	#$35
			sta	CPU_DATA

			lda	$d020
			sta	r0L
			lsr	r0L
			rol
			lsr	r0L
			rol
			lsr	r0L
			rol
			lsr	r0L
			rol

			stx	CPU_DATA
			plp

			jsr	i_UserColor		;Farb-RAM löschen.
			b	$00,$00,$28,$19

			jsr	ViewPaintFile		;Hintergrundbild anzeigen.
			txa				;Fehler ?
			bne	NoBackScrn		; => Ja, kein Startbild.

;*** Hintergrundgrafik speichern.
:SvBackScrn		lda	MP3_64K_SYSTEM		;Zeiger auf MP3-Systembank.
			sta	r3L

			LoadW	r0,SCREEN_BASE
			LoadW	r1,R2_ADDR_BS_GRAFX
			LoadW	r2,R2_SIZE_BS_GRAFX
			jsr	StashRAM		;Grafik speichern.
			LoadW	r0,COLOR_MATRIX
			LoadW	r1,R2_ADDR_BS_COLOR
			LoadW	r2,R2_SIZE_BS_COLOR
			jsr	StashRAM		;Farbe speichern.

			lda	sysRAMFlg		;sysRAMFlg setzen.
			ora	#%00001000
			sta	sysRAMFlg
			sta	sysFlgCopy
			lda	#$ff			;Hintergrundbild aktiv.
			sta	Flag_BackScreen
			jmp	MNU_REBOOT		;Zurück zum Desktop.

;*** Kein Startbild, Hintergrund löschen.
:NoBackScrn		lda	sysRAMFlg		;sysRAMFlg setzen.
			and	#%11110111
			sta	sysRAMFlg
			sta	sysFlgCopy
			lda	#$00			;Kein Hintergrundbild aktiv.
			sta	Flag_BackScreen
			jmp	MNU_REBOOT		;Zurück zum Desktop.

;*** Hintergrundbild anzeigen.
:ViewPaintFile		LoadW	r0,dataFileName
			jsr	OpenRecordFile		;geoPaint-Dokument öffnen.
			txa				;Fehler?
			bne	:53			; => Ja, Abbruch...

			LoadW	r14,SCREEN_BASE		;Zeiger auf Grafikspeicher.
			LoadW	r15,COLOR_MATRIX

			lda	#$00
::51			sta	a9H
			jsr	Get80Cards		;Grafikzeile einlesen.
			jsr	Prnt_Grfx_Cols		;Grafikzeile ausgeben.
			inc	a9H
			lda	a9H
			cmp	usedRecords		;Ende geoPaint-Dokument erreicht?
			bcs	:52			; => Ja, Ende...
			cmp	#13			;Bildschirm voll?
			bcc	:51			; => Nein, weiter...
::52			ldx	#$00
::53			txa
			pha
			jsr	CloseRecordFile		;geoPaint-Dokument schließen.
			pla
			tax
			rts

;*** Grafikdaten ausgeben.
;Eine geoPaint-Zeile besteht aus zwei
;Grafikzeilen a 8 Pixel Höhe.
:Prnt_Grfx_Cols		lda	#<GrfxData +   0	;Zeile #1 ausgeben.
			ldx	#>GrfxData +   0
			jsr	MoveGrfx
			lda	#<GrfxData +1288
			ldx	#>GrfxData +1288
			jsr	MoveCols

			lda	a9H			;12*2 +1 Zeilen.
			cmp	#12
			bcs	:1

			lda	#<GrfxData + 640	;Zeile #2 ausgeben.
			ldx	#>GrfxData + 640
			jsr	MoveGrfx
			lda	#<GrfxData +1368
			ldx	#>GrfxData +1368
			jmp	MoveCols

::1			rts

;*** Grafikdaten in Bildschirm kopieren.
:MoveGrfx		sta	r0L
			stx	r0H
			MoveW	r14,r1
			AddVW	320,r14
			LoadW	r2 ,$0140
			jmp	MoveData

;*** Farbdaten in Bildschirm kopieren.
:MoveCols		sta	r0L
			stx	r0H
			MoveW	r15,r1
			AddVW	40 ,r15
			LoadW	r2 ,$0028
			jmp	MoveData

;*** Eine Grafikzeile (80 Cards/8 Pixel hoch) einlesen.
:Get80Cards		jsr	PointRecord		;Zeiger auf Grafikzeile.
			txa				;Fehler?
			bne	NoGrfxData		; => Ja, Abbruch...
			tya
			bne	LoadVLIR_Data

:NoGrfxData		jsr	i_FillRam		;Keine weitere Grafikzeile.
			w	1280			;Leere Zeile ausgeben.
			w	GrfxData +   0
			b	$00
			jsr	i_FillRam
			w	160
			w	GrfxData +1288
			b	$bf
			rts

;*** Grafikbytes aus Datensatz einlesen.
:LoadVLIR_Data		LoadW	r4,diskBlkBuf		;Zeiger auf Diskettenspeicher.
			jsr	GetBlock		;Ersten Sektor des aktuellen
			txa				;Datensatzes einlesen. Fehler ?
			bne	NoGrfxData		;Nein, weiter...

			LoadW	r0 ,GrfxData		;Zeiger auf Grafikdatenspeicher.

			ldx	#$01			;Zeiger auf erstes Byte in Datei.
			stx	r5H
:GetNxDataByte		jsr	GetNxByte		;Nächstes Byte einlesen.
			sta	r2H			;Byte zwischenspeichern.

			ldy	#$00
			bit	r2H			;Gepackte Daten ?
			bmi	GetPackedBytes		;Ja, weiter...

			lda	r2H
			and	#$3f			;Anzahl Bytes ermitteln.
			beq	EndOfData		;$00 = Keine Daten.
			sta	r2H			;Anzahl Bytes merken.
			bvs	Repeat8Byte		;Bit #6 = 1, 8-Byte-Packformat.

::1			jsr	GetNxByte		;Byte einlesen und in Grafikdaten-
			sta	(r0L),y			;speicher kopieren.
			iny
			cpy	r2H			;Alle Bytes gelesen ?
			bne	:1			;Nein, weiter...

;*** Zeiger auf Grafikdatenspeicher korrigieren.
:SetNewMemPos		tya				;Zeiger auf Grafikdatenspeicher
			clc				;korrigieren.
			adc	r0L
			sta	r0L
			bcc	GetNxDataByte
			inc	r0H
			bne	GetNxDataByte		;Nächstes Byte einlesen.
:EndOfData		rts

;*** 8-Byte-Daten wiederholen.
:Repeat8Byte		jsr	GetNxByte		;Nächstes Byte aus Datensatz
			sta	ByteCopyBuf,y		;einlesen und in Zwischenspeicher.
			iny				;Zeiger auf nächstes Byte.
			cpy	#$08			;8 Byte eingelesen ?
			bne	Repeat8Byte		;Nein, weiter...

			ldx	#$00
::1			ldy	#$07			;8 Byte in Grafikdatenspeicher.
::2			lda	ByteCopyBuf,y
			sta	(r0L),y
			dey
			bpl	:2
			lda	r0L			;Zeiger auf Grafikdatenspeicher
			clc				;korrigieren.
			adc	#$08
			sta	r0L
			bcc	:3
			inc	r0H
::3			inx				;Anzahl Wiederholungen +1.
			cpx	r2H			;Wiederholungen beendet ?
			bne	:1			;Nein, weiter...
			beq	GetNxDataByte		;Weiter mit nächstem Byte.

;*** Gepackte Daten einlesen.
:GetPackedBytes		lda	r2H			;Anzahl gepackte Daten berechnen.
			and	#$7f
			beq	EndOfData		;$00 = Keine Daten, Ende...
			sta	r2H			;Anzahl Bytes merken.
			jsr	GetNxByte		;Datenbyte einlesen.

			ldy	r2H
			dey				;Byte in Grafikdatenspeicher
::1			sta	(r0L),y			;kopieren (Anzahl in ":r2H")
			dey
			bpl	:1

			ldy	r2H			;Zeiger auf Grafikdatenspeicher
			bne	SetNewMemPos		;korrigieren.

;*** Nächstes Byte aus Paint-Datei einlesen.
:GetNxByte		ldx	r5H
			inx
			bne	RdBytFromSek
			lda	r1L
			bne	GetNxSektor
:GetByteError		pla
			pla
			rts

;*** Nächsten Sektor aus Paint-Datensatz einlesen.
:GetNxSektor		lda	diskBlkBuf +$00
			sta	r1L
			lda	diskBlkBuf +$01
			sta	r1H
			sty	a9L
			jsr	GetBlock		;Sektor einlesen.
			ldy	a9L
			txa				;Diskettenfehler ?
			beq	:1			;Ja, Abbruch...
			pla
			pla
			jmp	NoBackScrn

::1			ldx	#$02			;Zeiger auf erstes Byte in Sektor.

;*** Nächstes Byte aus Sektor einlesen.
:RdBytFromSek		lda	r1L
			bne	:1
			cpx	r1H
			bcc	:1
			beq	:1
			bne	GetByteError
::1			lda	diskBlkBuf +$00,x	;Byte aus Sektor einlesen.
			stx	r5H			;Bytezeiger speichern.
			rts

;*** GeoPaint-Daten.
;Am Ende der Datei oder Bytepuffer setzen.
:ByteCopyBuf		s $08
:GrfxData		b $00
			;s (640 * 2) +8 +(80 * 2)
