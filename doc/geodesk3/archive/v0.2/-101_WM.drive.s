﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   OpenWinDrive
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Öffnet dem aktuellen Fenster zugeordnetes Laufwerk.
;******************************************************************************
.OpenWinDrive		lda	WM_WCODE

;******************************************************************************
;Routine:   OpenUserWinDrive
;Parameter: AKKU = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Öffnet einem Fenster zugeordnetes Laufwerk.
;******************************************************************************
.OpenUserWinDrive	sta	:tmpWindow		;Fenster-Nr. speichern.

			tax
			ldy	WMODE_DRIVE,x		;Laufwerk für Fenster definiert?
			beq	:0			; => Nein, Ende.
			lda	driveType -8,y		;Laufwerk verfügbar?
			bne	:2			; => Ja, weiter...
			ldx	#DEV_NOT_FOUND		;Fehler: Laufwerk nicht vorhanden.
			b $2c
::0			ldx	#NO_ERROR		;Kein Fehler.
::1			rts				;Ende.

::2			tya
			jsr	SetDevice		;Laufwerk aktivieren.
			txa				;Fehler?
			bne	:1			; => Ja, Abbruch...

			ldx	curDrive
			lda	RealDrvMode -8,x	;Partitionen/Unterverzeichnisse?
			and	#SET_MODE_PARTITION!SET_MODE_SUBDIR
			bne	:3			; => Ja, weiter...
			jmp	OpenDisk		;1541/71/81 => Nur Disk öffnen.

::3			pha
			and	#SET_MODE_PARTITION
			beq	:4			; => Keine Partitionen, weoter...

			ldx	:tmpWindow
			lda	WMODE_PART,x
			sta	r3H
			jsr	OpenPartition		;Partition für Fenster aktivieren.
			txa				;Fehler?
			bne	:1			; => Ja, Abbruch...

::4			pla
			and	#SET_MODE_SUBDIR
			beq	:1			; => Keine Verzeichnisse, Ende...

			ldx	:tmpWindow
			lda	WMODE_SDIR_T,x
			sta	r1L
			lda	WMODE_SDIR_S,x
			sta	r1H
			jmp	OpenSubDir		;Verzeichnis für Fenster aktivieren.

::tmpWindow		b $00
