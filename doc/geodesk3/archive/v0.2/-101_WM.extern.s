﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;******************************************************************************
;Routine:   InitForWM
;Parameter: -
;Rückgabe:  -
;Verändert: A
;Funktion:  Initialisiert die Mausabfrage für ":mouseVector".
;           Der FM klinkt sich hierbei direkt in die Mausabfrage ein und
;           kehrt danach erst zur eigentlichen Mausroutine zurück.
;******************************************************************************
.InitForWM		MoveW	otherPressVec,mouseOldVec
			LoadW	otherPressVec,WM_ChkMouse
			rts

;******************************************************************************
;Routine:   DoneWithWM
;Parameter: -
;Rückgabe:  -
;Verändert: A
;Funktion:  Deaktiviert die Mausabfrage über ":otherPressVec".
;******************************************************************************
.DoneWithWM		MoveW	mouseOldVec,otherPressVec
			rts

;******************************************************************************
;Routine:   WM_IS_WIN_FREE
;Parameter: -
;Rückgabe:  AKKU = Fenster-Nr.
;           XREG = $00=Kein Fehler.
;Verändert: A,X,Y
;Funktion:  Sucht nach einer freien Fenster-Nr.
;******************************************************************************
.WM_IS_WIN_FREE		ldy	WindowOpenCount
			cpy	#MAX_WINDOWS
			bcs	:3

			lda	#$00
::1			ldx	#$00
::2			cmp	WindowStack,x
			bne	:4
			clc
			adc	#$01
			cmp	#MAX_WINDOWS
			bcc	:1
::3			ldx	#NO_MORE_WINDOWS
			rts

::4			inx
			cpx	#MAX_WINDOWS
			bcc	:2
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_COPY_WIN_DATA
;Parameter: r0 = Zeiger auf Datentabelle.
;Rückgabe:  -
;Verändert: A,Y
;Funktion:  Fensterdaten in Fensterdaten-Tabelle einlesen.
;******************************************************************************
.WM_COPY_WIN_DATA	ldy	#WINDOW_DATA_SIZE -1
::1			lda	(r0L),y
			sta	WM_WIN_DATA_BUF,y
			dey
			bpl	:1
			rts

;******************************************************************************
;Routine:   WM_LOAD_WIN_DATA
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y
;Funktion:  Kopiert Daten aus Fensterdaten-Tabelle in Zwischenspeicher.
;******************************************************************************
.WM_LOAD_WIN_DATA	PushW	r0

			jsr	setWinDataVec
			jsr	WM_COPY_WIN_DATA

			PopW	r0
			rts

;******************************************************************************
;Routine:   WM_SAVE_WIN_DATA
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y
;Funktion:  Kopiert Daten aus Zwischenspeicher in Fensterdaten-Tabelle.
;******************************************************************************
.WM_SAVE_WIN_DATA	PushW	r0

			jsr	setWinDataVec

			ldy	#WINDOW_DATA_SIZE -1
::1			lda	WM_WIN_DATA_BUF,y
			sta	(r0L),y
			dey
			bpl	:1

			PopW	r0
			rts

;*** Zeiger auf Fensterdaten-Tabelle setzen.
;    Übergabe: WM_WCODE = Fenster-Nr.
:setWinDataVec		ldx	WM_WCODE
			lda	WM_WIN_DATA_VECL,x
			sta	r0L
			lda	WM_WIN_DATA_VECH,x
			sta	r0H
			rts

;*** Tabelle mit Adressen der Fenster-Speicher.
; -> ":WM_LOAD_WIN_DATA"
; -> ":WM_SAVE_WIN_DATA"
:WM_WIN_DATA_VECL	b <WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b <WM_WIN_DATA +WINDOW_DATA_SIZE *6

:WM_WIN_DATA_VECH	b >WM_WIN_DATA +WINDOW_DATA_SIZE *0
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *1
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *2
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *3
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *4
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *5
			b >WM_WIN_DATA +WINDOW_DATA_SIZE *6

;******************************************************************************
;Routine:   WM_CLR_WINDRVDAT
;Parameter: XREG = Fenster-Nr.
;Rückgabe:  -
;Verändert: A
;Funktion:  Laufwerksdaten für Fenster löschen.
;           Notwendig z.B. für "OpenMyComputer"
;           da das Fenster kein Laufwerk ist.
;******************************************************************************
.WM_CLR_WINDRVDAT	;ldx	WM_WCODE 		;Fenster-Daten löschen.
			lda	#$00
			sta	WMODE_DRIVE   ,x
			sta	WMODE_PART    ,x
			sta	WMODE_SDIR_T  ,x
			sta	WMODE_SDIR_S  ,x
			rts

;******************************************************************************
;Routine:   WM_CLR_WINSYSDAT
;Parameter: XREG = Fenster-Nr.
;Rückgabe:  -
;Verändert: A
;Funktion:  Optionen für Fenster löschen.
;           Notwendig für jedes neue
;           Fenster das geöffnet wird.
;******************************************************************************
.WM_CLR_WINSYSDAT	;ldx	WM_WCODE 		;Fenster-Daten löschen.
			lda	#$00
			sta	WMODE_VDEL    ,x	;Gelöschte Dateien anzeigen.
			sta	WMODE_SLCT_L  ,x	;Anzahl ausgewählter Einträge.
			sta	WMODE_SLCT_H  ,x
			sta	WMODE_VICON   ,x	;Icons anzeigen.
			sta	WMODE_VINFO   ,x	;Text/Details ausgeben.
			sta	WMODE_VSIZE   ,x	;Größe in KByte/Blocks.
			sta	WMODE_FILTER  ,x	;Filter für Fenster.
			sta	WMODE_SORT    ,x	;Sortierung für Fenster.
			rts

;******************************************************************************
;Routine:   WM_CALL_GETFILES
;Parameter: reloadFlag = $00 => Dateien aus Cache.
;                        $FF => Dateien von Disk einlesen.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Flag setzen "Verzeichnis neu laden.
;Hinweis:   Wenn ":reloadFiles" = $FF, dann wird
;           der BAM-Cache geprüft und ggf. das
;           Verzeichnis neu geladen.
;******************************************************************************
.WM_FLAG_RELOAD		lda	#$ff			;Flag setzen:
			sta	reloadFiles		;"Dateien neu laden"
			rts

;******************************************************************************
;Routine:   WM_TEST_MOVE
;Parameter: -
;Rückgabe:  -
;Verändert: A
;Funktion:  Auf Drag`n`Drop testen.
;******************************************************************************
.WM_TEST_MOVE		lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			jsr	SCPU_Pause

			lda	mouseData
			bmi	:1
			sec
			rts

::1			clc
			rts

;******************************************************************************
;Routine:   WM_OPEN_WINDOW
;Parameter: r0   = Zeiger auf Datentabelle.
;           r1L  = $00=Fenster-Optionen löschen.
;Rückgabe:  AKKU = Fenster-Nr.
;           XREG = $00=Kein Fehler.
;Verändert: A,X,Y,r0-r15
;Funktion:  Öffnet und zeichnet ein neues Fenster mit Inhalt.
;Hinweis:   Wenn das Fenster kein Laufwerksfenster ist, dann sollte
;           vorher ":WM_CLR_WINDRVDAT" aufgerufen werden um die
;           Laufwerksdaten für das Fenster zu löschen.
;******************************************************************************
.WM_OPEN_WINDOW		jsr	WM_IS_WIN_FREE		;Freie Fenster-Nr. suchen.
			cpx	#NO_ERROR		;Fenster gefunden ?
			beq	:1			; => Ja, weiter...
			rts

::1			ldy	WindowOpenCount		;Fenster in Stackspeicher eintragen.
			sta	WindowStack,y
			inc	WindowOpenCount

			bit	r1L
			bmi	WM_USER_WINDOW
			pha
			tax				;Fenster-Daten löschen.
			jsr	WM_CLR_WINSYSDAT
			pla

;******************************************************************************
;Routine:   WM_USER_WINDOW
;Parameter: AKKU = Fenster-Nr.
;           r0   = Zeiger auf Datentabelle.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Öffnet und zeichnet ein bereits resefviertes Fenster mit Inhalt.
;Hinweis:   Wenn das Fenster kein Laufwerksfenster ist, dann sollte
;           vorher ":WM_CLR_WINDRVDAT" aufgerufen werden um die
;           Laufwerksdaten für das Fenster zu löschen.
;******************************************************************************
.WM_USER_WINDOW		pha				;Fensterdaten in
			sta	WM_WCODE		;Zwischenspeicher kopieren.
			jsr	WM_COPY_WIN_DATA
			pla
			tax
			lda	#$00			;Flag löschen: "Maximiert".
			sta	WINDOW_MAXIMIZED,x

			txa
			jsr	WM_WIN2TOP		;Fenster nach oben holen.

			lda	WM_DATA_SIZE		;Feste Fenstergröße ?
			bne	:1			; => Ja, weiter...

			lda	WM_DATA_Y0
			ora	WM_DATA_Y1		;Fenstergröße definiert ?
			bne	:1			; => Ja, weiter...
			jsr	WM_DEF_STD_WSIZE	;Standard-Größe festlegen.

::1			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jsr	WM_CALL_DRAW		;Fenster ausgeben.

			lda	WM_WCODE
			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_CLOSE_ALL_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Alle Fenster schließen.
;******************************************************************************
.WM_CLOSE_ALL_WIN	lda	WindowStack		;Fenster-Nr. vom Stack holen.
			beq	:1			; => $00 = DeskTop, Ende.
			bmi	:1			; => $FF = Fenster nicht aktiv.
			sta	WM_WCODE		;Als aktives Fenster setzen.
			jsr	WM_CLOSE_WINDOW		;Fenster schliene.
			jmp	WM_CLOSE_ALL_WIN	;Weiter mit nächstem Fenster.
::1			rts

;******************************************************************************
;Routine:   WM_CLOSE_WINDOW
;Parameter: AKKU = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Einzelnes Fenster schließen.
;******************************************************************************
.WM_CLOSE_WINDOW	sta	:find_window +1		;Fenster-Nr. merken.
			jsr	WM_CALL_EXIT		;Vorbereiten "Fenster schließen".

			ldx	#$00			;Fenster-Nr. in Stack suchen und
			ldy	#$00			;löschen. Stack komprimieren.
::1			lda	WindowStack ,x
::find_window		cmp	#$ff
			beq	:2
			sta	WindowStack ,y
			iny
::2			inx
			cpx	#MAX_WINDOWS
			bne	:1
			cpy	#MAX_WINDOWS
			beq	:3

			dec	WindowOpenCount		;Anzahl offene Fenster -1.

			lda	#$ff			;Fenster "geschlossen".
			sta	WindowStack ,y

			jmp	WM_DRAW_ALL_WIN		;Alle Fenster aus ScreenBuffer
							;neu darstellen.
::3			rts				;Keine Fenster mehr, Ende.

;******************************************************************************
;Routine:   WM_CALL_DRAW
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Initialisiert das angegebene Fenster und zeichnet Fensterinhalt.
;******************************************************************************
.WM_CALL_DRAW		jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_DRAW_SLCT_WIN	;Leeres Fenster zeichnen.

			lda	WM_DATA_WININIT +0
			ldx	WM_DATA_WININIT +1
			ldy	WM_WCODE
			jsr	CallRoutine		;Fenster initialisieren.

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			jmp	WM_CALL_REDRAW_W	;Fensterinhalt zeichnen.

;******************************************************************************
;Routine:   WM_CALL_REDRAW
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet das angegebene Fenster neu.
;           Dabei wird kein "WIN_INIT" ausgeführt.
;******************************************************************************
.WM_CALL_REDRAW		jsr	WM_DRAW_SLCT_WIN	;Leeres Fenster zeichnen.
			jsr	WM_FLAG_RELOAD		;Flag setzen: "Dateien neu laden".

;******************************************************************************
;Routine:   WM_CALL_REDRAW_W
;Parameter: WM_WCODE = Fenster-Nr.
;           WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet das angegebene Fenster neu.
;           Dabei wird kein "WIN_INIT" ausgeführt.
;           Wird nur von ":WM_CALL_DRAW" direkt aufgerufen.
;******************************************************************************
:WM_CALL_REDRAW_W	jsr	WM_WIN_MARGIN		;Grenzen für Textausgabe setzen.
			jsr	WM_CALL_DRAWROUT	;Fensterinhalt ausgeben.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.
			jsr	WM_DRAW_TITLE		;Titelzeile ausgeben.
			jsr	WM_DRAW_INFO		;Infozeile ausgeben.
			jsr	WM_DRAW_MOVER		;Scrollbalken aktualisieren.
			jmp	WM_SAVE_SCREEN		;Fenster in ScreenBuffer speichern.

;******************************************************************************
;Routine:   WM_UPDATE
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Aktualisiert und prüft das oberste Fenster.
;******************************************************************************
.WM_UPDATE		jsr	WM_DRAW_NO_TOP		;Fenster aus ScreenBuffer laden.

			lda	WindowStack		;Oberstes Fenster aktivieren.
			sta	WM_WCODE

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_GET_ICON_XY		;Anzahl Icons ermitteln.

			jsr	WM_TEST_WIN_POS		;Testen ob aktuelle Position im
							;Fenster gültig ist, falls nein auf
							;erste Position zurücksetzen.

			jsr	WM_CALL_REDRAW		;Oberstes Fenster neu laden.

			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_REDRAW_ALL
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet alle Fenster neu (außer Desktop).
;           Dabei wird kein "WIN_INIT" ausgeführt.
;******************************************************************************
.WM_REDRAW_ALL		lda	reloadFiles		;Reload-Flag zwischenspeichern.
			sta	:reloadFilesBuf

			lda	#$00			;Hintergrundbild und Verknüpfungen
			sta	WM_WCODE		;aus Speicher holen und neuzeichnen.
			jsr	WM_LOAD_SCREEN

			lda	#MAX_WINDOWS -1		;Fenster Zähler setzen.
::loop			pha
			tax
			lda	WindowStack,x
			beq	:skip			;Desktop? => Ja, weiter....
			bmi	:skip			;Fenster aktiv? => Nein, weiter...
			sta	WM_WCODE		;Als aktuelles Fenster setzen.

			lda	:reloadFilesBuf		;Reload-Flag setzen.
			sta	reloadFiles

			jsr	WM_CALL_REDRAW		;Fenster neu zeichnen.

::skip			pla
			sec				;Fenster-Zähler korrigieren.
			sbc	#$01			;Alle Fenster gezeichnet?
			bpl	:loop			; => Nein, weiter...

			ldx	#NO_ERROR
			rts

::reloadFilesBuf	b $00

;******************************************************************************
;Routine:   WM_DRAW_ALL_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet alle Fenster neu.
;           Der Inhalt wird dabei aus dem ScreenBuffer eingelesen.
;******************************************************************************
.WM_DRAW_ALL_WIN	jsr	WM_DRAW_NO_TOP		;Fenster aus ScreenBuffer laden.

;******************************************************************************
;Routine:   WM_DRAW_TOP_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet oberstes Fenster neu.
;           Der Inhalt wird dabei aus dem ScreenBuffer eingelesen.
;******************************************************************************
.WM_DRAW_TOP_WIN	lda	WindowStack		;Oberstes Fenster aktivieren.
			sta	WM_WCODE

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			jsr	WM_LOAD_SCREEN		;Fenster aus ScreenBuffer einlesen.

			jsr	WM_DRAW_MOVER		;Scrollbalken aktualisieren.

			ldx	#NO_ERROR
			rts

;******************************************************************************
;Routine:   WM_DRAW_NO_TOP
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Zeichnet bis auf oberstes alle Fenster neu.
;           Der Inhalt wird dabei aus dem ScreenBuffer eingelesen.
;******************************************************************************
.WM_DRAW_NO_TOP		lda	WM_WCODE		;Aktuelles Fenster merken.
			pha

			lda	#MAX_WINDOWS -1		;Fenster Zähler setzen.
::loop			pha
			tax
			lda	WindowStack,x
			bmi	:skip			;Fenster aktiv? => Nein, weiter...
			sta	WM_WCODE		;Als aktuelles Fenster setzen.

			jsr	WM_LOAD_SCREEN		;Fenster aus ScreenBuffer einlesen.

::skip			pla
			sec				;Fenster-Zähler korrigieren.
			sbc	#$01			;Alle Fenster gezeichnet?
			bne	:loop			; => Nein, weiter...

			pla				;Aktives Fenster zurücksetzen.
			sta	WM_WCODE
			jmp	WM_LOAD_WIN_DATA	;Fensterdaten zurücksetzen.

;******************************************************************************
;Routine:   WM_FUNC_SORT
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster als Stapel sortieren.
;******************************************************************************
.WM_FUNC_SORT		lda	#$01
			sta	r1L			;Fensterzähler initialisieren.

::1			lda	r1L
			sta	WM_WCODE
			tax
			lda	#$00			;"Maximiert"-Flag löschen.
			sta	WINDOW_MAXIMIZED,x

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.
			jsr	WM_DEF_STD_WSIZE	;Fenstergröße auf Standard setzen.
			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			lda	WM_WCODE		;Fenster nach oben sortieren.
			jsr	WM_WIN2TOP

			inc	r1L
			lda	r1L
			cmp	#MAX_WINDOWS		;Alle Fenster neu angeordnet?
			bcc	:1			; => Nein, weiter...

			jsr	WM_FLAG_RELOAD		;Flag setzen: "Dateien neu laden".

			jmp	WM_REDRAW_ALL		;Alle Fenster neu darstellen.

;******************************************************************************
;Routine:   WM_FUNC_POS
;Parameter: -
;Rückgabe:  -
;Verändert: A,X,Y,r0-r15
;Funktion:  Fenster nebeneinander anordnen.
;******************************************************************************
.WM_FUNC_POS		lda	#MIN_AREA_WIN_Y
			sta	r2L
			lda	#MAX_AREA_WIN_Y -1
			sta	r2H			;Fensterhöhe für eine Fensterreihe.

			ldx	WindowOpenCount
			dex
			cpx	#$03 +1			;Weniger als vier Fenster?
			bcc	:2			; => Ja, weiter...
			bne	:1			; => Mehr oder weniger als vier.
			ldx	#$02			;Bei vier Fenster Anordnung 2x2.
			b $2c
::1			ldx	#$03			;Mehr als vier Anordnung 3x2.
			lda	#$57
			sta	r2H			;Fensterhöhe für zwei Fensterreihen.

::2			stx	r1H			;Anzahl Fenster je Reihe.

			dex				;Breite für Fenster einlesen.
			txa
			asl
			tax
			lda	:WIDTH +0,x
			sta	r5L
			lda	:WIDTH +1,x
			sta	r5H

			lda	#$00
			sta	r1L			;Fensterzähler initialisieren.

::3			LoadW	r3,4*8

::4			ldx	r1L
			lda	WindowStack,x		;Fenster geöffnet?
			beq	:5			; => Desktop ignorieren.
			bmi	:5			; => Nein, weiter...
			sta	WM_WCODE
			tax
			lda	#$00			;"Maximiert"-Flag löschen.
			sta	WINDOW_MAXIMIZED,x

			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			lda	r2L			;Fenster-Position oben setzen.
			sta	WM_DATA_Y0
			lda	r2H			;Fenster-Position unten setzen.
			sta	WM_DATA_Y1

			lda	r3L			;Fenster-Position links setzen.
			sta	WM_DATA_X0 +0
			lda	r3H
			sta	WM_DATA_X0 +1

			lda	r3L			;Fenster-Position rechts setzen.
			clc
			adc	r5L
			sta	r3L
			sta	WM_DATA_X1 +0
			lda	r3H
			adc	r5H
			sta	r3H
			sta	WM_DATA_X1 +1

			jsr	WM_SAVE_WIN_DATA	;Fensterdaten speichern.

			inc	r3L
			bne	:5
			inc	r3H

::5			inc	r1L
			lda	r1L
			cmp	r1H			;Fenster in Reihe neu angeordnet?
			bne	:6			; => Nein, weiter...

			lda	#$58			;Y-Position für zweite Reihe.
			sta	r2L
			lda	#MAX_AREA_WIN_Y -9
			sta	r2H
			jmp	:3			; => Weiter mit nächster Reihe.

::6			cmp	#MAX_WINDOWS		;Alle Fenster neu angeordnet?
			bcc	:4			; => Nein, weiter...

			jsr	WM_FLAG_RELOAD		;Flag setzen: "Dateien neu laden".

			jmp	WM_REDRAW_ALL		;Alle Fenster neu darstellen.

;*** Tabelle mit Fensterbreite für 1,2 oder 3 Spalten.
::WIDTH			w 36*8-1
			w 18*8-1
			w 12*8-1

;******************************************************************************
;Routine:   WM_GET_SLCT_SIZE
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Größe für aktuelles Fenster einlesen.
;******************************************************************************
.WM_GET_SLCT_SIZE	lda	WM_WCODE

;******************************************************************************
;Routine:   WM_GET_WIN_SIZE
;Parameter: AKKU = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Größe für bestimmtes Fenster einlesen.
;******************************************************************************
.WM_GET_WIN_SIZE	tax				;Fenster = DeskTop?
			bne	:no_desktop		; => Nein, weiter...
			jmp	WM_SET_MAX_WIN		;Größe für DeskTop setzen.

::no_desktop		lda	WM_WCODE
			pha
			stx	WM_WCODE
			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.
			ldx	WM_WCODE
			pla
			sta	WM_WCODE

			ldy	WINDOW_MAXIMIZED,x
			bne	:2			; => Fenster maximiert.

			ldx	#$05			;Fenster-Größe nach r2-r4.
::1			lda	WM_DATA_Y0,x
			sta	r2L       ,x
			dex
			bpl	:1
			bmi	:3

::2			jsr	WM_SET_MAX_WIN		;Fenster maximiert oder DeskTop.

::3			jmp	WM_LOAD_WIN_DATA	;Fensterdaten zurücksetzen.

;******************************************************************************
;Routine:   WM_WIN_MARGIN
;Parameter: WM_WCODE = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y,r2-r4
;Funktion:  Setzt Grenzen für Textausgabe auf Bereich den
;           Bereich innerhalb des aktuellen Fensterrahmens.
;******************************************************************************
.WM_WIN_MARGIN		lda	WM_WCODE

;******************************************************************************
;Routine:   WM_SET_MARGIN
;Parameter: AKKU = Fenster-Nr.
;Rückgabe:  -
;Verändert: X,Y,r2-r4
;Funktion:  Setzt Grenzen für Textausgabe auf Bereich den
;           Bereich innerhalb des angegebenen Fensterrahmens.
;******************************************************************************
.WM_SET_MARGIN		pha

			jsr	WM_GET_WIN_SIZE		;Fenstergröße ermitteln.

			lda	r3L			;Linken Rand nach innen setzen.
			clc
			adc	#$08
			sta	leftMargin +0
			lda	r3H
			adc	#$00
			sta	leftMargin +1

			lda	r4L			;Rechten Rand nach innen setzen.
			sec
			sbc	#$08
			sta	rightMargin +0
			lda	r4H
			sbc	#$00
			sta	rightMargin +1

			lda	r2L			;Oberen Rand nach innen setzen.
			clc
			adc	#$08
			sta	windowTop

			lda	r2H			;Unteren Rand nach innen setzen.
			sec
			sbc	#$08
			sta	windowBottom
			pla
			rts

;******************************************************************************
;Routine:   WM_NO_MARGIN
;Parameter: -
;Rückgabe:  -
;Verändert: A
;Funktion:  Grenzen für Textausgabe zurücksetzen.
;******************************************************************************
.WM_NO_MARGIN		lda	#$00
			sta	windowTop

			sta	leftMargin  +0
			sta	leftMargin  +1

			lda	#SCR_HIGHT_40_80 -1
			sta	windowBottom

			lda	#< SCR_WIDTH_40 -1
			sta	rightMargin +0
			lda	#> SCR_WIDTH_40 -1
			sta	rightMargin +1
			rts

;******************************************************************************
;Routine:   WM_NO_MOUSE_WIN
;Parameter: -
;Rückgabe:  -
;Verändert: A
;Funktion:  Grenzen für Mausbewegung zurücksetzen.
;******************************************************************************
.WM_NO_MOUSE_WIN	lda	#$00
			sta	mouseTop

			sta	mouseLeft +0
			sta	mouseLeft +1

			lda	#SCR_HIGHT_40_80 -1
			sta	mouseBottom

			lda	#< SCR_WIDTH_40 -1
			sta	mouseRight +0
			lda	#> SCR_WIDTH_40 -1
			sta	mouseRight +1
			rts

;******************************************************************************
;Routine:   WM_WIN2TOP
;Parameter: AKKU = Fenster-Nr.
;Rückgabe:  -
;Verändert: A,X,Y
;Funktion:  Setzt Fenster an erste Stelle im Stack.
;******************************************************************************
.WM_WIN2TOP		sta	WM_WCODE		;Fenster-Nr. sichern.

			ldx	#MAX_WINDOWS -1
			ldy	#MAX_WINDOWS -1
::1			lda	WindowStack ,x		;Ziel-Fenster im Stack suchen und
			cmp	WM_WCODE		;aus Stack löschen.
			beq	:2			; => Fenster gefunden, überspringen.
			sta	WindowStack ,y
			dey
			bmi	:3			;Fenster nicht gefunden, Abbruch.

::2			dex				;Gesamten Stack durchsucht?
			bpl	:1			; => Nein, weiter...

			lda	WM_WCODE		;Ziel-Fenster an erste Stelle
			sta	WindowStack		;in Stack schreiben.

::3			rts

;******************************************************************************
;Routine:   WM_FIND_WINDOW
;Parameter: -
;Rückgabe:  XREG = $00 => Kein Fehler.
;           YREG = Fenster-Nr.
;Verändert: A,X,Y,r0,r2-r4
;Funktion:  Aktuelles Fenster suchen.
;******************************************************************************
.WM_FIND_WINDOW		php				;IRQ sperren.
			sei

			lda	#$00			;Fenster-Nr. auf Anfang.
			sta	:tmpWindow

::1			ldx	#WINDOW_NOT_FOUND	;Fehlermeldung vorbereiten.

			ldy	:tmpWindow
			cpy	#MAX_WINDOWS		;Alle Fenster durchsucht ?
			beq	:3			; => Ja, Ende...
			lda	WindowStack,y		;Fenster-Nr. einlesen.
			bmi	:3			; => Ende erreicht...

			jsr	WM_GET_WIN_SIZE		;Fenstergröße einlesen.
			jsr	IsMseInRegion		;Mausposition abfragen.
			tax				;Ist Maus in Bereich ?
			bne	:2			; => Ja, Fenster gefunden.

			inc	:tmpWindow		;Zeiger auf nächstes Fenster und
			jmp	:1			;weitersuchen.

::2			ldx	#NO_ERROR		;Flag für "Kein Fehler" setzen und
			ldy	:tmpWindow		;Fenster-Nr. einlesen.
::3			plp
			rts

::tmpWindow		b $00

;******************************************************************************
;Routine:   WM_WIN_BLOCKED
;Parameter: -
;Rückgabe:  XREG = $00 => Fenster nicht verdeckt.
;Verändert: A,X,Y,r0,r2-r4
;Funktion:  Prüft ob aktuelles Fenster verdeckt ist.
;******************************************************************************
.WM_WIN_BLOCKED		lda	WM_WCODE		;Aktuelle Fenster-Nr. in
			sta	:tmpWindow		;Zwischenspeicher einlesen.
			jsr	:1			;Ist Fenster verdeckt ?

			txa				;Ergebnis zwischenspeichern.
			pha

			lda	:tmpWindow		;Fensterdaten für aktuelles
			sta	WM_WCODE		;Fenster wieder einlesen.
			jsr	WM_LOAD_WIN_DATA

			pla
			tax
			rts

;--- Ist Fenster verdeckt ?
::1			jsr	WM_LOAD_WIN_DATA	;Fensterdaten einlesen.

			ldx	#$05			;Größe des aktuellen
::2			lda	WM_DATA_Y0,x		;Fensters einlesen.
			sta	r2L       ,x
			dex
			bpl	:2

::3			inx
			stx	:tmpStackPointer	;Zeiger auf Fensterstack speichern.

			lda	WindowStack,x
			bmi	:4			; => Nicht definiert, weiter...
			beq	:4			; => Desktop-Fenster, weiter...
			cmp	:tmpWindow		;Aktuelles Fenster ?
			beq	:5			; => Ja, übergehen...

			sta	WM_WCODE		;Fensterdaten einlesen.
			jsr	WM_LOAD_WIN_DATA

			lda	WM_DATA_Y0		;Fenstergröße testen.
			cmp	r2H
			bcs	:4
			lda	WM_DATA_Y1
			cmp	r2L
			bcc	:4
			CmpW	WM_DATA_X0,r4
			bcs	:4
			CmpW	WM_DATA_X1,r3
			bcc	:4

			ldx	#WINDOW_BLOCKED		;Fenster blockiert, Ende...
			rts

::4			ldx	:tmpStackPointer
			cpx	#MAX_WINDOWS -1		;Alle Fenster überprüft ?
			bcc	:3			; => Nein, weiter...

::5			ldx	#NO_ERROR
			rts

::tmpStackPointer	b $00
::tmpWindow		b $00

;******************************************************************************
;Routine:   WM_CONVERT_CARDS
;Parameter: r1L = X-Koordinate (Cards)
;           r1H = Y-Koordinate (Pixel)
;           r2L = Breite (Cards)
;           r2H = Höhe (Pixel)
;Rückgabe:  r2L = Y-Koordinate/oben (Pixel)
;           r2H = Y-Koordinate/unten (Pixel)
;           r3  = X-Koordinate/links (Pixel)
;           r4  = X-Koordinate/rechts (Pixel)
;Verändert: A,X,Y,r2-r4
;Funktion:  Berechnet Grafikbereich von Icon in Pixel.
;******************************************************************************
.WM_CONVERT_CARDS	MoveB	r1L,r3L			;X-Koordinate von CARDs nach
			LoadB	r3H,0			;Pixel in ":r3" konvertieren.
			ldx	#r3L
			ldy	#$03
			jsr	DShiftLeft

			MoveB	r2L,r4L			;Breite von CARDs nach
			LoadB	r4H,0			;Pixel in ":r4" konvertieren.
			ldx	#r4L
			ldy	#$03
			jsr	DShiftLeft

			lda	r3L			;Breite in Pixel in rechte
			clc				;X-Koordinate wandeln.
			adc	r4L
			sta	r4L
			lda	r3H
			adc	r4H
			sta	r4H

			lda	r4L			;Rechte X-Koordinate -1 Pixel für
			bne	:1			;Ende des letzten CARDs setzen.
			dec	r4H
::1			dec	r4L

			lda	r1H			;Obere Y-Koordinate setzen.
			sta	r2L
			clc
			adc	r2H			;Höhe für Icon addieren.
			sta	r2H			;Untere Y-Koordinate.
			dec	r2H			;Auf Ende unterstes CARD setzen.
			rts

;******************************************************************************
;Routine:   WM_CONVERT_PIXEL
;Parameter: r3  = X-Koordinate (WORD, Pixel)
;           r2L = Y-Koordinate (BYTE, Pixel)
;Rückgabe:  r1L = X-Koordinate (Cards)
;           r1H = Y-Koordinate (Pixel)
;Verändert: A,r1L,r1H
;Funktion:  Rechteck-Koordinaten in ":BitmapUp"-Format konvertieren.
;******************************************************************************
.WM_CONVERT_PIXEL	lda	r3H			;X-Koordinate in CARDs
			lsr				;umrechnen.
			lda	r3L
			ror
			lsr
			lsr
			sta	r1L

			lda	r2L			;Y-Koordinate übernehmen.
			sta	r1H
			rts

;******************************************************************************
;Routine:   WM_GET_GRID_X
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  AKKU = Breite für Spalte.
;Verändert: A
;Funktion:  Berechnet Breite einer Spalte.
;******************************************************************************
.WM_GET_GRID_X		lda	WM_DATA_GRID_X
			bne	:1
			lda	#WM_GRID_ICON_XC
::1			rts

;******************************************************************************
;Routine:   WM_GET_GRID_Y
;Parameter: WM_WIN_DATA_BUF = Daten für aktuelles Fenster.
;Rückgabe:  AKKU = Höhe für Zeile.
;Verändert: A
;Funktion:  Berechnet Höhe einer Zeile.
;******************************************************************************
.WM_GET_GRID_Y		lda	WM_DATA_GRID_Y
			bne	:1
			lda	#WM_GRID_ICON_Y
::1			rts

;******************************************************************************
;Routine:   WM_SET_CACHE_POS
;Parameter: WM_WCODE = Fenster-Nr.
;           r14  = Nr. Eintrag in Dateitabelle.
;Rückgabe:  Carry-Flag =0, kein Cache, =1, RAM-Cache aktiv.
;           r14  = Zeiger auf Verzeichnis-Cache.
;           r13  = Zeiger auf Icon-Cache oder $0000=Kein Cache.
;           r12L = Eintragsgröße 32/64/96 Byte.
;           r12H = Speicherbank.
;Verändert: A,X,Y,r6-r8,r12-r14
;Funktion:  Zeiger auf Datei-Eintrag im Cache berechnen.
;******************************************************************************
.WM_SET_CACHE_POS	ldy	WM_WCODE
			lda	ramCacheMode0,y
			and	#%11000000
			bne	:set_cache
			clc
			rts

::set_cache		pha				;Cache-Modus zwischenspeichern.

			ldx	#$20
			cmp	#%10000000		;Verzeichnis-Cache aktiv?
			beq	:z0			; => Nein, weiter...
			ldx	#$40
			cmp	#%01000000		;Icon-Cache aktiv?
			beq	:z0			; => Nein, weiter...
			ldx	#$60
::z0			stx	r12L

			ldx	#r14L			;Position des Datei-Eintrages im
			ldy	#r12L			;RAM-Cache berechnen.
			jsr	BMult

			AddVW	$0300,r14		;Position Verzeichnis im Cache.

			ldx	#$00			;Position für Icon-Cache löschen.
			ldy	#$00

			pla
			and	#%01000000		;Icon-Cache aktiv?
			beq	:z1			; => Nein, weiter...

			lda	r14L			;Zeiger auf Icon-Cache berechnen.
			clc
			adc	#<32
			tax
			lda	r14H
			adc	#>32
			tay
::z1			stx	r13L			;Position Icon im Cache.
			sty	r13H

			ldx	WM_WCODE
			lda	ramBaseWin0,x		;64K-RAM-Bank setzen.
			sta	r12H			;Speicherbank RAM-Cache setzen.

			sec
			rts

;******************************************************************************
;Routine:   WM_SET_RAM_POS
;Parameter: XReg = Zero-Page-Adresse Faktor #1.
;           YReg = Zero-Page-Adresse Faktor #2.
;                  Der Wert selbst wird durch die Routine gesetzt.
;Rückgabe:  Zero-Page Faktor#1 erhält Adresse im RAM.
;Verändert: A,X,Y,r6-r8
;Funktion:  Zeiger auf Eintrag im Speicher berechnen.
;******************************************************************************
.WM_SET_RAM_POS		stx	:1 +1

			lda	#$20			;Größe Verzeichnis-Eintrag.
			sta	zpage,y
			jsr	BMult			;Anzahl Einträge x 32 Bytes.

::1			ldx	#$ff
			lda	zpage +0,x
			clc
			adc	#<BASE_DIR_DATA
			sta	zpage +0,x
			lda	zpage +1,x
			adc	#>BASE_DIR_DATA
			sta	zpage +1,x
			rts

;******************************************************************************
;Routine:   WM_SET_ENTRY
;Parameter: r0 = Nr. des gesuchten Eintrages.
;Rückgabe:  r1L = X-Position(Cards) für Icon.
;           r1H = X-Position(Pixel) für Icon.
;           XREG = $00 => Icon ist sichtbar.
;                  $FF => Icon ist nicht sichtbar.
;Verändert: A,X,Y,r0-r4
;Funktion:  Berechnet Position für Icon in Fenster.
;Hinweis:   Routine wird aktuell nicht verwendet.
;******************************************************************************
.WM_SET_ENTRY		PushW	r0

			jsr	WM_GET_ICON_XY		;Anzahl Icons ermitteln.

			jsr	WM_GET_GRID_X		;Größe für Eintrag setzen.
			sta	:CurGrid_X
			jsr	WM_GET_GRID_Y
			sta	:CurGrid_Y

			jsr	WM_GET_SLCT_SIZE	;Fenstergröße einlesen.

			PopW	r0

			lda	WM_DATA_CURENTRY +0
			sta	r4L
			lda	WM_DATA_CURENTRY +1
			sta	r4H			;Zeiger auf 1.Eintrag in Fenster.

			CmpW	r0,r4			;Eintrag größer als Position?
			bcs	:2			; => Ja, weiter...

::1			ldx	#$ff			;Ende, Eintrag nicht sichtbar.
			rts

::2			lda	r3L			;Erste X-Position für Icon in
			clc				;Fenster in CARDs berechnen.
			adc	#$10
			sta	r3L
			lda	r3H
			adc	#$00
			sta	r3H

			ldx	#r3L			;Pixel in CARDs umwandeln.
			ldy	#$03
			jsr	DShiftRight

			lda	r3L
			sta	:MinIcon_X

			lda	r2L			;Erste Y-Position für Icon in
			clc				;Fenster in Pixel berechnen.
			adc	#$10
			sta	r1H

			lda	WM_COUNT_ICON_Y		;Zähler für Anzahl Zeilen
			sta	:MaxIcon_Y		;initialisieren.

::3			lda	:MinIcon_X		;Zähler für X-Position
			sta	r1L			;initialisieren.

			lda	WM_COUNT_ICON_X		;Zähler für Anzahl Spalten
			sta	:MaxIcon_X		;initialisieren.

::4			CmpW	r4,r0			;Eintrag gefunden?
			bne	:5			; => Nein, weiter...

			ldx	#$00			;Ende, Eintrag ist sichtbar.
			rts

::5			inc	r4L			;Zähler auf nächsten Eintrag.
			bne	:6
			inc	r4H

::6			dec	:MaxIcon_X		;Alle Spalten durchsucht?
			bne	:7			; => Nein, weiter...
			dec	:MaxIcon_Y		;Alle Zeilen durchsucht?
			beq	:1			; => Ja, Ende, nicht sichtbar.

			lda	r1H			;Zeiger auf nächste Zeile.
			clc
			adc	:CurGrid_Y
			sta	r1H
			jmp	:3			;Nächste Zeile durchsuchen.

::7			lda	r1L			;Zeiger auf nächste Spalte.
			clc
			adc	:CurGrid_X
			sta	r1L
			jmp	:4			;Nächste Spalte durchsuchen.

::MinIcon_X		b $00
::MaxIcon_X		b $00
::MaxIcon_Y		b $00
::CurGrid_X		b $00
::CurGrid_Y		b $00
