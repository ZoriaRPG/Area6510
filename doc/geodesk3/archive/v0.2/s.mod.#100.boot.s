﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;--- Modul-Information:
;* GeoDesk Erststart.

if .p
			t "TopSym"
			t "TopMac"
			t "TopSym.MP3"
			t "TopSym.GD"
endif

			n "mod.#100.obj"
			t "-SYS_CLASS.h"
			f APPLICATION
			o VLIR_BOOT_START
			p MainInit
			a "Markus Kanet"
			z $80 ;Nur GEOS64.

;*** Systemvariablen.
			t "-SYS_VAR"

;*** Programmvariablen.
			t "-101_VarDataGD"
			t "-101_VarDataWM"

;*** System initialisieren.
:MainInit		lda	curDrive		;Startlaufwerk speichern.
			sta	BootDrive
			sta	LinkDrive
			jsr	SetDevice

			ldx	curDrive		;Bei RAM-Laufwerken ramBase sichern.
			lda	ramBase -8,x
			sta	BootRBase
			sta	LinkRBase

			lda	RealDrvType -8,x	;Laufwerkstyp sichern.
			sta	BootType
			sta	LinkType

			lda	RealDrvMode -8,x	;Laufwerksmodi sichern.
			sta	BootMode
			sta	LinkMode
			and	#%10000000		;CMD-Laufwerk?
			beq	:1			; => Nein, weiter...
			lda	drivePartData-8,x	;Aktuelle Partition sichern.
::1			sta	BootPart
			sta	LinkPart

			lda	RealDrvMode -8,x
			and	#%01000000		;NativeMode?
			tax
			beq	:2			; => Nein, weiter...
			lda	curDirHead +32		;Aktuellen Verzeichnis-Header
			ldx	curDirHead +33		;sichern.
::2			sta	BootSDir +0
			stx	BootSDir +1
			sta	LinkSDir +0
			stx	LinkSDir +1

;--- Auf MegaPatch/G3 testen.
			lda	MP3_CODE +0		;MP3/G3-Kennung testen.
			cmp	#"M"
			bne	:npmpg3			; => Kein MP3/G3...
			lda	MP3_CODE +1
			cmp	#"P"
			beq	:testgd3active		; => OK, weiter...

;--- Fehler: Kein G3/MP3.
::npmpg3		lda	#<Dlg_NoMP
			ldx	#>Dlg_NoMP
			bne	:syserr

;--- Fehler: Kein Speicher frei.
::noram			ldy	GD_SCRN_STACK		;Evtl. reservierten Speicher wieder
			beq	:noram_exit		;freigeben.
			jsr	FreeBank

			ldy	GD_SYSTEM_BUF
			beq	:noram_exit
			jsr	FreeBank

			ldy	GD_RAM_GDESK
			beq	:noram_exit
			jsr	FreeBank

;--- Fehler ausgeben, zurück zum DeskTop.
::noram_exit		jsr	GetBackScreen		;Bildschirm zurücksetzen.

			lda	#<Dlg_NoFreeRAM
			ldx	#>Dlg_NoFreeRAM
::syserr		sta	r0L
			stx	r0H
			jsr	DoDlgBox		;Fehlermeldung ausgeben.
			jmp	EnterDeskTop		;Zurück zum DeskTop Vx.

;--- Testen ob GD3 bereits aktiv.
;Falls ja, Programm nicht erneut in den
;Speicher kopieren, sondern zum DeskTop
;zurückkehren.
::testgd3active		jsr	SetADDR_EnterDT		;Original EnterDeskTop-Routine
			jsr	FetchRAM		;einlesen.

			ldy	#:GD3CODEPOS		;Auf GD3-Kennung testen.
			ldx	#$00
::3			lda	(r0L),y
			cmp	:GD3CODE,x
			bne	:testram		; => GD3 nicht aktiv, weiter...
			iny
			inx
			cpx	#$04
			bcc	:3
			jmp	EnterDeskTop		; => GD3 aktiv, zurück zum DeskTop.

;--- Auf 192K Speicher testen und Menü starten.
::testram		jsr	FindFreeBank		;64K für ScreenBuffer suchen.
			cpx	#NO_ERROR		;Speicher gefunden?
			bne	:noram			; => Nein, Fehler...
			sty	GD_SCRN_STACK
			jsr	AllocateBank		;Speicher reservieren.

			jsr	FindFreeBank		;64K für Systemspeicher suchen.
			cpx	#NO_ERROR		;Speicher gefunden?
			bne	:noram			; => Nein, Fehler...
			sty	GD_SYSTEM_BUF
			jsr	AllocateBank		;Speicher reservieren.

			jsr	FindFreeBank		;64K für GDesk3 suchen.
			cpx	#NO_ERROR		;Speicher gefunden?
			bne	:noram			; => Nein, Fehler...
			sty	GD_RAM_GDESK
			jsr	AllocateBank		;Speicher reservieren.

;--- Routine wurde bereits eingelesen umd auf einen
;    aktiven GD3-DeskTop zu testen. Allerdings können seither
;    die Register r0-r3L verändert worden sein.
			jsr	SetADDR_EnterDT		;Original EnterDeskTop-Routine
			jsr	FetchRAM		;einlesen.
			lda	#$00			;Zeiger auf GDesk3-Systemspeicher
			sta	r1L			;im GEOS-DACC setzen.
			sta	r1H
			lda	GD_RAM_GDESK
			sta	r3L
			jsr	StashRAM		;Original-Routine speichern.

			jsr	LoadGDesk2RAM		;GDesk3 in RAM laden.

			lda	GD_DACC_ADDR +0		;Startadresse GDDesk3 im GEOS-DACC
			sta	:ramAddrL +1		;in neue EnterDeskTop-Routine
			lda	GD_DACC_ADDR +1		;kopieren.
			sta	:ramAddrH +1
			lda	GD_DACC_ADDR +2
			sta	:ramSizeL +1
			lda	GD_DACC_ADDR +3
			sta	:ramSizeH +1
			lda	GD_RAM_GDESK
			sta	:ramBank +1

			jsr	SetADDR_EnterDT		;Neue EnterDeskTop-Routine
			LoadW	r0,:GDeskEnterDT	;installieren.
			jsr	StashRAM

;			jsr	LNK_NEW_CONFIG

			lda	#$00			;GDesk3 bereits im Speicher.
			sta	r0L			;Hauptprogramm starten.
			LoadW	r7,APP_RAM
			jmp	StartAppl

;--- Neue EnterDeskTop-Routine.
;ACHTUNG! Die Routine ist Relokatibel!
;Keine absoluten Adressen verwenden!
::GDeskEnterDT		sei				;GEOS initialisieren.
			cld
			ldx	#$ff
			stx	firstBoot
			txs
			jsr	GEOS_InitSystem
			jsr	ResetScreen

			lda	#<APP_RAM		;GDesk3-Hauptprogramm laden.
			sta	r0L
			lda	#>APP_RAM
			sta	r0H
::ramAddrL		lda	#$00
			sta	r1L
::ramAddrH		lda	#$00
			sta	r1H
::ramSizeL		lda	#$00
			sta	r2L
::ramSizeH		lda	#$00
			sta	r2H
::ramBank		lda	#$00
			sta	r3L
			jsr	FetchRAM
			lda	#$00			;GDesk3-Hauptprogramm starten.
			sta	r0L
			LoadW	r7,APP_RAM +3
			jmp	StartAppl

;--- Kennung für GD3-DeskTop aktiv.
::GD3CODE		b "GD3",NULL

;--- Füllbytes.
::dummy			s $0200 - (:dummy - :GDeskEnterDT)

;--- Zeiger auf GD3-Kennung innerhalb der neuen EnterDT-Routine.
::GD3CODEPOS		= ( :GD3CODE - :GDeskEnterDT )

;*** GDesk3 in GEOS-DACC übertragen.
:LoadGDesk2RAM		lda	#APPLICATION		;GeoDesk suchen.
			sta	r7L
			lda	#$01
			sta	r7H
			LoadW	r6,curSystemName
			LoadW	r10,GD_CLASS
			jsr	FindFTypes
			txa				;Diskettenfehler ?
			bne	:error_sys		; => Ja, Abbruch...

			lda	r7H			;Modul gefunden ?
			bne	:error_sys		; => Nein, Abbruch...
			sta	:curModule

;			lda	#< $0200		;Startadresse VLIR-Module im DACC.
			sta	a0L			;Bereich $0000-$0200 ist für die
			lda	#> $0200		;Original EnterDeskTop-Routine
			sta	a0H			;reserviert.

::open			LoadW	r0,curSystemName	;VLIR-Header einlesen.
			jsr	OpenRecordFile
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...

::loop2			lda	:curModule		;Aktuelle Modul-Nr. einlesen.
;			cmp	#127 -1			;Letztes Modul erreicht?
			cmp	#GD_VLIR_COUNT
			bcs	:continue		; => Ja, Ende...
			jsr	:readVlirRec		;VLIR-Datensatz einlesen.
			cpx	#$ff			;Ende erreicht?
			beq	:continue		; => Ja, Ende...
			txa				;Diskettenfehler?
			bne	:vlir_error		; => Ja, Abbruch...

			lda	:curModule		;Aktuelle Modul-Nr. einlesen.
			bne	:save			;Hauptmodul? => Nein, weiter...
			jsr	LoadConfig

::save			jsr	:saveVlir2RAM		;VLIR-Modusl speichern.

			inc	:curModule		;Zeiger auf nächstes VLIR-Modul.
			bne	:loop2			;Nächstes Modul kopieren.

;--- Alle VLIR-Module kopiert.
::continue		jsr	CloseRecordFile

;--- Hauptprogramm einlesen und
;Zeiger auf Unterprogramme speichern.
			jsr	:setRamVec		;Hauptmodul einlesen.
			jsr	FetchRAM

			jsr	i_MoveData		;VLIR-Daten speichern.
			w	SYSVAR_START
			w	APP_RAM +6
			w	(SYSVAR_END - SYSVAR_START)

			jsr	:setRamVec		;Hauptmodul aktualisieren.
			jmp	StashRAM

;--- Fehlermeldung ausgeben.
::vlir_error		jsr	CloseRecordFile

::error_disk		lda	#<Dlg_LdDiskErr		;Fehler beim Öffnen der VLIR-Datei.
			lda	#>Dlg_LdDiskErr
			bne	:error_exit
::error_sys		lda	#<Dlg_GeoDeskNFnd	;Systemdatei nicht gefunden.
			lda	#>Dlg_GeoDeskNFnd
			bne	:error_exit
::error_exit		sta	r0L
			stx	r0H
			jsr	DoDlgBox		;Fehlermeldung ausgeben.
			jmp	EnterDeskTop		;Zurück zum DeskTop Vx.

;--- Zeiger auf Hauptprogramm in GEOS-DACC setzen.
::setRamVec		ldx	#$03
::loop3			lda	GD_DACC_ADDR,x
			sta	r1L,x
			dex
			bpl	:loop3

			LoadW	r0,APP_RAM
			lda	GD_RAM_GDESK
			sta	r3L
			rts

;--- VLIR-Datensatz einlesen.
::readVlirRec		asl				;Zeiger auf VLIR-Daten berechnen.
			tax
			lda	fileHeader+4,x		;Zeiger auf ersten Datensatz-Sektor
			beq	:skip			;einlesen und speichern.
			sta	r1L
			lda	fileHeader+5,x
			sta	r1H
			LoadW	r2,(BASE_DIR_DATA - APP_RAM) -1
			LoadW	r7,APP_RAM
			jmp	ReadFile		;VLIR-Datensatz einlesen.

::skip			ldx	#$ff
			rts

;--- VLIR-Datensatz in GEOS-DACC speichern.
::saveVlir2RAM		lda	:curModule		;Zeiger auf Speicher in GEOS-DACC.
			asl
			asl
			tax
			lda	a0L
			sta	r1L
			sta	GD_DACC_ADDR +0,x
			lda	a0H
			sta	r1H
			sta	GD_DACC_ADDR +1,x

			lda	r7L			;Größe VLIR-Modul berechnen.
			sec
			sbc	#<APP_RAM
			sta	r2L
			sta	GD_DACC_ADDR +2,x
			lda	r7H
			sbc	#>APP_RAM
			sta	r2H
			sta	GD_DACC_ADDR +3,x

			LoadW	r0,APP_RAM		;C64-Speicher.

			lda	GD_RAM_GDESK		;GEOS-DACC-64K-Bank setzen.
			sta	r3L

			clc				;Zeiger für nächstes Modul
			lda	r2L			;berechnen.
			adc	a0L
			sta	a0L
			lda	r2H
			adc	a0H
			sta	a0H

			jmp	StashRAM		;Modul speichern.

;--- Aktuelles VLIR-Modul.
::curModule		b $00

;*** Konfiguration laden.
:LoadConfig		jsr	i_MoveData		;Konfiguration in Hauptmodul
			w	GD_VAR_START		;übertragen.
			w	APP_RAM +6 +SYSVAR_SIZE
			w	GD_VAR_SIZE

			lda	sysRAMFlg
			and	#%11110111
			bit	Flag_BackScreen		;GeoDesk-Hintergrundbild verwenden?
			bpl	:1			; => Nein, weiter...
			ora	#%00001000
::1			sta	sysRAMFlg
			rts

;*** Kein GEOS-MegaPatch.
:Dlg_NoMP		b %10000001
			b DBTXTSTR   ,$10,$10
			w :1
			b DBTXTSTR   ,$10,$24
			w :2
			b DBTXTSTR   ,$10,$30
			w :3
			b CANCEL     ,$02,$48
			b NULL

::1			b PLAINTEXT, BOLDON
			b "SYSTEMFEHLER!",NULL

::2			b PLAINTEXT
			b "Dieses Programm ist nur mit",NULL
::3			b "MegaPatch/GeoDesk V3 lauffähig!",NULL

;*** Titelzeile in Dialogbox löschen.
:Dlg_DrawTitel		lda	#$00
			jsr	SetPattern
			jsr	i_Rectangle
			b	$30,$3f
			w	$0040,$00ff
			lda	C_DBoxTitel
			jsr	DirectColor
			jmp	UseSystemFont

;*** Dialogboxen.
:Dlg_GeoDeskNFnd	b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w :1
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2a
			w :3
			b DBTXTSTR   ,$0c,$3a
			w :4
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT,BOLDON
			b "SYSTEMFEHLER",NULL
::2			b "GeoDesk konnte die System-",NULL
::3			b "Datei nicht finden!",NULL
::4			b "Programm wird beendet.",NULL

;*** Nicht genügend freier Speicher.
:Dlg_NoFreeRAM		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w :1
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2a
			w :3
			b DBTXTSTR   ,$0c,$3a
			w :4
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT,BOLDON
			b "SYSTEMFEHLER",NULL

::2			b PLAINTEXT
			b "GeoDesk benötigt mindestens",NULL
::3			b "drei freie Speicherbänke!",NULL
::4			b "Das Programm wird beendet.",NULL

;*** Diskettenfehler.
:Dlg_LdDiskErr		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w :1
			b DBTXTSTR   ,$0c,$20
			w :2
			b DBTXTSTR   ,$0c,$2a
			w :3
			b CANCEL     ,$01,$50
			b NULL

::1			b PLAINTEXT, BOLDON
			b "SYSTEMFEHLER!",NULL

::2			b PLAINTEXT
			b "GeoDesk konnte nicht in den",NULL
::3			b "GEOS-Speicher kopiert werden!",NULL

;*** System-Routinen.
			t "-SYS_RAM"
