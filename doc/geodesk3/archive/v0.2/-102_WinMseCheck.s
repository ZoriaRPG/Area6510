﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Mausklick auf Arbeitsplatz.
:MseClkMyComputer	jsr	WM_TEST_ENTRY		;Icon ausgewählt?
			bcc	:exit_mycomp		; => Nein, weiter.

			stx	:winEntry +0		;Nr. des Eintrags speichern.
			;sty	:winEntry +1

			cpy	#$00			;Aktuell max. 255 Einträge.
			bne	:exit_mycomp		; => Größer 256, Abbruch...

			cpx	#$04			;Laufwerk/Drucker ausgewählt?
			bcc	:slct_drive		; => Laufwerk.
			beq	:slct_prnt		; => Drucker.
			cpx	#$05			;Eingabe ausgewäht?
			bne	:exit_mycomp		; => Nein, Abbruch...
			jmp	AL_OPEN_INPUT		;Eingabetreiber wechseln.
::exit_mycomp		rts

;--- Laufwerk verschieben.
::slct_drive		lda	driveType,x		;Laufwerk vorhanden?
			beq	:exit_mycomp		; => Nein, Abbruch...

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_drive		; => Ja, weiter...
			jmp	OpenDriveCurWin		;Laufwerksfenster öffnen.

::dnd_drive		lda	:winEntry		;Laufwerk aktivieren.
			clc
			adc	#$08
			jsr	SetDevice
			jsr	OpenDisk		;Diskette öffnen.

			LoadW	r4,Icon_Drive +1
			jsr	DRAG_N_DROP_ICON	;Drag`n`Drop ausführen.
			cpx	#NO_ERROR		;Icon abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.
			tay				;Auf DeskTop abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.

			jmp	AL_DnD_Drive

;--- Drucker verschieben.
::slct_prnt		jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_prnt		; => Ja, weiter...

			jsr	SLCT_PRINTER		;Drucker auswählen.
			txa				;Fehler?
			bne	:exit_mycomp		; => Ja, Abruch...
			jmp	UpdateMyComputer	;Arbeitsplatz aktualisieren.

::dnd_prnt		LoadW	r4,Icon_Printer +1
			jsr	DRAG_N_DROP_ICON	;Drag`n`Drop ausführen.
			cpx	#NO_ERROR		;Icon abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.
			tay				;Auf DeskTop abgelegt?
			bne	:exit_mycomp		; => Nein, Abbruch.

;--- Hinweis:
;Es wird hier ein neuer Drucker
;ausgewählt der dann als AppLink auf
;dem DeskTop abgelegt wird.
;Notwendig da der Pfad zum Treiber im
;AppLink gespeichert wird.
			jsr	SLCT_PRINTER		;Drucker auswählen.
			txa				;Fehler?
			bne	:exit_mycomp		; => Ja, Abruch...

			jmp	AL_DnD_Printer		;AppLink erstellen.

::winEntry		w $0000

;*** Mausklick auf Dateifenster.
:MseClkFileWin		jsr	WM_TEST_ENTRY		;Icon ausgewählt?
			bcs	:slct_file		; => Ja, weiter...
			rts

;--- Mauslick auf Datei.
::slct_file		stx	r0L			;Datei-Nr. speichern.
			sty	r0H

			LoadW	r1,32			;Zeiger auf Verzeichnis-Eintrag
			ldx	#r0L			;berechnen.
			ldy	#r1L
			jsr	DMult
			AddVW	BASE_DIR_DATA,r0
			MoveW	r0,:dirEntryVec		;Zeiger zwischenspeichern.

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcs	:dnd_entry		; => Ja, weiter...

;--- Datei öffnen.
::OpenFile		jmp	OpenFile_r0		;Anwendung/Dokument/DA öffnen.

;--- Datei/Verzeichnis verschieben.
::dnd_entry		ldy	#$02
			lda	(r0L),y			;Dateityp "Gelöscht"?
			and	#%00001111		;Dateityp isolieren.
			cmp	#$06			;NativeMode Verzeichnis?
			bne	:dnd_file		; => Nein, weiter...
			jmp	:dnd_subdir		;Verzeichnis verschieben.

;--- Datei aus Fenster verschieben.
::dnd_file		tax				;Dateityp "Gelöscht"?
			bne	:chk_ftype		; => Nein, weiter...
			lda	#<Icon_Deleted +1	;Zeiger auf "Gelöscht"-Icon.
			ldx	#>Icon_Deleted +1
			bne	:do_dnd

::chk_ftype		jsr	CheckFType		;Dateityp auswerten.
			cpx	#NO_ERROR		;Starten möglich?
			bne	:exit_fwin		; => Nein, Ende...

			cmp	#$00			;BASIC oder GEOS-Datei?
			bne	:1			; => GEOS-Datei, weiter...
			lda	#<Icon_CBM +1		;Zeiger auf "CBM"-Icon.
			ldx	#>Icon_CBM +1
			bne	:do_dnd

::1			cmp	#PRINTER		;Druckertreiber?
			bne	:2			; => Nein, weiter...
			lda	#<Icon_Printer +1
			ldx	#>Icon_Printer +1
			bne	:do_dnd

::2			lda	r0L			;Zeiger auf Verzeichnis-Eintrag
			clc				;berechnen, da GetFHdrInfo einen
			adc	#$02			;30-Byte-Eintrag erwartet.
			sta	r9L
			lda	r0H
			adc	#$00
			sta	r9H

			jsr	OpenWinDrive		;Laufwerk aktivieren.
			jsr	GetFHdrInfo		;Dateiheader einlesen.

			lda	#<fileHeader +5
			ldx	#>fileHeader +5
::do_dnd		sta	r4L			;Zeiger auf Icon für DnD
			stx	r4H			;setzen.
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:exit_fwin
			tay				;Ziel-Fenster = DeskTop?
			bne	:exit_fwin		; => Nein, Ende.

			MoveW	:dirEntryVec,r0		;Zeiger auf Verzeichnis_Eintrag.

			ldy	#$18
			lda	(r0L),y			;GEOS-Dateityp auswerten.
			cmp	#PRINTER		;Drucker?
			beq	:dnd_prnt		; => Ja, Drucker-AppLink erstellen.

			jmp	AL_DnD_Files		;AppLink für Datei erstellen.

;--- Dateiklick abbrechen.
::exit_fwin		rts

;--- Drucker aus Fenster verschieben.
::dnd_prnt		jmp	AL_DnD_FilePrnt		;AppLink für Drucker erstellen.

;--- Verzeichnis aus Fenster verschieben.
::dnd_subdir		lda	#< Icon_Map +1		;Zeiger auf "Verzeichnis"-Icon.
			ldx	#> Icon_Map +1
			sta	r4L
			stx	r4H
			jsr	DRAG_N_DROP_ICON
			cpx	#NO_ERROR
			bne	:exit_fwin
			tay				;Ziel-Fenster = DeskTop?
			bne	:exit_fwin		; => Nein, Ende.

			MoveW	:dirEntryVec,r0		;Zeiger auf Verzeichnis_Eintrag.

			ldy	#$03
			lda	(r0L),y			;Track/Sektor für Verzeichnis-
			sta	r1L			;Header einlesen.
			iny
			lda	(r0L),y
			sta	r1H
			LoadW	r4,diskBlkBuf
			jsr	GetBlock		;Verzeichnis-Header einlesen.
			txa				;Fehler?
			bne	:exit_fwin		; => Ja, Abbruch...

			jmp	AL_DnD_SubDir		;AppLink für Verzeichnis erstellen.

::dirEntryVec		w $0000

;*** Mausklick auf Link-Eintrag ?
:MseClkAppLink		jsr	AL_FIND_ICON		;AppLink finden.
			txa				;AppLink gewählt?
			beq	:open_link		; => Ja, weiter...
			rts

::open_link		bit	appLinkLocked		;AppLinks gesperrt?
			bmi	:wait			; => Ja, weiter...

			jsr	WM_TEST_MOVE		;Drag`n`Drop?
			bcc	:wait			; => Nein, weiter...
			jmp	AL_MOVE_ICON

::wait			lda	mouseData		;Warten bis Maustaste nicht
			bpl	:wait			;mehr gedrückt.
			ClrB	pressFlag

			jmp	AL_OPEN_ENTRY		;AppLink öffnen.
