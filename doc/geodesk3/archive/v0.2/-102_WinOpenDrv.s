﻿; UTF-8 Byte Order Mark (BOM), do not remove!
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;

;*** Neue Laufwerks-Ansicht in "MyComputer" öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
;Im Unterschied zu :OpenDriveNewWin
;wird hier bei Native immer das Haupt-
;Verzeichnis geöffnet.
;Hier muss auch kein leeres Fenster
;gesucht werden da "MyComputer" dazu
;wiederverwendet wird.
:OpenDriveCurWin	cpy	#$00			;Eintrag in MyComputer >256?
			bne	:exit			; => Ja, Abbruch...
			cpx	#$04			;Laufwerk ausgewählt?
			bcs	:exit			; => Ja, Abbruch...
			txa
			clc				;Laufwerksadresse berechnen.
			adc	#$08
			jsr	SetDevice		;Laufwerk aktivieren.

			ldx	curDrive
			lda	RealDrvMode -8,x	;Laufwerksmodus einlesen.
			and	#SET_MODE_SUBDIR	;Native-Laufwerk?
			beq	:open_disk 		; => Nein, weiter...

::open_root		jsr	OpenRootDir		;Hauptverzeichnis öffnen.
			txa				;Fehler?
			bne	Error_NoDisk		; => Ja, Abbruch...
			jmp	:open_window		; => Nein, dann Fenster öffnen.

;--- Diskette öffnen.
::open_disk		jsr	OpenDisk		;Diskette öffnen.
			txa				;Fehler?
			bne	Error_NoDisk		; => Ja, Abbruch...

;--- Fenster öffnen.
::open_window		lda	Flag_MyComputer		;Fenster-Nr. für "MyComputer".
			jsr	WM_WIN2TOP		;Fenster nach oben holen.
			jsr	SaveWinDrive		;Laufwerk in Fenster speichern.

			jsr	WM_LOAD_WIN_DATA	;Fenster-Daten laden.

			LoadW	r0,WinData_Files	;Zeiger auf Fenster-Daten.

			ldy	#$01			;Fenster-Größe von "MyComputer"
::loop			lda	WM_WIN_DATA_BUF,y	;für neues Laufwerksfenster
			sta	(r0L),y			;kopieren.
			iny
			cpy	#$07
			bcc	:loop

			lda	Flag_MyComputer		;Fenster-Nr. für "MyComputer".
			ldx	#$00			;"MyComputer" schließen.
			stx	Flag_MyComputer

			ldx	#$ff
			stx	reloadFiles

;--- Hinweis:
;Das Fenster wurde bereits nach oben
;geholt, nicht neu zeichnen.
;			pha				;Fenster-Nr. speichern und das
;			jsr	WM_WIN2TOP		;Ziel-Fenster nach oben holen.
;			jsr	WM_DRAW_NO_TOP
;			LoadW	r0,WinData_Files
;			pla
			jsr	WM_USER_WINDOW		;Neues Fenster öffnen.

			ldy	#$01			;Standard-Fenstergröße löschen.
			lda	#$00
::38			sta	WinData_Files,y
			iny
			cpy	#$07
			bcc	:38

::exit			rts

::SelectDrive		b $00
::SelectedWindow	b $00

;--- Fehler: Keine Diskette im Laufwerk.
:Error_NoDisk		LoadW	r0,Dlg_ErrNoDisk
			jmp	DoDlgBox

;*** Fehler: Keine Diskette in Laufwerk.
:Dlg_ErrNoDisk		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Error
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$30
			w :2
			b OK         ,$01,$50
			b NULL

::1			b PLAINTEXT
			b "Laufwerk kann nicht geöffnet werden:",NULL
::2			b "Keine Diskette im Laufwerk!",NULL

;*** Neue Laufwerks-Ansicht öffnen,
;    Fenster-Optionen sind bereits
;    initialisiert => Nicht löschen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
:OpenDriveUsrWin	lda	#$ff			;Flag setzen: WMODE-Daten
			b $2c				;nicht löschen.

;*** Neue Laufwerks-Ansicht öffnen.
;    Übergabe: XReg/LOW und YReg/HIGH = Zähler für
;              Eintrag in MyComputer.
:OpenDriveNewWin	lda	#$00			;WMODE-Daten löschen.
			sta	:WM_INIT_WINOPT		;WMODE-Flag speichern.

			cpy	#$00			;Zähler prüfen:
			bne	:exit			;Nur Einträge 0-3 = Laufwerk A-D
			cpx	#$04			;sind erlaubt.
			bcs	:exit

			txa
			pha
			jsr	WM_IS_WIN_FREE		;Leeres Fenster suchen.
			sta	:WM_NV_WINDOW
			pla
			cpx	#NO_ERROR		;Fehler gefunden?
			bne	:exit			; => Nein, Abbruch...
			clc
			adc	#$08
			jsr	SetDevice		;Laufwerk aktivieren.
			jsr	OpenDisk		;Diskette öffnen.
			txa				;Fehler?
			bne	:error			; => Ja, Abbruch.

			ldx	:WM_NV_WINDOW		;Laufwerksdaten für
			jsr	SaveUserWinDrive	;neues Fenster speichern.

			ldx	#$ff
			stx	reloadFiles

			LoadW	r0,WinData_Files	;Zeiger auf Fenster-Daten.
			lda	:WM_INIT_WINOPT
			sta	r1L
			jmp	WM_OPEN_WINDOW		;Neues Fenster öffnen.
::error			jmp	Error_NoDisk		;Fehler ausgeben.
::exit			rts

::WM_NV_WINDOW		b $00
::WM_INIT_WINOPT	b $00

;*** Laufwerksdaten in Fenster-Daten speichern.
:SaveWinDrive		ldx	WM_WCODE		;Fenster-Nr. einlesen.
:SaveUserWinDrive	lda	curDrive		;Aktuelles Laufwerk einlesen.
			sta	WMODE_DRIVE,x		;Laufwerk speichern.

			tay
			lda	RealDrvMode-8,y		;CMD-Laufwerk?
			pha
			and	#SET_MODE_PARTITION
			beq	:no_part		; => Nein, weiter...

			lda	drivePartData-8,y	;Partition einlesen.
			b $2c
::no_part		lda	#$00			;Keine Partition.
			sta	WMODE_PART,x		;Partitions-Daten speichern.

			pla				;Native-Laufwerk?
			and	#SET_MODE_SUBDIR
			tay
			beq	:no_native		; => Nein, weiter...

			lda	curDirHead +32		;Zeiger auf Verzeichnis-Header
			ldy	curDirHead +33		;einlesen.
::no_native		sta	WMODE_SDIR_T,x		;Verzeichnis-Daten speichern.
			tya
			sta	WMODE_SDIR_S,x
			rts

;*** Laufwerks-Fenster initialisieren.
:InitWIN		lda	#$00			;Zähler löschen.
			sta	WM_DATA_MAXENTRY +0
			sta	WM_DATA_MAXENTRY +1
			sta	WM_DATA_CURENTRY +0
			sta	WM_DATA_CURENTRY +1

			ldx	WM_WCODE		;Anzahl gewählte Dateien löschen.
			sta	WMODE_SLCT_L,x
			sta	WMODE_SLCT_H,x

			jsr	INIT_WIN_GRID		;In Abhängigkeit der Routinen
							;":OpenDriveNewWin" und
							;":OpenDriveUsrWin" die Werte für
							;Grid X/Y setzen. ":OpenDriveUsrWin"
							;kann dabei Werte für Grid X/Y
							;vorgeben (z.B. Text-/Icon-Modus).

			ldx	WM_WCODE
			lda	ramBaseWin0,x		;RAM bereits reserviert?
			bne	:exit			; => Ja, Ende.

			jsr	FindFreeBank		;64K RAM für Cache suchen.
			cpx	#NO_ERROR		;RAM frei?
			beq	:enable_cache		; => Ja, weiter...

			lda	Flag_WarnNoRAM		;Hinweis "Kein Speicher frei"?
			bne	:1			; => Nein, weiter...

			ldx	WM_WCODE
			lda	ramCacheMode0,x
			and	#%00000001		;Hinweis bereits angezeigt?
			bne	:1			; => Ja, Info überspringen.

			LoadW	r0,Dlg_InfoNoRAM
			jsr	DoDlgBox		;Hinweis "Kein Speicher frei".

			lda	sysDBData
			cmp	#YES			;Hinweis weiterhin anzeigen?
			beq	:1			; => Ja, weiter...

			dec	Flag_WarnNoRAM		;Hinweis abschalten.

::1			jmp	:disable_cache		;RAM-Cache-Modus löschen.

::enable_cache		tya
			ldx	WM_WCODE
			sta	ramBaseWin0  ,x		;RAM-Cache-Modus aktivieren und
			jsr	AllocateBank		;Speicherbank reservieren.

			lda	#stdCacheMode
			b $2c
::disable_cache		lda	#$01
			ldx	WM_WCODE
			sta	ramCacheMode0,x

::exit			rts

;*** Aktuelles Fenster schließen.
:ExitWIN		ldx	WM_WCODE
			ldy	ramBaseWin0,x		;Speicherbank einlesen. Reserviert?
			beq	:exit			; => Nein, Ende.

			jsr	FreeBank		;Speicherbank freigeben.

			ldx	WM_WCODE
			lda	#$00
			sta	ramBaseWin0,x		;RAM bereits reserviert?

::exit			rts

;*** Hinweis: Kein Speicher frei.
:Dlg_InfoNoRAM		b %01100001
			b $30,$97
			w $0040,$00ff

			b DB_USR_ROUT
			w Dlg_DrawTitel
			b DBTXTSTR   ,$0c,$0b
			w Dlg_Titel_Info
			b DBTXTSTR   ,$0c,$20
			w :1
			b DBTXTSTR   ,$0c,$2d
			w :2
			b DBTXTSTR   ,$0c,$38
			w :3
			b DBTXTSTR   ,$0c,$46
			w :4
			b YES        ,$01,$50
			b NO         ,$11,$50
			b NULL

::1			b PLAINTEXT
			b "Nicht genügend GEOS-Speicher frei!",NULL
::2			b "Das Laufwerk wird ohne schnellen",NULL
::3			b "RAM-Zwischenspeicher geöffnet!",NULL
::4			b "Diesen Hinweis weiterhin anzeigen?",NULL

;*** Fenster-Grid initialisieren.
;    Übergabe: XReg = Fenster-Nr.
:INIT_WIN_GRID		;ldx	WM_WCODE
			lda	WMODE_VICON,x		;Icon-Modus aktiv?
			bne	:text_mode		; => Nein, Info/Text-Modus...

::icon_mode		lda	#$ff			;Standard-Verschieben für Icons.
			ldx	#$00			;Standard für Spaltenbreite.
			ldy	#$00			;Standard für Zeilenhöhe.
			beq	SET_WIN_GRID		;Grid-Daten setzen.

::text_mode		lda	WMODE_VINFO,x		;Text-Modus aktiv?
			bne	:info_mode		; => Nein, Info-Modus.

			lda	#$ff			;Standard-Verschieben für Icons.
			ldx	#$00			;Standard für Spaltenbreite.
			ldy	#$08			;8 Pixel Zeilenhöhe.
			bne	SET_WIN_GRID		;Grid-Daten setzen.

::info_mode		lda	#$ee			;Verschieben für Text-Modus.
			ldx	#$01			;Max. 1 Spalte.
			ldy	#$08			;8 Pixel Zeilenhöhe.
			;jmp	SET_WIN_GRID		;Grid-Daten setzen.

;*** Fenster-Grid ändern.
:SET_WIN_GRID		pha				;Grid-Werte sichern.
			txa
			pha
			tya
			pha

			jsr	WM_LOAD_WIN_DATA	;Fenster-Daten einlesen.

			pla
			sta	WM_DATA_GRID_Y		;Zeilenhöhe setzen.

			pla
			sta	WM_DATA_COLUMN		;Anzahl Spalten setzen.

			pla				;Verschiebe-Routine festlegen.
			sta	WM_DATA_WINMOVE+0
			sta	WM_DATA_WINMOVE+1

			jmp	WM_SAVE_WIN_DATA	;Fenster-Daten speichern.

;*** Laufwerks-Fenster: Disk-Namen anzeigen.
;    Übergabe: r1H = Zeile (Pixel).
;              r11 = Spalte (Pixel).
:PrntCurDkName		lda	curDrive		;Laufwerks-Adresse einlesen.
			tax
			clc
			adc	#$39			;Laufwerk nach ASCII wandeln und
			sta	:DiskName +0		;in Titelzeile schreiben.

			lda	RealDrvMode -8,x	;CMD-Laufwerk?
			and	#SET_MODE_PARTITION
			beq	:no_part		; => Nein, weiter...

			lda	drivePartData-8,x	;Partition einlesen.

::no_part		ldy	#"0"			;Partitions-Nr. nach
			ldx	#"0"			;ASCII wandeln.
::1			cmp	#100
			bcc	:2
			sbc	#100
			iny
			bne	:1
::2			cmp	#10
			bcc	:3
			sbc	#10
			inx
			bne	:2
::3			adc	#"0"
			sty	:DiskName +2		;ASCII-Wert der Partition
			stx	:DiskName +3		;in die Titel-Zeile schreiben.
			sta	:DiskName +4

;--- Diskname kopieren.
			ldx	#r0L			;Zeiger auf Disknamen setzen.
			jsr	GetPtrCurDkNm

			LoadW	r4,:DiskName +6		;Zeiger auf Titelzeile.

			ldx	#r0L			;Diskname aus BAM in Titelzeile
			ldy	#r4L			;kopieren.
			jsr	SysCopyName

			LoadW	r0,:DiskName		;Diskname ausgeben.
			jmp	PutString

::DiskName		b $00				;Laufwerk.
			b "/"
			b "000"				;Partition.
			b ":"
			s 17				;Disk-/Verzeichnis-Name.

;*** Laufwerks-Fenster: Disk-Namen anzeigen.
:PrntCurDkInfo		PushB	r1H			;Y-Koordinate Textausgabe sichern.

			jsr	OpenWinDrive		;Laufwerk öffnen.
			jsr	CalcBlksFree		;Freie Blöcke ermitteln.

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			beq	:0			; => Blocks, weiter...

			ldx	#r3L			;Freie und Max.Anzahl an
			ldy	#$02			;Blocks in KByte umrechnen.
			jsr	DShiftRight
			ldx	#r4L
			ldy	#$02
			jsr	DShiftRight

::0			PopB	r1H			;Y-Koordinate zurücksetzen.

			PushB	r3H
			PushB	r3L

;--- Speicher frei ausgeben.
			lda	r4H			;Anzahl freie Blocks für die
			sta	r0H			;Berechnung der belegten Blocks
			pha				;speichern und für die Ausgabe
			lda	r4L			;vorbereiten.
			sta	r0L
			pha

			lda	#%11000000		;Freie Blocks ausgeben.
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			bpl	:free_block		; => Blocks, weiter...
::free_kbyte		ldx	#0
			b $2c
::free_block		ldx	#4
			jsr	:prnt_strg		;"frei" ausgeben.

;--- Speicher belegt.
			PopW	r4			;Freie Blocks zurücksetzen.

			pla				;Belegten Speicher berechnen.
			sec
			sbc	r4L
			sta	r0L
			pla
			sbc	r4H
			sta	r0H
			lda	#%11000000		;Belegten Speicher ausgeben.
			jsr	PutDecimal

			ldx	WM_WCODE
			lda	WMODE_VSIZE,x		;Blocks oder KByte?
			bpl	:used_block		; => Blocks, weiter...
::used_kbyte		ldx	#2
			b $2c
::used_block		ldx	#6
			jsr	:prnt_strg		;"belegt" ausgeben.

;--- Anzahl Dateien ausgeben.
			lda	WM_DATA_MAXENTRY +0
			sta	r0L
			lda	WM_DATA_MAXENTRY +1
			sta	r0H
			lda	#%11000000
			jsr	PutDecimal

			ldx	#8
::prnt_strg		lda	:tx_adr +0,x
			sta	r0L
			lda	:tx_adr +1,x
			sta	r0H
			jmp	PutString		;"Dateien" ausgeben.

::tmp_Text0		b " Kb frei, ",NULL
::tmp_Text1		b " Kb belegt, ",NULL
::tmp_Text2		b " Blocks frei, ",NULL
::tmp_Text3		b " Blocks belegt, ",NULL
::tmp_Text10		b " Datei(en)",NULL

::tx_adr		w :tmp_Text0
			w :tmp_Text1
			w :tmp_Text2
			w :tmp_Text3
			w :tmp_Text10
