# Area6510

Area6510 is my place to keep my work on the Commodore C64 alive. This does currently include only some documentation files but more documentation and original source packages provided as D64 images will be added in the future.

## License

The documentation is licensed under the
Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit:
http://creativecommons.org/licenses/by-sa/4.0/

The scripts are free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
