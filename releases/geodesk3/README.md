# Area6510

### GEODESK64
This directory contains current and archived releases of GEODESK64, the new GEOS desktop for MegaPatch64.
This project is under heavy development, use at own risk!

### What is this?
GeoDesk is a new DeskTop for GEOS/MegaPatch64. GEOS/MegaPatch128 not supported.

GeoDesk is currently under development, many features like disk- and file operations are not yet implemented.

GeoDesk will support all CMD-Hardware and SD2IEC-devices.

GeoDesk supportes drag'n'drop and multiple windows, requires 192Kb of RAM.
