# GEOS MegaPatch3
### Fixed problems since 2003:
* Installation via GEOS.MP3 replaces multiple RAMNative drives with a single drive.
* The screen saver is reactivated each time you restart.
* The 1541 cache driver cannot be installed or uninstalled without errors and may free system memory which can lead to a crash.
* The ReBoot system is "Optional", but was detected as "missing system file" during the boot disk check: The installation cannot be continued.
* The scan of the system files of the startup diskette was faulty and was fixed.
* Incorrect color representation in the MegaPatch logo fixed.
* With the C128, different colors were used in the setup program for the author hint.
* Bug in color display with startup image "Megascreen.pic" fixed.
* Too long file name for the startup image overwrites the file name for the default printer.
* The faulty installation in 40character mode of the C128 was fixed.
* Incorrect color display in dialog boxes in the setup program in 80character mode of the C128 fixed.
* The option "Fast memory transfer for C=REU/MoveData" is no longer deactivated after each restart.
* The option "Fast memory transfer for C=REU/MoveData" is only enabled for a C=REU memory extension.
* The screensaver 64erMove now works together with C=REU and MoveData.
* 64erMove can now also be saved in the editor (wrong file name).
* In the GEOS.editor some settings were not updated if they are changed by other settings. This caused [X] options to be displayed as "Enabled" even though the option was disabled.
* GEOS.BOOT now starts the system clock, even if it was previously stopped outside GEOS.
* Immediate update of free/busy memory banks in GEOS when changes are made in the editor.
* System date is now set to 1.1.2018 by default if no RTC clock is found.
* When using a memory expansion with 16.384KByte the size was not recognized correctly. The error was fixed, but only 255x64KByte = 16.320JByte can be used.
* When using more than one setup diskette, the new diskette is now initialized after a diskette change and GEOS-internal system variables are updated to detect a faulty diskette change if necessary.
* When starting without a background image, the background color is now set to Standard in the 80 character mode of the C128. Necessary because 128DUALTOP does not delete the color memory at startup.
* The automatic recognition of an RTC to set the time leads to a system standstill if the parallel cable of the 1571 is installed.
* If GEOS.MP3 is started by GEOS128v2/DESKTOPv2 in 80Z mode, the DB_DblBit flag may not be set correctly. DialogBox icons are then not automatically doubled in width.
* Clear screen when leaving GEOS.MP3 otherwise GEOS128v2/DESKTOPv2 will be displayed with wrong colors.
* The X register must remain unchanged if ":MoveData" is used. At least TopDesk v4.1 has problems here if the X register is changed.
* In TaskManager128 reading the drive bytes from the REU via FetchRAM into the ZeroPage leads to an error ("The drive configuration was changed").
* When changing the current task TurboDOS is deactivated in all drives, otherwise VICE can crash on hardware drives (1541,71,81...) with a DISK-JAM.
* geoPaint crashes when moving the image section with REU-MoveData option enabled.
* geoPaint crashes when restoring a changed drawing.
* Problems with installation with distributed setup on 2x1541 disks fixed.
* Fixed a bug in the ToBASIC routine. The error prevented starting BASIC programs and executing BASIC commands when exiting GEOS.

### Fixed problems with version 2018:
* MP3 cannot be installed on a C128 with RAMLink and SuperCPU (GEOS.Editor hangs or GEOS.MakeBoot crashes due to missing switch to 1MHz in the drive drivers to RAMDrive, RAMlink and CMDHD with parallel cable)
* The system start messages have been revised, including the author's notes.
* Mark StartMP_64 under GEOS128 as "Only executable under GEOS64".
* Clarification of the option C=REU-MoveData in the GEOS editor: This option is deactivated with a SuperCPU, because 16Bit-MoveData of the SuperCPU is used here.
* Clean up system startup messages for a clearer startup process.
* GEOS.Editor displays some icons with the wrong width under DESKTOP 2.x in 80character mode. This is because DESKTOP does not activate the DblBit.
* Installation error with SuperCPU/RAMCard as GEOS-DACC and GeoRAM-Native drive as setup drive (source and destination) fixed. GRAM_BANK_SIZE was not detected because SuperCPU/RAMCard=GEOS-DACC.
* RAM81 drives could not be used with RAMLink drives.
* Fixed a bug when using CMDHD+CMDRAMLink+parallelcable: When using NativeMode a wrong drive size will be displayed in the editor.
* Fixed a bug on MP128 when switching drive or partition on the device where the editor was started from.

### Extensions/Changes:
* The AutoAssembler files can automatically delete object code files and external symbol tables after assembly.
* The GEOS.editor has been enhanced with a progress indicator at system startup.
* The AutoAssembler files can request a disk change.
* In the GEOS128.BOOT file, the I/O area is now activated before accessing the registers starting at $Dxxx.
* In the GEOS.Editor the possibility to change and save the GEOS serial number has been added.
* Support of GeoRAM/C=REU with 16Mb. The size is displayed when the system is started. The memory extensions are still only supported up to 4Mb when used as GEOS-DACC.
* New drive drivers GeoRAM-Native/C=REU-Native. The drivers allow to use the unused memory of a GeoRAM/C=REU as RAM drive, similar to the SuperRAM driver.
* HD compatible NativeMode driver without parallel cable support for DNP support under SD2IEC (IECBus NativeMode). Replaced by the SD2IEC driver, but still available in source code.
* The extended memory is tested at startup.
* Query the extended memory size at startup: If less than 192Kb return to BASIC.
* New SD2IEC driver: IECBus-NM only works with SD2IEC up to 8mb(127 tracks) due to TurboDOS limitations. The new SD2IEC avoids this problem with specific TurboDOS commands. Therefore the driver only works with SD2IEC.
* On SD2IEC/IECBusNM subdirectories are now possible.
* All drives are now checked for valid track/sector addresses when reading/writing sectors.
* Change in range 1581/NM drives: According to GEOS 2.x, the disc name for all drive types is displayed at byte $90 in the BAM sector. Applications that change the disc name from byte $04 in the BAM sector do not work anymore. Change corresponds to the behavior of GEOS 2.x!
* TaskManager128: When opening a new application, the screen flag is evaluated and activated according to the 40 or 80 character screen.
* In the GEOS.editor, the drive mode of the SD2IEC can now be changed between 1541/1571/1581 or SD2IEC/Native.
* Changing the DiskImages to SD2IEC is possible with the GEOS.Editor.
* Set mouse limits under MP128 when switching between 40/80 screen mode.
* CMD HD cable is now disabled by default.
* New option in GEOS.Editor "GeoCalc-Fix", see Register "PRINTER" (C64 only). If the current printer is  loaded from RAM or if the PrinterSpooler is active the size of a printer driver will be limited to be compatible with GeoCalc.
* MegaPatch/german only: Added new option "QWERTZ" to swap "Z" and "Y" on the keyboard.
* Drive icons can now be used in the file selection box with a user-defined file list.

### ToDo:
* GateWay displays a corrupted file icon at file info. The problem is known, as is the cause. Troubleshooting postponed for the time being because not easy.

### WishList:
* Save the drive driver for the boot process in a separate file in order to be able to select possibly several drivers at the system start, suitable for the selected drive.
* Make the entire memory of 16Mb available under GEOS.
* The InfoBlock for GEOS.1 and GEOS.BOOT is lost during update/MakeBoot (GEOS SaveFile routine deletes InfoBlock). Alternative: Write changed data to the sectors or restore InfoBlock after SaveFile.

---

##### Comments:
The following change was listed in the ChangeLog for the 2003 version:
> The kernel routines InitForIO and DoneWithIO have been changed so that
> is no longer switched back to 1Mhz when accessing Ram drives.

This is only partially correct:
The routines InitForIO and DoneWithIO no longer change the CLKRATE register at $D030. The RAM routines for the C=REU on the other hand still set the register to 1MHz. A comment suggests that this is necessary for the C=REU chip.

The 2003 version is able to recognize PC64 as emulator. With the emulator VICE there is no possibility if you don't switch on additional registers which make this possible.

TopDesk64/128 and GeoDOS use their own routines to exit after BASIC. Here a "cold start" is executed which releases the whole memory of the SuperCPU with RAMCard.
DualTop uses the GEOS routine "ToBASIC" in conformity with the system. This routine only performs a "warm start", the memory reserved by MegaPatch for the SuperCPU remains marked as "Reserved" and is available for a quick restart even after other programs have been used.
Note: Programs that do not respect the memory management of the SuperCPU can overwrite the memory of the SuperCPU and thus the system memory of MegaPatch.
If you want to free the whole memory of the SuperCPU/RAMCard it is sufficient to run a 'SYS64738' or switch the C64 off and on again.

TopDeskV4 displays the 1581 icon for the new (unknown) drives GeoRAM-Native/C=REU-Native/IECBus-Native.

Under VICE/x128 a wrong SETUP can lead to the fact that MegaPatch cannot be installed under GEOS 2.x (system hangs after unpacking the files). VICE should only be used for tests with default settings and without WARP mode!

The autostart file 'RUN_DUALTOP' starts the DeskTop interface 'DUAL_TOP' automatically at system start. The disadvantage of the startup program is that the first printer and mouse driver is also installed on disk, no matter which driver is set in GEOS.Editor.

geoCalc64 crashes when using printer spooler or "printer driver in RAM" when printing a file. The problem is due to geoCalc itself using a memory area reserved for the printer drivers (address $7F3F, call from geoCalc starting from $5569). The SnapShot from 2018/12/31 has a "GeoCalc-Fix" included in the GEOS.Editor.
