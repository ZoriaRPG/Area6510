# GEOS MegaPatch3
### Behobene Probleme seit 2003:
* Die Installation über GEOS.MP3 ersetzt mehrere RAMNative-Laufwerke durch ein einzelnes Laufwerk.
* Der Bildschirmschoner wird bei jedem Neustart wieder aktiviert.
* Der 1541-Cache-treiber lässt sich nicht fehlerfrei installieren bzw. de-installieren und gibt unter Umständen Systemspeicher frei was zum Absturz führen kann.
* Das ReBoot-System ist "Optional", wurde aber bei der Prüfung der Startdiskette als "fehlende Systemdatei" erkannt: Die Installation kann nicht fortgesetzt werden.
* Die Überprüfung der Systemdateien der Startdiskette war fehlerhaft und wurde behoben.
* Fehlerhafte Farbdarstellung im MegaPatch-Logo behoben.
* Beim C128 wurde im Setup-Programm andere Farben für den Autoren-Hinweis verwendet.
* Fehler in der Farbdarstellung mit Startbild "Megascreen.pic" behoben.
* Zu langer Dateiname für das Startbild überschreibt den Dateinamen für den voreingestellten Drucker.
* Die fehlerhafte Installation im 40Zeichen-Modus des C128 wurde behoben.
* Fehlerhafte Farbdarstellung in Dialogboxen im Setup-Programm im 80Zeichen-Modus des C128 behoben.
* Die Option "Schneller Speichertransfer für C=REU/MoveData" wird nicht mehr bei jedem Neustart deaktiviert.
* Die Option "Schneller Speichertransfer für C=REU/MoveData" wird nur noch bei einer C=REU als Speichererweiterung freigeschaltet.
* Der Bildschirmschoner 64erMove arbeitet jetzt mit C=REU und MoveData zusammen.
* 64erMove lässt sich jetzt auch im Editor speichern (falscher Dateiname).
* Im GEOS.Editor wurden einige Einstellungen nicht aktualisiert wenn diese durch andere Einstellungen verändert werden. Dadurch wurden [X]-Optionen als "Aktiviert" dargestellt obwohl die Option deaktiviert wurde.
* GEOS.BOOT startet jetzt die System-Uhrzeit, auch wenn diese zuvor außerhalb von GEOS angehalten wurde.
* Sofortige Aktualisierung der freien/belegten Speicherbänke in GEOS bei Änderungen im Editor.
* Das Systemdatum wird jetzt standardmäßig auf 1.1.2018 gesetzt wenn keine RTC-Uhr gefunden wird.
* Bei Verwendung einer Speichererweiterung mit 16.384KByte wurde die Größe nicht korrekt erkannt. Der Fehler wurde behoben, allerdings lassen sich nur 255x64KByte = 16.320JByte verwenden.
* Bei Verwendung von mehr als einer Setup-Diskette wird jetzt nach einem Diskettenwechsel die neue Diskette initialisiert und GEOS-interne Systemvariablen aktualisiert um ggf. einen fehlerhaften Diskettenwechsel zu erkennen.
* Beim starten ohne Hintergrundbild wird im 80Zeichen-Modus des C128 jetzt die Hintergrundfarbe auf Standard gesetzt. Notwendig da 128DUALTOP den Farbspeicher beim starten nicht löscht.
* Die automatische Erkennung einer RTC zum setzen der Uhrzeit führt bei installiertem Parallelkabel der 1571 zu einem Systemstillstand.
* Wird GEOS.MP3 von GEOS128v2/DESKTOPv2 im 80Z-Modus gestartet, dann ist evtl. das DB_DblBit-Flag nicht korrekt gesetzt. DialogBox-Icons werden dann nicht automatisch in der Breite gedoppelt.
* Löschen des Bildschirms beim verlassen von GEOS.MP3 da sonst GEOS128v2/DESKTOPv2 mit falschen Farben angezeigt wird.
* Das X-Register muß bei Verwendung von ":MoveData" unverändert bleiben. Zumindest TopDesk v4.1 hat hier Probleme wenn das X-Register verändert wird.
* Im TaskManager128 führt das Auslesen der Laufwerks-Kennbytes aus der REU über FetchRAM in die ZeroPage zu einem Fehler ("Die Laufwerkskonfiguration wurde geändert").
* Beim wechseln des aktuellen Tasks wird TurboDOS in allen Laufwerken deaktiviert, da VICE ansonsten auf Hardware-Laufwerken (1541,71,81...) mit einem DISK-JAM abstürzen kann.
* geoPaint stürzt beim verschieben des Bild-Ausschnitts bei aktivierter REU-MoveData-Option ab.
* geoPaint stürzt beim wieder herstellen einer geänderten Zeichnung ab.
* Probleme bei der Installation mit verteiltem Setup auf 2x1541-Disketten behoben.
* Fehler in de ToBASIC-Routine behoben. Unter MegaPatch64 war es damit nicht möglich BASIC-Befehle oder -Programme zu starten.

### Behobene Probleme mit der Version 2018:
* MP3 lässt sich auf einem C128 mit RAMLink und SuperCPU nicht installieren (GEOS.Editor hängt sich auf bzw. GEOS.MakeBoot stürzt ab wegen fehlender Umschaltung auf 1MHz in den Laufwerkstreibern zu RAMDrive, RAMlink und CMDHD mit Parallelkabel)
* Die Systemstart-Meldungen wurden überarbeitet inkl. der Autorenhinweise.
* StartMP_64 unter GEOS128 als "Nur unter GEOS64 lauffähig" kennzeichnen.
* Im GEOS.Editor Klarstellung der Option C=REU-MoveData: Diese Option ist mit einer SuperCPU deaktiviert, da hier 16Bit-MoveData der SuperCPU verwendet wird.
* Systemstart-Meldungen aufräumen für einen übersichtlicheren Startvorgang.
* GEOS.Editor zeigt unter DESKTOP 2.x im 80Zeichen-Modus einige Icons mit der falschen Breite an. Dies liegt daran das DESKTOP das DblBit nicht aktiviert.
* Installationsfehler mit SuperCPU/RAMCard als GEOS-DACC und GeoRAM-Native-Laufwerk als Setup-Laufwerk (Quelle und Ziel) beseitigt. GRAM_BANK_SIZE wurde nicht ermittelt da SuperCPU/RAMCard=GEOS-DACC.
* RAM81-Laufwerke konnten nicht in Verbindung mit RAMLink-Laufwerken genutzt werden.
* Bei Verwendung einer CMDHD+CMDRAMLink+Parallelkabel wird bei NativeMode-Partitionen die falsche Laufwerksgröße angezeigt.
* Wechselt man bei MP128 das Laufwerk von dem aus der Editor gestartet wurde kann es zu einem Fehler kommen ("Editor teilweise zerstört...").

### Erweiterungen/Änderungen:
* Die AutoAssembler-Dateien können Objekt-Code-Dateien und externe Symboltabellen nach dem assemblieren automatisch löschen.
* Der GEOS.Editor wurde um eine Fortschrittsanzeige beim Systemstart erweitert.
* Die AutoAssembler-Dateien können zu einem Diskettenwechsel auffordern.
* In der Datei GEOS128.BOOT wird jetzt vor dem Zugriff auf die Register ab $Dxxx der I/O-Bereich aktiviert.
* Im GEOS.Editor wurde die Möglichkeit ergänzt die GEOS-Seriennummer zu ändern und zu speichern.
* Unterstützung von GeoRAM/C=REU mit 16Mb. Die Größe wird beim Systemstart angezeigt. Die Speichererweiterungen werden bei Verwendung als GEOS-DACC weiterhin nur bis 4Mb unterstützt.
* Neue Laufwerkstreiber GeoRAM-Native/C=REU-Native. Die Treiber erlauben es den ungenutzten Speicher einer GeoRAM/C=REU als RAM-Laufwerk zu nutzen, ähnlich dem SuperRAM-Treiber.
* HD-kompatibler-NativeMode-Treiber ohne Parallelkabel-Unterstützung für Support von DNP unter SD2IEC (IECBus NativeMode). Wurde durch den SD2IEC-Treiber ersetzt, ist aber noch im SourceCode verfügbar.
* Beim Start wird der erweiterte Speicher getestet.
* Abfragen der erweiterten Speichergröße beim Start-Vorgang: Bei weniger als 192Kb Rückkehr zum BASIC.
* Neuer SD2IEC-Treiber: IECBus-NM funktioniert mit dem SD2IEC auf Grund der Einschränkungen im TurboDOS nur bis ca.8mb(127 Tracks). Der neue SD2IEC umgeht das Problem durch spezifische TurboDOS-Befehle. Daher funktioniert der Treiber nur noch mit SD2IEC.
* Auf SD2IEC/IECBusNM sind jetzt auch Unterverzeichnisse möglich.
* Bei allen Laufwerken wird jetzt beim lesen/screiben von Sektoren auf gültige Track/Sektor-Adressen geprüft.
* Änderung im Bereich 1581/NM-Laufwerken: Gemäß GEOS 2.x wird bei allen Laufwerkstypen der Diskname bei Byte $90 im BAM-Sektor eingeblendet. Anwendungen die den Disknamen ab Byte $04 in der BAM ändern funktionieren nicht mehr. Änderung entspricht dem Verhalten von GEOS 2.x!
* TaskManager128: Beim öffnen einer neuen Anwendung wird das Bildschirm-Flag ausgewertet und entsprechend der 40- oder 80-Zeichen-Bildschirm aktiviert.
* Im GEOS.Editor kann jetzt der Laufwerksmodus des SD2IEC zwischen 1541/1571/1581 oder SD2IEC/Native gewechselt werden.
* Wechsel des DiskImages auf SD2IEC ist mit dem GEOS.Editor möglich.
* Setzen der Mausgrenzen unter MP128 beim wechsel zwischen 40/80-Bildschirmmodus.
* CMD-HD-Kabel ist jetzt standardmäßig deaktiviert.
* Neue Option "GeoCalc-Fix" im GEOS.Editor unter "DRUCKER". Wenn der Druckertreiber im RAM gespeichert ist oder der Druckerspooler aktiv ist, dann wird die max. Größe von Druckertreibern reduziert um kompatibel mit GeoCalc zu sein.
* MegaPatch/Deutsch: Im GEOS.Editor wurde die Option "QWERTZ" ergänzt. Damit kann man die Tasten "Z" und "Y" auf der Tastatur zu tauschen. Funktioniert nicht mit geoKeys.
* In der Dateiauswahlbox mit einer benutzerdefinierter Dateiliste können jetzt auch Laufwerk-Icons verwendet werden.

### ToDo:
* GateWay zeigt bei Datei-Info ein beschädigtes Datei-Icon an. Das Problem ist bekannt, ebenso die Ursache. Fehlerbeseitigung vorerst zurückgestellt da nicht ganz einfach.

### WishList:
* Den Laufwerkstreiber für den Startvorgang in einer getrennten Datei speichern um evtl. mehrere Treiber beim Systemstart, passend zum gewählten Laufwerk, auswählen zu können.
* Den gesamten Speicher von 16Mb unter GEOS verfügbar machen.
* Der InfoBlock bei GEOS.1 und GEOS.BOOT geht beim Update/MakeBoot verloren (GEOS-SaveFile-Routine löscht InfoBlock). Alternative: Geänderte Daten in die Sektoren schreiben oder InfoBlock nach SaveFile wieder herstellen.

---

##### Anmerkungen:
Im ChangeLog zur Version von 2003 wurde folgende Änderung aufgelistet:
> Die Kernel-Routinen InitForIO und DoneWithIO wurden so geändert, daß
> bei Zugriffen auf Ram-Laufwerke nicht mehr auf 1Mhz zurückgeschaltet wird.

Dies ist nur teilweise richtig:
Die Routinen InitForIO und DoneWithIO verändern das CLKRATE-Register bei $D030 nicht mehr. Die RAM-Routinen für die C=REU hingegen setzen das Register weiterhin auf 1MHz. Ein Kommentar lässt vermuten das die für den C=REU-Chip erforderlich ist.

Die Version von 2003 ist in der Lage PC64 als Emulator zu erkennen. Beim Emulator VICE gibt es dazu keine Möglichkeit wenn man nicht zusätzliche Register einschaltet die das dann ermöglichen.

TopDesk64/128 und GeoDOS nutzen eigene Routinen zum Verlassen nach BASIC. Hier wird ein "Kaltstart" ausgeführt der bei der SuperCPU mit RAMCard den gesamten Speicher wieder freigibt.
DualTop nutzt systemkonform die GEOS-Routine "ToBASIC". Diese Routine führt nur einen "Warmstart" aus, der von MegaPatch reservierte Speicher der SuperCPU bleibt damit als "Reserviert" gekennzeichnet und steht auch nach der Nutzung anderer Programme für einen schnellen Neustart zur Verfügung.
Hinweis: Programme die das Speichermanagement der SuperCPU nicht beachten, können den Speicher der SuperCPU und damit den Systemspeicher von MegaPatch überschreiben.
Will man den gesamten Speicher der SuperCPU/RAMCard wieder freigeben ist es ausreichend ein 'SYS64738' auszuführen oder den C64 aus- und wieder einzuschalten.

TopDeskV4 zeigt für die neuen (unbekannten) Laufwerke GeoRAM-Native/C=REU-Native/IECBus-Native das 1581-Icon an.

Unter VICE/x128 kann ein falsches SETUP dazu führen das sich MegaPatch unter GEOS 2.x nicht Installieren lässt (System hängt nach dem entpacken der Dateien). VICE sollte für Tests nur mit Standard-Einstellungen und ohne WARP-Modus verwendet werden!

Die Autostartdatei 'RUN_DUALTOP' startet die DeskTop-Oberfläche "DUAL_TOP" automatisch beim Systemstart. Der Nachteil des Startprogrammes ist das hier auch der erste Drucker- und Maustreiber auf Disk installiert wird, egal welcher Treiber in GEOS.Editor eingestellt ist.

geoCalc64 stürzt bei der Verwendung von Druckerspooler oder "Druckertreiber im RAM" beim drucken einer Datei ab. Das Problem liegt an geoCalc selbst das einen für die Druckertreiber reservierten Speicherbereich nutzt (Adresse $7F3F, Aufruf aus geoCalc ab $5569). Der SnapShot vom 31.12.2018 verfügt über eine "GeoCalc-Fix"-Option im GEOS.Editor.
