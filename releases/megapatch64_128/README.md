# Area6510

## GEOS MEGAPATCH 64/128 [GERMAN]
For english translation please scroll down...


Dieses Verzeichnis enthält die aktuelle Versionen von GEOS MegaPatch.

#### Information
Die hier gefundenen D81-Images enthalten ein Setup für GEOS MegaPatch64 und MegaPatch128.
Die deutsche Version ist bezeichnet mit 'mp33-de.d81'

Die aktuellen Versionen enthalten kein TOPDESK mehr, da TOPDESK nicht zum GEOS MegaPatch-Kernsystem gehört.
TOPDESK aus MegaPatch von 2000/2003 kann aber weiterverwendet werden.

Im Verzeichnis "REFERENCE" findet sich das Programmierer-Handbuch / Referenz in Deutsch als D81/geoWrite und zusätzlich im PDF-Format.


.
.
.
.


## GEOS MEGAPATCH 64/128 [ENGLISH]
This directory contains current releases of GEOS MegaPatch.

#### About
The D81 images found here contain a setup for GEOS MegaPatch64 and MegaPatch128.
The englisch version is named like 'mp33-en.d81'.

The current releases no longer contain TOPDESK anymore since TOPDESK does not belong to the GEOS MegaPatch core system.
TOPDESK released with MegaPatch from 2000/2003 can still be used.
