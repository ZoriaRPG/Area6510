# Area6510
# WARNING!

THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### GEODESK64
GeoDesk is a new DeskTop for GEOS/MegaPatch64. GEOS/MegaPatch128 not supported.

GeoDesk is currently under development, many features like disk- and file operations are not yet implemented.

GeoDesk will support all CMD-Hardware and SD2IEC-devices.

GeoDesk supportes drag'n'drop and multiple windows, requires 192Kb of RAM.

# INSTALL
You need MegaAssmbler V4, enable AutoAssembler and use ass.GeoDesk to compile all files.
Set all drives in MegaAssembler to the device including the sources.

For most users install a snapshort is recommended instead of using the source code.
