# Area6510
# WARNING!

THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# GEOS MEGAPATCH 64/128 [GERMAN]
For english translation please scroll down...


### PATCH#01
Dieses Verzeichnis beinhaltet einige aktualisierte Dateien:

##### Disk#3 mp3_system:
* D5-EDIT1.CVT  => s.MP3.Edit.1
* D5-EDIT2.CVT  => - G3_Editor.Init
Die Init-Routine des GEOS.Editors wurde in eine eigene Datei ausgelagert und mit einer Status-Anzeige versehen.

##### Disk#5 mp3_program:
* D5-GMP2.CVT   => s.GEOS128.MP3
Fehlerbeseitigung 40Z-Setup. Bei fehlenden RBOOT-Dateien kann die Installation fortgesetzt werden.
* D5-GMP1.CVT   => s.GEOS64.MP3
Anpassung an die MP128-Version.

Die folgende Datei ist ein diff zur rev.2.
* [megapatch64-128.rev2+.diff](https://gitlab.com/mkslack/Area6510/tree/master/src/megapatch-3.0rev2/patch#01/megapatch64-128.rev2+.diff)

Die aktualisierten Dateien liegen im GeoConvert-Format vor. Nach der konvertierung einfach die entsprechenden Dateien auf den D81-images der Version rev.2 austauschen.

2018/06/27
M.Kanet




# GEOS MEGAPATCH 64/128 [ENGLISH]

### PATCH#01
This directory contains some updated files:

##### Disk#3 mp3_system:
* D5-EDIT1.CVT => s.MP3.Edit.1
* D5-EDIT2.CVT => - G3_Editor.Init
The Init-Routine of the GEOS.Editor has been swapped out to a separate file and now includes a status information bar.

##### Disk#5 mp3_program:
* D5-GMP2.CVT => s.GEOS128.MP3
Troubleshooting 40Z setup. When RBOOT-files are missing continue with the setup.
* D5-GMP1.CVT => s.GEOS64.MP3
Adaptation to the MP128 version.

The following file is a diff to rev.2.
megapatch64-128.rev2 + .diff

The updated files are in GeoConvert format. After the conversion simply replace the corresponding files on the D81-images of version rev.2.

2018/06/27
M.Kanet
