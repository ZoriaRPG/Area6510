#!/bin/sh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2018 Markus Kanet
#
#
# This script will convert all files in thie current directory to
# UTF-8 encoded text files using the 'convert.sh' script.
#

find ./ -type f \
	-mindepth 1 \
	-maxdepth 1 \
	-printf "%f\n" | \
	grep -v -e "filename.sh" \
		-e "filelist.txt" \
		-e "convert.sh" \
		-e "convertall.sh" | \
	sort >filelist.txt

while read filename; do
	# Rename filenames like "- G3_Code" to "-G3_Code.s.orig"
	# for the convert.sh script.
	# Note: "+G3_Code" is for MegaPatch128 files.
	new="${filename/- /-}"
	new="${new%.s}"
	new="${new/+ /+}.s.orig"
	mv -- "${filename}" "${new}"
	sed -i \
	    -e "s,\"- ,\"-,g" \
	    -e "s,\"+ ,\"+,g" \
	    -- "${new}"
	sh convert.sh "${new}"
done <filelist.txt
