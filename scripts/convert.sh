#!/bin/sh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2018 Markus Kanet
#
#
# This script will convert exported source code from C64/geoWrite using
# geoDOS into UTF-8 encoded and nicly formatted Unix-like text files.
#
# It is recommended to have the original files names like
#     Source files        : <filename>.s.orig
#     Documentation files : <filename>.txt.orig
# The output files will then become the same filename without '.orig'.
# For other files the suffix '.tmp' will be added.
#

# Define the SOURCE_FILE.
if [ x"$1" == x"" ]; then
	echo "Error: Missing <INPUT FILE>!"
	echo "Usage: $0 FILENAME"
	exit 1
else
	SOURCE_FILE="$1"
fi

# Define output target.
OUTPUT_MODE=file

#Define convert mode.
case "${SOURCE_FILE}" in
	*.s.orig	)	SOURCE_TYPE="SOURCE";;
	*.txt.orig	)	SOURCE_TYPE="PLAINTEXT";;
	*.bas.orig	)	SOURCE_TYPE="BASIC";;
	*		)	SOURCE_TYPE="SOURCE";;
esac

# Define OUTPUT_FILENAME when target is 'file'.
if [ x"${OUTPUT_MODE}" == x"file" ]; then
	if [ x"${SOURCE_FILE%.orig}" != x"${SOURCE_FILE}" ]; then
		OUTPUT_FILENAME="${SOURCE_FILE%.orig}"
	else
		OUTPUT_FILENAME="${SOURCE_FILE}.tmp"
	fi
	rm -f -- "${OUTPUT_FILENAME}"
fi

# TAB width.
TAB_WIDTH=8

# Only useful when output is redirected to screen.
if [ x"${OUTPUT_MODE}" == x"screen" ]; then
	tabs ${TAB_WIDTH:-8}
fi

# Define a temporary file for UTF conversion.
TEMPFILE="$(pwd)/temp"

# This is a conversion helper string for separator lines.
SPLIT_LINE=";******************************************************************************"

# This is a conversion helper string for TABs.
TABSTRING="@@@@@@@@@@"

# Define TAB stops:
let BLOCK1_SIZE=TAB_WIDTH*3
let BLOCK2_SIZE=TAB_WIDTH*1
let BLOCK3_SIZE=TAB_WIDTH*3
let BLOCK4_SIZE=TAB_WIDTH*5

# Some more variables.
INPUT_LINE=""
REMAINING_LINE=""
OUTPUT_LINE=""
LINE_CODE_BLOCK1=""	# Label or Variable name or Comment.
LINE_CODE_BLOCK2=""	# Command or data.
LINE_CODE_BLOCK3=""	# Command data or Comment.
LINE_CODE_BLOCK4=""	# Comment
LINE_CODE_BLOCK5=""	# Extra text.
TAB_LENGTH_CHARS=0
TAB_COUNT=0
CHARS_REMAINING=0
CODE_BLOCK_LENGTH=0
TARGET_BLOCK_LENGTH=0

# Do some verbose messages.
echo "Now processing file: '$1' -> '${OUTPUT_FILENAME}'"

# Create a copy of the source file.
cat -- "${SOURCE_FILE}" >"${TEMPFILE}"

# UTF-8 conversion source file into a temp file.
# This will convert the following characters:
# '['->Ä '\'->Ö ']'->Ü '{'->ä '|'->ö '}'->ü '~'->ß '@'->§
if [ x"${SOURCE_TYPE}" != x"BASIC" ]; then
	sed  -i -e "s,\[,\xC3\x84,g" \
		-e "s,\\\,\xC3\x96,g" \
		-e "s,\],\xC3\x9C,g" \
		-e "s,\x7B,\xC3\xA4,g" \
		-e "s,\x7C,\xC3\xB6,g" \
		-e "s,\x7D,\xC3\xBC,g" \
		-e "s,\x7E,\xC3\x9F,g" \
		-e "s,\x00\x00,,g" \
		-e "s,\xDF\x01,,g" \
		-e "s,\x58\x00,,g" \
		-e "s,\x90\x00,,g" \
		-e "s,\xF8\x00,,g" \
		-e "s,@,\xC2\xA7,g" \
		-- "${TEMPFILE}"
fi

# Convert CR into LF for Unix-like text format.
sed -i -e "s,\r,\n,g" -- "${TEMPFILE}"

# Remove EOF from end of file.
sed -i -e "s,\c@,," -- "${TEMPFILE}"

# Convert TAB into single Caracter for easier format the code.
if [ x"${SOURCE_TYPE}" == x"SOURCE" ]; then
	sed -i -e "s,\x09,@,g" -- "${TEMPFILE}"
fi

# Remove spaces at end of line.
sed -i -e "s, $,,g" -- "${TEMPFILE}"

# Add a 'Byte Order Mark' (BOM) for UTF-8 or
# Firefox will not auto detect encoding type.
if [ x"${OUTPUT_MODE}" == x"file" ]; then
	echo -e "\xEF\xBB\xBF; UTF-8 Byte Order Mark (BOM), do not remove!" >"${OUTPUT_FILENAME}"
	cat <<EOF >>"${OUTPUT_FILENAME}"
;
; Area6510 (c) by Markus Kanet
; This documentation is licensed under a
; Creative Commons Attribution-ShareAlike 4.0 International License.
;
; You should have received a copy of the license along with this
; work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
;
; This file is used for documentation of the code, not to compile the
; source code into an executable application.
;
; If you found a bug in this document, if you have problems with the
; license or if you are not mentioned as contributor then please inform
; the author of this documentation to solve the problem.
;


EOF
fi

# Source file mdoe?
if [ x"${SOURCE_TYPE}" == x"SOURCE" ]; then

	# Reformat the source code.
	while read INPUT_LINE; do
		LINE_CODE_BLOCK1=""	# Label or Variable name or Comment.
		LINE_CODE_BLOCK2=""	# Command or data.
		LINE_CODE_BLOCK3=""	# Command data or Comment.
		LINE_CODE_BLOCK4=""	# Comment
		LINE_CODE_BLOCK5=""	# Extra text.

		# First split the input data from the temp file
		# into code blocks.
		if [ x"${INPUT_LINE//@/}" != x"${INPUT_LINE}" ]; then
			LINE_CODE_BLOCK1=${INPUT_LINE//@*/}
			REMAINING_LINE=${INPUT_LINE#${LINE_CODE_BLOCK1}@}
			if [ x"${REMAINING_LINE//@/}" != x"${REMAINING_LINE}" ]; then
				LINE_CODE_BLOCK2=${REMAINING_LINE//@*/}
				REMAINING_LINE=${REMAINING_LINE#${LINE_CODE_BLOCK2}@}
				if [ x"${REMAINING_LINE//@/}" != x"${REMAINING_LINE}" ]; then
					LINE_CODE_BLOCK3=${REMAINING_LINE//@*/}
					REMAINING_LINE=${REMAINING_LINE#${LINE_CODE_BLOCK3}@}
					if [ x"${REMAINING_LINE//@/}" != x"${REMAINING_LINE}" ]; then
						LINE_CODE_BLOCK4=${REMAINING_LINE//@*/}
						LINE_CODE_BLOCK5=${REMAINING_LINE#${LINE_CODE_BLOCK4}@}
					else
						LINE_CODE_BLOCK4=${REMAINING_LINE}
					fi
				else
					LINE_CODE_BLOCK3=${REMAINING_LINE}
				fi
			else
				LINE_CODE_BLOCK2=${REMAINING_LINE}
			fi
		else
			LINE_CODE_BLOCK1=${INPUT_LINE}
		fi

		# Test for lines without TAB.
		if [ x"${LINE_CODE_BLOCK2}${LINE_CODE_BLOCK3}${LINE_CODE_BLOCK4}${LINE_CODE_BLOCK5}" != x"" ]; then

			# At leat one TAB found.
			CODE_BLOCK_LENGTH=${#LINE_CODE_BLOCK1}
			let TAB_LENGTH_CHARS=BLOCK1_SIZE-CODE_BLOCK_LENGTH
			let TAB_COUNT=TAB_LENGTH_CHARS/TAB_WIDTH

			# Wenn we have less then four characters
			# add another TAB.
			let CHARS_REMAINING=BLOCK1_SIZE-TAB_COUNT*TAB_WIDTH-CODE_BLOCK_LENGTH
			if test ${CHARS_REMAINING:-0} -gt 0 ; then
				let TAB_COUNT=TAB_COUNT+1
			fi

			# Create new line:
			# :LABEL<TABS>COMMAND
			OUTPUT_LINE="${LINE_CODE_BLOCK1}${TABSTRING:0:$TAB_COUNT}${LINE_CODE_BLOCK2}"

			# Do we have additional comments or
			# command data to add to the line?
			if [ x"${LINE_CODE_BLOCK3}${LINE_CODE_BLOCK4}${LINE_CODE_BLOCK5}" != x"" ]; then

				# Yes.
				let TARGET_BLOCK_LENGTH=BLOCK2_SIZE
				# Workaround for special case:
				#           ;comment.
				if [ x"${LINE_CODE_BLOCK3#;}" != x"${LINE_CODE_BLOCK3}" ]; then
					let TARGET_BLOCK_LENGTH=TAB_WIDTH*4
				fi

				#.variable b $00      ;comment
				CODE_BLOCK_LENGTH=${#LINE_CODE_BLOCK2}
				if test ${CODE_BLOCK_LENGTH} -gt 3 ; then
					if [ x"${LINE_CODE_BLOCK3}" == x"" -a x"${LINE_CODE_BLOCK4:0:1}" == x";" ]; then
						LINE_CODE_BLOCK3=${LINE_CODE_BLOCK4}
						LINE_CODE_BLOCK4=${LINE_CODE_BLOCK5}
						LINE_CODE_BLOCK5=""
					fi
				fi

				# Workaround for special case:
				#.variable w $0000    ;comment
				if test ${CODE_BLOCK_LENGTH} -gt 3 -a x"${LINE_CODE_BLOCK3:0:1}" == x";" ; then
					let TARGET_BLOCK_LENGTH=TAB_WIDTH*4
				fi

				if test ${CODE_BLOCK_LENGTH} -ne 0 ; then
					let TAB_LENGTH_CHARS=TARGET_BLOCK_LENGTH-CODE_BLOCK_LENGTH
					let TAB_COUNT=TAB_LENGTH_CHARS/TAB_WIDTH
					# Wenn we have less then TAB_WIDTH-1 characters
					# add another TAB.
					let CHARS_REMAINING=TARGET_BLOCK_LENGTH-TAB_COUNT*TAB_WIDTH-CODE_BLOCK_LENGTH
					if test ${CHARS_REMAINING:-0} -gt 0 ; then
						let TAB_COUNT=TAB_COUNT+1
					fi
				else
					let TAB_COUNT=TARGET_BLOCK_LENGTH/TAB_WIDTH
				fi

				# Create new line:
				# :LABEL<TABS>COMMAND<TABS>COMMAND_DATA
				OUTPUT_LINE="${OUTPUT_LINE}${TABSTRING:0:$TAB_COUNT}${LINE_CODE_BLOCK3}"

				# Workaround for special case:
				#        lda data<TAB>,y    ;comment
				if [ x"${LINE_CODE_BLOCK4:0:1}" != x";" ]; then
					OUTPUT_LINE="${OUTPUT_LINE} ${LINE_CODE_BLOCK4}"
					LINE_CODE_BLOCK4=${LINE_CODE_BLOCK5}
					LINE_CODE_BLOCK5=""
				fi

				if [ x"${LINE_CODE_BLOCK4}${LINE_CODE_BLOCK5}" != x"" ]; then

					# Workaround for special case:
					#.variable w $0000    ;comment
					CODE_BLOCK_LENGTH=${#LINE_CODE_BLOCK2}
					if test ${CODE_BLOCK_LENGTH} -gt 3 -a x"${LINE_CODE_BLOCK3:0:1}" == x";" ; then
						let TAB_COUNT=0
					else
						CODE_BLOCK_LENGTH=${#LINE_CODE_BLOCK3}
						if test ${CODE_BLOCK_LENGTH} -ne 0 ; then
							let TAB_LENGTH_CHARS=BLOCK3_SIZE-CODE_BLOCK_LENGTH
							let TAB_COUNT=TAB_LENGTH_CHARS/TAB_WIDTH
							let CHARS_REMAINING=BLOCK3_SIZE-TAB_COUNT*TAB_WIDTH-CODE_BLOCK_LENGTH
							if test ${CHARS_REMAINING:-0} -gt 0 ; then
								let TAB_COUNT=TAB_COUNT+1
							fi
						else
							let TAB_COUNT=BLOCK3_SIZE/TAB_WIDTH
						fi
					fi

					# Create new line:
					# :LABEL<TABS>COMMAND<TABS>COMMAND_DATA<TABS>COMMENT
					OUTPUT_LINE="${OUTPUT_LINE}${TABSTRING:0:$TAB_COUNT}${LINE_CODE_BLOCK4}${LINE_CODE_BLOCK5}"
				fi
			fi
		else
			# Test for begin/end of comment blocks with a ;**** separator.
			TEST_SPLIT_LINE="${LINE_CODE_BLOCK1//\*/}"
			let CHARS_REMAINING=${#LINE_CODE_BLOCK1}-${#TEST_SPLIT_LINE}
			if test ${CHARS_REMAINING:-0} -gt 40 ; then
				# Print a separator line.
				OUTPUT_LINE="${SPLIT_LINE}"
			else
				# Print complete line.
				OUTPUT_LINE="${LINE_CODE_BLOCK1}"
			fi

		fi

		# Print new code line to screen.
		if [ x"${OUTPUT_MODE}" == x"screen" ]; then
			echo -e "${OUTPUT_LINE}" | sed "s,@,\x09,g"
		fi

		# Print new code line to file.
		if [ x"${OUTPUT_MODE}" == x"file" ]; then
			echo -e "${OUTPUT_LINE}" | sed "s,@,\x09,g">>"${OUTPUT_FILENAME}"
		fi

	done <"${TEMPFILE}"
else
	cat "${TEMPFILE}" >>"${OUTPUT_FILENAME}"
fi

# Sqeeze blank lines:
cat -s -- "${OUTPUT_FILENAME}" >"${OUTPUT_FILENAME}.tmp"
mv -- "${OUTPUT_FILENAME}.tmp" "${OUTPUT_FILENAME}"

# Remove temporary file.
rm -- "${TEMPFILE}"

# Remove blanks from end of line.
sed -i "s, *$,,g" -- "${OUTPUT_FILENAME}"

# Keep original date and time.
touch --reference="${SOURCE_FILE}" -- "${OUTPUT_FILENAME}"

# Only useful when output is redirected to screen.
if [ x"${OUTPUT_MODE}" == x"screen" ]; then
	tabs 0
fi
