#!/bin/sh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2018 Markus Kanet
#
#
# This script will convert filenames from CBM lower/upper case to
# ASCII uppper/lower case.
#

find ./ -type f \
	-mindepth 1 \
	-maxdepth 1 \
	-printf "%f\n" | \
	grep -v -e "filename.sh" \
		-e "filelist.txt" \
		-e "cbm.sh" \
		-e "cbmall.sh" \
		-e "convert.sh" \
		-e "convertall.sh" | \
	sort >filelist.txt

while read filename; do
	echo "Now processing: ${filename}"
	new="${filename}"
	for lower in a b c d e f g h i j k l m n o p q r s t u v w x y z ä ö ü; do
		upper=${lower^^}
		new=${new//$upper/@}
		new=${new//$lower/$upper}
		new=${new//@/$lower}
	done
	mv -- "${filename}" "${new}"
done <filelist.txt
