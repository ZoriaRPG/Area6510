#!/bin/sh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2018 Markus Kanet
#
#
# This script will convert Unix-like text files to DOS text files which
# can be converted to geoWrite using geoDOS.
#

# Define the SOURCE_FILE.
if [ x"$1" == x"" ]; then
	echo "Error: Missing <INPUT FILE>!"
	echo "Usage: $0 FILENAME"
	exit 1
else
	SOURCE_FILE="$1"
fi

# Define OUTPUT_FILENAME.
OUTPUT_FILENAME="${SOURCE_FILE%.cbm}"
OUTPUT_FILENAME="${OUTPUT_FILENAME,,}"
rm -f -- "${OUTPUT_FILENAME}"

# Define a temporary file for UTF conversion.
TEMPFILE="$(pwd)/${OUTPUT_FILENAME}"

# Do some verbose messages.
echo "Now processing file: '$1' -> '${OUTPUT_FILENAME}'"

# Create a copy of the source file.
cat -- "${SOURCE_FILE}" >"${TEMPFILE}"

# UTF-8 conversion source file into a temp file.
# This will convert the following characters:
# '['->Ä '\'->Ö ']'->Ü '{'->ä '|'->ö '}'->ü '~'->ß '§'->@
# ISO-8859-1: ä=E4 ö=F6 ü=FC Ä=C4 Ö=D6 Ü=DC ß=DF
sed  -i -e "s,\xC3\x84,\x5B,g" \
	-e "s,\xC3\x96,\x5c,g" \
	-e "s,\xC3\x9C,\x5d,g" \
	-e "s,\xC3\xA4,\x7B,g" \
	-e "s,\xC3\xB6,\x7C,g" \
	-e "s,\xC3\xBC,\x7D,g" \
	-e "s,\xC3\x9F,\x7E,g" \
	-e "s,\xC2\xA7,@,g" \
	-- "${TEMPFILE}"

# Convert CR into LF for Unix-like text format.
sed -i -e "s,$,\r,g" -- "${TEMPFILE}"

# Remove Byte-Order-Mark from top of file.
sed -i -e "s,^\xEF\xBB\xBF,@BOM@," -- "${TEMPFILE}"
sed -i -e "/^@BOM@/ d" -- "${TEMPFILE}"

# Convert multiple TABs to single TAB.
sed -i -r -e "s,\t+,\t,g" -- "${TEMPFILE}"

# For some single-byte opcodes use two tabs between opcode and comment.
for opcode in asl brk clc cld cli clv inx iny \
              lsr nop pha php pla plp rts sec \
              sed sei tax tay tsx txa txs tya; do
    sed -i -r -e "s,$opcode\t;,$opcode\t\t;,g" -- "${TEMPFILE}"
done
