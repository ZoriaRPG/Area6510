#!/bin/sh
# Wandelt PCX-Dateien in PNG/PDF um.
# Das Format der Ursprungsdateien/PCX entspricht dem Format
# von geoPCXView/GEOS => "PAGE  1.PCX", "PAGE 20.PCX"...
# Die PCX-Dateien werden in einem Unterverzeichnis "pcx"
# erwartet, die PNGs werden in "png" erstellt.
# Das fertige PDF liegt dann in diesem Verzeichnis als "OUTPUT.PDF"

find ./pcx/ -type f -name "*.pcx" -printf "%f\n" | sort >tmpfile

while read file; do 
  TMPFILE=${file//  / 0}
  OLDFILE=${TMPFILE// /}
  NEWFILE=${OLDFILE//pcx/png}
  echo "$OLDFILE => $NEWFILE"
  convert pcx/"$file" -level 25% -scale 1920x -extent 1920x2600 -filter box png/"${NEWFILE}"
done <tmpfile

echo -n "convert -page A4 -density 25 " >makepdf.sh

for file in $(find ./png/ -type f -name "*.png" | sort); do
echo -n "$file " >>makepdf.sh
done

echo " output.pdf" >>makepdf.sh
sh makepdf.sh
