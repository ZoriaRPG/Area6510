# Area6510

# GEOS MEGAPATCH 64/128
Released: 2018/09/12 20:00
Version : V3.2

Tested platforms:
```
HOST     BASE-OS    DACC     SOURCE   TARGET      BOOT        SETUP  RESULT
------------------------------------------------------------------------------
x64      MP64-3.2   C=REU    D:REUNM  D:REUNM     A:1581      FULL   OK
xscpu    MP64-3.2   RAMCard  D:SRCNM  D:SRCNM     A:1581      FULL   OK
xscpu    MP64-3.2   RAMCard  D:SRCNM  C:IECNM/FD  C:IECNM/FD  FULL   OK 3)5)
xscpu    MP64-3.2   GeoRAM   D:GEONM  C:IECNM/FD  C:IECNM/FD  FULL   OK 5)
x128/40  MP128-3.2  C=REU    D:REUNM  D:REUNM     A:1581      FULL   OK
x128/40  GEOS2      C=REU    B:1581   B:1581      B:1581      SLCT   OK 1)
x128/80  GEOS2      C=REU    B:1581   B:1581      B:1581      FULL   OK 1)2)
C64      GD3        C=REU    C:RLNM   B:RL81      A:RL81      FULL   OK
C64      MP64-3.2   RLDACC   C:RLNM   B:RL81      A:RL81      SLCT   OK 4)
C64SCPU  GD3        C=REU    C:RL81   B:RL81      A:RL81      FULL   OK
C64SCPU  GD3        RLDAC    C:RL81   B:RL81      A:RL81      FULL   OK
C64SCPU  GD3        SRAM     C:RL81   B:RL81      A:RL81      FULL   OK
C64SCPU  GD3        C=REU    C:RL81   D:FDNM      D:FDNM      FULL   OK
C64SCPU  MP64-3.2   C=REU    C:RL81   D:IECNM/FD  D:IECNM/FD  FULL   OK 5)
C64SCPU  MP64-3.2   SRAM     C:RL81   D:IECNM/FD  D:IECNM/FD  FULL   OK 5)6)
```

1) Boot GEOS from Drive A:1571, copy TopDesk128 V4.1 named as "128 DESKTOP"
   to Drive B:. Install MegaPatch from B:1581 to B:1581. If setup crashes
   when the system should be installed reboot to GEOS 128 and start the
   application "GEOS.MP3" from Drive B:. GEOS MegaPatch will be installed.
   Done...
2) Don't use WARP-Mode on VICE in 80 column mode. Use VICE defaults, set
   Drive A: to 1571, Drive B: to 1581, setup a REU with 2Mb. Boot GEOS128.
   Setup works well in non-WARP-mode with 100%. In WARP-Mode GEOS.MP3 may
   hang during setup but might work if started from the desktop.
   MegaPatch will work fine with VICE in WARP-mode.
3) The CMD FD2000 was used with the IECBus-NativeMode disk driver.
4) Tested with two RAMlink DACC partitions.
5) Using the CMDFD as IECBus-device will work for SetupMP. When GEOS.MP3
   is launched to install MegaPatch the automatic drive detection will
   install the CMDFD as FDNativeMode device. This is normal. If you want
   to test the IECBus driver simply setup the device using the GEOS.Editor
   and start GEOS.MakeBoot again.
6) This Test was done twice. For the 2nd test the first 4Mb of the RAMcard
   got locked to test the SuperCPU memory management. MegaPatch detected
   the RAMCard memory during the update beginning at $42:0000 -> OK.

Note: On C64 DUALTOP was used as DESKTOP replacement. Since GEOS expect
the DeskTop application to be named as "DESK TOP" DualTop was renamed.
