# Area6510

## GEOS MEGAPATCH 64/128 [GERMAN]
For english translation please scroll down...


Dieses Verzeichnis enthält Entwickler-Versionen von GEOS MegaPatch. Diese können instabil sein und Datenverlust verursachen. Benutzung auf eigene Gefahr!

#### Information
Die hier gefundenen D81-Images enthalten ein Setup für GEOS MegaPatch64 und MegaPatch128.

Die archivierten Releases für die Version V3.01 werden wie die Originalversion von 2003 mit einer Version von TOPDESK ausgeliefert.
Die aktuellen Snapshots enthalten kein TOPDESK mehr, da TOPDESK nicht zum GEOS MegaPatch-Kernsystem gehört.

Die aktuellen Snapshots basieren auf den neuesten Entwickler-Version von GEOS MegaPatch, einschließlich BugFixes und Verbesserungen.

Im Moment gibt es nur die deutsche Version, dazu ist eine deutsche Version von TOPDESK erforderlich wenn man es ausprobieren will.


.
.
.
.


## GEOS MEGAPATCH 64/128 [ENGLISH]
This directory contains developer versions of GEOS MegaPatch. These can be unstable and cause data loss. Use at your own risk!

#### About
The D81 images found here contain a setup for GEOS MegaPatch64 and MegaPatch128.

The archived releases for version V3.01, like the original version of 2003, are delivered with a version of TOPDESK.
The current snapshots no longer contain TOPDESK since TOPDESK does not belong to the GEOS MegaPatch core system.

The latest snapshots are based on the latest developer release of GEOS MegaPatch, including bugfixes and enhancements.

At the moment there is only the german version, a german version of TOPDESK is required if you want to try it.
